<?php 
class Db_log 
{

    function __construct() 
    {
    }


    function logQueries() 
    {

        $CI = & get_instance();
        if(!isset($CI->db)){
            return;
        }
        $filepath = APPPATH . 'logs/Query-log-' . date('Y-m-d') . '.json';         
        $data = [];
        if(file_exists($filepath)){
            $file = file_get_contents($filepath);
            if(!empty($file)){
                $data = json_decode($file);
            }
        }
        $times = $CI->db->query_times;
        foreach ($CI->db->queries as $key => $sql) 
        { 
            if(!empty($_SESSION['user'])){
                $accion = '';
                if(strpos($sql,'INSERT')>-1){
                    $accion = 'insert';
                }
                if(strpos($sql,'UPDATE')>-1){
                    $accion = 'update';
                }
                if(strpos($sql,'DELETE')>-1){
                    $accion = 'delete';
                }           
                if(!empty($accion)){                
                    $query = str_replace("'","",$sql);
                    $data[] = [
                        'user_id'=>$_SESSION['user'],
                        'accion'=>$accion,
                        'query'=>$query,
                        'fecha'=>date("Y-m-d H:i:s")
                    ];                                        
                }
            }
        }
        @file_put_contents($filepath,json_encode($data));        
    }

}