<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Panel extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->check_database_version();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');                
                if(empty($_SESSION['user'])){
                    header("Location:".base_url());
                    die();
                }                
                if(!$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operaci贸n','403');
                    exit;
                } 
                if(!empty($_SESSION['cajadiaria']) && $this->db->get_where('cajadiaria',['id'=>$_SESSION['cajadiaria']])->row()->abierto==0){
                    unset($_SESSION['cajadiaria']);
                    redirect('panel/selcajadiaria');
                }
                $this->as = array('usuarios'=>'user','categorias'=>'categoriaproducto');
        }

        function check_database_version(){                            
            $this->load->config('migration');
            $required = false;
            if($this->db->table_exists('migrations')){
                $this->db->limit(1);
                $version = $this->db->get('migrations')->row()->version;
            }else{
                $required = true;
            }
            if (!$required && $this->config->item('migration_version') != $version) {
               $required = true;
            }else{
                $version = $this->config->item('migration_version');
            }
            if($required){
                if(empty($_GET['apply_migration'])){
                    $this->loadView([
                        'view'=>'panel',
                        'crud'=>'user',
                        'output'=>$this->load->view('migration_required',[],TRUE)
                    ]);   
                }else{
                    $this->load->library('migration',[
                        'migration_enabled'     => true,
                        'migration_type'        => $this->config->item('migration_type'),
                        'migration_table'       => $this->config->item('migration_table'),
                        'migration_auto_latest' => TRUE,
                        'migration_version'     => $version,
                        'migration_path'        => $this->config->item('migration_path'),
                    ]);    
                    redirect('panel','refresh');
                }
                die();
            }               
        }

        function validate_caja(){
            if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
                header("Location:".base_url('panel/selsucursal'));
                die();
            }
            if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
                header("Location:".base_url('panel/selcaja'));
                die();
            }
            if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
                header("Location:".base_url('panel/selcajadiaria'));
                die();
            }  
        }

        public function index($url = 'main',$page = 0)
        {
                if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
                    header("Location:".base_url('panel/selsucursal'));
                    die();
                }
                if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
                    header("Location:".base_url('panel/selcaja'));
                    die();
                }
                if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
                    header("Location:".base_url('panel/selcajadiaria'));
                    die();
                }            
                $this->loadView('panel');
        }
        
        public function loadView($param = array('view' => 'main'))
        {
            if(empty($_SESSION['user'])){
                header("Location:".base_url('main?redirect='.$_SERVER['REQUEST_URI']));
            }            
            else{
                if(!empty($param->output)){
                    $param->view = empty($param->view)?'panel':$param->view;
                    $param->crud = empty($param->crud)?'user':$param->crud;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                }
                parent::loadView($param);            
            }
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            if($this->user->admin==0){
              //  $crud->set_model('usuario_model');
            }
            $crud->set_theme('bootstrap');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            $crud->unset_jquery();
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }         
        
        function selsucursal($x = '', $y = '')
        {
            unset($_SESSION['caja']);
            unset($_SESSION['cajanombre']);
            unset($_SESSION['cajadiaria']);             
            $x = (empty($x) || !is_numeric($x)) && !empty($this->user->sucursales_id)?$this->user->sucursales_id:$x;

            if(!empty($x) && is_numeric($x))
            {
                $_SESSION['sucursal'] = $x;
                $y = empty($y) && is_numeric($x)?$this->db->get_where('sucursales',['id'=>$x])->row()->denominacion:$y;
                $_SESSION['sucursalnombre'] = urldecode(str_replace('-','+',$y));
                if(empty($_GET['redirect']))
                header("Location:".base_url('panel/selcaja'));
                else
                header("Location:".base_url($_GET['redirect']));
            }
            else{
            $crud = new ajax_grocery_CRUD();
            $crud->set_subject('Seleccione una sucursal');
            $crud->set_table('sucursales');
            $crud->set_theme('bootstrap');
            
            $crud->unset_delete()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_print()
                 ->unset_read()
                 ->unset_export()
                 ->columns('denominacion')
                 ->callback_column('denominacion',function($val,$row){$redirect = empty($_GET['redirect'])?'':'?redirect='.$_GET['redirect']; return '<a href="'.base_url('panel/selsucursal/'.$row->id.'/'.str_replace('+','-',urlencode($val)).$redirect).'">'.$val.'</a>';});
            
            $output = $crud->render();
            $output->crud = 'user';
            $output->view = 'panel';
            $output->title = 'Seleccione una sucursal';
            $output->output = '<h2>Seleccione una sucursal</h2>'.$output->output;
            $this->loadView($output);
            }
        }
        
        function selcaja($x = '',$y = '')
        {
            unset($_SESSION['cajadiaria']);
            $x = (empty($x) || !is_numeric($x)) && !empty($this->user->cajas_id)?$this->user->cajas_id:$x;
            if(!empty($x) && is_numeric($x))
            {
                $_SESSION['caja'] = $x;    
                $y = empty($y) && is_numeric($x)?$this->db->get_where('cajas',['id'=>$x])->row()->denominacion:$y;
                $_SESSION['cajanombre'] = urldecode(str_replace('-','+',$y)); 
                if(empty($_GET['redirect']))
                header("Location:".base_url('panel/selcajadiaria'));
                else
                header("Location:".base_url($_GET['redirect']));
            }
            else{
            $crud = new ajax_grocery_CRUD();
            $crud->set_subject('Seleccione una caja');
            $crud->set_table('cajas');
            $crud->set_theme('bootstrap');
            if(!empty($_SESSION['sucursal']))
                $crud->where('cajas.sucursal',$_SESSION['sucursal']);
            $crud->unset_delete()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_print()
                 ->unset_read()
                 ->unset_export()
                 ->columns('denominacion')
                 ->callback_column('cajas.denominacion',function($val,$row){
                    $redirect = empty($_GET['redirect'])?'':'?redirect='.$_GET['redirect']; 
                    return '<a href="'.base_url('panel/selcaja/'.$row->id.'/'.str_replace('+','-',urlencode($val)).$redirect).'">'.$val.'</a>';
                })   
                ->callback_column('denominacion',function($val,$row){
                    $redirect = empty($_GET['redirect'])?'':'?redirect='.$_GET['redirect']; 
                    return '<a href="'.base_url('panel/selcaja/'.$row->id.'/'.str_replace('+','-',urlencode($val)).$redirect).'">'.$val.'</a>';
                });          
            
            $output = $crud->render();
            $output->crud = 'user';
            $output->view = 'panel';
            $output->title = 'Seleccione una caja';
            $output->output = '<h2>Seleccione una caja</h2>'.$output->output;
            $this->loadView($output);
            }
        }
        
        function selcajadiaria($x = '')
        {
            if(empty($x) && !empty($_SESSION['caja'])){
                $cajadiarias = $this->db->get_where('cajadiaria',array('caja'=>$_SESSION['caja'],'cajadiaria.abierto'=>1));
                if($cajadiarias->num_rows()==1){
                    //$x = $cajadiarias->row()->id;
                }
            }
            if(!empty($x) && is_numeric($x))
            {
                $this->db->select('cajadiaria.*,cajas.bloquear_ingreso');
                $this->db->join('cajas','cajas.id = cajadiaria.caja');
                $cajadiaria = $this->db->get_where('cajadiaria',['cajadiaria.id'=>$x])->row();
                if($cajadiaria->usuario!=$this->user->id && $cajadiaria->bloquear_ingreso==1){
                    header("Location:".base_url('panel/selcaja'));
                    return;
                }
                $_SESSION['cajadiaria'] = $x;
                if(empty($_GET['redirect']))
                header("Location:".base_url('panel'));
                else
                header("Location:".base_url($_GET['redirect']));
            }
            else{
            $crud = new ajax_grocery_CRUD();
            $crud->set_subject('Seleccione una cajadiaria');
            $crud->set_table('cajadiaria');
            $crud->set_theme('bootstrap');
            if(!empty($_SESSION['caja']))
                $crud->where('caja',$_SESSION['caja']);
            $crud->where('abierto',1);
            if ($this->db->get_where('cajadiaria', array('caja' => $_SESSION['caja'], 'sucursal' => $_SESSION['sucursal'], 'abierto' => 1))->num_rows() > 0) {
                $crud->unset_add();
                $crud->where('cajadiaria.abierto', 1);
            }
            if($x=='add'){
                redirect('cajas/admin/cajadiaria/add');
            }
            $crud->unset_delete()                 
                 ->unset_edit()
                 ->unset_print()
                 ->unset_read()
                 ->unset_export()
                 ->columns('fecha_apertura','monto_inicial','fecha_cierre','correlativo')
                 ->callback_column('fecha_apertura',function($val,$row){$redirect = empty($_GET['redirect'])?'':'?redirect='.$_GET['redirect']; return '<a href="'.base_url('panel/selcajadiaria/'.$row->id.$redirect).'" data-user="'.$row->usuario.'" data-bloquear="'.$this->db->get_where('cajas',['id'=>$row->caja])->row()->bloquear_ingreso.'">'.$val.'</a>';});
            
            $output = $crud->render();
            $output->crud = 'selcaja';
            $output->view = 'panel';
            $output->title = 'Seleccione una caja diaria';            
            $this->loadView($output);
            }
        }
        //JSONS                   
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
