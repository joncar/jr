<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_start();

class Instalacion extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    //Insert body css & js
    public $js = [];
    public $css = [];
    //Insert head css & js
    public $hjs = [];
    public $hcss = [];
    public function __construct() {
        parent::__construct();        
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->library('form_validation');
    }

    public function instalar() {        
       if(empty($_POST)){
       		$this->loadView([
       			'view'=>'instalacion/main',
       			'crud'=>'user',
       			'title'=>'Instalación de Binario SysVentas',
       			'favicon'=>'',
       			'output'=>$this->load->view('instalacion/formulario',[],TRUE)
       		]);
       }else{
            $this->form_validation->set_rules('dbhost','HOST','required')
                                  ->set_rules('dbname','DBName','required')
                                  ->set_rules('dbuser','DBUser','required')
                                  ->set_rules('dbpass','DBPassword','required');
            if($this->form_validation->run()){
                echo '<p>Datos recibidos</p>';
                echo '<p>Intentando conectarse</p>';
                $db = $_POST;
                $cn = mysqli_connect($db['dbhost'],$db['dbuser'],$db['dbpass']);
                $result = $cn->query("SHOW DATABASES;");
                $exists = false;
                echo '<p>Verificando si la base de datos existe</p>';
                while($obj = $result->fetch_object()){
                    if($obj->Database==$db['dbname']){
                        $exists = true;
                    }
                }
                if(!$exists){//Se crea la base de datos
                    echo '<p>Creando nueva base de datos</p>';
                    $cn->query('CREATE DATABASE '.$db['dbname']);
                    echo '<p>base de datos creada</p>';
                }
                $cn = mysqli_connect($db['dbhost'],$db['dbuser'],$db['dbpass'],$db['dbname']);
                echo '<p>Importando tablas</p>';
                $sql = file_get_contents('sysventas.sql');
                //print_r($sql);
                echo '<hr>';
                $response = $this->importSQL($cn,$sql);
                if (!$response) {
                    printf("Importación fallida: %s\n");                    
                }else{
                    echo '<hr>';
                    echo '<p>Base de datos importada</p>';
                    echo '<p>Creando fichero de conexión</p>';
                    $dbs = '';
                    $dbs.= '$db[\'default\'][\'hostname\'] = \''.$db['dbhost'].'\';';
                    $dbs.= '$db[\'default\'][\'username\'] = \''.$db['dbuser'].'\';';
                    $dbs.= '$db[\'default\'][\'password\'] = \''.$db['dbpass'].'\';';
                    $dbs.= '$db[\'default\'][\'database\'] = \''.$db['dbname'].'\';';                
                    $dbs.= '$db[\'default\'][\'dbdriver\'] = \'mysqli\';';
                    $dbs.= '$db[\'default\'][\'dbprefix\'] = \'\';';
                    $dbs.= '$db[\'default\'][\'pconnect\'] = TRUE;';
                    $dbs.= '$db[\'default\'][\'db_debug\'] = FALSE;';
                    $dbs.= '$db[\'default\'][\'cache_on\'] = TRUE;';
                    $dbs.= '$db[\'default\'][\'cachedir\'] = \'\';';
                    $dbs.= '$db[\'default\'][\'char_set\'] = \'utf8\';';
                    $dbs.= '$db[\'default\'][\'dbcollat\'] = \'utf8_general_ci\';';
                    $dbs.= '$db[\'default\'][\'swap_pre\'] = \'\';';
                    $dbs.= '$db[\'default\'][\'autoinit\'] = FALSE;';
                    $dbs.= '$db[\'default\'][\'stricton\'] = FALSE;';
                    $dbs.= '$db[\'default\'][\'save_queries\'] = TRUE;';
                    file_put_contents(APPPATH.'config/database.php','<?php if ( ! defined(\'BASEPATH\')) exit(\'No direct script access allowed\');$active_group = \'default\';$active_record = TRUE; '.$dbs.'?>');
                    echo '<p>Fichero Creado</p>';
                    echo '<p>Clave de administrador de usuario: <b>root@binario.com.py</b> Pass: <b>12345678</b>';
                    echo '<p>Importación finalizada, <a href="'.base_url().'">intente ingresar nuevamente</a>';
                }                
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }

    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    function getHead($page){        
        return $page;
    }

    function getBody($page){                
        return $page;
    }

    public function loadView($param = array('view' => 'main')) {

        if (is_string($param)){
            $param = array('view' => $param);
        }
        if(is_object($param)){
            $param = (array)$param;
        }               
        $param['view'] = $this->load->view($param['view'],$param,TRUE);
        $param = array_merge($param,['css'=>$this->css,'js'=>$this->js,'hcss'=>$this->hcss,'hjs'=>$this->hjs]);        
        if(!empty($param['css_files'])){
            foreach($param['css_files'] as $p){
                $param['hcss'][] = '<link rel="stylesheet" href="'.$p.'" type="text/css">';
            }
        }
        if(!empty($param['js_files'])){
            foreach($param['js_files'] as $p){
                $param['js'][] = '<script src="'.$p.'"></script>';
            }        
        }
        
        $page = $this->load->view('template', $param,true);        
        $page = $this->getHead($page);
        $page = $this->getBody($page); 
        echo $page;
    }

    function importSQL($cn,$sql){        
        $types = explode('DELIMITER',$sql);

        foreach($types as $type){
            if(!strpos($type,'PROCEDURE') && !strpos($type,'TRIGGER')){
                $type = str_replace(['$$'],[''],$type);
                $sq = explode(';',$type);
                foreach($sq as $s){
                    if(!$cn->query($s)){
                        echo "<p>Importación fallida: ".$cn->error." -- ".$s."</p>";
                    }
                }
            }
        }
        die;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
