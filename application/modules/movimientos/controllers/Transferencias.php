<?php

require_once APPPATH.'/controllers/Panel.php';    

class Transferencias extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
        }                
    }
    
    public function transferencias($x = '', $y = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/transferencias'));
        else {
            $crud = parent::crud_function($x, $y);
            $crud->set_theme('bootstrap');
            $crud->unset_delete();
            $crud->set_relation('sucursal_origen', 'sucursales', 'denominacion')
                    ->set_relation('sucursal_destino', 'sucursales', 'denominacion');
            $crud->display_as('procesado', 'Status');            
            $crud->field_type('procesado', 'dropdown', array('0' => 'No procesado', '-1' => 'Rechazado', '2' => 'Aprobado', '3' => 'Entregado'));           
            $crud->callback_column('s574b0641',function($val,$row){
                return '<a href="javascript:showDetail('.$row->id.')">'.$val.'</a>';
            });
            $crud->callback_column('procesado', function($val, $row) {
                if ($row->sucursal_origen == $_SESSION['sucursal'] && $val == 0){
                    return form_dropdown('procesado', array('0' => 'Sin procesar', '-1' => 'Rechazado', '1' => 'Aprobado'), $val, 'class="procesado form-control" data-rel="' . $row->id . '"');
                }
                elseif ($row->sucursal_destino == $_SESSION['sucursal'] && $val == 1){
                    return form_dropdown('procesado', array('1' => 'Aprobado', '2' => 'Entregado'), $val, 'class="procesado form-control" data-rel="' . $row->id . '"');
                }
                else {
                    switch ($val) {
                        case '0': return 'No procesado';
                        case '-1': return 'Rechazado';
                        case '1': return 'Aprobado';
                        case '2': return 'Entregado';
                    }
                }
            });
            $crud->order_by('id','DESC');
            $crud->columns('id','sucursal_origen','sucursal_destino','motivo_transferencia_id','fecha_solicitud','procesado','user_id');
            if(is_numeric($x)){
                $crud->where('procesado',$x);
            }
            $crud->set_rules('productos','Productos','required|callback_unProducto|callback_validar');        
            $crud->callback_after_insert(array($this,'addBody'));
            $output = $crud->render();        
            if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
                $edit = '';
                $default = array();
                if($crud->getParameters()=='edit'){
                    $edit = $y;                
                }
                $output->output = $this->load->view('transferencias',array('output'=>$output->output,'edit'=>$edit),TRUE);
            }
            $this->loadView($output);  
        } 
    }

    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        //$this->db->delete('ventadetalle',array('venta'=>$primary));
        foreach($productos as $p){
            $pro[] = $p->nombre;            
            $p->producto = @$this->db->get_where('productos',array('codigo'=>$p->codigo))->row();            
            $vencimiento = !isset($p->vencimiento) || empty($p->vencimiento)?'0000-00-00':$p->vencimiento;
            $this->db->insert('transferencias_detalles',array(
                'transferencia'=>$primary,
                'lote'=>'',
                'producto'=>$p->codigo,
                'cantidad'=>$p->cantidad,
                'vencimiento'=>$vencimiento,
                'producto'=>$p->codigo,
                'stock_anterior_destino'=>@$this->db->get_where('productosucursal',['sucursal'=>$post['sucursal_destino'],'producto'=>$p->codigo])->row()->stock,
                'stock_anterior_origen'=>@$this->db->get_where('productosucursal',['sucursal'=>$post['sucursal_origen'],'producto'=>$p->codigo])->row()->stock
            ));
        }
        $this->db->update('transferencias',array('productos'=>implode($pro)),array('id'=>$primary));                
    }

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }

    function validar(){ 
        $productos = $_POST['productos'];       
        $ajustes = $this->ajustes;
        $productos = json_decode($_POST['productos']);        
        $return = true;
        //Validar sucursales
        $msj = '';
        foreach($productos as $p){                
            $pro = $this->db->get_where('productos',array('codigo'=>$p->codigo));
            if($pro->num_rows()==0){
                $return = false;
                $msj.= '<p>El producto '.$p->codigo.' no existe</p>';
            }else{
                $pro = $pro->row();
            }
        }
        //Validar stocks                
        if($return && $this->ajustes->vender_sin_stock==0){
            $msj = '';
            foreach($productos as $p){                
                $pro = $this->db->get_where('productos',array('codigo'=>$p->codigo));
                if($pro->num_rows()==0){
                    $return = false;
                    $msj.= '<p>El producto '.$p->codigo.' no existe</p>';
                }else{
                    $pro = $pro->row();
                    if($pro->inventariable==1){
                        $producto = $this->db->get_where('productosucursal',array('producto'=>$p->codigo,'sucursal'=>$_POST['sucursal_origen']));
                        if($producto->num_rows()==0 || $producto->row()->stock<$p->cantidad){
                            $return = false;
                            $msj.= '<p>El producto '.$p->codigo.' no tiene stock suficiente</p>';
                        }
                    }
                }
            }            
        }
        if($_POST['sucursal_origen']==$_POST['sucursal_destino']){
            $return = false;
            $msj.= '<p>La sucursal de origen debe ser diferente a la de destino</p>';
        }
        if(!$return){
            $this->form_validation->set_message('validar',$msj);
        }
        return $return;
    }

    function getFactura($x = '',$y = ''){
        if ($x == 'imprimir') {
            if (!empty($y) && is_numeric($y)) {
                $this->db->select('transferencias.*, proveedores.denominacion as proveedorname, motivo_salida.denominacion as motivoname, CONCAT(user.nombre," ",user.apellido) as usuario',FALSE);
                $this->db->join('motivo_salida','motivo_salida.id = transferencias.motivo');
                $this->db->join('proveedores','proveedores.id = transferencias.proveedor');
                $this->db->join('user','user.id = transferencias.usuario');
                $venta = $this->db->get_where('transferencias', array('transferencias.id' => $y));
                if ($venta->num_rows() > 0) {
                    ob_clean();
                    $this->db->select('salida_detalle.*, productos.nombre_comercial');
                    $this->db->join('productos', 'salida_detalle.producto = productos.codigo');
                    $detalles = $this->db->get_where('salida_detalle', array('salida' => $venta->row()->id));
                    $papel = 'A4';
                    $this->load->library('html2pdf/html2pdf');
                    $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', array(5, 5, 5, 8));
                    $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/imprimir_salida', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                    $html2pdf->Output('Reporte Legal.pdf', 'D');
                    //echo $this->load->view('reportes/imprimir_salida',array('venta'=>$venta->row(),'detalles'=>$detalles),TRUE);                    
                } else {
                    echo "Factura no encontrada";
                }
            } else {
                echo 'Factura no encontrada';
            }
        } else {
            $this->loadView($output);
        }
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
