<?php

require_once APPPATH.'/controllers/Panel.php';    

class Presupuesto extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }

    function notas_pedido($x = '', $y = ''){
        $this->as['notas_pedido'] = 'presupuesto';
        $crud = parent::crud_function($x, $y);
        if (!empty($_SESSION['sucursal'])){
            $crud->where('presupuesto.sucursal', $_SESSION['sucursal']);
        } 
        if (!empty($_SESSION['caja'])){
            $crud->where('presupuesto.caja', $_SESSION['caja']);
        }        
        $this->presupuesto($crud);
    }
    /* Cruds */    
    
    public function presupuesto($x = '', $y = '') {
        if($x=='updateTemp' && !empty($_POST['productos'])){
            $this->updateTemp();
            return;
        }

        if($x=='getTemp'){
            $this->getTemp();
            return;
        }

        if($x=='cleanTemp'){
            $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'presupuesto']);       
            return;
        }
        $crud = is_object($x)?$x:parent::crud_function($x, $y);
        $crud->set_theme('bootstrap');
        $crud->unset_read();

        $crud->columns('id','transaccion', 'cliente', 'fecha', 'caja', 'observacion','total_presupuesto','usuario','repartidor_id','sucursal');
        $crud->unset_searchs('facturado');
        if($this->user->admin!=1){
            $crud->unset_edit()
                 ->unset_delete();
        }
        $crud->set_relation('transaccion', 'tipotransaccion', 'denominacion')
             ->set_relation('cliente', 'clientes', '{nombres} {apellidos}')
             ->set_relation('sucursal', 'sucursales', 'denominacion')
             ->set_relation('usuario','user','{nombre} {apellido}')
             ->set_relation('repartidor_id','repartidores','{denominacion}')
             ->display_as('usuario','Vendedor')
             ->display_as('repartidor_id','Repartidor');                
        $crud->order_by('id','DESC');        
        ///Validations
        $crud->set_rules('total_presupuesto','Total de presupuesto','required|numeric|greater_than[0]');        
        $crud->set_rules('productos','Productos','required|callback_unProducto|callback_validar');                        
        $crud->callback_before_insert(array($this,'validarFacturacion'));
        $crud->callback_after_insert(array($this,'addBody'));        
        $crud->callback_after_update(array($this,'updateBody'));
        $crud->callback_after_delete(function($primary){get_instance()->db->delete('presupuesto_detalle',array('presupuesto_id'=>$primary));});        
        $crud->callback_column('facturado',function($val,$row){
            return $val=1?'<span class="badge badge-success">SI</span>':'<span class="badge badge-danger">NO</span>';
        });
        $crud->unset_back_to_list();
        $crud->add_action('Facturar','',base_url('movimientos/ventas/ventas/add/Presupuesto/').'/');
        if($y=='return'){
            return $crud;
        }
        $crud->where('facturado = 0','ESCAPE',TRUE);
        $output = $crud->render();   
        if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
            $edit = '';
            if($crud->getParameters()=='edit'){
                $edit = $y;                
            }
            $output->output = $this->load->view('presupuesto',array('output'=>$output->output,'edit'=>$edit),TRUE);
        }else{
            $output->output = $this->load->view('presupuesto_list',array('output'=>$output->output),TRUE);
        }
        $this->loadView($output);     
    }

    public function presupuestodetalle($x = '', $y = '') {         
        $crud = parent::crud_function($x, $y);
        if(!empty($_POST['presupuesto_id'])){
            $crud->where('presupuesto_id',$_POST['presupuesto_id']);
        }
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$val))->row();
        });        
        $crud->unset_add()->unset_edit()->unset_delete();
        $output = $crud->render();        
        $this->loadView($output);     
    }
    //Callback para validar si la venta almacena el id de factura o no
    function validarFacturacion($post){        
        $post['fecha'] = date("Y-m-d H:i:s");
        return $post;
    }
    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();        
        foreach($productos as $p){
            $pro[] = $p->nombre;
            $p->preciocosto = @$this->db->get_where('productos',array('codigo'=>$p->codigo))->row()->precio_costo;
            $p->total_adic = ($p->precio_venta*$p->cantidad)+((($p->precio_venta*$p->cantidad)*$p->por_venta)/100);
            $this->db->insert('presupuesto_detalle',array(
                'presupuesto_id'=>$primary,
                'producto'=>$p->codigo,                
                'cantidad'=>$p->cantidad,                
                'precio_venta'=>$p->precio_venta,  
                'porc'=>$p->por_venta,              
                'total'=>$p->total,
                'total_adic'=>$p->total_desc,
                'iva'=>0
            ));
        }
        $this->db->update('presupuesto',array('productos'=>implode($pro)),array('id'=>$primary));  
        $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'presupuesto']);             
    }   

    function updateBody($post,$primary){
        $postBody = json_decode($_POST['productos']);
        $beforep = $this->db->get_where('presupuesto_detalle',['presupuesto_id'=>$primary,'anulado'=>0]);        
        $before = [];
        foreach($beforep->result() as $b){
            $before[] = $b->producto;
        }

        //Anular los que no vinieron en el array
        foreach($beforep->result() as $b){
            $exists = false;
            foreach($postBody as $n=>$p){
                if($p->codigo==$b->producto){
                    $exists = true;
                }
            }
            if(!$exists){
                $this->db->update('presupuesto_detalle',['anulado'=>1],['id'=>$b->id]);
            }
        }         
        foreach($postBody as $n=>$p){            
          //INSERT          
          if(!in_array($p->codigo,$before)){ //INSERT              
              $this->db->insert('presupuesto_detalle',[
                'presupuesto_id'=>$primary,
                'producto'=>$p->codigo,                
                'cantidad'=>$p->cantidad,                
                'precio_venta'=>$p->precio_venta,   
                'porc'=>$p->por_venta,                         
                'total'=>$p->total,
                'total_adic'=>$p->total_desc,
                'iva'=>0,
                'anulado'=>0
              ]);
          }else{ //Update              
              $this->db->update('presupuesto_detalle',[
                'cantidad'=>$p->cantidad,                
                'precio_venta'=>$p->precio_venta, 
                'porc'=>$p->por_venta,                           
                'total'=>$p->total,
                'total_adic'=>$p->total_desc,
                'iva'=>0
              ],['producto'=>$p->codigo,'presupuesto_id'=>$primary]);
          }
      }      
    } 

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }

    function validar(){ 
        $productos = $_POST['productos'];       
        $ajustes = $this->ajustes;        
        if($_POST['transaccion']==2 && $_POST['cliente']==1){
            $this->form_validation->set_message('validar','Debe indicar a que cliente desea asignarle esta venta a crédito');        
            return false;
        }
        return true;
    }

    function presupuesto_detail($ventaid){
        $this->as['ventas_detail'] = 'presupuesto_detalle';
        $crud = $this->crud_function('','');
        $crud->where('presupuesto_id',$ventaid);
        $crud->columns('producto','cantidad','totalcondesc');
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$row->producto))->roW()->nombre_comercial;
        }); 
        $crud->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_export()
             ->unset_print()
             ->unset_read();
        $crud->staticShowList = true;
        $crud = $crud->render();
        echo $crud->output;

    }

    public function facturadas(){
        $this->as['facturadas'] = 'presupuesto';
        $crud = parent::crud_function('','');        
        $crud = $this->presupuesto($crud,'return');
        $crud->where('facturado',1);
        $crud->set_url('movimientos/presupuesto/facturadas/');
        $output = $crud->render();
        echo $output->output;
    }

    function updateTemp(){
        $productos = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'presupuesto']);
        if($productos->num_rows()==0){
            $this->db->insert('ventatemp',['productos'=>$_POST['productos'],'cajas_id'=>$this->user->caja,'user_id'=>$this->user->id,'tipo'=>'presupuesto']);
        }else{
            $this->db->update('ventatemp',['productos'=>$_POST['productos'],'user_id'=>$this->user->id],['cajas_id'=>$this->user->caja,'tipo'=>'presupuesto']);
        }
        echo 'success';
    }

    function getTemp(){
        $productos = [];
        $pp = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'presupuesto']);
        if($pp->num_rows()>0){
            $productos = json_decode($pp->row()->productos);
        }
        echo json_encode($productos);
    }


}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
