<?php

require_once APPPATH.'/controllers/Panel.php';    

class OrdenTrabajo extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    } 

    function anular_venta(){
        $this->form_validation->set_rules('id','ID','required|integer');
        if($this->form_validation->run()){
            $id = $this->input->post('id');
            if($this->db->get_where('ventas',['orden_trabajo_id'=>$id])->num_rows()==0){
                $this->db->update('orden_trabajo',array('anulado'=>1),array('id'=>$id));                
                echo $this->success('factura anulada con exito').refresh_list();
            }
        }
        else echo $this->error('Ha ocurrido un error');
    }
    
    public function orden_trabajo($x = '', $y = '') {
        if($x=='updateTemp' && !empty($_POST['productos'])){
            $this->updateTemp();
            return;
        }

        if($x=='getTemp'){
            $this->getTemp();
            return;
        }

        if($x=='cleanTemp'){
            $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'orden_trabajo']);       
            return;
        }

        if($x=='anular'){
            $this->anular_venta();
            die();
        }
        $crud = is_object($x)?$x:parent::crud_function($x, $y);
        $crud->set_theme('bootstrap');
        $crud->unset_read();     
        if($crud->getParameters()=='edit' && $this->db->get_where('orden_trabajo',['id'=>$y])->row()->anulado==1){
            redirect('movimientos/ordenTrabajo/orden_trabajo');
        } 
        
        if($this->user->admin!=1){
            $crud->unset_edit()
                 ->unset_delete();
        }
        $crud->replace_form_add('cruds/orden_trabajo/add','movimientos')
             ->replace_form_edit('cruds/orden_trabajo/edit','movimientos')
             ->set_relation('clientes_id', 'clientes', '{nombres} {apellidos}')
             ->set_relation('user_id','user','{nombre} {apellido}')
             ->display_as('user_id','Vendedor')
             ->display_as('repartidor_id','Repartidor')
             ->field_type('fecha_registro','hidden',date("Y-m-d H:i:s"))
             ->field_type('estado','hidden',0)
             ->field_type('sucursal','hidden',$this->user->sucursal)
             ->field_type('cajadiaria','hidden',$this->user->cajadiaria)
             ->field_type('anulado','hidden',0)
             ->field_type('total_orden','invisible',0)
             ->field_type('facturado','invisible',0)
             ->unset_columns('facturado');
        if($crud->getParameters()=='list'){
        	$crud->set_relation('sucursal', 'sucursales', 'denominacion');
        	$crud->field_type('estado','dropdown',['0'=>'Pendiente','1'=>'Facturado']);
        }
        $crud->callback_column('anulado', function($val, $row) {
            switch ($val) {
                //case '0':return $this->user->allow_function('anular_venta')?'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="fa fa-times-circle"></i></a>':'Activa';
                case '0':return 'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="fa fa-times-circle"></i></a>';
                    break;
                case '1':return 'Anulada';
                    break;
            }
        });
        $crud->order_by('id','DESC');        
        ///Validations
        $crud->set_rules('total_orden','Total de presupuesto','required|numeric|greater_than[0]');        
        $crud->set_rules('productos','Productos','required|callback_unProducto|callback_validar');                        
        $crud->callback_before_insert(array($this,'validarFacturacion'));
        $crud->callback_after_insert(array($this,'addBody'));        
        $crud->callback_after_update(array($this,'updateBody'));
        $crud->callback_after_delete(function($primary){get_instance()->db->delete('orden_trabajo_detalle',array('orden_trabajo_id'=>$primary));});        
        /*$crud->callback_column('estado',function($val,$row){
            return $val=1?'<span class="badge badge-success">SI</span>':'<span class="badge badge-danger">NO</span>';
        });*/
        $crud->unset_back_to_list();
        $crud->add_action('Facturar','',base_url('movimientos/ventas/ventas/add/OrdenTrabajo/').'/');
        if($y=='return'){
            return $crud;
        }
        $crud->where('estado = 0','ESCAPE',TRUE);
        $output = $crud->render();   
        if($crud->getParameters()=='list'){
            $output->output = $this->load->view('orden_trabajo_list',array('output'=>$output->output),TRUE);
        }
        $this->loadView($output);     
    }

    public function ordentrabajodetalle($x = '', $y = '') {         
        $crud = parent::crud_function($x, $y);
        if(!empty($_POST['orden_trabajo_id'])){
            $crud->where('orden_trabajo_id',$_POST['orden_trabajo_id']);
        }
        $crud->callback_column('productos_id',function($val,$row){
            return $this->db->get_where('productos',array('id'=>$val))->row();
        });        
        $crud->unset_add()->unset_edit()->unset_delete();
        $output = $crud->render();        
        $this->loadView($output);     
    }
    //Callback para validar si la venta almacena el id de factura o no
    function validarFacturacion($post){        
        //$post['fecha'] = date("Y-m-d H:i:s");
        $post['estado'] = 0;
        $post['anulado'] = 0;
        $post['sucursal'] = $this->user->sucursal;
        $post['cajadiaria'] = $this->user->cajadiaria;
        return $post;
    }
    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();        
        foreach($productos as $p){
            $pro[] = $p->nombre_comercial;
            $p->producto = @$this->db->get_where('productos',array('codigo'=>$p->codigo))->row();
            $p->por_venta = 0;
            $p->total_adic = ($p->precio_venta*$p->cantidad)+((($p->precio_venta*$p->cantidad)*$p->por_venta)/100);
            $this->db->insert('orden_trabajo_detalle',array(
                'orden_trabajo_id'=>$primary,
				'productos_id'=>$p->producto->id,
				'producto'=>$p->producto->codigo,
				'cantidad'=>$p->cantidad,
				'precio_venta'=>$p->precio_venta,
				'total'=>$p->total_adic,

            ));
        }
        //$this->db->update('presupuesto',array('productos'=>implode($pro)),array('id'=>$primary));  
        $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'orden_trabajo']);             
    }   

    function updateBody($post,$primary){
        $postBody = json_decode($_POST['productos']);
        $beforep = $this->db->get_where('orden_trabajo_detalle',['orden_trabajo_id'=>$primary,'anulado'=>0]);        
        $before = [];
        foreach($beforep->result() as $b){
            $before[] = $b->producto;
        }

        //Anular los que no vinieron en el array
        foreach($beforep->result() as $b){
            $exists = false;
            foreach($postBody as $n=>$p){
                if($p->codigo==$b->producto){
                    $exists = true;
                }
            }
            if(!$exists){
                $this->db->update('orden_trabajo_detalle',['anulado'=>1],['id'=>$b->id]);
            }
        }         
        foreach($postBody as $n=>$p){            
          //INSERT   
          $p->por_venta = 0;
          $p->total_adic = ($p->precio_venta*$p->cantidad)+((($p->precio_venta*$p->cantidad)*$p->por_venta)/100);       
    	  $p->producto = @$this->db->get_where('productos',array('codigo'=>$p->codigo))->row();
          if(!in_array($p->codigo,$before)){ //INSERT              
              $this->db->insert('orden_trabajo_detalle',[
                'orden_trabajo_id'=>$primary,
				'productos_id'=>$p->producto->id,
				'producto'=>$p->producto->codigo,
				'cantidad'=>$p->cantidad,
				'precio_venta'=>$p->precio_venta,
				'total'=>$p->total_adic,
              ]);
          }else{ //Update              
              $this->db->update('orden_trabajo_detalle',[
                'cantidad'=>$p->cantidad,                
                'precio_venta'=>$p->precio_venta,
                'total'=>$p->total_adic,                
              ],['producto'=>$p->codigo,'orden_trabajo_id'=>$primary]);
          }
      }      
    } 

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }

    function validar(){ 
        $productos = $_POST['productos'];       
        $ajustes = $this->ajustes;        
        if($_POST['transaccion']==2 && $_POST['cliente']==1){
            $this->form_validation->set_message('validar','Debe indicar a que cliente desea asignarle esta venta a crédito');        
            return false;
        }
        return true;
    }

    function orden_trabajo_detail($ventaid){
        $this->as['ventas_detail'] = 'orden_trabajo_detalle';
        $crud = $this->crud_function('','');
        $crud->where('v',$ventaid);
        $crud->columns('producto','cantidad','total');
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$row->producto))->roW()->nombre_comercial;
        }); 
        $crud->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_export()
             ->unset_print()
             ->unset_read();
        $crud->staticShowList = true;
        $crud = $crud->render();
        echo $crud->output;

    }

    public function facturadas(){
        $this->as['facturadas'] = 'orden_trabajo';
        $crud = parent::crud_function('','');        
        $crud = $this->orden_trabajo($crud,'return');
        $crud->where('estado',1);
        $crud->set_url('movimientos/OrdenTrabajo/facturadas/');
        $output = $crud->render();
        echo $output->output;
    }

    function updateTemp(){
        $productos = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'orden_trabajo']);
        if($productos->num_rows()==0){
            $this->db->insert('ventatemp',['productos'=>$_POST['productos'],'cajas_id'=>$this->user->caja,'user_id'=>$this->user->id,'tipo'=>'orden_trabajo']);
        }else{
            $this->db->update('ventatemp',['productos'=>$_POST['productos'],'user_id'=>$this->user->id],['cajas_id'=>$this->user->caja,'tipo'=>'orden_trabajo']);
        }
        echo 'success';
    }

    function getTemp(){
        $productos = [];
        $pp = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'orden_trabajo']);
        if($pp->num_rows()>0){
            $productos = json_decode($pp->row()->productos);
        }
        echo json_encode($productos);
    }


}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
