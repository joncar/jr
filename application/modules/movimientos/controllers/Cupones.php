<?php

require_once APPPATH.'/controllers/Panel.php';    

class Cupones extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }
        
     function cupones($action = ''){
     	if($action == 'asignar'){
     		$this->asignar();
     		return;
     	}
     	$crud = parent::crud_function('','');     	
     	$output = $crud->render();      	
     	$output->output = $this->load->view('cupones',['output'=>$output->output],TRUE);
        $this->loadView($output);
     }

     function asignar(){
     	$this->load->view('cupones',['asignar'=>1]);
     }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
