<?php

require_once APPPATH.'/controllers/Panel.php';    

class Ventas_mantenimiento extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }
    
    public function ventas_mantenimiento($x = '', $y = '', $z = '') { 
        if($x=='upd' && !empty($_POST) && is_numeric($_POST['id'])){
        	$this->db->update('ventas',['nro_factura'=>$_POST['nro'],'tipo_facturacion_id'=>$_POST['fac']],['id'=>$_POST['id']]);
        	echo 'success';
        	return;
        }

        if($x=='correlativo' && !empty($_POST)){
        	if(is_numeric($_POST['correlativo'])){
	        	$this->db->update('cajadiaria',['correlativo'=>$_POST['correlativo']],['id'=>$this->user->cajadiaria]);
	        	echo $this->success('Correlativo actualizado con éxito');
        	}else{
        		echo $this->error('El correlativo debe ser un valor numérico');
        	}
        	return;
        }

        
        $this->as['ventas_mantenimiento'] = 'ventas';
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap');
        $crud->columns('id','transaccion', 'cliente', 'fecha', 'caja', 'ventas.nro_factura', 'tipo_facturacion_id','total_venta', 'status');
        $crud->callback_column('ventas.nro_factura', function($val, $row) {
            return '<input type="text" class="form-control nro" style="width: 80%;display: inline-block;" data-val="'.$row->id.'" value="'.$row->nro_factura.'"> <a href="javascript:showDetail('.$row->id.')"><i class="fa fa-exclamation"></i></a>';
        });
        $crud->callback_column('s11e337cc', function($val, $row) {
            return form_dropdown_from_query('tipo_facturacion_id','tipo_facturacion','id','denominacion',$row->tipo_facturacion_id);
        });
        $crud->callback_column('observacion',function($val,$row){
            return 'a';
        });
        $crud->callback_column('total_venta',function($val){
            return round($val,0);
        });
        $crud->callback_column('status', function($val, $row) {
            switch ($val) {
                case '0':return 'Activa';
                    break;
                case '-1':return 'Anulada';
                    break;
                case '-2': return 'Anulada y procesada';
                    break;
            }
        });
        $crud->unset_edit()
        	 ->unset_add()
        	 ->unset_delete()
        	 ->unset_print()
        	 ->unset_export()
        	 ->unset_read();
        $crud->set_relation('transaccion', 'tipotransaccion', 'denominacion')
             ->set_relation('cliente', 'clientes', '{nombres} {apellidos}')
             ->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->where('ventas.sucursal', $_SESSION['sucursal']);
        if (!empty($_SESSION['caja'])){
            $crud->where('ventas.caja', $_SESSION['caja']);
        }
        $crud->order_by('id','DESC');
        $output = $crud->render();
		$output->output = $this->load->view('ventas_mantenimiento',array('output'=>$output->output),TRUE);
        $this->loadView($output);     
    }

    public function ventadetalle($x = '', $y = '') {         
        $crud = parent::crud_function($x, $y);
        if(!empty($_POST['venta'])){
            $crud->where('venta',$_POST['venta']);
        }
        $crud->callback_column('producto',function($val,$row){
            $producto = $this->db->get_where('productos',array('codigo'=>$val))->row();            
            $producto->remitidos = get_instance()->db->query("SELECT IFNULL(SUM(cantidad),0) as total FROM nota_remision_detalle INNER JOIN nota_remision ON nota_remision.id = nota_remision_detalle.nota_remision_id WHERE nota_remision.venta = '{$row->venta}' AND producto = '$val'")->row()->total;
            $producto->disponibles = $row->cantidad - $producto->remitidos;
            return $producto;
        });        
        $crud->unset_add()->unset_edit()->unset_delete();
        $output = $crud->render();        
        $this->loadView($output);     
    }
    

    function ventas_detail($ventaid){
        $this->as['ventas_detail'] = 'ventadetalle';
        $crud = $this->crud_function('','');
        $crud->where('venta',$ventaid);
        $crud->columns('producto','cantidad','totalcondesc');
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$row->producto))->roW()->nombre_comercial;
        }); 
        $crud->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_export()
             ->unset_print()
             ->unset_read();
        $crud->staticShowList = true;
        $crud = $crud->render();
        echo $crud->output;

    }
    
    
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
