<?php

require_once APPPATH.'/controllers/Panel.php';    

class Compras extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }                
    }

    function anular_compra(){
        $this->form_validation->set_rules('id','ID','required|integer');
        if($this->form_validation->run()){            
            $this->db->update('compras',array('status'=>-1),array('id'=>$this->input->post('id')));                
            echo $this->success('factura anulada con exito').refresh_list();
        }
        else echo $this->error('Ha ocurrido un error');
    }
    
    public function compras($x = '',$y = '',$z = '') {
        if($x=='anular'){
            $this->anular_compra();
            die();
        }
        if($x=='updateTemp' && !empty($_POST['productos'])){
            $this->updateTemp();
            return;
        }

        if($x=='getTemp'){
            $this->getTemp();
            return;
        }

        if($x=='cleanTemp'){
            $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'compra']);       
            return;
        }
        $this->norequireds = ['user_id'];
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        if(!empty($_POST['compras_id'])){            
            if(is_numeric($_POST['compras_id'])){
                $crud->where('compras.id = '.$_POST['compras_id'],'ESCAPE',FALSE);
            }else{
                $crud->where('compras.id',-1);
            }
        }
        $crud->field_type('transaccion', 'dropdown', array('0' => 'ASD'));
        $crud->callback_column('status', function($val, $row) {
            switch ($val) {
                case '1':return 'Pagada <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>';
                    break;
                case '0':return $this->user->admin==1?'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="fa fa-times-circle"></i></a>':'Activa';
                    break;
                case '-1':return 'Anulada';
                    break;
                case '-2': return 'Anulada y procesada';
                    break;
            }
        });
        $crud->set_relation('proveedor','proveedores','denominacion',['anulado'=>'0']);
        $crud->callback_column('nro_factura', function($val, $row) {
            return '<a href="javascript:showDetail('.$row->id.')">'.$val.'</a>';
        });

        $crud->set_rules('total_compra','Total de compra','required|numeric|greater_than[0]');        
        $crud->set_rules('productos','Productos','required|callback_unProducto');                
        /*if($crud->getParameters()=='add'){
            $crud->set_rules('nro_factura','Numero de factura','required|is_unique[compras.nro_factura]');
        }else{
            $crud->set_rules('nro_factura','Numero de factura','required');
        }*/
        $crud->callback_before_insert(array($this,'addUser'));
        $crud->callback_after_insert(array($this,'addBody'));
        $crud->callback_before_update(function($post){            
            echo '<textarea>{"success":false,"message":"La edición no está permitida"}</textarea>';
            die();
        });
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->order_by('compras.id','DESC');
        $crud->columns('compras.id','nro_factura','timbrado','sucursal','fecha','tipo_facturacion_id','proveedor','total_compra','total_retencion','fecha_registro','status');
        $crud->display_as('compras.id','#Compra')
             ->callback_column('compras.id',function($val,$row){return $row->id;});
        $output = $crud->render();
        $output->crud = 'compras';
        if ($crud->getParameters()!='list'){
            $edit = '';
            if($crud->getParameters()=='edit'){
                $edit = $y;  
                $output->output = $this->load->view('compras-edit',array('output'=>$output->output,'edit'=>$edit),TRUE);              
            }else{
                $default = '';
                if(is_numeric($z) && is_string($y)){
                    $default = $this->querys->{'get'.$y}($z);                            
                }
                $output->output = $this->load->view('compras_bew',array('output'=>$output->output,'edit'=>$edit,'default'=>$default),TRUE);
            }
        }else{
            $output->output = $this->load->view('compras_list',array('output'=>$output->output),TRUE);
        }

        $this->loadView($output);
    }

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        if($this->ajustes->controlar_vencimiento_stock==1){
            $productos = json_decode($_POST['productos']);
            foreach($productos as $p){
                if(empty($p->vencimiento)){
                    $this->form_validation->set_message('unProducto','El producto '.$p->codigo.' no posee un vencimiento válido');        
                    return false;
                }
            }
        }
        //Validar si el pago es completo
    }

    function addUser($post){
        $post['fecha_registro'] = date("Y-m-d H:i:s");
        $post['user_id'] = $this->user->id;
        return $post;
    }

    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        //$this->db->delete('ventadetalle',array('venta'=>$primary));
        $total = 0;
        $total_iva = 0;  
        $total_retencion = 0;     
        foreach($productos as $p){
            $pro[] = $p->nombre_comercial; 
            $producto = $this->db->get_where('productos',['codigo'=>$p->codigo]);
            $precio_ant = $producto->num_rows()>0?$producto->row()->precio_venta:0;
            $precio_costo_ant = $producto->num_rows()>0?$producto->row()->precio_costo:0;
            $venc = empty($p->vencimiento)?'00/0000':$p->vencimiento;
            $venc = explode('/',$venc);
            $venc = $venc[1].'-'.$venc[0].'-01';

            $p->desc_compra = isset($p->desc_compra)?$p->desc_compra:0;
            $porc_mayorista1 = (($p->precio_venta_mayorista1/$p->precio_costo)-1)*100;
            $porc_mayorista2 = (($p->precio_venta_mayorista2/$p->precio_costo)-1)*100;
            $porc_mayorista3 = (($p->precio_venta_mayorista3/$p->precio_costo)-1)*100;

            $this->db->insert('compradetalles',array(
                'compra'=>$primary,
                'producto'=>$p->codigo,                
                'cantidad'=>$p->cantidad,
                'lote'=>'',
                'vencimiento'=>$venc,
                'precio_costo'=>$p->precio_costo,
                'precio_costo_ant'=>$precio_costo_ant,
                'desc_compra'=>$p->desc_compra,
                'por_desc'=>$p->por_desc,
                'por_venta'=>$p->por_venta,
                'precio_venta'=>$p->precio_venta,
                'precio_venta_ant'=>$precio_ant,
                'total'=>$p->total,
                'precio_credito'=>isset($p->precio_credito)?$p->precio_credito:0,
                'sucursal'=>$p->sucursal,
                'precio_venta_mayorista1'=>$p->precio_venta_mayorista1,
                'precio_venta_mayorista2'=>$p->precio_venta_mayorista2,
                'precio_venta_mayorista3'=>$p->precio_venta_mayorista3,
                'porc_mayorista1'=>$porc_mayorista1,
                'porc_mayorista2'=>$porc_mayorista2,
                'porc_mayorista3'=>$porc_mayorista3,
                'stock_anterior'=>@$this->db->get_where('productosucursal',['sucursal'=>$p->sucursal,'productos_id'=>$p->id])->row()->stock
            ));
            
            $datos = [
                'porc_venta'=>$p->por_venta,
                'precio_venta'=>$p->precio_venta,
                'precio_costo'=>$p->precio_costo,
                'precio_credito'=>isset($p->precio_credito)?$p->precio_credito:0,
                'precio_venta_mayorista1'=>$p->precio_venta_mayorista1,
                'precio_venta_mayorista2'=>$p->precio_venta_mayorista2,
                'precio_venta_mayorista3'=>$p->precio_venta_mayorista3,
                'porc_mayorista1'=>$porc_mayorista1,
                'porc_mayorista2'=>$porc_mayorista2,
                'porc_mayorista3'=>$porc_mayorista3
            ];
            if(isset($this->ajustes->actualizar_proveedor) && $this->ajustes->actualizar_proveedor==1){
                $datos['proveedor_id']=$post['proveedor'];
            }
            $this->db->update('productos',$datos,['codigo'=>$p->codigo]);
            if($this->ajustes->precioxsucursal==1){
                $this->db->update('productosucursal',[
                    'precio_venta'=>$datos['precio_venta'],
                    'precio_costo'=>$datos['precio_costo'],
                    'precio_venta_mayorista1'=>$datos['precio_venta_mayorista1'],
                    'precio_venta_mayorista2'=>$datos['precio_venta_mayorista2'],
                    'precio_venta_mayorista3'=>$datos['precio_venta_mayorista3'],
                    'precio_credito'=>$datos['precio_credito'],
                    'porc_mayorista1'=>$porc_mayorista1,
                    'porc_mayorista2'=>$porc_mayorista2,
                    'porc_mayorista3'=>$porc_mayorista3
                ],['sucursal'=>$this->user->sucursal,'producto'=>$p->codigo]);
            }

            if($producto->row()->iva_id>0){
                $iva = $producto->row()->iva_id==10?1.1:1.05;
                $total_iva+= $p->total - ($p->total/$iva);                
            }
            $total+= $p->total;
        }
        
        $proveedor = $this->db->get_where('proveedores',['id'=>$post['proveedor']])->row();        
        if($proveedor->retencion==1 && $total>100000){
            $total_retencion = ($total_iva * 30)/100;        
        }
        $total_a_pagar = $total - $total_retencion;

        $this->db->update('compras',array(
            'productos'=>implode($pro),
            'total_compra'=>$total,
            'total_iva'=>$total_iva,
            'total_retencion'=>$total_retencion,
            'total_a_pagar'=>$total_a_pagar
        ),array('id'=>$primary));        
        if($post['pago_caja_diaria']==1){
            $this->db->insert('gastos',array(
                 'cajadiaria'=>$this->user->cajadiaria,
                 'beneficiario'=>@$this->db->get_where('proveedores',array('id'=>$post['proveedor']))->row()->denominacion,
                 'concepto'=>'Compra #'.$primary,
                 'cuentas_id'=>1,
                 'fecha_gasto'=>date("Y-m-d"),                 
                 'monto'=>$total,
                 'nro_documento'=>$post['nro_factura'],
                 'user_created'=>$this->user->id, 
                 'user_modified'=>$this->user->id
            ));

            $this->db->insert('pagoproveedores',array(
                'fecha'=>date("Y-m-d H:i:s"),
                'forma_pago'=>'Efectivo',
                'comprobante'=>'Compra #'.$primary,
                'nro_recibo'=>$post['nro_factura'],
                'proveedor'=>$post['proveedor'],
                'total_abonado'=>$total,
                'totalnotacredito'=>0,
                'totalefectivo'=>$total,
                'anulado'=>0,
                'sucursal'=>$this->user->sucursal,
                'caja'=>$this->user->caja,
                'user'=>$this->user->id,
                'cajadiaria'=>$this->user->cajadiaria
            ));
            $pago = $this->db->insert_id();
            $this->db->insert('pagoproveedor_detalles',array(
                'pagoproveedor'=>$pago,
                'nro_factura'=>$post['nro_factura'],
                'compra'=>$primary,
                'fecha_factura'=>date('Y-m-d'),
                'monto'=>$total,
                'nota_credito'=>0,
                'total'=>$total
            ));
        }

        if($post['pago_caja_diaria']==2){//Caja Grande
            $this->db->insert('ajuste_efectivo',array(
                 'sucursales_id'=>$this->user->sucursal,
                 'cajas_id'=>$this->user->caja,
                 'cajadiaria_id'=>$this->user->cajadiaria,
                 'tipo_ajuste'=>-1,
                 'cuentas_id'=>0,
                 'fecha'=>date("Y-m-d H:i:s"),
                 'observacion'=>'Cargo en compras',
                 'monto'=>$total,
                 'comprobante'=>$post['nro_factura'],
                 'anulado'=>0,
                 'user_id'=>$this->user->id
            ));
        }
        $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'compra']);             
    }

    function redondeo($valor){
        if($valor<100){
            return $valor;
        }               
        $centena = substr($valor,-2);
        if($centena<50){
            $valor-= $centena;
        }else if($centena>50){
            $valor+= 100-$centena;
        }
        return abs($valor);
    }

    function compras_detail($ventaid){
        $this->as['compras_detail'] = 'compradetalles';
        $crud = $this->crud_function('','');
        $crud->where('compra',$ventaid);
        $crud->columns('producto','cantidad','precio_compra','precio_venta','total');
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$row->producto))->row()->nombre_comercial;
        }); 
        $crud->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_export()
             ->unset_print()
             ->unset_read();        
        $crud = $crud->render();
        echo $crud->output;
    }

    function compradetalles(){        
        $crud = $this->crud_function('','');                
        $crud->unset_add()             
             ->unset_delete()
             ->unset_export()             
             ->unset_read()
             ->edit_fields('precio_costo','cantidad','total')
             ->set_primary_key('codigo','productos')
             ->set_relation('compra','compras','{nro_factura}|{proveedor}|{fecha}')
             ->set_relation('producto','productos','{codigo}|{nombre_comercial}|{iva_id}')
             ->set_relation('j570f8e40.proveedor','proveedores','denominacion')
             ->columns('j570f8e40.nro_factura','j570f8e40.proveedor','j570f8e40.fecha','j286e18ee.codigo','j286e18ee.nombre_comercial','precio_costo','cantidad','total','j286e18ee.iva_id')
             ->callback_column('j570f8e40.nro_factura',function($val,$row){return explode('|',$row->s570f8e40)[0];})
             ->callback_column('j570f8e40.proveedor',function($val,$row){return explode('|',$row->s570f8e40)[1];})
             ->callback_column('j570f8e40.fecha',function($val,$row){return explode('|',$row->s570f8e40)[2];})
             ->callback_column('j286e18ee.codigo',function($val,$row){return explode('|',$row->s286e18ee)[0];})
             ->callback_column('j286e18ee.nombre_comercial',function($val,$row){return explode('|',$row->s286e18ee)[1];})
             ->callback_column('j286e18ee.iva_id',function($val,$row){return explode('|',$row->s286e18ee)[2];})
             ->display_as('j570f8e40.nro_factura','#Factura')
             ->display_as('j570f8e40.proveedor','Proveedor')
             ->display_as('j570f8e40.fecha','Fecha')
             ->display_as('j286e18ee.codigo','Código')
             ->display_as('j286e18ee.nombre_comercial','Producto')
             ->display_as('j286e18ee.iva_id','IVA');
        if(!empty($_POST['compra'])){
            $crud->where('compra',$_POST['compra']);
        }
        $crud = $crud->render();
        $crud->output = $this->load->view('compradetalles',array('output'=>$crud->output),TRUE);
        $this->loadView($crud);

    }

    public function compradetalle($x = '', $y = '') {
        $this->as['compradetalle'] = 'compradetalles';
        $crud = parent::crud_function($x, $y);
        if(!empty($_POST['compra'])){
            $crud->where('compra',$_POST['compra']);
        }
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$val))->row();
        });        
        $crud->unset_add()->unset_edit()->unset_delete();
        $output = $crud->render();        
        $this->loadView($output);     
    }
    function updateTemp(){
        $productos = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'compra']);
        if($productos->num_rows()==0){
            $this->db->insert('ventatemp',['productos'=>$_POST['productos'],'cajas_id'=>$this->user->caja,'user_id'=>$this->user->id,'tipo'=>'compra']);
        }else{
            $this->db->update('ventatemp',['productos'=>$_POST['productos'],'user_id'=>$this->user->id],['cajas_id'=>$this->user->caja,'tipo'=>'compra']);
        }
        echo 'success';
    }

    function getTemp(){
        $productos = [];
        $pp = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'compra']);
        if($pp->num_rows()>0){
            $productos = json_decode($pp->row()->productos);
        }
        echo json_encode($productos);
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
