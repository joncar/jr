<?php

require_once APPPATH.'/controllers/Panel.php';    

class Notas_credito extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }

    public function notas_credito_proveedor($x = '', $y = '') {
        if($x=='anular' && is_numeric($y)){
            //$this->db->update('notas_credito_cliente',array('anulado'=>1),array('id'=>$y));
            $this->anular_nota($y);
            //redirect('movimientos/ventas/notas_credito_cliente/success');
            echo $this->success('Anulado realizado con éxito');
            die();
        }
        $crud = parent::crud_function($x, $y);
        $crud->set_subject('notas de crédito');
        $crud->set_relation('compra','compras','nro_factura');
        $crud->unset_delete();
        $crud->set_rules('productos','Productos','required|callback_unProducto|callback_notaCreditoDuplicar');
        if($crud->getParameters()=='add'){
            //$crud->set_rules('nro_nota_credito','#Nota','required|is_unique[notas_credito_proveedor.nro_nota_credito]');
        }
        if($crud->getParameters()=='edit'){
            $crud->unset_fields('productos');
        }
        $crud->columns('id','nro_nota_credito','fecha','proveedor','total_monto','actualizar_stock','productos','anulado');
        $crud->set_relation('proveedor','proveedores','denominacion');
        $crud->callback_column('anulado', function($val, $row) {
            switch ($val) {
                case '0':return $this->user->admin==1?'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="fa fa-times-circle"></i></a>':'Activa';
                    break;
                case '1':return 'Anulada';
                    break;
            }
        });
        $crud->callback_after_insert(function($post,$primary){
            $this->db = get_instance()->db;
            $productos = json_decode($_POST['productos']);
            $pro = array();            
            foreach($productos as $p){
                $pro[] = $p->producto->nombre_comercial;
                $this->db->insert('notas_credito_proveedor_detalle',array(                    
                    'nota_credito'=>$primary,
                    'producto'=>$p->producto->id,
                    'lote'=>'',
                    'cantidad'=>$p->cantidad,
                    'precio_costo'=>$p->precio_costo,
                    'total'=>$p->precio_costo * $p->cantidad
                ));
            }
            $this->db->update('notas_credito_proveedor',array('productos'=>implode($pro)),array('id'=>$primary));            
            
            //Hay que actualizar inventario?
            if($post['actualizar_stock']==1 && $post['motivo_nota_id']==3){
                $this->db->insert('entrada_productos',array(
                     'fecha'=>date("Y-m-d"),
                     'motivo'=>3,
                     'proveedor'=>'',
                     'total_monto'=>$post['total_monto'],
                     'usuario'=>get_instance()->user->id,
                     'cajadiaria'=>$_SESSION['cajadiaria'],
                     'observacion'=>'Devolución cargada desde el módulo notas de crédito #'.$post['nro_nota_credito']
                ));
                $entrada = $this->db->insert_id();
                foreach($productos as $p){
                     $this->db->insert('entrada_productos_detalles',array(
                        'entrada_producto'=>$entrada,
                        'producto'=>$p->producto->codigo,
                        'lote'=>'',
                        'vencimiento'=>'',
                        'cantidad'=>$p->cantidad,
                        'precio_costo'=>$p->producto->precio_costo,
                        'precio_venta'=>$p->producto->precio_venta,
                        'total'=>$p->producto->precio_costo * $p->cantidad,
                        'sucursal'=>$_SESSION['sucursal']
                    ));
                }
            }
            //Hay que actualizar inventario?
            if($post['actualizar_stock']==1 && ($post['motivo_nota_id']==1 || $post['motivo_nota_id']==4)){
                $this->db->insert('salidas',array(
                    'proveedor'=>'',
                    'fecha'=>date("Y-m-d"),
                    'motivo'=>3,
                    'usuario'=>get_instance()->user->id,
                    'total_costo'=>$post['total_monto'],
                    'caja'=>$_SESSION['caja'],
                    'cajadiaria'=>$_SESSION['cajadiaria'],
                    'productos'=>'',
                ));
                $entrada = $this->db->insert_id();
                foreach($productos as $p){
                     $this->db->insert('salida_detalle',array(
                        'salida'=>$entrada,
                        'producto'=>$p->producto->codigo,
                        'lote'=>'',
                        'vencimiento'=>'',
                        'cantidad'=>$p->cantidad,
                        'precio_costo'=>$p->producto->precio_costo,
                        'total'=>$p->producto->precio_costo * $p->cantidad,
                        'sucursal'=>$_SESSION['sucursal']
                    ));
                }
            }
        });
        //$crud->add_action('Anular','',base_url('movimientos/notas_credito/notas_credito_proveedor/anular').'/');
        $output = $crud->render();        
        if ($crud->getParameters()!='list'){
            $edit = '';
            if($crud->getParameters()=='edit'){
                $edit = $y;                
            }
            $output->output = $this->load->view('notas_credito_proveedor',array('output'=>$output->output,'edit'=>$edit),TRUE);            
        }else{
            $output->output.= $this->load->view('nota_credito_proveedor_list',array('output'=>$output->output),TRUE);            
        }
        $output->title = 'Notas de crédito a proveedor';
        $this->loadView($output);
    }

    function anular_nota($id){
        $nota = $this->db->get_where('notas_credito_proveedor',['id'=>$id]);
        if($nota->num_rows()>0){
            $nota = $nota->row();
            $this->db->update('notas_credito_proveedor',array('anulado'=>1),array('id'=>$id));
            //Hay que actualizar inventario?
            if($nota->actualizar_stock==1 && $nota->motivo_nota_id==3){
                $this->db->insert('salidas',array(
                    'proveedor'=>'',
                    'fecha'=>date("Y-m-d"),
                    'motivo'=>3,
                    'usuario'=>get_instance()->user->id,
                    'total_costo'=>'',
                    'caja'=>'',
                    'cajadiaria'=>$_SESSION['cajadiaria'],
                    'productos'=>''
                ));
                $salida = $this->db->insert_id();
                $this->db->select('notas_credito_proveedor_detalle.*,productos.codigo,productos.precio_costo');
                $this->db->join('productos','productos.id = notas_credito_proveedor_detalle.producto');
                $productos = $this->db->get_where('notas_credito_proveedor_detalle',['nota_credito'=>$id]);
                foreach($productos->result() as $p){
                     $this->db->insert('salida_detalle',array(
                        'salida'=>$salida,
                        'producto'=>$p->codigo,
                        'lote'=>'',
                        'vencimiento'=>'',
                        'cantidad'=>$p->cantidad,
                        'precio_costo'=>$p->precio_costo,
                        'total'=>$p->total,
                        'sucursal'=>$_SESSION['sucursal']
                    ));
                }
            }

            //Hay que actualizar inventario?
            if($nota->actualizar_stock==1 && ($nota->motivo_nota_id==1 || $nota->motivo_nota_id==4)){
                $this->db->insert('entrada_productos',array(
                    'proveedor'=>'',
                    'fecha'=>date("Y-m-d"),
                    'motivo'=>3,
                    'usuario'=>get_instance()->user->id,
                    'total_monto'=>'',
                    'cajadiaria'=>$_SESSION['cajadiaria'],
                    'productos'=>''
                ));
                $salida = $this->db->insert_id();
                $this->db->select('notas_credito_proveedor_detalle.*,productos.codigo,productos.precio_costo,productos.precio_venta');
                $this->db->join('productos','productos.id = notas_credito_proveedor_detalle.producto');
                $productos = $this->db->get_where('notas_credito_proveedor_detalle',['nota_credito'=>$id]);
                foreach($productos->result() as $p){
                     $this->db->insert('entrada_productos_detalles',array(
                        'entrada_producto'=>$salida,
                        'producto'=>$p->codigo,
                        'lote'=>'',
                        'vencimiento'=>'',
                        'cantidad'=>$p->cantidad,
                        'precio_costo'=>$p->precio_costo,
                        'precio_venta'=>$p->precio_venta,
                        'total'=>$p->total,
                        'sucursal'=>$_SESSION['sucursal']
                    ));
                }
            }
        }
    }

    function notaCreditoDuplicar(){
        $existe = $this->db->get_where('notas_credito_proveedor',array('compra'=>$_POST['compra'],'anulado'=>0));
        if($existe->num_rows()>0){
            $this->form_validation->set_message('notaCreditoDuplicar','Nota de crédito ya ha sido generada anteriormente');
            return false;
        }
        return true;
    }

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
