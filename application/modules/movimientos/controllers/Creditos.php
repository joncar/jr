<?php
	require_once APPPATH.'/controllers/Panel.php';
	class Creditos extends Panel {
		function __construct(){
			parent::__construct();
		}

		function Creditos($venta = '',$return = false,$cliente = null){
			if(empty($this->user->cajadiaria)){
				redirect('panel/selcajadiaria');
			}
			$vent = $this->db->get_where('ventas',array('id'=>$venta));
			if($vent->num_rows()>0){
				if($vent->row()->transaccion==1){
					throw new Exception("La venta seleccionada no es una venta a crédito", 503);
				}
				$crud = $this->crud_function('','');
				$crud->set_theme('bootstrap2_horizontal');
				$crud->field_type('ventas_id','hidden',$venta)
					 ->unset_columns('ventas_id')
					 ->field_type('cajadiaria','hidden',$this->user->cajadiaria)
					 ->field_type('user_id','hidden',$this->user->id)
					 ->unset_columns('cajadiaria')
					 ->field_type('forma_pago_id','dropdown',array('7'=>'Semanal','30'=>'Mensual'))
					 ->where('ventas_id',$venta);
				if($crud->getParameters()=='add'){
					$crud->set_rules('ventas_id','Nro Factura','required|is_unique[creditos.ventas_id]');
				}
				$crud->callback_before_insert(function($post){
					$post['total_credito'] = str_replace(['.',','],['','.'],$post['total_credito']);
					$post['cant_cuota'] = str_replace(['.',','],['','.'],$post['cant_cuota']);
					$post['monto_cuota'] = str_replace(['.',','],['','.'],$post['monto_cuota']);
					$post['entrega_inicial'] = str_replace(['.',','],['','.'],$post['entrega_inicial']);
					$post['monto_credito'] = str_replace(['.',','],['','.'],$post['monto_credito']);
					return $post;
				});
				$crud->callback_after_insert(function($post,$primary){

					$cuotas = $post['cant_cuota'];
					$dias = $post['forma_pago_id'];
					$post['monto_cuota'] = str_replace(['.',','],['','.'],$post['monto_cuota']);
					$post['total_interes'] = str_replace(['.',','],['','.'],$post['total_interes']);
					for($i = 0; $i< $cuotas; $i++){
						if($dias==30){
							$fecha = ' +'.$i.' month';
						}else{
							$fecha = ' +'.$i.' week';
						}

						if($post['interes']==0){
							$capital = $post['monto_cuota'];
							$interes = 0;
						}else{
							$capital = $post['monto_credito']/$cuotas;
							$interes = $post['total_interes']/$cuotas;
						}
						$data = array(
							'creditos_id'=>$primary,
							'nro_cuota'=>($i+1),
							'fecha_a_pagar'=>date("Y-m-d",strtotime(str_replace('/','-',$post['fecha_inicio_plan']).$fecha)),
							'capital'=> $capital,
							'interes'=>$interes,
							'monto_a_pagar'=>$post['monto_cuota'],
							'pagado'=>0
						);			
						$this->db->insert('plan_credito',$data);						
					}
					//Generar factura electronica
					$venta = $this->db->get_where('ventas',['id'=>$post['ventas_id']])->row();
					$tipoFacturacion = $this->db->get_where('tipo_facturacion',array('id'=>$venta->tipo_facturacion_id));
        			if($tipoFacturacion->num_rows()>0 && $tipoFacturacion->row()->emite_factura==1){
						$this->load->library('Kuatia');
						$this->kuatia->generarFactura($post['ventas_id']);
					}
				});
				$crud->add_action('Plan de creditos','',base_url('movimientos/creditos/plan_credito').'/');
				$crud->set_lang_string('insert_success_message','Datos almacenados con éxito <a href="javascript:printPlan({id})">Imprimir plan</a> | <a href="javascript:printPagare({id})">Imprimir pagare</a> | <a href="'.base_url().'movimientos/creditos/plan_credito/{id}">Ir a plan de crédito</a> | ');
				$crud->add_action('Imprimir plan','',base_url('reportes/rep/verReportes/91/html/creditos_id').'/');
				$accion = $crud->getParameters();
				if($accion=='add'){
					$crud->field_type('anulado','hidden',0);
				}
				$crud = $crud->render();
				if($accion=='add'){
					$crud->output = $this->load->view('creditos',array('output'=>$crud->output,'venta'=>$vent->row()),TRUE);
				}

				if(is_numeric($venta)){
					$crud->header = new ajax_grocery_crud();
					$crud->header->set_table('ventas')->set_subject('Venta')->set_theme('header_data')->where('ventas.id',$venta)->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read()->set_url('movimientos/ventas/ventas/edit/'.$venta);
					$crud->header->columns('cliente','fecha','total_venta','productos')->set_relation('cliente','clientes','{nombres} {apellidos}');
					$crud->header = $crud->header->render(1)->output;
				}

				$this->loadView($crud);
			}else{
				$crud = $this->crud_function('','');
				$crud->add_action('Plan de creditos','',base_url('movimientos/creditos/plan_credito').'/');
				$encoded = $crud->encodeFieldName('ventas_id','s');
				get_instance()->ajustes = $this->ajustes;
				$crud->unset_add()
					 ->unset_edit()
					 ->unset_delete()
					 ->unset_print()
					 ->unset_export()
					 ->where('(j5122c2ec.status = 0 OR j5122c2ec.status is NULL)','ESCAPE')
					 ->set_relation('ventas_id','ventas','nro_factura')
					 ->set_relation('j5122c2ec.cliente','clientes','{nro_documento}|{nombres}|{apellidos}|{celular}')
					 ->callback_column('s5122c2ec',function($val,$row){
				 		return '<a href="javascript:showDetail('.$row->ventas_id.')">'.$val.'</a>';
					 })
					 ->unset_read();
				$crud->display_as('ventas_id','Nro Factura')
					 ->display_as('s51a61bd2','Cliente')
					 ->display_as('j51a61bd2.nro_documento','#Doc')
					 ->display_as('j51a61bd2.nombres','Nombres')
					 ->display_as('j51a61bd2.apellidos','Apellidos')
					 ->display_as('j51a61bd2.celular','Celular');
				$crud->columns(
					'id',
					'ventas_id',					
					'j51a61bd2.nro_documento',
					'j51a61bd2.nombres',
					'j51a61bd2.apellidos',
					'j51a61bd2.celular',
					'fecha_credito',
					'monto_venta',
					'entrega_inicial',
					'monto_credito',
					'interes',
					'total_credito',
					'monto_cuota',
					'total_pagado',
					'total_mora',
					'saldo',
					'cuotas',
					'anulado'
				);
				$crud->callback_column('total_pagado',function($val,$row){
					return (string)$this->db->query("select IF(SUM(total_pagado) IS NULL,0,SUM(total_pagado)) as total from pagocliente where (pagocliente.anulado = 0 or pagocliente.anulado is null) AND creditos_id = '".$row->id."'")->row()->total;
				})->callback_column('total_mora',function($val,$row){
					$query = $this->db->get_where("plan_credito",array('creditos_id'=>$row->id));
					$total = 0;
					$ajustes = get_instance()->ajustes;
					foreach($query->result() as $q){
						$dias = strtotime(date("Y-m-d"))-strtotime(str_replace('/','-',$q->fecha_a_pagar));
				 		$dias = $dias/60/60/24;
				 		if($q->pagado==0 && $dias>$ajustes->cant_dias_mora){
					 		$dias = $dias-$ajustes->cant_dias_mora;
					 		if(!isset($this->ajustes->acumular_interes) || $this->ajustes->acumular_interes == 1){
					 			$total += $dias * ($q->monto_a_pagar * $ajustes->porc_interes_mora/100);
					 		}else{
					 			$total += ($q->monto_a_pagar * $ajustes->porc_interes_mora/100);
					 		}
					 	}
					}
					//Se suman los ya pagados
					$total+= $this->db->query("SELECT IF(SUM(total_mora) IS NULL,0,SUM(total_mora)) as total FROM plan_credito INNER JOIN pagocliente ON pagocliente.plan_credito_id = plan_credito.id WHERE plan_credito.pagado = 1 AND plan_credito.creditos_id = ".$row->id." AND (pagocliente.anulado = 0 or pagocliente.anulado is null)")->row()->total;
					return (string)$total;
				})
				->callback_column('saldo',function($val,$row){
					return (string)(($row->total_credito+$row->total_mora) - $row->total_pagado);
				})
				->callback_column('j51a61bd2.nro_documento',function($val,$row){
					$row->cl = $row->s51a61bd2;
					return explode('|',$row->s51a61bd2)[0];
				})
				->callback_column('j51a61bd2.nombres',function($val,$row){
					$row->cl = $row->s51a61bd2;
					return explode('|',$row->s51a61bd2)[1];
				})
				->callback_column('j51a61bd2.apellidos',function($val,$row){
					$row->cl = $row->s51a61bd2;
					return explode('|',$row->s51a61bd2)[2];
				})
				->callback_column('j51a61bd2.celular',function($val,$row){
					$row->cl = $row->s51a61bd2;
					return explode('|',$row->s51a61bd2)[3];
				})
				->callback_column('cuotas',function($val,$row){
					return $this->db->query("
						SELECT IF(pagadas!=cuotas,CONCAT(pagadas,'/',cuotas),'<span class=\"label label-danger\">CANCELADO</span>') as res FROM (
						SELECT 
						COUNT(id) as cuotas,
						(SELECT COUNT(id) from plan_credito WHERE creditos_id = $row->id AND pagado = 1)as pagadas
						FROM plan_credito 
						WHERE creditos_id = $row->id) as cd"
					)->row()->res;
				})
				->callback_column('anulado',function($val,$row){
					return $val==1?'<span class="label label-danger">SI</span>':'<span class="label label-success">NO</span>';
				});
				if($cliente){
					$crud->where('j51a61bd2.id',$cliente);
				}
				$crud->set_lang_string('insert_success_message','Datos almacenados con éxito <a href="javascript:printPlan({id})">Imprimir plan</a> | <a href="javascript:printPagare({id})">Imprimir pagare</a> | <a href="'.base_url().'movimientos/creditos/plan_credito/{id}">Ir a plan de crédito</a> | ');
				$crud->add_action('Imprimir plan','',base_url('reportes/rep/verReportes/91/html/creditos_id').'/');
				if($return){
					return $crud;
				}
				$crud = $crud->render();
				$crud->output = $this->load->view('ventas_list',array('output'=>$crud->output),TRUE);
				$this->loadView($crud);
			}
		}

		function plan_credito($venta = '',$return = false){
			$crud = $this->crud_function('','');
			$this->creditos_id = $venta;
			$crud->where('creditos_id',$venta)
				 ->field_type('creditos_id','hidden',$venta)
				 ->unset_add()
				 ->unset_delete()
				 ->fields('pagado')
				 ->display_as('fecha_retraso','Dias de retraso')
				 ->columns('nro_cuota','capital','interes','monto_a_pagar','fecha_a_pagar','fecha_retraso','total_pago_mora','total_pagado','saldo','pagado','fecha_pago')
				 ->callback_column('total_pago_mora',function($val,$row){
				 	$mora = get_instance()->db->query("SELECT IFNULL(SUM(total_mora),0) as total FROM pagocliente WHERE plan_credito_id = '".$row->id."' AND (pagocliente.anulado IS NULL OR pagocliente.anulado = 0)");
				 	return $mora->num_rows()>0?$mora->row()->total:0;
				 })
				 ->callback_column('nro_cuota',function($val,$row){
				 	$pagos = $this->db->get_where('pagocliente',array('plan_credito_id'=>$row->id,'anulado'=>0));
				 	if($pagos->num_rows()>0){
				 		return '<a href="javascript:pagos('.$row->id.')">'.$val.'</a>';
				 	}
				 	return $val;
				 })
				 ->callback_column('fecha_retraso',function($val,$row){
				 	$dias = strtotime(date("Y-m-d"))-strtotime(str_replace('/','-',$row->fecha_a_pagar));
				 	$dias = round($dias/60/60/24,0);
				 	$dias = $row->pagado==1?(string)'0':$dias;
				 	return $dias;
				 })
				 ->add_action('Pagar credito','',base_url('movimientos/creditos/pagar_credito').'/')
				 ->callback_column('pagado',function($val,$row){					 		
					 	return $val=='1'?'<span class="label label-success">PAGADO</span>':'<span class="label label-danger">PENDIENTE</span>';
				 });
			if($return && $return==1){
				return $crud;
			}
			$crud = $crud->render();			
			$this->as['plan_credito'] = 'creditos';
			$crud->header = $this->creditos('',true);
			$crud->header->set_theme('header_data')
					  	 ->where('creditos.id',$venta)
					  	 ->set_url('movimientos/creditos/creditos/');
			$crud->header = $crud->header->render(1)->output;  	 
			$crud->output = $this->load->view('pagar_credito',array('output'=>$crud->output),TRUE);
			$this->loadView($crud);				 
		}

		function pagar_credito($plan_credito = ''){
			if(is_numeric($plan_credito)){
				$this->db->select('plan_credito.*, ventas.cliente');
				$this->db->join('creditos','creditos.id = plan_credito.creditos_id');
				$this->db->join('ventas','ventas.id = creditos.ventas_id');
				$plan_credito = $this->db->get_where('plan_credito',array('plan_credito.id'=>$plan_credito));
				if($plan_credito->num_rows()>0){
					$plan_credito = $plan_credito->row();


					$dias = strtotime(date("Y-m-d"))-strtotime(str_replace('/','-',$plan_credito->fecha_a_pagar));
				 	$dias = $dias/60/60/24;				 	
				 	$mora = 0;
				 	$ajustes = $this->ajustes;
				 	if($dias>$ajustes->cant_dias_mora){
				 		$dias-= $ajustes->cant_dias_mora;
				 		if(!isset($this->ajustes->acumular_interes) || $this->ajustes->acumular_interes == 1){
							$mora = $dias * (($plan_credito->monto_a_pagar * $ajustes->porc_interes_mora)/100);
						}else{
							$mora = (($plan_credito->monto_a_pagar * $ajustes->porc_interes_mora)/100);
						}
					}

					$this->mora = $mora;
					$this->plan_credito = $plan_credito;
					$this->as['pagar_credito'] = 'pagocliente';
					$this->saldo = $this->db->query("SELECT 
						id,
						creditos_id,
						monto_a_pagar,
						totalpago.total_pagado as totalpagado
						FROM plan_credito
						INNER JOIN (
							SELECT pagocliente.plan_credito_id as plan_credito_id,
						    sum(pagocliente.total_pagado) as total_pagado
						    FROM pagocliente
						    WHERE (pagocliente.anulado = 0 or pagocliente.anulado is null)
						) as totalpago on totalpago.plan_credito_id = plan_credito.id
						WHERE creditos_id = ".$plan_credito->creditos_id);

					$cuotas = $this->db->query("
						SELECT CONCAT(pagadas,'/',cuotas) as res FROM (
						SELECT 
						COUNT(id) as cuotas,
						(SELECT COUNT(id)+1 FROM plan_credito WHERE creditos_id = $plan_credito->creditos_id AND pagado = 1)as pagadas
						FROM plan_credito 
						WHERE creditos_id = $plan_credito->creditos_id) as cd"
					)->row()->res;

					get_instance()->cuotas = 'Pago de cuota '.$cuotas;

					$crud = $this->crud_function('','');
					$crud->set_subject('Pago');
					$crud->field_type('clientes_id','hidden',$plan_credito->cliente)
						 ->field_type('cliente','hidden',@$this->db->get_where('clientes',array('id'=>$plan_credito->cliente))->row()->nombres)
						 ->field_type('plan_credito_id','hidden',$plan_credito->id)
						 ->field_type('creditos_id','hidden',$plan_credito->creditos_id)
						 ->field_type('fecha','hidden',date("Y-m-d H:i:s"))
						 ->field_type('anulado','hidden',0)
						 ->field_type('sucursal','hidden',$this->user->sucursal)
						 ->field_type('caja','hidden',$this->user->caja)
						 ->field_type('user','hidden',$this->user->id)
						 ->field_type('cajadiaria','hidden',$this->user->cajadiaria)
						 ->field_type('anulado','true_false')
						 ->field_type('fecha_anulado','hidden')
						 ->field_type('quien_anulado','hidden')
						 ->edit_fields('anulado')
						 ->where('plan_credito_id',$plan_credito->id)
						 ->unset_delete()
						 ->add_action('Imprimir recibo','',base_url('reportes/rep/verReportes/90/html/pago_cliente_id/').'/')
						 ->set_lang_string('insert_success_message','Pago registrado con éxito <a href="javascript:printRecibo({id})">Imprimir recibo</a> | ')
						 ->columns('recibo','fecha','total_pago_mora','formapago_id','total_credito','concepto','total_pagado','anulado')
						 ->callback_column('anulado',function($val,$row){
						 	$color = $val==0?'success':'danger';
						 	$val = $val==1?'SI':'NO';
						 	return '<span class="label label-'.$color.'">'.$val.'</span>';
						 })
						 ->callback_field('concepto',function($val){
						 	if(empty($val)){
						 		$val = get_instance()->cuotas;
						 	}
						 	return form_input('concepto',$val,'id="field-concepto" class="form-control"');
						 })						 
						 ->callback_field('total_pagado',function($val){
						 		$saldo = $this->saldo;							 									
							 	if(empty($val) && $saldo->num_rows()>0){
							 		$saldo = $saldo->row();
							 		$val = ($this->plan_credito->monto_a_pagar+$this->mora)-$saldo->totalpagado;
							 	}else{
							 		$val = empty($val)?$this->plan_credito->monto_a_pagar:$val;
							 	}
							 	return form_input('total_pagado',$val,'id="field-total_pagado" class="form-control"');
						 })
						 ->callback_field('total_credito',function($val){
						 	$val = empty($val)?$this->plan_credito->monto_a_pagar:$val;
						 	return form_input('total_credito',$val,'id="field-total_credito" class="form-control" readonly');
						 })
						 ->callback_field('total_mora',function($val){
						 	$val = empty($val)?$this->mora:$val;
						 	return form_input('total_mora',$val,'id="field-total_mora" class="form-control"');
						 });					

					if(!empty($_GET['modal'])){
						$crud->staticShowList = true;
						$crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
						$crud->columns('total_pagado','fecha');
						$crud->where('anulado !=',1);
						echo $crud->render(1)->output;
						die();
					}
					if($crud->getParameters()=='add'){
						$crud->set_rules('plan_credito_id','Cuota','required|callback_validar_cuota');
					}
					$crud = $crud->render();
					$crud->output = $this->load->view('pagar_credito',array('output'=>$crud->output),TRUE);
					$crud->title = 'Pago de cuotas';
					$this->as['pagar_credito'] = 'plan_credito';
					$crud->header = $this->plan_credito($plan_credito->creditos_id,true);
					$crud->header->set_theme('header_data')
								 ->where('plan_credito.id',$plan_credito->id)
								 ->set_url('movimientos/creditos/plan_credito/'.$plan_credito->creditos_id.'/');
					$crud->header = $crud->header->render(1)->output;
					$crud->output = $this->load->view('pagar_credito',array('crud'=>$crud),TRUE);					
					$this->loadView($crud);

				}
			}
		}

		function validar_cuota(){
			$cuota = $_POST['plan_credito_id'];			
			$cuotas = $this->db->get_where('plan_credito',['creditos_id'=>$_POST['creditos_id'],'id <'=>$cuota,'pagado'=>0]);
			if($cuotas->num_rows()>0){
				$this->form_validation->set_message('validar_cuota','Existe una cuota anterior a esta sin pagar, primero debes pagar las cuotas anteriores para poder pagar esta');
				return false;
			}
		}

		
	}
?>