<?php

require_once APPPATH.'/controllers/Panel.php';    

class Productos extends Panel {

    function __construct() {
        parent::__construct();     
    }     
    
    public function categorias($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->columns('id','denominacion','control_vencimiento');
        if($crud->getParameters()=='add'){
            $crud->set_rules('denominacion','Denominación','required|is_unique[categoriaproducto.denominacion]');
        }
        $crud->unset_delete();
        $crud->field_type('control_vencimiento', 'true_false', array(0 => 'No', 1 => 'Si'));
        $output = $crud->render();
        $this->loadView($output);
    }

    public function sub_categoria_producto($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->columns('id','nombre_sub_categoria_producto','categoriaproducto_id');
        $crud->display_as('nombre_sub_categoria_producto','nombre')
             ->display_as('categoriaproducto_id','categoria');
        $crud->unset_delete();        
        $output = $crud->render();
        $this->loadView($output);
    }

    public function sub_sub_categoria($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->columns('id','nombre_sub_sub_categoria','sub_categoria_producto_id');
        $crud->display_as('nombre_sub_sub_categoria','nombre')
             ->display_as('sub_categoria_producto_id','SubCategoria');
        $crud->unset_delete();        
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function productos($x = '', $y = '', $return = false) {
        if($x=='anulados' && $y=='edit'){
            redirect('/movimientos/productos/productos/edit/'.$return);
            return;
        }
        if($x=='codigoInterno'){
            echo $this->db->query('select getUltimoCodigoInterno()+1 as cod')->row()->cod;
            return;
        }
        if($x=='masDetalles' && !empty($y)){
            $this->load->view('_productosMasDetallesModal',['producto'=>$y]);
            return;
        }        
        $crud = parent::crud_function($x, $y, $this);
        
        $crud->set_theme('bootstrap');
        if(in_array('Cajeros',$this->user->getGroupsArray('nombre'))){            
            $crud->columns('id','foto_principal','codigo', 'codigo_interno','nombre_comercial', 'proveedor_id', 'categoria_id','precio_venta','precio_credito','precio_venta_mayorista1','precio_venta_mayorista2','precio_venta_mayorista3','descmax','stock','iva_id','descuentos');
            $crud->unset_add()                 
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read();
        }else{
            $crud->columns('foto_principal','codigo', 'codigo_interno','nombre_comercial', 'proveedor_id', 'categoria_id', 'precio_costo','precio_venta','precio_credito','precio_venta_mayorista1','precio_venta_mayorista2','precio_venta_mayorista3','stock','iva_id','descuentos');
        }
        if(!$this->user->allow_function('editar_productos')){
            $crud->unset_edit();
        }
        $crud->set_relation('proveedor_id', 'proveedores', 'denominacion');
        $crud->set_relation('categoria_id', 'categoriaproducto', 'denominacion');
        $crud->set_relation('nacionalidad_id', 'paises', 'denominacion');
        $crud->callback_column('stock',function($val,$row){
            $sucursal = empty($_POST['sucursal'])?$this->user->sucursal:$_POST['sucursal'];
            return @$this->db->get_where('productosucursal',array('producto'=>strip_tags($row->codigo),'sucursal'=>$sucursal))->row()->stock;
        });
        if($x!='json_list'){
            $crud->callback_column('codigo',function($val,$row){            
                return '<a href="javascript:;" onclick="masDetalles(\''.$val.'\')">'.$val.'</a>';
            });
        }
        if (empty($x) || $x == 'ajax_list' || $x == 'success'){
            $crud->set_relation('usuario_id', 'user', 'nombre');        
        }
        //Etiquetas
        $crud->display_as('proveedor_id', 'Proveedor')
                ->display_as('categoria_id', 'Categoria')
                ->display_as('id', 'ID de producto')
                ->display_as('nacionalidad_id', 'Nacionalidad')
                ->display_as('descmin', 'Minimo de descuento')
                ->display_as('descmax', 'Maximo de descuento')
                ->display_as('usuario_id', 'Usuario')
                ->field_type('iva_id', 'dropdown', array('0' => 'Exento', '5' => '5%', '10' => '10%'))
                ->field_type('inventariable', 'true_false', array('0' => 'NO', '1' => 'SI'))
                ->field_type('no_caja', 'true_false', array('0' => 'NO', '1' => 'SI'))
                ->field_type('foto_principal','image',array('path'=>'img/productos','width'=>'1024','height'=>'768'))
                ->display_as('iva_id', 'IVA')
                ->display_as('inventariable','Inventariable');
        //$crud->unset_fields('created', 'modified', 'stock');
        //Validaciones
        if($crud->getParameters()=='add'){
            $crud->set_rules('codigo', 'Codigo', 'required|is_unique[productos.codigo]');
        }        
        if($crud->getParameters()=='edit' && isset($_POST['codigo']) && isset($_POST['codigo_interno'])){
            $producto = $this->db->get_where('productos',['id'=>$y])->row();
            if($producto->codigo!=$_POST['codigo']){
                $crud->set_rules('codigo', 'Codigo', 'required|is_unique[productos.codigo]');
            }
            if($producto->codigo_interno!=$_POST['codigo_interno']){
                $crud->set_rules('codigo', 'Codigo', 'required|is_unique[productos.codigo_interno]');
            }
        }
        $crud->field_type('usuario_id', 'hidden', $_SESSION['user']);
        if (!empty($x) && !empty($y) && $y == 'json' && !empty($return)) {
            $crud->field_type('codigo', 'hidden', $return);
            $crud->unset_back_to_list();
            $crud->set_lang_string('insert_success_message', '<script>window.opener.tryagain(); window.close();</script>');
        }
        $crud->unset_delete(); 
        $crud->callback_before_insert(function($post){
            $post['codigo_interno'] = $this->db->query('select getUltimoCodigoInterno()+1 as cod')->row()->cod;
            return $post;
        });
        $crud->callback_before_update(function($post,$primary){
            $producto = get_instance()->db->get_where('productos',array('id'=>$primary));
            if($producto->row()->precio_venta!=$post['precio_venta']){
                get_instance()->db->update('productosucursal',array('precio_venta'=>$post['precio_venta']),array('producto'=>$producto->row()->codigo));
            }
            if($producto->row()->precio_costo!=$post['precio_costo']){
                $post['precio_costo_ant'] = $producto->row()->precio_costo;                
            }
            if($producto->row()->precio_venta!=$post['precio_venta']){
                $post['precio_venta_ant'] = $producto->row()->precio_venta;                
                $post['fecha_mod_precio'] = date("Y-m-d H:i:s");
            }
            //Actualizar padre en caso de que haya cambiado
            get_instance()->db->update('productos',['parent_codigo'=>$post['parent_codigo']],['parent_codigo'=>$producto->row()->parent_codigo]);
            return $post;
        })
        ->callback_after_insert(function($post){
            $sucursal = get_instance()->db->get_where('sucursales');
            foreach($sucursal->result() as $s){
                $this->db->insert('productosucursal',[
                    'proveedor_id'=>$post['proveedor_id'],
                    'fechaalta'=>date("Y-m-d H:i:s"),
                    'producto'=>$post['codigo'],
                    'sucursal'=>$s->id,
                    'lote'=>'',
                    'vencimiento'=>'',
                    'precio_venta'=>$post['precio_venta'],
                    'precio_costo'=>$post['precio_costo'],
                    'stock'=>0
                ]);
            }        
            return $post;
        })
        ->callback_column('foto_principal',function($val,$row){
            $val = empty($val)?base_url('200x200.png'):base_url('img/productos/'.$val);
            return '<a href="'.$val.'" class="image-thumbnail"><img src="'.$val.'" height="50px"></a>';
        });
        if($crud->getParameters()=='json_list'){
            $crud->callback_column('descuentos',function($val,$row){
                $fecha = date("Y-m-d");
                $this->db->where('estado',1);
                $this->db->where('productos_id',$row->id);
                $this->db->where('por_desc >',0);
                $this->db->where("DATE('$fecha') BETWEEN desde AND hasta",NULL,TRUE);
                $this->db->where('(descuento_detalle.anulado IS NULL OR descuento_detalle.anulado = 0)',NULL,TRUE);
                $this->db->select('desde,hasta,productos_id,por_desc,precio_desc');
                $this->db->order_by('hasta','ASC');
                return $this->db->get('descuento_detalle')->result();
            });
        }

        $crud->unset_read();

        $crud->add_action('Productos asociados','',base_url('movimientos/productos/producto_asociado').'/');        
        //Buscador de ventas
        if(!empty($_POST['codigo'])){
            if(is_numeric($_POST['codigo']) && empty($_POST['balanza'])){
                $crud->where("(productos.codigo = '".$_POST['codigo']."' OR productos.codigo2 = '".$_POST['codigo']."' OR productos.codigo_interno = '".$_POST['codigo']."') AND (productos.anulado IS NULL OR productos.anulado = '0')",'ESCAPE',TRUE);
            }else{
                $crud->where("(productos.codigo = '".$_POST['codigo']."' OR productos.codigo2 = '".$_POST['codigo']."') AND (productos.anulado IS NULL OR productos.anulado = '0')",'ESCAPE',TRUE);
            }
        }

        if(!empty($_POST['query'])){
            if(is_numeric($_POST['query'])){
                $crud->where("(productos.codigo = '".$_POST['query']."' OR productos.codigo2 = '".$_POST['query']."' OR productos.codigo_interno = '".$_POST['query']."' OR productos.nombre_comercial LIKE '%".$_POST['query']."%')",'ESCAPE',TRUE);
            }else{
                $crud->where("(productos.codigo = '".$_POST['query']."' OR productos.codigo2 = '".$_POST['query']."' OR productos.nombre_comercial LIKE '%".$_POST['query']."%')",'ESCAPE',TRUE);
            }
            
        }
        if($x!='anulados'){
            $crud->where('(productos.anulado = 0 OR productos.anulado IS NULL)','ESCAPE',TRUE);
        }else{
            $crud->where('productos.anulado = 1','ESCAPE',TRUE);
        }

        if($x=='json_list'){
            $crud->columns([
                'id','foto_principal','codigo', 'codigo_interno','nombre_comercial', 'proveedor_id', 'categoria_id','precio_venta','precio_credito','precio_venta_mayorista1','precio_venta_mayorista2','precio_venta_mayorista3','descmax','stock','iva_id','descuentos',
                'cant_1',
                'cant_2',
                'cant_3',
                'porc_comision',
                'porc_mayorista1',
                'porc_mayorista2',
                'porc_mayorista3',
                'porc_venta',
                'precio_costo',
                'precio_venta',
                'precio_venta_mayorista1',
                'precio_venta_mayorista2',
                'precio_venta_mayorista3',
                'servicios'
            ]);
        }

        $crud->callback_column('cant_1',function($val,$row){return get_instance()->getVal($row,'cant_1');});
        $crud->callback_column('cant_2',function($val,$row){return get_instance()->getVal($row,'cant_2');});
        $crud->callback_column('cant_3',function($val,$row){return get_instance()->getVal($row,'cant_3');});
        $crud->callback_column('precio_costo',function($val,$row){return get_instance()->getVal($row,'precio_costo');});
        $crud->callback_column('precio_venta',function($val,$row){return get_instance()->getVal($row,'precio_venta');});
        $crud->callback_column('precio_venta_mayorista1',function($val,$row){return get_instance()->getVal($row,'precio_venta_mayorista1');});
        $crud->callback_column('precio_venta_mayorista2',function($val,$row){return get_instance()->getVal($row,'precio_venta_mayorista2');});
        $crud->callback_column('precio_venta_mayorista3',function($val,$row){return get_instance()->getVal($row,'precio_venta_mayorista3');});


        $output = $crud->render();   
        if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
            $edit = (is_numeric($y))?$edit = $this->db->get_where('productos',array('id'=>$y)):'';
            $output->output = $this->load->view('productos',array('action'=>$crud->getParameters(),'output'=>$output->output,'edit'=>$edit),TRUE);
        }
        if($crud->getParameters()=='list'){
            $output->output = $this->load->view('productosList',array('output'=>$output->output),TRUE);
        }
        $this->loadView($output);
    }

    public function getVal($row,$field){
        if(!isset($this->ajustes->precioxsucursal) || $this->ajustes->precioxsucursal==0){
            return $row->$field;
        }
        $value = 0;
        $producto = $this->db->get_where('productosucursal',['producto'=>$row->codigo,'sucursal'=>$this->user->sucursal])->result();
        if(!empty($producto)){
            $value = $producto[0]->$field;
        }
        
        return $value;
    }

    function producto_asociado($x = ''){
        if(is_numeric($x)){
            $crud = parent::crud_function("",'');
            $crud->where('productos_id',$x)
                 ->field_type('productos_id','hidden',$x)
                 ->unset_columns('productos_id')
                 ->set_relation('descontar','productos','{codigo} {codigo_interno} {nombre_comercial}',['ingrediente'=>1]);
            $output = $crud->render();

            $head = new ajax_grocery_crud();
            $head->set_table('productos')->set_theme('header_data')->set_subject('producto')->columns('codigo','nombre_comercial','nombre_generico','propiedades')->where('productos.id',$x)->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
            $output->header = $head->render(1)->output;
            $this->loadView($output);
        }
    }
    
    function buscador_productos(){
        $this->as['buscador_productos'] = 'productos';
        $crud = parent::crud_function("",''); 
        $crud->columns('id','codigo','nombre_comercial','precio_venta','precio_credito','precio_costo','stock');
        if($crud->getParameters(FALSE)!=='json_list'){
            $crud->callback_column('nombre_comercial',function($val,$row){
                return '<a href="javascript:parent.seleccionarProducto('.$row->id.',\''.$val.'\','.$row->precio_venta.','.$row->precio_credito.', '.$row->precio_costo.')">'.$val.'</a>';
            });
        }
        $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_export()
                 ->unset_print()
                 ->unset_read();
        $output = $crud->render();
        $output->view = "json";
        $output->output = $this->load->view('busqueda',array('output'=>$output->output),TRUE);
        $this->loadView($output);
    }

    public function allStock($producto) {   
        $this->db->select("
            productos.codigo,
            productos.nombre_comercial,
            productos.nombre_generico,
            productosucursal.stock,
            productosucursal.vencimiento,
            productos.precio_venta,
            sucursales.denominacion as sucursal
        ");
        $this->db->join('productos','productos.codigo = productosucursal.producto');
        $this->db->join('sucursales','sucursales.id = productosucursal.sucursal');
        $productos = $this->db->get_where('productosucursal',['producto'=>$producto]);
        sqltohtml($productos);
    }
    
    public function inventario($x = '', $y = '') {   
        if($x=='reload'){
            $this->db->query('call set_stock('.$y.')');
            redirect('movimientos/productos/inventario/success');
        }     
        $this->as['inventario'] = 'productosucursal';        
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap');
        $crud->unset_read()
             ->unset_export()
             ->unset_print()
             ->unset_edit()
             ->unset_delete()
             ->unset_add();     
        $crud->columns('j4b04c546.codigo','j4b04c546.nombre_comercial','j4b04c546.nombre_generico','stock','precio_venta','precio_credito','sucursal');
        $crud->set_relation('productos_id','productos','{codigo}|{nombre_comercial}|{nombre_generico}');
        $crud->set_relation('sucursal','sucursales','denominacion');
        /*$crud->callback_column('scb5fc3d6', function($val, $row) {
            return '<a href="' . base_url('movimientos/productos/inventario/' . $row->sucursal) . '">' . $val . '</a>';
        });*/
        $crud->callback_column('precio_venta', function($val, $row) {
            return $this->ajustes->precioxsucursal==1?$val:$this->db->get_where('productos',['id'=>$row->productos_id])->row()->precio_venta;
        });
        $crud->callback_column('j4b04c546.codigo', function($val, $row) {
            return explode('|',$row->s4b04c546)[0];
        });
        $crud->callback_column('j4b04c546.nombre_comercial', function($val, $row) {
            return explode('|',$row->s4b04c546)[1];
        });
        $crud->callback_column('j4b04c546.nombre_generico', function($val, $row) {
            return explode('|',$row->s4b04c546)[2];
        });
        $crud->callback_column('stock', function($val, $row) {
            //return $val.' <a href="' . base_url('movimientos/productos/inventario/reload/'.$row->producto).'"><i class="la la-refresh"></i></a>';
            return $val.' <a href="javascript:;" onclick="allStock(\''.$row->producto.'\')"><i class="fa fa-box"></i></a>';
        });

        $crud->display_as('j4b04c546.codigo','Codigo')
             ->display_as('j4b04c546.nombre_comercial','Nombre Comercial')
             ->display_as('j4b04c546.nombre_generico','Nombre Genérico');
        //$crud->set_primary_key('codigo','productos');
        //if (is_numeric($x)) {
            $crud->where('sucursal', $this->user->sucursal);
        //}
        $crud->where('(j4b04c546.anulado = 0 OR j4b04c546.anulado IS NULL)','ESCAPE',TRUE);
        $output = $crud->render();
        $output->output = $this->load->view('inventario',array('output'=>$output->output),TRUE);
        $output->crud = 'productosucursal';
        $this->loadView($output);
    }

    public function inventario_modal($sucursal = ''){
        if($sucursal == 'ajax_list_info'){
            //echo '{"total_results":300}';
            //return;
        }
        $crud = new ajax_grocery_crud();
        $crud->set_table('productosucursal')
             ->set_subject('Inventario')
             ->set_theme('bootstrap')
             ->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_print()
             ->unset_export()
             ->unset_read()   
             ->field_type('productos_id','hidden')          
             ->callback_column('Codigo',function($val,$row){
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
             });
            $crud->columns('j286e18ee.codigo','j286e18ee.codigo_interno','j286e18ee.nombre_comercial','stock','j286e18ee.precio_venta','j286e18ee.precio_venta_mayorista1','j286e18ee.precio_venta_mayorista2');
            $crud->set_relation('producto','productos','{codigo}|{nombre_comercial}|{nombre_generico}|{precio_venta}|{codigo_interno}|{foto_principal}|{precio_costo}|{precio_venta_mayorista1}|{precio_venta_mayorista2}');
            //$crud->set_relation('sucursal','sucursales','denominacion');
            $crud->callback_column('scb5fc3d6', function($val, $row) {
                return '<a href="' . base_url('movimientos/productos/inventario/' . $row->sucursal) . '">' . $val . '</a>';
            });
            $crud->callback_column('j286e18ee.codigo', function($val, $row) {
                $val = explode('|',$row->s286e18ee)[0];
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
            });            
            $crud->callback_column('j286e18ee.nombre_comercial', function($val, $row) {
                $cod = explode('|',$row->s286e18ee)[0];
                $val = explode('|',$row->s286e18ee)[1];
                $img = explode('|',$row->s286e18ee)[5];
                $img = empty($img)?base_url('200x200.png'):base_url('img/productos/'.$img);
                return '<a href="javascript:selCod(\''.$cod.'\',\'\',\'\')">'.$val.'</a> <a href="javascript:showImage(\''.$img.'\')"><i class="fa fa-image"></i></a>';
            });            
            $crud->callback_column('j286e18ee.nombre_generico', function($val, $row) {
                return explode('|',$row->s286e18ee)[2];
            });
            $crud->callback_column('j286e18ee.precio_venta', function($val, $row) {                
                $val = explode('|',$row->s286e18ee)[3];
                return number_format((float)$val,0,',','.');
            });
            $crud->callback_column('j286e18ee.codigo_interno', function($val, $row) {
                $val = explode('|',$row->s286e18ee)[4];
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
            });
            $crud->callback_column('j286e18ee.precio_costo', function($val, $row) {
                $val = explode('|',$row->s286e18ee)[6];
                return $val;
            });
            $crud->callback_column('j286e18ee.precio_venta_mayorista1', function($val, $row) {
                $val = explode('|',$row->s286e18ee)[7];
                return $val;
            });
            $crud->callback_column('j286e18ee.precio_venta_mayorista2', function($val, $row) {
                $val = explode('|',$row->s286e18ee)[8];
                return $val;
            });
            $crud->callback_column('stock', function($val, $row) {
                return $val.' <a href="' . base_url('movimientos/productos/inventario/reload/'.$row->producto).'"><i class="fa  fa-refresh "></i></a>';
            });
            $crud->display_as('j286e18ee.codigo','Codigo')
                 ->display_as('j286e18ee.nombre_comercial','Nombre Comercial')
                 ->display_as('j286e18ee.nombre_generico','Nombre Genérico');
            $crud->set_primary_key('codigo','productos');

        if(!empty($this->user->sucursal) && !is_numeric($sucursal)){
            $crud->where('sucursal',$this->user->sucursal);
        }elseif(is_numeric($sucursal)){
            $crud->where('sucursal',$sucursal);
        }
        $crud->order_by('nombre_comercial','ASC');
        $crud->where("(j286e18ee.anulado IS NULL OR j286e18ee.anulado = '0')",'ESCAPE',TRUE);
        $crud = $crud->render();
        $this->loadView($crud);

    }

    public function productos_modal($x = '', $y = '', $return = false) {
        $this->as['productos_modal'] = 'productos';
        $crud = parent::crud_function($x, $y, $this);
        
        $crud->set_theme('bootstrap');
        
        $crud->columns('codigo','codigo_interno','nombre_comercial','precio_venta')
              ->unset_add()
               ->unset_edit()
               ->unset_delete()
               ->unset_print()
               ->unset_export()
               ->unset_read()
               ->unset_jquery()
               ->unset_jquery_ui()
               ->set_primary_key('id')                                             
               ->display_as('sucursales_id','Sucursal')
               ->callback_column('codigo',function($val,$row){
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
             });
        if(!empty($_POST['codigo_list'])){
            $crud->where("(productos.codigo = '".$_POST['codigo_list']."' OR productos.codigo2 = '".$_POST['codigo_list']."' OR productos.codigo_interno = '".$_POST['codigo_list']."' OR productos.nombre_comercial LIKE '%".$_POST['codigo_list']."%')",'ESCAPE',TRUE);
        }
        //if($crud->getParameters()=='json_list'){
            $crud->where('(productos.anulado = 0 OR productos.anulado IS NULL)','ESCAPE',TRUE);
        //}
        $crud->order_by('nombre_comercial','ASC');
        $output = $crud->render();   
        
        $this->loadView($output);
    }

    public function inventario_modal_compras($sucursal = ''){
        if($sucursal == 'ajax_list_info'){
            echo '{"total_results":100}';
            return;
        }
        $crud = new ajax_grocery_crud();
        $crud->set_table('productos')
             ->set_subject('Inventario')
             ->set_theme('bootstrap')
             ->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_print()
             ->unset_export()
             ->unset_read()             
             ->callback_column('codigo',function($val,$row){
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
             });
            $crud->columns('codigo','codigo_interno','nombre_comercial','nombre_generico','stock','precio_venta');
            $crud->callback_column('codigo', function($val, $row) {                
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
            }); 
            $crud->callback_column('precio_venta', function($val, $row) {
                return number_format((float)$val,0,',','.');
            });

            $crud->callback_column('nombre_comercial', function($val, $row) {                
                $img = $row->foto_principal;
                $img = empty($img)?base_url('200x200.png'):base_url('img/productos/'.$img);
                return $val.' <a href="javascript:showImage(\''.$img.'\')"><i class="fa fa-image"></i></a>';
            });
            $crud->where("(productos.anulado IS NULL OR productos.anulado = '0')",'ESCAPE',TRUE);
            $crud->set_primary_key('codigo','productos');
            $crud = $crud->render();
            $this->loadView($crud);
    }
    
    public function transferencias($x = '', $y = '',$z = '') {
        redirect('movimientos/transferencias/transferencias');
    }

    public function transferencias_detalles($x = '') {        
            $crud = parent::crud_function('','');
            $crud->set_theme('bootstrap');
            $crud->unset_delete()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_export()
                 ->unset_read()
                 ->unset_print()
                 ->set_primary_key('codigo','productos')
                 ->set_relation('producto','productos','{codigo}|{nombre_comercial}|{nombre_generico}')
                 ->columns('codigo','nombre_comercial','nombre_generico','cantidad')
                 ->callback_column('codigo',function($val,$row){return @explode('|',$row->s286e18ee)[0];})
                 ->callback_column('nombre_comercial',function($val,$row){return @explode('|',$row->s286e18ee)[1];})
                 ->callback_column('nombre_generico',function($val,$row){return @explode('|',$row->s286e18ee)[2];});
            $crud->where('transferencia',$x);
            $crud->staticShowList = true;
            $output = $crud->render();
            echo $output->output;
    }

    public function productosucursal_ainsert($post, $id) {
        $this->db->update('productosucursal', array('precio_venta' => $post['precio_venta']), array('producto' => $post['producto']));
        $this->db->update('productos', array('precio_venta' => $post['precio_venta']), array('codigo' => $post['producto']));
    }
    /* Cruds */

    function productos_costos($prodId){
        $crud = $this->crud_function("","");
        $crud->where('productos_id',$prodId)
             ->field_type('productos_id','hidden',$prodId)
             ->unset_columns('productos_id')
             ->set_relation('ingredientes_id','productos','{codigo} {nombre_comercial}',array('ingrediente'=>1));                
        $crud = $crud->render();
        $crud->header = new ajax_grocery_crud();
        $crud->header = $crud->header->set_table('productos')
               ->set_subject('productos')
               ->set_theme('header_data')
               ->columns('codigo','nombre_generico','nombre_comercial')
               ->where('productos.id',$prodId)
               ->render('1')
               ->output;         
        $crud->title = "Adm. Costos de productos";
        $crud->output = $this->load->view('productos_costos',array('crud'=>$crud),TRUE);
        $this->loadView($crud);
    }

    public function updateInventario($x = '', $y = '') {           
        $this->as['updateInventario'] = 'productosucursal';        
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap');
        $crud->set_subject('Actualización de Stock');
        $crud->unset_read()
             ->unset_export()
             ->unset_print()             
             ->unset_delete()
             ->unset_add();     
        $crud->columns('j286e18ee.codigo','j286e18ee.codigo_interno','j286e18ee.nombre_comercial','j286e18ee.nombre_generico','stock','vencimiento');
        $crud->set_relation('producto','productos','{codigo}|{nombre_comercial}|{nombre_generico}|{codigo_interno}');
        $crud->set_relation('sucursal','sucursales','denominacion');
        $crud->callback_column('scb5fc3d6', function($val, $row) {
            return '<a href="' . base_url('movimientos/productos/inventario/' . $row->sucursal) . '">' . $val . '</a>';
        });
        $crud->callback_column('j286e18ee.codigo', function($val, $row) {
            return explode('|',$row->s286e18ee)[0];
        });
        $crud->callback_column('j286e18ee.nombre_comercial', function($val, $row) {
            return explode('|',$row->s286e18ee)[1];
        });
        $crud->callback_column('j286e18ee.nombre_generico', function($val, $row) {
            return explode('|',$row->s286e18ee)[2];
        });
        $crud->callback_column('j286e18ee.codigo_interno', function($val, $row) {
            return explode('|',$row->s286e18ee)[3];
        });
        $crud->callback_column('stock', function($val, $row) {
            return '<input type="number" name="stock" class="form-control stockage" data-id="'.$row->id.'" value="'.$val.'">';
        });
        $crud->edit_fields('stock');
        $crud->display_as('j286e18ee.codigo','Codigo')
             ->display_as('j286e18ee.nombre_comercial','Nombre Comercial')
             ->display_as('j286e18ee.nombre_generico','Nombre Genérico')
             ->display_as('j286e18ee.codigo_interno','Código Interno');
        $crud->set_primary_key('codigo','productos');
        $crud->where('sucursal',$this->user->sucursal);        
        $output = $crud->render();        
        $output->title = 'Actualización Masiva de Stock';
        $output->output = $this->load->view('updateInventario',['output'=>$output->output],TRUE);
        $this->loadView($output);
    }

    function vaciarStock($sucursal){
        $this->db->select('productosucursal.*');
        $this->db->join('productos','productos.codigo = productosucursal.producto');
        $stock = $this->db->get_where('productosucursal',['productosucursal.stock >'=>0,'sucursal'=>$sucursal]);
        echo json_encode($stock->result());
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
