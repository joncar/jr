<?php
	require_once APPPATH.'/controllers/Panel.php';
	class Facturas_clientes extends Panel {
		function __construct(){
			parent::__construct();
			if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
	            header("Location:".base_url('panel/selsucursal'));
	            die();
	        }
	        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
	            header("Location:".base_url('panel/selcaja'));
	            die();
	        }
	        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
	            header("Location:".base_url('panel/selcajadiaria'));
	            die();
	        }
		}

		function facturas_clientes(){
			$crud = $this->crud_function('','');			
			$crud->set_subject('Saldo');
			$crud->set_relation('clientes_id','clientes','{nro_documento}|{nombres}|{apellidos}');
			$crud->columns('j3eb7f57f.nro_documento','j3eb7f57f.nombres','j3eb7f57f.apellidos','saldo_cliente','ultimo_pago');
			$crud->display_as('j3eb7f57f.nro_documento','#documento')
				 ->display_as('j3eb7f57f.nombres','Nombres')
				 ->display_as('j3eb7f57f.apellidos','Apellidos')
				 ->callback_column('j3eb7f57f.nro_documento',function($val,$row){return explode('|',$row->s3eb7f57f)[0];})
				 ->callback_column('j3eb7f57f.nombres',function($val,$row){return explode('|',$row->s3eb7f57f)[1];})
				 ->callback_column('j3eb7f57f.apellidos',function($val,$row){return explode('|',$row->s3eb7f57f)[2];});
			$crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
			$crud->display_as('clientes_id','Cliente');
			$crud->add_action('Facturas','',base_url('movimientos/facturas_clientes/facturas_clientes_detalles').'/');
			$crud->add_action('Añadir Pagos','',base_url('movimientos/facturas_clientes/facturas_clientes_pagos/add').'/');
			$crud->callback_after_update(function($post,$primary){
				$this->db->query("UPDATE facturas_clientes SET actualizar_saldo = 1,saldo_cliente = get_saldo(clientes_id) WHERE id = ".$primary);
			});
			$crud = $crud->render();
			$crud->title = 'Saldo de Clientes';
			$this->loadView($crud);
		}

		function facturas_clientes_detalles($cliente,$y = '',$z = '',$r = ''){
			if(is_numeric($y) && !empty($z) && !empty($r)){
				redirect('movimientos/facturas_clientes/facturas_clientes_detalles/'.$cliente.'/'.$z.'/'.$r);
				die;
			}
			$this->as['facturas_clientes_detalles'] = 'facturas_clientes_detalle';		
			$crud = $this->crud_function('','');
			$crud->set_subject('Facturas');
			$crud->unset_columns('facturas_clientes')
				 ->where('facturas_clientes',$cliente)
				 ->field_type('facturas_clientes','hidden',$cliente)
				 ->set_relation('venta','ventas','{id} - {nro_factura}')
				 ->edit_fields('anulado')
				 ->unset_delete()->unset_print()->unset_export()->unset_read()->unset_add();
			if(is_numeric($y)){
				if($y==1){
					$crud->where('(facturas_clientes_detalle.pagado=0)','ESCAPE',TRUE);
				}
				if($y==2){
					$crud->where('(facturas_clientes_detalle.pagado=1)','ESCAPE',TRUE);
				}
				if($y==3){
					 $crud->where('facturas_clientes_detalle.pagare = 1','ESCAPE',TRUE);
					 $crud->add_action_list('','Pagar seleccionados','javascript:pagarModal()');
					 $crud->add_action_list('','Pagos realizados',base_url('movimientos/facturas_clientes/facturas_clientes_pagos_pagare/'.$cliente));
				}
			}
			
			$crud = $crud->render();
			$crud->header = new ajax_grocery_crud();
			$crud->header->set_table('facturas_clientes')
						->where('facturas_clientes.id',$cliente)
						->set_subject('Facturas')
						->set_theme('header_data')						
						->set_relation('clientes_id','clientes','nombres')						
						->set_url('movimientos/facturas_clientes/facturas_clientes/');			
			$crud->header = $crud->header->render(1)->output;
			$crud->header = '<a href="'.base_url('movimientos/facturas_clientes/facturas_clientes_pagos/'.$cliente).'" class="btn btn-info">Administrar Pagos</a>'.$crud->header;
			$crud->title = 'Facturas';
			$crud->output = $this->load->view('facturas_clientes_detalles',['cliente'=>$cliente,'act'=>$y,'output'=>$crud->output],TRUE);
			$this->loadView($crud);
		}

		function facturas_clientes_pagos($cliente,$cliente2 = ''){
			if($cliente=='add' && is_numeric($cliente2)){
				redirect('movimientos/facturas_clientes/facturas_clientes_pagos/'.$cliente2.'/add');
				return;
			}
			$crud = $this->crud_function('','');
			$crud->set_subject('Pagos');
			$crud->set_theme('bootstrap2_horizontal');
			$crud->display_as('bancos_id','Banco')
             	 ->display_as('cuentas_bancos_id','Cuenta')
             	 ->set_relation('bancos_id','bancos','denominacion')
	             ->set_relation('cuentas_bancos_id','cuentas_bancos','nro_cuenta')
	             ->set_relation_dependency('cuentas_bancos_id','bancos_id','bancos_id');
			$crud->unset_columns('facturas_clientes')
				 ->where('facturas_clientes',$cliente)
				 ->field_type('facturas_clientes','hidden',$cliente)	
				 ->field_type('tipo_cheque','dropdown',['1'=>'A la vista','-1'=>'Diferido'])			 
				 ->field_type('pagare','hidden',0)
				 ->edit_fields('anulado')
				 ->unset_delete()->unset_print()->unset_export()->unset_read()
				 ->field_type('fecha','hidden',date("Y-m-d H:i:s"))
				 ->field_type('cajadiaria_id','hidden',$this->user->cajadiaria)
				 ->set_lang_string('insert_success_message','Su pago ha sido registrado con éxito <script>window.open("'.base_url('reportes/rep/verReportes/90/html/valor').'/{id}","_blank")</script>')
				 ->callback_field('fecha_emision',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_emision" id="field-fecha_emision" class="form-control" value="'.$value.'">';})
	             ->callback_field('fecha_pago',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_pago" id="field-fecha_pago" class="form-control" value="'.$value.'">';})
				 ->callback_after_insert(function($post,$primary){		                
		                //Insertar en libreo de banco
		                if($post['formapago_id'] == '3' || $post['formapago_id']=='4' || $post['formapago_id']=='5'  || $post['formapago_id']=='6'){
		                	switch($post['formapago_id']){
		                		case '3':
		                		case '4':
		                			$forma = 'Cheque';
		                		break;
		                		case '5':
		                			$forma = 'Deposito';
		                		break;
		                		case '6':
		                			$forma = 'Transferencia';
		                		break;
		                	}		   

		                	$saldo_actual = $this->db->get_where('cuentas_bancos',['id'=>$post['cuentas_bancos_id']])->row()->saldo;             

		                    $this->db->insert("libro_banco",[
		                        'sucursales_id'=>$this->user->sucursal,
		                        'cajas_id'=>$this->user->caja,
		                        'cajadiaria_id'=>$this->user->cajadiaria,
		                        'fecha'=>date("Y-m-d H:i:s"),
		                        'saldo_actual'=>$saldo_actual,
		                        'bancos_id'=>$post['bancos_id'],
		                        'cuentas_bancos_id'=>$post['cuentas_bancos_id'],
		                        'tipo_movimiento'=>1,
		                        'concepto'=>$forma,
		                        'beneficiario'=>$this->db->get_where('cuentas_bancos',['id'=>$post['cuentas_bancos_id']])->row()->nro_cuenta,
		                        'monto'=>$post['monto'],
		                        'comprobante'=>$primary,
		                        'forma_pago'=>$forma,
		                        'tipo_cheque'=>$post['tipo_cheque'],
		                        'nro_cheque'=>$post['nro_cheque'],
		                        'fecha_emision'=>date("Y-m-d H:i:s",strtotime(str_replace('/','-',$post['fecha_emision']))),
		                        'fecha_pago'=>date("Y-m-d H:i:s",strtotime(str_replace('/','-',$post['fecha_pago']))),
		                        'cobrado'=>0,
		                        'anulado'=>0,
		                        'user_id'=>$this->user->id,
		                        'fecha_registro'=>date("Y-m-d H:i:s")
		                    ]);
		                }
		                $this->db->query("UPDATE facturas_clientes SET actualizar_saldo = 1,saldo_cliente = get_saldo(clientes_id) WHERE id = ".$post['facturas_clientes']);
						$cuotas = $this->db->get_where('facturas_clientes_detalle',['pagado'=>0,'facturas_clientes'=>$post['facturas_clientes']]);
						$monto = $post['monto'];
						foreach($cuotas->result() as $cuota){
							if($monto>0){
								$this->db->update('facturas_clientes_detalle',['ultimo_pago'=>date("Y-m-d H:i:s")],['id'=>$cuota->id]);
								$monto-=$cuota->saldo_venta;
							}
						}
	             })
	             ->callback_after_update(function($post,$primary){
	             	$row = $this->db->get_where('facturas_clientes_pagos',['id'=>$primary])->row();
	             	$this->db->query("UPDATE facturas_clientes SET actualizar_saldo = 1,saldo_cliente = get_saldo(clientes_id) WHERE id = ".$row->facturas_clientes);
	             });
			if($crud->getParameters()=='add'){
				$crud->field_type('anulado','hidden',0);
			}
			if(in_array(3,$this->user->getGroupsArray())){
				$crud->unset_edit()
				     ->field_type('user_id','hidden',$this->user->id);
			}else{
				$crud->set_relation('user_id','user','{nombre} {apellido}');
			}
			$crud = $crud->render();
			$crud->header = new ajax_grocery_crud();
			$crud->header->set_table('facturas_clientes')
						->where('facturas_clientes.id',$cliente)
						->set_subject('Facturas')
						->set_theme('header_data')						
						->set_relation('clientes_id','clientes','nombres')
						->set_url('movimientos/facturas_clientes/facturas_clientes/');
			$crud->header = $crud->header->render(1)->output;
			if(!in_array(3,$this->user->getGroupsArray())){
				$crud->header = '<a href="'.base_url('movimientos/facturas_clientes/facturas_clientes_detalles/'.$cliente).'" class="btn btn-info">Administrar Facturas</a>'.$crud->header;
			}
			$crud->title = 'Pagos';			
			$crud->output = $this->load->view('facturas_clientes_pagos',['output'=>$crud->output],TRUE);
			$this->loadView($crud);
		}

		function facturas_clientes_pagos_general(){
			$this->as['facturas_clientes_pagos_general'] = 'facturas_clientes_pagos';
			$crud = $this->crud_function('','');
			$crud->set_subject('Pagos');
			$crud->set_theme('bootstrap2_horizontal');
			$crud->display_as('bancos_id','Banco')
             	 ->display_as('cuentas_bancos_id','Cuenta')
             	 ->display_as('j00b9b8b9.nro_documento','#Doc')
             	 ->display_as('j00b9b8b9.nombres','Nombres')
             	 ->display_as('j00b9b8b9.apellidos','Apellidos')
             	 ->edit_fields('anulado')
             	 ->set_relation('bancos_id','bancos','denominacion')
	             ->set_relation('cuentas_bancos_id','cuentas_bancos','nro_cuenta')
	             ->set_relation('facturas_clientes','facturas_clientes','clientes_id')
	             ->set_relation('j47350949.clientes_id','clientes','{nro_documento}|{nombres}|{apellidos}')
	             ->columns('id','j00b9b8b9.nro_documento','j00b9b8b9.nombres','j00b9b8b9.apellidos','fecha','nro_recibo','formapago_id','concepto','monto','bancos_id','cuentas_bancos_id','nro_cheque','tipo_cheque','fecha_emision','fecha_pago','cajadiaria_id','pagare','user_id','anulado')
	             ->set_relation_dependency('cuentas_bancos_id','bancos_id','bancos_id')
				 ->field_type('tipo_cheque','dropdown',['1'=>'A la vista','-1'=>'Diferido'])			 
				 ->field_type('pagare','hidden',0)
				 ->edit_fields('anulado')
				 ->unset_add()->unset_delete()->unset_print()->unset_export()->unset_read()
				 ->field_type('fecha','hidden',date("Y-m-d H:i:s"))
				 ->field_type('cajadiaria_id','hidden',$this->user->cajadiaria)
				 ->field_type('user_id','hidden',$this->user->id)
				 ->set_lang_string('insert_success_message','Su pago ha sido registrado con éxito <script>window.open("'.base_url('reportes/rep/verReportes/90/html/valor').'/{id}","_blank")</script>')
				 ->callback_field('fecha_emision',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_emision" id="field-fecha_emision" class="form-control" value="'.$value.'">';})
	             ->callback_field('fecha_pago',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_pago" id="field-fecha_pago" class="form-control" value="'.$value.'">';})
	             ->callback_column('j00b9b8b9.nro_documento',function($val,$row){return explode('|',$row->s00b9b8b9)[0];})
	             ->callback_column('j00b9b8b9.nombres',function($val,$row){return explode('|',$row->s00b9b8b9)[1];})
	             ->callback_column('j00b9b8b9.apellidos',function($val,$row){return explode('|',$row->s00b9b8b9)[2];})
	             ->callback_after_update(function($post,$primary){
					$row = $this->db->get_where('facturas_clientes_pagos',['id'=>$primary])->row();
					$this->db->query("UPDATE facturas_clientes SET actualizar_saldo = 1,saldo_cliente = get_saldo(clientes_id) WHERE id = ".$row->facturas_clientes);
				});
			$crud = $crud->render();
			$crud->title = 'Pagos';
			$this->loadView($crud);
		}

		function facturas_clientes_pagos_pagare($cliente){			
			$crud = $this->crud_function('','');
			$crud->set_subject('Pagos');
			$crud->set_theme('bootstrap2_horizontal');
			$crud->display_as('bancos_id','Banco')
             	 ->display_as('cuentas_bancos_id','Cuenta')
             	 ->set_relation('bancos_id','bancos','denominacion')
	             ->set_relation('cuentas_bancos_id','cuentas_bancos','nro_cuenta')
	             ->set_relation_dependency('cuentas_bancos_id','bancos_id','bancos_id');
			$crud->unset_columns('facturas_clientes')
				 ->where('facturas_clientes',$cliente)
				 ->field_type('facturas_clientes','hidden',$cliente)	
				 ->field_type('tipo_cheque','dropdown',['1'=>'A la vista','-1'=>'Diferido'])			 
				 ->field_type('pagare','hidden',0)
				 ->edit_fields('anulado')
				 ->unset_delete()->unset_print()->unset_export()->unset_read()->unset_add()
				 ->field_type('fecha','hidden',date("Y-m-d H:i:s"))
				 ->field_type('cajadiaria_id','hidden',$this->user->cajadiaria)
				 ->field_type('user_id','hidden',$this->user->id)
				 ->callback_field('fecha_emision',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_emision" id="field-fecha_emision" class="form-control" value="'.$value.'">';})
	             ->callback_field('fecha_pago',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_pago" id="field-fecha_pago" class="form-control" value="'.$value.'">';})
	             ->callback_before_update(function($post,$primary){
	             	$row = $this->db->get_where('facturas_clientes_pagos_pagare',['id'=>$primary]);	             		             	
	             	if($row->num_rows()>0){
	             		$row = $row->row();
	             		$factura = $this->db->get_where('facturas_clientes_detalle',['id'=>$row->facturas_clientes_detalles]);
	             		if($factura->num_rows()>0){		             		
		             		$factura = $factura->row();
		             		if($row->anulado==0 && $post['anulado']==1){
			             		$this->db->query("UPDATE facturas_clientes SET actualizar_saldo = 1,saldo_cliente = get_saldo(clientes_id) WHERE id = ".$row->facturas_clientes);
			             		$saldo_venta = $factura->saldo_venta+$row->monto;
			             		$this->db->update('facturas_clientes_detalle',[
			             			'saldo_venta'=>$saldo_venta,
			             			'pagado'=>0,
			             			'fecha_pagado'=>'',
			             			'total_pagado'=>$factura->total_pagado-$row->monto
			             		],[
			             			'id'=>$factura->id
			             		]);
		             		}
	             		}
	             	}
	             });
			if($crud->getParameters()=='add'){
				$crud->field_type('anulado','hidden',0);
			}
			$crud = $crud->render();
			$crud->header = new ajax_grocery_crud();
			$crud->header->set_table('facturas_clientes')
						->where('facturas_clientes.id',$cliente)
						->set_subject('Facturas')
						->set_theme('header_data')						
						->set_relation('clientes_id','clientes','nombres')
						->set_url('movimientos/facturas_clientes/facturas_clientes/');
			$crud->header = $crud->header->render(1)->output;
			$crud->header = '<a href="'.base_url('movimientos/facturas_clientes/facturas_clientes_detalles/'.$cliente).'" class="btn btn-info">Administrar Facturas</a>'.$crud->header;
			$crud->title = 'Pagos';			
			$this->loadView($crud);
		}

		function pagos_pagare(){

			$this->form_validation->set_rules('nro_recibo','#Recibo','required')
								  ->set_rules('formapago_id','Forma de Pago','required')
								  ->set_rules('monto','Monto','required')
								  ->set_rules('facturas_clientes','Monto','required')
								  ->set_rules('facturas','Facturas Seleccionadas','required');
			if($this->form_validation->run()){
				$facturas = explode(',',$_POST['facturas']);
				$this->db->select('facturas_clientes_detalle.*, ventas.total_venta as totalcondesc');
				$this->db->join('ventas','ventas.id = facturas_clientes_detalle.venta');
				$this->db->where('facturas_clientes_detalle.pagado',0);
				$this->db->where('facturas_clientes_detalle.anulado',0);
				$this->db->where('facturas_clientes_detalle.saldo_venta >',0);
				$wh = [];
				foreach($facturas as $factura){
					$wh[] = 'facturas_clientes_detalle.id = '.$factura;
				}
				$this->db->where('('.implode(' OR ',$wh).')',NULL,FALSE);

				$ventas = $this->db->get_where('facturas_clientes_detalle');
				$total_pagado = $_POST['monto'];
				$transaccion_id = uniqid();
				foreach($ventas->result() as $venta){
					if($total_pagado>0){
						$total_pagado-= $venta->saldo_venta;
						$saldo = $total_pagado>0?0:abs($total_pagado);
						$this->db->insert('facturas_clientes_pagos_pagare',[
							'facturas_clientes'=>$venta->facturas_clientes,
							'facturas_clientes_detalles'=>$venta->id,
							'transaction_id'=>$transaccion_id,
							'total_pagado'=>$_POST['monto'],
							'fecha'=>date("Y-m-d H:i:s"),
							'nro_recibo'=>$_POST['nro_recibo'],
							'formapago_id'=>$_POST['formapago_id'],
							'concepto'=>$_POST['concepto'],
							'monto'=>$venta->saldo_venta-$saldo,
							'bancos_id'=>$_POST['bancos_id'],
							'cuentas_bancos_id'=>$_POST['cuentas_bancos_id'],
							'nro_cheque'=>$_POST['nro_cheque'],
							'tipo_cheque'=>$_POST['tipo_cheque'],
							'fecha_emision'=>$_POST['fecha_emision'],
							'fecha_pago'=>date("Y-m-d H:i:s"),
							'cajadiaria_id'=>$this->user->cajadiaria,
							'user_id'=>$this->user->id,
							'anulado'=>0,
							'venta'=>$venta->venta,
							'saldo_anterior'=>$venta->saldo_venta,
							'saldo'=>$saldo,
						]);
						$pagado = $saldo==0?1:0;
						$fecha_pagado = $saldo==0?date("Y-m-d H:i:s"):'';
						$this->db->update('facturas_clientes_detalle',[
							'saldo_venta'=>$saldo,
							'pagado'=>$pagado,
							'total_pagado'=>abs($saldo-$venta->total_venta),
							'fecha_pagado'=>$fecha_pagado,
							'total_venta'=>$venta->totalcondesc
						],[
							'id'=>$venta->id
						]);
						if($_POST['formapago_id'] == '3' || $_POST['formapago_id']=='4' || $_POST['formapago_id']=='5'  || $_POST['formapago_id']=='6'){
		                	switch($_POST['formapago_id']){
		                		case '3':
		                		case '4':
		                			$forma = 'Cheque';
		                		break;
		                		case '5':
		                			$forma = 'Deposito';
		                		break;
		                		case '6':
		                			$forma = 'Transferencia';
		                		break;
		                	}		   

		                	$saldo_actual = $this->db->get_where('cuentas_bancos',['id'=>$_POST['cuentas_bancos_id']])->row()->saldo;             

		                    $this->db->insert("libro_banco",[
		                        'sucursales_id'=>$this->user->sucursal,
		                        'cajas_id'=>$this->user->caja,
		                        'cajadiaria_id'=>$this->user->cajadiaria,
		                        'fecha'=>date("Y-m-d H:i:s"),
		                        'saldo_actual'=>$saldo_actual,
		                        'bancos_id'=>$_POST['bancos_id'],
		                        'cuentas_bancos_id'=>$_POST['cuentas_bancos_id'],
		                        'tipo_movimiento'=>1,
		                        'concepto'=>$forma,
		                        'beneficiario'=>$this->db->get_where('cuentas_bancos',['id'=>$_POST['cuentas_bancos_id']])->row()->nro_cuenta,
		                        'monto'=>$_POST['monto'],
		                        'comprobante'=>$primary,
		                        'forma_pago'=>$forma,
		                        'tipo_cheque'=>$_POST['tipo_cheque'],
		                        'nro_cheque'=>$_POST['nro_cheque'],
		                        'fecha_emision'=>date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['fecha_emision']))),
		                        'fecha_pago'=>date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['fecha_pago']))),
		                        'cobrado'=>0,
		                        'anulado'=>0,
		                        'user_id'=>$this->user->id,
		                        'fecha_registro'=>date("Y-m-d H:i:s")
		                    ]);
		                }
						$this->db->query("UPDATE facturas_clientes SET actualizar_saldo = 1,saldo_cliente = get_saldo(clientes_id) WHERE id = ".$venta->facturas_clientes);

					}
				}
				if($ventas->num_rows()>0){
					echo $this->success('Datos almacenados con éxito <a href="'.base_url('reportes/rep/verReportes/190/html/valor/'.$transaccion_id).'" target="_blank" class="btn btn-info">Imprimir recibo</a>');
					return;
				}else{
					echo $this->error('Las facturas seleccionadas no estan pendientes por pagar o su saldo no es superior a 0');
					return;
				}

			}else{
				echo $this->error($this->form_validation->error_string());
			}
		}
	}
?>