<?php

require_once APPPATH.'/controllers/Panel.php';    

class ProductosFrontend extends Main {

    function __construct() {
        parent::__construct();        
    }     

    public function allStock($producto) {   
        $this->db->select("            
            productosucursal.stock,
            productosucursal.vencimiento,
            sucursales.denominacion as sucursal
        ");
        $this->db->join('productos','productos.codigo = productosucursal.producto');
        $this->db->join('sucursales','sucursales.id = productosucursal.sucursal');
        $productos = $this->db->get_where('productosucursal',['producto'=>$producto]);
        ob_start();
        sqltohtml($productos);
        $return = ob_get_contents();
        ob_end_clean();
        return $return;
    }
    
    public function productos($x = '', $y = '') {
        if(!empty($_POST)){
            $this->db->select('
                codigo,
                codigo_interno,
                foto_principal,
                nombre_comercial,
                precio_venta AS _precio_venta,
                precio_venta_mayorista1 AS _precio_venta_mayorista1,
                precio_venta_mayorista2 AS _precio_venta_mayorista2,
                precio_venta_mayorista3 AS _precio_venta_mayorista3,
                CONCAT(FORMAT(precio_venta,0,"de_DE")," ","Gs.") as precio_venta,
                CONCAT(FORMAT(precio_venta_mayorista1,0,"de_DE")," ","Gs.") as precio_venta_mayorista1,
                CONCAT(FORMAT(precio_venta_mayorista2,0,"de_DE")," ","Gs.") as precio_venta_mayorista2,
                CONCAT(FORMAT(precio_venta_mayorista3,0,"de_DE")," ","Gs.") as precio_venta_mayorista3,                
                cant_1,
                cant_2,
                cant_3'
            );
            $ci = !is_numeric($_POST['producto'])?-1:$_POST['producto'];
            $this->db->where("(codigo = '$ci' OR codigo_interno = '$ci' OR codigo2 = '$ci')",NULL,TRUE);
            $this->db->where('(productos.anulado = 0 OR productos.anulado IS NULL)',NULL,TRUE);
            $producto = $this->db->get_where('productos');

            if($producto->num_rows()>0){
                $producto = $producto->row();
                if($producto->cant_1>0 && !empty($_POST['cantidad']) && $_POST['cantidad']>=$producto->cant_1){
                    $producto->_precio_venta = $producto->_precio_venta_mayorista1;
                    $producto->precio_venta = number_format($producto->_precio_venta,0,',','.').' Gs.';
                }
                if($producto->cant_2>0 && !empty($_POST['cantidad']) && $_POST['cantidad']>=$producto->cant_2){
                    $producto->_precio_venta = $producto->_precio_venta_mayorista2;
                    $producto->precio_venta = number_format($producto->_precio_venta,0,',','.').' Gs.';
                }
                if($producto->cant_3>0 && !empty($_POST['cantidad']) && $_POST['cantidad']>=$producto->cant_3){
                    $producto->_precio_venta = $producto->_precio_venta_mayorista3;
                    $producto->precio_venta = number_format($producto->_precio_venta,0,',','.').' Gs.';
                }
                if(is_numeric($_POST['cantidad'])){
                    $producto->_precio_venta*=$_POST['cantidad'];
                    $producto->precio_venta = number_format($producto->_precio_venta,0,',','.').' Gs.';
                }
                $producto->precio_tachado = 0;
                $this->db->order_by('descuento_detalle.id','DESC');
                $this->db->where("(DATE(NOW()) BETWEEN descuento_detalle.desde AND descuento_detalle.hasta)",NULL,TRUE);
                $this->db->where('codigo',$ci);
                $this->db->where('descuentos.estado',1);
                $this->db->where('descuento_detalle.anulado',0);
                $this->db->join('descuentos','descuentos.id = descuento_detalle.descuento_id');
                $descuentos = $this->db->get_where('descuento_detalle');
                if($descuentos->num_rows()>0){
                    $producto->precio_tachado = $producto->precio_venta;
                    $producto->_precio_venta-=$descuentos->row()->precio_desc;
                    $producto->precio_venta = number_format($producto->_precio_venta,0,',','.').' Gs.';

                    $producto->_precio_venta_mayorista1-=$descuentos->row()->precio_desc;
                    $producto->_precio_venta_mayorista2-=$descuentos->row()->precio_desc;
                    $producto->_precio_venta_mayorista3-=$descuentos->row()->precio_desc;

                    $producto->precio_venta_mayorista1 = number_format($producto->_precio_venta_mayorista1,0,',','.').' Gs.';
                    $producto->precio_venta_mayorista2 = number_format($producto->_precio_venta_mayorista2,0,',','.').' Gs.';
                    $producto->precio_venta_mayorista3 = number_format($producto->_precio_venta_mayorista3,0,',','.').' Gs.';
                }
                $producto->precio_dolares = number_format($producto->_precio_venta/$this->ajustes->tasa_dolares,2,',','.');
                $producto->precio_reales = number_format($producto->_precio_venta/$this->ajustes->tasa_reales,2,',','.');
                $producto->precio_pesos = number_format($producto->_precio_venta/$this->ajustes->tasa_pesos,2,',','.');                 
                $producto->foto_principal = empty($producto->foto_principal)?base_url('200x200.png'):base_url('img/productos/'.$producto->foto_principal);
                $producto->stockage = $this->allStock($producto->codigo);                
                echo json_encode($producto);
            }else{
                echo json_encode([]);
            }
            return;
        }
        $this->loadView([
        	'view'=>'panel',
        	'crud'=>'user',
        	'output'=>$this->load->view('consultarProducto',[],TRUE)
        ]);
    }

    public function inventario_modal($sucursal = ''){
        if($sucursal == 'ajax_list_info'){
            echo '{"total_results":100}';
            return;
        }
        $crud = new ajax_grocery_crud();
        $crud->set_table('productosucursal')
             ->set_subject('Inventario')
             ->set_theme('bootstrap')
             ->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_print()
             ->unset_export()
             ->unset_read()             
             ->callback_column('Codigo',function($val,$row){
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
             })
             /*->set_relation('sucursal','sucursales','denominacion')*/;
            $crud->columns('j4b04c546.codigo','j4b04c546.codigo_interno','j4b04c546.nombre_comercial','j4b04c546.nombre_generico','stock','vencimiento','j4b04c546.precio_venta','sucursal');
            $crud->set_relation('productos_id','productos','{codigo}|{nombre_comercial}|{nombre_generico}|{precio_venta}|{codigo_interno}|{foto_principal}');
            //$crud->set_relation('sucursal','sucursales','denominacion');
            $crud->callback_column('scb5fc3d6', function($val, $row) {
                return '<a href="' . base_url('movimientos/productos/inventario/' . $row->sucursal) . '">' . $val . '</a>';
            });
            $crud->callback_column('j4b04c546.codigo', function($val, $row) {
                $val = explode('|',$row->s4b04c546)[0];
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
            });            
            $crud->callback_column('j4b04c546.nombre_comercial', function($val, $row) {
                $cod = explode('|',$row->s4b04c546)[0];
                $val = explode('|',$row->s4b04c546)[1];
                $img = explode('|',$row->s4b04c546)[5];
                $img = empty($img)?base_url('200x200.png'):base_url('img/productos/'.$img);
                return '<a href="javascript:selCod(\''.$cod.'\',\'\',\'\')">'.$val.'</a> <a href="javascript:showImage(\''.$img.'\')"><i class="fa fa-image"></i></a>';
            });            
            $crud->callback_column('j4b04c546.nombre_generico', function($val, $row) {
                return explode('|',$row->s4b04c546)[2];
            });
            $crud->callback_column('j4b04c546.precio_venta', function($val, $row) {                
                $val = explode('|',$row->s4b04c546)[3];
                return number_format((float)$val,0,',','.');
            });
            $crud->callback_column('j4b04c546.codigo_interno', function($val, $row) {
                $val = explode('|',$row->s4b04c546)[4];
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
            });
            $crud->callback_column('stock', function($val, $row) {
                return $val.' <a href="' . base_url('movimientos/productos/inventario/reload/'.$row->producto).'"><i class="fa  fa-refresh "></i></a>';
            });
            $crud->display_as('j4b04c546.codigo','Codigo')
                 ->display_as('j4b04c546.nombre_comercial','Nombre Comercial')
                 ->display_as('j4b04c546.nombre_generico','Nombre Genérico');
        //$crud->set_primary_key('codigo','productos');
        $crud->where("(j4b04c546.anulado IS NULL OR j4b04c546.anulado = '0')",'ESCAPE',TRUE);
        $crud = $crud->render();
        $this->loadView($crud);

    }
}