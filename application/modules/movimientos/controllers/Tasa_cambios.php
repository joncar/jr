<?php

require_once APPPATH.'/controllers/Panel.php';    

class Tasa_cambios extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
        }                
    }

    function divisas($venta = '',$return = false){
        $this->as['divisas'] = 'monedas';
        $crud = $this->crud_function('','');
        $crud = $crud->render();
        $this->loadView($crud);              
    }

    function movimiento_divisas($venta = '',$return = false){
        $crud = $this->crud_function('','');
        $crud->set_theme('bootstrap2_horizontal');
        $crud->field_type('tipo_operacion','dropdown',['1'=>'Compra','-1'=>'Venta'])
             ->set_relation('moneda_entrada','monedas','{denominacion} C. {compra} V. {venta}')
             ->set_relation('moneda_salida','monedas','{denominacion} C. {compra} V. {venta}')             
             ->field_type('user_id','hidden',$this->user->id)
             ->field_type('anulado','hidden',0)
             ->field_type('fecha','hidden',date("Y-m-d H:i:s"))
             ->field_type('cajadiaria_id','hidden',$this->user->cajadiaria);
        if($crud->getParameters()=='list' || $crud->getParameters()=='ajax_list'){
            $crud->set_relation('cajadiaria_id','cajadiaria','caja');
        }
        $crud->callback_after_insert(function($post,$primary){
            $this->db->insert('ajuste_cajadiaria',[
                'sucursales_id'=>$this->user->sucursal,
                'cajas_id'=>$this->user->caja,
                'cajadiaria_id'=>$this->user->cajadiaria,
                'tipo_ajuste'=>1,
                'fecha'=>date("Y-m-d H:i:s"),
                'observacion'=>'Cambio de moneda',
                'monto'=>$post['monto_entrada'],
                'comprobante'=>$primary,
                'tipo_moneda'=>$post['moneda_entrada'],
                'anulado'=>0,
                'user_id'=>$this->user->id
            ]);

            $this->db->insert('ajuste_cajadiaria',[
                'sucursales_id'=>$this->user->sucursal,
                'cajas_id'=>$this->user->caja,
                'cajadiaria_id'=>$this->user->cajadiaria,
                'tipo_ajuste'=>-1,
                'fecha'=>date("Y-m-d H:i:s"),
                'observacion'=>'Cambio de moneda',
                'monto'=>$post['monto_salida'],
                'comprobante'=>$primary,
                'tipo_moneda'=>$post['moneda_salida'],
                'anulado'=>0,
                'user_id'=>$this->user->id
            ]);
        });        
        $crud->where('jd340b3e8.caja',$this->user->caja);
        $crud->unset_delete()
             ->unset_edit();
        $crud->set_lang_string('insert_success_message','Datos almacenados correctamente <script>window.open("'.base_url('reportes/rep/verReportes/224/html/valor').'/{id}")</script>');
        $crud = $crud->render();
        $crud->output.= $this->load->view('movimientos_divisas',[],TRUE);
        $this->loadView($crud);              
    }

    
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
