<?php

require_once APPPATH.'/controllers/Panel.php';    

class ActualizacionPrecios extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }  
    
    public function actualizador_precio($x = '', $y = '') {
        if($x=='updateTemp' && !empty($_POST['productos'])){
            $this->updateTemp();
            return;
        }

        if($x=='getTemp'){
            $this->getTemp();
            return;
        }

        if($x=='cleanTemp'){
            $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'actualizador_precio']);       
            return;
        }
        $crud = is_object($x)?$x:parent::crud_function($x, $y);
        $crud->set_theme('bootstrap');
        $crud->unset_read();

        $crud->columns('id','fecha', 'usuario');        
        $crud->order_by('id','DESC');        
        ///Validation
        $crud->unset_delete();
        if($crud->getParameters()=='add'){
            $crud->field_type('productos','hidden')
                 ->field_type('usuario','string',$this->user->nombre.' '.$this->user->apellido)
                 ->field_type('anulado','hidden')
                 ->field_type('fecha','string',date("d-m-Y H:i:s"));
        }else{
            $crud->field_type('productos','hidden');
        }
        $crud->replace_form_add('cruds/actualizacion_precios/add','movimientos');
        $crud->replace_form_edit('cruds/actualizacion_precios/edit','movimientos');
        //$crud->set_rules('productos','Productos','required|callback_unProducto|callback_validar');                        
        $crud->callback_before_insert(array($this,'validarFacturacion'));
        $crud->callback_after_insert(array($this,'addBody'));                
        $crud->callback_after_delete(function($primary){get_instance()->db->delete('actualizador_precio_detalle',array('presupuesto_id'=>$primary));});        
        $crud->unset_back_to_list();                
        $output = $crud->render();
        $this->loadView($output);     
    }
    //Callback para validar si la venta almacena el id de factura o no
    function validarFacturacion($post){        
        $post['fecha'] = date("Y-m-d H:i:s");
        $post['anulado'] = 0;
        $post['usuario'] = $this->user->id;

        return $post;
    }
    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();        
        foreach($productos as $p){
            $producto = $this->db->get_where('productos',array('codigo'=>$p->codigo))->row();
            $p->preciocostoant = $producto->precio_costo;
            $p->precioventaant = $producto->precio_venta;
            $this->db->insert('actualizador_precio_detalle',array(
                'actualizador_precio_id'=>$primary,
                'productos_id'=>$p->id,                
                'precio_costo'=>$p->precio_costo,                
                'precio_venta'=>$p->precio_venta,  
                'ventamayorista1'=>$p->precio_venta_mayorista1,              
                'ventamayorista2'=>$p->precio_venta_mayorista2,
                'ventamayorista3'=>$p->precio_venta_mayorista3,
                'cant_1'=>$p->cant_1,              
                'cant_2'=>$p->cant_2,
                'cant_3'=>$p->cant_3,
                'precio_costo_anterior'=>$p->preciocostoant,
                'porc_venta'=>$p->porc_venta,
                'porc_mayorista1'=>$p->porc_mayorista1,
                'porc_mayorista2'=>$p->porc_mayorista2,
                'porc_mayorista3'=>$p->porc_mayorista3,
                'fecha_registro'=>date("Y-m-d H:i:s")
            ));
            if($this->ajustes->precioxsucursal!=1){
                $this->db->update('productos',[
                    'precio_costo'=>$p->precio_costo,                
                    'precio_venta'=>$p->precio_venta,  
                    'precio_venta_mayorista1'=>$p->precio_venta_mayorista1,              
                    'precio_venta_mayorista2'=>$p->precio_venta_mayorista2,
                    'precio_venta_mayorista3'=>$p->precio_venta_mayorista3,
                    'cant_1'=>$p->cant_1,
                    'cant_2'=>$p->cant_2,
                    'cant_3'=>$p->cant_3,
                    'fecha_mod_precio'=>date("Y-m-d H:i:s"),
                    'precio_costo_ant'=>$p->preciocostoant,
                    'precio_venta_ant'=>$p->precioventaant,
                    'porc_venta'=>$p->porc_venta,
                    'porc_mayorista1'=>$p->porc_mayorista1,
                    'porc_mayorista2'=>$p->porc_mayorista2,
                    'porc_mayorista3'=>$p->porc_mayorista3
                ],['id'=>$p->id]);
            }
            else{
                $this->db->update('productosucursal',[
                    'precio_venta'=>$p->precio_venta,
                    'precio_costo'=>$p->precio_costo,
                    'cant_1'=>$p->cant_1,
                    'cant_2'=>$p->cant_2,
                    'cant_3'=>$p->cant_3,
                    'precio_venta_mayorista1'=>$p->precio_venta_mayorista1,
                    'precio_venta_mayorista2'=>$p->precio_venta_mayorista2,
                    'precio_venta_mayorista3'=>$p->precio_venta_mayorista3,
                ],['sucursal'=>$this->user->sucursal,'productos_id'=>$p->id]);
            }
        }
        $this->db->update('actualizador_precio',array('productos'=>implode($pro)),array('id'=>$primary));  
        $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'actualizador_precio']);             
    }

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }

    function validar(){ 
        $productos = $_POST['productos'];       
        $ajustes = $this->ajustes;                
        return true;
    }

    function updateTemp(){
        $productos = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'actualizador_precio']);
        if($productos->num_rows()==0){
            $this->db->insert('ventatemp',['productos'=>$_POST['productos'],'cajas_id'=>$this->user->caja,'user_id'=>$this->user->id,'tipo'=>'actualizador_precio']);
        }else{
            $this->db->update('ventatemp',['productos'=>$_POST['productos'],'user_id'=>$this->user->id],['cajas_id'=>$this->user->caja,'tipo'=>'actualizador_precio']);
        }
        echo 'success';
    }

    function getTemp(){
        $productos = [];
        $pp = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'actualizador_precio']);
        if($pp->num_rows()>0){
            $productos = json_decode($pp->row()->productos);
        }
        echo json_encode($productos);
    }

}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
