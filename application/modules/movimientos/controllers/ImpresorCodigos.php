<?php

require_once APPPATH.'/controllers/Panel.php';    

class ImpresorCodigos extends Main {

    function __construct() {
        parent::__construct();        
    }     

    public function allStock($producto) {   
        $this->db->select("            
            productosucursal.stock,
            productosucursal.vencimiento,
            sucursales.denominacion as sucursal
        ");
        $this->db->join('productos','productos.codigo = productosucursal.producto');
        $this->db->join('sucursales','sucursales.id = productosucursal.sucursal');
        $productos = $this->db->get_where('productosucursal',['producto'=>$producto]);
        ob_start();
        sqltohtml($productos);
        $return = ob_get_contents();
        ob_end_clean();
        return $return;
    }
    
    public function productos($x = '', $y = '') {
        if(!empty($_POST)){
            $_POST['cantidad'] = 1;
            $this->db->select('
                id,
                codigo,
                codigo_interno,
                foto_principal,
                nombre_comercial,                
                precio_venta AS _precio_venta,
                precio_venta_mayorista1 AS _precio_venta_mayorista1,
                precio_venta_mayorista2 AS _precio_venta_mayorista2,                
                precio_venta_mayorista3 AS _precio_venta_mayorista3,                
                CONCAT(FORMAT(precio_venta,0,"de_DE")," ","Gs.") as precio_venta,
                CONCAT(FORMAT(precio_venta_mayorista1,0,"de_DE")," ","Gs.") as precio_venta_mayorista1,
                CONCAT(FORMAT(precio_venta_mayorista2,0,"de_DE")," ","Gs.") as precio_venta_mayorista2,
                CONCAT(FORMAT(precio_venta_mayorista3,0,"de_DE")," ","Gs.") as precio_venta_mayorista3,            
                cant_1,
                cant_2,
                cant_3'
            );
            $this->db->where('codigo',$_POST['producto']);
            $ci = !is_numeric($_POST['producto'])?-1:$_POST['producto'];
            $this->db->or_where('codigo_interno',$_POST['producto']);
            $this->db->where('(productos.anulado = 0 OR productos.anulado IS NULL)',NULL,TRUE);
            $producto = $this->db->get_where('productos');

            if($producto->num_rows()>0){
                $producto = $producto->row();                
                if(is_numeric($_POST['cantidad'])){
                    $producto->_precio_venta*=$_POST['cantidad'];
                    $producto->precio_venta = number_format($producto->_precio_venta,0,',','.').' Gs.';

                    $producto->_precio_venta_mayorista1*=$_POST['cantidad'];
                    $producto->precio_venta_mayorista1 = number_format($producto->_precio_venta_mayorista1,0,',','.').' Gs.';

                    $producto->_precio_venta_mayorista2*=$_POST['cantidad'];
                    $producto->precio_venta_mayorista2 = number_format($producto->_precio_venta_mayorista2,0,',','.').' Gs.';

                    $producto->_precio_venta_mayorista3*=$_POST['cantidad'];
                    $producto->precio_venta_mayorista3 = number_format($producto->_precio_venta_mayorista3,0,',','.').' Gs.';
                    

                    $producto->cant_elegida = '';
                    if($producto->cant_elegida == '' && $_POST['cantidad']<=$producto->cant_1){
                        $producto->cant_elegida = 'unidad';
                    }

                    if($producto->cant_elegida == '' && $_POST['cantidad']<=$producto->cant_2){
                        $producto->cant_elegida = 'mayorista1';
                    }

                    if($producto->cant_elegida == '' && $_POST['cantidad']<=$producto->cant_3){
                        $producto->cant_elegida = 'mayorista2';
                    }

                    if($producto->cant_elegida == '' && $_POST['cantidad']>$producto->cant_3){
                        $producto->cant_elegida = 'mayorista3';
                    }
                }
                /*$producto->precio_dolares = number_format($producto->_precio_venta/$this->ajustes->tasa_dolares,2,',','.');
                $producto->precio_reales = number_format($producto->_precio_venta/$this->ajustes->tasa_reales,2,',','.');
                $producto->precio_pesos = number_format($producto->_precio_venta/$this->ajustes->tasa_pesos,2,',','.');                 
                $producto->foto_principal = empty($producto->foto_principal)?base_url('200x200.png'):base_url('img/productos/'.$producto->foto_principal);
                $producto->stockage = $this->allStock($producto->codigo);                */
                echo json_encode($producto);
            }else{
                echo json_encode([]);
            }
            return;
        }
        $this->loadView([
        	'view'=>'panel',
        	'crud'=>'user',
        	'output'=>$this->load->view('impresorCodigos',[],TRUE)
        ]);
    }

    public function inventario($x = '', $y = '') {
        $crud = new ajax_grocery_crud();
        $crud->set_table('productosucursal')
             ->set_theme('bootstrap')
             ->set_subject('Inventario');
        $crud->set_theme('bootstrap');
        $crud->unset_read()
             ->unset_export()
             ->unset_print()
             ->unset_edit()
             ->unset_delete()
             ->unset_add();     
        $crud->columns('j4b04c546.codigo','j4b04c546.nombre_comercial','stock','j4b04c546.precio_costo','j4b04c546.precio_min_unidad','j4b04c546.precio_min_caja','j4b04c546.precio_min_cant','j4b04c546.proveedor_id','sucursal');
        $crud->set_relation('productos_id','productos','{codigo}|{nombre_comercial}|{precio_costo}|{precio_min_unidad}|{precio_min_caja}|{precio_min_cant}');        
        $crud->set_relation('j4b04c546.proveedor_id','proveedores','denominacion');        
        $crud->callback_column('j4b04c546.codigo', function($val, $row) {
            return explode('|',$row->s4b04c546)[0];
        });
        $crud->callback_column('j4b04c546.nombre_comercial', function($val, $row) {
            return explode('|',$row->s4b04c546)[1];
        });  
        $crud->callback_column('j4b04c546.precio_costo', function($val, $row) {
            return explode('|',$row->s4b04c546)[2];
        });  
        $crud->callback_column('j4b04c546.precio_min_unidad', function($val, $row) {
            return explode('|',$row->s4b04c546)[3];
        });  
        $crud->callback_column('j4b04c546.precio_min_caja', function($val, $row) {
            return explode('|',$row->s4b04c546)[4];
        });  
        $crud->callback_column('j4b04c546.precio_min_cant', function($val, $row) {
            return explode('|',$row->s4b04c546)[5];
        });
        $crud->display_as('j4b04c546.codigo','Codigo')
             ->display_as('j4b04c546.nombre_comercial','Nombre Comercial')
             ->display_as('j4b04c546.precio_costo','Precio costo')
             ->display_as('j4b04c546.precio_min_unidad','Precio min unidad')
             ->display_as('j4b04c546.precio_min_caja','Precio min caja')
             ->display_as('j4b04c546.precio_min_cant','Precio min cant')
             ->display_as('j4b04c546.proveedor_id','Proveedor');        
        if(!empty($_POST['codigo'])){
            $crud->where('j4b04c546.codigo',$_POST['codigo']);
        }
        $output = $crud->render();
        $this->loadView([
            'view'=>'panel',
            'crud'=>'user',
            'js_files'=>$output->js_files,
            'output'=>$this->load->view('inventario_frontend',['output'=>$output->output],TRUE)
        ]);
    }
}