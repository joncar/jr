<?php

require_once APPPATH.'/controllers/Panel.php';    

class Ventas extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }
        
     function anular_venta(){
        $this->form_validation->set_rules('id','ID','required|integer');
        if($this->form_validation->run()){
            $detalles = $this->db->get_where('ventadetalle',array('venta'=>$this->input->post('id')));                                
            $this->db->update('ventas',array('status'=>-1),array('id'=>$this->input->post('id')));                
            $cliente = $this->db->get_where('ventas',['id'=>$this->input->post('id')])->row()->cliente;
            $this->db->query("UPDATE facturas_clientes SET actualizar_saldo = 1,saldo_cliente = get_saldo(clientes_id) WHERE clientes_id = $cliente");              
            echo $this->success('factura anulada con exito').refresh_list();

            $venta = $this->db->get_where('ventas',['id'=>$this->input->post('id')]);
            if($venta->num_rows()>0){
                $venta = $venta->row();
                if(is_numeric($venta->orden_trabajo_id)){
                    $this->db->update('orden_trabajo',['estado'=>0],['id'=>$venta->orden_trabajo_id]);
                }
            }
        }
        else echo $this->error('Ha ocurrido un error');
    }
    
    public function ventas($x = '', $y = '', $z = '') { 
        if($x=='nroFactura'){
            echo $this->querys->get_nro_factura();
            die();
        }       
        if($x=='anular'){
            $this->anular_venta();
            die();
        }
        if($x=='updateTemp' && !empty($_POST['productos'])){
            $this->updateTemp();
            return;
        }

        if($x=='getTemp'){
            $this->getTemp();
            return;
        }

        if($x=='cleanTemp'){
            $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'ventas']);       
            return;
        }

        if($x=='hasCupones'){
            $cupones = $this->db->get_where('cupones',['ventas_id'=>$y]);
            echo json_encode(['print'=>$cupones->num_rows()]);
            return;
        }

        if($x=='kuatia'){
            $this->load->library('Kuatia');
            $this->kuatia->generarFactura($y);
            return;
        }

        
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap');
        $crud->unset_read()
             ->unset_delete();
        
        if(!empty($_POST['ventas_id'])){            
            if(is_numeric($_POST['ventas_id'])){
                $crud->where('ventas.id = '.$_POST['ventas_id'],'ESCAPE',FALSE);
            }else{
                $crud->where('ventas.id',-1);
            }
        }
        $crud->columns('id','transaccion', 'cliente', 'fecha', 'caja', 'ventas.nro_factura', 'total_venta', 'status');
        $crud->callback_column('ventas.nro_factura', function($val, $row) {
            return '<a href="javascript:showDetail('.$row->id.')">'.$row->nro_factura.'</a>';
        });
        $crud->callback_column('observacion',function($val,$row){
            return 'a';
        });
        $crud->callback_column('total_venta',function($val){
            return round($val,0);
        });
        $crud->callback_column('status', function($val, $row) {
            switch ($val) {
                case '0':return $this->user->allow_function('anular_venta')?'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="fa fa-times-circle"></i></a>':'Activa';
                    break;
                case '-1':return 'Anulada';
                    break;
                case '-2': return 'Anulada y procesada';
                    break;
            }
        });
        if($this->user->admin!=1){
            $crud->unset_edit();
        }
        $crud->set_relation('transaccion', 'tipotransaccion', 'denominacion')
             ->set_relation('cliente', 'clientes', '{nombres} {apellidos}')
             ->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->where('ventas.sucursal', $_SESSION['sucursal']);
        if (!empty($_SESSION['caja']) && empty($_POST['ventas_id'])){
            $crud->where('ventas.caja', $_SESSION['caja']);
        }
        
        $crud->order_by('id','DESC');
        
        ///Validations
        $crud->set_rules('total_venta','Total de venta','required|numeric|greater_than[0]');        
        $crud->set_rules('productos','Productos','required|callback_unProducto|callback_validar');                
        $crud->set_rules('nro_factura','Numero de factura','required');
        $crud->set_rules('forma_pago','Forma de pago','required');
        $crud->set_rules('transaccion','Tipo de transacción','required');
        $crud->set_rules('cliente','Cliente','required|numeric|greater_than[0]');
        
        $crud->callback_before_insert(array($this,'validarFacturacion'));
        $crud->callback_after_insert(array($this,'addBody'));
        $crud->callback_after_update(function($post,$primary){
            $this->db->update('facturas_clientes_detalle',['pagare'=>$post['pagare']],['venta'=>$primary]);
        });
        foreach($this->db->get_where('tipo_facturacion')->result() as $t){
            $crud->add_action($t->denominacion,'',base_url($t->reporte).'/');        
        }
        $crud->add_action('Reenviar FE','',base_url('movimientos/ventas/ventas/kuatia').'/');
        //$crud->add_action('Creditos','',base_url('movimientos/creditos/creditos').'/');        
        //$crud->add_action('Cheques','',base_url('movimientos/ventas/cheques_a_cobrar').'/');
        $crud->unset_back_to_list();
        $crud->display_as('ventas.nro_factura','#Factura');
        $crud->replace_form_edit('ventas-edit.php','movimientos');      
        if($crud->getParameters()=='edit'){
            $crud->field_type('nro_factura','readonly')
                 ->field_type('total_venta','readonly')
                 ->field_type('fecha','readonly')
                 ->edit_fields('transaccion','pagare','plan','tipo_facturacion_id','cliente','fecha','nro_factura','total_venta','usuario');
            if(in_array(3,$this->user->getGroupsArray())){
                $crud->field_type('usuario','hidden',$this->user->id);
            }else{
                $crud->set_relation('usuario','user','{nombre} {apellido}');
            }
        }  
        $output = $crud->render();   
        if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
            $edit = '';
            $default = array();
            if($crud->getParameters()=='edit'){
                $edit = $y;                
                //$output->output = $this->load->view('ventas-edit',array('output'=>$output->output,'edit'=>$edit,'default'=>$default),TRUE);                
            }else{
                if(is_numeric($z) && is_string($y)){
                    $default = $this->querys->{'get'.$y}($z);
                    $default->sucursal = $this->user->sucursal;
                    $default->caja = $this->user->caja;
                    $default->cajadiaria = $this->user->cajadiaria;
                }
                $output->output = $this->load->view('ventas',array('output'=>$output->output,'edit'=>$edit,'default'=>$default),TRUE);
            }
        }else{
            $output->output = $this->load->view('ventas_list',array('output'=>$output->output),TRUE);
        }
        $this->loadView($output);     
    }

    public function ventadetalle($x = '', $y = '') {         
        $crud = parent::crud_function($x, $y);
        if(!empty($_POST['venta'])){
            $crud->where('venta',$_POST['venta']);
        }
        $crud->callback_column('producto',function($val,$row){
            $producto = $this->db->get_where('productos',array('codigo'=>$val))->row();            
            $producto->remitidos = get_instance()->db->query("SELECT IFNULL(SUM(cantidad),0) as total FROM nota_remision_detalle INNER JOIN nota_remision ON nota_remision.id = nota_remision_detalle.nota_remision_id WHERE nota_remision.venta = '{$row->venta}' AND producto = '$val'")->row()->total;
            $producto->disponibles = $row->cantidad - $producto->remitidos;
            return $producto;
        });        
        $crud->unset_add()->unset_edit()->unset_delete();
        $output = $crud->render();        
        $this->loadView($output);     
    }
    //Callback para validar si la venta almacena el id de factura o no
    function validarFacturacion($post){
        $tipoFacturacion = $this->db->get_where('tipo_facturacion',array('id'=>$post['tipo_facturacion_id']));
        if($tipoFacturacion->num_rows()==0 || $tipoFacturacion->row()->emite_factura==0){
            $post['nro_factura'] = 0;
        }else{
            $post['nro_factura'] = $this->querys->get_nro_factura();
            $post['timbrados_id'] = get_instance()->db->get_where('cajadiaria',['id'=>get_instance()->user->cajadiaria])->row()->timbrados_id;
        }
        $post['pagare'] = !isset($post['pagare'])?0:$post['pagare'];
        $post['fecha'] = date("Y-m-d H:i:s");
        return $post;
    }
    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        //$this->db->delete('ventadetalle',array('venta'=>$primary));
        foreach($productos as $p){            
            $comision = 0;
            $p->producto = @$this->db->get_where('productos',array('codigo'=>$p->codigo))->row();
            $p->preciocosto = !empty($p->producto->precio_costo)?$p->producto->precio_costo:0;
            $categoria = @$this->db->get_where('categoriaproducto',array('id'=>$p->producto->categoria_id));
            if($p->producto->porc_comision>0){
                $comision = $p->total*$p->producto->porc_comision/100;
            }else{
                $comision = $categoria->num_rows()>0?$p->total*$categoria->row()->porc_comision/100:0;
            }           
            $iva = 0;            
            switch($p->producto->iva_id){                
                case 5:
                $iva = $p->total/21;
                break;
                case 10:
                $iva = $p->total/11;
                break;
            }
            $iva5 = 0;
            $iva10 = 0;
            $exenta = 0;          
            $pro[] = $p->producto->nombre_comercial;  
            $p->producto->iva_id = empty($p->producto->iva_id)?0:$p->producto->iva_id;
            $p->por_desc = empty($p->por_desc)?0:$p->por_desc;
            $vencimiento = empty($p->vencimiento)?'0000-00-00':$p->vencimiento;
            $this->db->insert('ventadetalle',array(
                'venta'=>$primary,
                'producto'=>$p->codigo,
                'lote'=>'',
                'cantidad'=>$p->cantidad,
                'pordesc'=>$p->por_desc,
                'preciocosto'=>$p->preciocosto,
                'precioventa'=>$p->precio_venta,
                'precioventadesc'=>$p->precio_venta,
                'precio_descuento'=>$p->precio_descuento,
                'totaldescuento'=>$p->precio_descuento*$p->cantidad,
                'totalsindesc'=>0,
                'totalcondesc'=>$p->total,
                'iva'=>$p->producto->iva_id,
                'total_iva'=>$iva,
                'inventariable'=>0,
                'comision'=>$comision,
                'vencimiento'=>$vencimiento,
                'stock_anterior'=>@$this->db->get_where('productosucursal',['sucursal'=>$this->user->sucursal,'producto'=>$p->codigo])->row()->stock
            ));
            $exenta+= $p->producto->iva_id == 0?$iva:0;
            $iva5+= $p->producto->iva_id == 5?$iva:0;
            $iva10+= $p->producto->iva_id == 10?$iva:0;
        }
        $this->db->update('ventas',array('productos'=>implode($pro),'total_iva'=>($iva5+$iva10),'exenta'=>$exenta,'iva'=>$iva5,'iva2'=>$iva10),array('id'=>$primary));
        //Actualizar correlativo
        $tipoFacturacion = $this->db->get_where('tipo_facturacion',array('id'=>$post['tipo_facturacion_id']));

        if($tipoFacturacion->num_rows()>0 && $tipoFacturacion->row()->emite_factura==1){
            $caja = $this->db->get_where('cajadiaria',array('caja'=>$_SESSION['caja'],'abierto'=>1));
            $correlativo = $caja->row()->correlativo+1;
            $this->db->update('cajadiaria',array('correlativo'=>$correlativo),array('caja'=>$_SESSION['caja'],'abierto'=>1));
            if($post['transaccion']==1 || $post['plan']==0){
                $this->load->library('Kuatia');
                $this->kuatia->generarFactura($primary);
            }
        }

        //Guardar cheques
        $cheques = json_decode($_POST['cheques']);
        if(!empty($cheques)){
            foreach($cheques as $ch){
                $ch = (array)$ch;
                $this->db->insert('cheques_a_cobrar',array(
                    'ventas_id'=>$primary,
                    'nro_cheque'=>$ch['cheque'],
                    'bancos_id'=>$ch['bancos_id'],
                    'monto'=>$ch['monto'],
                    'vencimiento'=>$ch['vencimiento'],
                    'cruzado'=>$ch['cruzado']
                ));
            }
        }

        //Si es a credito se añade la factura en facturacion_clientes_detalles
        if($post['transaccion']==2 && $post['plan']==0){
            $cliente = $this->db->get_where('facturas_clientes',['clientes_id'=>$post['cliente']]);
            if($cliente->num_rows()==0){
                $this->db->insert('facturas_clientes',['clientes_id'=>$post['cliente'],'saldo_cliente'=>0]);
                $cliente = $this->db->insert_id();
            }else{
                $cliente = $cliente->row()->id;
            }
            $pagare = !isset($post['pagare'])?0:$post['pagare'];
            $this->db->insert("facturas_clientes_detalle",[
                "facturas_clientes"=>$cliente,
                'venta'=>$primary,
                'saldo_venta'=>$post['total_venta'],
                'pagado'=>0,
                'pagare'=>$pagare
            ]);
            //$this->db->query("CALL ajustSaldoFromVentaCredito($cliente)");
            //$this->db->update("facturas_clientes",['actualizar_saldo'=>1],['id'=>$cliente]);
            $this->db->query("UPDATE facturas_clientes SET actualizar_saldo = 1,saldo_cliente = get_saldo(clientes_id) WHERE id = $cliente");
        }

        $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'ventas']);   

        if(!empty($_POST['orden_trabajo_id'])){
            $this->querys->successAddOrdenTrabajo($post,$primary);
        }
        if(!empty($_POST['sport_canchas_id'])){
            $this->querys->successAddCancha($post,$primary);
        }
    }

    function getFactura($x = '',$y = ''){
        if ($x == 'imprimir' || $x=='imprimir2' || $x == 'imprimirticket') {
            if (!empty($y) && is_numeric($y)) {
                $this->load->library('enletras');        
                $this->db->select('ventas.*, sucursales.denominacion, sucursales.telefono, sucursales.direccion, cajas.denominacion as caja, clientes.nombres as clientename, clientes.apellidos as clienteadress');
                $this->db->join('clientes', 'clientes.id = ventas.cliente');
                $this->db->join('cajas', 'cajas.id = ventas.caja');
                $this->db->join('sucursales', 'sucursales.id = ventas.sucursal');
                $venta = $this->db->get_where('ventas', array('ventas.id' => $y));
                if ($venta->num_rows() > 0) {
                    $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
                    if ($x == 'imprimir' || $x=="imprimir2" || $x=="imprimirticket") {
                        $view = $x=="imprimir"?"imprimir_factura":"imprimir_factura_linea";
                        $margen = $x=="imprimir"?array(5, 1, 5, 8):array(2, 5, 5, 8);
                        $papel = 'L';
                        if($x=="imprimir" || $x=="imprimir2"){
                            if(!isset($_GET['pdf']) || $_GET['pdf']!=0){
                                ob_clean();
                                $this->load->library('html2pdf/html2pdf');
                                $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', $margen);
                                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/'.$view, array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                                $html2pdf->Output('Factura Legal.pdf');
                            }else{
                                $this->load->view('reportes/'.$view, array('venta' => $venta->row(), 'detalles' => $detalles));
                            }
                        } else {
                            $output = $this->load->view('reportes/imprimirTicket', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
                            echo $output;
                        }
                    } else {
                        echo "Factura no encontrada";
                    }
                } else {
                    echo 'Factura no encontrada';
                }
            }
        }
    }

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }

    function validar(){ 
        $productos = $_POST['productos'];       
        $ajustes = $this->ajustes;
        $pagoGs = 0;
        if(!empty($_POST['pago_guaranies'])){
            $pagoGs+= $_POST['pago_guaranies'];
        }
        if(!empty($_POST['pago_dolares'])){
            $pagoGs+= $_POST['pago_dolares']*$ajustes->tasa_dolares;
        }
        if(!empty($_POST['pago_reales'])){
            $pagoGs+= $_POST['pago_reales']*$ajustes->tasa_reales;
        }
        if(!empty($_POST['pago_pesos'])){
            $pagoGs+= $_POST['pago_pesos']*$ajustes->tasa_pesos;
        }
        if(!empty($_POST['total_debito'])){
            $pagoGs+= $_POST['total_debito'];
        }

        if(!empty($_POST['total_cheque'])){
            $pagoGs+= $_POST['total_cheque'];
        }

        if($_POST['total_venta']>$pagoGs && $_POST['transaccion']==1){
            $this->form_validation->set_message('validar','Debe pagar la totalidad de la factura');        
            return false;
        }   

        if($_POST['transaccion']==2 && $_POST['cliente']==1){
            $this->form_validation->set_message('validar','Debe indicar a que cliente desea asignarle esta venta a crédito');        
            return false;
        }    

        //Validar limite de crédito
        if($_POST['transaccion']==2){
        	$cliente = $this->db->get_where('clientes',array('id'=>$_POST['cliente']));
        	if($cliente->num_rows()>0){
        		$cliente = $cliente->row();
        		$limiteCredito = !empty($cliente->limite_credito)?$cliente->limite_credito:0;
        		$saldo = $this->elements->get_saldo($_POST['cliente']);
        		if($limiteCredito>0 && ($saldo+$_POST['total_venta'])>$limiteCredito){
        			$this->form_validation->set_message('validar','El cliente excede el limite de crédito establecido');        
        			return false;
        		}
        	}else{
        		$this->form_validation->set_message('validar','El cliente no existe');        
        		return false;
        	}
        }  

        $caja = $this->db->get_where('cajadiaria',['id'=>$this->user->cajadiaria]);
        if($caja->num_rows()==0 || $caja->row()->id!=$_POST['cajadiaria'] || $caja->row()->abierto==0){
            $this->form_validation->set_message('validar','<p>Código de Caja diaria no permitido, Por favor <a href="'.base_url('panel/selcajadiaria').'">Selecciona la caja diaria correcta</a></p>');
            return false;
        }

        //Validar stocks
        $productos = json_decode($_POST['productos']);        
        $return = true;
        if($this->ajustes->vender_sin_stock==0){
            $msj = '';
            foreach($productos as $p){                
                $pro = $this->db->get_where('productos',array('codigo'=>$p->codigo));
                if($pro->num_rows()==0){
                    $return = false;
                    $msj.= '<p>El producto '.$p->codigo.' no existe</p>';
                }else{
                    $pro = $pro->row();
                    if($pro->inventariable==1){
                        $producto = $this->db->get_where('productosucursal',array('producto'=>$p->codigo,'sucursal'=>$this->user->sucursal));
                        if($producto->num_rows()==0 || $producto->row()->stock<$p->cantidad){
                            $return = false;
                            $msj.= '<p>El producto '.$p->codigo.' no tiene stock suficiente</p>';
                        }
                    }
                }
            }
            if(!$return){
                $this->form_validation->set_message('validar',$msj);
            }
        }

        //Validar numero de items
        $tipoFacturacion = $this->db->get_where('tipo_facturacion',['id'=>$_POST['tipo_facturacion_id'],'max_items >'=>-1]);
        if($tipoFacturacion->num_rows()>0 && count($productos)>$tipoFacturacion->row()->max_items){
            $this->form_validation->set_message('validar','La cantidad de items excede del máximo permitido para este tipo de facturación, por favor elimine los items excedentes y registrelos en una nueva facturación [Límite Permitido: '.$tipoFacturacion->row()->max_items.']');
            return false;
        }

         //Validar numero de factura
        $timbrado = get_instance()->db->get_where('cajadiaria',['id'=>get_instance()->user->cajadiaria])->row()->timbrados_id;
        $tipoFacturacion = $this->db->get_where('ventas',['timbrados_id'=>$timbrado,'nro_factura'=>$_POST['nro_factura']]);
        if($tipoFacturacion->num_rows()>0){
            $this->form_validation->set_message('validar','El numero de factura ya fue registrado, actualizado el nro intente guardar nuevamente. <script>window.venta.reloadNroFactura();</script>');
            return false;
        }
        return $return;
    }

    function ventas_detail($ventaid){
        $this->as['ventas_detail'] = 'ventadetalle';
        $crud = $this->crud_function('','');
        $crud->where('venta',$ventaid);
        $crud->columns('producto','cantidad','totalcondesc');
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$row->producto))->roW()->nombre_comercial;
        }); 
        $crud->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_export()
             ->unset_print()
             ->unset_read();
        $crud->staticShowList = true;
        $crud = $crud->render();
        echo $crud->output;

    }
    
    function next_nro_factura(){
        echo $this->querys->get_nro_factura();
    }
    /* Cruds */

    public function notas_credito($x = '', $y = '') {
        
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'compras';
        if ($x == 'add')
            $output->output = $this->load->view('notas_credito', null, TRUE);
        if ($x == 'edit') {
            $compra = $this->db->get_where('notas_credito', array('id' => $y));
            if ($compra->num_rows() > 0) {
                $compra = $compra->row();
                $output->output = $this->load->view('notas_credito', array('nota' => $compra, 'detalles' => $this->db->get_where('notas_credito_detalles', array('nota_credito' => $y))), TRUE);
            }
        }

        $this->loadView($output);
    }

    public function notas_credito_cliente($x = '', $y = '') {
        if($x=='anular' && is_numeric($y)){
            //$this->db->update('notas_credito_cliente',array('anulado'=>1),array('id'=>$y));
            $this->anular_nota($y);
            //redirect('movimientos/ventas/notas_credito_cliente/success');
            echo $this->success('Anulado realizado con éxito');
            die();
        }
        if($x=='getNroNota'){
            echo $this->querys->getNroNota();
            die();
        }
        if($x=='kuatia'){
            $this->load->library('Kuatia');
            $this->kuatia->generarFacturaNCR($y);
            die();
        }
        $crud = parent::crud_function($x, $y);
        $crud->set_subject('notas de crédito');
        $crud->set_relation('venta','ventas','nro_factura');
        $crud->unset_delete();
        $crud->set_rules('productos','Productos','required|callback_unProducto|callback_notaCreditoDuplicar');
        if($crud->getParameters()=='add'){
            //$crud->set_rules('nro_nota_credito','#Nota','required|is_unique[notas_credito_cliente.nro_nota_credito]');
        }
        if($crud->getParameters()=='edit'){
            $crud->unset_fields('productos');
        }
        $crud->columns('id','nro_nota_credito','fecha','cliente','total_monto','actualizar_stock','productos','anulado');
        $crud->set_relation('cliente','clientes','nombres');

        $crud->callback_column('anulado', function($val, $row) {
            switch ($val) {
                case '0':return $this->user->admin==1?'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="fa fa-times-circle"></i></a>':'Activa';
                    break;
                case '1':return 'Anulada';
                    break;
            }
        });
        

        $crud->callback_before_insert(function($post){
            $venta = get_instance()->db->get_where('ventas',['id'=>$post['venta']]);
            if($venta->num_rows()>0){
                if($venta->row()->tipo_facturacion_id=='2' || $venta->row()->tipo_facturacion_id=='4'){
                    $post['nro_nota_credito'] = $this->querys->getNroNota();
                }else{
                    $post['nro_nota_credito'] = 0;
                }
            }            
            $post['timbrados_id'] = get_instance()->db->get_where('cajadiaria',['id'=>get_instance()->user->cajadiaria])->row()->timbrados_id;
            return $post;
        });
        $crud->callback_after_insert(function($post,$primary){
            $this->db = get_instance()->db;
            $productos = json_decode($_POST['productos']);
            $pro = array();            
            foreach($productos as $p){
                $pro[] = $p->producto->nombre_comercial;
                $this->db->insert('notas_credito_cliente_detalle',array(                    
                    'nota_credito'=>$primary,
                    'producto'=>$p->producto->id,
                    'lote'=>'',
                    'cantidad'=>$p->cantidad,
                    'precio_venta'=>$p->precioventa,
                    'total'=>$p->precioventa * $p->cantidad
                ));
            }
            $this->db->update('notas_credito_cliente',array('productos'=>implode($pro)),array('id'=>$primary));            
            //Hay que actualizar inventario?
            if($post['actualizar_stock']==1){
                $this->db->insert('entrada_productos',array(
                     'fecha'=>date("Y-m-d"),
                     'motivo'=>3,
                     'proveedor'=>'',
                     'total_monto'=>$post['total_monto'],
                     'usuario'=>get_instance()->user->id,
                     'cajadiaria'=>$_SESSION['cajadiaria'],
                     'observacion'=>'Devolución cargada desde el módulo notas de crédito #'.$post['nro_nota_credito']
                ));
                $entrada = $this->db->insert_id();
                foreach($productos as $p){
                     $this->db->insert('entrada_productos_detalles',array(
                        'entrada_producto'=>$entrada,
                        'producto'=>$p->producto->codigo,
                        'lote'=>'',
                        'vencimiento'=>'',
                        'cantidad'=>$p->cantidad,
                        'precio_costo'=>$p->producto->precio_costo,
                        'precio_venta'=>$p->producto->precio_venta,
                        'total'=>$p->totalcondesc * $p->cantidad,
                        'sucursal'=>$_SESSION['sucursal']
                    ));
                }
            }
            $venta = get_instance()->db->get_where('ventas',['id'=>$post['venta']]);
            if($venta->num_rows()>0 && ($venta->row()->tipo_facturacion_id=='2' || $venta->row()->tipo_facturacion_id=='4')){
                $caja = $this->db->get_where('cajadiaria',array('caja'=>$_SESSION['caja'],'abierto'=>1));
                $correlativo = $caja->row()->nota_cred_correlativo+1;
                $this->db->update('cajadiaria',array('nota_cred_correlativo'=>$correlativo),array('caja'=>$_SESSION['caja'],'abierto'=>1));
            }
            $cliente = $post['cliente'];
            $this->db->query("UPDATE facturas_clientes SET actualizar_saldo = 1,saldo_cliente = get_saldo(clientes_id) WHERE clientes_id = $cliente");              
            //Generar factura electronica
            if($post['tipo_facturacion_nota']==1){
                $this->load->library('Kuatia');
                $this->kuatia->generarFacturaNCR($primary);
            }
        });
        //$crud->add_action('Anular','',base_url('movimientos/ventas/notas_credito_cliente/anular').'/');
        $crud->add_action('Reenviar FE','',base_url('movimientos/ventas/notas_credito_cliente/kuatia').'/');
        $crud->add_action('Imprimir recibo','',base_url('reportes/rep/verReportes/'.$this->ajustes->id_reporte_nota_credito.'/pdf/valor').'/');
        $output = $crud->render();
        $output->crud = 'compras';
        if ($crud->getParameters()!='list'){
            $edit = '';
            if($crud->getParameters()=='edit'){
                $edit = $y;                
            }
            $output->output = $this->load->view('nota_credito',array('output'=>$output->output,'edit'=>$edit),TRUE);            
        }else{
            $output->output.= $this->load->view('nota_credito_list');
        }
        $output->title = 'Notas de crédito';
        $this->loadView($output);
    }

    function anular_nota($id){
        $nota = $this->db->get_where('notas_credito_cliente',['id'=>$id]);
        if($nota->num_rows()>0){
            $nota = $nota->row();
            $this->db->update('notas_credito_cliente',array('anulado'=>1),array('id'=>$id));
            //Hay que actualizar inventario?
            if($nota->actualizar_stock==1){
                $this->db->insert('salidas',array(
                    'proveedor'=>'',
                    'fecha'=>date("Y-m-d"),
                    'motivo'=>3,
                    'usuario'=>get_instance()->user->id,
                    'total_costo'=>'',
                    'caja'=>'',
                    'cajadiaria'=>$_SESSION['cajadiaria'],
                    'productos'=>''
                ));
                $salida = $this->db->insert_id();
                $this->db->select('notas_credito_cliente_detalle.*,productos.codigo,productos.precio_costo');
                $this->db->join('productos','productos.id = notas_credito_cliente_detalle.producto');
                $productos = $this->db->get_where('notas_credito_cliente_detalle',['nota_credito'=>$id]);
                foreach($productos->result() as $p){
                     $this->db->insert('salida_detalle',array(
                        'salida'=>$salida,
                        'producto'=>$p->codigo,
                        'lote'=>'',
                        'vencimiento'=>'',
                        'cantidad'=>$p->cantidad,
                        'precio_costo'=>$p->precio_costo,
                        'total'=>$p->total,
                        'sucursal'=>$_SESSION['sucursal']
                    ));
                }
            }
            $cliente = $nota->cliente;
            $this->db->query("UPDATE facturas_clientes SET actualizar_saldo = 1,saldo_cliente = get_saldo(clientes_id) WHERE clientes_id = $cliente");              
        }
    }


    function notaCreditoDuplicar(){
        $existe = $this->db->get_where('notas_credito_cliente',array('venta'=>$_POST['venta'],'anulado'=>0));
        if($existe->num_rows()>0){
            $this->form_validation->set_message('notaCreditoDuplicar','Nota de crédito ya ha sido generada anteriormente');
            return false;
        }
        return true;
    }

    function saldo(){
        if(!empty($_POST['cliente'])){            
            $saldo = $this->elements->get_saldo($_POST['cliente']);
            $limite = $this->db->get_where('clientes',['id'=>$_POST['cliente']]);
            if($limite->num_rows()>0){
                $limite = number_format($limite->row()->limite_credito - $saldo,0,',','.').'Gs.';
                echo json_encode([number_format($saldo,0,',','.').'Gs.',$limite]);
            }else{
                echo json_encode([]);
            }
        }else{
            echo '0 Gs.';
        }
    }

    public function pagocliente($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_subject('pago de clientes');        
        if(is_numeric($x)){
            $crud->where('clientes_id',$x);
        }
        $crud->unset_add()->unset_delete()
        ->set_relation('sucursal','sucursales','denominacion')
        ->set_relation('caja','cajas','denominacion')
        ->set_relation('clientes_id','clientes','{nro_documento}|{nombres}|{apellidos}');
        $crud->columns('id','j3eb7f57f.nro_documento','j3eb7f57f.nombres','j3eb7f57f.apellidos','fecha','concepto','total_pagado','anulado','sucursal','caja','cajadiaria','anulado')
             ->callback_column('j3eb7f57f.nro_documento',function($val,$row){return explode('|',$row->s3eb7f57f)[0];})
             ->callback_column('j3eb7f57f.nombres',function($val,$row){return explode('|',$row->s3eb7f57f)[1];})
             ->callback_column('j3eb7f57f.apellidos',function($val,$row){return explode('|',$row->s3eb7f57f)[2];})
             ->callback_after_update(function($post,$primary){                
                if($post['anulado']==1){
                    $p = $this->db->get_where('pagocliente',['id'=>$primary])->row();
                    $this->db->query("UPDATE facturas_clientes SET actualizar_saldo = 1,saldo_cliente = get_saldo(clientes_id) WHERE clientes_id = {$p->clientes_id}");
                }
             })
             ->display_as('j3eb7f57f.nro_documento','Cédula')
             ->display_as('j3eb7f57f.nombres','Nombres')
             ->display_as('j3eb7f57f.apellidos','Apellidos');
        $crud->edit_fields('anulado');
        $crud->add_action('Imprimir ticket','',base_url('reportes/rep/verReportes/'.$this->ajustes->id_reporte_ticket_pagocliente.'/html/valor').'/');
        $crud->add_action('Imprimir recibo','',base_url('reportes/rep/verReportes/'.$this->ajustes->id_reporte_pagocliente.'/html/valor').'/');
        $crud->order_by('id','DESC');
        $output = $crud->render();
        $output->title = 'Pagos de clientes';
        $this->loadView($output);
    }

    function cheques_a_cobrar($x = ''){
        $crud = parent::crud_function('','');
        if(is_numeric($x)){
            $crud->where('ventas_id',$x)
                 ->field_type('ventas_id','hidden',$x)
                 ->unset_columns('ventas_id');
        }
        $output = $crud->render();        
        $this->loadView($output);
    }

    function updateTemp(){
        $productos = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'ventas']);
        if($productos->num_rows()==0){
            $this->db->insert('ventatemp',['productos'=>$_POST['productos'],'cajas_id'=>$this->user->caja,'user_id'=>$this->user->id,'tipo'=>'ventas']);
        }else{
            $this->db->update('ventatemp',['productos'=>$_POST['productos'],'user_id'=>$this->user->id],['cajas_id'=>$this->user->caja,'tipo'=>'ventas']);
        }
        echo 'success';
    }

    function getTemp(){
        $productos = [];
        $pp = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'ventas']);
        if($pp->num_rows()>0){
            $productos = json_decode($pp->row()->productos);
        }
        echo json_encode($productos);
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
