<?php

require_once APPPATH.'/controllers/Panel.php';    

class Libro_banco extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }
    
    public function libro_banco($x = '', $y = '') { 
        $crud = parent::crud_function($x, $y); 
        $crud->order_by('fecha','desc');       
        $crud->unset_read()
             ->unset_delete()
             ->set_theme('bootstrap2_horizontal');
        $crud->display_as('bancos_id','Banco')
             ->display_as('cuentas_bancos_id','Cuenta')
        	 ->field_type('sucursales_id','hidden',$this->user->sucursal)
        	 ->field_type('cajas_id','hidden',$this->user->caja)
        	 ->field_type('cajadiaria_id','hidden',$this->user->cajadiaria)  
        	 ->field_type('user_id','hidden',$this->user->id)           	 
        	 ->field_type('cobrado','hidden',0)        	         	 
        	 ->field_type('quien_anulo','hidden')    
        	 ->field_type('fecha_anulado','hidden')    
        	 ->field_type('tipo_movimiento','dropdown',['-1'=>'Egreso','1'=>'Ingreso'])
        	 ->field_type('forma_pago','enum',['Efectivo','Cheque','Transferencia','Deposito']) 
        	 ->field_type('tipo_cheque','dropdown',['1'=>'A la vista','-1'=>'Diferido'])
             ->field_type('saldo_actual','invisible','')
             ->field_type('fecha_registro','hidden',date('Y-m-d H:i:s'))
        	 ->set_relation('bancos_id','bancos','denominacion')
             ->set_relation('cuentas_bancos_id','cuentas_bancos','nro_cuenta')
             ->set_relation_dependency('cuentas_bancos_id','bancos_id','bancos_id')
        	 ->callback_field('fecha',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha" id="field-fecha" class="form-control" value="'.$value.'">';})
             ->callback_field('fecha_emision',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_emision" id="field-fecha_emision" class="form-control" value="'.$value.'">';})
             ->callback_field('fecha_pago',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_pago" id="field-fecha_pago" class="form-control" value="'.$value.'">';})
        	 ->unset_columns('sucursales_id','cajas_id','cajadiaria_id','user_id','quien_anulo','fecha_anulado','cobrado')
        	 ->callback_before_insert(function($post){
                $post['anulado'] = 0; 
                $saldo_actual = $this->db->get_where('cuentas_bancos',['id'=>$post['cuentas_bancos_id']]);
                $post['saldo_actual'] = $saldo_actual->row()->saldo;
                $post['fecha_registro'] = date("Y-m-d H:i:s");
                return $post;
            })
        	 ->callback_before_update(function($post,$primary){
        	 	$libro = $this->db->get_where('libro_banco',['id'=>$primary]);
        	 	if($libro->row()->anulado==0 && $post['anulado']==1){
        	 		$post['quien_anulo'] = $this->user->id;
        	 		$post['fecha_anulado'] = date("Y-m-d H:i:s");        	 	
        	 	}
        	 	return $post;
        	 })
        	 ->where('sucursales_id',$this->user->sucursal)
        	 ->where('cajas_id',$this->user->caja)
        	 ->where('anulado = 0','ESCAPE',TRUE)
             ->edit_fields('anulado');

        
        $crud->columns('fecha', 'bancos_id', 'cuentas_bancos_id', 'tipo_movimiento', 'concepto','beneficiario','monto','forma_pago','tipo_cheque','nro_cheque','fecha_emision','fecha_pago','anulado');
        $crud->set_rules('forma_pago','Forma de Pago','required|callback_validarFormaPago');
        
        if($crud->getParameters()=='add'){
        	$crud->field_type('anulado','hidden',0);
        }else{
            $crud->required_fields('anulado');
        }
        $output = $crud->render();
        $output->output = $this->load->view('libro_banco',['output'=>$output->output],TRUE);
        $this->loadView($output);     
    }

    function validarFormaPago(){
        $forma = $_POST['forma_pago'];
        if($forma=='Cheque' || $forma=='Transferencia'){
            $err = false;

            if(empty($_POST['bancos_id']) || empty($_POST['cuentas_bancos_id']))
                $err = true;
            if($forma=='Cheque' && (empty($_POST['nro_cheque']) || empty($_POST['tipo_cheque']) || empty($_POST['fecha_emision']) || empty($_POST['fecha_pago'])))
                $err = true;
            if($err){
                $this->form_validation->set_message('validarFormaPago','Debe completar los campos requeridos por la forma del pago seleccionada');
                return false;
            }
        }
        return true;
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
