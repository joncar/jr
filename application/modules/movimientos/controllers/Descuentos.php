<?php

require_once APPPATH.'/controllers/Panel.php';    

class Descuentos extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }
    
    public function descuentos($x = '', $y = '', $z = '') {
    	if($x=='updateTemp' && !empty($_POST['productos'])){
            $this->updateTemp();
            return;
        }

        if($x=='getTemp'){
            $this->getTemp();
            return;
        }

        if($x=='cleanTemp'){
            $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'descuentos']);       
            return;
        }

        $crud = parent::crud_function($x, $y);
        $crud->unset_read()
             ->unset_delete()
             //->columns('fecha','anulado')
             ->field_type('productos','invisible')
			 ->field_type('usuario','hidden',$this->user->id)			 
			 ->field_type('fecha','hidden',date("Y-m-d H:i:s"))
             ->display_as('proveedores_id','Proveedor')
			 ->replace_form_add('cruds/descuentos/add','movimientos')
			 ->replace_form_edit('cruds/descuentos/edit','movimientos')
             ->columns('id','fecha','productos','proveedores_id','desde','hasta','estado')
             ->order_by('id','DESC')
             ->unset_searchs('productos');
        if($crud->getParameters()=='list'){
            $crud->set_relation('usuario','user','{nombre} {apellido}');
        }

        $crud->callback_column('productos',function($val,$row){
            $this->db->select('productos.nombre_comercial');
            $this->db->join('productos','productos.id = descuento_detalle.productos_id');
            $prs = $this->db->get_where('descuento_detalle',['descuento_id'=>$row->id])->result();
            $values = [];
            foreach($prs as $p){
                $values[] = $p->nombre_comercial;
            }
            return implode(', ',$values);
        });

        if($this->user->admin!=1){
            $crud->unset_edit();
        }
        
        $crud->callback_before_insert(array($this,'validarFacturacion'));
        $crud->callback_after_insert(array($this,'addBody'));
        $crud->callback_after_update(array($this,'addBodyEdit'));

        $output = $crud->render();
        $this->loadView($output);     
    }

    function validarFacturacion($post){
        $post['fecha'] = date("Y-m-d H:i:s");
        $post['usuario'] = $this->user->id;        
        return $post;
    }   

    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        $date = date("Y-m-d H:i:s");
        foreach($productos as $p){
            $this->db->insert('descuento_detalle',array(
            	'descuento_id'=>$primary,
				'productos_id'=>$p->id,
				'codigo'=>$p->codigo,
				'nombre_comercial'=>$p->nombre_comercial,
                'precio_venta'=>$p->precio_venta,
				'por_desc'=>$p->por_desc,
				'precio_desc'=>$p->precio_desc,
				'desde'=>date("Y-m-d",strtotime(str_replace('/','-',$post['desde']))),
				'hasta'=>date("Y-m-d",strtotime(str_replace('/','-',$post['hasta']))),
				'estado'=>$post['estado'],
                'anulado'=>$p->anulado,
                'stocks'=>$p->stocks
            ));
        }

        $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'descuentos']);        
    }

    //Callback de function ventas a,b insert
    function addBodyEdit($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        $date = date("Y-m-d H:i:s");
        $ids = [];
        foreach($productos as $p){
        	$p->productos_id = (isset($p->productos_id)?$p->productos_id:$p->id);
            $data = array(
            	'descuento_id'=>$primary,
				'productos_id'=>$p->productos_id,
				'codigo'=>$p->codigo,
				'nombre_comercial'=>$p->nombre_comercial,
				'por_desc'=>$p->por_desc,
                'precio_desc'=>$p->precio_desc,				
				'desde'=>date("Y-m-d",strtotime(str_replace('/','-',$post['desde']))),
				'hasta'=>date("Y-m-d",strtotime(str_replace('/','-',$post['hasta']))),
                'anulado'=>$p->anulado,
				'estado'=>$post['estado']
            );
            $ids[] = $p->productos_id;            
            $id = $this->db->get_where('descuento_detalle',['descuento_id'=>$primary,'productos_id'=>$p->productos_id]);
            if($id->num_rows()==0){
            	$this->db->insert('descuento_detalle',$data);
            }else{
            	$this->db->update('descuento_detalle',$data,['id'=>$id->row()->id]);
            }
        }

        foreach($this->db->get_where('descuento_detalle',['descuento_id'=>$primary])->result() as $p){
        	if(!in_array($p->productos_id,$ids)){
        		$this->db->delete('descuento_detalle',['id'=>$p->id]);
        	}
        }

        $this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'descuentos']);        
    }

    function updateTemp(){
        $productos = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'descuentos']);
        if($productos->num_rows()==0){
            $this->db->insert('ventatemp',['productos'=>$_POST['productos'],'cajas_id'=>$this->user->caja,'user_id'=>$this->user->id,'tipo'=>'descuentos']);
        }else{
            $this->db->update('ventatemp',['productos'=>$_POST['productos'],'user_id'=>$this->user->id],['cajas_id'=>$this->user->caja,'tipo'=>'descuentos']);
        }
        echo 'success';
    }

    function getTemp(){
        $productos = [];
        $pp = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'descuentos']);
        if($pp->num_rows()>0){
            $productos = json_decode($pp->row()->productos);
        }
        echo json_encode($productos);
    }

    public function productos($x = '', $y = '', $return = false) {
        if($x=='masDetalles' && !empty($y)){
            $this->load->view('_productosMasDetallesModal',['producto'=>$y]);
            return;
        }
        $crud = parent::crud_function($x, $y, $this);   
        $columns = ['id','codigo','codigo_interno','nombre_comercial','proveedor_id','precio_costo','cant_1','cant_2','cant_3','precio_min_unidad','precio_min_cant','precio_min_caja','precio_may_unidad','precio_may_cant','precio_may_caja','precio_campo_unidad','precio_campo_cant','precio_campo_caja'];    
        if($crud->getParameters()=='json_list'){
            $columns[] = 'descuentos';
            $columns[] = 'stocks';
            $columns[] = 'estado';
        }
        $crud->columns($columns);
        $crud->set_relation('categoria_id', 'categoriaproducto', 'denominacion')
             ->set_relation('proveedor_id', 'proveedores', 'denominacion');
        $crud->field_type('descripcion','invisible')
             ->field_type('fecha_mod_precio','invisible')
             ->field_type('precio_costo_ant','invisible')
             ->field_type('precio_venta_ant','invisible')
             ->field_type('peso','invisible')
             ->field_type('foto_principal','invisible')
             ->field_type('usuario_id','invisible')
             ->field_type('created','invisible')
             ->field_type('modified','invisible')
             ->field_type('iva_id', 'dropdown', array('0' => 'Exento', '5' => '5%', '10' => '10%'))
             ->display_as('cant_1','Unidad')
             ->display_as('cant_2','Cantidad')
             ->display_as('cant_3','Caja')
             ->display_as('precio_min_unidad','Precio Minorista Unidad')
             ->display_as('precio_min_cant','Precio Minorista Cantidad')
             ->display_as('precio_min_caja','Precio Minorista Caja')
             ->display_as('precio_may_unidad','Precio Mayorista Unidad')
             ->display_as('precio_may_cant','Precio Mayorista Cantidad')
             ->display_as('precio_may_caja','Precio Mayorista Caja')
             ->display_as('precio_campo_unidad','Precio Campo Unidad')
             ->display_as('precio_campo_cant','Precio Campo Cantidad')
             ->display_as('precio_campo_caja','Precio Campo Caja')
             ->display_as('porc_min_unidad','Porcentaje Minorista Unidad')
             ->display_as('porc_min_cant','Porcentaje Minorista Cantidad')
             ->display_as('porc_min_caja','Porcentaje Minorista Caja')
             ->display_as('porc_may_unidad','Porcentaje Mayorista Unidad')
             ->display_as('porc_may_cant','Porcentaje Mayorista Cantidad')
             ->display_as('porc_may_caja','Porcentaje Mayorista Caja')
             ->display_as('porc_campo_unidad','Porcentaje Campo Unidad')
             ->display_as('porc_campo_cant','Porcentaje Campo Cantidad')
             ->display_as('porc_campo_caja','Porcentaje Campo Caja')
             ->display_as('parent_codigo','Código Padre')
             ->display_as('parent_fracc','Cantidad');
        $crud->replace_form_add('cruds/productos/add','movimientos')
             ->replace_form_edit('cruds/productos/edit','movimientos')
             ->unset_delete();
        
        if($crud->getParameters()=='edit' && is_numeric($y)){
            $stock = $this->db->get_where('productosucursal',['productos_id'=>$y]);
            $this->stock = 0;
            if($stock->num_rows()>0){
                $this->stock = $stock->row()->stock;
            }
            $crud->callback_field('stock',function($val){
                return '<input type="hidden" name="stock"><div class="form-control">'.get_instance()->stock.'</div>';
            });
        }
        $crud->callback_column('descuentos',function($val,$row){
            $fecha = date("Y-m-d");
            $this->db->where('estado',1);
            $this->db->where('productos_id',$row->id);
            $this->db->where('por_desc >',0);
            $this->db->where("DATE('$fecha') BETWEEN desde AND hasta",NULL,TRUE);
            $this->db->where('(descuento_detalle.anulado IS NULL OR descuento_detalle.anulado = 0)',NULL,TRUE);
            $this->db->select('desde,hasta,productos_id,por_desc,precio_desc');
            $this->db->order_by('hasta','ASC');
            return $this->db->get('descuento_detalle')->result();
        });
        $crud->callback_column('stocks',function($val,$row){
            $this->db->where('productos_id',$row->id);
            if(empty($_POST['sucursal'])){
                $this->db->where('sucursal',get_instance()->user->sucursal);
            }
            if(!empty($_POST['sucursal']) && is_numeric($_POST['sucursal'])){
                $this->db->where('sucursal',$_POST['sucursal']);
            }
            return $this->db->get('productosucursal')->result();
        });
        $crud->callback_after_insert(function($post,$primary){
            get_instance()->db->query("INSERT INTO productosucursal(id,fechaalta,productos_id,sucursal,stock) SELECT NULL,NOW(),$primary,id,0 FROM sucursales");
        });
        if($crud->getParameters()!='json_list'){
            $crud->callback_column('codigo',function($val,$row){            
                return '<a href="'.base_url('movimientos/productos/productos/edit/'.$row->id).'" target="_blank">'.$val.'</a>';
                //return  '<a href="javascript:masDetalles(\''.$row->codigo.'\')">'.$val.'</a>';
            });
        }
        if($crud->getParameters()=='json_list' && !empty($_POST['codigo'])){
            if(!empty($_POST['codigo']) && empty($_POST['balanza'])){
                $crud->where("(productos.codigo = '".$_POST['codigo']."' OR productos.codigo_interno = '".$_POST['codigo']."') AND (productos.anulado IS NULL OR productos.anulado = '0')",'ESCAPE',TRUE);
            }elseif(is_numeric($_POST['codigo']) && !empty($_POST['balanza'])){
                $crud->where("((productos.balanza = 1 AND productos.id = '".$_POST['codigo']."') OR productos.codigo = '".$_POST['codigo_origen']."') AND (productos.anulado IS NULL OR productos.anulado = '0')",'ESCAPE',TRUE);
            }else{
                $crud->where("(productos.codigo = '".$_POST['codigo']."') AND (productos.anulado IS NULL OR productos.anulado = '0')",'ESCAPE',TRUE);
            }
        }
        if($crud->getParameters()=='add'){
            $crud->set_rules('codigo','Codigo','required|is_unique[productos.codigo]|callback_validarCodigo');
        }

        if($crud->getParameters()=='edit' && is_numeric($y) && !empty($_POST['codigo'])){
            $pr = $this->db->get_where('productos',['id'=>$y])->row();
            if($pr->codigo!=$_POST['codigo']){
                $crud->set_rules('codigo','Codigo','required|is_unique[productos.codigo]|callback_validarCodigo');
            }
            if($pr->parent_codigo!=$_POST['parent_codigo']){
                $crud->set_rules('parent_codigo','Codigo','callback_validarCodigo');
            }
        }

        $crud->search_types = [
            'codigo'=>'<input type="hidden" name="search_text[]"><input type="text" name="codigo_list" class="form-control">'
        ];

        if(!empty($_POST['codigo_list'])){
            $crud->where("(codigo LIKE '%{$_POST['codigo_list']}%' OR codigo_interno LIKE '%{$_POST['codigo_list']}%' OR nombre_comercial LIKE '%{$_POST['codigo_list']}%')",'ESCAPE',TRUE);
        }

        if(!empty($_POST['parent_codigo'])){
            $crud->where('parent_codigo',$_POST['parent_codigo']);
        }

        $output = $crud->render();
        $output->output.= $this->load->view('productos-list',[],TRUE);
        $this->loadView($output);
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
