<?php

require_once APPPATH.'/controllers/Panel.php';    

class Entradas extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
        }                
    }
    
    public function entrada_productos($x = '', $y = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/entrada_productos'));
        else {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('motivo', 'motivo_entrada', 'denominacion')
                ->set_relation('proveedor', 'proveedores', 'denominacion')
                ->set_relation('usuario', 'user', '{nombre} {apellido}');
        $crud->unset_delete();
        $crud->columns('id','fecha','motivo','usuario','total_monto'); 
        $crud->set_rules('productos','Productos','required|callback_unProducto|callback_validar');        
        $crud->callback_after_insert(array($this,'addBody'));
        $crud->order_by('id','DESC');
        $output = $crud->render();
        if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
            $edit = '';
            $default = array();
            if($crud->getParameters()=='edit'){
                $edit = $y;                
            }
            $output->output = $this->load->view('entradas',array('output'=>$output->output,'edit'=>$edit),TRUE);
        }

        $this->loadView($output);
        }
    }

    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        //$this->db->delete('ventadetalle',array('venta'=>$primary));
        foreach($productos as $p){
            $pro[] = $p->nombre;            
            $p->producto = @$this->db->get_where('productos',array('codigo'=>$p->codigo))->row();            
            $venc = empty($p->vencimiento)?'00/0000':$p->vencimiento;
            $venc = explode('/',$venc);
            $venc = $venc[1].'-'.$venc[0].'-01';
            $this->db->insert('entrada_productos_detalles',array(
                'entrada_producto'=>$primary,
                'producto'=>$p->codigo,
                'lote'=>'',
                'vencimiento'=>$venc,
                'cantidad'=>$p->cantidad,                
                'precio_costo'=>$p->precio_costo,
                'precio_venta'=>$p->precio_venta,
                'total'=>$p->total,
                'sucursal'=>$p->sucursal,
                'stock_anterior'=>@$this->db->get_where('productosucursal',['sucursal'=>$p->sucursal,'producto'=>$p->codigo])->row()->stock
            ));
        }
        $this->db->update('entrada_productos',array('productos'=>implode($pro)),array('id'=>$primary));                
    }

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        if($this->ajustes->controlar_vencimiento_stock==1){
            $productos = json_decode($_POST['productos']);
            foreach($productos as $p){
                if(empty($p->vencimiento)){
                    $this->form_validation->set_message('unProducto','El producto '.$p->codigo.' no posee un vencimiento válido');        
                    return false;
                }
            }
        }
        //Validar si el pago es completo
    }

    function validar(){ 
        $productos = $_POST['productos'];       
        $ajustes = $this->ajustes;
        $productos = json_decode($_POST['productos']);        
        $return = true;
        //Validar sucursales
        $msj = '';
        foreach($productos as $p){                
            $pro = $this->db->get_where('productos',array('codigo'=>$p->codigo));
            if($pro->num_rows()==0){
                $return = false;
                $msj.= '<p>El producto '.$p->codigo.' no existe</p>';
            }else{
                $pro = $pro->row();
                //Si esta vacia la sucursal muestra un error
                if(empty($p->sucursal)){
                    $msj.= '<p>Falta seleccionar la sucursal para el producto '.$pro->codigo.'</p>';
                    $return = false;
                }
            }
        }
        //Validar stocks                
        if($return && $this->ajustes->vender_sin_stock==0){
            $msj = '';
            foreach($productos as $p){                
                $pro = $this->db->get_where('productos',array('codigo'=>$p->codigo));
                if($pro->num_rows()==0){
                    $return = false;
                    $msj.= '<p>El producto '.$p->codigo.' no existe</p>';
                }else{
                    $pro = $pro->row();
                    if($pro->inventariable==1){
                        $producto = $this->db->get_where('productosucursal',array('producto'=>$p->codigo,'sucursal'=>$p->sucursal));
                        if($producto->num_rows()==0 || $producto->row()->stock<$p->cantidad){
                            $return = false;
                            $msj.= '<p>El producto '.$p->codigo.' no tiene stock suficiente</p>';
                        }
                    }
                }
            }            
        }
        if(!$return){
            $this->form_validation->set_message('validar',$msj);
        }
        return $return;
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
