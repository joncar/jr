<?php

require_once APPPATH.'/controllers/Panel.php';    

class NotasRemision extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }
    
    public function nota_remision($x = '', $y = '', $z = '') {  
        if($x=='getNroRemision'){
            echo $this->getNroRemision();
            die();
        }       
        $crud = parent::crud_function($x, $y);
        $crud->set_subject('Nota de Remisión');
        $crud->set_theme('bootstrap');
        $crud->field_type('timbrados_id','hidden')
        	 ->field_type('cajas_id','hidden',$this->user->caja)
        	 ->field_type('cajadiaria_id','hidden',$this->user->cajadiaria)
        	 ->field_type('user_id','hidden',$this->user->id)
        	 ->field_type('fecha','hidden',date("Y-m-d H:i:s"))
        	 ->field_type('anulado','hidden',0)
        	 ->field_type('nro_nota','hidden','')
        	 ->field_type('nombre_transportista','invisible')
             ->field_type('ruc_transportista','invisible')
             ->field_type('nombre_conductor','invisible')
             ->field_type('documento_conductor','invisible')
             ->field_type('direccion_conductor','invisible')
             ->field_type('marca_vehiculo','invisible')
             ->field_type('fecha_traslado','invisible')
             ->field_type('nro_chapa','invisible')
             ->field_type('motivo_traslado','invisible')
             ->field_type('fecha_salida','invisible')
             ->field_type('fecha_llegada','invisible')
             ->field_type('punto_partida','invisible')
             ->field_type('punto_llegada','invisible')

        	 ->callback_before_insert(function($post){
        	 	$this->db = get_instance()->db;
        	 	$cajadiaria = get_instance()->user->cajadiaria;
        	 	$timbrado = $this->db->get_where('cajadiaria',['id'=>$cajadiaria])->row()->timbrados_remision;
        	 	$timbrado = $this->db->get_where('timbrados_nota_remision',['id'=>$timbrado])->row();
        	 	$post['timbrados_id'] = $timbrado->id;
        	 	$post['cajas_id'] = get_instance()->user->caja;
        	 	$post['cajadiaria_id'] = $cajadiaria;
        	 	$post['nro_nota'] = $timbrado->serie.$timbrado->correlativo;
        	 	$this->db->update('timbrados_nota_remision',['correlativo'=>$timbrado->correlativo+1],['id'=>$timbrado->id]);

        	 	return $post;
        	 })
        	 ->callback_after_insert(function($post,$primary){
        	 	foreach($_POST['producto'] as $n=>$p){
        	 		$data = [
        	 			'nota_remision_id'=>$primary,
        	 			'producto'=>$p,
        	 			'cantidad'=>$_POST['cantidad'][$n]
        	 		];
        	 		$this->db->insert('nota_remision_detalle',$data);
        	 	}
        	 })
        	 ->callback_after_update(function($post,$primary){
        	 	foreach($_POST['producto'] as $n=>$p){
        	 		$data = [
        	 			'nota_remision_id'=>$primary,
        	 			'producto'=>$p,
        	 			'cantidad'=>$_POST['cantidad'][$n]
        	 		];
        	 		$this->db->update('nota_remision_detalle',$data,['nota_remision_id'=>$primary,'producto'=>$p]);
        	 	}
        	 });
        $crud->unset_read()
             ->unset_delete();
        $crud->columns('timbrados_id','venta', 'nro_nota','fecha', 'punto_partida', 'punto_llegada', 'cajas_id', 'cajadiaria_id','anulado');               
        $crud->replace_form_add('cruds/notas_remision/add.php','movimientos');  
        $crud->replace_form_edit('cruds/notas_remision/edit.php','movimientos');  
        $output = $crud->render();
        $output->title = 'Nota de Remisión';
        $this->loadView($output);     
    }

    function getNroRemision(){
        $cajadiaria = get_instance()->user->cajadiaria;
        $timbrado = $this->db->get_where('cajadiaria',['id'=>$cajadiaria])->row()->timbrados_remision;
        $timbrado = $this->db->get_where('timbrados_nota_remision',['id'=>$timbrado]);
        return $timbrado->num_rows()>0?$timbrado->row()->serie.$timbrado->row()->correlativo:'0';
    }
    
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
