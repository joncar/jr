<?php

require_once APPPATH.'/controllers/Panel.php';    

class Asistencia extends Main {

    function __construct() {
        parent::__construct();        
    }     
    
    public function asistencia($x = '', $y = '') {  
        $response = ['success'=>false,'msj'=>$this->error('La cedula ingresada no concuerda con ningún usuario registrado')];
        if(!empty($_POST)){
            if(isset($_POST['cedula']) && !empty($_POST['cedula'])){
                $cedula = $this->db->get_where('user',['cedula'=>$_POST['cedula']]);
                if($cedula->num_rows()>0){ 
                    $cedula->row()->foto = empty($cedula->row()->foto)?base_url('assets/grocery_crud/css/jquery_plugins/cropper/vacio.png'):base_url('img/fotos/'.$cedula->row()->foto);
                    $response['success']=true;
                    $response['user'] = $cedula->row();                
                    $fecha = date("Y-m-d H:i:s");
                    $fecha2 = '&nbsp;<b>'.date("d/m/Y H:i",strtotime($fecha)).'</b>';
                    $response['hora'] = $fecha2;
                    $asistencia = $this->db->get_where('asistencia',['user_id'=>$cedula->row()->id,'DATE(entrada1)'=>date("Y-m-d")]);
                    if($asistencia->num_rows()==0){
                        $this->db->insert('asistencia',['user_id'=>$cedula->row()->id,'entrada1'=>date("Y-m-d H:i:s")]);
                        $response['msj'] = $this->success('Entrada 1 registrada con éxito ');
                    }else{
                        if(empty($asistencia->row()->salida1)){
                            $this->db->update('asistencia',['salida1'=>$fecha],['id'=>$asistencia->row()->id]);
                            $response['msj'] = $this->success('Salida 1 registrada con éxito ');
                        }
                        elseif(empty($asistencia->row()->entrada2)){
                            $this->db->update('asistencia',['entrada2'=>$fecha],['id'=>$asistencia->row()->id]);
                            $response['msj'] = $this->success('Entrada 2 registrada con éxito ');
                        }
                        elseif(empty($asistencia->row()->salida2)){
                            $this->db->update('asistencia',['salida2'=>$fecha],['id'=>$asistencia->row()->id]);
                            $response['msj'] = $this->success('Salida 2 registrada con éxito ');
                        }
                        elseif(empty($asistencia->row()->entrada3)){
                            $this->db->update('asistencia',['entrada3'=>$fecha],['id'=>$asistencia->row()->id]);
                            $response['msj'] = $this->success('Entrada 3 registrada con éxito ');
                        }
                        elseif(empty($asistencia->row()->salida3)){
                            $this->db->update('asistencia',['salida3'=>$fecha],['id'=>$asistencia->row()->id]);
                            $response['msj'] = $this->success('Salida 3 registrada con éxito ');
                        }else{
                            $response['success']=false;
                            $response['msj'] = $this->error('Jornada completada por el día de hoy');
                        }       
                    }
                }else{
                    $response['msj'] = $this->error('La cedula ingresada no concuerda con ningún usuario registrado');
                }
            }
            echo json_encode($response);
            return;
        }
        $this->loadView([
        	'view'=>'panel',
        	'crud'=>'user',
        	'output'=>$this->load->view('asistencia',[],TRUE)
        ]);
    }
}