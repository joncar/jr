<?php

require_once APPPATH.'/controllers/Panel.php';    

class Salidas extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
        }                
    }
    
    public function salidas($x = '', $y = '') {
        if (empty($_SESSION['sucursal']))
            header("Location:" . base_url('panel/selsucursal?redirect=admin/salidas'));
        else {
            $crud = parent::crud_function($x, $y);
            $crud->unset_delete();
            $crud->columns('id','fecha','motivo','usuario','total_costo'); 
            $crud->set_relation('proveedor', 'proveedores', 'denominacion')
                    ->set_relation('motivo', 'motivo_salida', 'denominacion')
                    ->set_relation('usuario', 'user', '{nombre} {apellido}');
            $crud->unset_read()
                    ->unset_delete();
            $crud->set_rules('productos','Productos','required|callback_unProducto|callback_validar');        
            $crud->callback_after_insert(array($this,'addBody'));
            $crud->order_by('id','DESC');
            $output = $crud->render();        
            if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
                $edit = '';
                $default = array();
                if($crud->getParameters()=='edit'){
                    $edit = $y;                
                }
                $output->output = $this->load->view('salidas',array('output'=>$output->output,'edit'=>$edit),TRUE);
            }
            $this->loadView($output);    
        } 
    }

    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        //$this->db->delete('ventadetalle',array('venta'=>$primary));
        foreach($productos as $p){
            $pro[] = $p->nombre;            
            $p->producto = @$this->db->get_where('productos',array('codigo'=>$p->codigo))->row();            
            $vencimiento = empty($p->vencimiento)?'0000-00-00':$p->vencimiento;
            $this->db->insert('salida_detalle',array(
                'salida'=>$primary,
                'producto'=>$p->codigo,
                'lote'=>'',
                'vencimiento'=>$vencimiento,
                'cantidad'=>$p->cantidad,                
                'precio_costo'=>$p->precio_costo,
                'total'=>$p->total,
                'sucursal'=>$p->sucursal,
                'stock_anterior'=>@$this->db->get_where('productosucursal',['sucursal'=>$p->sucursal,'producto'=>$p->codigo])->row()->stock
            ));
        }
        $this->db->update('salidas',array('productos'=>implode($pro)),array('id'=>$primary));                
    }

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }

    function validar(){ 
        $productos = $_POST['productos'];       
        $ajustes = $this->ajustes;
        $productos = json_decode($_POST['productos']);        
        $return = true;
        //Validar sucursales
        $msj = '';
        foreach($productos as $p){                
            $pro = $this->db->get_where('productos',array('codigo'=>$p->codigo));
            if($pro->num_rows()==0){
                $return = false;
                $msj.= '<p>El producto '.$p->codigo.' no existe</p>';
            }else{
                $pro = $pro->row();
                //Si esta vacia la sucursal muestra un error
                if(empty($p->sucursal)){
                    $msj.= '<p>Falta seleccionar la sucursal para el producto '.$pro->codigo.'</p>';
                    $return = false;
                }
            }
        }
        //Validar stocks                
        if($return && $this->ajustes->vender_sin_stock==0){
            $msj = '';
            foreach($productos as $p){                
                $pro = $this->db->get_where('productos',array('codigo'=>$p->codigo));
                if($pro->num_rows()==0){
                    $return = false;
                    $msj.= '<p>El producto '.$p->codigo.' no existe</p>';
                }else{
                    $pro = $pro->row();
                    if($pro->inventariable==1){
                        $producto = $this->db->get_where('productosucursal',array('producto'=>$p->codigo,'sucursal'=>$p->sucursal));
                        if($producto->num_rows()==0 || $producto->row()->stock<$p->cantidad){
                            $return = false;
                            $msj.= '<p>El producto '.$p->codigo.' no tiene stock suficiente</p>';
                        }
                    }
                }
            }            
        }
        if(!$return){
            $this->form_validation->set_message('validar',$msj);
        }
        return $return;
    }

    function getFactura($x = '',$y = ''){
        if ($x == 'imprimir') {
            if (!empty($y) && is_numeric($y)) {
                $this->db->select('salidas.*, proveedores.denominacion as proveedorname, motivo_salida.denominacion as motivoname, CONCAT(user.nombre," ",user.apellido) as usuario',FALSE);
                $this->db->join('motivo_salida','motivo_salida.id = salidas.motivo');
                $this->db->join('proveedores','proveedores.id = salidas.proveedor');
                $this->db->join('user','user.id = salidas.usuario');
                $venta = $this->db->get_where('salidas', array('salidas.id' => $y));
                if ($venta->num_rows() > 0) {
                    ob_clean();
                    $this->db->select('salida_detalle.*, productos.nombre_comercial');
                    $this->db->join('productos', 'salida_detalle.producto = productos.codigo');
                    $detalles = $this->db->get_where('salida_detalle', array('salida' => $venta->row()->id));
                    $papel = 'A4';
                    $this->load->library('html2pdf/html2pdf');
                    $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', array(5, 5, 5, 8));
                    $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/imprimir_salida', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                    $html2pdf->Output('Reporte Legal.pdf', 'D');
                    //echo $this->load->view('reportes/imprimir_salida',array('venta'=>$venta->row(),'detalles'=>$detalles),TRUE);                    
                } else {
                    echo "Factura no encontrada";
                }
            } else {
                echo 'Factura no encontrada';
            }
        } else {
            $this->loadView($output);
        }
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
