<?php
	if(!empty($edit) && is_numeric($edit)){
		$nota = $this->elements->compras(array('compras.id'=>$edit),null,false);
	}
?>
<?php get_instance()->hcss[] = '
<style>
	a:focus,div:focus,.chzn-container-active{
		border:1px dashed blue;
	}
	body {
        color: #000;
        font-weight: 600;
    }
    .form-control{
    	color: #000;
	    font-weight:600;
	    opacity: 1;
	    font-size:15px;
    }
	.panel{
		border-radius:0px;
	}

	.error{
		border: 1px solid red !important;
	}

	.patternCredito{
		background: #ffffffa6;width: 100%;height: 100%;position: absolute;top: 0px;left: 0px;
	}

	.btnsaldo{
		position: absolute;top: 0;right: 15px;
	}
	@media screen and (max-width:768px){
		.btnsaldo{
			top: -15px;
			font-size:20px;
		}	
	}
	.kt-portlet{
		margin:0;
	}

	.table thead th, .table td{
		padding:6px;
		color: #000;
		font-weight: 600;
	}

	tbody .form-control{
		height:22px;
	}

	.kt-portlet .kt-portlet__head{
		min-height: inherit;
		padding:10px;
	}

	.totales input, .totales .form-control:focus{
		background:lightgreen !important;
		padding: 0 12px;
		border-radius: 0;
	}

	.pagoEfectivo input, .pagoEfectivo .form-control:focus{
		background-color: lightblue;
		border-radius: 0;
		padding: 0 12px;
	}

	.vueltos input{
		background: #e8e800 !important;
		color: red;
		border-radius: 0;
		padding: 0 12px;
	}

	input.guaranies{
		font-weight: bold;
		font-size: 19px;
	}
	div.inp{
		position: relative;
	}
	div.banderas{
		background-image:url('.base_url().'img/banderas.jpg);
		width:20px;
		height:20px;	
		position: absolute;
		right:0px;
		top:0px;
		background-size: 100%;	
	}	
	div.banderas.us{
		background-position: 0 0px;
	}
	div.banderas.ar{
		background-position: 0 -20px;
	}		
	div.banderas.br{
		background-position: 0 -40px;
	}
	div.banderas.py{
		background-position: 0 -60px;
	}

	div.banderas-2x{
		background-image:url('.base_url().'img/banderas.jpg);
		width:40px;
		height:40px;	
		position: absolute;
		right:0px;
		top:0px;
		background-size: 100%;	
	}	
	div.banderas-2x.us{
		background-position: 0 0px;
	}
	div.banderas-2x.ar{
		background-position: 0 -40px;
	}		
	div.banderas-2x.br{
		background-position: 0 -80px;
	}
	div.banderas-2x.py{
		background-position: 0 -120px;
	}
	#procesar th{
		font-weight: bold;
		font-size: 16px;
	}
	.btn-lg, .btn-group-lg > .btn {
	    padding: 0.7rem 1.45rem;
	}

	@media screen and (min-width:769px){
		.cuerpoTransaccion{
			position:relative;
			overflow-x:hidden;
		}

		.cuerpoTransaccion .tarifario{
			position: absolute;
			right: 0;
			top: 50px;
			background: #fff;
			border: 1px solid #1e1e2d;
			right: -241px;
			max-width:241px;
			-webkit-transition: 0.5s ease-in-out;
			-moz-transition: 0.5s ease-in-out;
			-o-transition: 0.5s ease-in-out;
			-ms-transition: 0.5s ease-in-out;
			transition: 0.5s ease-in-out;
		}

		.tarifario:hover {
		    right: 0;
		}

		.tarifario .kt-portlet{
			position:relative;
		}

		.tarifarioToggle{
			position: absolute;
			left: -37px;
			background: #fff;
			width: 27px;
			text-align: center;
			border: 1px solid #000;
			top: 0;
		}
	}
</style>
'; 
?>
<div class="kt-portlet">
	<div class="kt-portlet__body" style="padding:10px 25px;">
		<div class="kt-section">
			<div class="row" style="position: relative;">
				<!--<div style="width:100%; text-align: right; position:absolute; left:0; padding:0 30px">
					#FACTURA: <span id="nroFactura"></span>
				</div>-->
				<div class="col-12 col-md-10">
					<div class="row">
						<div class="col-12 col-md-3">
							#Factura: 
							<input type="text" name="nro_factura" class="form-control" id="nro_factura" value="" placeholder="Numero de factura de compra">
						</div>
						<div class="col-12 col-md-2">
							Timbrado: 
							<input type="text" name="timbrado" class="form-control" id="timbrado" value="" placeholder="Timbrado">
						</div>
						<div class="col-12 col-md-3">
							Sucursal:
							<?php 
								//$this->db->limit(1);
								echo form_dropdown_from_query('sucursal','sucursales','id','denominacion',$this->user->sucursal,'id="sucursales"') 
							?>					
						</div>
						
						<div class="col-12 col-md-2">
							Tipo facturación: 
							<?= form_dropdown_from_query('tipo_facturacion_id','tipo_facturacion','id','denominacion',1) ?>
						</div>
						
						<div class="col-12 col-md-2">
							Forma de Pago: 
							<?php 
								echo form_dropdown_from_query('forma_pago',$this->db->get('formapago'),'id','denominacion',1,'id="formapago"')
							?>
						</div>

						
						<div class="col-12 col-md-3">
							Transacción: 
							<?= form_dropdown_from_query('transaccion','tipotransaccion','id','denominacion',1,'id="transaccion"'); ?>
						</div>
						
						<div class="col-12 col-md-3">
							Proveedor: 
							<?php 
								echo form_dropdown_from_query('proveedor','proveedores','id','ruc denominacion',1,'id="proveedores"');
							?>
						</div>
						<div class="col-12 col-md-2">
							Fecha: 
							<input type="text" name="fecha" class="form-control" id="fecha" value="<?= date("d/m/Y") ?>" placeholder="Fecha">
						</div>	
						<div class="col-12 col-md-2">
							Vencimiento Pago: 
							<input type="text" name="vencimiento_pago" class="form-control" id="vencimiento_pago" value="" placeholder="Vencimiento de pago">
						</div>
						<div class="col-12 col-md-2">
							Tipo de Egreso: 
							<?php 
								echo form_dropdown('pago_caja_diaria',array('0'=>'Sin Pago','1'=>'Caja Diaria','2'=>'Caja Grande','3'=>'Bonificación'),0,'id="pagocajadiaria" class="form-control"')
							?>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-2" style="position: relative;">
					<span style="position: absolute;top: 26px;left: 30px;font-weight: bold;">Total Compra: </span>
					<input type="text" name="total_compra" class="form-control" readonly="" value="300.000" style="font-size:30px; font-weight:bold; text-align: right;height: 89px;vertical-align: baseline;margin-top: 19px; background:lightgreen">
				</div>

			</div>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-12 respuestaVenta"></div>
</div>

<div class="row cuerpoTransaccion">
	<div class="col-12 col-sm-10 col-md-12">
		<div class="kt-portlet">
			<div class="kt-portlet__body" style="padding-top:0">
				<div class="kt-section">
					<div style="height:350px; overflow-y:auto">
						<table class="table table-bordered" id="ventaDescr">
							<thead style="background: #f2f3f8;">
								<tr>
									<th>Código</th>
									<th>Nombre</th>
									<th>Cantidad</th>
									<th>P.Costo</th>
									<th>%Desc</th>
									<th>%Venta</th>
									<th>P.Venta</th>
									<th>Total</th>
									<th>Venc.</th>
									<th>Stock</th>
									<th>IVA</th>
									<th>Sucursal</th>
								</tr>
							</thead>
							<tbody>

								<tr id="productoVacio">
									<td tabindex="1">
										<a href="javascript:void(0)" class="rem" style="display:none;color:red">
											<i class="fa fa-times"></i>
										</a> 
										<span>&nbsp;</span>
									</td>
									<td tabindex="2">&nbsp;</td>
									<td tabindex="3"><input name="cantidad" class="cantidad form-control" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td tabindex="4"><input name="precio_costo" class="precio_costo form-control" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0"></td>
									<td tabindex="5"><input name="por_desc" class="por_desc form-control" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td tabindex="6"><input name="por_venta" class="por_venta form-control" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td tabindex="7">
										<input name="precio_venta" class="precio_venta form-control" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0"> <i class="fa fa-chevron-down" onclick="compra.mayoristas(this)" style="cursor: pointer;"></i>
										<div class="mayoristas" style="display: none">
											<hr>
											<div class="d-flex">M1<input name="precio_venta1" class="precio_venta1 form-control" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0" placeholder="Mayorista 1"></div>
											<div class="d-flex">M2<input name="precio_venta2" class="precio_venta2 form-control" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0" placeholder="Mayorista 2"></div>
											<div class="d-flex">M3<input name="precio_venta3" class="precio_venta3 form-control" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0" placeholder="Mayorista 3"></div>
											<div class="d-flex">PC<input name="precio_credito" class="precio_credito form-control" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0" placeholder="P.Credito"></div>
										</div>
									</td>
									<td tabindex="8">&nbsp;</td>
									<td tabindex="9"><input name="vencimiento" class="vencimiento form-control" type="text" style="display:none; width:70px;text-align: right;padding: 0 6px;" value=""></td>
									<td tabindex="10">&nbsp;</td>
									<td tabindex="11"></td>
									<td><?php echo form_dropdown_from_query('sucursal','sucursales','id','denominacion',$this->user->sucursal,'style="width:50px;text-align: right;padding: 0 6px;"',FALSE,'sucursal') ?></td>
								</tr>

								
							</tbody>
						</table>
					</div>
				</div>

				<?php if(empty($edit)): ?>
					<div class="panel-footer">
						<div class="row" style="background: #f2f3f8;padding: 6px;">
							<div class="col-12 col-md-2" style="text-align: center;padding: 6px;">
								Cant: <span id="cantidadProductos">4</span>					
							</div>
							<!--<div class="col-12 col-md-1 d-flex align-items-center" style="text-align: center;padding: 6px;background: #fff;border: 1px solid #eee;border-radius: 4px;">
								<label for="descontar" style="margin: 0;">%desc</label> 
								<input type="text" name="descontar" id="descontar" value="0" class="form-control" style="border: 0;text-align: center;padding: 0 5px;height: auto;">
							</div>-->
							<div class="col-12 col-md-9" style="position: relative;">
								<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
								<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" style="padding-left: 25px;padding-right: 73px;">
								<button style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar insertar">Insertar</button>

								<div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
							</div>
						</div>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>
	<div class="col-12 col-sm-2 col-md-2 tarifario">
		<div class="kt-portlet">
			<div class="tarifarioToggle">
				<i class="fas fa-dollar-sign fa-2x"></i>
			</div>
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						<b>Resumen de compra</b>
					</h1>
				</div>
			</div>
			<div class="kt-portlet__head">
				<div class="row">
					<div class="col-12 col-md-4">Pesos: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_pesos" value="300.000" readonly="">
					</div>
					<div class="col-12 col-md-4">Reales: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_reales" value="300.000" readonly="">
					</div>
					<div class="col-12 col-md-4">Dolares: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_dolares" value="300.000" readonly="">
					</div>
				</div>
			</div>
		</div>

		
	</div>
</div>

<div class="row">
	<div class="col-12 col-md-10">
		<div class="btn-group btn-group-justified" role="group" aria-label="...">
		  <div class="btn-group" role="group">
		    <button type="button" data-toggle="modal" data-target="#addProduct" class="btn btn-default" style="color:#000 !important"><i class="fa fa-columns"></i> Añadir producto</button>
		  </div>

		  <?php if(empty($edit)): ?>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-success" style="color:#000 !important" onclick="nuevaCompra();"><i class="fa fa-plus-circle"></i> Nueva compra</button>
		  </div>
		  <?php else: ?>
		  	<div class="btn-group" role="group">
		    	<a href="<?= base_url() ?>movimientos/compras/compras/add" class="btn btn-success" style="color:#000 !important"><i class="fa fa-plus-circle"></i> Nueva compra</a>
		  	</div>
		  <?php endif ?>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary" onclick="saveCompra()"><i class="fa fa-floppy-o"></i> Procesar compra</button>
		  </div>
		  <!--<div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" style="color:#000 !important"><i class="fa fa-print"></i> Imprimir</button>
		  </div>-->
		</div>
	</div>
</div>


<div id="addProduct" class="modal fade" tabindex="-1" role="dialog">
  <form onsubmit="insertar('movimientos/productos/productos/insert',this,'.resultClienteAdd'); return false;">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Agregar producto</h4>
	      </div>
	      <div class="panel-body">
	  		  <div class="row" style="margin-left: 0; margin-right: 0">
  		  		<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Código interno</label>
				    <input type="text" class="form-control" name="codigo_interno">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Código</label>
				    <input type="text" class="form-control" name="codigo">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Nombre comercial</label>
				    <input type="text" class="form-control" name="nombre_comercial">
				  </div>
				</div>
				

				
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Categoria</label>
				    <?php echo form_dropdown_from_query('categoria_id', 'categoriaproducto', 'id', 'denominacion', 1, 'id="field-categoriaproducto"'); ?>
				  </div>
				</div>


				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Precio de costo</label>
				    <input type="text" class="form-control" name="precio_costo">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Precio de Venta</label>
				    <input type="text" class="form-control" name="precio_venta">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Precio Mayorista 1</label>
				    <input type="text" class="form-control" name="precio_venta_mayorista1">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				   <div class="form-group">
				    <label for="exampleInputEmail1">Precio Mayorista 2</label>
				    <input type="text" class="form-control" name="precio_venta_mayorista2">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Precio Mayorista 3</label>
				    <input type="text" class="form-control" name="precio_venta_mayorista3">
				  </div>
				</div>

				<input type="hidden" name="unidad_medida_id" value="1">
				<input type="hidden" name="iva_id" value="10">
				<div class="col-12 col-md-12">
					<div class="resultClienteAdd"></div>
			    </div>
			</div>			  
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary">Guardar</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->


<?php get_instance()->js[] = '<script src="'.base_url('assets/grocery_crud/js/jquery_plugins/jquery.mask.js').'"></script>'; ?>
<?php $this->load->view('_inventario_modal_compras',array(),FALSE,'movimientos'); ?>
<script src="<?= base_url() ?>js/compras.js?v=1.9"></script>
<script>	
	<?php
		$ajustes = $this->ajustes;
	?>
	var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
	var tasa_real = <?= $ajustes->tasa_reales ?>;
	var tasa_peso = <?= $ajustes->tasa_pesos ?>;
	var aplicar_sugerencia = <?= $ajustes->aplicar_sugerencia ?>;
	var codigo_balanza = <?= json_encode(explode(',',str_replace(', ',',',$ajustes->cod_balanza))) ?>;
	var controlar_vencimiento = <?= $ajustes->controlar_vencimiento_stock ?>;
	var firstLoad = false;
	window.sucursalConectada = '<?= $this->user->sucursal ?>';
	var cantidades_mayoristas = <?php 
        $ar = array();
        foreach($this->db->get('cantidades_mayoristas')->result() as $c){
            $ar[] = $c;
        }
        echo json_encode($ar);
	?>;
	var vender_sin_stock = <?= $ajustes->vender_sin_stock ?>;
	var onsend = false;	
	
	window.shouldSave = true;	
	window.addEventListener('beforeunload',function(e){
		if (window.shouldSave) {
	        e.preventDefault();
	        e.returnValue = '';
	        return;
	    }
	    delete e['returnValue'];
	});

	window.afterLoad.push(function(){
		window.compra = new Compras();	
		initDatos();
		compra.initEvents();
		compra.draw();
		compra.loadTemp();

		<?php 
			if(!empty($edit) && is_numeric($edit)): 			
		?>
			var edit = <?= count($nota)>0?json_encode($nota[0]):'{}' ?>;
			compra.datos = edit;
			compra.draw();		
		<?php endif ?>

		<?php 
			if(!empty($default)): 			
		?>
			var edit = <?= json_encode($default) ?>;
			compra.datos = edit;
			compra.draw();	
			compra.loadTemp = ()=>{};	
			compra.updateTemp = ()=>{};
		<?php endif ?>
		firstLoad = true;
	});
	

	function initDatos(){		
		compra.datos.caja = '<?= $this->user->caja ?>';
		compra.datos.cajadiaria = '<?= $this->user->cajadiaria ?>';
		compra.datos.fecha = '<?= date("d/m/Y") ?>';
	}	

	function selCod(codigo,esc,obj){		
		/*venta.addProduct(codigo);		
		$("#inventarioModal").modal('toggle');
		$("#codigoAdd").focus();*/
		$("#inventarioModal").modal('toggle');
		var ht = $("#codigoAdd").val();		
		compra.addProduct(ht+codigo,obj);			
		$("#codigoAdd").focus();
	}

	function saveCompra(){		
		if(!onsend){
			onsend = true;
			var datos =  JSON.parse(JSON.stringify(compra.datos));
			var productos = [];
			for(var i in datos.productos){
				var p = {};
				p.id = datos.productos[i].id;
				p.codigo = datos.productos[i].codigo;
				p.nombre_comercial = datos.productos[i].nombre_comercial;
				p.sucursal = datos.productos[i].sucursal;
				p.cantidad = datos.productos[i].cantidad;
				p.precio_venta = datos.productos[i].precio_venta;
				p.precio_credito = datos.productos[i].precio_credito;
				p.precio_venta_mayorista1 = datos.productos[i].precio_venta_mayorista1;
				p.precio_venta_mayorista2 = datos.productos[i].precio_venta_mayorista2;
				p.precio_venta_mayorista3 = datos.productos[i].precio_venta_mayorista3;
				p.precio_costo = datos.productos[i].precio_costo;
				p.por_desc = datos.productos[i].por_desc;
				p.total = datos.productos[i].total;				
				p.por_venta = datos.productos[i].por_venta;
				p.vencimiento = datos.productos[i].vencimiento;
				p.desc_compra = datos.productos[i].desc_compra;
				productos.push(p);
				if(isNaN(parseInt(datos.productos[i].sucursal))){
					onsend = false;
					alert('Todos los productos deben contener una sucursal de destino para poder ser procesados.');
					return false;
				}
			}
			datos.productos = JSON.stringify(productos);
			var accion = '<?= empty($edit)?'insert':'update/'.$edit  ?>';
			$("button").attr('disabled',true);
			insertar('movimientos/compras/compras/'+accion,datos,'.respuestaVenta',function(data){						
				if(data.success){
					var id = data.insert_primary_key;						
					var enlace = '';
					$(".respuestaVenta").removeClass('alert-info');
					imprimir(id);
					enlace = 'javascript:imprimir('+id+')';
					$('.respuestaVenta').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar <a href="'+enlace+'">este enlace</a></p>');				
					$("button").attr('disabled',false);
					onsend = false;
					firstLoad = false;
					nuevaCompra();
				}else{
					$(".respuestaVenta").removeClass('alert-info');
					$('.respuestaVenta').removeClass('alert alert-success').addClass('alert alert-danger').html(data.message);				
					$("button").attr('disabled',false);
					onsend = false;
				}
			},function(){
				onsend = false;
				$("button").attr('disabled',false);
			});
		}	
	}

	function imprimir(codigo){	
		var idReporte = '<?= $this->ajustes->id_reporte_compras ?>';
		window.open('<?= base_url() ?>reportes/rep/verReportes/'+idReporte+'/html/valor/'+codigo);	        
	}

	function nuevaCompra(){
		if(!firstLoad || confirm('¿Seguro desea aperturar un nuevo registro?, se perderá todo el progreso no guardado')){
			firstLoad = true;		
			compra.initDatos();
			initDatos();
			compra.draw();
		}
	}
</script>