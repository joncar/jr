<div id="tokenModal" class="modal fade" tabindex="-1" role="dialog">
  <form action='seguridad/validarToken' onsubmit="consultarToken(this); return false;" autocomplete="off">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">	        
	        <h4 class="modal-title">Clave de supervisor</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	  		<div class="row" style="margin-left: 0; margin-right: 0">

				<div class="col-12 col-md-12">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Contraseña</label>
				    <input type="text" class="form-control" name="token" placeholder="**************" autocomplete="off" style="text-security:disc; -webkit-text-security:disc; ">
				  </div>
				</div>

				<div class="col-12 col-md-12">
					<div class="resultToken"></div>
			  </div>
			</div>			  
	      </div>
	      <div class="modal-footer">	      	
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary">Guardar</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->

<script>
  window.afterLoad.push(function(){
  	$("#tokenModal").on("hidden.bs.modal",function(){      
      $("#tokenModal").trigger('uncomplete');
      $("#tokenModal [name='token']").val('');
    });
    $("#tokenModal").on("shown.bs.modal",function(){      
      $("#tokenModal [name='token']").focus();
    });
  });
  function consultarToken(f){
  	sendForm(f,'.resultToken',function(data){  	
  		data = JSON.parse(data);
  		$("#tokenModal [name='token']").val('');
  		if(data.result=='fail'){
  			error('.resultToken',data.msj);
  			$("#tokenModal [name='token']").focus();
  		}else{  			
  			$('.resultToken').removeClass('alert alert-success alert-info alert-danger').html('');
  			$("#tokenModal").trigger('complete');
  		}
  	});
  }

  function validarToken(){
  	return new Promise((resolve,reject)=>{
  		$("#tokenModal").modal('show');
  		
  		$("#tokenModal").unbind('complete');
		$("#tokenModal").unbind('uncomplete');
  		$("#tokenModal").on('complete',function(){  			
  			resolve(true);
  			$("#tokenModal").modal('hide');
  		});
  		$("#tokenModal").on('uncomplete',function(){  			
  			resolve(false);
  		});
  	});  	
  }
</script>