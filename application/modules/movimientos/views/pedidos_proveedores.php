<?php
	if(!empty($edit) && is_numeric($edit)){
		$nota = $this->elements->pedidos_proveedores(array('id'=>$edit),null,false);
	}
?>
<style>
	.panel{
		border-radius:0px;
	}

	.error{
		border: 1px solid red !important;
	}

	.patternCredito{
		background: #ffffffa6;width: 100%;height: 100%;position: absolute;top: 0px;left: 0px;
	}
</style>
<div class="kt-portlet">
	<div class="kt-portlet__body" style="padding:10px 25px;">
		<div class="kt-section">
			<div class="row" style="position: relative;">
				<!--<div style="width:100%; text-align: right; position:absolute; left:0; padding:0 30px">
					#FACTURA: <span id="nroFactura"></span>
				</div>-->
				<div class="col-12 col-md-10">
					<div class="row">						
						<div class="col-12 col-md-3">
							Sucursal:
							<?php 
								//$this->db->limit(1);
								echo form_dropdown_from_query('sucursal','sucursales','id','denominacion',$this->user->sucursal,'id="sucursales"') 
							?>					
						</div>
						
						<div class="col-12 col-md-3">
							Tipo facturación: 
							<?= form_dropdown_from_query('tipo_facturacion_id','tipo_facturacion','id','denominacion',1) ?>
						</div>
						
						<div class="col-12 col-md-3">
							Forma de Pago: 
							<?php 
								echo form_dropdown_from_query('forma_pago',$this->db->get('formapago'),'id','denominacion',1,'id="formapago"')
							?>
						</div>

						
						<div class="col-12 col-md-3">
							Transacción: 
							<?= form_dropdown_from_query('transaccion','tipotransaccion','id','denominacion',1,'id="transaccion"'); ?>
						</div>
						
						<div class="col-12 col-md-3">
							Proveedor: 
							<?php 
								echo form_dropdown_from_query('proveedor','proveedores','id','denominacion',1,'id="proveedores"');
							?>
						</div>
						<div class="col-12 col-md-3">
							Fecha: 
							<input type="text" name="fecha" class="form-control" id="fecha" value="<?= date("d/m/Y") ?>" placeholder="Fecha">
						</div>	
						<div class="col-12 col-md-3">
							Vencimiento Pago: 
							<input type="text" name="vencimiento_pago" class="form-control" id="vencimiento_pago" value="" placeholder="Vencimiento de pago">
						</div>
						<div class="col-12 col-md-3">
							Pago caja diaria: 
							<?php 
								echo form_dropdown('pago_caja_diaria',array('0'=>'NO','1'=>'SI'),0,'id="pagocajadiaria" class="form-control"')
							?>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-2" style="position: relative;">
					<span style="position: absolute;top: 26px;left: 30px;font-weight: bold;">Total Pedido: </span>
					<input type="text" name="total_compra" class="form-control" readonly="" value="300.000" style="font-size:30px; font-weight:bold; text-align: right;height: 89px;vertical-align: baseline;margin-top: 19px; background:lightgreen">
				</div>

			</div>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-12 respuestaVenta"></div>
</div>

<div class="row">
	<div class="col-12 col-md-10">
		<div class="kt-portlet">
			<div class="kt-portlet__body" style="padding-top:0">
				<div class="kt-section">
					<div style="height:350px; overflow-y:auto">
						<table class="table table-bordered" id="ventaDescr">
							<thead style="background: #f2f3f8;">
								<tr>
									<th>Código</th>
									<th>Nombre</th>
									<th>Cantidad</th>
									<th>P.Costo</th>
									<th>%Desc</th>
									<th>%Venta</th>
									<th>P.Venta</th>
									<th>Total</th>
									<th>Venc.</th>
									<th>Stock</th>
								</tr>
							</thead>
							<tbody>

								<tr id="productoVacio">
									<td tabindex="1">
										<a href="javascript:void(0)" class="rem" style="display:none;color:red">
											<i class="fa fa-times"></i>
										</a> 
										<span>&nbsp;</span>
									</td>
									<td tabindex="2">&nbsp;</td>
									<td tabindex="3"><input name="cantidad" class="cantidad form-control" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td tabindex="4"><input name="precio_costo" class="precio_costo form-control" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0"></td>
									<td tabindex="5"><input name="por_desc" class="por_desc form-control" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td tabindex="6"><input name="por_venta" class="por_venta form-control" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td tabindex="7"><input name="precio_venta" class="precio_venta form-control" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>									
									<td tabindex="8">&nbsp;</td>
									<td tabindex="9"><input name="vencimiento" class="vencimiento form-control" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value=""></td>
									<td tabindex="10">&nbsp;</td>
								</tr>

								
							</tbody>
						</table>
					</div>
				</div>

				<?php if(empty($edit)): ?>
					<div class="panel-footer">
						<div class="row" style="background: #f2f3f8;padding: 6px;">
							<div class="col-12 col-md-2" style="text-align: center;padding: 6px;">
								Cant: <span id="cantidadProductos">4</span>					
							</div>
							<div class="col-12 col-md-10" style="position: relative;">
								<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
								<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" style="padding-left: 25px;padding-right: 73px;">
								<button style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar insertar">Insertar</button>

								<div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
							</div>
						</div>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>
	<div class="col-12 col-md-2" style="padding-left: 5px;">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						<b>Resumen de pedido</b>
					</h1>
				</div>
			</div>
			<div class="kt-portlet__head">
				<div class="row">
					<div class="col-12 col-md-4">Pesos: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_pesos" value="300.000" readonly="">
					</div>
					<div class="col-12 col-md-4">Reales: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_reales" value="300.000" readonly="">
					</div>
					<div class="col-12 col-md-4">Dolares: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_dolares" value="300.000" readonly="">
					</div>
				</div>
			</div>
			<!--<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h1 class="kt-portlet__head-title">
							<b>Atajos</b>
						</h1>
					</div>
				</div>
				<div class="kt-portlet__body">
					<span>(ALT+C) <small>Enfocar busqueda por código</small></span><br/>
					<span>(ALT+I) <small>Mostrar busqueda avanzada</small></span><br/>
					<span>(ALT+P) <small>Procesar venta</small></span><br/>
					<span>(ALT+N) <small>Nueva venta</small></span><br/>
					<span>(ALT+B) <small>Consultar saldo del cliente</small></span><br/>
				</div>
			</div>-->
		</div>

		
	</div>
</div>

<div class="row">
	<div class="col-12 col-md-10">
		<div class="btn-group btn-group-justified" role="group" aria-label="...">
		  <div class="btn-group" role="group">
		    <button type="button" data-toggle="modal" data-target="#addProduct" class="btn btn-default" style="color:#000 !important"><i class="fa fa-columns"></i> Añadir producto</button>
		  </div>

		  <?php if(empty($edit)): ?>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-success" style="color:#000 !important" onclick="nuevaCompra();"><i class="fa fa-plus-circle"></i> Nuevo pedido</button>
		  </div>
		  <?php else: ?>
		  	<div class="btn-group" role="group">
		    	<a href="<?= base_url() ?>movimientos/PedidosProveedores/pedidos_proveedores/add" class="btn btn-success" style="color:#000 !important"><i class="fa fa-plus-circle"></i> Nueva compra</a>
		  	</div>
		  <?php endif ?>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary" onclick="send()"><i class="fa fa-floppy-o"></i> Procesar pedido</button>
		  </div>
		  <!--<div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" style="color:#000 !important"><i class="fa fa-print"></i> Imprimir</button>
		  </div>-->
		</div>
	</div>
</div>


<div id="addProduct" class="modal fade" tabindex="-1" role="dialog">
  <form onsubmit="insertar('movimientos/productos/productos/insert',this,'.resultClienteAdd'); return false;">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Agregar producto</h4>
	      </div>
	      <div class="panel-body">
	  		  <div class="row" style="margin-left: 0; margin-right: 0">
  		  		<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Código interno</label>
				    <input type="text" class="form-control" name="codigo_interno">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Código</label>
				    <input type="text" class="form-control" name="codigo">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Nombre comercial</label>
				    <input type="text" class="form-control" name="nombre_comercial">
				  </div>
				</div>
				

				
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Categoria</label>
				    <?php echo form_dropdown_from_query('categoria_id', 'categoriaproducto', 'id', 'denominacion', 1, 'id="field-categoriaproducto"'); ?>
				  </div>
				</div>


				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Precio de costo</label>
				    <input type="text" class="form-control" name="precio_costo">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Precio de Venta</label>
				    <input type="text" class="form-control" name="precio_venta">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Precio Mayorista 1</label>
				    <input type="text" class="form-control" name="precio_venta_mayorista1">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				   <div class="form-group">
				    <label for="exampleInputEmail1">Precio Mayorista 2</label>
				    <input type="text" class="form-control" name="precio_venta_mayorista2">
				  </div>
				</div>
				<div class="col-12 col-md-4" style="height:80px">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Precio Mayorista 3</label>
				    <input type="text" class="form-control" name="precio_venta_mayorista3">
				  </div>
				</div>

				<input type="hidden" name="unidad_medida_id" value="1">
				<input type="hidden" name="iva_id" value="10">
				<div class="col-12 col-md-12">
					<div class="resultClienteAdd"></div>
			    </div>
			</div>			  
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary">Guardar</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->



<?php $this->load->view('_inventario_modal_compras',array(),FALSE,'movimientos'); ?>
<script src="<?= base_url() ?>js/PedidosProveedor.js?v=1.2"></script>


<script>	
	<?php
		$ajustes = $this->ajustes;
	?>
	var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
	var tasa_real = <?= $ajustes->tasa_reales ?>;
	var tasa_peso = <?= $ajustes->tasa_pesos ?>;
	var codigo_balanza = <?= json_encode(explode(',',str_replace(', ',',',$ajustes->cod_balanza))) ?>;
	window.sucursalConectada = '<?= $this->user->sucursal ?>';
	var cantidades_mayoristas = <?php 
        $ar = array();
        foreach($this->db->get('cantidades_mayoristas')->result() as $c){
            $ar[] = $c;
        }
        echo json_encode($ar);
	?>;
	var vender_sin_stock = <?= $ajustes->vender_sin_stock ?>;
	var onsend = false;	
	

	window.afterLoad.push(function(){
		window.compra = new Compras();	
		initDatos();
		compra.initEvents();
		compra.draw();

		<?php 
			if(!empty($edit) && is_numeric($edit)): 			
		?>
			var edit = <?= count($nota)>0?json_encode($nota[0]):'{}' ?>;
			compra.datos = edit;		
			console.log(compra.datos);
			compra.draw();		
		<?php endif ?>

		<?php 
			if(!empty($default)): 			
		?>
			var edit = <?= json_encode($default) ?>;
			compra.datos = edit;		
			console.log(compra.datos);
			compra.draw();		
		<?php endif ?>
	});
	

	function initDatos(){		
		compra.datos.caja = '<?= $this->user->caja ?>';
		compra.datos.cajadiaria = '<?= $this->user->cajadiaria ?>';
		compra.datos.fecha = '<?= date("d/m/Y") ?>';
	}	

	function selCod(codigo){		
		compra.addProduct(codigo);		
		$("#inventarioModal").modal('toggle');
	}

	function send(){		
		if(!onsend){
			onsend = true;
			var datos =  JSON.parse(JSON.stringify(compra.datos));
			var productos = [];
			for(var i in datos.productos){
				var p = {};
				p.id = datos.productos[i].id;
				p.codigo = datos.productos[i].codigo;
				p.nombre_comercial = datos.productos[i].nombre_comercial;
				p.cantidad = datos.productos[i].cantidad;
				p.precio_venta = datos.productos[i].precio_venta;
				p.precio_costo = datos.productos[i].precio_costo;
				p.por_desc = datos.productos[i].por_desc;
				p.total = datos.productos[i].total;				
				p.por_venta = datos.productos[i].por_venta;
				productos.push(p);
			}
			datos.productos = JSON.stringify(productos);
			var accion = '<?= empty($edit)?'insert':'update/'.$edit  ?>';
			$("button").attr('disabled',true);
			insertar('movimientos/PedidosProveedores/pedidos_proveedores/'+accion,datos,'.respuestaVenta',function(data){						
				if(data.success){
					var id = data.insert_primary_key;						
					var enlace = '';
					$(".respuestaVenta").removeClass('alert-info');
					imprimir(id);
					enlace = 'javascript:imprimir('+id+')';
					$('.respuestaVenta').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar <a href="'+enlace+'">este enlace</a></p>');				
					$("button").attr('disabled',false);
					onsend = false;
					nuevaCompra();
				}else{
					$(".respuestaVenta").removeClass('alert-info');
					$('.respuestaVenta').removeClass('alert alert-success').addClass('alert alert-danger').html(data.message);				
					$("button").attr('disabled',false);
					onsend = false;
				}
			},function(){
				onsend = false;
				$("button").attr('disabled',false);
			});
		}	
	}

	function imprimir(codigo){	
		var idReporte = 137;
		window.open('<?= base_url() ?>reportes/rep/verReportes/'+idReporte+'/html/valor/'+codigo);	        
	}

	function nuevaCompra(){
		compra.initDatos();
		initDatos();
		compra.draw();
	}
</script>