<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h1 class="kt-portlet__head-title">
				<b>Entrada de productos</b>
			</h1>
		</div>
	</div>
	<div class="kt-portlet__body">
		<div class="kt-section">

<div class="alert <?= !empty($_SESSION['msj'])?'alert-success':'' ?>" style="<?= !empty($_SESSION['msj'])?'':'display:none' ?>"><?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?></div>
<?php $_SESSION['msj'] = !empty($_SESSION['msj'])?'':'' ?>
<form class="form-horizontal" id='formulario' onsubmit="return val_send(this)" role="form">
    <div class="well">
    <div class="row">
        <div class="col-3">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Proveedor: </label>
              <div class="col-sm-8" id="cliente_div">
                  <? $sel = empty($entrada)?0:$entrada->proveedor; ?>
                  <?= form_dropdown_from_query('proveedor','proveedores','id','denominacion',$sel,'id="proveedor"') ?>
              </div>
            </div>
        </div> 
        <div class="col-3">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Motivo: </label>
              <div class="col-sm-8" id="cliente_div">
                  <? $sel = empty($entrada)?0:$entrada->motivo; ?>
                  <?= form_dropdown_from_query('motivo','motivo_entrada','id','denominacion',$sel,'id="motivo"') ?>
              </div>
            </div>
        </div> 
        <div class="col-3">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Fecha: </label>
              <div class="col-sm-8">
                  <input type="text" name="fecha" value="<?= empty($entrada)?date("d/m/Y H:i:s"):date("d/m/Y H:i:s",strtotime($entrada->fecha)) ?>" id="fecha" class="datetime-input form-control">
              </div>
            </div>
        </div> 
        <div class="col-2">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Total Monto: </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" name="total_monto" id="total_costo" value="<?= empty($entrada)?0:$entrada->total_monto ?>">
              </div>
            </div>
        </div>        
    </div>    
    </div>
    <div class="row">
        <div class="col-12">Detalle de salida <a href="javascript:advancesearch()" class="btn btn-default">Busqueda avanzada de productos</a></div>
        <div class="col-12">
            <table class="table table-striped" style="font-size:12px;" cellspacing="2">
                <thead>
                    <tr>
                        <th style="width:18%">Código</th>
                        <th style="width:21%">Nombre artículo</th>
                        <th style="width:7%">Lote</th>
                        <th style="width:9%">Vence</th>
                        <th style="width:9%">Cant.</th>
                        <th style="width:9%">P.Costo</th>
                        <th style="width:9%">P.Venta</th>
                        <th style="width:10%">Total</th>
                        <th style="width:10%">Sucursal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($detalles)): foreach($detalles->result() as $d): ?>
                    <tr>
                        <td><input name="codigo[]" type="text" value='<?= $d->producto ?>' class="form-control codigo" placeholder="Codigo"></td>
                        <td><input name="producto[]" type="text" value='<?= $this->db->get_where('productos',array('codigo'=>$d->producto))->row()->nombre_comercial ?>'  class="form-control producto" readonly placeholder="Nombre"></td>
                        <td><input name="lote[]" type="text" value='<?= $d->lote ?>'  class="form-control lote" placeholder="Lote"></td>
                        <td><input name="vencimiento[]" type="text" value='<?= $d->vencimiento!='1969-12-31'?date("d/m/Y",strtotime($d->vencimiento)):'' ?>'  class="form-control vencimiento datetime-input" placeholder="Vencimiento"></td>
                        <td><input name="cantidad[]" type="text" value='<?= $d->cantidad ?>'  class="form-control cantidad" placeholder="Cantidad"></td>
                        <td><input name='precio_costo[]' type="text" value='<?= $d->precio_costo ?>'  class="form-control precio_costo" placeholder="Precio costo"></td>
                        <td><input name='precio_venta[]' type="text" value='<?= $d->precio_venta ?>'  class="form-control precio_venta" placeholder="Precio venta"></td>
                        <td><input name='total[]' type="text" value='<?= $d->total ?>'  class="form-control total" placeholder="Total">
                        <td><?= form_dropdown_from_query('sucursal[]','sucursales','id','denominacion',$d->sucursal,'id="sucursal"',FALSE) ?>
                        </td>                                                
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                    <?php  endforeach; endif ?>
                    <tr>
                        <td><input name="codigo[]" type="text" class="form-control codigo" placeholder="Codigo"></td>
                        <td><input name="producto[]" type="text" class="form-control producto" readonly placeholder="Nombre"></td>
                        <td><input name="lote[]" type="text" class="form-control lote" placeholder="Lote"></td>
                        <td><input name="vencimiento[]" type="text" class="form-control vencimiento datetime-input" placeholder="Vencimiento"></td>
                        <td><input name="cantidad[]" type="text" class="form-control cantidad" placeholder="Cantidad"></td>
                        <td><input name='precio_costo[]' type="text" class="form-control precio_costo" placeholder="Precio costo"></td>
                        <td><input name='precio_venta[]' type="text"   class="form-control precio_venta" placeholder="Precio venta"></td>
                        <td><input name='total[]' type="text" value=''  class="form-control total" placeholder="Total"></td>
                        <td><?= form_dropdown_from_query('sucursal[]','sucursales','id','denominacion','','id="sucursal"',FALSE) ?></td>
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <?php if(!empty($entrada)): ?><input type="hidden" name="id" id="id" value="<?= $entrada->id ?>"><?php endif ?>
    <div class="row" style="margin-top:40px">                
        <button type="submit" id="guardar" class="btn btn-success">Guardar Entrada</button>
        <a href="#" class="btn btn-default">Imprimir Reporte</a>
    </div>
    </form>
</div></div>
<script>
    
    
    window.afterLoad.push(function(){    
        //Eventos
        $(document).on('keydown','input',function(event){        
                
            if (event.which == 13){
                if($(this).hasClass('total')){
                    addrow($(this));
                }
                var inputs = $(this).parents("form").eq(0).find(":input");
                var idx = inputs.index(this);
                if (idx == inputs.length - 1) {
                    inputs[0].select()
                } 
                else if($(this).val()!='' && $(this).hasClass('codigo')){
                    $(this).trigger('change');                
                }
                else if($(this).val()=='' && $(this).hasClass('codigo')){
                    return false;
                }
                else
                {
                    inputs[idx + 1].focus(); //  handles submit buttons
                    inputs[idx + 1].select();
                }
                return false;
            }
        });
        
        $("body").on('click','.addrow',function(e){
            e.preventDefault();        
            addrow($(this).parent('p'));
        })

        $("body").on('click','.remrow',function(e){
            e.preventDefault();
            removerow($(this).parent('p'));
        }); 
        
       $("body").on('change','.codigo',function(){
           if($(this).val()!=''){
            focused = $(this);
            elements = $(".codigo").length;
            focusedcode = $(this).val();        
            $.post('<?= base_url('json/getProduct') ?>',{codigo:$(this).val()},function(data){
                data = JSON.parse(data);
                data = data['producto'];
                if(data!=''){
                focused = focused.parent().parent();
                focused.find('.producto').val(data['nombre_comercial'])
                //focused.find('.cantidad').val(1);                    
                focused.find('.total').val(data['precio_venta']);  
                focused.find('.precio_costo').val(data['precio_costo']);  
                $(document).trigger('total');
                addrow(focused.find('a'));
                $("tbody tr").last().find('.codigo').focus();
                }
                else{
                    emergente('El producto ingresado no se encuentra registrado. <a target="_new" href="<?= base_url($this->router->fetch_class().'/productos/add/json') ?>/'+focused.val()+'">¿Desea registrar uno nuevo?</a>');                        
                }                                        
            });        
            }
        });
        
        $(document).on('change','.cantidad',function(){$(document).trigger('total')});
        
        $(document).on('total',function(){      
            $("#total_costo").val(0);
            $("#total_iva").val(0);
            $("#iva").val(0);
            $("#iva2").val(0);
            $("#exenta").val(0);
            $("#total_descuentos").val(0);
            $("tbody tr").each(function(){
                self = $(this);            
                total = parseFloat(self.find('.cantidad').val())*parseInt(self.find('.precio_costo').val());                                    
                if(!isNaN(total)){
                    self.find('.total').val(total);
                    /*Total Venta*/
                    total_venta = parseInt($("#total_costo").val());            
                    total_venta += total;
                    $("#total_costo").val(total_venta);
                }
            });
        });
    });
    function addrow(obj){
        var row = $(obj).parents('tr');
            row.after('<tr>'+row.html()+'</tr>');
            $(".total").attr('readonly',true);
            $(".hasDatepicker").removeAttr('id').removeClass('hasDatepicker');            
            date_init_calendar();
    }
    function removerow(obj){
        $(obj).parent('td').parent('tr').remove();
        $(document).trigger('total');
    }
    
    function tryagain(){
        $.post('<?= base_url('json/clientes') ?>',{},function(data){
            
            $("#cliente_div #cliente_chzn").remove();
            $("#cliente").html(data);
            $("#cliente").removeClass('chzn-done');            
            $(".chosen-select,.chosen-multiple-select").chosen({allow_single_deselect:true});
        });
    }
    
    function val_send(form){
        $(".mask").show();
        var data = document.getElementById('formulario');
        $(document).trigger('total');
        $("#guardar").attr('disabled','disabled');
        data = new FormData(data);
        $.ajax({
            url:'<?= empty($entrada)?base_url('json/entradas'):base_url('json/entradas/edit') ?>',
            method:'post',
            data:data,
            processData:false,
            cache: false,
            contentType: false,
            success:function(data){
                data = JSON.parse(data);
                if(data['status']){
                    document.location.reload();
                }
                else{
                    $(".alert").removeClass('alert-success').addClass('alert-danger').html(data['message']).show();
                    $(".mask").hide();
                }
                $("#guardar").removeAttr('disabled');
            },error:function(){
                 $(".mask").hide();
             }
            });
            return false;
    }
    
    function advancesearch()
    {
        $.post('<?= base_url('json/searchProduct/0') ?>',{},function(data){
            emergente(data);
        })
    }
    
    function selCod(cod,lote,datos){
        
        $("tbody tr:last-child").find('.codigo').val(cod);
        $("tbody tr:last-child").find('.lote').val(lote);
        $("tbody tr:last-child").find('.cantidad').val(datos['cantidad']);
        $("tbody tr:last-child").find('.vencimiento').val(datos['vencimiento']);
        $("tbody tr:last-child").find('.codigo').trigger('change');        
        $("tbody tr:last-child").find('.precio_costo').val(datos['precio_costo']);        
        $('#myModal').modal('hide');
    }    
</script>
<?php $this->load->view('predesign/datepicker') ?>
<?= $this->load->view('predesign/chosen.php') ?>
<div class="mask" style="background:rgba(0,0,0,.8); width:100%; height:100%; position:fixed; top:0px; left:0px; display:none;"></div>