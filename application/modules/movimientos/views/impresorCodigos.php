<style>
	div.banderas{
		background-image:url('https://dev.binario.com.py/sysventas/img/banderas.jpg');
		width:20px;
			height:20px;
		position: absolute;
		right:0px;
		top:0px;
			background-size: 100%;
		}
	div.banderas.us{
		background-position: 0 0px;
	}
	div.banderas.ar{
		background-position: 0 -20px;
			}
	div.banderas.br{
		background-position: 0 -40px;
	}
	div.banderas.py{
		background-position: 0 -60px;
	}
	div.banderas-2x{
		background-image:url('https://dev.binario.com.py/sysventas/img/banderas.jpg');
		width:40px;
			height:40px;
		position: absolute;
		right:0px;
		top:0px;
			background-size: 100%;
		}
	div.banderas-2x.us{
		background-position: 0 0px;
	}
	div.banderas-2x.ar{
		background-position: 0 -40px;
			}
	div.banderas-2x.br{
		background-position: 0 -80px;
	}
	div.banderas-2x.py{
		background-position: 0 -120px;
	}

	.form-control-plaintext{
		text-align: right
	}
	.form-control-plaintext {	  
	  font-size: 24px;
	}
	.oculto{
		font-size:16px;
	}

	.unidad.active,
	.cantidad.active,
	.caja.active{
		background: #fe822b;
	}
</style>
<div class="row">
	
	<div class="col-12">
		<div class="kt-portlet">
			<div class="kt-portlet__body" style="padding:10px 25px;">
				<div class="kt-section">
					<div class="kt-portlet__head">
						<form id="consultor" action="movimientos/impresorCodigos/productos" onsubmit="consultar(this,'.response');return false;">
							<div class="form-group" style="position: relative;">
								<label for="producto">Código de producto [ALT+C]</label>
								<input type="text" class="form-control" id="producto" name="producto">
								<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 40px;right: 10px;cursor:pointer;"></i>
								<small id="emailHelp" class="form-text text-muted">Ingresa el código del producto para consultarlo.</small>								
							</div>
							
							<button type="submit" class="btn btn-primary">Consultar</button>
							<div class="response" style="margin-top:50px"></div>
						</form>
						
					</div>
					
					<div class="row">
						<div class="col-12">							
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-1 col-form-label">Código: </label>
								<div class="col-sm-11">
									<input style="background: #5867dd;color: #fff;text-align: right;padding: 8px 20px;" type="text" name="codigo" readonly="" class="form-control-plaintext" id="staticEmail" value="">
								</div>
							</div>
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-1 col-form-label">Código Interno: </label>
								<div class="col-sm-11">
									<input style="background: #5867dd;color: #fff;text-align: right;padding: 8px 20px;" type="text" name="codigo_interno" readonly="" class="form-control-plaintext" id="staticEmail" value="">
								</div>
							</div>

							<div class="form-group row">
								<label for="staticEmail" class="col-sm-1 col-form-label">Nombre: </label>
								<div class="col-sm-11">
									<input style="background: #5867dd;color: #fff;text-align: right;padding: 0px 22px;" type="text" name="nombre" readonly="" class="form-control-plaintext" id="staticEmail" value="">
								</div>
							</div>

							<div class="form-group">
								<a href="javascript:;" id="barra" class="btn btn-info">Imprimir Barra</a>
								<a href="javascript:;" id="gondola" class="btn btn-info">Gondola</a>
							</div>
							

							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
<?php $this->load->view('_inventario_modal',array(),FALSE,'movimientos'); ?>
<script>
	function consultar(f){
		var cod = $("input[name='producto']").val();
		var cant = $("input[name='cantidad']").val();
		if(cod!=''){
			$.post('<?= base_url('movimientos/impresorCodigos/productos') ?>',{producto:cod,cantidad:cant},function(data){
				data = JSON.parse(data);				
				if(typeof data.codigo!='undefined'){
					$("input[name='codigo']").val(data.codigo);
					$("input[name='codigo_interno']").val(data.codigo_interno);
					
					$("input[name='nombre']").val(data.nombre_comercial);					
					$("input[name='precio_venta']").val(data.precio_venta);
					$("input[name='precio_venta_mayorista1']").val(data.precio_venta_mayorista1);
					$("input[name='precio_venta_mayorista2']").val(data.precio_venta_mayorista2);
					$("input[name='precio_venta_mayorista3']").val(data.precio_venta_mayorista3);
					$('[data-id="cant1"]').html(data.cant_1);
					$('[data-id="cant1+1"]').html(parseInt(data.cant_1)+1);					
					$('[data-id="cant2"]').html(data.cant_2);
					$('[data-id="cant3"]').html(data.cant_3);
					$('input[name="producto"]').val('');
					$('input[name="cantidad"]').val(1);
					$('input[name="producto"]').attr('placeholder','');
					$('#foto_principal').attr('src',data.foto_principal);
					$("#stockage").html(data.stockage);
					$(".cantidad,.precio").removeClass('active');
					$("."+data.cant_elegida).addClass('active');

					/*$("#barra").attr('href','<?= base_url() ?>/reportes/rep/verReportes/71/html/codigo/'+data.id);
					$("#gondola").attr('href','<?= base_url() ?>/reportes/rep/verReportes/174/html/codigo/'+data.id);*/
					$("#barra").attr('onclick','imprimir("<?= base_url() ?>reportes/rep/verReportes/257/html/valor/'+data.id+'")');
					$("#gondola").attr('onclick','imprimir("<?= base_url() ?>reportes/rep/verReportes/174/html/valor/'+data.id+'")');
					
				}else{
					$("input").val('');
					$('input[name="producto"]').val('');
					$('input[name="producto"]').attr('placeholder','Producto no encontrado');
				}				
			});		
		}
	}
	function imprimir(url){
		window.open(url,'Impresion','width=800,height=600');
	}
	var alt = false;
	window.afterLoad.push(function(){
		$('body').attr('class','kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize');
		$("input[name='producto']").focus();
		
		$("body").on('click',function(e){			
			if($(e.target).attr('name')!='cantidad' && $(e.target).attr('name')!='search_text[]'  && !$(e.target).hasClass('tipo_precio')){
				$("input[name='producto']").focus();
			}
		});

		$(document).on('change','.tipo_precio',function(){
			$(".precios").hide();
			$("."+$(this).val()).show();
		});

		$(document).on('keydown',function(e){
            if(e.which==18 && !alt){
                e.preventDefault();
                alt = true;
            }
            else if(e.which==18 && alt){
                e.preventDefault();
                alt = false;
            }
            if(alt){
                e.preventDefault();
            }
        });

	    $(document).on('keyup',function(e){                        
	    	if(alt){   

                switch(e.which){
                    case 67: //[c] Focus en codigos
                        $("#producto").focus();
                        alt = false;
                    break;
                    case 73: //[i] busqueda avanzada
                        $("#inventarioModal").modal('toggle');
                        alt = false;
                    break;
                }
            }
	    });
	});

	function selCod(codigo,esc,obj){				
		$("#inventarioModal").modal('toggle');
		$("#producto").val(codigo);
		$("#producto").focus();
		$("form#consultor").submit();
	}

</script>
<!--End::Dashboard 1-->