<style>
  #inventarioModal tbody.ajax_list tr.active{
    background:#5867dd;
    color:#fff;
  }

  #inventarioModal .table td{
    font-size:14px;
  }

  #inventarioModal tbody.ajax_list tr.active a{
    color:#fff;
  }

  #inventarioModal .kt-portlet__head{
    display: none;
  }

  #inventarioModal .modal-body, #inventarioModal .kt-portlet__body{
    padding:0;    
  }

  #inventarioModal .modal-body{
    overflow-y: auto;
    height: 70vh;
  }

  #inventarioModal .table-bordered th{
    border: 0px;
  }

  #inventarioModal th:first-child, #inventarioModal td:first-child{
    display: none
  }

  #inventarioModal .form-control{
    padding: 5px;
    font-size: 11px;
  }

  #inventarioModal table th:nth-child(4) {
    width: 32%;
  }
</style>
<div id="inventarioModal" class="modal fade in" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Inventario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>        
      </div>
      <div class="modal-body">
        <?php 
          $controller = 'productos';
          if(empty($_SESSION['user'])){
            $controller = 'productosFrontend';
          }
          $this->load->library('grocery_crud')
                     ->library('ajax_grocery_crud');
          $crud = new ajax_grocery_crud();
          $crud->set_table('productos')
               ->set_subject('Inventario')
               ->set_theme('bootstrap')
               ->unset_add()
               ->unset_edit()
               ->unset_delete()
               ->unset_print()
               ->unset_export()
               ->unset_read()
               ->unset_jquery()
               ->unset_jquery_ui()
               ->set_primary_key('id')                                             
               ->display_as('sucursales_id','Sucursal')
               ->columns('codigo','codigo_interno','nombre_comercial','precio_venta');
          $crud->per_page(100)
               ->set_url('movimientos/'.$controller.'/productos_modal/');
          
          //if(get_instance()->router->fetch_class()=='compras' || get_instance()->router->fetch_class()=='presupuesto' || get_instance()->router->fetch_class()=='ventas'){
            $crud->search_types = [
              'codigo'=>'
              <div class="input-group">                  
                  <input type="hidden" name="search_text[]">
                  <input type="text" name="codigo_list" class="form-control">
                  <div class="input-group-prepend" style="cursor:pointer" onclick="jQuery(this).parents(\'form\').submit();">
                    <div class="input-group-text"><i class="fa fa-chevron-right"></i></div>
                  </div>
              </div>                
              '
            ];
          //}

          $crud = $crud->render(1);
          echo $crud->output;
          ?>

          <?php get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>
          <script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>
          <script src="'.base_url().'assets/grocery_crud/js/common/list.js"></script>          
          <script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/cookies.js"></script>          
          <script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>
          <script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js"></script>
          <script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.printElement.min.js"></script>
          <script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/pagination.js"></script>
          <script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js"></script>
          <script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js"></script>
          <script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid.js"></script>
          '; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<script>
  window.afterLoad.push(function(){
    window.inventarioIsOpen = false;
    window.inventarioItemFocus = -1;
    $("#inventarioModal").on("shown.bs.modal",function(){
      $("#inventarioModal .searchRow").removeClass('d-none');
      setTimeout(function(){
        $($("#inventarioModal input[type='text'")[0]).focus();
        $('select[name="per_page"]').val(100).trigger('change') 
      },600);
      window.inventarioIsOpen = true;
    });

    $("#inventarioModal").on("hidden.bs.modal",function(){      
      window.inventarioIsOpen = false;
      window.inventarioItemFocus = -1;
      <?php if(get_instance()->router->fetch_class()!='presupuesto'): ?>
        $("#inventarioModal input[name='search_text[]'],#inventarioModal input[name='codigo_list']").val('');
      <?php endif ?>
      setTimeout(function(){$("#codigoAdd").focus();},600);
    });

    $("#inventarioModal input[name='search_text[]']").on('focus',function(){
      window.inventarioItemFocus = -1;
    });

    $("#inventarioModal input").on('keyup',function(e){
      if(e.which!=38 && e.which!=40 && e.which!=13){
        window.inventarioItemFocus = -1;
      }
    });

    $(document).on('keydown',function(e){
        var trs = $("#inventarioModal tbody.ajax_list tr");
        if(window.inventarioIsOpen){
            switch(e.which){
                case 38: //UP
                e.preventDefault();
                if(window.inventarioItemFocus>0){      
                  window.inventarioItemFocus = window.inventarioItemFocus-1;            
                  var h = $("#inventarioModal .modal-body").height();
                  var atop = $("#inventarioModal .modal-body").scrollTop();
                  var actual_top = $(trs[window.inventarioItemFocus]).offset().top;                  
                  var top = parseFloat($(trs[window.inventarioItemFocus]).attr('data-position'));
                  if(actual_top<h){
                    //var newTop = atop-h;
                    var newTop = top - h;
                    $("#inventarioModal .modal-body").scrollTop(newTop);
                  }
                  $($("#inventarioModal input[name='search_text[]']")[1]).blur();
                }else{
                  window.inventarioItemFocus = -1;
                }
                break;
                case 40: //Down 
                e.preventDefault();                                 
                if(window.inventarioItemFocus<$("#inventarioModal tbody.ajax_list tr").length-1){                  
                  window.inventarioItemFocus = window.inventarioItemFocus+1;              
                  var h = $("#inventarioModal .modal-body").height();
                  var atop = $("#inventarioModal .modal-body").scrollTop();
                  //var top = $(trs[window.inventarioItemFocus]).offset().top;
                  var top = parseFloat($(trs[window.inventarioItemFocus]).attr('data-position'));
                  if(top>h){
                    //var newTop = atop+top;
                    var newTop = top - h;
                    $("#inventarioModal .modal-body").scrollTop(newTop);
                  }
                  $($("#inventarioModal input[name='search_text[]']")[1]).blur();
                }
                break;
            }
        }

      });    

    $(document).on('keyup',function(e){
      var trs = $("#inventarioModal tbody.ajax_list tr");
      if(window.inventarioIsOpen){
          switch(e.which){
              case 13: //Enter
              if(window.inventarioItemFocus>=0 && window.inventarioItemFocus<trs.length){                
                e.preventDefault();                
                document.location.href = $($("#inventarioModal tbody.ajax_list tr.active a")[0]).attr('href');
              }
              break;
              case 38: //UP
              e.preventDefault();
              if(window.inventarioItemFocus>=0){
                $("#inventarioModal tbody.ajax_list tr").removeClass('active');
                $(trs[window.inventarioItemFocus]).addClass('active');                
              }else{
                $("#inventarioModal tbody.ajax_list tr").removeClass('active');
              }
              break;
              case 40: //Down  
              e.preventDefault();
              if(window.inventarioItemFocus<$("#inventarioModal tbody.ajax_list tr").length){                
                $("#inventarioModal tbody.ajax_list tr").removeClass('active');
                $(trs[window.inventarioItemFocus]).addClass('active');                
              }else{
                $("#inventarioModal tbody.ajax_list tr").removeClass('active');
              }
              break;
          }
      }

      });

    $("#inventarioModal .flexigrid").on('ajax_list_complete',()=>{
      $("#inventarioModal tbody.ajax_list tr").toArray().map(x=>{
        $(x).attr('data-position',$(x).offset().top);
      });
    });

  });

  function showImage(url){
      emergente('<img src="'+url+'" style="width:100%;">');
  } 
</script>