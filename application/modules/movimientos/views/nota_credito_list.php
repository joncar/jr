<?php $this->load->view('_token_modal',array(),FALSE,'movimientos'); ?>
<script>
	<?php
		$ajustes = $this->ajustes;
		$caja = $this->db->get_where('cajas',['id'=>$this->user->caja])->row();
		$ajustes->solicitar_token_ventas = $caja->solicitar_token;		
	?>
	var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;
	var validateToken = function(){
	    return new Promise((resolve,reject)=>{
	        if(ajustes.solicitar_token_ventas=='0'){
	            resolve();
	        }else{
	            validarToken().then((result)=>{
	                if(result){
	                    resolve();
	                }else{
	                    reject();
	                }
	            });
	        }
	    });
	}
	function anular(id){
        if(confirm('Seguro que desea anular este registro?')){
            validateToken().then(()=>{
	            $.post('<?= base_url('/movimientos/ventas/notas_credito_cliente/anular') ?>/'+id,{id:id},function(data){
	                emergente(data);
	                $("#filtering_form").submit();
	            });
            });
        }
    }
	window.afterLoad.push(function(){		
		$(document).on('click',"a",function(e){	
			if($(e.target).attr('href').indexOf('add') > -1 || $(e.target).attr('href').indexOf('edit') > -1){
				e.preventDefault();
				validateToken().then(()=>{
					document.location.href=$(e.target).attr('href');
				});
		  }
		});
	});
</script>	