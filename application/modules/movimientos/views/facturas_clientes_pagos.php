<?= $output ?>
<script>
	var bancos = '';
    var cheques = '';
	window.afterLoad.push(function(){
		if($("#field-formapago_id").val()==''){
			$("#field-formapago_id").val('1').chosen().trigger('liszt:updated');
			$("#field-concepto").val('PAGO POR FACTURA');
		}

		bancos = $("#bancos_id_field_box,#cuentas_bancos_id_field_box");
        cheques = $("#nro_cheque_field_box,#tipo_cheque_field_box,#fecha_emision_field_box,#fecha_pago_field_box");
        bancos.parent().hide();
        cheques.parent().hide();
        $("#field-formapago_id").on('change',function(){
            var forma = $(this).val();
            bancos.parent().hide();
            cheques.parent().hide();
            switch(forma){
                case '6': //Transferencia
                    bancos.parent().show();
                break;
                case '5': //Deposito
                    bancos.parent().show();
                break;
                case '3': //Cheque Vista
                	bancos.parent().show();
                    cheques.parent().show();
                    $("#field-tipo_cheque").val(1).chosen().trigger('liszt:updated');
                break;
                case '4': //Cheque
                    bancos.parent().show();
                    cheques.parent().show();
                    $("#field-tipo_cheque").val(-1).chosen().trigger('liszt:updated');
                break;
            }
        });
        $("#field-user_id").val(<?= $this->user->id ?>).chosen().trigger('liszt:updated');
        <?php if($this->ajustes->permitir_pago_superior_a_saldo==0): ?>
        $(document).on('change','#field-monto',function(){
            let saldo = parseFloat($('#saldo_cliente').val());
            let monto = parseFloat($(this).val());
            if(isNaN(saldo) || isNaN(monto)){
                monto = 0;
            }
            if(monto>saldo){
                monto = saldo;
            }
            $(this).val(monto);
        })
        <?php endif ?>
	});
</script>