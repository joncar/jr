<?php
if (empty($edit) || $edit->num_rows() > 0):
    $edit = !empty($edit) ? $edit->row() : '';
    $accion = empty($edit) ? 'insert' : 'update/' . $edit->id;
    $ajustes = $this->ajustes;
    $acciones = [
        'add'=>'Añadir',
        'edit'=>'Modificar'
    ];
    $aplicar_sugerencia = 1;
    if(!empty($edit) && $this->ajustes->aplicar_sugerencia==0){
        $aplicar_sugerencia = 0;
    }
    ?>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.js"></script>-->
    <?php get_instance()->js[] = '<script src="/js/morris.js"></script>'; ?>
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.css">-->
    <?php get_instance()->hcss[] = '<link rel="stylesheet" href="/css/morris.css">'; ?>
    <form action="" onsubmit="enviar(this); return false;" method="post">
        <style>
            .panel-body > div{
                margin-top:10px;
            }

            .agrupado, .agrupado2{
                position: relative;
                width: 100%;
                margin-left:5px;
            }
            .agrupado:first-child, .agrupado2:first-child{
                margin-left: 0;
            }

            .agrupado > label, .agrupado2 > label{
                position:absolute;
                top: 10px;
                left: 5px;
            }

            .agrupado > input{
                padding-left:30px;
            }

            .agrupado2 > input{
                padding-left:50px;
            }

            .oculto{
                display: none;
            }
            .form-group {
                margin-bottom: 1rem;
            }
            .chzn-drop,.chzn-drop input{
                width:100% !important;
            }
        </style>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h1 class="kt-portlet__head-title"><?= $acciones[$action] ?> Producto</h1>
                            <div  style="position: absolute;right: 16px;">
                                <input type="checkbox" id="completo" value="1"> Extendido
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-section">
                            <div class="form-group row" id="codigo_field_box">
                                <label for="field-codigo" id="codigo_display_as_box" class="col-md-2">
                                    Codigo interno<span class="required">*</span>:
                                </label>
                                <div class="col-12 col-md-10">
                                    <?php 
                                        if($accion=='insert'){                                        
                                            $codigo = $this->db->query('select getUltimoCodigoInterno()+1 as cod')->row()->cod;
                                        }else{
                                            $codigo = @$edit->codigo_interno;
                                        }
                                    ?>
                                    <input id="field-codigo_interno" name="codigo_interno" class="form-control codigo_interno" type="text" value="<?= $codigo ?>">
                                    <?php if($accion=='insert' || ($this->db->get_where('ventadetalle',['producto'=>$edit->codigo])->num_rows()==0 || $this->db->get_where('compradetalles',['producto'=>$edit->codigo])->num_rows()==0)): ?>
                                        <button type="button" class="btn btn-dark btn-icon" onclick="refreshInterno()" style="position: absolute;right: 0;top: 0;border-radius: 0;line-height: 0px;width: 37px;height: 37px;"><i class="fa fa-sync" style=""></i></button>
                                    <?php endif ?>
                                </div>
                            </div>

                            <div class="form-group row" id="codigo_field_box">
                                <label for="field-codigo" id="codigo_display_as_box" class="col-md-2">
                                    Codigo de barra<span class="required">*</span>:
                                </label>
                                <div class="col-12 col-md-10">
                                    <input id="field-codigo" name="codigo" class="form-control codigo" type="text" value="<?= @$edit->codigo ?>" <?php echo $accion!='insert' && 
                                                                                                                                                            (
                                                                                                                                                                $this->db->get_where('ventadetalle',['producto'=>$edit->codigo])->num_rows()>0 ||
                                                                                                                                                                $this->db->get_where('compradetalles',['producto'=>$edit->codigo])->num_rows()>0
                                                                                                                                                            )?
                                                                                                                                                            'readonly':'' 
                                                                                                                                                 ?>>                    
                                </div>
                            </div>

                            <div class="form-group row" id="codigo2_field_box">
                                <label for="field-codigo2" id="codigo2_display_as_box" class="col-md-2">
                                    Codigo de barra 2:
                                </label>
                                <div class="col-12 col-md-10">
                                    <input id="field-codigo2" name="codigo2" class="form-control codigo2" type="text" value="<?= @$edit->codigo2 ?>">
                                </div>
                            </div>

                            <div class="form-group row" id="parent_codigo_field_box">
                                <label for="field-parent_codigo" id="parent_codigo_display_as_box" class="col-md-2">
                                    Codigo padre:
                                </label>
                                <div class="col-6 col-md-5">
                                    <input id="field-parent_codigo" name="parent_codigo" class="form-control parent_codigo" type="text" value="<?= @$edit->parent_codigo ?>">
                                </div>
                                <div class="col-6 col-md-5">
                                    <input id="field-parent_fracc" name="parent_fracc" class="form-control parent_fracc" type="text" value="<?= @$edit->parent_fracc ?>" placeholder="Fraccionamiento">
                                </div>
                            </div>
                            
                            <div class="form-group row" id="nombre_comercial_field_box">
                                <label for="field-nombre_comercial" id="nombre_comercial_display_as_box" class="col-md-2">
                                    Nombre comercial<span class="required">*</span>:
                                </label>
                                <div class="col-12 col-md-10">
                                    <input id="field-nombre_comercial" name="nombre_comercial" class="form-control codigo" type="text" value="<?= @$edit->nombre_comercial ?>">                    
                                </div>
                            </div>
                            <div class="form-group oculto" id="nombre_generico_field_box">
                                <div class="row">
                                    <label for="field-nombre_generico" id="nombre_generico_display_as_box" class="col-md-2">
                                        Nombre genérico<span class="required">*</span>:
                                    </label>
                                    <div class="col-12 col-md-10">
                                        <input id="field-nombre_generico" name="nombre_generico" class="form-control codigo" type="text" value="<?= @$edit->nombre_generico ?>">                    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group oculto" id="propiedades_field_box">
                                <div class="row">
                                    <label for="field-propiedades" id="propiedades_display_as_box" class="col-md-2">
                                        Propiedades<span class="required">*</span>:
                                    </label>
                                    <div class="col-12 col-md-10">
                                        <input id="field-propiedades" name="propiedades" class="form-control codigo" type="text" value="<?= @$edit->propiedades ?>">                    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" id="propiedades_field_box">
                                <label for="field-propiedades" id="propiedades_display_as_box" class="col-md-2">
                                    Proveedor<span class="required">*</span>:
                                </label>
                                <div class="col-12 col-md-10">
                                    <?php echo form_dropdown_from_query('proveedor_id', 'proveedores', 'id', 'denominacion', @$edit->proveedor_id, 'id="field-proveedor_id"'); ?>
                                </div>
                            </div>
                            <div class="form-group row" id="categoria_id_field_box">
                                <label for="field-propiedades" id="categoria_id_display_as_box" class="col-md-2">
                                    Categoria <a href="#categorias" data-toggle="modal"><i class="fa fa-plus"></i></a><span class="required">*</span>:
                                </label>
                                <div class="col-12 col-md-10">
                                    <?php echo form_dropdown_from_query('categoria_id', 'categoriaproducto', 'id', 'denominacion', @$edit->categoria_id, 'id="field-categoria_id"'); ?>
                                </div>
                            </div>
                            <div class="form-group row oculto" id="sub_categoria_producto_id_field_box">
                                <div class="row ml-0 mr-0">
                                    <label for="field-sub_categoria_producto_id" id="sub_categoria_producto_id_display_as_box" class="col-md-2">
                                        Sub Categoria  <a href="#subcategorias" data-toggle="modal"><i class="fa fa-plus"></i></a>:
                                    </label>
                                    <div class="col-12 col-md-10">
                                        <?php echo form_dropdown_from_query('sub_categoria_producto_id', 'sub_categoria_producto', 'id', 'nombre_sub_categoria_producto', @$edit->sub_categoria_producto_id, 'id="field-sub_categoria_producto_id"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row oculto" id="sub_sub_categoria_field_box">
                                <div class="row ml-0 mr-0">
                                    <label for="field-propiedades" id="sub_sub_categoria_display_as_box" class="col-md-2">
                                        Sub Categoria 2 <a href="#subsubsubcategorias" data-toggle="modal"><i class="fa fa-plus"></i></a>:
                                    </label>
                                    <div class="col-12 col-md-10">
                                        <?php echo form_dropdown_from_query('sub_sub_categoria_id', 'sub_sub_categoria', 'id', 'nombre_sub_sub_categoria', @$edit->sub_sub_categoria_id, 'id="field-sub_sub_categoria_id"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group oculto" id="propiedades_field_box">
                                <div class="row ml-0 mr-0">
                                    <label for="field-propiedades" id="propiedades_display_as_box" class="col-md-2 p-0">
                                        Marca: <a href="#marcas" data-toggle="modal"><i class="fa fa-plus"></i></a>
                                    </label>
                                    <div class="col-12 col-md-10 p-0">
                                        <?php echo form_dropdown_from_query('marcas_id', 'marcas', 'id', 'marcas_nombre', @$edit->marcas_id, 'id="field-marcas_id"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" id="unidad_medida_id_field_box">
                                <label for="field-unidad_medida_id" id="unidad_medida_id_display_as_box" class="col-md-2">
                                    Unidad de medida: <a href="#unidades" data-toggle="modal"><i class="fa fa-plus"></i></a>
                                </label>
                                <div class="col-12 col-md-10">
                                    <?php echo form_dropdown_from_query('unidad_medida_id', 'unidad_medida', 'id', 'denominacion', @$edit->unidad_medida_id, 'id="field-unidad_medida_id"'); ?>
                                </div>
                            </div>
                            <div class="form-group row" id="propiedades_field_box">
                                <label for="field-propiedades" id="propiedades_display_as_box" class="col-md-2">
                                    IVA<span class="required">*</span>:
                                </label>
                                <div class="col-12 col-md-10">
                                    <?php $val = !isset($edit->iva_id) ? 10 : $edit->iva_id; ?>
                                    <?php echo form_dropdown('iva_id', array('0' => 'Exento', '5' => '5%', '10' => '10%'), $val, 'id="field-iva_id" class="form-control"') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h1 class="kt-portlet__head-title">Precios</h1>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-section">
                            <div style="display: flex;">
                                <div class="" id="precio_costo_field_box" style="width:100%;">
                                    <label for="field-precio_costo" id="precio_costo_display_as_box">
                                        P.costo<span class="required">*</span>:
                                    </label>
                                    <input id="field-precio_costo" name="precio_costo" class="form-control codigo" type="text" value="<?= @$edit->precio_costo ?>" placeholder="Precio de costo" <?= !empty($edit) && $this->ajustes->precioxsucursal==1?'readonly':'' ?>>
                                </div>
                                <div class="" id="porc_venta_field_box"  style="width:50%; margin-left:5px;">
                                    <label for="field-porc_venta" id="porc_venta_display_as_box">
                                       %Venta <span class="required"></span>:
                                    </label>
                                    <input id="field-porc_venta" name="porc_venta" class="form-control porc_venta" type="text" value="<?= @$edit->porc_venta ?>"  placeholder="%Venta" style="background-color: #8b96fbe6;" <?= !empty($edit) && $this->ajustes->precioxsucursal==1?'readonly':'' ?>>                    
                                </div>
                                <div class="" id="precio_venta_field_box"  style="width:100%; margin-left:5px;">
                                    <label for="field-precio_venta" id="precio_venta_display_as_box">
                                        P.Venta<span class="required"></span>:
                                    </label>
                                    <input id="field-precio_venta" name="precio_venta" class="form-control codigo" type="text" value="<?= @$edit->precio_venta ?>"  placeholder="Precio de venta"  <?= !empty($edit) && $this->ajustes->precioxsucursal==1?'readonly':'' ?>>                    
                                </div>
                                <div class="" id="precio_credito_field_box"  style="width:100%; margin-left:5px;">
                                    <label for="field-precio_credito" id="precio_credito_display_as_box">
                                        P.Credito
                                    </label>
                                    <input id="field-precio_credito" name="precio_credito" class="form-control codigo" type="text" value="<?= @$edit->precio_credito ?>"  placeholder="Precio de venta-credito"  <?= !empty($edit) && $this->ajustes->precioxsucursal==1?'readonly':'' ?>>                    
                                </div>                                
                                <div class="" id="porc_comision_field_box"  style="width:50%; margin-left:5px;">
                                    <label for="field-porc_comision" id="porc_comision_display_as_box">
                                        %Comisión<span class="required"></span>:
                                    </label>
                                    <input id="field-porc_comision" name="porc_comision" class="form-control codigo" type="text" value="<?= @$edit->porc_comision ?>"  placeholder="%Comisión" style="background-color: #8b96fbe6;">                    
                                </div>
                            </div>
                            <div class="form-group row" id="precio_venta_field_box">
                                <label for="field-precio_venta" id="precio_venta_display_as_box" class="col-md-12">
                                    Precios mayoristas<span class="required">*</span>:
                                </label>
                                <div class="col-12 col-md-12" style="display: flex">
                                    <div class="agrupado" style="width:50%">
                                        <label for="field-precio_venta_mayorista1" id="precio_venta_mayorista1_display_as_box";>
                                            %<span class="required"></span>:
                                        </label>
                                        <input id="field-porc_mayorista1" <?= $ajustes->rango_precio_cantidades==0?'readonly="true"':'' ?> name="porc_mayorista1" class="form-control codigo" type="text" value="<?= empty($edit->porc_mayorista1)?'':$edit->porc_mayorista1 ?>" placeholder="0" style="background-color: #8b96fbe6;"  <?= !empty($edit) && $this->ajustes->precioxsucursal==1?'readonly':'' ?>>
                                    </div>
                                    <div class="agrupado">
                                        <label for="field-precio_venta_mayorista1" id="precio_venta_mayorista1_display_as_box">
                                            1<span class="required"></span>:
                                        </label>                                        
                                        <input id="field-precio_venta_mayorista1" <?= $ajustes->rango_precio_cantidades==0?'readonly="true"':'' ?> name="precio_venta_mayorista1" class="form-control codigo" type="text" value="<?= empty($edit->precio_venta_mayorista1)?0:$edit->precio_venta_mayorista1 ?>" placeholder="Nivel 1"  <?= !empty($edit) && $this->ajustes->precioxsucursal==1?'readonly':'' ?>>
                                    </div>
                                    <div class="agrupado" style="width:50%">
                                        <label for="field-precio_venta_mayorista2" id="precio_venta_mayorista2_display_as_box" style="width:50%";>
                                            %<span class="required"></span>:
                                        </label>
                                        <input id="field-porc_mayorista2" <?= $ajustes->rango_precio_cantidades==0?'readonly="true"':'' ?> name="porc_mayorista2" class="form-control codigo" type="text" value="<?= empty($edit->porc_mayorista2)?'':$edit->porc_mayorista2 ?>" placeholder="0" style="background-color: #8b96fbe6;"  <?= !empty($edit) && $this->ajustes->precioxsucursal==1?'readonly':'' ?>>
                                    </div>
                                    <div class="agrupado">
                                        <label for="field-precio_venta_mayorista2" id="precio_venta_mayorista2_display_as_box">
                                            2<span class="required"></span>:
                                        </label>
                                        <input id="field-precio_venta_mayorista2" <?= $ajustes->rango_precio_cantidades==0?'readonly="true"':'' ?> name="precio_venta_mayorista2" class="form-control codigo" type="text" value="<?= empty($edit->precio_venta_mayorista2)?0:$edit->precio_venta_mayorista2 ?>" placeholder="Nivel 2"  <?= !empty($edit) && $this->ajustes->precioxsucursal==1?'readonly':'' ?>>
                                    </div>
                                    <div class="agrupado" style="width:50%">
                                        <label for="field-precio_venta_mayorista3" id="precio_venta_mayorista3_display_as_box" style="width:50%";>
                                            %<span class="required"></span>:
                                        </label>
                                        <input id="field-porc_mayorista3" <?= $ajustes->rango_precio_cantidades==0?'readonly="true"':'' ?> name="porc_mayorista3" class="form-control codigo" type="text" value="<?= empty($edit->porc_mayorista3)?'':$edit->porc_mayorista3 ?>" placeholder="0" style="background-color: #8b96fbe6;"  <?= !empty($edit) && $this->ajustes->precioxsucursal==1?'readonly':'' ?>>
                                    </div>
                                    <div class="agrupado">
                                        <label for="field-precio_venta_mayorista3" id="precio_venta_mayorista3_display_as_box">
                                            3<span class="required"></span>:
                                        </label>
                                        <input id="field-precio_venta_mayorista3" <?= $ajustes->rango_precio_cantidades==0?'readonly="true"':'' ?> name="precio_venta_mayorista3" class="form-control codigo" type="text" value="<?= empty($edit->precio_venta_mayorista3)?0:$edit->precio_venta_mayorista3 ?>" placeholder="Nivel 3"  <?= !empty($edit) && $this->ajustes->precioxsucursal==1?'readonly':'' ?>>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="precio_venta_field_box">
                                <label for="field-precio_venta" id="precio_venta_display_as_box" class="col-md-12">
                                    Cantidad personalizada de mayoristas<span class="required">*</span>:
                                </label>
                                <div class="col-12 col-md-12" style="display: flex">
                                    <div class="agrupado">
                                        <label for="field-cant_1" id="cant_1_display_as_box">
                                            1<span class="required">*</span>:
                                        </label>
                                        <input id="field-cant_1" name="cant_1" <?= $ajustes->rango_precio_cantidades==0?'readonly="true"':'' ?> class="form-control codigo" type="text" value="<?= empty($edit->cant_1)?0:$edit->cant_1 ?>" placeholder="Nivel 1">
                                    </div>
                                    <div class="agrupado">
                                        <label for="field-cant_2" id="cant_2_display_as_box">
                                            2<span class="required">*</span>:
                                        </label>
                                        <input id="field-cant_2" name="cant_2" <?= $ajustes->rango_precio_cantidades==0?'readonly="true"':'' ?> class="form-control codigo" type="text" value="<?= empty($edit->cant_2)?0:$edit->cant_2 ?>" placeholder="Nivel 2">
                                    </div>
                                    <div class="agrupado">
                                        <label for="field-cant_3" id="cant_3_display_as_box">
                                            3<span class="required">*</span>:
                                        </label>
                                        <input id="field-cant_3" name="cant_3" <?= $ajustes->rango_precio_cantidades==0?'readonly="true"':'' ?> class="form-control codigo" type="text" value="<?= empty($edit->cant_3)?0:$edit->cant_3 ?>" placeholder="Nivel 3">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="precio_venta_field_box">
                                <label for="field-precio_venta" id="precio_venta_display_as_box" class="col-md-12">
                                    %Descuentos<span class="required">*</span>:
                                </label>
                                <div class="col-12 col-md-12" style="display: flex">
                                    <div class="agrupado2">
                                        <label for="field-descmin" id="descmin_display_as_box">
                                            Min<span class="required">*</span>:
                                        </label>
                                        <input id="field-descmin" name="descmin" class="form-control codigo" type="text" value="<?= @$edit->descmin ?>" placeholder="Mínimo">
                                    </div>
                                    <div class="agrupado2">
                                        <label for="field-descmax" id="descmax_display_as_box">
                                            Max<span class="required">*</span>:
                                        </label>
                                        <input id="field-descmax" name="descmax" class="form-control codigo" type="text" value="<?= @$edit->descmax ?>" placeholder="Maximo">
                                    </div>
                                </div>
                            </div>
                            <div style="display: flex">
                                <div class="form-group" id="stock_minimo_field_box" style="width:100%">
                                    <label for="field-stock_minimo" id="stock_minimo_display_as_box">
                                        Min. Stock<span class="required">*</span>:
                                    </label>
                                    <input id="field-stock_minimo" name="stock_minimo" class="form-control codigo" type="text" value="<?= @$edit->stock_minimo ?>" placeholder="Cantidad mínima en stock">
                                </div>
                                <div class="form-group" id="peso_field_box" style="width:100%">
                                    <label for="field-peso" id="peso_display_as_box">
                                        Peso<span class="required">*</span>:
                                    </label>
                                    <input id="field-peso" name="peso" class="form-control codigo" type="text" value="<?= @$edit->peso ?>" placeholder="Peso del producto">
                                </div>
                            </div>
                            <div style="display: flex">
                                <div class="form-group" id="presentacion_field_box" style="width:100%">
                                    <label for="field-presentacion" id="presentacion_display_as_box">
                                        Presentación: 
                                    </label>
                                    <input id="field-presentacion" name="presentacion" class="form-control codigo" type="text" value="<?= @$edit->presentacion ?>" placeholder="Presentación">
                                </div>
                            </div>
                            <div class="form-group" id="precio_venta_mayorista3_field_box" style="width:100%">                      
                                <div style="text-align: left;margin: 19px 19px 0 19px;">                                    
                                    Balanza
                                    <input type="radio" name="balanza" value="1" <?= @$edit->balanza == 1 ? 'checked' : '' ?>> SI 
                                    <input type="radio" name="balanza" value="0" <?= @$edit->balanza == 0 ? 'checked' : '' ?>> NO
                                    <br/>Inventariable
                                    <input type="radio" name="inventariable" value="1" <?= $accion == 'insert' || $accion!='insert' && $edit->inventariable == 1 ? 'checked' : '' ?>> SI
                                    <input type="radio" name="inventariable" value="0" <?= $accion!='insert' && $edit->inventariable == 0 ? 'checked' : '' ?>> NO
                                    <br/>Mod. Precio/Ventas
                                    <input type="radio" name="mod_precio" value="1" <?= @$edit->mod_precio==1 ? 'checked' : '' ?>> SI
                                    <input type="radio" name="mod_precio" value="0" <?= $accion=='insert' || @$edit->mod_precio==0 ? 'checked' : '' ?>> NO
                                    <br/>Ingrediente
                                    <input type="radio" name="ingrediente" value="1" <?= @$edit->ingrediente==1 ? 'checked' : '' ?>> SI
                                    <input type="radio" name="ingrediente" value="0" <?= $accion=='insert' || @$edit->ingrediente==0 ? 'checked' : '' ?>> NO
                                    <br/> Servicio
                                    <input type="radio" name="servicios" value="1" <?= @$edit->servicios==1 ? 'checked' : '' ?>> SI
                                    <input type="radio" name="servicios" value="0" <?= $accion=='insert' || @$edit->servicios==0 ? 'checked' : '' ?>> NO
                                    <br/>Anulado
                                    <input type="radio" name="anulado" value="1" <?= @$edit->anulado==1 ? 'checked' : '' ?>> SI
                                    <input type="radio" name="anulado" value="0" <?= $accion=='insert' || @$edit->anulado==0 ? 'checked' : '' ?>> NO
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 oculto">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head" role="tab" id="headingOne">
                            <div class="kt-portlet__head-label">
                                <h4 class="kt-portlet__head-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="fa fa-chevron-down"></i> Web
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="kt-portlet__body">
                                <div class="kt-section">
                                    <div class='form-group' id="descripcion_field_box">
                                        <label for='field-descripcion' id="descripcion_display_as_box">
                                            Descripcion:
                                        </label>
                                        <textarea id='field-descripcion' name='descripcion' class='texteditor' ><?= @$edit->descripcion ?></textarea>
                                    </div>
                                    <div class='form-group' id="foto_principal_field_box">
                                        <label for='field-foto_principal' id="foto_principal_display_as_box" style="width:100%">
                                            Foto principal:
                                        </label>
                                        <div class="crop" data-width="150px" data-height="150px" id="crop-foto_principal">
                                            <div class="slim" id="slim-foto_principal"          
                                             data-service="<?= base_url('movimientos/productos/productos/cropper/foto_principal/') ?>"
                                             data-post="input, output, actions"
                                             data-size="1024,768"
                                             data-instant-edit="true"
                                             data-push="true"
                                             data-did-upload="imageSlimUpload"
                                             data-download="true"
                                             data-will-save="addValueSlim"
                                             data-label="Subir foto"
                                             data-meta-name="foto"                         
                                             data-force-size="1024,768"  style="width:200px; height:120px;">                                      
                                             <input type="file" id="slim-foto_principal"/>                        
                                             <input type="hidden" data-val="<?= @$edit->foto_principal ?>" name="foto_principal" value="<?= @$edit->foto_principal ?>" id="field-foto_principal">
                                             <img id="image-foto_principal" style="margin-bottom: 0" src="<?= base_url().'img/productos/'.@$edit->foto_principal ?>" style="width:120px; height:120px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12">
                <div class="btn-group btn-group-justified" role="group" aria-label="...">		  
                    <div class="btn-group" role="group">
                        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#procesar"><?= $acciones[$action] ?> producto</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default" <?= empty($edit) ? 'disabled' : 'data-target="#masdetalles" data-toggle="modal"' ?>>Más datos</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="responseDiv"></div>
            </div>
        </div>
    </form>
    <?php if (!empty($edit)): ?>
        <?php $this->load->view('_productosMasDetallesModal',['producto'=>$edit->codigo]); ?>
    <?php endif ?>


<?php else: ?>
    Ocurrio un error al procesar
<?php endif ?>


<?php $this->load->view('_unidades_medida_modal'); ?>
<?php $this->load->view('_marcas_modal'); ?>
<?php $this->load->view('_categorias_modal'); ?>

<script>
    function enviar(form){
        var ci = $("input[name='codigo_interno']").val();
        var cc = $("input[name='codigo']").val();
        if(cc=='' && ci!=''){
            $("input[name='codigo']").val(ci);
            //$("form").submit();
        }
        insertar('movimientos/productos/productos/<?= $accion ?>', form, '.responseDiv');        
        return false;
    }
    window.afterLoad.push(function(){
        window.sub1 = new relation_dependency();
        window.sub2 = new relation_dependency();
        sub1.select_dependency_chosen(
            'maestras/sub_categoria_producto/json_list',
            $("#field-categoria_id"),
            $("#field-sub_categoria_producto_id"),
            'categoriaproducto_id',
            'id',
            'nombre_sub_categoria_producto'
        );

        sub2.select_dependency_chosen(
            'maestras/sub_sub_categoria/json_list',
            $("#field-sub_categoria_producto_id"),
            $("#field-sub_sub_categoria_id"),
            'sub_categoria_producto_id',
            'id',
            'nombre_sub_sub_categoria'
        ); 

        $(document).on('change','#field-categoria_id',function(){
            $(".categoriaproducto_id").val($('#field-categoria_id option:selected').html());
            $("#subcategorias input[name='categoriaproducto_id']").val($(this).val());
        });

        $(document).on('change','#field-sub_categoria_producto_id',function(){
            $(".sub_categoria_producto_id").val($('#field-sub_categoria_producto_id option:selected').html());
            $("#subsubsubcategorias input[name='sub_categoria_producto_id']").val($(this).val());
        });

        $(document).on('click','#completo',function(){
            if($(this).prop('checked')){
                $(".oculto").show();
            }
            else{
                $(".oculto").hide();
            }
        });

        $(document).on('change','<?= $this->ajustes->aplicar_sugerencia==1?'input[name="precio_costo"],':'' ?>input[name="porc_venta"],input[name="porc_mayorista1"],input[name="porc_mayorista2"],input[name="porc_mayorista3"]',function(){
            var precio_costo = parseFloat($('input[name="precio_costo"]').val());
            var porc_venta = parseFloat($('input[name="porc_venta"]').val());
            var porc_mayorista1 = parseFloat($('input[name="porc_mayorista1"]').val());
            var porc_mayorista2 = parseFloat($('input[name="porc_mayorista2"]').val());
            var porc_mayorista3 = parseFloat($('input[name="porc_mayorista3"]').val());
            var precio = 0;
            if(isNaN(precio_costo)){
                return;
            }
            if(!isNaN(porc_venta)){
                precio = precio_costo + ((precio_costo*porc_venta)/100);
                $("input[name='precio_venta']").val(redondeo(parseInt(precio.toFixed(0))));
            }

            if(!isNaN(porc_mayorista1)){
                precio = precio_costo + ((precio_costo*porc_mayorista1)/100);
                $("input[name='precio_venta_mayorista1']").val(redondeo(parseInt(precio.toFixed(0))));
            }

            if(!isNaN(porc_mayorista2)){
                precio = precio_costo + ((precio_costo*porc_mayorista2)/100);
                $("input[name='precio_venta_mayorista2']").val(redondeo(parseInt(precio.toFixed(0))));
            }

            if(!isNaN(porc_mayorista3)){
                precio = precio_costo + ((precio_costo*porc_mayorista3)/100);
                $("input[name='precio_venta_mayorista3']").val(redondeo(parseInt(precio.toFixed(0))));
            }
        });

        $(document).on('change','input[name="precio_venta"],input[name="precio_venta_mayorista1"],input[name="precio_venta_mayorista2"],input[name="precio_venta_mayorista3"]',function(){
            var precio_costo = parseFloat($('input[name="precio_costo"]').val());
            var porc_venta = parseFloat($('input[name="precio_venta"]').val());
            var porc_mayorista1 = parseFloat($('input[name="precio_venta_mayorista1"]').val());
            var porc_mayorista2 = parseFloat($('input[name="precio_venta_mayorista2"]').val());
            var porc_mayorista3 = parseFloat($('input[name="precio_venta_mayorista3"]').val());
            var precio = 0;
            if(isNaN(precio_costo)){
                return;
            }
            if(!isNaN(porc_venta)){
                precio = ((porc_venta/precio_costo)-1)*100;
                $("input[name='porc_venta']").val(redondeo(parseInt(precio.toFixed(0))));
            }

            if(!isNaN(porc_mayorista1)){
                precio = ((porc_mayorista1/precio_costo)-1)*100;
                $("input[name='porc_mayorista1']").val(redondeo(parseInt(precio.toFixed(0))));
            }

            if(!isNaN(porc_mayorista2)){
                precio = ((porc_mayorista2/precio_costo)-1)*100;
                $("input[name='porc_mayorista2']").val(redondeo(parseInt(precio.toFixed(0))));
            }

            if(!isNaN(porc_mayorista3)){
                precio = ((porc_mayorista3/precio_costo)-1)*100;
                $("input[name='porc_mayorista3']").val(redondeo(parseInt(precio.toFixed(0))));
            }
        });
    });

    function refreshInterno(){
        $("#field-codigo_interno").val('Consultando código interno');
        $.get('<?= base_url() ?>movimientos/productos/productos/codigoInterno',{},function(data){
            $("#field-codigo_interno").val(data);
        });
    }

    function redondeo(valor){
        if(valor<100){
            return valor;
        }               
        var centena = valor.toString().slice(-2);
        if(centena<50){
            valor-= centena;
        }else if(centena>50){
            valor+= 100-centena;
        }
        return valor<0?Math.abs(valor):valor;
    }
</script>