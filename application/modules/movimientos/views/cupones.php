<?php if(!isset($asignar)): ?>
<a href="javascript:asignar()" class="btn btn-info">Asignar Ganador</a>
<?= $output ?>
<div id="asignarModal" class="modal fade" tabindex="-1" role="dialog">
  <form onsubmit="mostrar(); return false;">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">	        
	        <h4 class="modal-title">Agregar cliente</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	  		
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary" disabled="">Generar Ganador</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->

<script>
	function spinner(){
		$("#asignarModal .modal-body").html('<div class="text-center"><div class="spinner-border text-primary" role="status"><span class="sr-only">Cargando...</span></div></div>');
	}
	function asignar(){
		spinner();
		$("#asignarModal button[type='submit']").attr('disabled',true);
		$("#asignarModal").modal('show');
		$.get('<?= base_url() ?>movimientos/cupones/cupones/asignar',{},function(data){
			$("#asignarModal .modal-body").html(data);
			$("#asignarModal button[type='submit']").attr('disabled',false);
		});
	}
</script>
<?php else: ?>
	<?php 		
		$this->db->select("cupones.*,IFNULL(clientes.nombres,'') as nombres,IFNULL(clientes.apellidos,'') as apellidos,IFNULL(clientes.nro_documento,'') as nro_documento");
		$this->db->join('clientes','clientes.id = cupones.clientes_id');
		$cupones = $this->db->get_where('cupones');
		$this->db->group_by('cupones.clientes_id');
		$this->db->select('cupones.*,clientes.nombres,clientes.apellidos');
		$this->db->join('clientes','clientes.id = cupones.clientes_id');
		$jugadores = $this->db->get_where('cupones');
	?>
	<div class="row">
		<div class="col-12">Total Participantes: <b><?= $jugadores->num_rows(); ?></b></div>
		<div class="col-12">Total Cupones: <b><?= $cupones->num_rows(); ?></b></div>		
	</div>
	<div class="row justify-content-center">
		<div class="col-12 col-md-6">		  
		  <div style="text-align: center;border: 1px solid #000;padding: 20px;min-height: 200px; display: flex; align-items: center;" id="ganador">
		  	<h1 style="width:100%">Ganador</h1>
		  </div>
		</div>
	</div>
	<script>
		var jugadores = <?php
			$arr = [];
			foreach($jugadores->result() as $jugador){
				$arr[] = $jugador->nombres.' '.$jugador->apellidos;
			}
			echo json_encode($arr);
		?>;
		var ganador = <?php 
			$ganador = rand(0,$cupones->num_rows()-1);
			$this->db->update('cupones',['ganador'=>0]);			
			for($i=1;$i<=$cupones->num_rows();$i++){
				$cupon = $cupones->row($i-1);
				if($i==$ganador){
					$this->db->update('cupones',['ganador'=>1],['id'=>$cupones->row($i)->id]);
					echo json_encode($cupon);
				}
			}
		?>;

		function sleep(ms) {
		  return new Promise(resolve => setTimeout(resolve, ms));
		}

		async function mostrar(){
			$("#asignarModal button[type='submit']").attr('disabled',true);
			var tiempo = 200;
			var actual = 0;
			$("#ganador h1").html('');
			for(var i=0;i<tiempo;i++){
				var pos = Math.ceil(Math.random()*<?= $jugadores->num_rows()-1; ?>);
				$("#ganador h1").html(jugadores[pos]);
				await sleep(50);
			}
			$("#ganador h1").html(`${ganador.nro_documento} - ${ganador.nombres} ${ganador.apellidos}`);
		}
	</script>
<?php endif ?>