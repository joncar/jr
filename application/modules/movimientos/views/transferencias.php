<?php 
    $rules = [];
    if($this->db->table_exists('sucursales_transferencias')){
        $this->db->select('sucursales_transferencias.*, sucursales.denominacion');
        $this->db->join('sucursales','sucursales.id = sucursales_transferencias.sucursal_destino');
        $sucs = $this->db->get_where('sucursales_transferencias')->result();
        $rules = [];
        foreach($sucs as $s){
            if(!isset($rules[$s->sucursal_id])){
                $rules[$s->sucursal_id] = [];
            }
            $rules[$s->sucursal_id][] = [$s->sucursal_destino,$s->denominacion];
        }
    }
?>
<script>
    <?php
        $ajustes = $this->ajustes;
    ?>
    var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;
    var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
    var tasa_real = <?= $ajustes->tasa_reales ?>;
    var tasa_peso = <?= $ajustes->tasa_pesos ?>;
    var codigo_balanza = <?= json_encode(explode(',',str_replace(', ',',',$ajustes->cod_balanza))) ?>;
    var modPrecio = <?= $ajustes->permitir_modificar_precio_en_ventas ?>;
    var cantidades_mayoristas = <?php 
        $ar = array();
        foreach($this->db->get('cantidades_mayoristas')->result() as $c){
            $ar[] = $c;
        }
        echo json_encode($ar);
    ?>;
    var vender_sin_stock = <?= $ajustes->vender_sin_stock ?>;
    var onsend = false;
    var controlar_vencimiento = <?= $ajustes->controlar_vencimiento_stock ?>;
<?php 
    if(!empty($edit)){
        $venta = $this->querys->getTransferencia($edit);
        if($venta){
            echo 'var editar = '.json_encode($venta).';';
        }else{
            echo 'var editar = undefined;';
        }
    }else{
        echo 'var editar = undefined;';
    }
    echo 'var rules = '.json_encode($rules).';';
?>
</script>
<style>
    .panel{
        border-radius:0px;
    }

    .error{
        border: 1px solid red !important;
    }

    .patternCredito{
        background: #ffffffa6;width: 100%;height: 100%;position: absolute;top: 0px;left: 0px;
    }
    .btnsaldo{
        position: absolute;top: 0;right: 15px;
    }
    @media screen and (max-width:768px){
        .btnsaldo{
            top: -15px;
            font-size:20px;
        }   
    }
    .kt-portlet{
        margin:0;
    }

    .table thead th, .table td{
        padding:6px;
    }

    tbody .form-control{
        height:22px;
    }

    .kt-portlet .kt-portlet__head{
        min-height: inherit;
        padding:10px;
    }
</style>
<div class="kt-portlet">
    <div class="kt-portlet__body">
        <div class="kt-section">
            <div class="row" style="position: relative;">
                <div class="col-12 col-md-9">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            Origen: 
                            <?= form_dropdown_from_query('sucursal_origen','sucursales','id','denominacion','','id="sucursal_origen"') ?>                
                            <button type="button" class="btn btn-info" onclick="vaciarStock()">Vaciar stock</button>
                        </div>
                        <div class="col-12 col-md-4">
                            Destino: 
                            <?= form_dropdown_from_query('sucursal_destino','sucursales','id','denominacion','','id="sucursal_destino"') ?>
                        </div>
                        <div class="col-12 col-md-4">
                            Fecha: 
                            <input type="text" name="fecha_solicitud" value="<?= empty($venta)?date('Y-m-d'):$venta->fecha_solicitud ?>" id="fecha_solicitud" class="datetime-input form-control">
                        </div>
                        <div class="col-12 col-md-4">
                            Motivo: 
                            <?= form_dropdown_from_query('motivo_transferencia_id','motivo_transferencia','id','denominacion','','id="motivo_transferencia_id"') ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3" style="position: relative;">
                    <span style="position: absolute;top: 26px;left: 30px;font-weight: bold;">Total Transf. Gs: </span>
                    <input type="text" name="total_venta" class="form-control" readonly="" value="300.000" style="font-size:30px; font-weight:bold; text-align: right;height: 89px;vertical-align: baseline;margin-top: 19px; background:lightgreen">
                </div>
            </div>
        </div>

    </div>
</div>
<div id="responseTransferencia"></div>
<div class="row">
    <div class="col-12 col-md-9" style="padding-right: 39px;">
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="kt-section">
                    <div style="height:200px; overflow-y:auto">
                        <table class="table table-bordered" id="ventaDescr">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    <?php if($this->ajustes->controlar_vencimiento_stock==1): ?>
                                        <th>Venc.</th>
                                    <?php endif ?>
                                    <th>Total</th>
                                    <th>Stock</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr id="productoVacio">
                                    <td>
                                        <a href="javascript:void(0)" class="rem" style="display:none;color:red">
                                            <i class="fa fa-times"></i>
                                        </a>
                                        <span>&nbsp;</span>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td><input name="cantidad" class="form-control cantidad" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
                                    <td><input name="precio" class="form-control precio" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
                                    <?php if($this->ajustes->controlar_vencimiento_stock==1): ?>
                                        <td>
                                            <select name="vencimiento" class="form-control vencimiento" style="width:90px;text-align: right;padding: 0 6px;"></select>
                                        </td>
                                    <?php endif ?>
                                    <td><input name="total" class="form-control total" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
                                    <td>&nbsp;</td>
                                </tr>

                                
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-2" style="text-align: center;padding: 6px;">
                        Cant: <span id="cantidadProductos">4</span>                 
                    </div>
                    <div class="col-12 col-md-10" style="position: relative;">
                        <i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
                        <input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" style="padding-left: 25px;padding-right: 73px;">
                        <button style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar insertar">Insertar</button>

                        <div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-3" style="padding-left: 5px;">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h1 class="kt-portlet__head-title">
                        <b>Resumen de transf</b>
                    </h1>
                </div>
            </div>
            <div class="kt-portlet__head">
                <div class="row">
                    <div class="col-12 col-md-4">Pesos: </div>
                    <div class="col-8" style="margin-bottom:5px">                       
                        <input type="text" class="form-control" name="total_pesos" value="300.000" readonly="">
                    </div>
                    <div class="col-12 col-md-4">Reales: </div>
                    <div class="col-8" style="margin-bottom:5px">                       
                        <input type="text" class="form-control" name="total_reales" value="300.000" readonly="">
                    </div>
                    <div class="col-12 col-md-4">Dolares: </div>
                    <div class="col-8" style="margin-bottom:5px">                       
                        <input type="text" class="form-control" name="total_dolares" value="300.000" readonly="">
                    </div>
                </div>
            </div>
        </div>

        
    </div>
</div>

<?php if(empty($edit)): ?>
<div class="row">
    <div class="col-12 col-md-9">
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
          <div class="btn-group" role="group">
            <button type="button" class="btn btn-default btnNueva" onclick="nuevaVenta();" style="color:#000 !important"><i class="fa fa-plus-circle"></i> Nueva transferencia</button>
          </div>
          <div class="btn-group" role="group">
            <button type="button" class="btn btn-primary" onclick="sendVenta()"><i class="fa fa-floppy-o"></i> Procesar transferencia</button>
          </div>
          <div class="btn-group" role="group">
            <button type="button" class="btn btn-default" <?= empty($edit)?'disabled="true"':'' ?> style="color:#000 !important"><i class="fa fa-print"></i> Imprimir</button>
          </div>
        </div>
    </div>
</div>
<?php endif ?>

<?php get_instance()->js[] = '<script src="'.base_url().'js/transferencias.js?v=1.2"></script>'; ?>
<?php $this->load->view('_inventario_modal',array(),FALSE,'movimientos'); ?>


<script>    
    
    window.shouldSave = true;   
    window.addEventListener('beforeunload',function(e){
        if (window.shouldSave) {
            e.preventDefault();
            e.returnValue = '';
            return;
        }
        delete e['returnValue'];
    });

    window.afterLoad.push(function(){
        window.venta = new PuntoDeVenta();  
        if(editar!=undefined){      
            console.log(editar);
            
            venta.setDatos(editar);
            $(".chzn-container").remove();
            $("select,input").show().attr('disabled',true);
        }else{
            if(rules.length>0){
                setTimeout(()=>{
                    filtrar_origenes($("#sucursal_origen").val());
                },600);
                
                $("#sucursal_origen").on('change',function(){filtrar_origenes($(this).val())});
            }
        }
        if(!editar){ 
        initDatos();
        venta.initEvents();
        venta.updateFields();
        }

        $(document).on('shown.bs.modal',"#procesar",function(){
            $("#procesar input[name='pago_guaranies']").focus();
        });
    });
    

    function initDatos(){
        venta.datos.sucursal_origen = '<?= $this->user->sucursal ?>';
        venta.datos.caja = '<?= $this->user->caja ?>';
        venta.datos.cajadiaria = '<?= $this->user->cajadiaria ?>';
        venta.datos.fecha_solicitud = '<?= date("Y-m-d H:i") ?>'; 
        venta.datos.user_id = '<?= $this->user->id ?>';     
        venta.modPrecio = modPrecio;
        venta.refresh();
    }

    function imprimir(codigo){        
        window.open('<?= base_url() ?>reportes/rep/verReportes/70/html/codigo/'+codigo);
    }

    function selCod(codigo,esc,obj){        
        /*venta.addProduct(codigo);     
        $("#inventarioModal").modal('toggle');
        $("#codigoAdd").focus();*/
        $("#inventarioModal").modal('toggle');
        var ht = $("#codigoAdd").val();     
        venta.addProduct(ht+codigo,obj);           
        $("#codigoAdd").focus();
    }

    function nuevaVenta(){      
        venta.initVenta();
        initDatos();        
        $("#responseTransferencia").html('').removeClass('alert alert-info alert-danger alert-success');                       
        $("button").attr('disabled',false);        
        onsend = false;
    }

    
    function sendVenta(){
        if(typeof(editar)=='undefined'){
            if(!onsend){
                onsend = true;
                var datos =  JSON.parse(JSON.stringify(venta.datos));
                datos.productos = JSON.stringify(datos.productos);
                var accion = typeof(editar)=='undefined'?'insert':'update/'+editar.id;
                $("button").attr('disabled',true);
                insertar('movimientos/transferencias/transferencias/'+accion,datos,'#responseTransferencia',function(data){                      
                    var id = data.insert_primary_key;                       
                    var enlace = '';
                    $("#responseTransferencia").removeClass('alert-info');
                    imprimir(id);
                    enlace = 'javascript:imprimir('+id+')';
                    $('#responseTransferencia').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar <a href="'+enlace+'">este enlace</a></p>');                    
                    $(".btnNueva").attr('disabled',false);                  
                    onsend = false;
                },function(){
                    onsend = false;
                    $("button").attr('disabled',false);
                });
            }
        }else{
            alert('Edición no permitida');
        }
    }

    function vaciarStock(){
        venta.datos.productos = [];
        if($("#sucursal_origen").val()!=''){
            $.post('<?= base_url('movimientos/productos/vaciarStock/') ?>/'+$("#sucursal_origen").val(),{},function(data){
                let d = JSON.parse(data);
                for(var i of d){
                    console.log(i.id,i.stock+'*'+i.producto);
                    venta.addProduct(i.stock+'*'+i.producto);
                }
            });
        }
    }

    function filtrar_origenes(origen){
        
        let o = [];
        let options = '<option value="" selected="selected">Seleccione una opcion</option>';
        if(rules[origen]){
            for(var i in rules[origen]){
                options+= '<option value="'+rules[origen][i][0]+'" selected="selected">'+rules[origen][i][1]+'</option>';
            }
        }
        $("#sucursal_destino").html(options).chosen().trigger('liszt:updated');
    }
</script>