<form action="movimientos/facturas_clientes/pagos_pagare" method="post" onsubmit="sendForm(this,'.response'); return false;">
	<div class="modal fade" id="pagare_pagos_modal" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-lg" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	            	<h4 class="modal-title">Añadir pagos</h4>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>	                
	            </div>
            	<div class="modal-body">                
	        		<div class="kt-portlet__body">
		                <div class="kt-section kt-section--first">
		                	<div class="nform alert alert-info">
		                		Selecciona las facturas que deseas añadir a este pago antes de poder guardar el pago
		                	</div>
		                    <div class="row form">                
		                        <div class="col-12 col-md-4">
		                            <div class="form-group" id="nro_recibo_field_box">
		                                <div><label for="field-nro_recibo" id="nro_recibo_display_as_box">Nro recibo:</label></div>
		                                <input id="field-nro_recibo" name="nro_recibo" class="form-control nro_recibo" type="text" value="" maxlength="25">                            
		                            </div>
		                        </div>
		                        <div class="col-12 col-md-4">
		                            <div class="form-group" id="formapago_id_field_box">
		                                <div><label for="field-formapago_id" id="formapago_id_display_as_box">Formapago id<span class="required">*</span> :</label></div>
		                                <?php echo form_dropdown_from_query('formapago_id','formapago','id','denominacion','','id="field-formapago_id"',FALSE); ?>
		                            </div>
		                        </div>
		                        <div class="col-12 col-md-4">
		                            <div class="form-group" id="concepto_field_box">
		                                <div><label for="field-concepto" id="concepto_display_as_box">Concepto<span class="required">*</span> :</label></div>
		                                <input id="field-concepto" name="concepto" class="form-control concepto" type="text" value="" maxlength="225">                            
		                            </div>
		                        </div>
		                        <div class="col-12 col-md-4">
		                            <div class="form-group" id="monto_field_box">
		                                <div><label for="field-monto" id="monto_display_as_box">Monto<span class="required">*</span> :</label></div>
		                                <input id="field-monto" name="monto" class="form-control monto" type="text" value="" maxlength="20">                            
		                            </div>
		                        </div>
		                        <div class="col-12 col-md-4" style="display: none;">
			                        <div class="form-group" id="bancos_id_field_box">
			                            <div><label for="field-bancos_id" id="bancos_id_display_as_box">Banco:</label></div>
			                            <?php echo form_dropdown_from_query('bancos_id','bancos','id','denominacion','','id="field-bancos_id"',FALSE); ?>
			                        </div>
		                        </div>
		                        <div class="col-12 col-md-4" style="display: none;">
		                            <div class="form-group" id="cuentas_bancos_id_field_box">
		                                <div><label for="field-cuentas_bancos_id" id="cuentas_bancos_id_display_as_box">Cuenta:</label></div>
		                                <?php echo form_dropdown_from_query('cuentas_bancos_id','cuentas_bancos','id','nro_cuenta','','id="field-cuentas_bancos_id"',FALSE); ?>
		                            </div>
		                        </div>
				                <div class="col-12 col-md-4" style="display: none;">
		                            <div class="form-group" id="nro_cheque_field_box">
		                                <div><label for="field-nro_cheque" id="nro_cheque_display_as_box">Nro cheque:</label></div>
		                                <input id="field-nro_cheque" name="nro_cheque" nro="" cheque="" type="text" value="" class="nro_cheque numeric form-control" maxlength="11">                            
		                            </div>
		                        </div>
				                <div class="col-12 col-md-4" style="display: none;">
		                            <div class="form-group" id="tipo_cheque_field_box">
		                                <div><label for="field-tipo_cheque" id="tipo_cheque_display_as_box">Tipo cheque:</label></div>
		                                <?php echo form_dropdown_from_query('tipo_cheque','tipo_cheques','id','denominacion','','id="field-tipo_cheque"',FALSE); ?>
		                            </div>
		                        </div>
				                <div class="col-12 col-md-4" style="display: none;">
		                            <div class="form-group" id="fecha_emision_field_box">
		                                <div><label for="field-fecha_emision" id="fecha_emision_display_as_box">Fecha emision:</label></div>
		                                <input type="date" name="fecha_emision" id="field-fecha_emision" class="form-control" value="2022-12-19">                            
		                            </div>
		                        </div>
				                <div class="col-12 col-md-4" style="display: none;">
	                                <div class="form-group" id="fecha_pago_field_box">
	                                    <div><label for="field-fecha_pago" id="fecha_pago_display_as_box">Fecha pago:</label></div>
	                                    <input type="date" name="fecha_pago" id="field-fecha_pago" class="form-control" value="2022-12-19">                            
	                                </div>
	                            </div>
	                            <div class="col-12 col-md-4">
	                                <div class="form-group" id="facturas_field_box">	                                    
	                                    <div>
	                                    	Facturas: <span id="facturas"></span><br/>
	                                    	Saldo: <span id="saldo"></span>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
			                <!-- Start of hidden inputs -->
	                    	<input id="field-facturas_clientes" type="hidden" name="facturas_clientes" value="<?= $cliente ?>">
	                    	<input id="field-fecha" type="hidden" name="fecha" value="<?= date("Y-m-d H:i:s") ?>">
	                    	<input id="field-cajadiaria_id" type="hidden" name="cajadiaria_id" value="<?= $this->user->cajadiaria ?>">
	                    	<input id="field-user_id" type="hidden" name="user_id" value="<?= $this->user->id ?>">
	                    	<input id="field-anulado" type="hidden" name="anulado" value="">
	                    	<input id="field-facturas" type="hidden" name="facturas" value="">
	                    	<!-- End of hidden inputs -->
	                	</div>
	            	</div>
           			<div id="report-error" class="response"></div>
	            </div>
	            <div class="modal-footer">
	            	<button type="submit" class="btn btn-success form">Guardar</button>        
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>        
	            </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</form>

<script>
	function pagarModal(){
		$("#pagare_pagos_modal .form").hide();
		$("#pagare_pagos_modal .nform").show();
		let f = $("tbody .kt-checkbox input:checked");
		let s = 0;
		let fs = [];
		if(f.length>0){
			$("#pagare_pagos_modal .form").show();
			$("#pagare_pagos_modal .nform").hide();
			$("#facturas").html(f.length);
			$("tbody .kt-checkbox input:checked").toArray().map((x)=>{
				if($(x).val()!=''){
					s+= parseFloat($(x).parents('tr').find('td:nth-child(3)').text().trim())
					fs.push($(x).val());
				}
			});
			$("#saldo").html(s);
			$("#field-facturas").val(fs.join(','));
		}


		$("#pagare_pagos_modal").modal('show');
	}
</script>

<script>
	var bancos = '';
    var cheques = '';
	window.afterLoad.push(function(){
		if($("#field-formapago_id").val()==''){
			$("#field-formapago_id").val('1').trigger('liszt:updated');
			$("#field-concepto").val('PAGO POR FACTURA');
		}

		bancos = $("#bancos_id_field_box,#cuentas_bancos_id_field_box");
        cheques = $("#nro_cheque_field_box,#tipo_cheque_field_box,#fecha_emision_field_box,#fecha_pago_field_box");
        bancos.parent().hide();
        cheques.parent().hide();
        $("#field-formapago_id").on('change',function(){
            var forma = $(this).val();
            bancos.parent().hide();
            cheques.parent().hide();
            switch(forma){
                case '6': //Transferencia
                    bancos.parent().show();
                break;
                case '5': //Deposito
                    bancos.parent().show();
                break;
                case '3': //Cheque Vista
                	bancos.parent().show();
                    cheques.parent().show();
                    $("#field-tipo_cheque").val(1).trigger('liszt:updated');
                break;
                case '4': //Cheque
                    bancos.parent().show();
                    cheques.parent().show();
                    $("#field-tipo_cheque").val(-1).trigger('liszt:updated');
                break;
            }
        });

        $(document).on('change','#field-bancos_id',function(e) {
	        if($(this).val()!=''){
                e.stopPropagation();
                var selectedValue = $('#field-bancos_id').val();                    
                $.post(window.URI+'movimientos/facturas_clientes/facturas_clientes_pagos/ajax_extension/cuentas_bancos_id/bancos_id/'+encodeURI(selectedValue.replace(/\//g,'$this->slash_replacement')), {}, function(data) {                 
                var $el = $('#field-cuentas_bancos_id');
                          var newOptions = data;
                          $el.empty(); // remove old options
                          $el.append($('<option></option>').attr('value', '').text(''));
                          $.each(newOptions, function(key, value) {
                          	if(value!=''){
	                            $el.append($('<option></option>')
	                               .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
                      		}
                  		  });
                          //$el.attr('selectedIndex', '-1');
                          //$el.chosen().trigger('liszt:updated');

	        },'json');
	        $('#field-cuentas_bancos_id').change();
	    }
	    });
	});
</script>