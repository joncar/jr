<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Pendientes</a>
    <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Facturados</a>    
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
  	<?= $output ?>
  </div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  	<?php get_instance()->facturadas(); ?>
  </div>
</div>


<script>
	function showDetail(id){
		$.post('<?= base_url('movimientos/OrdenTrabajo/orden_trabajo_detail/') ?>/'+id+'/',{},function(data){
			emergente(data);
		});
	}
</script>

<script>
	function anular(id){
        if(confirm('Seguro que desea anular esta factura?')){
            $.post('<?= base_url('movimientos/OrdenTrabajo/orden_trabajo/anular') ?>',{id:id},function(data){
	                emergente(data);
            });
        }
    }
</script>