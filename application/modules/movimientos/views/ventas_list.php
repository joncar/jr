<?= $output ?>
<?php $this->load->view('_token_modal',array(),FALSE,'movimientos'); ?>
<script>
	<?php
		$ajustes = $this->ajustes;
		$caja = $this->db->get_where('cajas',['id'=>$this->user->caja])->row();
		$ajustes->solicitar_token_ventas = $caja->solicitar_token;		
	?>
	var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;
	var validateToken = function(){
	    return new Promise((resolve,reject)=>{
	        if(ajustes.solicitar_token_ventas=='0'){
	            resolve();
	        }else{
	            validarToken().then((result)=>{
	                if(result){
	                    resolve();
	                }else{
	                    reject();
	                }
	            });
	        }
	    });
	}
	function showDetail(id){
		$.post('<?= base_url('movimientos/ventas/ventas_detail/') ?>/'+id+'/',{},function(data){
			emergente(data);
		});
	}

	function anular(id){
        if(confirm('Seguro que desea anular esta factura?')){
            validateToken().then(()=>{
	            $.post('<?= base_url('movimientos/ventas/ventas/anular') ?>',{id:id},function(data){
	                emergente(data);
	            });
            });
        }
    }
</script>