<?php
	if(!empty($edit) && is_numeric($edit)){
		$nota = $this->elements->nota_remision(array('id'=>$edit));
	}
?>
<style>
	a:focus,div:focus,.chzn-container-active{
		border:1px dashed blue;
	}
	body {
        color: #000;
        font-weight: 600;
    }
    .form-control{
    	color: #000;
	    font-weight:600;
	    opacity: 1;
	    font-size:15px;
    }
	.panel{
		border-radius:0px;
	}

	.error{
		border: 1px solid red !important;
	}

	.patternCredito{
		background: #ffffffa6;width: 100%;height: 100%;position: absolute;top: 0px;left: 0px;
	}

	.btnsaldo{
		position: absolute;top: 0;right: 15px;
	}
	@media screen and (max-width:768px){
		.btnsaldo{
			top: -15px;
			font-size:20px;
		}	
	}
	.kt-portlet{
		margin:0;
	}

	.table thead th, .table td{
		padding:6px;
		color: #000;
		font-weight: 600;
	}

	tbody .form-control{
		height:22px;
	}

	.kt-portlet .kt-portlet__head{
		min-height: inherit;
		padding:10px;
	}

	.totales input, .totales .form-control:focus{
		background:lightgreen !important;
		padding: 0 12px;
		border-radius: 0;
	}

	.pagoEfectivo input, .pagoEfectivo .form-control:focus{
		background-color: lightblue;
		border-radius: 0;
		padding: 0 12px;
	}

	.vueltos input{
		background: #e8e800 !important;
		color: red;
		border-radius: 0;
		padding: 0 12px;
	}

	input.guaranies{
		font-weight: bold;
		font-size: 19px;
	}
	div.inp{
		position: relative;
	}
	div.banderas{
		background-image:url('<?= base_url() ?>img/banderas.jpg');
		width:20px;
		height:20px;	
		position: absolute;
		right:0px;
		top:0px;
		background-size: 100%;	
	}	
	div.banderas.us{
		background-position: 0 0px;
	}
	div.banderas.ar{
		background-position: 0 -20px;
	}		
	div.banderas.br{
		background-position: 0 -40px;
	}
	div.banderas.py{
		background-position: 0 -60px;
	}

	div.banderas-2x{
		background-image:url('<?= base_url() ?>img/banderas.jpg');
		width:40px;
		height:40px;	
		position: absolute;
		right:0px;
		top:0px;
		background-size: 100%;	
	}	
	div.banderas-2x.us{
		background-position: 0 0px;
	}
	div.banderas-2x.ar{
		background-position: 0 -40px;
	}		
	div.banderas-2x.br{
		background-position: 0 -80px;
	}
	div.banderas-2x.py{
		background-position: 0 -120px;
	}
	#procesar th{
		font-weight: bold;
		font-size: 16px;
	}
	.btn-lg, .btn-group-lg > .btn {
	    padding: 0.7rem 1.45rem;
	}
</style>
<div class="kt-portlet">
	<div class="kt-portlet__body" style="padding:10px 25px;">
		<div class="kt-section">
			<div class="row" style="position: relative;">
				<div class="col-12 col-md-9" style="background: lightblue;">
					<div class="row">			
						<div class="col-xs-12 col-md-3">
							#Venta
							<input type="text" class="form-control" name="venta" value="" placeholder="Inserte el numero de venta aquí" class="form-control">
						</div>
						<!--<div class="col-xs-12 col-md-2">
							#Nota
							<input type="text" name="nro_nota_credito" value="" placeholder="Inserte el numéro de control de nota de crédito" class="form-control">
						</div>-->
						<div class="col-xs-12 col-md-3">
							Cliente
							<input type="text" class="form-control" name="cliente" value="" placeholder="Inserte el numero de venta" readonly="" class="form-control">
						</div>
						<div class="col-xs-12 col-md-3">
							Origen
							<input type="text" class="form-control" name="origen" value="" placeholder="" readonly="" class="form-control">
						</div>
						<div class="col-xs-12 col-md-3">
							Destino
							<input type="text" class="form-control" name="destino" value="" placeholder="" readonly="" class="form-control">
						</div>												
					</div>
				</div>
				<div class="col-12 col-md-3" style="position: relative;">
					<span style="position: absolute;top: 26px;left: 30px;font-weight: bold;">Total Nota Gs: </span>
					<input type="text" name="total_nota" class="form-control" readonly="" value="0" style="font-size:30px; font-weight:bold; text-align: right;height: 89px;vertical-align: baseline;margin-top: 19px; background:lightgreen">
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-md-9" style="padding-right: 39px;">
		<div class="kt-portlet">
			<div class="kt-portlet__body" style="padding: 0 15px;">
				<div class="kt-section">
					<div style="height:350px; overflow-y:auto; background:#f2f3f8">
						<table class="table table-bordered" id="ventaDescr">
							<thead>
								<tr>
									<th>Código</th>
									<th>Nombre</th>
									<th>Cantidad</th>
									<th>Precio</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>

								<tr id="productoVacio">
									<td>
										<a href="javascript:void(0)" class="rem" style="display:none;color:red">
											<i class="fa fa-times"></i>
										</a> 
										<span>&nbsp;</span>
									</td>
									<td>&nbsp;</td>
									<td><input name="cantidad" class="cantidad form-control" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td><input name="precio" class="precio form-control" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0"></td>							
									<td>&nbsp;</td>
								</tr>

								
							</tbody>
						</table>
					</div>

					<div class="row">
						<div class="col-xs-12 col-md-2" style="text-align: center;padding: 6px;">
							Cant: <span id="cantidadProductos">0</span>					
						</div>
						<div class="col-xs-12 col-md-10" style="position: relative;">
							<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
							<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" <?= empty($edit)?'readonly=""':'' ?> style="padding-left: 25px;padding-right: 73px;">
							<button style="position: absolute;top: 1px;right: 16px;padding: 1px;" class="btn btn-primary">Insertar</button>

							<div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
	<div class="col-xs-12 col-md-3" style="padding-left: 5px;">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						<b>Resumen de nota</b>
					</h1>
				</div>
			</div>
			<div class="kt-portlet__head">
				<div class="row totales">
					<div class="col-12 col-md-4">Pesos: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<div class="inp">
							<input type="text" class="form-control" name="total_pesos" value="0" readonly="">
							<div class="banderas-2x ar"></div>
						</div>

					</div>
					<div class="col-12 col-md-4">Reales: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<div class="inp">
							<input type="text" class="form-control" name="total_reales" value="0" readonly="">
							<div class="banderas-2x br"></div>
						</div>

					</div>
					<div class="col-12 col-md-4">Dolares: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<div class="inp">
							<input type="text" class="form-control" name="total_dolares" value="0" readonly="">
							<div class="banderas-2x us"></div>
						</div>

					</div>
				</div>
			</div>
		</div>

		
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-md-9">
		<div class="btn-group btn-group-justified" role="group" aria-label="...">
		  <?php if(empty($edit)): ?>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default btnNueva" onclick="nuevaNota();">Nueva nota</button>
		  </div>
		  <?php else: ?>
		  	<div class="btn-group" role="group">
		    	<a href="<?= base_url() ?>movimientos/ventas/nota_remision/add" class="btn btn-default btnNueva">Nueva nota</a>
		  	</div>
		  <?php endif ?>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary" onclick="saveNota()">Guardar nota</button>
		  </div>		  
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 respuestaVenta"></div>
</div>
<?php $this->load->view('_inventario_modal',array(),FALSE,'movimientos'); ?>
<?php get_instance()->js[] = '<script src="'.base_url().'js/notasCredito.js"></script>'; ?>
<script>
	<?php
		$ajustes = $this->ajustes;
	?>
	var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
	var tasa_real = <?= $ajustes->tasa_reales ?>;
	var tasa_peso = <?= $ajustes->tasa_pesos ?>;
	var codigo_balanza = <?= $ajustes->cod_balanza ?>;
	var vender_sin_stock = <?= $ajustes->vender_sin_stock ?>;
	var onsend = false;

	window.shouldSave = true;	
	window.addEventListener('beforeunload',function(e){
		if (window.shouldSave) {
	        e.preventDefault();
	        e.returnValue = '';
	        return;
	    }
	    delete e['returnValue'];
	});

	function imprimir(codigo){	
		var idReporte = '<?= $this->ajustes->id_reporte_nota_credito ?>';
		window.open('<?= base_url() ?>reportes/rep/verReportes/'+idReporte+'/pdf/valor/'+codigo);	        
	}
	
	function initDatos(){
		nota.datos.sucursal = '<?= $this->user->sucursal ?>';
		nota.datos.caja = '<?= $this->user->caja ?>';
		nota.datos.cajadiaria = '<?= $this->user->cajadiaria ?>';
		nota.datos.fecha = '<?= date("Y-m-d H:i:s") ?>';	
		nota.datos.usuario = '<?= $this->user->id ?>';		
	}
	window.afterLoad.push(function(){
		window.nota = new NotaCredito();
		initDatos();
		nota.initEvents();
		//venta.updateFields();		
		<?php 
			if(!empty($edit) && is_numeric($edit)): 			
		?>
			var edit = <?= count($nota)>0?json_encode($nota[0]):'{}' ?>;
			nota.datos = edit;
			nota.INP_nro_venta.val(edit.venta);
			nota.INP_nro_nota.val(edit.nro_nota_credito);
			console.log(nota.datos);
			nota.updateData();		
		<?php endif ?>
	});
	function selCod(codigo){		
		nota.addProduct(codigo);		
		$("#inventarioModal").modal('toggle');
	}

	function saveNota(){		
		if(!onsend){
			onsend = true;
			var datos =  JSON.parse(JSON.stringify(nota.datos));
			datos.productos = JSON.stringify(datos.productos);
			var accion = '<?= empty($edit)?'insert':'update/'.$edit  ?>';
			$("button").attr('disabled',true);
			insertar('movimientos/ventas/nota_remision/'+accion,datos,'.respuestaVenta',function(data){						
				var id = data.insert_primary_key;						
				var enlace = '';
				$(".respuestaVenta").removeClass('alert-info');
				imprimir(id);
				enlace = 'javascript:imprimir('+id+')';
				$('.respuestaVenta').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar <a href="'+enlace+'">este enlace</a></p>');
				$(".btnNueva").show();
				$(".btnNueva").attr('disabled',false);
			},function(){
				onsend = false;
				$("button").attr('disabled',false);
			});
		}	
	}

	function nuevaNota(){
		$("button").attr('disabled',false);
		nota.initNota();
	}
</script>