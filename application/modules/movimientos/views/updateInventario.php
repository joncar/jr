<?= $output ?>
<script>
	window.afterLoad.push(function(){
		$(document).on('change',".stockage",function(){
			if(confirm('¿Seguro desea hacer esta actualización?, nota: esta acción no tiene reversa')){
				$(".stockage").attr('disabled',true);
				$.post('<?= base_url('movimientos/productos/updateInventario/update/') ?>/'+$(this).data('id'),{
					stock:$(this).val()
				},function(){
					$(".stockage").attr('disabled',false);
				});
			}
		});
		$(".searchActionClick").click();
		$($('input[name="search_text[]"]')[0]).focus();
	});
</script>