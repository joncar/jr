<script>
	window.afterLoad.push(function(){
		if($("#field-moneda_entrada").val()==''){
			$("#field-moneda_salida").val(1).chosen().trigger('liszt:updated');
			$("#field-tipo_operacion").val(1).chosen().trigger('liszt:updated');
		}
		$("#field-tipo_operacion").on('change',function(){
			if($(this).val()=='1'){
				$("#field-moneda_salida").val(1).chosen().trigger('liszt:updated');
				$("#field-moneda_entrada").val('').chosen().trigger('liszt:updated');
			}else{
				$("#field-moneda_entrada").val(1).chosen().trigger('liszt:updated');
				$("#field-moneda_salida").val('').chosen().trigger('liszt:updated');
			}
			$("#field-monto_entrada").trigger('change');
		});	
		$("#field-moneda_entrada,#field-monto_entrada,#field-moneda_salida").on('change',function(){
			if($("#field-tipo_operacion").val()=='-1'){
				var tasa = "#field-moneda_salida";
			}else{
				var tasa = "#field-moneda_entrada";
			}

			if($(tasa+" option:selected").text()!=''){
				var compra = parseFloat($(tasa+" option:selected").text().split('C.')[1].replace('V.','').trim().split(/(\s+)/)[0]);
				var venta = parseFloat($(tasa+" option:selected").text().split('C.')[1].replace('V.','').trim().split(/(\s+)/)[2]);
				var tipo = parseInt($("#field-tipo_operacion").val());
				var entrada = parseFloat($("#field-monto_entrada").val());
				entrada = isNaN(entrada)?1:entrada;
				var salida = 0;
				if(!isNaN(compra) && !isNaN(venta) && !isNaN(tipo) && !isNaN(entrada) && !isNaN(salida)){
					var cambio = tipo==1?compra:venta;				
					if(tipo==1){
						salida = entrada*cambio;
					}else{
						salida = entrada/cambio;
					}
					$("#field-monto_entrada").val(entrada.toFixed(2));
					$("#field-monto_salida").val(salida.toFixed(2));
					$("#field-precio_cambio").val(cambio);
				}else{
					$("#field-precio_cambio").val('0');
					$("#field-monto_salida").val('0');
				}
			}
		});	

		$("#field-monto_salida").on('change',function(){
			if($("#field-tipo_operacion").val()=='-1'){
				var tasa = "#field-moneda_salida";
			}else{
				var tasa = "#field-moneda_entrada";
			}
			if($(tasa+" option:selected").text()!=''){
				var compra = parseFloat($(tasa+" option:selected").text().split('C.')[1].replace('V.','').trim().split(/(\s+)/)[0]);
				var venta = parseFloat($(tasa+" option:selected").text().split('C.')[1].replace('V.','').trim().split(/(\s+)/)[2]);
				var tipo = parseInt($("#field-tipo_operacion").val());			
				var salida = parseFloat($("#field-monto_salida").val());
				if(!isNaN(compra) && !isNaN(venta) && !isNaN(tipo) && !isNaN(salida)){
					var cambio = tipo==1?compra:venta;				
					if(tipo==1){
						entrada = salida/cambio;
					}else{
						entrada = salida*cambio;
					}
					$("#field-monto_entrada").val(entrada.toFixed(2));
					$("#field-monto_entrada").trigger('change');								
				}
			}
		});	
	});	
</script>