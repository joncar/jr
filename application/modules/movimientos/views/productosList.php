<div class="row">
	<div class="col-12">
		<a href="<?= base_url('movimientos/productos/productos/anulados') ?>" class="btn btn-info">Anulados</a>
	</div>
</div>
<?php echo $output ?>
<script>
	function masDetalles(codigo){
		window.afterLoad = [];
		$.get('<?= base_url() ?>movimientos/productos/productos/masDetalles/'+codigo,{},function(data){			
			$("body").append(data);
			$("body").find('#masdetalles').modal('toggle');
			$("body").find('#masdetalles').on('hidden.bs.modal',function(){
				$("body").find('#responseMasDetalles').remove();
			});
			for(var i in window.afterLoad){
				window.afterLoad[i]();
			}
		});
	}
	window.afterLoad.push(()=>{
		$(".flexigrid table thead tr:nth-child(2) > th:nth-child(3)").attr('colspan',3);
		$(".flexigrid table thead tr:nth-child(2) > th:nth-child(4)").remove();
		$(".flexigrid table thead tr:nth-child(2) > th:nth-child(4)").remove();
		$(".flexigrid table thead tr:nth-child(2) > th:nth-child(3) input[type='text']").attr('type','hidden');
		$(".flexigrid table thead tr:nth-child(2) > th:nth-child(3)").append('<input type="text" name="query" class="form-control" placeholder="Buscar por código, interno o nombre"/>');
	});
</script>