<?= $output ?>
<div style="text-align:right">
	<a class="btn btn-info" href="<?php echo base_url('movimientos/creditos/creditos') ?>">Volver a créditos</a>
</div>
<script>
	function printRecibo(id){
		window.open('<?= base_url() ?>reportes/rep/verReportes/90/html/valor/'+id,'Recibo');
	}

	function pagos(id){
		$.post('<?= base_url('movimientos/creditos/pagar_credito/') ?>/'+id+'/?modal=1',{},function(data){
			emergente(data);
		});
	}

	window.afterLoad.push(function(){
		if($("#field-efectivo").val()==''){
			calcular();
		}

		$("#field-efectivo").on('change',function(){
			calcular();
		});

		$("#field-total_mora").on('change',function(){
			calcular();
		});
	});

	function calcular(){
		var mora = parseFloat($("#field-total_mora").val());
			mora = isNaN(mora)?0:mora;
			var cuota = parseFloat($("#field-total_pagado").val());
			cuota = isNaN(cuota)?0:cuota;
			var total = mora+cuota;
			$("#field-total_pago_mora").val(total);

		var total = parseFloat($("#field-total_pago_mora").val());
			var efectivo = parseFloat($("#field-efectivo").val());
			efectivo = isNaN(efectivo)?0:efectivo;
			var vuelto = efectivo - total;
			vuelto = vuelto<0?0:vuelto;
			$("#field-vuelto").val(vuelto);
	}
</script>