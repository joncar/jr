
<?php 
	get_instance()->hcss[] = '
		<style>
			#tabla_tus th, #tabla_tus td {
			  font-size: 14px;
			  padding: 0 5px;
			}

			#tabla_tus td > div {
			  /*white-space: nowrap;*/
			  width: 120px;
			  /*overflow: hidden;*/
			  /*text-overflow: ellipsis;*/
			  padding: 10px 0;
			  /*overflow-x: auto;*/
			  overflow-y: auto;
			}

			#tabla_tus thead th {
			  background: #fff;
			  position: sticky;
			  top: 0;
			  box-shadow: 0 2px 2px -1px rgba(0,0,0,.4);
			  z-index: 1;
			  -webkit-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -moz-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -ms-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -o-box-shadow: -2px 2px 0 -1px #0d5a26;
			  box-shadow: -2px 2px 0 -1px #0d5a26;
			}
			div.inp{
				position: relative;
			}
			div.banderas{
				background-image:url('.base_url().'img/banderas.jpg);
				width:20px;
				height:20px;	
				position: absolute;
				right:0px;
				top:0px;
				background-size: 100%;	
			}	
			div.banderas.us{
				background-position: 0 0px;
			}
			div.banderas.ar{
				background-position: 0 -20px;
			}		
			div.banderas.br{
				background-position: 0 -40px;
			}
			div.banderas.py{
				background-position: 0 -60px;
			}

			div.banderas-2x{
				background-image:url('.base_url().'img/banderas.jpg);
				width:40px;
				height:40px;	
				position: absolute;
				right:0px;
				top:0px;
				background-size: 100%;	
			}	
			div.banderas-2x.us{
				background-position: 0 0px;
			}
			div.banderas-2x.ar{
				background-position: 0 -40px;
			}		
			div.banderas-2x.br{
				background-position: 0 -80px;
			}
			div.banderas-2x.py{
				background-position: 0 -120px;
			}
			.chzn-results li {

			    color: #000;

			}
			.cabecera input{
				height: auto;padding: 3px;
			}
			tbody .form-control {
			  font-size: 14px;
			  height: 22px;
			}
		</style>
	'
?>
<div class="row">	
	<div class="col-12 col-md-12 cabecera">
		<div class="kt-portlet kt-portlet--solid-info">			    
	        <div class="kt-portlet__body">
	            <div class="kt-section kt-section--first">
	                
	                <div class="row">                
	                    <?php foreach($fields as $field): if($field->field_name=='efectivo') break; ?>
	                        <div class="col-12 col-md-3 col-lg-3">
	                            <div class="form-group m-0" id="<?php echo $field->field_name; ?>_field_box">
	                                <div>
	                                	<label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo str_replace(' id','',$input_fields[$field->field_name]->display_as); ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label>
	                                </div>
	                                <?php echo $input_fields[$field->field_name]->input ?>                            
	                            </div>
	                        </div>
	                    <?php endforeach ?>
	                </div>

				</div>
	        </div>
		</div>

	</div>

	<div class="col-12 col-md-12">
		<div class="kt-portlet">	
		    <div class="kt-portlet__head">
		        <div class="kt-portlet__head-label">
		            <h3 class="kt-portlet__head-title">
		                Productos
		            </h3>
		        </div>
		    </div>
	        <div class="kt-portlet__body p-0">
	            <div id="seguimiento" class="kt-section kt-section--first" style="overflow-x:auto;overflow-y:auto;height: 40vh;">
	                <table class="table table-bordered table-striped" id="tabla_tus">
	                	<thead>
	                		<tr>
	                			<th>Código</th>
	                			<th>Nombre</th>
	                			<th>Precio Venta</th>
	                			<th>%Desc</th>
	                			<th>Precio Desc</th>
	                			<th>Stocks</th>
	                			<th>Anulado</th>
	                		</tr>
	                	</thead>
	                	<tbody>
	                		<tr class="empty">
	                			<td colspan="7">Añade productos a la lista</td>
	                		</tr>
	                		<tr class="producto" id="productoVacio">
								<td style="width:20%">
									<a href="javascript:void(0)" class="rem" style="color:red">
										<i class="fa fa-times"></i>
									</a> 
									<span data-val="codigo">&nbsp;</span>
								</td>
								<td data-val="nombre_comercial" style="width:30%">&nbsp;</td>
								<td data-val="precio_venta">&nbsp;</td>
								<td>
									<input type="text" data-name="por_desc" style="text-align: right;padding: 0 6px;" class="form-control">									
								</td>
								<td>
									<input type="text" data-name="precio_desc" style="text-align: right;padding: 0 6px;" class="form-control">
								</td>
								<td>
									<input type="text" data-name="stocks" readonly style="text-align: right;padding: 0 6px;" class="form-control">
								</td>
								<td>
									<?= form_dropdown('',['0'=>'NO','1'=>'SI'],0,'class="form-control" data-name="anulado" style="padding:0"'); ?>
									<div class="d-none extraFields"></div>
								</td>
							</tr>

							<!---------------- Update Temp ---------------->

							<?php 
								if(empty($primary_key)):
								foreach(get_instance()->db->get_where('ventatemp',['cajas_id'=>get_instance()->user->caja,'tipo'=>'descuentos'])->result() as $v): 
								$productos = json_decode($v->productos);
								foreach($productos as $p):
							?>

								<tr class="producto">
									<td>
										<a href="javascript:void(0)" class="rem" style="color:red">
											<i class="fa fa-times"></i>
										</a> 
										<span data-val="codigo"><?= $p->codigo ?></span>
									</td>
									<td data-val="nombre_comercial"><?= $p->nombre_comercial ?></td>
									<td data-val="precio_venta"><?= $p->precio_venta ?></td>
									<td>
										<input type="text" data-name="por_desc" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->por_desc ?>">										
									</td>
									<td>
										<input type="text" data-name="precio_desc" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_desc ?>">
									</td>
									<td>
										<input type="text" data-name="stocks" readonly style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->stocks ?>">
									</td>
									<td>
										<?= form_dropdown('',['0'=>'NO','1'=>'SI'],$p->anulado,'class="form-control" data-name="anulado" style="padding:0"'); ?>
										<div class="extraFields d-none">
											<?php 
												$hiddens = [
													'por_desc',
													'tipo_cantidad',
													'precio_desc',
													'stocks',
													'anulado'
												];
											?>
											<?php foreach($p as $nn=>$pp): if(!in_array($nn,$hiddens)): ?>
												<input type="hidden" data-name="<?= $nn ?>" value="<?= $pp ?>">
											<?php endif; endforeach ?>
										</div>
									</td>
								</tr>

								<?php endforeach ?>
							<?php endforeach ?>
							<?php endif ?>


							<!-------------- Edit --------------->
							<?php 
								if(!empty($primary_key)):
								foreach(get_instance()->db->get_where('descuento_detalle',['descuento_id'=>$primary_key])->result() as $p):									
							?>

								<tr class="producto">
									<td>
										<a href="javascript:void(0)" class="rem" style="color:red">
											<i class="fa fa-times"></i>
										</a> 
										<span data-val="codigo"><?= $p->codigo ?></span>
									</td>
									<td data-val="nombre_comercial"><?= $p->nombre_comercial ?></td>
									<td data-val="precio_venta"><?= $p->precio_venta ?></td>									
									<td>
										<input type="text" data-name="por_desc" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->por_desc ?>">										
									</td>
									<td>
										<input type="text" data-name="precio_desc" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_desc ?>">
									</td>
									<td>
										<input type="text" data-name="stocks" readonly style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->stocks ?>">
									</td>									
									<td>
										<?= form_dropdown('',['0'=>'NO','1'=>'SI'],$p->anulado,'class="form-control" data-name="anulado" style="padding:0"'); ?>
										<div class="extraFields d-none">
											<?php 
												$hiddens = [
													'por_desc',
													'precio_desc',
													'stocks',
													'anulado'
												];
											?>
											<?php foreach($p as $nn=>$pp): if(!in_array($nn,$hiddens)): ?>
												<input type="hidden" data-name="<?= $nn ?>" value="<?= $pp ?>">
											<?php endif; endforeach ?>
										</div>
									</td>
								</tr>
							<?php endforeach ?>
							<?php endif ?>
	                	</tbody>
	                </table>	                
	            </div>
	            <div class="row">
					<div class="col-12 col-md-2" style="text-align: center;padding: 6px;">
						Cant: <span id="cant">0</span>					
					</div>
					<div class="col-12 col-md-10" style="position: relative;">
						<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 13px;left: 16px;cursor:pointer;"></i>
						<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto (ALT + C)" style="padding-left: 25px;padding-right: 73px;" autocomplete="off">
						<button type="button" style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar">Insertar</button>
					</div>
				</div>

	        </div>

	        <div class="kt-portlet__foot">
			    <div class="kt-form__actions">
			    	<div class="report"></div>
			        <a class="btn btn-info" href="<?= base_url('movimientos/descuentos/descuentos') ?>">Atrás</a>
			        <button type="button" class="btnNueva btn btn-info m-10" data-toggle="modal" data-target="#inventarioModal">Inventario <span class="alt" style="display:none">(I)</span></button>
			    	<button type='submit' class="btn btn-success"><?= $accion ?></button>	

			    </div>
			</div>
		</div>

	</div>

</div>