<?php	
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-edit.js?v=1.1"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';
    get_instance()->hcss[] = '<style>.alert{flex-wrap:wrap;} .alert p{width:100%;}</style>';
?>

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $update_url, 'method="post" id="crudForm" onsubmit="venta.edit(this); return false;" autocomplete="off" enctype="multipart/form-data"'); ?>
    <?php get_instance()->load->view('cruds/descuentos/campos',['accion'=>'Actualizar','subject'=>$subject,'fields'=>$fields,'input_fields'=>$input_fields]); ?>
    <?php
        foreach($hidden_fields as $hidden_field){
            echo $hidden_field->input;
        }
    ?>
    <!-- End of hidden inputs -->
    <?php if ($is_ajax) { ?>
        <input type="hidden" name="is_ajax" value="true" />
    <?php }?>
    <?php echo form_close(); ?>
</div>
<?php get_instance()->js[] = '<script src="'.base_url('js/descuentos.js').'?v='.uniqid().'"></script>'; ?>
<?php
    $ajustes = $this->ajustes;
    $ajustes->solicitar_token_ventas = $this->db->get_where('cajas',['id'=>$this->user->caja])->row()->solicitar_token;
?>

<?php $this->load->view('_inventario_from_productos_modal.php',array(),FALSE,'movimientos'); ?>


<script>
    window.afterLoad.push(function(){
        Venta.prototype.updateTemp = function(){}
    });
    var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;    
    function selCod(codigo){
        venta.addProduct(codigo);        
        $("#inventarioModal").modal('hide');
    }

    window.shouldSave = false;  
    window.addEventListener('beforeunload',function(e){
        if (window.shouldSave) {
            e.preventDefault();
            e.returnValue = '';
            return;
        }
        delete e['returnValue'];
    });
</script>