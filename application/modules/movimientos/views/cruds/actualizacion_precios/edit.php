<?php   
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-add.js?v=1.1"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';
    get_instance()->hcss[] = '<style>.alert{flex-wrap:wrap;} .alert p{width:100%;}</style>';
?>

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $update_url, 'method="post" id="crudForm" onsubmit="insertar(\''.$update_url.'\',this,\'.report\'); return false;" autocomplete="off" enctype="multipart/form-data"'); ?>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Editar <?php echo $subject?>
                </h3>
            </div>
        </div>

        <!--begin::Form-->        
        <div class="kt-portlet__body">
            <div>
                <div class="row">
                    <?php foreach($fields as $field): ?>
                        <div class="col-6 col-md-3">
                            <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
                                <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>
                                <?php echo $input_fields[$field->field_name]->input ?>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>          
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Productos
                </h3>
            </div>
        </div>

        <!--begin::Form-->        
        
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__body p-0">
                            <div class="kt-section m-0">
                                <div id="seguimiento" style="height:350px; overflow-y:auto">
                                    <table class="table table-bordered" id="ventaDescr">
                                        <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Nombre</th>
                                                <th>Precio Costo</th>
                                                <th>Precio Venta|%Venta</th>
                                                <th>V.Mayorista1|%Venta</th>                            
                                                <th>V.Mayorista2|%Venta</th>
                                                <th>V.Mayorista3|%Venta</th>                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $productos = get_instance()->db->get_where('actualizador_precio_detalle',['actualizador_precio_id'=>$primary_key]);
                                                foreach($productos->result() as $p): 
                                                $producto = get_instance()->db->get_where('productos',array('id'=>$p->productos_id))->row();
                                            ?>

                                                <tr>
                                                    <td style="width:15%">
                                                        
                                                        <span data-name="codigo"><?= $producto->codigo ?></span>
                                                    </td>
                                                    <td style="width:25%" data-name="nombre_comercial"><?= $producto->nombre_comercial ?></td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <div><?= $p->precio_costo ?></div>                                                            
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <div style="width:100%"><?= $p->precio_venta ?></div>
                                                            <div style="background-color: #8b96fbe6; width:100%"><?= $p->porc_venta ?></div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <div style="width:100%"><?= $p->ventamayorista1 ?></div>
                                                            <div style="background-color: #8b96fbe6; width:100%"><?= $p->porc_mayorista1 ?></div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <div style="width:100%"><?= $p->ventamayorista2 ?></div>
                                                            <div style="background-color: #8b96fbe6; width:100%"><?= $p->porc_mayorista2 ?></div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <div style="width:100%"><?= $p->ventamayorista3 ?></div>
                                                            <div style="background-color: #8b96fbe6; width:100%"><?= $p->porc_mayorista3 ?></div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            <?php endforeach ?>

                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>              
            </div>
            <div class="kt-form__actions text-right mr-20">
                <!--<button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-servicios-tab').click()" class="btn btn-success">Atras</button>-->
                <button type="submit" class="btn btn-success">Actualizar Encabezado</button>                    
            </div>            
        

        <!--end::Form-->
    </div>   
    <div class="report"></div>     
    <!--end::Form-->
    <!-- Start of hidden inputs -->
    <?php
        foreach($hidden_fields as $hidden_field){
            echo $hidden_field->input;
        }
    ?>
    <?php if ($is_ajax) { ?>
        <input type="hidden" name="is_ajax" value="true" />
    <?php }?>
    <!-- End of hidden inputs -->
    <?php echo form_close(); ?>
</div>
<script>
    var validation_url = '<?php echo $validation_url?>';
    var list_url = '<?php echo $list_url?>';
    var message_alert_add_form = "Almacenado correctamente";
    var message_insert_error = "Error al insertar";
</script>

<?php get_instance()->hcss[] = "
    <style>
        .kt-section{
            margin:20px 0;
        }
        .chzn-drop,.chzn-search,.chzn-results,.chzn-search input{
            min-width:120px !important;
        }
        .form-group{
            margin-bottom:5px !important;
        }
        [type='radio']:not(:checked), [type='radio']:checked {
            position: initial;
            left: initial;
            opacity: 1;
            margin-right:5px;
        }
        @media screen and (max-width:480px){
            .nav {
                flex-wrap: wrap;
            }
        }

        .table th, .table td {
          padding: 2px 5px;
          vertical-align: middle;
          border-top: 1px solid #ebedf2;
        }

        .table th {
          background: #fff;
          position: sticky;
          top: 0;
          box-shadow: 0 2px 2px -1px rgba(0,0,0,.4);
          z-index: 1;
          -webkit-box-shadow: -2px 2px 0 -1px #ebedf2;
          -moz-box-shadow: -2px 2px 0 -1px #ebedf2;
          -ms-box-shadow: -2px 2px 0 -1px #ebedf2;
          -o-box-shadow: -2px 2px 0 -1px #ebedf2;
          box-shadow: -2px 2px 0 -1px #ebedf2;
        }
    </style>
"; ?>