<?php	
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-add.js?v=1.1"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';
    get_instance()->hcss[] = '<style>.alert{flex-wrap:wrap;} .alert p{width:100%;}tbody td{min-width:140px}tbody input{min-width:90px}</style>';
?>

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $insert_url, 'method="post" onsubmit="return false;" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Añadir <?php echo $subject?>
                </h3>
            </div>
        </div>

        <!--begin::Form-->        
        <div class="kt-portlet__body">
            <div>
            	<div class="row">
            		<?php foreach($fields as $field): ?>
        				<div class="col-6 col-md-3">
		                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
		                        <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>
		                        <?php echo $input_fields[$field->field_name]->input ?>
		                    </div>
	                    </div>
	                <?php endforeach ?>
                </div>
            </div>          
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Productos
                </h3>
            </div>
        </div>

        <!--begin::Form-->        
        
            <div class="row">
				<div class="col-12 col-md-12">
					<div class="kt-portlet">
						<div class="kt-portlet__body p-0">
							<div class="kt-section m-0">
								<div id="seguimiento" style="height:350px; overflow-y:auto">
									<table class="table table-bordered" id="ventaDescr">
										<thead>
											<tr>
												<th>Código</th>
												<th>Nombre</th>
												<th>Precio Costo</th>
												<th>Precio Venta|%Venta</th>
												<th>Cant|V.Mayorista1|%Venta</th>							
												<th>Cant|V.Mayorista2|%Venta</th>
												<th>Cant|V.Mayorista3|%Venta</th>												
											</tr>
										</thead>
										<tbody>

											<tr id="productoVacio">
												<td style="width:15%">
													<a href="javascript:void(0)" class="rem" style="display:none;color:red">
														<i class="fa fa-times"></i>
													</a> 
													<span data-name="codigo">&nbsp;</span>
													<input type="hidden" name="id">
													<input type="hidden" name="codigo">
													<input type="hidden" name="nombre_comercial">
												</td>
												<td style="width:25%" data-name="nombre_comercial">&nbsp;</td>
												<td>
													<input name="precio_costo" class="precio_costo form-control" type="text" style="display:none;" value="0">
												</td>
												<td>
													<div class="d-flex">
														<input name="precio_venta" class="precio_venta form-control" type="text" style="display:none;" value="0">
														<input name="porc_venta" class="form-control porc_venta" type="text" value=""  placeholder="%Venta" style="background-color: #8b96fbe6;">
													</div>
												</td>
												<td>
													<div class="d-flex">
														<input name="cant_1" class="cant_1 form-control" type="text" style="display:none;" value="0">
														<input name="precio_venta_mayorista1" class="precio_venta_mayorista1 form-control" type="text" style="display:none;" value="0">
														<input name="porc_mayorista1" class="form-control" type="text" value="" placeholder="0" style="background-color: #8b96fbe6;">
													</div>
												</td>
												<td>
													<div class="d-flex">
														<input name="cant_2" class="cant_2 form-control" type="text" style="display:none;" value="0">
														<input name="precio_venta_mayorista2" class="precio_venta_mayorista2 form-control" type="text" style="display:none;" value="0">
														<input name="porc_mayorista2" class="form-control" type="text" value="" placeholder="0" style="background-color: #8b96fbe6;">
													</div>
												</td>
												<td>
													<div class="d-flex">
														<input name="cant_3" class="cant_3 form-control" type="text" style="display:none;" value="0">
														<input name="precio_venta_mayorista3" class="precio_venta_mayorista3 form-control" type="text" style="display:none;" value="0">
														<input name="porc_mayorista3" class="form-control" type="text" value="" placeholder="0" style="background-color: #8b96fbe6;">
													</div>
												</td>
											</tr>

											<?php 
												//$this->db->delete('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'actualizador_precio']);       
												$temp = $this->db->get_where('ventatemp',['cajas_id'=>$this->user->caja,'tipo'=>'actualizador_precio']);
												if($temp->num_rows()>0): 
												$productos = json_decode($temp->row()->productos);
												foreach($productos as $p):
											?>

												<tr>
													<td style="width:15%">
														<a href="javascript:void(0)" class="rem" style="color:red">
															<i class="fa fa-times"></i>
														</a> 
														<span data-name="codigo"><?= $p->codigo ?></span>
														<input type="hidden" name="id" value="<?= $p->id ?>">
														<input type="hidden" name="codigo" value="<?= $p->codigo ?>">
														<input type="hidden" name="nombre_comercial" value="<?= $p->nombre_comercial ?>">
													</td>
													<td style="width:25%" data-name="nombre_comercial"><?= $p->nombre_comercial ?></td>
													<td>
														<div class="d-flex">
															<input name="precio_costo" class="precio_costo form-control" type="text" value="<?= $p->precio_costo ?>" style="" value="0">														
														</div>
													</td>
													<td>
														<div class="d-flex">
															<input name="precio_venta" class="precio_venta form-control" type="text" value="<?= $p->precio_venta ?>" style="" value="0">
															<input name="porc_venta" class="form-control porc_venta" type="text" value="<?= $p->porc_venta ?>"  placeholder="%Venta" style="background-color: #8b96fbe6;">
														</div>
													</td>
													<td>
														<div class="d-flex">
															<input name="cant_1" class="cant_1 form-control" type="text" value="<?= $p->cant_1 ?>" style="" value="0">
															<input name="precio_venta_mayorista1" class="precio_venta_mayorista1 form-control" type="text" value="<?= $p->precio_venta_mayorista1 ?>" style="" value="0">
															<input name="porc_mayorista1" class="form-control" type="text" value="<?= $p->porc_mayorista1 ?>" placeholder="0" style="background-color: #8b96fbe6;">
														</div>
													</td>
													<td>
														<div class="d-flex">
															<input name="cant_2" class="cant_2 form-control" type="text" value="<?= $p->cant_2 ?>" style="" value="0">
															<input name="precio_venta_mayorista2" class="precio_venta_mayorista2 form-control" type="text" value="<?= $p->precio_venta_mayorista2 ?>" style="" value="0">
															<input name="porc_mayorista2" class="form-control" type="text" value="<?= $p->porc_mayorista2 ?>" placeholder="0" style="background-color: #8b96fbe6;">
														</div>
													</td>
													<td>
														<div class="d-flex">
															<input name="cant_3" class="cant_3 form-control" type="text" value="<?= $p->cant_3 ?>" style="" value="0">
															<input name="precio_venta_mayorista3" class="precio_venta_mayorista3 form-control" type="text" value="<?= $p->precio_venta_mayorista3 ?>" style="" value="0">
															<input name="porc_mayorista3" class="form-control" type="text" value="<?= $p->porc_mayorista3 ?>" placeholder="0" style="background-color: #8b96fbe6;">
														</div>
													</td>
												</tr>

											<?php endforeach; endif ?>

											
										</tbody>
									</table>
								</div>

								<div class="row">
									<div class="col-12 col-md-2" style="text-align: center;padding: 6px;">
										Cant: <span id="cantidadProductos">4</span>					
									</div>
									<div class="col-12 col-md-10" style="position: relative;">
										<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
										<div class="d-flex">
											<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" style="padding-left: 25px;padding-right: 73px;">											
										</div>							
										<button style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar insertar" type="button">Insertar</button>

										<div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</div>
            <div class="kt-form__actions text-right mr-20">
	        	<!--<button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-servicios-tab').click()" class="btn btn-success">Atras</button>-->
	            <button type="button" onclick="sendVenta()" class="btn btn-success">Guardar</button>                    
	        </div>            
        

        <!--end::Form-->
    </div>   
    <div class="report"></div>     
    <!--end::Form-->
    <!-- Start of hidden inputs -->
    <?php
        foreach($hidden_fields as $hidden_field){
            echo $hidden_field->input;
        }
    ?>
    <?php if ($is_ajax) { ?>
        <input type="hidden" name="is_ajax" value="true" />
    <?php }?>
    <!-- End of hidden inputs -->
    <?php echo form_close(); ?>
</div>
<?php get_instance()->js[] = '<script src="'.base_url().'js/actualizador_precios.js?v=1.7"></script>'; ?>
<?php get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.chosen.min.js?v=1.3"></script>'; ?>
<?php get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/ajax-chosen-jsonlist.js?v=1.3"></script>'; ?>
<?php $this->load->view('_inventario_modal',array(),FALSE,'movimientos'); ?>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_add_form = "Almacenado correctamente";
	var message_insert_error = "Error al insertar";
</script>

<script>
	window.trOriginal = '';
	window.table = '';
	window.empty = '';
	
</script>

<?php get_instance()->hcss[] = "
	<style>
		.kt-section{
			margin:20px 0;
		}
		.chzn-drop,.chzn-search,.chzn-results,.chzn-search input{
			min-width:120px !important;
		}
		.form-group{
			margin-bottom:5px !important;
		}
		[type='radio']:not(:checked), [type='radio']:checked {
		    position: initial;
		    left: initial;
		    opacity: 1;
		    margin-right:5px;
		}
		@media screen and (max-width:480px){
			.nav {
			    flex-wrap: wrap;
			}
		}

		.table th, .table td {
		  padding: 2px 5px;
		  vertical-align: middle;
		  border-top: 1px solid #ebedf2;
		}

		.table th {
		  background: #fff;
		  position: sticky;
		  top: 0;
		  box-shadow: 0 2px 2px -1px rgba(0,0,0,.4);
		  z-index: 1;
		  -webkit-box-shadow: -2px 2px 0 -1px #ebedf2;
		  -moz-box-shadow: -2px 2px 0 -1px #ebedf2;
		  -ms-box-shadow: -2px 2px 0 -1px #ebedf2;
		  -o-box-shadow: -2px 2px 0 -1px #ebedf2;
		  box-shadow: -2px 2px 0 -1px #ebedf2;
		}
	</style>
"; ?>

<script>
	<?php
		$ajustes = $this->ajustes;
	?>
	var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;
	var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
	var tasa_real = <?= $ajustes->tasa_reales ?>;
	var tasa_peso = <?= $ajustes->tasa_pesos ?>;
	var codigo_balanza = <?= json_encode(explode(',',str_replace(', ',',',$ajustes->cod_balanza))) ?>;
	var cantidades_mayoristas = <?php 
        $ar = array();
        foreach($this->db->get('cantidades_mayoristas')->result() as $c){
            $ar[] = $c;
        }
        echo json_encode($ar);
	?>;
	var vender_sin_stock = true;
	var onsend = false;

	var controlar_vencimiento = <?= $ajustes->controlar_vencimiento_stock ?>;

	var firstLoad = false;
	window.shouldSave = false;	

	window.addEventListener('beforeunload',function(e){
		if (window.shouldSave) {
	        e.preventDefault();
	        e.returnValue = '';
	        return;
	    }
	    delete e['returnValue'];
	});

	window.afterLoad.push(function(){
		window.venta = new PuntoDeVenta();		
		venta.initEvents();
	});

	function imprimir(codigo){
		var url = 'reportes/rep/verReportes/1/html/valor/'+codigo;        
	}

	function selCod(codigo,esc,obj){
		$("#inventarioModal").modal('toggle');
		var ht = $("#codigoAdd").val();		
		venta.addProduct(ht+codigo,obj);			
		$("#codigoAdd").focus();
	}

	function nuevaVenta(){
		if(!firstLoad || confirm('¿Seguro desea aperturar un nuevo registro?, se perderá todo el progreso no guardado')){		
			firstLoad = true;
			venta.resetVenta();			
			onsend = false;
		}
	}

	
	function sendVenta(){
		if(!onsend){
			onsend = true;				
			var ht = document.getElementById('crudForm');
			var datos = new FormData(ht);
			datos.append('productos',JSON.stringify(venta.getProductsJson()));
			$("button").attr('disabled',true);
			insertar('movimientos/actualizacionPrecios/actualizador_precio/insert',datos,'.report',function(data){						
				var id = data.insert_primary_key;						
				var enlace = '';
				$(".report").removeClass('alert-info');
				imprimir(id);
				enlace = 'javascript:imprimir('+id+')';
				$('.report').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar <a href="'+enlace+'">este enlace</a></p>');
				$(".btnNueva").show();
				$("button").attr('disabled',false);				
				nuevaVenta();
				onsend = false;
				window.shouldSave = false;
				firstLoad = false;
			},function(){
				onsend = false;
				$("button").attr('disabled',false);				
			});
		}
	}
</script>