
<?php 
	get_instance()->hcss[] = '
		<style>
			#tabla_tus th, #tabla_tus td {
			  font-size: 14px;
			  padding: 0 5px;
			}

			#tabla_tus td > div {
			  /*white-space: nowrap;*/
			  width: 120px;
			  /*overflow: hidden;*/
			  /*text-overflow: ellipsis;*/
			  padding: 10px 0;
			  /*overflow-x: auto;*/
			  overflow-y: auto;
			}

			#tabla_tus thead th {
			  background: #fff;
			  position: sticky;
			  top: 0;
			  box-shadow: 0 2px 2px -1px rgba(0,0,0,.4);
			  z-index: 1;
			  -webkit-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -moz-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -ms-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -o-box-shadow: -2px 2px 0 -1px #0d5a26;
			  box-shadow: -2px 2px 0 -1px #0d5a26;
			}
			div.inp{
				position: relative;
			}
			div.banderas{
				background-image:url('.base_url().'img/banderas.jpg);
				width:20px;
				height:20px;	
				position: absolute;
				right:0px;
				top:0px;
				background-size: 100%;	
			}	
			div.banderas.us{
				background-position: 0 0px;
			}
			div.banderas.ar{
				background-position: 0 -20px;
			}		
			div.banderas.br{
				background-position: 0 -40px;
			}
			div.banderas.py{
				background-position: 0 -60px;
			}

			div.banderas-2x{
				background-image:url('.base_url().'img/banderas.jpg);
				width:40px;
				height:40px;	
				position: absolute;
				right:0px;
				top:0px;
				background-size: 100%;	
			}	
			div.banderas-2x.us{
				background-position: 0 0px;
			}
			div.banderas-2x.ar{
				background-position: 0 -40px;
			}		
			div.banderas-2x.br{
				background-position: 0 -80px;
			}
			div.banderas-2x.py{
				background-position: 0 -120px;
			}
			.chzn-results li {

			    color: #000;

			}
			.cabecera input{
				height: auto;padding: 3px;
			}
			tbody .form-control {
			  font-size: 14px;
			  height: 22px;
			}
		</style>
	'
?>
<div class="row">	
	<div class="col-12 col-md-12">
		<div class="kt-portlet">	
		    <div class="kt-portlet__head">
		        <div class="kt-portlet__head-label">
		            <h3 class="kt-portlet__head-title">
		                Productos
		            </h3>
		        </div>
		    </div>
	        <div class="kt-portlet__body p-0">
	            <div id="seguimiento" class="kt-section kt-section--first" style="overflow-x:auto;overflow-y:auto;height: 40vh;">
	                <table class="table table-bordered table-striped" id="tabla_tus">
	                	<thead>
	                		<tr>
	                			<th>Código</th>
	                			<th>Nombre</th>
	                			<th>Unidad</th>
	                			<th>Cantidad</th>
	                			<th>Caja</th>
	                			<th>P.Costo</th>
	                			<th>Unidad Min</th>
	                			<th>Cant Min</th>
	                			<th>Caja Min</th>
	                			<th>Unidad May</th>
	                			<th>Cant May</th>
	                			<th>Caja May</th>
	                			<th>Unidad Camp</th>
	                			<th>Cant Camp</th>
	                			<th>Caja Camp</th>
	                		</tr>
	                	</thead>
	                	<tbody>

							<?php 
            					$productos = get_instance()->db->get_where('actualizador_precio_detalle',['actualizador_precio_id'=>$primary_key]);
            					foreach($productos->result() as $p):             					
            				?>

								<tr class="producto">
									<td><?= $p->codigo ?></td>
									<td data-val="nombre_comercial"><?= $p->nombre_comercial ?></td>
									<td><?= $p->cant_1 ?></td>
									<td><?= $p->cant_2 ?></td>
									<td><?= $p->cant_3 ?></td>
									<td><?= $p->precio_costo ?></td>
									<td><?= $p->precio_min_unidad ?></td>
									<td><?= $p->precio_min_cant ?></td>
									<td><?= $p->precio_min_caja ?></td>
									<td><?= $p->precio_may_unidad ?></td>
									<td><?= $p->precio_may_cant ?></td>
									<td><?= $p->precio_may_caja ?></td>
									<td><?= $p->precio_campo_unidad ?></td>
									<td><?= $p->precio_campo_cant ?></td>
									<td><?= $p->precio_campo_caja ?></td>
								</tr>
							<?php endforeach ?>
	                	</tbody>
	                </table>	                
	            </div>
	            <div class="row">
					<div class="col-12 col-md-2" style="text-align: center;padding: 6px;">
						Cant: <span id="cant">0</span>					
					</div>
					<div class="col-12 col-md-10" style="position: relative;">
						<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 13px;left: 16px;cursor:pointer;"></i>
						<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto (ALT + C)" style="padding-left: 25px;padding-right: 73px;" autocomplete="off">
						<button type="button" style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar">Insertar</button>
					</div>
				</div>

	        </div>

	        <div class="kt-portlet__foot">
			    <div class="kt-form__actions">
			    	<div class="report"></div>			        
			    	<a class="btn btn-info" href="<?= base_url('movimientos/actualizacionPrecios/actualizador_precio/') ?>">Atrás</a>
			    	<button type="button" onclick="anular()" class="btn btn-danger">Anular</button>
			    </div>
			</div>
		</div>

	</div>

</div>