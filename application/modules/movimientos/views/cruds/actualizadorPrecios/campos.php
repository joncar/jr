
<?php 
	get_instance()->hcss[] = '
		<style>
			#tabla_tus th, #tabla_tus td {
			  font-size: 14px;
			  padding: 0 5px;
			}

			#tabla_tus td > div {
			  /*white-space: nowrap;*/
			  width: 120px;
			  /*overflow: hidden;*/
			  /*text-overflow: ellipsis;*/
			  padding: 10px 0;
			  /*overflow-x: auto;*/
			  overflow-y: auto;
			}

			#tabla_tus thead th {
			  background: #fff;
			  position: sticky;
			  top: 0;
			  box-shadow: 0 2px 2px -1px rgba(0,0,0,.4);
			  z-index: 1;
			  -webkit-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -moz-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -ms-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -o-box-shadow: -2px 2px 0 -1px #0d5a26;
			  box-shadow: -2px 2px 0 -1px #0d5a26;
			}
			div.inp{
				position: relative;
			}
			div.banderas{
				background-image:url('.base_url().'img/banderas.jpg);
				width:20px;
				height:20px;	
				position: absolute;
				right:0px;
				top:0px;
				background-size: 100%;	
			}	
			div.banderas.us{
				background-position: 0 0px;
			}
			div.banderas.ar{
				background-position: 0 -20px;
			}		
			div.banderas.br{
				background-position: 0 -40px;
			}
			div.banderas.py{
				background-position: 0 -60px;
			}

			div.banderas-2x{
				background-image:url('.base_url().'img/banderas.jpg);
				width:40px;
				height:40px;	
				position: absolute;
				right:0px;
				top:0px;
				background-size: 100%;	
			}	
			div.banderas-2x.us{
				background-position: 0 0px;
			}
			div.banderas-2x.ar{
				background-position: 0 -40px;
			}		
			div.banderas-2x.br{
				background-position: 0 -80px;
			}
			div.banderas-2x.py{
				background-position: 0 -120px;
			}
			.chzn-results li {

			    color: #000;

			}
			.cabecera input{
				height: auto;padding: 3px;
			}
			tbody .form-control {
			  font-size: 14px;
			  height: 22px;
			}
		</style>
	'
?>
<div class="row">	
	<div class="col-12 col-md-12 cabecera">
		<div class="kt-portlet kt-portlet--solid-info">			    
	        <div class="kt-portlet__body">
	            <div class="kt-section kt-section--first">
	                <div class="row">                	                    
						<div class="col-12 col-md-3 row precios_wrap my-20 align-items-end p-0" style="margin:20px 0">
							<div class="col-12 col-md-6">
								<label for="field-precio_costo" id="cant_3_display_as_box">
									Precio Costo:
								</label>
							</div>
							<div class="precios col-12 col-md-6">
								<input id="field-precio_costo" name="precio_costo" class="form-control precio_costo" type="text" value="">
							</div>
						</div>

						<?php foreach($fields as $field): if($field->field_name=='efectivo') break; ?>
	                        <div class="col-12 col-md-3 col-lg-3">
	                            <div class="form-group m-0" id="<?php echo $field->field_name; ?>_field_box">
	                                <div>
	                                	<label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo str_replace(' id','',$input_fields[$field->field_name]->display_as); ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label>
	                                </div>
	                                <?php echo $input_fields[$field->field_name]->input ?>                            
	                            </div>
	                        </div>
	                    <?php endforeach ?>

	                </div>
	                


					<div class="row">
						<div class="col-12 col-md-3">
							<div class="row colum cuadro">
								<div class="col-12 text-center" style="margin-bottom: 20px">
									<b>Cantidades | %</b>
								</div>
								<div class="col-12 col-md-12">
									<div class="precios_wrap row" id="cant_1_field_box">
										<div class="col-12 col-md-6">
											<label for="field-anulado" id="cant_1_display_as_box">
												Unidad:
											</label>
										</div>
										<div class="precios col-12 col-md-6">
											<input id="field-cant_1" name="cant_1" class="form-control cant_1" type="text" value="">
										</div>
									</div>
									<div class="precios_wrap row" id="cant_2_field_box">
										<div class="col-12 col-md-6">
											<label for="field-anulado" id="cant_2_display_as_box">
												Cantidad:
											</label>
										</div>
										<div class="precios col-12 col-md-6">
											<input id="field-cant_2" name="cant_2" class="form-control cant_2" type="text" value="">
										</div>
									</div>
									<div class="precios_wrap row" id="cant_3_field_box">
										<div class="col-12 col-md-6">
											<label for="field-anulado" id="cant_3_display_as_box">
												Caja:
											</label>
										</div>
										<div class="precios col-12 col-md-6">
											<input id="field-cant_3" name="cant_3" class="form-control cant_3" type="text" value="">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="row colum cuadro">
								<div class="col-12 text-center" style="margin-bottom: 20px">
									<b>Minoristas</b>
								</div>
								<div class="col-12 col-md-12">
									<div class="precios_wrap row" id="precio_min_unidad_field_box">
										<div class="precios col-12 col-md-12">
											<input id="field-precio_min_unidad" name="precio_min_unidad" precio="" minorista="" unidad="" type="text" value="" class="precio_min_unidad numeric form-control" maxlength="11">
										</div>
										<!--<div class="col-6 porcentajes">
											<input id="field-porc_min_unidad" name="porc_min_unidad" porcentaje="" minorista="" unidad="" type="text" value="" class="porc_min_unidad numeric form-control" maxlength="11">
										</div>-->
									</div>
									<div class="precios_wrap row" id="precio_min_cant_field_box">
										<div class="precios col-12 col-md-12">
											<input id="field-precio_min_cant" name="precio_min_cant" precio="" minorista="" cantidad="" type="text" value="" class="precio_min_cant numeric form-control" maxlength="11">
										</div>
										<!--<div class="col-6 porcentajes">
											<input id="field-porc_min_cant" name="porc_min_cant" class="form-control porc_min_cant" type="text" value="">
										</div>-->
									</div>
									<div class="precios_wrap row" id="precio_min_caja_field_box">
										<div class="precios col-12 col-md-12">
											<input id="field-precio_min_caja" name="precio_min_caja" precio="" minorista="" caja="" type="text" value="" class="precio_min_caja numeric form-control" maxlength="11">
										</div>
										<!--<div class="col-6 porcentajes">
											<input id="field-porc_min_caja" name="porc_min_caja" class="form-control porc_min_caja" type="text" value="">
										</div>-->
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="row colum cuadro">
								<div class="col-12 text-center" style="margin-bottom: 20px">
									<b>Mayorista</b>
								</div>
								<div class="col-12 col-md-12">
									<div class="precios_wrap row" id="precio_may_unidad_field_box">
										<div class="precios col-12 col-md-12">
											<input id="field-precio_may_unidad" name="precio_may_unidad" precio="" mayorista="" unidad="" type="text" value="" class="precio_may_unidad numeric form-control" maxlength="11">
										</div>
										<!--<div class="col-6 porcentajes">
											<input id="field-porc_may_unidad" name="porc_may_unidad" class="form-control porc_may_unidad" type="text" value="">
										</div>-->
									</div>
									<div class="precios_wrap row" id="precio_may_cant_field_box">
										<div class="precios col-12 col-md-12">
											<input id="field-precio_may_cant" name="precio_may_cant" precio="" mayorista="" cantidad="" type="text" value="" class="precio_may_cant numeric form-control" maxlength="11">
										</div>
										<!--<div class="col-6 porcentajes">
											<input id="field-porc_may_cant" name="porc_may_cant" class="form-control porc_may_cant" type="text" value="">
										</div>-->
									</div>
									<div class="precios_wrap row" id="precio_may_caja_field_box">
										<div class="precios col-12 col-md-12">
											<input id="field-precio_may_caja" name="precio_may_caja" precio="" mayorista="" caja="" type="text" value="" class="precio_may_caja numeric form-control" maxlength="11">
										</div>
										<!--<div class="col-6 porcentajes">
											<input id="field-porc_may_caja" name="porc_may_caja" class="form-control porc_may_caja" type="text" value="">
										</div>-->
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<div class="row colum cuadro">
								<div class="col-12 text-center" style="margin-bottom: 20px">
									<b>Campo</b>
								</div>
								<div class="col-12 col-md-12">
										<div class="precios_wrap row" id="precio_campo_unidad_field_box">
											<div class="precios col-12 col-md-12">
												<input id="field-precio_campo_unidad" name="precio_campo_unidad" precio="" campo="" unidad="" type="text" value="" class="precio_campo_unidad numeric form-control" maxlength="11">
											</div>
											<!--<div class="col-6 porcentajes">
												<input id="field-porc_campo_unidad" name="porc_campo_unidad" class="form-control porc_campo_unidad" type="text" value="">
											</div>-->
										</div>
										<div class="precios_wrap row" id="precio_campo_cant_field_box">
											<div class="precios col-12 col-md-12">
												<input id="field-precio_campo_cant" name="precio_campo_cant" precio="" campo="" cantidad="" type="text" value="" class="precio_campo_cant numeric form-control" maxlength="11">
											</div>
											<!--<div class="col-6 porcentajes">
												<input id="field-porc_campo_cant" name="porc_campo_cant" class="form-control porc_campo_cant" type="text" value="">
											</div>-->
										</div>
										<div class="precios_wrap row" id="precio_campo_caja_field_box">
											<div class="precios col-12 col-md-12">
												<input id="field-precio_campo_caja" name="precio_campo_caja" precio="" campo="" caja="" type="text" value="" class="precio_campo_caja numeric form-control" maxlength="11">
											</div>
											<!--<div class="col-6 porcentajes">
												<input id="field-porc_campo_caja" name="porc_campo_caja" class="form-control porc_campo_caja" type="text" value="">
											</div>-->
										</div>
								</div>
							</div>
						</div>						
					</div>


				</div>
	        </div>
		</div>

	</div>

	<div class="col-12 col-md-12">
		<div class="kt-portlet">	
		    <div class="kt-portlet__head">
		        <div class="kt-portlet__head-label">
		            <h3 class="kt-portlet__head-title">
		                Productos
		            </h3>
		        </div>
		    </div>
	        <div class="kt-portlet__body p-0">
	            <div id="seguimiento" class="kt-section kt-section--first" style="overflow-x:auto;overflow-y:auto;height: 40vh;">
	                <table class="table table-bordered table-striped" id="tabla_tus">
	                	<thead>
	                		<tr>
	                			<th>Código</th>
	                			<th>Nombre</th>
	                			<th>Unidad</th>
	                			<th>Cantidad</th>
	                			<th>Caja</th>
	                			<th>P.Costo</th>
	                			<th>Unidad Min</th>
	                			<th>Cant Min</th>
	                			<th>Caja Min</th>
	                			<th>Unidad May</th>
	                			<th>Cant May</th>
	                			<th>Caja May</th>
	                			<th>Unidad Camp</th>
	                			<th>Cant Camp</th>
	                			<th>Caja Camp</th>
	                		</tr>
	                	</thead>
	                	<tbody>
	                		<tr class="empty">
	                			<td colspan="7">Añade productos a la lista</td>
	                		</tr>
	                		<tr class="producto" id="productoVacio">
								<td>
									<a href="javascript:void(0)" class="rem" style="color:red">
										<i class="fa fa-times"></i>
									</a> 
									<span data-val="codigo">&nbsp;</span>
								</td>
								<td data-val="nombre_comercial">&nbsp;</td>
								<td><input type="text" data-name="cant_1" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td><input type="text" data-name="cant_2" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td><input type="text" data-name="cant_3" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td><input type="text" data-name="precio_costo" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td><input type="text" data-name="precio_min_unidad" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td><input type="text" data-name="precio_min_cant" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td><input type="text" data-name="precio_min_caja" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td><input type="text" data-name="precio_may_unidad" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td><input type="text" data-name="precio_may_cant" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td><input type="text" data-name="precio_may_caja" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td><input type="text" data-name="precio_campo_unidad" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td><input type="text" data-name="precio_campo_cant" style="text-align: right;padding: 0 6px;" class="form-control"></td>
								<td>
									<input type="text" data-name="precio_campo_caja" style="text-align: right;padding: 0 6px;" class="form-control">
									<div class="d-none extraFields"></div>
								</td>
							</tr>

							<?php 
								foreach(get_instance()->db->get_where('ventatemp',['cajas_id'=>get_instance()->user->caja,'tipo'=>'actualizadorPrecios'])->result() as $v): 
								$productos = json_decode($v->productos);
								foreach($productos as $p):
									$extraclass = '';
									if($p->estado==1){
										$extraclass = 'disponible';
									}
							?>

								<tr class="producto <?= $extraclass ?>">
									<td>
										<a href="javascript:void(0)" class="rem" style="color:red">
											<i class="fa fa-times"></i>
										</a> 
										<span data-val="codigo"><?= $p->codigo ?></span>
									</td>
									<td data-val="nombre_comercial"><?= $p->nombre_comercial ?></td>
									<td><input type="text" data-name="cant_1" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->cant_1 ?>"></td>
									<td><input type="text" data-name="cant_2" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->cant_2 ?>"></td>
									<td><input type="text" data-name="cant_3" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->cant_3 ?>"></td>
									<td><input type="text" data-name="precio_costo" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_costo ?>"></td>
									<td><input type="text" data-name="precio_min_unidad" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_min_unidad ?>"></td>
									<td><input type="text" data-name="precio_min_cant" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_min_cant ?>"></td>
									<td><input type="text" data-name="precio_min_caja" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_min_caja ?>"></td>
									<td><input type="text" data-name="precio_may_unidad" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_may_unidad ?>"></td>
									<td><input type="text" data-name="precio_may_cant" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_may_cant ?>"></td>
									<td><input type="text" data-name="precio_may_caja" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_may_caja ?>"></td>
									<td><input type="text" data-name="precio_campo_unidad" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_campo_unidad ?>"></td>
									<td><input type="text" data-name="precio_campo_cant" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_campo_cant ?>"></td>

									<td>
										<input type="text" data-name="precio_campo_caja" style="text-align: right;padding: 0 6px;" class="form-control"  value="<?= $p->precio_campo_caja ?>">
										<div class="extraFields d-none">
											<?php 												
												$hiddens = [
													'cant_1',
													'cant_2',
													'cant_3',
													'precio_costo',
													'precio_min_unidad',
													'precio_min_cant',
													'precio_min_caja',
													'precio_may_unidad',
													'precio_may_cant',
													'precio_may_caja',
													'precio_campo_unidad',
													'precio_campo_cant',
													'precio_campo_caja'
												];
											?>
											<?php foreach($p as $nn=>$pp): if(!in_array($nn,$hiddens)): ?>
												<input type="hidden" data-name="<?= $nn ?>" value="<?= $pp ?>">
											<?php endif; endforeach ?>
										</div>
									</td>
								</tr>

								<?php endforeach ?>
							<?php endforeach ?>
	                	</tbody>
	                </table>	                
	            </div>
	            <div class="row">
					<div class="col-12 col-md-2" style="text-align: center;padding: 6px;">
						Cant: <span id="cant">0</span>					
					</div>
					<div class="col-12 col-md-10" style="position: relative;">
						<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 13px;left: 16px;cursor:pointer;"></i>
						<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto (ALT + C)" style="padding-left: 25px;padding-right: 73px;" autocomplete="off">
						<button type="button" style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar">Insertar</button>
					</div>
				</div>

	        </div>

	        <div class="kt-portlet__foot">
			    <div class="kt-form__actions">
			    	<div class="report"></div>
			        <a class="btn btn-info" href="<?= base_url('movimientos/actualizacionPrecios/actualizador_precio/') ?>">Atrás</a>
			        <button type="button" class="btnNueva btn btn-info m-10" data-toggle="modal" data-target="#inventarioModal">Inventario <span class="alt" style="display:none">(I)</span></button>
			    	<button type='submit' class="btn btn-success"><?= $accion ?></button>	

			    </div>
			</div>
		</div>

	</div>

</div>