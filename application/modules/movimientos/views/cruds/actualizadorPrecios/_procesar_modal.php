<?php get_instance()->hcss[] = '
	<style>
		a:focus,div:focus,.chzn-container-active{
			border:1px dashed blue;
		}
		body {
	        color: #000;
	        font-weight: 600;
	    }
	    .form-control{
	    	color: #000;
		    font-weight:600;
		    opacity: 1;
		    font-size:15px;
	    }
		.panel{
			border-radius:0px;
		}

		.error{
			border: 1px solid red !important;
		}

		.patternCredito{
			background: #ffffffa6;
			width: 100%;
			height: 100%;
			position: absolute;
			top: 0px;
			left: 0px;
			z-index:10;
		}

		.btnsaldo{
			position: absolute;top: 0;right: 15px;
		}
		@media screen and (max-width:768px){
			.btnsaldo{
				top: -15px;
				font-size:20px;
			}	
		}
		.kt-portlet{
			margin:0;
		}

		.table thead th, .table td{
			padding:6px;
			color: #000;
			font-weight: 600;
		}

		tbody .form-control{
			height:22px;
		}

		.kt-portlet .kt-portlet__head{
			min-height: inherit;
			padding:10px;
		}

		.totales input, .totales .form-control:focus{
			background:lightgreen !important;
			padding: 0 12px;
			border-radius: 0;
		}

		.pagoEfectivo input, .pagoEfectivo .form-control:focus{
			background-color: lightblue;
			border-radius: 0;
			padding: 0 12px;
		}

		.vueltos input{
			background: #e8e800 !important;
			color: red;
			border-radius: 0;
			padding: 0 12px;
		}

		input.guaranies{
			font-weight: bold;
			font-size: 19px;
		}
		div.inp{
			position: relative;
		}
		div.banderas{
			background-image:url('.base_url().'img/banderas.jpg);
			width:20px;
			height:20px;	
			position: absolute;
			right:0px;
			top:0px;
			background-size: 100%;	
		}	
		div.banderas.us{
			background-position: 0 0px;
		}
		div.banderas.ar{
			background-position: 0 -20px;
		}		
		div.banderas.br{
			background-position: 0 -40px;
		}
		div.banderas.py{
			background-position: 0 -60px;
		}

		div.banderas-2x{
			background-image:url('.base_url().'img/banderas.jpg);
			width:40px;
			height:40px;	
			position: absolute;
			right:0px;
			top:0px;
			background-size: 100%;	
		}	
		div.banderas-2x.us{
			background-position: 0 0px;
		}
		div.banderas-2x.ar{
			background-position: 0 -40px;
		}		
		div.banderas-2x.br{
			background-position: 0 -80px;
		}
		div.banderas-2x.py{
			background-position: 0 -120px;
		}
		#procesar th{
			font-weight: bold;
			font-size: 16px;
		}
		.btn-lg, .btn-group-lg > .btn {
		    padding: 0.7rem 1.45rem;
		}
		.caja {
    color: #3c4cca;
    font-family: system-ui;
    font-weight: bold;
    margin-left: 250px;
		}

		@media screen and (min-width:769px){
			/*.cuerpoTransaccion{
				position:relative;
				overflow-x:hidden;
				overflow-y: hidden;
			}

			.cuerpoTransaccion .tarifario{
				position: absolute;
				right: 0;
				top: 0;
				background: #fff;
				border: 1px solid #1e1e2d;
				right: -241px;
				max-width:241px;
				-webkit-transition: 0.5s ease-in-out;
				-moz-transition: 0.5s ease-in-out;
				-o-transition: 0.5s ease-in-out;
				-ms-transition: 0.5s ease-in-out;
				transition: 0.5s ease-in-out;
			}

			.tarifario:hover {
			    right: 0;
			}

			.tarifario .kt-portlet{
				position:relative;
			}

			.tarifarioToggle{
				position: absolute;
				left: -32px;
				background: #fff;
				width: 27px;
				text-align: center;
				border: 1px solid #000;
				top: 0;
			}*/
		}
	</style>
'; 
?>
<div id="procesar" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title">
        	Procesar venta
        </h4>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="row">
      	<div class="caja">
       		<h5>
 				<b><?= $this->user->sucursalnombre?> - <?= $this->user->cajanombre?></b>
 			</h5>
 		</div>	 
 	  </div> 
      <div class="modal-body">
      	<div class="row" style="margin-left: 0; margin-right: 0">
      		<div class="col-12">
      			<div class="radio facturaLegalDiv">      			 
				  <span id="facturaD" style="font-weight:bold; position: absolute; right:0; top:0; z-index:1">
				  	<a href="javascript:;" onclick="window.venta.reloadNroFactura()"> 
					  <i class="fa fa-sync"></i>
				    </a>
				  	#FACTURA: <span id="nroFactura"></span>
				  </span>
				  
				</div>
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-12 col-md-4 totales">
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Total Importe</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Guaranies</td>
      						<td>
      							<div class="inp">
	      							<input type="text" class="guaranies form-control" name="total_venta" value="0" readonly="" style="width: 100%;">
	      							<div class="banderas py"></div>
      							</div>
      						</td>
      					</tr>
      					<tr>
      						<td>Pesos</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="total_pesos" value="0" readonly="" style="width: 100%;">
      								<div class="banderas ar"></div>
      							</td>
      					</tr>
      					<tr>
      						<td>Dolares</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="total_dolares" value="0" readonly="" style="width: 100%;">
      								<div class="banderas us"></div>
      							</td>
      					</tr>
      					<tr>
      						<td>Reales</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="total_reales" value="0" readonly="" style="width: 100%;">
      								<div class="banderas br"></div>
      							</td>
      					</tr>
      					
      				</tbody>
      			</table>   
      			<div id="pago_total" style="padding: 0px 30px;margin: 0 0 31px;">Gs.: <span class="totalGs" style="font-size: 30px;font-weight: bold;">0</span></div>   			
      		</div>
      		<div class="col-12 col-md-4 pagoEfectivo">
      			<div class="patternCredito"></div>
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Pago</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Guaranies</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="guaranies form-control" name="pago_guaranies" value="0" style="width: 100%;">
      								<div class="banderas py"></div>
      							</div>
      						</td>
      					</tr>
      					<tr>
      						<td>Pesos</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="pago_pesos" value="0" style="width: 100%;">
      								<div class="banderas ar"></div>
      							</div>
      						</td>
      					</tr>
      					<tr>
      						<td>Dolares</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="pago_dolares" value="0" style="width: 100%;">
      								<div class="banderas us"></div>
      							</div>
      						</td>
      					</tr>
      					<tr>
      						<td>Reales</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="pago_reales" value="0" style="width: 100%;">
      								<div class="banderas br"></div>
      							</div>
      						</td>
      					</tr>
      					
      					<tr>
      						<td>Debito/Crédito</td>
      						<td><input type="text" class="form-control" name="total_debito" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr id="chequeInp" style="display: none">
      						<td>Total Cheque</td>
      						<td><input type="text" class="form-control" name="total_cheque" value="0" style="width: 100%;" readonly="" onclick="venta.showChequeModal()"></td>
      					</tr>
      				</tbody>
      			</table>
      		</div>
      		<div class="col-12 col-md-4 vueltos">
      			<div class="patternCredito"></div>
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Vuelto</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Guaranies</td>
      						<td>
      							<div class="inp">
      								<input class="guaranies form-control" style="color:red; width:100%;" type="text" name="vuelto" readonly="true" value="0">
      								<div class="banderas py"></div>
      							</div>
      						</td>
      					</tr>
      					<tr>
      						<td>Pesos</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="vuelto_pesos" readonly="true" value="0" style="width: 100%;">
      								<div class="banderas ar"></div>
      							</div>
      						</td>
      					</tr>
      					<tr>
      						<td>Dolares</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="vuelto_dolares" readonly="true" value="0" style="width: 100%;">
      								<div class="banderas us"></div>
      							</div>
      						</td>
      					</tr>
      					<tr>
      						<td>Reales</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="vuelto_reales" readonly="true" value="0" style="width: 100%;">
      								<div class="banderas br"></div>
      							</div>
      						</td>
      					</tr>
      					
      				</tbody>
      			</table>
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-12">
      			<div class="report"></div>
      		</div>
      	</div>		
      </div>
      <div class="modal-footer">
      	<button type="button" class="btnNueva btn btn-info" onclick="venta.nuevaVenta()">Nueva venta <span class="alt" style="display:none">(N)</span></button>
        <button type="submit" class="btn btn-success">Guardar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->