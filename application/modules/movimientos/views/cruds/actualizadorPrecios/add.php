<?php	
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-add.js?v=1.1"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';
    get_instance()->hcss[] = '<style>
    .alert{flex-wrap:wrap;} .alert p{width:100%;}
    tr.producto:not(.disponible) td {
        background: #e9b3b3 !important;
    }</style>';
?>

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $insert_url, 'method="post" onsubmit="venta.save(this); return false;" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
    

    <?php get_instance()->load->view('cruds/actualizadorPrecios/campos',['accion'=>'Añadir','subject'=>$subject,'fields'=>$fields,'input_fields'=>$input_fields]); ?>


    <?php
        foreach($hidden_fields as $hidden_field){
            echo $hidden_field->input;
        }
    ?>
    <!-- End of hidden inputs -->
    <?php if ($is_ajax) { ?>
        <input type="hidden" name="is_ajax" value="true" />
    <?php }?>    
    <?php echo form_close(); ?>
</div>

<?php get_instance()->js[] = '<script src="'.base_url('js/actualizadorPrecios.js').'?v='.uniqid().'"></script>'; ?>
<?php
    $ajustes = $this->ajustes;
    $ajustes->solicitar_token_ventas = $this->db->get_where('cajas',['id'=>$this->user->caja])->row()->solicitar_token;
?>

<?php $this->load->view('cruds/actualizadorPrecios/_inventario_from_productos_modal',array(),FALSE,'movimientos'); ?>


<script>
    var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;    
    function selCod(codigo){
        venta.addProduct(codigo);        
        $("#inventarioModal").modal('hide');
    }

    function selCodMasivo(){        
        $("#inventarioModal td:first-child input:checked").each(function(){
          venta.addProduct(($(this).parents('tr').find('td:nth-child(2)').text().trim()));
        });
        $("#inventarioModal").modal('hide');
    }

    window.shouldSave = false;  
    window.addEventListener('beforeunload',function(e){
        if (window.shouldSave) {
            e.preventDefault();
            e.returnValue = '';
            return;
        }
        delete e['returnValue'];
    });
</script>