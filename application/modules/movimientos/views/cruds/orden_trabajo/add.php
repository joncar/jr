<?php	
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-add.js?v=1.1"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';
    get_instance()->hcss[] = '<style>.alert{flex-wrap:wrap;} .alert p{width:100%;}</style>';
?>

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $insert_url, 'method="post" onsubmit="venta.save(this); return false;" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
    

    <?php get_instance()->load->view('cruds/orden_trabajo/campos',['accion'=>'Añadir','subject'=>$subject,'fields'=>$fields,'input_fields'=>$input_fields]); ?>


    <?php
        foreach($hidden_fields as $hidden_field){
            echo $hidden_field->input;
        }
    ?>
    <!-- End of hidden inputs -->
    <?php if ($is_ajax) { ?>
        <input type="hidden" name="is_ajax" value="true" />
    <?php }?>    
    <?php echo form_close(); ?>
</div>

<?php get_instance()->js[] = '<script src="'.base_url('js/orden_trabajo.js').'?v='.uniqid().'"></script>'; ?>
<?php
    $ajustes = $this->ajustes;
    $ajustes->solicitar_token_ventas = $this->db->get_where('cajas',['id'=>$this->user->caja])->row()->solicitar_token;
    $ajustes->codigo_balanza = explode(', ',$ajustes->cod_balanza);
?>

<?php $this->load->view('_inventario_from_productos_modal.php',array(),FALSE,'movimientos'); ?>
<?php $this->load->view('_add_cliente_modal',array(),FALSE,'movimientos'); ?>


<script>
    var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;
    var tiposFacturacion = <?php echo json_encode($this->db->get('tipo_facturacion')->result()) ?>;
    var clienteDefault = <?php echo json_encode($this->db->get_where('clientes',['id'=>1])->result()[0]); ?>;
    var formasPago = <?php echo json_encode($this->db->get_where('formapago')->result()); ?>;
    function selCod(codigo){
        cantidad = $("#codigoAdd").val();
        if(cantidad!=''){
            codigo = cantidad+codigo;
        }
        venta.addProduct(codigo);        
        $("#inventarioModal").modal('hide');
    }
    window.afterLoad.push(function(){
        $("#field-tipo_venta").val(1).chosen().trigger('liszt:updated');
        $("#field-transaccion").val(1).chosen().trigger('liszt:updated');
        $("#field-tipo_facturacion_id").val(1).chosen().trigger('liszt:updated');
        $("#field-forma_pago").val(1).chosen().trigger('liszt:updated');
        $("#field-cliente").val(1).chosen().trigger('liszt:updated');
        $("#field-user_id").val(<?= $this->user->id ?>).chosen().trigger('liszt:updated');

        var c = clienteDefault;
        $("#field-cliente").html('<option value="'+c.id+'">'+c.nro_documento+' '+c.nombres+' '+c.apellidos+'</option>').val(c.id).chosen().trigger('liszt:updated');
        $("#field-direccion").val(c.direccion);
        $("#field-fecha").val('<?= date("d/m/Y H:i:s") ?>');
    });

    window.shouldSave = false;  
    window.addEventListener('beforeunload',function(e){
        if (window.shouldSave) {
            e.preventDefault();
            e.returnValue = '';
            return;
        }
        delete e['returnValue'];
    });
</script>