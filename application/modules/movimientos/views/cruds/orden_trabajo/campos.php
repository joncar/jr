<?php 
	get_instance()->hcss[] = '
		<style>
			#tabla_tus th, #tabla_tus td {
			  font-size: 14px;
			  padding: 0 5px;
			}

			#tabla_tus td > div {
			  /*white-space: nowrap;*/
			  width: 120px;
			  /*overflow: hidden;*/
			  /*text-overflow: ellipsis;*/
			  padding: 10px 0;
			  /*overflow-x: auto;*/
			  overflow-y: auto;
			}

			#tabla_tus thead th {
			  background: #fff;
			  position: sticky;
			  top: 0;
			  box-shadow: 0 2px 2px -1px rgba(0,0,0,.4);
			  z-index: 1;
			  -webkit-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -moz-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -ms-box-shadow: -2px 2px 0 -1px #0d5a26;
			  -o-box-shadow: -2px 2px 0 -1px #0d5a26;
			  box-shadow: -2px 2px 0 -1px #0d5a26;
			}
			div.inp{
				position: relative;
			}
			div.banderas{
				background-image:url('.base_url().'img/banderas.jpg);
				width:20px;
				height:20px;	
				position: absolute;
				right:0px;
				top:0px;
				background-size: 100%;	
			}	
			div.banderas.us{
				background-position: 0 0px;
			}
			div.banderas.ar{
				background-position: 0 -20px;
			}		
			div.banderas.br{
				background-position: 0 -40px;
			}
			div.banderas.py{
				background-position: 0 -60px;
			}

			div.banderas-2x{
				background-image:url('.base_url().'img/banderas.jpg);
				width:40px;
				height:40px;	
				position: absolute;
				right:0px;
				top:0px;
				background-size: 100%;	
			}	
			div.banderas-2x.us{
				background-position: 0 0px;
			}
			div.banderas-2x.ar{
				background-position: 0 -40px;
			}		
			div.banderas-2x.br{
				background-position: 0 -80px;
			}
			div.banderas-2x.py{
				background-position: 0 -120px;
			}
			.chzn-results li {

			    color: #000;

			}
			.cabecera input{
				height: auto;padding: 3px;
			}
			tbody .form-control {
			  font-size: 14px;
			  height: 22px;
			}
		</style>
	'
?>
<div class="row">
	<div class="col-12 col-md-12">
		<b>Cotizaciones: </b>
		<div class="inp" style="display:inline-block;margin-left: 13px;">
			<span><?= number_format(get_instance()->ajustes->tasa_dolares,0,',','.') ?> Gs.</span>
			<div class="banderas-2x us" style="width:10px; height:10px; left:-13px; top:4px"></div>
		</div>
		<div class="inp" style="display:inline-block;margin-left: 13px;">
			<span><?= number_format(get_instance()->ajustes->tasa_reales,0,',','.') ?> Gs.</span>
			<div class="banderas-2x br" style="background-position: 0 -60px;width:10px; height:10px; left:-13px; top:4px"></div>
		</div>
		<div class="inp" style="display:inline-block;margin-left: 13px;">
			<span><?= number_format(get_instance()->ajustes->tasa_pesos,0,',','.') ?> Gs.</span>
			<div class="banderas-2x ar" style="background-position: 0 -50px; width:10px; height:10px; left:-13px; top:4px"></div>
		</div>
		| 
		<b>Totales: </b>
		<div class="inp" style="display:inline-block;margin-left: 13px;">
			<span id="total_dolares">0 Gs.</span>
			<div class="banderas-2x us" style="width:10px; height:10px; left:-13px; top:4px"></div>
			<input type="hidden" name="total_dolares" value="0">
		</div>
		<div class="inp" style="display:inline-block;margin-left: 13px;">
			<span id="total_reales">0 Gs.</span>
			<div class="banderas-2x br" style="background-position: 0 -60px;width:10px; height:10px; left:-13px; top:4px"></div>
			<input type="hidden" name="total_reales" value="0">
		</div>
		<div class="inp" style="display:inline-block;margin-left: 13px;">
			<span id="total_pesos">0 Gs.</span>
			<div class="banderas-2x ar" style="background-position: 0 -50px; width:10px; height:10px; left:-13px; top:4px"></div>
			<input type="hidden" name="total_pesos" value="0">
		</div>
	</div>
	<div class="col-12 col-md-9 cabecera">
		<div class="kt-portlet kt-portlet--solid-info">			    
	        <div class="kt-portlet__body py-0">
	            <div class="kt-section kt-section--first">
	                <div class="row">                
	                    <?php foreach($fields as $field): if($field->field_name!='observaciones'): ?>
	                        <div class="col-12 col-md-3 col-lg-3">
	                            <div class="form-group m-0" id="<?php echo $field->field_name; ?>_field_box">
	                                <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo str_replace(' id','',$input_fields[$field->field_name]->display_as); ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>
	                                <?php echo $input_fields[$field->field_name]->input ?>                            
	                            </div>
	                        </div>
	                    <?php endif; endforeach ?>
	                    <div class="col-12 col-md-9 col-lg-9">
                            <div class="form-group m-0" id="<?php echo $field->field_name; ?>_field_box">
                                <div><label for='field-observaciones' id="observaciones_display_as_box"><?php echo str_replace(' id','',$input_fields['observaciones']->display_as); ?><?php echo ($input_fields['observaciones']->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>
                                <?php echo $input_fields['observaciones']->input ?>
                                
                            </div>
                        </div>
	                    
	                </div>
	                <!-- Start of hidden inputs -->
	                
	            </div>
	        </div>
		</div>

	</div>

	<div class="col-12 col-md-3">
		<div class="kt-portlet kt-portlet--solid-success">	
		    <div class="kt-portlet__head">
		        <div class="kt-portlet__head-label">
		            <h3 class="kt-portlet__head-title">
		                Total Gs
		            </h3>
		        </div>
		    </div>
	        <div class="kt-portlet__body">
	            <div class="kt-section kt-section--first text-right">
	                <h1>Gs <span id="span_total_venta">0.00</span></h1>
	                <input type="hidden" name="total_orden" value="0">
	            </div>
	        </div>
		</div>

	</div>

	<div class="col-12 col-md-12">
		<div class="kt-portlet">	
		    <div class="kt-portlet__head">
		        <div class="kt-portlet__head-label">
		            <h3 class="kt-portlet__head-title">
		                Productos
		            </h3>
		        </div>
		    </div>
	        <div class="kt-portlet__body p-0">
	            <div id="seguimiento" class="kt-section kt-section--first" style="overflow-x:auto;overflow-y:auto;height: 40vh;">
	                <table class="table table-bordered table-striped" id="tabla_tus">
	                	<thead>
	                		<tr>
	                			<th>Código</th>
	                			<th>Nombre</th>
	                			<th>Cantidad</th>
	                			<th>Precio</th>
	                			<th>Total</th>
	                		</tr>
	                	</thead>
	                	<tbody>
	                		<tr class="empty">
	                			<td colspan="7">Añade productos a la lista</td>
	                		</tr>
	                		<tr class="producto" id="productoVacio">
								<td style="width:20%">
									<a href="javascript:void(0)" class="rem" style="color:red">
										<i class="fa fa-times"></i>
									</a> 
									<span data-val="codigo">&nbsp;</span>
								</td>
								<td data-val="nombre_comercial" style="width:30%">&nbsp;</td>
								<td>
									<input data-name="cantidad" class="form-control cantidad" type="text" style="text-align: right;padding: 0 6px;" value="0">
								</td>
								<td>
									<input data-name="precio_venta" class="form-control precio_venta" type="text" style="text-align: right;padding: 0 6px;" value="0">
								</td>
								
								<td>
									<span data-val="total">&nbsp;</span>
									<div class="extraFields d-none"></div>
								</td>
							</tr>


							<!---------------- Update Temp ---------------->

							<?php 
								if(empty($primary_key)):
								foreach(get_instance()->db->get_where('ventatemp',['cajas_id'=>get_instance()->user->caja,'tipo'=>'orden_trabajo'])->result() as $v): 
								$productos = json_decode($v->productos);
								foreach($productos as $p):
							?>

								<tr class="producto">
									<td style="width:20%">
										<a href="javascript:void(0)" class="rem" style="color:red">
											<i class="fa fa-times"></i>
										</a> 
										<span data-val="codigo"><?= $p->codigo ?></span>
									</td>
									<td data-val="nombre_comercial" style="width:30%"><?= $p->nombre_comercial ?></td>
									<td>
										<input data-name="cantidad" class="form-control cantidad" type="text" style="text-align: right;padding: 0 6px;" value="<?= $p->cantidad ?>">
									</td>
									<td>
										<input data-name="precio_venta" class="form-control precio_venta" type="text" style="text-align: right;padding: 0 6px;" value="<?= $p->precio_venta ?>">
									</td>

									<td>
										<span data-val="total"><?= @$p->total ?></span>
										<div class="extraFields d-none">
											<?php 
												$hiddens = [
													'cantidad',
													'precio_venta',
													'por_desc',
													'precio_descuento'
												];
											?>
											<?php foreach($p as $nn=>$pp): if(!in_array($nn,$hiddens)): ?>
												<input type="hidden" data-name="<?= $nn ?>" value="<?= $pp ?>">
											<?php endif; endforeach ?>
										</div>
									</td>
								</tr>

								<?php endforeach ?>
							<?php endforeach ?>
							<?php endif ?>

							<!-------------- Edit --------------->
							<?php 
								if(!empty($primary_key)):
								get_instance()->db->select('orden_trabajo_detalle.*,productos.codigo_interno,productos.nombre_comercial');
								get_instance()->db->join('productos','productos.id = orden_trabajo_detalle.productos_id');
								foreach(get_instance()->db->get_where('orden_trabajo_detalle',['orden_trabajo_id'=>$primary_key])->result() as $p):
									$p->codigo = $p->producto;
							?>

								<tr class="producto">
									<td style="width:20%">
										<a href="javascript:void(0)" class="rem" style="color:red">
											<i class="fa fa-times"></i>
										</a> 
										<span data-val="codigo"><?= $p->producto ?></span>
									</td>
									<td data-val="nombre_comercial" style="width:30%"><?= $p->nombre_comercial ?></td>
									<td>
										<input data-name="cantidad" class="form-control cantidad" type="text" style="text-align: right;padding: 0 6px;" value="<?= $p->cantidad ?>">
									</td>
									<td>
										<input data-name="precio_venta" class="form-control precio_venta" type="text" style="text-align: right;padding: 0 6px;" value="<?= $p->precio_venta ?>">
									</td>
									<td>
										<span data-val="total"><?= $p->total ?></span>
										<div class="extraFields d-none">
											<?php 
												$hiddens = [
													'cantidad',
													'precio_venta',
													'por_desc',
													'precio_descuento',
												];
											?>
											<?php foreach($p as $nn=>$pp): if(!in_array($nn,$hiddens)): ?>
												<input type="hidden" data-name="<?= $nn ?>" value="<?= $pp ?>">
											<?php endif; endforeach ?>
										</div>
									</td>
								</tr>
							<?php endforeach ?>
							<?php endif ?>

	                	</tbody>
	                </table>	                
	            </div>
	            <div class="row">
					<div class="col-12 col-md-2" style="text-align: center;padding: 6px;">
						Cant: <span id="cant">0</span>					
					</div>
					<div class="col-12 col-md-10" style="position: relative;">
						<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 13px;left: 16px;cursor:pointer;"></i>
						<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto (ALT + C)" style="padding-left: 25px;padding-right: 73px;" autocomplete="off">
						<button type="button" style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar">Insertar</button>
					</div>
				</div>

	        </div>

	        <div class="kt-portlet__foot">
			    <div class="kt-form__actions">
			    	<div class="report"></div>
			        
			    	<?php if($accion=='Añadir'): ?>			        	
			        	<button type="button" class="btnNueva btn btn-info m-10" onclick="venta.nuevaVenta()">Nueva orden <span class="alt" style="display:none">(N)</span></button>
			        	<button type="button" class="btnNueva btn btn-info m-10" onclick="venta.AddCliente()">Nuevo Cliente <span class="alt" style="display:none">(X)</span></button>
			        	<button type="button" class="btnNueva btn btn-info m-10" data-toggle="modal" data-target="#inventarioModal">Inventario <span class="alt" style="display:none">(I)</span></button>
			        	<button type='submit' class="btn btn-success">Guardar Orden</button>
			        <?php else: ?>
			        	<button type='submit' class="btn btn-success">Guardar Orden</button>
			        <?php endif ?>

			    </div>
			</div>
		</div>

	</div>

</div>

<?php 
    get_instance()->db->select('tipo_trabajo_sub.*');
    $sucs = get_instance()->db->get_where('tipo_trabajo_sub')->result();
    $rules = [];
    foreach($sucs as $s){
        if(!isset($rules[$s->tipo_trabajo_id])){
            $rules[$s->tipo_trabajo_id] = [];
        }
        $rules[$s->tipo_trabajo_id][] = [$s->id,$s->denominacion];
    }
?>

<script>
<?php 
    echo 'var rules = '.json_encode($rules).';';
?>
</script>

<script>
	window.afterLoad.push(function(){
		$("#inventarioModal .flexigrid table thead tr:nth-child(2) th:nth-child(2)").attr('colspan',3);
		$("#inventarioModal .flexigrid table thead tr:nth-child(2) th:nth-child(3),#inventarioModal .flexigrid table thead tr:nth-child(2) th:nth-child(4)").remove();		

		$("#field-tipo_trabajo_id").on('change',function(){
			filtrar_origenes($(this).val());
		});
	});

	function filtrar_origenes(origen){
        
        let o = [];
        let options = '<option value="" selected="selected">Seleccione una opcion</option>';
        if(rules[origen]){
            for(var i in rules[origen]){
                options+= '<option value="'+rules[origen][i][0]+'" selected="selected">'+rules[origen][i][1]+'</option>';
            }
        }
        $("#field-tipo_trabajo_sub_id").html(options).chosen().trigger('liszt:updated');
    }
</script>	