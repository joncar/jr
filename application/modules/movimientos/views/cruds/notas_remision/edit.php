<?php	
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-edit.js?v=1.1"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';
    get_instance()->hcss[] = '<style>.alert{flex-wrap:wrap;} .alert p{width:100%;}</style>';
    $this->db->select('nota_remision.*,clientes.nombres, clientes.apellidos');
    $this->db->join('ventas','ventas.id = nota_remision.venta');
    $this->db->join('clientes','clientes.id = ventas.cliente');
    $remision = $this->db->get_where('nota_remision',['nota_remision.id'=>$primary_key]);
    if($remision->num_rows()>0):
        $remision = $remision->row();
?>

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $update_url, 'method="post" id="crudForm" onsubmit="insertar(\''.$update_url.'\',this,\'.report\'); return false;" autocomplete="off" enctype="multipart/form-data"'); ?>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Editar <?php echo $subject?>
                </h3>
            </div>
        </div>

        <!--begin::Form-->        
        <div class="kt-portlet__body">
            

            

            <div class="kt-section kt-section--first">
            	<div class="row">
            		<?php foreach($fields as $field): ?>
        				<div class="col-12 col-md-3">
		                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
		                        <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>
		                        <?php echo $input_fields[$field->field_name]->input ?>
		                    </div>
	                    </div>
	                <?php endforeach ?>	
                    <div class="col-12 col-md-3">
                        <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
                            <div><label for='field-'>Cliente:</label></div>
                            <input type="text" id="cliente" class="form-control" readonly="" value="<?= $remision->nombres.' '.$remision->apellidos ?>">
                        </div>
                    </div>                  
                    <div class="col-12 col-md-3">
                        <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
                            <div><label for='field-'>Nro Remisión:</label></div>
                            <input type="text" id="remision" class="form-control" readonly="" value="<?= $remision->nro_nota ?>">
                        </div>
                    </div>    	                
                </div>
            </div>          
        </div>    

            <div class = "row">
        <div class="col-12 col-md-6">
        <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h1 class="kt-portlet__head-title">
                            Datos Conductor y Vehiculos Transporte
                        </h1>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div>
                        <div class="row">
                            <div class="col-2 col-md-6">
                                <div class="form-group" id="nombre_transportista_field_box">
                                    <div><label for='field-'>Transportista:</label></div>
                                    <input type="text" id="nombre_transportista" class="form-control" name="nombre_transportista" value="<?= $remision->nombre_transportista ?>">
                                </div>
                            </div>
                            <div class="col-3 col-md-6">
                                <div class='form-group' id="ruc_transportista_field_box">
                                    <label for='field-ruc_transportista' id="ruc_transportista_display_as_box" style="width:100%">
                                    <div><label for='field-'>RUC-CI:</label></div>
                                    <input type="text" id="ruc_transportista" class="form-control" name="ruc_transportista" value="<?= $remision->ruc_transportista ?>">
                                </div>
                            </div>
                            <div class="col-3 col-md-6">
                                <div class='form-group' id="nombre_conductor_field_box">
                                    <div><label for='field-'>Conductor:</label></div>
                                    <input type="text" id="nombre_conductor" class="form-control" name="nombre_conductor" value="<?= $remision->nombre_conductor ?>">
                                </div>
                            </div>
                            <div class="col-3 col-md-6">
                                <div class='form-group' id="documento_conductor_field_box">
                                    <div><label for='field-'>RUC-CI:</label></div>
                                    <input type="text" id="documento_conductor" class="form-control" name="documento_conductor" value="<?= $remision->documento_conductor ?>">
                                </div>
                            </div>
                            <div class="col-3 col-md-6">
                                <div class='form-group' id="direccion_conductorfield_box">
                                    <div><label for='field-'>Dirección de Conductor:</label></div>
                                    <input type="text" id="direccion_conductor" class="form-control" name="direccion_conductor" value="<?= $remision->direccion_conductor ?>">
                                </div>
                            </div>                          
                            <div class="col-3 col-md-6">
                                <div class='form-group' id="fecha_trasladofield_box">
                                    <div><label for='field-'>Fecha de Traslado:</label></div>
                                    <input id='fecha_traslado' name="fecha_traslado" class="form-control" placeholder='dd/mm/yyyy' type="date" value="<?= $remision->fecha_traslado ?>">
                                </div>
                            </div>
                            <div class="col-3 col-md-6">
                                <div class='form-group' id="marca_vehiculofield_box">
                                    <div><label for='field-'>Marca de Vehiculo:</label></div>
                                    <input type="text" id="marca_vehiculo" name="marca_vehiculo" class="form-control" value="<?= $remision->marca_vehiculo ?>">
                                </div>
                            </div>  
                            <div class="col-3 col-md-6">
                                <div class='form-group' id="nro_chapafield_box">
                                    <div><label for='field-'>Nro de Chapa:</label></div>
                                    <input type="text" id="nro_chapa" name="nro_chapa" class="form-control" value="<?= $remision->nro_chapa ?>">
                                </div>
                            </div>                                                                              
                        </div>
                    </div>
                </div>
        </div><!--- EnD conductor --->
    </div>
        <div class="col-12 col-md-6">
        <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h1 class="kt-portlet__head-title">
                            Datos de Traslado
                        </h1>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div>
                        <div class="row">
                            <div class="col-3 col-md-6">
                                <div class="form-group" id="motivo_traslado_field_box">
                                    <div><label for='field-'>Motivo de Traslado</label></div>
                                    <input type="text" id="motivo_traslado" name="motivo_traslado" class="form-control" value="<?= $remision->motivo_traslado ?>">
                                </div>
                            </div>
                            <div class="col-3 col-md-6">
                                <div class='form-group' id="punto_partida_field_box">
                                    <div><label for='field-'>Punto de Partida</label></div>
                                    <input type="text" id="punto_partida" name="punto_partida" class="form-control" value="<?= $remision->punto_partida ?>">
                                </div>
                            </div>
                            <div class="col-3 col-md-6">
                                <div class='form-group' id="nombre_conductor_field_box">
                                    <div><label for='field-'>Punto de Llegada</label></div>
                                    <input type="text" id="punto_llegada" name="punto_llegada" class="form-control" value="<?= $remision->punto_llegada ?>">
                                </div>
                            </div>                      
                            <div class="col-3 col-md-6">
                                <div class='form-group' id="fecha_partidafield_box">
                                    <div><label for='field-'>Fecha de Partida</label></div>
                                    <input id='fecha_salida' name="fecha_salida" class="form-control" placeholder='dd/mm/yyyy' type="date" value="<?= $remision->fecha_salida ?>">
                                </div>
                            </div>
                            <div class="col-3 col-md-6">
                                <div class='form-group' id="fecha_llegadafield_box">
                                    <div><label for='field-'>Fecha de Llegada</label></div>
                                    <input id='fecha_llegada' name="fecha_llegada" class="form-control" placeholder='dd/mm/yyyy' type="date" value="<?= $remision->fecha_llegada ?>">
                                </div>
                            </div>                                                                                  
                        </div>
                    </div>
                </div>
        </div>
        <!--- EnD traslado --->         
    </div>

    </div>



        <!--end::Form-->
    </div>   
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Productos
                </h3>
            </div>
        </div>

        <!--begin::Form-->        
        <div class="kt-portlet__body">
            

            

            <div class="kt-section kt-section--first">
            	<div class="row productos">
            		<table class="table table-bordered">
            			<thead>
            				<tr>
            					<th>Codigo</th>
            					<th>Producto</th>            					
            					<th>Cantidad</th>
            				</tr>
            			</thead>
            			<tbody>
            				<?php 
            					$productos = get_instance()->db->get_where('nota_remision_detalle',['nota_remision_id'=>$primary_key]);
            					foreach($productos->result() as $p): 
            					$producto = get_instance()->db->get_where('productos',array('codigo'=>$p->producto))->row();            						        
            				?>
            					<tr>
	            					<td><?= $p->producto ?></td>
	            					<td><?= @$producto->nombre_comercial ?></td>	            					
	            					<td>
	            						<input type="hidden" name="producto[]" value="<?= $p->producto ?>">
            							<input type="text" name="cantidad[]" class="form-control" value="<?= $p->cantidad ?>" placeholder="Cantidad">
	            					</td>
	            				</tr>
            				<?php endforeach ?>
            			</tbody>
            		</table>
                </div>
            </div>
            <div class="kt-form__actions text-right mr-20">
	        	<!--<button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-servicios-tab').click()" class="btn btn-success">Atras</button>-->
	            <button type='submit' class="btn btn-success">Guardar</button>                    
	            <a href="<?= base_url() ?>reportes/rep/verReportes/<?= get_instance()->ajustes->id_reporte_remision ?>/html/valor/<?= $primary_key ?>" class="btn btn-success" data-target="_blank">Imprimir</a>
	        </div>            
        </div>    

        <!--end::Form-->
    </div>   
    <div class="report"></div>     
    <!--end::Form-->
    <!-- Start of hidden inputs -->
    <?php
        foreach($hidden_fields as $hidden_field){
            echo $hidden_field->input;
        }
    ?>
    <?php if ($is_ajax) { ?>
        <input type="hidden" name="is_ajax" value="true" />
    <?php }?>
    <!-- End of hidden inputs -->
    <?php echo form_close(); ?>
</div>

<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_add_form = "Almacenado correctamente";
	var message_insert_error = "Error al insertar";
</script>

<?php get_instance()->hcss[] = "
	<style>
		.kt-section{
			margin:20px 0;
		}
		.chzn-drop,.chzn-search,.chzn-results,.chzn-search input{
			min-width:120px !important;
		}
		.form-group{
			margin-bottom:5px !important;
		}
		[type='radio']:not(:checked), [type='radio']:checked {
		    position: initial;
		    left: initial;
		    opacity: 1;
		    margin-right:5px;
		}
		@media screen and (max-width:480px){
			.nav {
			    flex-wrap: wrap;
			}
		}
	</style>
"; ?>
<?php endif ?>