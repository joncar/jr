<?php	
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-add.js?v=1.1"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';
    get_instance()->hcss[] = '<style>.alert{flex-wrap:wrap;} .alert p{width:100%;}</style>';
?>

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $insert_url, 'method="post" onsubmit="enviar(this); return false;" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Añadir <?php echo $subject?>
                </h3>
            </div>
        </div>

        <!--begin::Form-->        
        <div class="kt-portlet__body">
            <div>
            	<div class="row">
            		<?php foreach($fields as $field): ?>
        				<div class="col-6 col-md-3">
		                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
		                        <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>
		                        <?php echo $input_fields[$field->field_name]->input ?>
		                    </div>
	                    </div>
	                <?php endforeach ?>	
	                <div class="col-6 col-md-3">
	                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
	                        <div><label for='field-'>Cliente:</label></div>
	                        <input type="text" id="cliente" class="form-control" readonly="">
	                    </div>
                    </div>	                
                    <div class="col-6 col-md-3">
	                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
	                        <div><label for='field-'>Nro Remisión:</label></div>
	                        <input type="text" id="remision" class="form-control" readonly="">
	                    </div>
                    </div>	                
                </div>
            </div>          
        </div>    

        <!--end::Form-->
    <div class = "row">
    	<div class="col-12 col-md-6">
        <div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h1 class="kt-portlet__head-title">
							Datos Conductor y Vehiculos Transporte
						</h1>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div>
						<div class="row">
							<div class="col-2 col-md-6">
	                    		<div class="form-group" id="nombre_transportista_field_box">
	                      			<div><label for='field-'>Transportista:</label></div>
	                        		<input type="text" id="nombre_transportista" name="nombre_transportista" class="form-control">
	                    		</div>
							</div>
							<div class="col-3 col-md-6">
								<div class='form-group' id="ruc_transportista_field_box">
				                    <label for='field-ruc_transportista' id="ruc_transportista_display_as_box" style="width:100%">
	                      			<div><label for='field-'>RUC-CI:</label></div>
	                        		<input type="text" id="ruc_transportista" name="ruc_transportista" class="form-control">
				                </div>
							</div>
							<div class="col-3 col-md-6">
								<div class='form-group' id="nombre_conductor_field_box">
	                      			<div><label for='field-'>Conductor:</label></div>
	                        		<input type="text" id="nombre_conductor" name="nombre_conductor" class="form-control">
				                </div>
							</div>
							<div class="col-3 col-md-6">
								<div class='form-group' id="documento_conductor_field_box">
	                      			<div><label for='field-'>RUC-CI:</label></div>
	                        		<input type="text" id="documento_conductor" name="documento_conductor" class="form-control">
				                </div>
							</div>
							<div class="col-3 col-md-6">
								<div class='form-group' id="direccion_conductorfield_box">
	                      			<div><label for='field-'>Dirección de Conductor:</label></div>
	                        		<input type="text" id="direccion_conductor" name="direccion_conductor" class="form-control">
				                </div>
							</div>							
							<div class="col-3 col-md-6">
								<div class='form-group' id="fecha_trasladofield_box">
	                      			<div><label for='field-'>Fecha de Traslado:</label></div>
			                    	<input id='fecha_traslado' name="fecha_traslado" class='datepicker-input form-control' placeholder='dd/mm/yyyy' type="date">
				                </div>
							</div>
							<div class="col-3 col-md-6">
								<div class='form-group' id="marca_vehiculofield_box">
	                      			<div><label for='field-'>Marca de Vehiculo:</label></div>
	                        		<input type="text" id="marca_vehiculo" name="marca_vehiculo" class="form-control">
				                </div>
							</div>	
							<div class="col-3 col-md-6">
								<div class='form-group' id="nro_chapafield_box">
	                      			<div><label for='field-'>Nro de Chapa:</label></div>
	                        		<input type="text" id="nro_chapa" name="nro_chapa" class="form-control">
				                </div>
							</div>																				
						</div>
					</div>
				</div>
		</div><!--- EnD conductor --->
	</div>
		<div class="col-12 col-md-6">
        <div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h1 class="kt-portlet__head-title">
							Datos de Traslado
						</h1>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div>
						<div class="row">
							<div class="col-3 col-md-6">
	                    		<div class="form-group" id="motivo_traslado_field_box">
	                        		<div><label for='field-'>Motivo de Traslado</label></div>
	                        		<input type="text" id="motivo_traslado" name="motivo_traslado" class="form-control">
	                    		</div>
							</div>
							<div class="col-3 col-md-6">
								<div class='form-group' id="punto_partida_field_box">
	                        		<div><label for='field-'>Punto de Partida</label></div>
	                        		<input type="text" id="punto_partida" name="punto_partida" class="form-control">
				                </div>
							</div>
							<div class="col-3 col-md-6">
								<div class='form-group' id="nombre_conductor_field_box">
	                        		<div><label for='field-'>Punto de Llegada</label></div>
	                        		<input type="text" id="punto_llegada" name="punto_llegada" class="form-control">
				                </div>
							</div>						
							<div class="col-3 col-md-6">
								<div class='form-group' id="fecha_partidafield_box">
	                        		<div><label for='field-'>Fecha de Partida</label></div>
			                    	<input id='fecha_salida' name="fecha_salida" class='datepicker-input form-control' placeholder='dd/mm/yyyy' type="date">
				                </div>
							</div>
							<div class="col-3 col-md-6">
								<div class='form-group' id="fecha_llegadafield_box">
	                        		<div><label for='field-'>Fecha de Llegada</label></div>
			                    	<input id='fecha_llegada' name="fecha_llegada" class='datepicker-input form-control' placeholder='dd/mm/yyyy' type="date">
				                </div>
							</div>																					
						</div>
					</div>
				</div>
		</div>
		<!--- EnD traslado --->			
	</div>

    </div>   
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Productos
                </h3>
            </div>
        </div>

        <!--begin::Form-->        
        <div class="kt-portlet__body">
            <div class="kt-section kt-section--first">
            	<div class="row productos">
            		<table class="table table-bordered">
            			<thead>
            				<tr>
            					<th>Codigo</th>
            					<th>Producto</th>
            					<th>Cantidad</th>
            					<th>Entregado</th>
            					<th>Disponible</th>
            					<th>Cantidad</th>
            				</tr>
            			</thead>
            			<tbody>
            				<tr>
            					<td>{codigo}</td>
            					<td>{nombre_comercial}</td>
            					<td>{cantidad}</td>
            					<td>{entregados}</td>
            					<td>{disponible}</td>
            					<td>
            						<input type="hidden" name="producto[]" value="{codigo}">
            						<input type="text" name="cantidad[]" class="form-control" value="{disponible}" data-max="{disponible}" placeholder="Cantidad">
            					</td>            					
            				</tr>
            				<tr>
            					<td colspan="6">Incluye un numero de venta para mostrar el detalle de venta</td>
            				</tr>
            			</tbody>
            		</table>
                </div>
            </div>
            <div class="kt-form__actions text-right mr-20">
	        	<!--<button type="button" onclick="$(this).parents('.kt-portlet__body').find('#nav-servicios-tab').click()" class="btn btn-success">Atras</button>-->
	            <button type='submit' class="btn btn-success">Guardar</button>                    
	        </div>            
        </div>    

        <!--end::Form-->
    </div>   
    <div class="report"></div>     
    <!--end::Form-->
    <!-- Start of hidden inputs -->
    <?php
        foreach($hidden_fields as $hidden_field){
            echo $hidden_field->input;
        }
    ?>
    <?php if ($is_ajax) { ?>
        <input type="hidden" name="is_ajax" value="true" />
    <?php }?>
    <!-- End of hidden inputs -->
    <?php echo form_close(); ?>
</div>
<script>
      window.afterLoad.push(function(){
               $("#field-venta").on('keydown',function(e){
                     if(e.which==13){ //Enter
                          e.preventDefault();
                          searchVenta();
                          return false;
                     }
               });
      });
</script>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_add_form = "Almacenado correctamente";
	var message_insert_error = "Error al insertar";
</script>

<script>
	window.trOriginal = '';
	window.table = '';
	window.empty = '';
	window.afterLoad.push(function(){
		window.trOriginal = $($(".productos tbody tr")[0]).clone();
		window.empty = $($(".productos tbody tr")[1]).clone();
		window.table = $('.productos tbody');
		$($(".productos tbody tr")[0]).remove();
		$("#field-venta").on('change',function(){
			if($(this).val()!=''){
				searchVenta();
			}
		});

		$(document).on('change','input[name="cantidad[]"]',function(){
			var max = parseFloat($(this).data('max'));
			var val = parseFloat($(this).val());
			if(val>max){
				$(this).val(max);
			}
			totalizar();
		});	
		getNroRemision();
	});	

	function totalizar(){
		var total = 0;
		for(var i=0;i<$('input[name="cantidad[]"]').length;i++){
			total+= parseFloat($($('input[name="cantidad[]"]')[i]).val());
		}
		$("#field-total").val(total);
	}

	function getNroRemision(){
		$.post('<?= base_url() ?>movimientos/notasRemision/nota_remision/getNroRemision',{

		},
		function(data){
			$("#remision").val(data);
		});
	}

	function searchVenta(){
		var l = this;
		$("#cliente").val('');
		window.table.html(window.empty.clone().find('td').html('Buscando productos...'));		
		$.post('<?= base_url() ?>movimientos/ventas/ventas/json_list',{
			'ventas.id':$("#field-venta").val()
		},function(data){	
			data = JSON.parse(data);		
			if(data.length>0){				
				data = data[0];
				$("#cliente").val(data.s4983a0ab);
				$.post(base_url+'movimientos/ventas/ventadetalle/json_list',{
					venta:data.id,
					per_page:1000,
					page:1
				},function(dd){					
					dd = JSON.parse(dd);
					if(dd.length>0){
						window.table.html('');						
						for (var i in dd){							
							var tr = window.trOriginal.html();
							tr = tr.replace(/{codigo}/g,dd[i].producto.codigo);
							tr = tr.replace(/{nombre_comercial}/g,dd[i].producto.nombre_comercial);
							tr = tr.replace(/{entregados}/g,dd[i].producto.remitidos);
							tr = tr.replace(/{disponible}/g,dd[i].producto.disponibles);
							tr = tr.replace(/{cantidad}/g,dd[i].cantidad);
							window.table.append('<tr>'+tr+'</tr>');
							totalizar();
						}
					}else{
						window.table.html(window.empty.clone().find('td').html('Productos no encontrados'));
					}
				});
			}else{
				$("#field-venta").addClass('error');
				window.table.html(window.empty.clone().find('td').html('Productos no encontrados'));
			}
		});
	}

	function enviar(form){
		var url = $(form).attr('action');
	    insertar(url,form,'.report',function(data){
	        for(var i in data){
	            data.success_message = data.success_message.replace('{id}',data.insert_primary_key);
	        }
	        $(document).find('.report').removeClass('alert').removeClass('alert-info');
	        $(document).find('.report').addClass('alert alert-success').html(data.success_message);
	        $(form).find('input[type="text"]').val('');
	        $(form).find('input[type="number"]').val('');
	        $(form).find('input[type="password"]').val('');
	        $(form).find('select').val('');
	        $(form).find('textarea').val('');
	        window.table.html(window.empty.clone());
	        window.open('<?= base_url() ?>reportes/rep/verReportes/<?= get_instance()->ajustes->id_reporte_remision ?>/html/valor/'+data.insert_primary_key);
	        getNroRemision();
	    });
	}
</script>

<?php get_instance()->hcss[] = "
	<style>
		.kt-section{
			margin:20px 0;
		}
		.chzn-drop,.chzn-search,.chzn-results,.chzn-search input{
			min-width:120px !important;
		}
		.form-group{
			margin-bottom:5px !important;
		}
		[type='radio']:not(:checked), [type='radio']:checked {
		    position: initial;
		    left: initial;
		    opacity: 1;
		    margin-right:5px;
		}
		@media screen and (max-width:480px){
			.nav {
			    flex-wrap: wrap;
			}
		}
	</style>
"; ?>