<?= $output ?>

<script>
	function showDetail(id){
		$.post('<?= base_url('movimientos/ventas/ventas_detail/') ?>/'+id+'/',{},function(data){
			emergente(data);
		});
	}

	function anular(id){
        if(confirm('Seguro que desea anular esta factura?')){
            $.post('<?= base_url('movimientos/ventas/ventas/anular') ?>',{id:id},function(data){
                emergente(data);
            });
        }
    }
</script>