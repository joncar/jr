<?= $output ?>
<script>
	const gs = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'PYG',minimumFractionDigits: 0});
	var venta = <?= !empty($venta)?json_encode($venta):'[]' ?>;
	var maxCuotas = <?php echo get_instance()->ajustes->cantidad_maxima_cuotas_plan_credito ?>;
	
	window.afterLoad.push(function(){
		$("#field-fecha_credito").val('<?= date("d/m/Y H:i:s") ?>').parent().parent().hide();
		$("#field-monto_venta").val(venta.total_venta).attr('readonly',true);
		$("#field-entrega_inicial").val(0);
		$("#field-monto_credito").val(venta.total_venta);
		$("#field-entrega_inicial").trigger('change');	
	
		$(document).on('change',"#field-entrega_inicial",function(){
			$(this).val(formatCurrency($(this).val()));
			var credito = parseFloat(unFormatCurrency($("#field-monto_credito").val()));
			var inicial = parseFloat(unFormatCurrency($("#field-entrega_inicial").val()));
			inicial = isNaN(inicial)?0:inicial;
			var total = credito-inicial;
			if(total<0){
				alert("Disculpe, el monto inicial no puede ser superior al monto del crédito");
				$("#field-entrega_inicial").val(0);
				$("#field-total_credito").val(credito);
			}else{
				$("#field-total_credito").val(total);
			}
			totalizar();
		});

		$(document).on('change','#field-interes',function(){
			var interes = parseFloat($('#field-interes').val());
			var venta = parseFloat(unFormatCurrency($("#field-monto_venta").val()));
			credito = venta;
			if(isNaN(credito)){
				credito = parseFloat(unFormatCurrency($("#field-monto_credito").val()));
			}else{
				var m = credito + (credito * (interes/100));
				$("#field-monto_credito").val(formatCurrency(m));				
			}
			totalizar();
			$("#field-entrega_inicial").trigger('change');
		});

		$(document).on('change','#field-cant_cuota',function(){
			var credito = parseFloat(unFormatCurrency($("#field-total_credito").val()));
			var cuotas = parseFloat(unFormatCurrency($("#field-cant_cuota").val()));
			if(cuotas>maxCuotas){
				cuotas = maxCuotas;
				$("#field-cant_cuota").val(maxCuotas);
			}
			if(!isNaN(credito) && !isNaN(cuotas)){
				$("#field-monto_cuota").val(formatCurrency(credito/cuotas));
			}
			//$("#field-entrega_inicial").trigger('change');
		});

		$(document).on('change','#field-monto_cuota',function(){
			var monto = parseFloat(unFormatCurrency($(this).val()));
			var cuotas = parseFloat(unFormatCurrency($("#field-cant_cuota").val()));			
			if(!isNaN(cuotas) && !isNaN(monto)){
				$("#field-total_credito").val(formatCurrency(cuotas*monto));
			}
			$(this).val(formatCurrency(monto));
			//$("#field-entrega_inicial").trigger('change');
		});
	});

	function totalizar(){
		var interes = parseFloat($('#field-interes').val());
		var venta = parseFloat(unFormatCurrency($("#field-monto_venta").val()));
		credito = venta;
		if(isNaN(credito)){
			credito = parseFloat(unFormatCurrency($("#field-monto_credito").val()));
		}else{
			var m = credito + (credito * (interes/100));
			//$("#field-monto_credito").val(formatCurrency(m));
			credito = m-parseFloat(unFormatCurrency($("#field-entrega_inicial").val()));
		}
		if(!isNaN(interes) && !isNaN(credito)){
			var total = isNaN(venta)?credito + (credito * (interes/100)):credito;
			var total_tag = total.toFixed(0);
			if(isNaN(venta)){
				var interes_tag = total-credito;
			}else{
				var interes_tag = parseFloat(unFormatCurrency($("#field-monto_credito").val()))-venta;
			}
			total_tag = formatCurrency(total_tag)
			interes_tag = formatCurrency(interes_tag)
			$("#field-total_credito").val(total_tag);
			$("#field-total_interes").val(interes_tag);
		}
	}

	function formatCurrency(input){        
        input = parseInt(input);
        input = isNaN(input)?0:input;        
        let num = (input === 0) ? "" : input.toLocaleString("en-US");
        input = num.split(",").join(".");        
        return input==''?0:input;
    }

    function unFormatCurrency(input){ 
    	if(typeof(input) == 'undefined'){
    		return input;
    	}
    	console.log(input);           	
    	input = input.replace(/\./g,'');
    	console.log(input);
    	input = input.replace(/,/g,'.');
    	console.log(input);
        return input;
    }
</script>

<script>
	function showDetail(id){
		$.post('<?= base_url('movimientos/ventas/ventas_detail/') ?>/'+id+'/',{},function(data){
			emergente(data);
		});
	}

	function printPlan(id){				
		var idReporte = '<?= $this->ajustes->id_reporte_plan ?>';
		window.open('<?= base_url() ?>reportes/rep/verReportes/'+idReporte+'/html/valor/'+id);
	}

	function printPagare(id){
		var idReporte = '<?= $this->ajustes->id_reporte_pagare ?>';
		window.open('<?= base_url() ?>reportes/rep/verReportes/'+idReporte+'/html/valor/'+id);
	}
</script>