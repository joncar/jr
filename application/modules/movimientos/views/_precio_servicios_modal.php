<div id="PrecioServicioModal" class="modal fade" tabindex="-1" role="dialog">
  <form action='' onsubmit="window.setPrecioServicio();return false;" autocomplete="off">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">	        
	        <h4 class="modal-title">Asignar precio a servicio</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	  		<div class="row" style="margin-left: 0; margin-right: 0">

				<div class="col-12 col-md-12">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Precio Venta</label>
				    <input type="text" id="field-precio_servicio" class="form-control" autocomplete="off">
				  </div>
				</div>

				<div class="col-12 col-md-12">
					<div class="resultToken"></div>
			  </div>
			</div>			  
	      </div>
	      <div class="modal-footer">	      	
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        <button type="submit" class="btn btn-primary">Asignar</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->

<script>
  window.afterLoad.push(function(){
  	$("#PrecioServicioModal").on("hidden.bs.modal",function(){      
      $("#PrecioServicioModal").trigger('uncomplete');
      $("#PrecioServicioModal #field-precio_servicio").val('');
    });
    $("#PrecioServicioModal").on("shown.bs.modal",function(){      
      $("#PrecioServicioModal #field-precio_servicio").focus();
    });
  });
  function getPrecioServicio(resolve){
	$("#PrecioServicioModal").modal('show');
	window.setPrecioServicio = ()=>{
		var precio = parseFloat($("#field-precio_servicio").val()) || 0;
		resolve(precio)
		$("#PrecioServicioModal").modal('hide');
	}
  }
</script>