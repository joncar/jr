<?php get_instance()->hcss[] = '
	<style>
		a:focus,div:focus,.chzn-container-active{
			border:1px dashed blue;
		}
		body {
	        color: #000;
	        font-weight: 600;
	    }
	    .form-control{
	    	color: #000;
		    font-weight:600;
		    opacity: 1;
		    font-size:15px;
	    }
		.panel{
			border-radius:0px;
		}

		.error{
			border: 1px solid red !important;
		}

		.patternCredito{
			background: #ffffffa6;
			width: 100%;
			height: 100%;
			position: absolute;
			top: 0px;
			left: 0px;
			z-index:10;
		}

		.btnsaldo{
			position: absolute;top: 0;right: 15px;
		}
		@media screen and (max-width:768px){
			.btnsaldo{
				top: -15px;
				font-size:20px;
			}	
		}
		.kt-portlet{
			margin:0;
		}

		.table thead th, .table td{
			padding:6px;
			color: #000;
			font-weight: 600;
		}

		tbody .form-control{
			height:22px;
		}

		.kt-portlet .kt-portlet__head{
			min-height: inherit;
			padding:10px;
		}

		.totales input, .totales .form-control:focus{
			background:lightgreen !important;
			padding: 0 12px;
			border-radius: 0;
		}

		.pagoEfectivo input, .pagoEfectivo .form-control:focus{
			background-color: lightblue;
			border-radius: 0;
			padding: 0 12px;
		}

		.vueltos input{
			background: #e8e800 !important;
			color: red;
			border-radius: 0;
			padding: 0 12px;
		}

		input.guaranies{
			font-weight: bold;
			font-size: 19px;
		}
		div.inp{
			position: relative;
		}
		div.banderas{
			background-image:url('.base_url().'img/banderas.jpg);
			width:20px;
			height:20px;	
			position: absolute;
			right:0px;
			top:0px;
			background-size: 100%;	
		}	
		div.banderas.us{
			background-position: 0 0px;
		}
		div.banderas.ar{
			background-position: 0 -20px;
		}		
		div.banderas.br{
			background-position: 0 -40px;
		}
		div.banderas.py{
			background-position: 0 -60px;
		}

		div.banderas-2x{
			background-image:url('.base_url().'img/banderas.jpg);
			width:40px;
			height:40px;	
			position: absolute;
			right:0px;
			top:0px;
			background-size: 100%;	
		}	
		div.banderas-2x.us{
			background-position: 0 0px;
		}
		div.banderas-2x.ar{
			background-position: 0 -40px;
		}		
		div.banderas-2x.br{
			background-position: 0 -80px;
		}
		div.banderas-2x.py{
			background-position: 0 -120px;
		}
		#procesar th{
			font-weight: bold;
			font-size: 16px;
		}
		.btn-lg, .btn-group-lg > .btn {
		    padding: 0.7rem 1.45rem;
		}
		.caja {
    color: #3c4cca;
    font-family: system-ui;
    font-weight: bold;
    margin-left: 250px;
		}

		.datosprincipales.oculto{
			display:none
		}
		.totalinpspan{
			position: absolute;
			top: 26px;
			left: 30px;
			font-weight: bold;
		}
		.totalinp{
			font-size:50px !important; 
			font-weight:bold !important; 
			text-align: right !important;
			height: 107px !important;
			vertical-align: baseline !important;
			background:lightgreen !important;
			border:0 !important;
		}

		@media screen and (max-width:480px){
			.totalinpspan {
			  position: absolute;
			  top: 7px;
			  left: 8px;
			  font-weight: bold;
			}

			.totalinp {
			  height: 69px !important;
			}

			.caja {
			  color: #3c4cca;
			  font-family: system-ui;
			  font-weight: bold;
			  margin-left: 27px;
			}

			#ventaDescr thead th:nth-child(2) {
			  min-width: 177px;
			}

			#ventaDescr thead th:nth-child(1),
			#ventaDescr tbody td:nth-child(1){
				display:none
			}

			.otrasdivisas{
				display:none;
			}
		}
	</style>
'; 

$tipofacturacion = $this->ajustes->tipo_facturacion_id;
$tipotransaccion = 1;

$caja = $this->db->get_where('cajas',['id'=>$this->user->caja]);
if($caja->num_rows()>0){
	$caja = $caja->row();	
	if(isset($this->ajustes->cerrarAlImprimirVenta) && isset($caja->cerrarAlImprimirVenta) && $this->ajustes->cerrarAlImprimirVenta!=$caja->cerrarAlImprimirVenta){
		$this->ajustes->cerrarAlImprimirVenta = $caja->cerrarAlImprimirVenta;		
	}

	if(isset($this->ajustes->tipo_facturacion_id) && isset($caja->tipo_facturacion_id) && $this->ajustes->tipo_facturacion_id!=$caja->tipo_facturacion_id){
		$tipofacturacion = $caja->tipo_facturacion_id;	
		$tipotransaccion = $caja->tipotransaccion_id;	
	}
}

?>
<div class="kt-portlet">
	<div class="kt-portlet__body" style="padding:10px 25px;">
		<div class="kt-section">
			<div class="row" style="position: relative;">
				<!--<div style="width:100%; text-align: right; position:absolute; left:0; padding:0 30px">
					#FACTURA: <span id="nroFactura"></span>
				</div>-->
				<div class="col-12 col-md-12">
					Cotizaciones: 
					<div class="inp" style="display:inline-block;margin-left: 13px;">
						<span><?= number_format($this->ajustes->tasa_dolares,0,',','.') ?> Gs.</span>
						<div class="banderas-2x us" style="width:10px; height:10px; left:-13px; top:4px"></div>
					</div>
					<div class="inp" style="display:inline-block;margin-left: 13px;">
						<span><?= number_format($this->ajustes->tasa_reales,0,',','.') ?> Gs.</span>
						<div class="banderas-2x br" style="background-position: 0 -60px;width:10px; height:10px; left:-13px; top:4px"></div>
					</div>
					<div class="inp" style="display:inline-block;margin-left: 13px;">
						<span><?= number_format($this->ajustes->tasa_pesos,0,',','.') ?> Gs.</span>
						<div class="banderas-2x ar" style="background-position: 0 -50px; width:10px; height:10px; left:-13px; top:4px"></div>
					</div>
				</div>
				<div class="col-12 col-md-9">
					<div class="row" style="background: lightblue;">
						<div onclick="toggleDatos()" class="col-12 p-3 d-flex d-md-none">
							<div style="width: 90%;"><b>Datos de Factura</b></div>
							<div style="cursor:pointer;width: 10%;text-align: center;font-size: 14px;"><i class="fa fa-chevron-down"></i></div>
						</div>
						<div class="datosprincipales col-12 col-md-4">
							Cliente <span class="alt" style="display:none">(H)</span><a href="#addCliente" data-toggle="modal" style="color:green"><i class="fa fa-plus"></i> <span class="alt" style="display:none">(X)</span></a>: 
							<?php 
								$this->db->limit(1);
								echo form_dropdown_from_query('cliente','clientes','id','nro_documento nombres apellidos',1,'id="cliente"') 
							?>
							<a href="javascript:saldo()" class="btnsaldo d-none d-md-block">
								<span class="alt" style="display:none">(B)</span>
								<i class="fa fa-credit-card"></i>
							</a>
						</div>
						<div class="datosprincipales col-12 col-md-2">
							Transacción: 
							<?php 
								$tipo = 1;
								echo form_dropdown_from_query('transaccion',$this->db->get('tipotransaccion'),'id','denominacion',1,'id="field-transaccion"');
							?>
						</div>
						<div class="datosprincipales col-12 col-md-2 plan" style="display: none;">
							PLAN: 
							<br/>
							SI <input type="radio" name="plan" id="plan" value="1"> NO <input type="radio" name="plan" id="plan" value="0" checked="">
						</div>
						<div class="datosprincipales col-12 col-md-2">
							Forma de Pago: 
							<?php 
								$formas = $this->db->get_where('formapago');								
							?>
							<select name="forma_pago" id="formapago" class="form-control chosen-select">
								<option value="">Seleccione una forma de pago</option>	
								<?php foreach($formas->result() as $f): ?>
									<option value="<?= $f->id ?>" data-porVenta="<?= $f->por_venta ?>"><?= $f->denominacion ?> <?= $f->por_venta ?>%</option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="datosprincipales col-12 col-md-4">
							Vendedor: 
							<?php 
								echo form_dropdown_from_query('usuario','user','id','nombre apellido',$this->user->id,'id="usuario"')
							?>
						</div>
						
						<div class="datosprincipales col-12 col-md-2" style="display: none">
							Pedido: 
							<?php 
								echo form_dropdown('pedido',array('0'=>'NO','1'=>'SI'),0,'id="field-pedido" class="form-control"');
							?>
						</div>	
						<div class="datosprincipales col-12 col-md-2" style="display: none">
							Pagado: 
							<?php 
								echo form_dropdown('pagado',array('0'=>'NO','1'=>'SI'),0,'id="field-pagado" class="form-control"');
							?>
						</div>
						<div class="datosprincipales col-12 col-md-2">
							T. Facturación
							<select class="tipoFacturacion form-control" name="tipo_facturacion_id" id="optionsRadios1">
								<?php foreach($this->db->get('tipo_facturacion')->result() as $t): ?>
									<option data-emitefactura="<?= $t->emite_factura ?>" data-url="<?= $t->reporte ?>" data-max_items="<?= $t->max_items ?>" value="<?= $t->id ?>"><?= $t->denominacion ?></option>
								<?php endforeach ?>
							</select>							  
						</div>	
						<div class="datosprincipales col-12 col-md-4">
							Dirección
							<input type="text" id="direccion_cliente" class="form-control" readonly="" style="height: 31px;">
						</div>
						<div class="datosprincipales col-12 col-md-2">
							Teléfono
							<input type="text" id="telefono_cliente" class="form-control" readonly="" style="height: 31px;">
						</div>
						<div class="datosprincipales col-12 col-md-4">
							Repartidor: 
							<?php 
								echo form_dropdown_from_query('repartidores_id','repartidores','id','denominacion','','id="repartidor"')
							?>
						</div>						
					</div>
				</div>
				<div class="col-12 col-md-3 mt-2 mt-md-0" style="position: relative;background:lightgreen;">
					<span class="totalinpspan">Total Venta Gs: </span>
					<input type="text" name="total_venta" class="form-control totalinp" readonly="" value="300.000">
				</div>

			</div>
		</div>

	</div>
</div>


<div class="row cuerpoTransaccion">
	<div class="col-12 col-sm-9 col-md-9">
		<div class="kt-portlet">
			<div class="kt-portlet__body" style="padding: 0 15px;">
				<div class="kt-section">
					<div style="height:350px; overflow-y:auto; background:#f2f3f8">
						<table class="table table-bordered" id="ventaDescr">
							<thead style="background: #f2f3f8;">
								<tr>
									<th>Código</th>
									<th>Nombre</th>
									<th>Cantidad</th>
									<th>Precio</th>
									<th class="descuento">%Desc</th>
									<th class="descuento">P.Desc</th>
									<?php if($this->ajustes->controlar_vencimiento_stock==1): ?>
										<th>Venc.</th>
									<?php endif ?>
									<th>Total</th>
									<th>Stock</th>
								</tr>
							</thead>
							<tbody>

								<tr id="productoVacio">
									<td>
										<a href="javascript:void(0)" class="rem d-none d-md-inline-block" style="display:none;color:red">
											<i class="fa fa-times"></i>
										</a>
										<span>&nbsp;</span>
									</td>
									<td>
										<a href="javascript:void(0)" class="rem d-md-none" style="color: red;font-size: 19px;vertical-align: middle;">
											<i class="fa fa-times"></i>
										</a>
										<span>&nbsp;</span>
									</td>
									<td><input name="cantidad" class="form-control cantidad" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td><input name="precio" class="form-control precio" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0"></td>
									<td class="descuento"><input name="por_desc" class="form-control por_desc" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td class="descuento"><input name="precio_descuento" class="form-control precio_descuento" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<?php if($this->ajustes->controlar_vencimiento_stock==1): ?>
									<td>
										<select name="vencimiento" class="form-control vencimiento" style="width:90px;text-align: right;padding: 0 6px;"></select>
									</td>
									<?php endif ?>
									<td style="text-align:right; background:#add8e6">&nbsp;</td>
									<td>&nbsp;</td>
								</tr>

								
							</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-12 col-md-2" style="text-align: center;padding: 6px;">
							Cant: <span id="cantidadProductos">4</span>					
						</div>
						<div class="col-12 col-md-10" style="position: relative;">
							<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
							<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto (ALT + C)" style="padding-left: 25px;padding-right: 73px;" autocomplete="off">
							<button style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar insertar">Insertar</button>

							<div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-12 col-md-3 tarifario d-none d-md-block" style="padding-left: 5px;">
		<div class="kt-portlet">
			<!--<div class="tarifarioToggle">
				<i class="fas fa-dollar-sign fa-2x"></i>
			</div>-->
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						<b>Resumen de venta</b>
					</h1>
				</div>
			</div>
			<div class="kt-portlet__head">
				<div class="row totales">
					<div class="col-12 col-md-4">Pesos: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<div class="inp">
							<input type="text" class="form-control" name="total_pesos" value="300.000" readonly="">
							<div class="banderas-2x ar"></div>
						</div>

					</div>
					<div class="col-12 col-md-4">Reales: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<div class="inp">
							<input type="text" class="form-control" name="total_reales" value="300.000" readonly="">
							<div class="banderas-2x br"></div>
						</div>

					</div>
					<div class="col-12 col-md-4">Dolares: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<div class="inp">
							<input type="text" class="form-control" name="total_dolares" value="300.000" readonly="">
							<div class="banderas-2x us"></div>
						</div>

					</div>					
				</div>
			</div>
			<div class="kt-portlet__head">
				<div class="row" style="width:100%">
					<div class="col-12 col-md-12">
						Observaciones: 
						<input type="text" name="observaciones" id="observacion" class="form-control">
					</div>
				</div>
			</div>
			<div id="foto" style="<?= isset($this->ajustes->ocultar_foto_producto_en_ventas) && $this->ajustes->ocultar_foto_producto_en_ventas==1?'display:none':'' ?>">
				
			</div>
		</div>

		
	</div>
</div>

<div class="col-12 d-md-none m-2">
	<div class="btn-group d-flex" style="justify-content: space-between;">
		<button class="btn btn-primary" onclick="nuevaVenta();" type="button">Nueva Venta</button>
		<button class="btn btn-info" data-target="#addCliente" data-toggle="modal" type="button">Nuevo Cliente</button>
		<button class="btn btn-success" data-toggle="modal" data-target="#inventarioModal" type="button">Buscar productos</button>
	</div>
	<div class="btn-group d-flex mt-2">
		<?php if(empty($edit)): ?>
		  <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#procesar"><i class="fa fa-floppy"></i> Procesar venta <span class="alt" style="display:none">(P)</span></button>
	  <?php endif ?>
	</div>
</div>

<div class="row d-none d-md-block">
	<div class="col-12 col-md-9">
		<div class="btn-block btn-group-justified d-flex flex-wrap" role="group" aria-label="..." style="white-space:nowrap">
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-success btn-lg" onclick="nuevaVenta();" style="color:#fff !important"><i class="fa fa-plus-circle"></i> Nueva venta <span class="alt" style="display:none">(N)</span></button>
		  </div>
		  <?php if(empty($edit)): ?>
			  <div class="btn-group" role="group">
			    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#procesar"><i class="fa fa-floppy"></i> Procesar venta <span class="alt" style="display:none">(P)</span></button>
			  </div>
		  <?php endif ?>
		  <div class="btn-group" role="group">
		    <a class="btn btn-warning btn-lg" data-toggle="modal" data-target="#inventarioModal" style="color:#000 !important"><i class="fa fa-search"></i> Buscar productos <span class="alt" style="display:none">(I)</span></a>
		  </div>
		  <div class="btn-group" role="group">
		    <a id="newWindow" target="_new" href="<?= base_url('movimientos/ventas/ventas/add') ?>" class="btn btn-warning btn-lg" style="color:#000 !important"><i class="fa fa-window-restore"></i> Nueva pestaña de ventas <span class="alt" style="display:none">(W)</span></a>
		  </div>
		  <div class="btn-group" role="group">
		  	<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#inventarioModal">Inventario <span class="alt" style="display:none">(I)</span></button>
		  </div>
		  <!--<div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" <?= empty($edit)?'disabled="true"':'' ?>><i class="fa fa-print"></i> Imprimir</button>
		  </div>-->
		</div>
	</div>
	
</div>

<div id="procesar" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title">
        	Procesar venta
        </h4>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="row">
      	<div class="caja">
       		<h5>
     				<b><?= $this->user->sucursalnombre?> - <?= $this->user->cajanombre?></b>
     			</h5>
     		</div>	 
     	</div> 
      <div class="modal-body">
      	<div class="row" style="margin-left: 0; margin-right: 0">
      		<div class="col-12">
      			<div class="radio facturaLegalDiv">      			 
				  <span id="facturaD" style="font-weight:bold; position: absolute; right:0; top:0; z-index:1">
				  	<a href="javascript:;" onclick="window.venta.reloadNroFactura()"> 
					  <i class="fa fa-sync"></i>
				    </a>
				  	#FACTURA: <span id="nroFactura"></span>
				  </span>
				  
				</div>
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-12 col-md-4 totales">
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Total Importe</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Guaranies</td>
      						<td>
      							<div class="inp">
	      							<input type="text" class="guaranies form-control" name="total_venta" value="0" readonly="" style="width: 100%;">
	      							<div class="banderas py"></div>
      							</div>
      						</td>
      					</tr>
      					<tr class="otrasdivisas">
      						<td>Pesos</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="total_pesos" value="0" readonly="" style="width: 100%;">
      								<div class="banderas ar"></div>
      							</td>
      					</tr>
      					<tr class="otrasdivisas">
      						<td>Dolares</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="total_dolares" value="0" readonly="" style="width: 100%;">
      								<div class="banderas us"></div>
      							</td>
      					</tr>
      					<tr class="otrasdivisas">
      						<td>Reales</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="total_reales" value="0" readonly="" style="width: 100%;">
      								<div class="banderas br"></div>
      							</td>
      					</tr>
      					
      				</tbody>
      			</table>   
      			<div id="pago_total" style="padding: 0px 30px;margin: 0 0 31px;">Gs.: <span class="totalGs" style="font-size: 30px;font-weight: bold;">0</span></div>   			
      		</div>
      		<div class="col-12 col-md-4 pagoEfectivo">
      			<div class="patternCredito"></div>
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Pago</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Guaranies</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="guaranies form-control" name="pago_guaranies" value="0" style="width: 100%;">
      								<div class="banderas py"></div>
      							</div>
      						</td>
      					</tr>
      					<tr class="otrasdivisas">
      						<td>Pesos</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="pago_pesos" value="0" style="width: 100%;">
      								<div class="banderas ar"></div>
      							</div>
      						</td>
      					</tr>
      					<tr class="otrasdivisas">
      						<td>Dolares</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="pago_dolares" value="0" style="width: 100%;">
      								<div class="banderas us"></div>
      							</div>
      						</td>
      					</tr>
      					<tr class="otrasdivisas">
      						<td>Reales</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="pago_reales" value="0" style="width: 100%;">
      								<div class="banderas br"></div>
      							</div>
      						</td>
      					</tr>
      					
      					<tr class="otrasdivisas">
      						<td>Debito/Crédito</td>
      						<td><input type="text" class="form-control" name="total_debito" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr id="chequeInp" style="display: none">
      						<td>Total Cheque</td>
      						<td><input type="text" class="form-control" name="total_cheque" value="0" style="width: 100%;" readonly="" onclick="venta.showChequeModal()"></td>
      					</tr>
      				</tbody>
      			</table>
      		</div>
      		<div class="col-12 col-md-4 vueltos">
      			<div class="patternCredito"></div>
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Vuelto</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Guaranies</td>
      						<td>
      							<div class="inp">
      								<input class="guaranies form-control" style="color:red; width:100%;" type="text" name="vuelto" readonly="true" value="0">
      								<div class="banderas py"></div>
      							</div>
      						</td>
      					</tr>
      					<tr  class="otrasdivisas">
      						<td>Pesos</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="vuelto_pesos" readonly="true" value="0" style="width: 100%;">
      								<div class="banderas ar"></div>
      							</div>
      						</td>
      					</tr>
      					<tr  class="otrasdivisas">
      						<td>Dolares</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="vuelto_dolares" readonly="true" value="0" style="width: 100%;">
      								<div class="banderas us"></div>
      							</div>
      						</td>
      					</tr>
      					<tr  class="otrasdivisas">
      						<td>Reales</td>
      						<td>
      							<div class="inp">
      								<input type="text" class="form-control" name="vuelto_reales" readonly="true" value="0" style="width: 100%;">
      								<div class="banderas br"></div>
      							</div>
      						</td>
      					</tr>
      					
      				</tbody>
      			</table>
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-12">
      			<div class="respuestaVenta"></div>
      		</div>
      	</div>		
      </div>
      <div class="modal-footer">
      	<button type="button" class="btnNueva btn btn-info" onclick="nuevaVenta()" style="display: none">Nueva venta <span class="alt" style="display:none">(N)</span></button>
        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>-->
        <button type="button" onclick="sendVenta()" class="btn btn-primary">Guardar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="saldo" class="modal" tabindex="-1" role="dialog">  
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">	        
	        <h4 class="modal-title">Saldo del cliente</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="kt-portlet__head">
	  		  <h1 id="saldoTag" style="text-align: center">0GS</h1>	
	  		  <p style="padding: 0 20px; color:red">Límite de crédito: <span id="limiteSaldoTag"> 0GS </span></p>	  
	      </div>
	      <div class="modal-footer">
	      	<a href="#" id="detallarbtn" class="btn btn-success" target="_blank">Detallar</a>
	      	<a href="#" id="cobrarbtn" class="btn btn-success" target="_blank">Cobrar</a>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>	        
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="addCheque" class="modal" tabindex="-1" role="dialog">  
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title">Agregar cheques</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="kt-portlet__head">
      	  <div class="mensajes"></div>
  		  <table class="table table-bordered">
  		  	<thead>
  		  		<tr>
  		  			<th>Banco</th>
  		  			<th>#Cheque</th>
  		  			<th>Importe</th>
  		  			<th>Vencimiento</th>
  		  			<th>Cruzado</th>
  		  			<th>Acciones</th>
  		  		</tr>
  		  	</thead>
  		  	<tbody>
  		  		<tr>
  		  			<td><?= form_dropdown_from_query('bancos_id','bancos','id','denominacion',0,'',FALSE) ?></td>
  		  			<td><input type="text" name="cheque" class="form-control" value=""></td>
  		  			<td><input type="text" name="monto" class="form-control" value=""></td>
  		  			<td><input type="date" name="vencimiento" class="form-control" value=""></td>
  		  			<td><?= form_dropdown('cruzado',array(0=>'NO',1=>'SI'),0,'class="form-control"') ?></td>
  		  			<td>
  		  				<a href="javascript:venta.addCheque()"><i class="fa fa-plus-circle"></i></a>
  		  			</td>
  		  		</tr>
  		  	</tbody>
  		  </table>	
  		  <div>
  		  	<div class="totalVenta">
  		  		Total Venta: <input type="text" name="total_venta" value="0" readonly="">
  		  	</div>
  		  </div>	  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="venta.closeChequeModal()">Cerrar</button>        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->  
</div><!-- /.modal -->

<?php $this->load->view('_inventario_modal',array(),FALSE,'movimientos'); ?>
<?php $this->load->view('_add_cliente_modal',array(),FALSE,'movimientos'); ?>
<?php $this->load->view('_token_modal',array(),FALSE,'movimientos'); ?>
<?php $this->load->view('_precio_servicios_modal',array(),FALSE,'movimientos'); ?>
<script>
<?php 
	if(!empty($edit)){
		$venta = $this->querys->getVenta($edit);
		if($venta){
			echo 'var editar = '.json_encode($venta).';';
		}else{
			echo 'var editar = undefined;';
		}
	}else{
		echo 'var editar = undefined;';
	}

	if(!empty($default)){		
		echo 'var precargo = '.json_encode($default).';';
		if($default->tipo=='Presupuesto'):
			$ventas = $this->db->get_where('ventas',['presupuestos_id'=>$default->id]);
			if($ventas->num_rows()>0):
	?>
			alert('El presupuesto elegido ya se encuentra facturado, con el id #<?= $ventas->row()->id ?>');
		<?php endif; endif;		
	}else{
		echo 'var precargo = undefined;';
	}

?>
</script>
<?php get_instance()->js[] = '<script src="'.base_url().'js/ventas.js?v='.time().'"></script>'; ?>
<?php get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/ajax-chosen-jsonlist.js?v=1.6"></script>'; ?>
<script>	
	<?php
		$ajustes = $this->ajustes;
		$caja = $this->db->get_where('cajas',['id'=>$this->user->caja])->row();
		$ajustes->solicitar_token_ventas = $caja->solicitar_token;
		$ajustes->activar_mayorista_por_venta = $caja->activar_mayoristas;
		$ajustes->ventastemp = $caja->ventastemp;
		$ajustes->permitir_descuento = $caja->permitir_descuento;
	?>
	var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;
	var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
	var tasa_real = <?= $ajustes->tasa_reales ?>;
	var tasa_peso = <?= $ajustes->tasa_pesos ?>;
	var codigo_balanza = <?= json_encode(explode(',',str_replace(', ',',',$ajustes->cod_balanza))) ?>;
	var modPrecio = <?= $ajustes->permitir_modificar_precio_en_ventas ?>;
	var cantidades_mayoristas = <?php 
        $ar = array();
        foreach($this->db->get('cantidades_mayoristas')->result() as $c){
            $ar[] = $c;
        }
        echo json_encode($ar);
	?>;
	var vender_sin_stock = <?= $ajustes->vender_sin_stock ?>;
	var onsend = false;
	var controlar_vencimiento = <?= $ajustes->controlar_vencimiento_stock ?>;
	var firstLoad = false;
	window.shouldSave = false;	
	window.addEventListener('beforeunload',function(e){
		if (window.shouldSave) {
	        e.preventDefault();
	        e.returnValue = '';
	        return;
	    }
	    delete e['returnValue'];
	});
	window.afterLoad.push(function(){
		window.venta = new PuntoDeVenta();

		initDatos();
		venta.initEvents();
		venta.updateFields();
		if(editar==undefined && precargo==undefined){
			nuevaVenta();	
		}
		
		if(editar!=undefined){		
			venta.setDatos(editar);
		}

		if(precargo!=undefined){	
			precargo.tipo_facturacion_id = '<?= $tipofacturacion ?>';	
			venta.setDatos(precargo);
			venta.datos.precargo = JSON.stringify(precargo);
		}

		if(editar==undefined && precargo == undefined){
			venta.datos.usuario = '<?= $this->user->id ?>';
			venta.loadTemp();
		}
		$(document).on('shown.bs.modal',"#procesar",function(){
			$("#procesar input[name='pago_guaranies']").focus();
			window.venta.doUpdate = false;
		});
		$(document).on('hidden.bs.modal',"#procesar",function(){
			$("#procesar input[name='pago_guaranies']").focus();
			window.venta.doUpdate = true;
		});
		firstLoad = true;



	});

	function initDatos(){
		venta.datos.sucursal = '<?= $this->user->sucursal ?>';
		venta.datos.caja = '<?= $this->user->caja ?>';
		venta.datos.cajadiaria = '<?= $this->user->cajadiaria ?>';
		venta.datos.fecha = '<?= date("Y-m-d H:i") ?>';	
		venta.datos.forma_pago = 1;
		venta.datos.usuario = '<?= $this->user->id ?>';
		venta.datos.tipo_facturacion_id = '<?= $tipofacturacion ?>';
		venta.datos.transaccion = '<?= $tipotransaccion ?>';
		venta.datos.observaciones = '';
		venta.modPrecio = modPrecio;		
	}

	function imprimir(codigo,id){
		var url = typeof(id)!=='undefined'?id:$(".tipoFacturacion option:selected").data('url');
        var w = window.open('<?= base_url() ?>'+url+codigo,'Impresion','width=800,height=600');
        <?php if(isset($this->ajustes->cerrarAlImprimirVenta) && $this->ajustes->cerrarAlImprimirVenta == 1): ?>
	        w.onload = function(){
	        	console.log(w);
	        	setTimeout(function(){w.close()},3000);
	        }        
    	<?php endif ?>    	
	}

	function imprimirCupon(codigo){
		<?php if(!empty($this->ajustes->imprimir_cupon) && !empty($this->ajustes->imprimir_cupon_id)): ?>
	        fetch(window.URI+'movimientos/ventas/ventas/hasCupones/'+codigo).then((resp)=>{
	        	
	        	resp.json().then((x)=>{	        		
	        		if(x.print>0){
	        			var url2 = 'reportes/rep/verReportes/<?= $this->ajustes->imprimir_cupon_id ?>/html/valor/'+codigo;
				        var w2 = window.open('<?= base_url() ?>'+url2,'Cupón','width=800,height=600');
				        <?php if(isset($this->ajustes->cerrarAlImprimirVenta) && $this->ajustes->cerrarAlImprimirVenta == 1): ?>
					        w2.onload = function(){

					        	setTimeout(function(){w2.close()},3000);
					        }        
				    		<?php endif ?>
	        		}
	        	});
	        });
	        


    	<?php endif ?>
	}

	function selCod(codigo,esc,obj){		
		/*venta.addProduct(codigo);		
		$("#inventarioModal").modal('toggle');
		$("#codigoAdd").focus();*/
		$("#inventarioModal").modal('toggle');
		var ht = $("#codigoAdd").val();		
		venta.addProduct(ht+codigo,obj);			
		$("#codigoAdd").focus();
	}

	function _nuevaVenta(){
		venta.initVenta();
		initDatos();
		$(".respuestaVenta").html('').removeClass('alert alert-info alert-danger alert-success');		
		if($("#procesar").css('display')=='block'){
			$("#procesar").modal('toggle');
		}
		$(".btnNueva").hide();
		$("button").attr('disabled',false);
		$("#transaccion").val(1);
		$("#transaccion").chosen().trigger('liszt:updated');
		setCliente({success:true,insert_primary_key:1});
        venta.refresh();
        venta.updateFields();        
		venta.datos.usuario = $("#usuario").val()
		onsend = false;
	}

	function nuevaVenta(){
		if(firstLoad){		
			venta.validateToken().then(()=>{
				_nuevaVenta();
			});
		}else{
			firstLoad = true;
			_nuevaVenta();
		}
	}

	
	function sendVenta(){
		if(typeof(editar)=='undefined'){
			if(!onsend){
				onsend = true;
				var datos =  JSON.parse(JSON.stringify(venta.datos));				
				datos.cheques = JSON.stringify(datos.cheques);
				var productos = [];
				for(var i in datos.productos){
					var p = {};
					p.id = datos.productos[i].id;
					p.codigo = datos.productos[i].codigo;					
					p.cantidad = datos.productos[i].cantidad;
					p.por_desc = datos.productos[i].por_desc;
					p.preciocosto = datos.productos[i].preciocosto;
					p.precio_venta = datos.productos[i].precio_venta;
					p.precio_descuento = datos.productos[i].precio_descuento;
					p.total = datos.productos[i].total;
					p.vencimiento = datos.productos[i].vencimiento;
					productos.push(p);
				}
				datos.productos = JSON.stringify(productos);
				var accion = typeof(editar)=='undefined'?'insert':'update/'+editar.id;
				$("button").attr('disabled',true);
				insertar('movimientos/ventas/ventas/'+accion,datos,'.respuestaVenta',function(data){						
					var id = data.insert_primary_key;						
					var enlace = '';
					$(".respuestaVenta").removeClass('alert-info');
					if(ajustes.abrir_reporte_al_vender==1){
						imprimir(id);
						imprimirCupon(id);
					}
					var options = $('select[name="tipo_facturacion_id"] option');
					enlace = '';
					for(var i=0;i<options.length;i++){
						enlace+= ' | <a href="javascript:imprimir('+id+',\''+$($('select[name="tipo_facturacion_id"] option')[i]).data('url')+'\')">'+$($('select[name="tipo_facturacion_id"] option')[i]).html()+'</a>';
					}	
					<?php if(!empty($this->ajustes->imprimir_cupon) && !empty($this->ajustes->imprimir_cupon_id)): ?>
				        enlace+= ' | <a href="javascript:imprimirCupon('+id+')">Imprimir Cupón</a>';
			    	<?php endif ?>				
					$('.respuestaVenta').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar estos enlaces '+enlace+'</p>');
					$(".btnNueva").show();					
					$(".btnNueva").attr('disabled',false);
					window.shouldSave = false;
					firstLoad = false;

					<?php if($ajustes->plan_credito==1): ?>
						if(venta.datos.transaccion==2){
							document.location.href="<?= base_url() ?>movimientos/creditos/creditos/"+id+'/add';						
						}
					<?php endif ?>	
					if(venta.datos.transaccion==2 && venta.datos.plan=='1'){
						document.location.href="<?= base_url() ?>movimientos/creditos/creditos/"+id+'/add';						
					}				
					/*$(document).on('hidden.bs.modal',"#procesar",function(){
						window.shouldSave = false;
						document.location.reload();
					});*/
				},function(){
					onsend = false;
					$("button").attr('disabled',false);
				});
			}
		}else{
			alert('Edición no permitida');
		}
	}

	function setCliente(data){
		$("#addCliente").modal('hide');
		if(data.success){
			success(".resultClienteAdd",'Cliente añadido con éxito');
			$.post(URI+'maestras/clientes/json_list',{	            
	            'clientes_id':data.insert_primary_key
	        },function(data){       
	            data = JSON.parse(data);   
	            venta.selectClient(data);
	        });
		}
	}

	function toggleDatos(){
		if($(".datosprincipales.oculto").length>0){
			$(".datosprincipales").removeClass('oculto');
		}else{
			$(".datosprincipales").addClass('oculto');
		}
	}

	

	function saldo(){
		$.post(base_url+'movimientos/ventas/saldo',{cliente:$("#cliente").val()},function(data){
			data = JSON.parse(data);
			$("#saldoTag").html(data[0]);
			$("#limiteSaldoTag").html(data[1]);
			$("#saldo").modal('toggle');			
			$("#detallarbtn").attr('href',base_url+'reportes/rep/verReportes/40');
			$("#cobrarbtn").attr('href',base_url+'cajas/admin/abonarDeuda/'+$("#cliente").val());
		});
	}
</script>