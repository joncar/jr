<?php echo $output ?>
<script>
	window.afterLoad.push(function(){
		$(document).on('change','#field-producto,#field-parent_cant',function(){
			var producto = $("#field-producto").val();
			var cantidad_hijo = $("#field-producto option:selected").data('cant');
			var cantidad = $("#field-parent_cant").val();
			if(producto!='' && cantidad!=''){
				var res = parseFloat(cantidad_hijo)*parseFloat(cantidad);
				$("#field-cantidad").val(res);
			}
		});
	});	
</script>