<?php get_instance()->hcss[] = '
	<style>
		a:focus,div:focus,.chzn-container-active{
			border:1px dashed blue;
		}
		body {
	        color: #000;
	        font-weight: 600;
	    }
	    .form-control{
	    	color: #000;
		    font-weight:600;
		    opacity: 1;
		    font-size:15px;
	    }
		.panel{
			border-radius:0px;
		}

		.error{
			border: 1px solid red !important;
		}

		.patternCredito{
			background: #ffffffa6;
			width: 100%;
			height: 100%;
			position: absolute;
			top: 0px;
			left: 0px;
			z-index:10;
		}

		.btnsaldo{
			position: absolute;top: 0;right: 15px;
		}
		@media screen and (max-width:768px){
			.btnsaldo{
				top: -15px;
				font-size:20px;
			}	
		}
		.kt-portlet{
			margin:0;
		}

		.table thead th, .table td{
			padding:6px;
			color: #000;
			font-weight: 600;
		}

		tbody .form-control{
			height:22px;
		}

		.kt-portlet .kt-portlet__head{
			min-height: inherit;
			padding:10px;
		}

		.totales input, .totales .form-control:focus{
			background:lightgreen !important;
			padding: 0 12px;
			border-radius: 0;
		}

		.pagoEfectivo input, .pagoEfectivo .form-control:focus{
			background-color: lightblue;
			border-radius: 0;
			padding: 0 12px;
		}

		.vueltos input{
			background: #e8e800 !important;
			color: red;
			border-radius: 0;
			padding: 0 12px;
		}

		input.guaranies{
			font-weight: bold;
			font-size: 19px;
		}
		div.inp{
			position: relative;
		}
		div.banderas{
			background-image:url('.base_url().'img/banderas.jpg);
			width:20px;
			height:20px;	
			position: absolute;
			right:0px;
			top:0px;
			background-size: 100%;	
		}	
		div.banderas.us{
			background-position: 0 0px;
		}
		div.banderas.ar{
			background-position: 0 -20px;
		}		
		div.banderas.br{
			background-position: 0 -40px;
		}
		div.banderas.py{
			background-position: 0 -60px;
		}

		div.banderas-2x{
			background-image:url('.base_url().'img/banderas.jpg);
			width:40px;
			height:40px;	
			position: absolute;
			right:0px;
			top:0px;
			background-size: 100%;	
		}	
		div.banderas-2x.us{
			background-position: 0 0px;
		}
		div.banderas-2x.ar{
			background-position: 0 -40px;
		}		
		div.banderas-2x.br{
			background-position: 0 -80px;
		}
		div.banderas-2x.py{
			background-position: 0 -120px;
		}
		#procesar th{
			font-weight: bold;
			font-size: 16px;
		}
		.btn-lg, .btn-group-lg > .btn {
		    padding: 0.7rem 1.45rem;
		}
		.caja {
    color: #3c4cca;
    font-family: system-ui;
    font-weight: bold;
    margin-left: 250px;
		}

		@media screen and (min-width:769px){
			/*.cuerpoTransaccion{
				position:relative;
				overflow-x:hidden;
				overflow-y: hidden;
			}

			.cuerpoTransaccion .tarifario{
				position: absolute;
				right: 0;
				top: 0;
				background: #fff;
				border: 1px solid #1e1e2d;
				right: -241px;
				max-width:241px;
				-webkit-transition: 0.5s ease-in-out;
				-moz-transition: 0.5s ease-in-out;
				-o-transition: 0.5s ease-in-out;
				-ms-transition: 0.5s ease-in-out;
				transition: 0.5s ease-in-out;
			}

			.tarifario:hover {
			    right: 0;
			}

			.tarifario .kt-portlet{
				position:relative;
			}

			.tarifarioToggle{
				position: absolute;
				left: -32px;
				background: #fff;
				width: 27px;
				text-align: center;
				border: 1px solid #000;
				top: 0;
			}*/
		}
	</style>
'; 

$venta = $this->querys->getVenta($primary_key);

?>
<?php   
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-add.js?v=1.1"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';
    get_instance()->hcss[] = '<style>.alert{flex-wrap:wrap;} .alert p{width:100%;}</style>';
?>

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $update_url, 'method="post" id="crudForm" onsubmit="insertar(\''.$update_url.'\',this,\'.report\'); return false;" autocomplete="off" enctype="multipart/form-data"'); ?>
		<div class="kt-portlet">
			<div class="kt-portlet__body" style="padding:10px 25px;">
				<div class="kt-section">
					<div class="row" style="position: relative;">
							
						<?php foreach($fields as $field): ?>
		                    <div class="col-6 col-md-3">
		                        <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
		                            <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>
		                            <div>
		                            	<?php echo $input_fields[$field->field_name]->input ?>
		                        	</div>
		                        </div>
		                    </div>
		                <?php endforeach ?>
					</div>
				</div>

			</div>
		</div>


		<div class="row cuerpoTransaccion">
			<div class="col-12 col-sm-9 col-md-9">
				<div class="kt-portlet">
					<div class="kt-portlet__body" style="padding: 0 15px;">
						<div class="kt-section">
							<div style="height:350px; overflow-y:auto; background:#f2f3f8">
								<table class="table table-bordered" id="ventaDescr">
									<thead style="background: #f2f3f8;">
										<tr>
											<th>Código</th>
											<th>Nombre</th>
											<th>Cantidad</th>
											<th>Precio</th>
											<th class="descuento">%Desc</th>
											<th class="descuento">P.Desc</th>
											<?php if($this->ajustes->controlar_vencimiento_stock==1): ?>
												<th>Venc.</th>
											<?php endif ?>
											<th>Total</th>
											<th>Stock</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($venta->productos as $v): $v = (object)$v;?>
											<tr>
												<td tabindex="1"><a href="<?= base_url('movimientos/productos/productos/edit/'.$v->id) ?>" target="_blank"><?= $v->codigo ?></a></td>
												<td><?= $v->nombre ?></td>
												<td><?= $v->cantidad ?></td>
												<td><?= $v->precio_venta ?></td>
												<td><?= $v->por_desc ?></td>
												<td><?= $v->precio_descuento ?></td>
												<?php if($this->ajustes->controlar_vencimiento_stock==1): ?>
												<td>
													<?= @$v->vencimiento ?>
												</td>
												<?php endif ?>
												<td><?= $v->total ?></td>
												<td><?= @$v->stock ?></td>
											</tr>
										<?php endforeach ?>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-md-9">
				<div class="report"></div> 
				<div class="btn-block btn-group-justified" role="group" aria-label="..." style="white-space:nowrap">		  
				  <div class="btn-group" role="group">
				    <a id="newWindow" target="_new" href="<?= base_url('movimientos/ventas/ventas/add') ?>" class="btn btn-warning btn-lg" style="color:#000 !important"><i class="fa fa-window-restore"></i> Nueva pestaña de ventas <span class="alt" style="display:none">(W)</span></a>
				    <button type="submit" class="btn btn-success">Actualizar Encabezado</button>                    
				  </div>
				</div>
			</div>
			
		</div>
	<?php echo form_close(); ?>
</div>


<script>
    var validation_url = '<?php echo $validation_url?>';
    var list_url = '<?php echo $list_url?>';
    var message_alert_add_form = "Almacenado correctamente";
    var message_insert_error = "Error al insertar";
    window.afterLoad.push(function(){
    	$("#field-tipo_facturacion_id").parents('.form-group').find('.chzn-container').remove();
    	let f = $("#field-tipo_facturacion_id").parents('.form-group').find('select');
    	$("#field-tipo_facturacion_id").parents('.form-group').append(f.find('option:selected').html());
    });
</script>

<?php get_instance()->hcss[] = "
    <style>
        .kt-section{
            margin:20px 0;
        }
        .chzn-drop,.chzn-search,.chzn-results,.chzn-search input{
            min-width:120px !important;
        }
        .form-group{
            margin-bottom:5px !important;
        }
        [type='radio']:not(:checked), [type='radio']:checked {
            position: initial;
            left: initial;
            opacity: 1;
            margin-right:5px;
        }
        @media screen and (max-width:480px){
            .nav {
                flex-wrap: wrap;
            }
        }

        .table th, .table td {
          padding: 2px 5px;
          vertical-align: middle;
          border-top: 1px solid #ebedf2;
        }

        .table th {
          background: #fff;
          position: sticky;
          top: 0;
          box-shadow: 0 2px 2px -1px rgba(0,0,0,.4);
          z-index: 1;
          -webkit-box-shadow: -2px 2px 0 -1px #ebedf2;
          -moz-box-shadow: -2px 2px 0 -1px #ebedf2;
          -ms-box-shadow: -2px 2px 0 -1px #ebedf2;
          -o-box-shadow: -2px 2px 0 -1px #ebedf2;
          box-shadow: -2px 2px 0 -1px #ebedf2;
        }
    </style>
"; ?>

<script>
	window.afterLoad.push(function(){
		$("#field-transaccion").parent().hide();
		$("#field-transaccion").parent().after($("#field-transaccion option:selected").html());
	});
</script>	