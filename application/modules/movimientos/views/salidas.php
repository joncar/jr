<script>
<?php 
	if(!empty($edit)){
		$venta = $this->querys->getSalida($edit);
		if($venta){
			echo 'var editar = '.json_encode($venta).';';
		}else{
			echo 'var editar = undefined;';
		}
	}else{
		echo 'var editar = undefined;';
	}
?>
</script>
<style>
	.panel{
		border-radius:0px;
	}

	.error{
		border: 1px solid red !important;
	}

	.patternCredito{
		background: #ffffffa6;width: 100%;height: 100%;position: absolute;top: 0px;left: 0px;
	}
	.btnsaldo{
		position: absolute;top: 0;right: 15px;
	}
	@media screen and (max-width:768px){
		.btnsaldo{
			top: -15px;
			font-size:20px;
		}	
	}
	.kt-portlet{
		margin:0;
	}

	.table thead th, .table td{
		padding:6px;
	}

	tbody .form-control{
		height:22px;
	}

	.kt-portlet .kt-portlet__head{
		min-height: inherit;
		padding:10px;
	}
</style>
<div class="kt-portlet">
	<div class="kt-portlet__body">
		<div class="kt-section">
			<div class="row" style="position: relative;">
				<!--<div style="width:100%; text-align: right; position:absolute; left:0; padding:0 30px">
					#FACTURA: <span id="nroFactura"></span>
				</div>-->
				<div class="col-12 col-md-9">
					<div class="row">
						<div class="col-12 col-md-4">
							Proveedor: 
							<?php 						
								echo form_dropdown_from_query('proveedor','proveedores','id','denominacion',!empty($venta)?$venta->proveedor:1,'id="field-proveedor"') 
							?>					
						</div>
						<div class="col-12 col-md-4">
							Motivo: 
							<?php 
								echo form_dropdown_from_query('motivo',$this->db->get('motivo_salida'),'id','denominacion',!empty($venta)?$venta->motivo:1,'id="field-motivo"')
							?>
						</div>
						<div class="col-12 col-md-4">
							Observaciones: 
							<?php 
								echo form_input('observacion',@$edit->observacion,'id="field-observacion" class="form-control"')
							?>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-3" style="position: relative;">
					<span style="position: absolute;top: 26px;left: 30px;font-weight: bold;">Total Costo: </span>
					<input type="text" name="total_venta" class="form-control" readonly="" value="300.000" style="font-size:30px; font-weight:bold; text-align: right;height: 89px;vertical-align: baseline;margin-top: 19px;">
				</div>

			</div>
		</div>

	</div>
</div>
<div id="responseSalida"></div>
<div class="row">
	<div class="col-12 col-md-9" style="padding-right: 39px;">
		<div class="kt-portlet">
			<div class="kt-portlet__body">
				<div class="kt-section">
					<div style="height:200px; overflow-y:auto">
						<table class="table table-bordered" id="ventaDescr">
							<thead>
								<tr>
									<th>Código</th>
									<th>Nombre</th>
									<th>Cantidad</th>
									<th>Costo</th>
									<?php if($this->ajustes->controlar_vencimiento_stock==1): ?>
										<th>Venc.</th>
									<?php endif ?>
									<th>Total</th>
									<th>Sucursal</th>
									<th>Stock</th>
								</tr>
							</thead>
							<tbody>

								<tr id="productoVacio">
									<td>
										<a href="javascript:void(0)" class="rem" style="display:none;color:red">
											<i class="fa fa-times"></i>
										</a> 
										<span>&nbsp;</span>
									</td>
									<td>&nbsp;</td>
									<td><input name="cantidad" class="form-control cantidad" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td><input name="costo" class="form-control costo" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0"></td>
									<?php if($this->ajustes->controlar_vencimiento_stock==1): ?>
									<td>
										<select name="vencimiento" class="form-control vencimiento" style="width:90px;text-align: right;padding: 0 6px;"></select>
									</td>
									<?php endif ?>
									<td><input name="total" class="form-control total" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td><?php echo form_dropdown_from_query('sucursal','sucursales','id','denominacion',$this->user->sucursal,'style="width:50px;text-align: right;padding: 0 6px;"',FALSE) ?></td>
									<td>&nbsp;</td>
								</tr>

								
							</tbody>
						</table>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-md-2" style="text-align: center;padding: 6px;">
						Cant: <span id="cantidadProductos">4</span>					
					</div>
					<div class="col-12 col-md-10" style="position: relative;">
						<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
						<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" style="padding-left: 25px;padding-right: 73px;">
						<button style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar insertar">Insertar</button>

						<div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-12 col-md-3" style="padding-left: 5px;">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						<b>Resumen de salida</b>
					</h1>
				</div>
			</div>
			<div class="kt-portlet__head">
				<div class="row">
					<div class="col-12 col-md-4">Pesos: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_pesos" value="300.000" readonly="">
					</div>
					<div class="col-12 col-md-4">Reales: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_reales" value="300.000" readonly="">
					</div>
					<div class="col-12 col-md-4">Dolares: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_dolares" value="300.000" readonly="">
					</div>
				</div>
			</div>
		</div>

		
	</div>
</div>

<div class="row">
	<div class="col-12 col-md-9">
		<div class="btn-group btn-group-justified" role="group" aria-label="...">
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" onclick="nuevaVenta();" style="color:#000 !important"><i class="fa fa-plus-circle"></i> Nueva salida</button>
		  </div>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary" onclick="sendVenta()"><i class="fa fa-floppy-o"></i> Procesar salida</button>
		  </div>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" <?= empty($edit)?'disabled="true"':'' ?> style="color:#000 !important"><i class="fa fa-print"></i> Imprimir</button>
		  </div>
		</div>
	</div>
	<div class="col-12 col-md-3">
		<!--<div class="kt-portlet" style="margin-top:-130px">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						<b>Atajos</b>
					</h1>
				</div>
			</div>
			<div class="kt-portlet__body">
				<span>(ALT+C) <small>Enfocar busqueda por código</small></span><br/>
				<span>(ALT+I) <small>Mostrar busqueda avanzada</small></span><br/>
				<span>(ALT+P) <small>Procesar venta</small></span><br/>
				<span>(ALT+N) <small>Nueva venta</small></span><br/>
				<span>(ALT+B) <small>Consultar saldo del cliente</small></span><br/>
			</div>
		</div>-->
	</div>
</div>


<?php get_instance()->js[] = '<script src="'.base_url().'js/salidas.js?cache='.time().'"></script>'; ?>
<?php $this->load->view('_inventario_modal',array(),FALSE,'movimientos'); ?>

<script>	
	<?php
		$ajustes = $this->ajustes;
	?>
	var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;
	var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
	var tasa_real = <?= $ajustes->tasa_reales ?>;
	var tasa_peso = <?= $ajustes->tasa_pesos ?>;
	var codigo_balanza = <?= json_encode(explode(',',str_replace(', ',',',$ajustes->cod_balanza))) ?>;
	var modPrecio = <?= $ajustes->permitir_modificar_precio_en_ventas ?>;
	var cantidades_mayoristas = <?php 
        $ar = array();
        foreach($this->db->get('cantidades_mayoristas')->result() as $c){
            $ar[] = $c;
        }
        echo json_encode($ar);
	?>;
	var vender_sin_stock = <?= $ajustes->vender_sin_stock ?>;
	var onsend = false;
	var controlar_vencimiento = <?= $ajustes->controlar_vencimiento_stock ?>;

	window.shouldSave = true;	
	window.addEventListener('beforeunload',function(e){
		if (window.shouldSave) {
	        e.preventDefault();
	        e.returnValue = '';
	        return;
	    }
	    delete e['returnValue'];
	});

	window.afterLoad.push(function(){
		window.venta = new PuntoDeVenta();	
		if(editar!=undefined){		
			venta.setDatos(editar);
		}

		if(!editar){
			initDatos();
			venta.initEvents();
			venta.updateFields();
		}

		$(document).on('shown.bs.modal',"#procesar",function(){
			$("#procesar input[name='pago_guaranies']").focus();
		});
	});
	

	function initDatos(){
		venta.datos.sucursal = '<?= $this->user->sucursal ?>';
		venta.datos.caja = '<?= $this->user->caja ?>';
		venta.datos.cajadiaria = '<?= $this->user->cajadiaria ?>';
		venta.datos.fecha = '<?= date("Y-m-d H:i") ?>';	
		venta.datos.usuario = '<?= $this->user->id ?>';		
		venta.modPrecio = modPrecio;
	}

		function imprimir(codigo){		
        window.open('<?= base_url() ?>reportes/rep/verReportes/150/html/valor/'+codigo);
	}

	function selCod(codigo,esc,obj){		
		/*venta.addProduct(codigo);		
		$("#inventarioModal").modal('toggle');*/
		$("#inventarioModal").modal('toggle');
		var ht = $("#codigoAdd").val();		
		venta.addProduct(ht+codigo,obj);			
		$("#codigoAdd").focus();
	}

	function nuevaVenta(){		
		venta.initVenta();
		initDatos();
		$("#responseSalida").html('').removeClass('alert alert-info alert-danger alert-success');		
		if($("#procesar").css('display')=='block'){
			$("#procesar").modal('toggle');
		}
		$(".btnNueva").hide();
		$("button").attr('disabled',false);
		$("#transaccion").val(1);
		$("#transaccion").chosen().trigger('liszt:updated');
		$.post(URI+'maestras/clientes/json_list',{   
            search_field:'id',     
            search_text:'1',
            operator:'where'
        },function(data){       
            data = JSON.parse(data);   
            venta.selectClient(data);
        });
		onsend = false;
	}

	
	function sendVenta(){
		if(typeof(editar)=='undefined'){
			if(!onsend){
				onsend = true;
				var datos =  JSON.parse(JSON.stringify(venta.datos));
				datos.productos = JSON.stringify(datos.productos);
				var accion = typeof(editar)=='undefined'?'insert':'update/'+editar.id;
				$("button").attr('disabled',true);
				insertar('movimientos/salidas/salidas/'+accion,datos,'#responseSalida',function(data){						
					var id = data.insert_primary_key;						
					var enlace = '';
					$("#responseSalida").removeClass('alert-info');
					imprimir(id);
					enlace = 'javascript:imprimir('+id+')';
					$('#responseSalida').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar <a href="'+enlace+'">este enlace</a></p>');
					$(".btnNueva").show();
					$(".btnNueva").attr('disabled',false);					
					onsend = false;
				},function(){
					onsend = false;
					$("button").attr('disabled',false);
				});
			}
		}else{
			alert('Edición no permitida');
		}
	}

	function setCliente(data){
		if(data.success){
			$(".resultClienteAdd").html('<div class="alert alert-success">Cliente añadido con éxito</div>');
			$.post(URI+'maestras/clientes/json_list',{   
	            search_field:'id',     
	            search_text:data.insert_primary_key,
	            operator:'where'
	        },function(data){       
	            data = JSON.parse(data);   
	            venta.selectClient(data);
	        });
		}
	}

	

	function saldo(){
		$.post(base_url+'movimientos/ventas/saldo',{cliente:$("#cliente").val()},function(data){
			$("#saldoTag").html(data);
			$("#saldo").modal('toggle');
		});
	}
</script>