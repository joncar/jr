<?= $output ?>
<script>
	var cheques = '';
	window.afterLoad.push(function(){
		cheques = $("#nro_cheque_field_box,#tipo_cheque_field_box,#fecha_emision_field_box,#fecha_pago_field_box");
		$("#field-forma_pago").on('change',function(){
            var forma = $(this).val();            
            cheques.parent().hide();
            switch(forma){                
                case 'Cheque':
                    cheques.parent().show();
                break;
            }
        });
        $("#field-tipo_movimiento").on('change',function(){
            if($(this).val()=='1'){
            	$("#field-beneficiario").val($("#field-cuentas_bancos_id option:selected").html());
            }else{
            	$("#field-beneficiario").val('');
            }
        });
        $("#field-forma_pago").change();

        $(".flexigrid > form").on('afterSend',function(e,primary){
            window.open('<?= base_url() ?>reportes/rep/verReportes/218/html/valor/'+primary);
        });
	});
</script>