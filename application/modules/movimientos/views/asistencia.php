<?php 
	get_instance()->hcss[] = "
		<style>
			.datos{
				text-align: center;
			}
			.fotoContent{
				display: inline-block;
				padding: 30px;
				overflow: hidden;
				width: 170px;
				height: 170px;
			}
			#nombre{
				margin-top:-14px;
				font-size: 28px;
			}
		}
		</style>
	";
?>
<div class="kt-portlet">
	<div class="kt-portlet__body" style="padding:10px 25px;">
		<div class="kt-section">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						<b>Registrar asistencia</b>
					</h1>
				</div>
			</div>
			
			
			<form id="formulario" class="form" action="movimientos/asistencia/asistencia" method="post" onsubmit="actualizar(this,'.loginResponse'); return false;">
                <div class="loginResponse" style="font-size:36px"></div>
                <div class="box-body">
                	<div class="row">
  						<div class="col-8">
		                  <div class="form-group">
		                    <label>Cédula</label>
		                    <div class="input-group mb-3">
		                      <div class="input-group-prepend">
		                        <span class="input-group-text"><i class="fa fa-user"></i></span>
		                      </div>
		                      <input type="text" class="form-control" placeholder="Cédula" name="cedula" style="font-size: 34px;" size="7" maxlength="7">
		                    </div>
		                  </div>		                  
		                  <button type="submit" class="btn btn-rounded btn-primary btn-outline">
		                    <i class="ti-unlock"></i> Registrar
		                  </button>
		                  <div id="fecha" style="display: inline-block;font-size: 50px;vertical-align: middle;"></div>
		                </div>
		                <div class="col-4 datos">
		                  <h1 class="horaSistema"></h1>
						  <div class="fotoContent">
						    <img id="foto" src="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/cropper/vacio.png" style="width: 100%;">
						  </div>
						  <div id="nombre">
						    Nombre y apellido
						  </div>
					    </div>
		            </div>
                </div>
            </form>
		</div>
	</div>
</div>

<script>
	window.onsend = false;	
	window.afterLoad.push(function(){
		window.pictureEmpty = $("#foto").attr('src');
		$('input[name="cedula"]').on('keyup',function(){
			var val = $(this).val();
			console.log(val.length);
			if(!window.onsend && val.length>=7){
				$("#formulario").submit();
			}
		});
	});
	window.onsend = false;
	function actualizar(form,div){
		if(!window.onsend){
			window.onsend = true;
			sendForm(form,div,function(data){
				data = JSON.parse(data);
				$(div).html(data.msj);
				if(data.success){
					$("#foto").attr('src',data.user.foto);
					$("#nombre").html(data.user.nombre+' '+data.user.apellido);
					$("#fecha").html(data.hora);
					setTimeout(function(){
						$("#foto").attr('src',window.pictureEmpty);
						$("#nombre").html('');
						$("#fecha").html('');
						$(".loginResponse").html('');
					},2000);
				}
				$("input").val('');
				window.onsend = false;
			});
		}
	}
</script>