<?php if($this->user->admin==1): ?>
	<a href="<?= base_url('movimientos/compras/compradetalles') ?>" class="btn btn-info">Ver más detalles</a>
<?php endif ?>
<?= $output ?>

<div class="modal fade modalEmergente" id="comprasdetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body modal-lg p-0">
				data
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
	function showDetail(id){
		$.post('<?= base_url('movimientos/compras/compras_detail/') ?>/'+id+'/',{},function(data){
			$("#comprasdetail .modal-body").html(data);
			$("#comprasdetail .filtering_form").trigger('submit');
			$("#comprasdetail").modal('show');
		});
	}

	function anular(id){
        if(confirm('Seguro que desea anular esta factura?')){
            $.post('<?= base_url('movimientos/compras/compras/anular') ?>',{id:id},function(data){
                emergente(data);
            });
        }
    }
</script>