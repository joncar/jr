<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <a href="<?= base_url('movimientos/facturas_clientes/facturas_clientes_detalles/'.$cliente.'/') ?>" class="nav-link <?= $act==''?'active':'' ?>">Todos</a>
  </li>
  <li class="nav-item" role="presentation">
    <a href="<?= base_url('movimientos/facturas_clientes/facturas_clientes_detalles/'.$cliente.'/1') ?>" class="nav-link <?= $act=='1'?'active':'' ?>" id="profile-tab">Pendientes</a>
  </li>
  <li class="nav-item" role="presentation">
    <a href="<?= base_url('movimientos/facturas_clientes/facturas_clientes_detalles/'.$cliente.'/2') ?>" class="nav-link <?= $act=='2'?'active':'' ?>" id="contact-tab">Pagados</a>
  </li>
  <li class="nav-item" role="presentation">
    <a href="<?= base_url('movimientos/facturas_clientes/facturas_clientes_detalles/'.$cliente.'/3') ?>" class="nav-link <?= $act=='3'?'active':'' ?>" id="contact-tab">Pagaré</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
  	<?= $output ?>
  </div>
</div>
<?php $this->load->view('_pagare_pagos_modal'); ?>