<div class="card">
	<div class="card-body p-0">
		<form action="movimientos/ventas_mantenimiento/ventas_mantenimiento/correlativo" onsubmit="sendForm(this,'.respose'); return false;">
			<div class="row m-0">			
				<div class="col-12 col-md-3">
					<div class="form-group">
						<label for="correlativo">#Correlativo</label>
						<input type="text" name="correlativo" class="form-control" value="<?= $this->db->get_where('cajadiaria',['id'=>$this->user->cajadiaria])->row()->correlativo ?>">					
					</div>
				</div>								
				<div class="col-12 col-md-2">
					<label for="" style="visibility: hidden">&nbsp;</label>
					<button type="submit" class="btn btn-success btn-block">Ajustar</button>
				</div>
				<div class="col-12">
					<div class="respose"></div>
				</div>
			</div>
		</form>
	</div>
</div>
<?= $output ?>

<script>
	function showDetail(id){
		$.post('<?= base_url('movimientos/ventas/ventas_detail/') ?>/'+id+'/',{},function(data){
			emergente(data);
		});
	}

	window.afterLoad.push(function(){		
		$(document).on('change',"#filtering_form tbody input.nro,#filtering_form tbody select",function(){
			var tr = $(this).parents('tr');		
			var data = {
				nro: tr.find('.nro').val(),
				fac: tr.find('select').val(),
				id:  tr.find('input[type="checkbox"]').val(),
			};
		  	$.post(window.URI+'movimientos/ventas_mantenimiento/ventas_mantenimiento/upd',data,function(data){
				if(data!='success'){
					alert(data);
				}
			});
		});
	});
</script>