<?php
	if(!empty($edit) && is_numeric($edit)){
		$compra = $this->elements->compras(array('compras.id'=>$edit),null,false)[0];
	}
?>
<?php get_instance()->hcss[] = '
<style>
	a:focus,div:focus,.chzn-container-active{
		border:1px dashed blue;
	}
	body {
        color: #000;
        font-weight: 600;
    }
    .form-control{
    	color: #000;
	    font-weight:600;
	    opacity: 1;
	    font-size:15px;
    }
	.panel{
		border-radius:0px;
	}

	.error{
		border: 1px solid red !important;
	}

	.patternCredito{
		background: #ffffffa6;width: 100%;height: 100%;position: absolute;top: 0px;left: 0px;
	}

	.btnsaldo{
		position: absolute;top: 0;right: 15px;
	}
	@media screen and (max-width:768px){
		.btnsaldo{
			top: -15px;
			font-size:20px;
		}	
	}
	.kt-portlet{
		margin:0;
	}

	.table thead th, .table td{
		padding:6px;
		color: #000;
		font-weight: 600;
	}

	tbody .form-control{
		height:22px;
	}

	.kt-portlet .kt-portlet__head{
		min-height: inherit;
		padding:10px;
	}

	.totales input, .totales .form-control:focus{
		background:lightgreen !important;
		padding: 0 12px;
		border-radius: 0;
	}

	.pagoEfectivo input, .pagoEfectivo .form-control:focus{
		background-color: lightblue;
		border-radius: 0;
		padding: 0 12px;
	}

	.vueltos input{
		background: #e8e800 !important;
		color: red;
		border-radius: 0;
		padding: 0 12px;
	}

	input.guaranies{
		font-weight: bold;
		font-size: 19px;
	}
	div.inp{
		position: relative;
	}
	div.banderas{
		background-image:url('.base_url().'img/banderas.jpg);
		width:20px;
		height:20px;	
		position: absolute;
		right:0px;
		top:0px;
		background-size: 100%;	
	}	
	div.banderas.us{
		background-position: 0 0px;
	}
	div.banderas.ar{
		background-position: 0 -20px;
	}		
	div.banderas.br{
		background-position: 0 -40px;
	}
	div.banderas.py{
		background-position: 0 -60px;
	}

	div.banderas-2x{
		background-image:url('.base_url().'img/banderas.jpg);
		width:40px;
		height:40px;	
		position: absolute;
		right:0px;
		top:0px;
		background-size: 100%;	
	}	
	div.banderas-2x.us{
		background-position: 0 0px;
	}
	div.banderas-2x.ar{
		background-position: 0 -40px;
	}		
	div.banderas-2x.br{
		background-position: 0 -80px;
	}
	div.banderas-2x.py{
		background-position: 0 -120px;
	}
	#procesar th{
		font-weight: bold;
		font-size: 16px;
	}
	.btn-lg, .btn-group-lg > .btn {
	    padding: 0.7rem 1.45rem;
	}

	@media screen and (min-width:769px){
		.cuerpoTransaccion{
			position:relative;
			overflow-x:hidden;
		}

		.cuerpoTransaccion .tarifario{
			position: absolute;
			right: 0;
			top: 50px;
			background: #fff;
			border: 1px solid #1e1e2d;
			right: -241px;
			max-width:241px;
			-webkit-transition: 0.5s ease-in-out;
			-moz-transition: 0.5s ease-in-out;
			-o-transition: 0.5s ease-in-out;
			-ms-transition: 0.5s ease-in-out;
			transition: 0.5s ease-in-out;
		}

		.tarifario:hover {
		    right: 0;
		}

		.tarifario .kt-portlet{
			position:relative;
		}

		.tarifarioToggle{
			position: absolute;
			left: -37px;
			background: #fff;
			width: 27px;
			text-align: center;
			border: 1px solid #000;
			top: 0;
		}
	}
</style>
'; 
?>
<div class="kt-portlet">
	<div class="kt-portlet__body" style="padding:10px 25px;">
		<div class="kt-section">
			<div class="row" style="position: relative;">
				<!--<div style="width:100%; text-align: right; position:absolute; left:0; padding:0 30px">
					#FACTURA: <span id="nroFactura"></span>
				</div>-->
				<div class="col-12 col-md-10">
					<div class="row">
						<div class="col-12 col-md-3">
							#Factura: 
							<div class="form-control">
								<?php 																	
									echo $compra->nro_factura;
								?>
							</div>							
						</div>
						<div class="col-12 col-md-2">
							Timbrado: 
							<div class="form-control">
								<?php 																	
									echo $compra->timbrado;
								?>
							</div>							
						</div>
						<div class="col-12 col-md-3">
							Sucursal:
							<div class="form-control">
								<?php 								
									$c = $this->db->get_where('sucursales',['id'=>$compra->sucursal])->row();
									echo $c->denominacion;
								?>
							</div>
						</div>
						
						<div class="col-12 col-md-2">
							Tipo facturación: 
							<div class="form-control">
								<?php 								
									$c = $this->db->get_where('tipo_facturacion',['id'=>$compra->tipo_facturacion_id])->row();
									echo $c->denominacion;
								?>
							</div>							
						</div>
						
						<div class="col-12 col-md-2">
							Forma de Pago: 
							<div class="form-control">
								<?php 								
									$c = $this->db->get_where('formapago',['id'=>$compra->forma_pago])->row();
									echo $c->denominacion;
								?>
							</div>
						</div>

						
						<div class="col-12 col-md-3">
							Transacción: 
							<div class="form-control">
								<?php 								
									$c = $this->db->get_where('tipotransaccion',['id'=>$compra->transaccion])->row();
									echo $c->denominacion;
								?>
							</div>
						</div>
						
						<div class="col-12 col-md-3">
							Proveedor: 
							<div class="form-control">
								<?php 								
									$c = $this->db->get_where('proveedores',['id'=>$compra->proveedor])->row();
									echo $c->ruc.' '.$c->denominacion;
								?>
							</div>
						</div>
						<div class="col-12 col-md-2">
							Fecha: 
							<div class="form-control">
								<?php 																	
									echo $compra->fecha;
								?>
							</div>
						</div>	
						<div class="col-12 col-md-2">
							Vencimiento Pago: 
							<div class="form-control">
								<?php 																	
									echo $compra->vencimiento_pago;
								?>
							</div>
						</div>
						<div class="col-12 col-md-2">
							Tipo de Egreso: 
							<div class="form-control">
								<?php 																	
									echo array('0'=>'Sin Pago','1'=>'Caja Diaria','2'=>'Caja Grande','3'=>'Bonificación')[$compra->pago_caja_diaria];
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-2" style="position: relative;">
					<span style="position: absolute;top: 26px;left: 30px;font-weight: bold;">Total Compra: </span>
					<input type="text" name="total_compra" class="form-control" readonly="" value="<?= number_format($compra->total_compra,0,',','.') ?>" style="font-size:30px; font-weight:bold; text-align: right;height: 89px;vertical-align: baseline;margin-top: 19px; background:lightgreen">
				</div>

			</div>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-12 respuestaVenta"></div>
</div>

<div class="row cuerpoTransaccion">
	<div class="col-12 col-sm-10 col-md-12">
		<div class="kt-portlet">
			<div class="kt-portlet__body" style="padding-top:0">
				<div class="kt-section">
					<div style="height:350px; overflow-y:auto">
						<table class="table table-bordered" id="ventaDescr">
							<thead style="background: #f2f3f8;">
								<tr>
									<th>Código</th>
									<th>Nombre</th>
									<th>Cantidad</th>
									<th>P.Costo</th>
									<th>%Desc</th>
									<th>%Venta</th>
									<th>P.Venta</th>
									<th>Total</th>
									<th>Venc.</th>
									<th>Stock</th>
									<th>IVA</th>
								</tr>
							</thead>
							<tbody>								
								<?php foreach($compra->productos as $v): $v = (object)$v;?>
									<tr>
										<td tabindex="1"><a href="<?= base_url('movimientos/productos/productos/edit/'.$v->id) ?>" target="_blank"><?= $v->codigo ?></a></td>
										<td tabindex="2"><?= $v->nombre_comercial ?></td>
										<td tabindex="3"><?= $v->cantidad ?></td>
										<td tabindex="4"><?= $v->precio_costo ?></td>
										<td tabindex="5"><?= $v->por_desc ?></td>
										<td tabindex="6"><?= $v->por_venta ?></td>
										<td tabindex="7"><?= $v->precio_venta ?></td>
										<td tabindex="8"><?= $v->total ?></td>
										<td tabindex="9"><?= $v->vencimiento ?></td>
										<td tabindex="10"><?= $v->stock ?></td>
										<td tabindex="11"><?= $v->iva_id ?></td>
									</tr>
								<?php endforeach ?>

								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-12 col-md-10">
		<div class="btn-group btn-group-justified" role="group" aria-label="...">		  
		  <div class="btn-group" role="group">
	    	<a href="<?= base_url() ?>movimientos/compras/compras/add" class="btn btn-success" style="color:#000 !important"><i class="fa fa-plus-circle"></i> Nueva compra</a>
	  	  </div>
		</div>
	</div>
</div>