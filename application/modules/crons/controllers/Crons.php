<?php
require_once APPPATH.'/controllers/Panel.php';  
class Crons extends Main {

    function __construct() {
        parent::__construct();
    }
    
    public function ver($x = '') {        
        $this->loadView(array(
            'view'=>'panel',
            'crud'=>'user',
            'output'=>$this->load->view($x,array(),TRUE)
        ));
    }

    function actualizar_saldos(){
    	$this->db->limit(1);
        $need = $this->db->get_where('facturas_clientes',['actualizar_saldo'=>1]);
        foreach($need->result() as $n){
            $this->db->query("CALL ajustSaldoFromVentaCredito({$n->id})");
            $this->db->update('facturas_clientes',['actualizar_saldo'=>0],['id'=>$n->id]);
        }
        echo 'Actualizado';
    }

}