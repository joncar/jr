<script>
	window.afterLoad.push(function(){
		$("#field-cant_hora,#field-monto_porhora").on('change',function(){
			var cant = parseFloat($("#field-cant_hora").val());
			var monto = parseFloat($("#field-monto_porhora").val());
			var total = 0;
			if(!isNaN(cant) && !isNaN(monto)){
				total = monto*cant;
			}
			$("#field-total_monto").val(total);
		});
	});
</script>