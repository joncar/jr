<ul class="nav nav-tabs m-0" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link <?= !is_numeric($x)?'active':'' ?>" id="home-tab" href="<?= base_url('rrhh/pago_empleados') ?>">Pendientes</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link <?= is_numeric($x)?'active':'' ?>" id="profile-tab" href="<?= base_url('rrhh/pago_empleados/1') ?>">Pagados</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
  	<?= $output ?>
  </div>
</div>


<script>
	window.afterLoad.push(function(){
		$("#field-empleados_id").on('change',function(){
			$.post(window.URI+'rrhh/empleados/json_list',{
				id:$(this).val()
			},function(data){
				data = JSON.parse(data);
				if(data.length>0){
					data = data[0];
				}
				$("#field-salario").val(data.monto_salario);
				$("#field-ips").val(data.ips);
				$("#field-periodo_pago_id").val(data.periodo_pago).chosen().trigger('liszt:updated');
				$("#field-fecha_pago").val('<?= date("d/m/Y") ?>');
			});
		});

		$("#field-salario,#field-ips,#field-sanciones_descuentos,#field-saldo_credito,#field-otros_ingresos,#field-bonificaciones,#field-horas_extras,#field-adelanto_empleados").on('change',totalizar);

		$("#field-desde").on('change',function(){
			var periodo = $("#field-periodo_pago_id").val();
			var dias = 0;
			switch(periodo){
				case '1':
					dias = 6;
				break;
				case '2':
					dias = 14;
				break;
				case '4':
					dias = 29;
				break;
			}
			var fecha = $(this).val();
			if(fecha!=''){
				var desde = new Date(fecha+' 00:00:00');
				desde.setDate(desde.getDate() + dias);
				var hasta = desde.getFullYear()+'-';
				hasta+= ((desde.getMonth()+1)<10?'0'+(desde.getMonth()+1):(desde.getMonth()+1))+'-';
				hasta+= ((desde.getDate())<10?'0'+(desde.getDate()):(desde.getDate()));				
				$("#field-hasta").val(hasta);
			}	
			getAsignaciones();
		});


		var bancos = $("#bancos_id_field_box,#cuentas_bancos_id_field_box");
		var cheques = $("#nro_cheque_field_box,#tipo_cheque_field_box,#fecha_emision_field_box");
		bancos.parent().hide();
		cheques.parent().hide();
		$("#field-forma_pago").on('change',function(){
		    var forma = $(this).val();
		    bancos.parent().hide();
		    cheques.parent().hide();
		    switch(forma){
		        case 'Transferencia':
		            bancos.parent().show();
		        break;
		        case 'Deposito':
		            bancos.parent().show();
		        break;
		        case 'Cheque':
		            bancos.parent().show();
		            cheques.parent().show();
		        break;
		    }
		});

		$("#cancel-button").after('<a href="#" disabled="true" id="imprimir" target="_blank" class="btn btn-info"><i class="fa fa-print"></i></a>');
		<?php if($x=='edit' && is_numeric($y)): ?>
			var href = '<?= base_url() ?>reportes/rep/verReportes/230/html/valor/<?= $y ?>';
			$("#imprimir").attr({'href':href,'disabled':false});
		<?php endif ?>

		$(document).on('click','a',function(e){
			if($(e.target).attr('href').indexOf('pagar')>-1){
				e.preventDefault();
				window.open(e.target);
			}
		});
	});

	function getAsignaciones(){
		var desde,hasta;
		desde = $("#field-desde").val();
		hasta = $("#field-hasta").val();

		if(desde!='' && hasta!=''){
			$.post(window.URI+'rrhh/pago_empleados/getAsignaciones/'+$("#field-empleados_id").val(),{
				desde:desde,
				hasta:hasta,
			},function(data){
				data = JSON.parse(data);
				$("#field-sanciones_descuentos").val(parseFloat(data.sanciones));
				$("#field-saldo_credito").val(parseFloat(data.saldo_credito));
				$("#field-otros_ingresos").val(parseFloat(data.otros_ingresos));
				$("#field-bonificacion").val(parseFloat(data.bonificaciones));
				$("#field-horas_extras").val(parseFloat(data.horas_extras));
				$("#field-adelanto_empleados").val(parseFloat(data.adelanto_empleados));
				totalizar();
			});
		}
	}

	function totalizar(){
		var salario = parseFloat($("#field-salario").val());
		var ips = parseFloat($("#field-ips").val());
		var sanciones_descuentos = parseFloat($("#field-sanciones_descuentos").val());
		var saldo_credito = parseFloat($("#field-saldo_credito").val());
		var otros_ingresos = parseFloat($("#field-otros_ingresos").val());
		var bonificacion = parseFloat($("#field-bonificacion").val());
		var horas_extras = parseFloat($("#field-horas_extras").val());
		var adelanto_empleados = parseFloat($("#field-adelanto_empleados").val());
		sanciones_descuentos = isNaN(sanciones_descuentos)?0:sanciones_descuentos;
		saldo_credito = isNaN(saldo_credito)?0:saldo_credito;
		otros_ingresos = isNaN(otros_ingresos)?0:otros_ingresos;
		bonificacion = isNaN(bonificacion)?0:bonificacion;
		horas_extras = isNaN(horas_extras)?0:horas_extras;
		adelanto_empleados = isNaN(adelanto_empleados)?0:adelanto_empleados;
		var total = 0;
		if(
			!isNaN(salario) && 
			!isNaN(ips) && 
			!isNaN(sanciones_descuentos) && 
			!isNaN(saldo_credito) && 
			!isNaN(otros_ingresos) && 
			!isNaN(bonificacion) && 
			!isNaN(horas_extras) &&
			!isNaN(adelanto_empleados)			
		){
			total = salario+otros_ingresos+bonificacion+horas_extras;
			total-= sanciones_descuentos+saldo_credito+ips+adelanto_empleados;
		}

		$("#field-total_a_cobrar").val(total);
	}

	function imprimir(codigo){
		var href = '<?= base_url() ?>reportes/rep/verReportes/230/html/valor/'+codigo;
		$("#imprimir").attr({'href':href,'disabled':false});
        window.open(href,'Impresion','width=800,height=600');        
	}
</script>	