<script>
window.afterLoad.push(function(){
	var bancos = $("#bancos_id_field_box,#cuentas_bancos_id_field_box");
	var cheques = $("#nro_cheque_field_box,#tipo_cheque_field_box,#fecha_emision_field_box,#fecha_pago_field_box");
	bancos.parent().hide();
	cheques.parent().hide();
	$("#field-forma_pago").on('change',function(){
	    var forma = $(this).val();
	    bancos.parent().hide();
	    cheques.parent().hide();
	    switch(forma){
	        case 'Transferencia':
	            bancos.parent().show();
	        break;
	        case 'Deposito':
	            bancos.parent().show();
	        break;
	        case 'Cheque':
	            bancos.parent().show();
	            cheques.parent().show();
	        break;
	    }
	});
});
</script>