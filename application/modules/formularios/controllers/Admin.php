<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{   
    	protected $prefix = 'formularios_'; 	
        function __construct() {
            parent::__construct();            
        }

        function formularios(){
            $crud = $this->crud_function('','');
            $crud->columns('nombre','slug');     
            $crud->field_type('mostrar_columnas','string');       
            $crud->add_action('Mostrar tabla','',base_url('formularios/admin/mostrar').'/');            
            $crud->add_action('Campos','',base_url('formularios/admin/formularios_campos').'/');            
            $crud->add_action('Generar tabla','',base_url('formularios/admin/generar').'/');            
            $crud->add_action('Vaciar tabla','',base_url('formularios/admin/vaciar').'/');            
            if($crud->getParameters()!='list'){
            	$crud->set_lang_string('insert_success_message','Datos almacenados con éxito <script>document.location.href="'.base_url('formularios/admin/formularios_campos/{id}').'";</script>');            
        	}
            if($crud->getParameters()=='add'){
            	$crud->set_rules('nombre','Nombre de tabla','required|callback_ifTableExists');
        	}
        	$crud->callback_before_delete(function($primary){
        		$primary = get_instance()->db->get_where('formularios',['id'=>$primary]);
        		if($primary->num_rows()>0){
        			$primary = $primary->row();
        			$datos = $this->db->get_where(get_instance()->prefix.$primary->nombre);
        			if($datos->num_rows()==0){
        				$this->db->delete('formularios_campos',['formularios_id'=>$primary->id]);
        				$this->db->query('DROP TABLE IF EXISTS '.$primary->nombre);
        				return true;
        			}
        		}	
        		return false;
        	});
        	$crud->callback_column('slug',function($val,$row){
        		return 'formularios/admin/mostrar/'.$row->nombre;
        	});
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function ifTableExists($val){
        	if(!preg_match('/^[0-9a-z_\d]+$/', $val)){
        		$this->form_validation->set_message('ifTableExists','Formato de nombre incorrecto solo son aceptados los caracteres "09az_"');
        		return false;
        	} 
        	$existe = $this->db->table_exists($this->$prefix.$val);
        	if($existe){
        		$this->form_validation->set_message('ifTableExists','Ya existe una tabla creada con ese nombre');
        		return false;
        	}
        	return true;
        }
        function formularios_campos($x = ''){
            $crud = $this->crud_function('','');
            $crud->where('formularios_id',$x)
            	 ->field_type('formularios_id','hidden',$x)
            	 ->field_type('valores','string')
            	 ->field_type('tipo','dropdown',[
            	 	'dropdown'=>'Selección [Param1,Param2,...]',
            	 	'set'=>'Selección multiple [Param1,Param2,...]',
            	 	'string'=>'Text',
            	 	'date'=>'Fecha',
            	 	'text'=>'Editor de texto',
            	 	'true_false'=>'Verdadero Falso [FALSE,TRUE]',
            	 	'hidden'=>'Oculto',
            	 	'integer'=>'Numero',
            	 	'password'=>'Contraseña',
            	 	'relation'=>'Relación [TABLA,CAMPO]',
            	 	'relation_dependency'=>'Relación dependiente [TABLA,CAMPO,CAMPO DISPARADOR, CAMPO RELACIONADO]',
            	 	'file'=>'Fichero [PATH]'
            	 ]);
            if($crud->getParameters()=='add'){            
            	$crud->set_rules('nombre','Nombre','required|callback_validateFieldName');
        	}
        	$crud->field_type('mostrar_listado','true_false',['0'=>'<span class="label label-default">NO</span>','1'=>'<span class="label label-success">SI</span>']);
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function validateFieldName($val){
        	if(!preg_match('/^[0-9a-z_\d]+$/', $val)){
        		$this->form_validation->set_message('validateFieldName','Formato de nombre incorrecto solo son aceptados los caracteres "09az_"');
        		return false;
        	}        	

        	$val = $this->db->get_where('formularios_campos',['nombre'=>$_POST['nombre'],'formularios_id'=>$_POST['formularios_id']]);
        	if($val->num_rows()>0){
        		$this->form_validation->set_message('validateFieldName','Ya existe un campo creado con ese nombre');
        		return false;
        	}        	
        	return true;
        }

        function generar($tabla){
        	$campos = [
        		'integer'=>'INT(11)',
        		'true_false'=>'TINYINT(1)',
        		'dropdown'=>'varchar(255)',
        		'set'=>'varchar(255)',
        		'date'=>'DATE',
        		'text'=>'TEXT',
        		'hidden'=>'VARCHAR(255)',
        		'string'=>'VARCHAR(255)',
        		'password'=>'VARCHAR(255)',
        		'relation'=>'INT(11)',
        		'relation_dependency'=>'INT(11)',
        		'file'=>'VARCHAR(255)'
        	];
        	$tabla = $this->db->get_where('formularios',['id'=>$tabla]);
        	if($tabla->num_rows()>0){
        		$tabla = $tabla->row();
        		$tabla->campos = $this->db->get_where('formularios_campos',['formularios_id'=>$tabla->id]);
        		
        		if($this->db->table_exists($this->$prefix.$tabla->nombre)){
        			$datos = $this->db->get_where($this->prefix.$tabla->nombre);
        			if($datos->num_rows()>0){
	        			echo 'Ya se ha generado la tabla';
	        			return;
        			}
        		}
        		$this->db->query('DROP TABLE IF EXISTS '.$this->prefix.$tabla->nombre);
        		$query = "CREATE TABLE ".$this->prefix.$tabla->nombre." (id INT(11) NOT NULL AUTO_INCREMENT,";
        		foreach($tabla->campos->result() as $t){
        			$required = $t->requerido==1?'NOT NULL':'';
        			$query.= $t->nombre." ".$campos[$t->tipo].' '.$required.',';
        		}
        		$query.= "CONSTRAINT ".$tabla->nombre."_pkey PRIMARY KEY(id))";
        		$this->db->query($query);
        		redirect('formularios/admin/formularios/success');
        	}
        }

        function mostrar($tabla){
        	if(is_numeric($tabla)){
        		$tabla = $this->db->get_where('formularios',['id'=>$tabla]);
        	}else{
        		$tabla = $this->db->get_where('formularios',['nombre'=>$tabla]);
        	}
        	if($tabla->num_rows()>0){
        		$tabla = $tabla->row();
        		$this->as['mostrar'] = $this->prefix.$tabla->nombre;
        		$tabla->campos = $this->db->get_where('formularios_campos',['formularios_id'=>$tabla->id]);

        		//CRUD
        		$this->norequireds = [];
        		foreach($tabla->campos->result() as $t){
        			if($t->tipo=='set'){
        				$this->norequireds[] = $t->nombre;
        			}
        		}
        		$crud = $this->crud_function('','');
        		$crud->set_subject($tabla->nombre);
        		$columnas = [];
        		foreach($tabla->campos->result() as $t){
        			if($t->mostrar_listado==1){
        				$columnas[] = $t->nombre;
        			}
        			//DISPLAYS
        			$crud->display_as($t->nombre,$t->etiqueta);
        			//Fields
        			switch($t->tipo){
        				case 'file':        					
        					$crud->set_field_upload($t->nombre,$t->valores);
        				break;
        				case 'dropdown':
        					$values = [];
        					foreach(explode(',',$t->valores) as $v){
        						$values[trim($v)] = trim($v);
        					}
        					$crud->field_type($t->nombre,'dropdown',$values);
        				break;
        				case 'set':
        					$values = [];
        					foreach(explode(',',$t->valores) as $v){
        						$values[trim($v)] = trim($v);
        					}
        					$crud->field_type($t->nombre,'set',$values);
        				break;
        				case 'true_false':
        					$values = explode(',',$t->valores);
        					$crud->field_type($t->nombre,'true_false',$values);
        				break;
        				case 'integer':
        					$crud->field_type($t->nombre,'integer',$values);
        				break;
        				case 'relation':
        					$files = explode(',',$t->valores);
        					$files[0] = $this->prefix.trim($files[0]);
        					if($this->db->table_exists($files[0])){
        						$crud->set_relation($t->nombre,$files[0],trim($files[1]));
        					}
        				break;
        				case 'relation_dependency':
        					$files = explode(',',$t->valores);
        					$files[0] = $this->prefix.trim($files[0]);
        					if(count($files)!=4){
        						echo 'EL campo '.$t->nombre.' Debe contener 4 valores';
        						die();
        					}
        					if($this->db->table_exists($files[0])){
        						$crud->set_relation($t->nombre,$files[0],trim($files[1]));
        						$crud->set_relation_dependency($t->nombre,$files[2],$files[3]);
        					}
        				break;
        			}
        		}
        		if(count($columnas)>0){          		        		
        			eval('$crud->columns('."'".implode("','",$columnas)."'".');');
        		}
        		//Actions
        		if($tabla->unset_add == 1){
        			$crud->unset_add();
        		}
        		if($tabla->unset_edit == 1){
        			$crud->unset_edit();
        		}
        		if($tabla->unset_delete == 1){
        			$crud->unset_delete();
        		}
        		if($tabla->unset_print == 1){
        			$crud->unset_print();
        		}
        		if($tabla->unset_export == 1){
        			$crud->unset_export();
        		}
        		if($tabla->unset_list == 1){
        			$crud->unset_list();
        		}

        		$crud = $crud->render();
        		$crud->title = $tabla->nombre;                
        		$this->loadView($crud);
        	}
        }

        function vaciar($tabla){
        	$tabla = $this->db->get_where('formularios',['id'=>$tabla]);
        	if($tabla->num_rows()>0){
        		$tabla = $tabla->row();
        		$this->db->query('DROP TABLE IF EXISTS '.$tabla->nombre);
        		redirect('formularios/admin/formularios/success');
        	}
        }
    }
?>
