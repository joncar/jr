<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{    	
        function __construct() {
            parent::__construct();
        }

        function formularios(){
            $crud = $this->crud_function('','');
            $crud->add_action('campos','',base_url('formularios/formularios_campos').'/');
            $crud->add_action('generar','',base_url('formularios/generar').'/');
            $crud->add_action('vaciar','',base_url('formularios/vaciar').'/');
            $crud->set_lang_string('insert_success_message','Datos almacenados con éxito <script>'.base_url('formularios/formularios_campos/{id}').'</script>');
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function formularios_campos($x = ''){
            $crud = $this->crud_function('','');
            $crud->where('formularios_id',$x)
            	 ->field_tyle('formularios_id','hidden',$x);
            $crud = $crud->render();            
            $this->loadView($crud);
        }
    }
?>
