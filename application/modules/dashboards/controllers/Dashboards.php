<?php

require_once APPPATH.'/controllers/Panel.php';    

class Dashboards extends Panel {

    function __construct() {
        parent::__construct();
    }
    
    public function ver($x = '') {        
        $this->loadView(array(
            'view'=>'panel',
            'crud'=>'user',
            'output'=>$this->load->view($x,array(),TRUE)
        ));
    }
    
    function create(){
        
    }

    function refresh($element){
        $this->load->view('components/'.$element);
    }

}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
