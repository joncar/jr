<div id="totalesPorSucursalContent">
<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $qry = $this->db->query("
        SELECT 
        sucursales.denominacion as categoria, 
        TRUNCATE(SUM(ventadetalle.totalcondesc),0) as total 
        FROM ventas 
        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
        INNER JOIN sucursales on sucursales.id = ventas.sucursal
        WHERE ventas.status = 0 and YEAR(ventas.fecha) = '".$year."' group by ventas.sucursal order by sum(ventadetalle.totalcondesc) desc
    ");
?>
<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="kt-portlet__head-title">Totales por sucursal</h3>
                </div>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" data-toggle="tab">Año</a>

                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <a class="dropdown-item" href="javascript:changeYeartotalesPorSucursal(<?= $i ?>)"><?= $i ?></a>
                            <?php endfor ?>     
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-widget14">
					<div class="kt-widget14__header">
						<h3 class="kt-widget14__title">
							Resumen de ventas por sucursal
						</h3>
						<span class="kt-widget14__desc">
							Se muestran las sucursales más vendidas
						</span>
					</div>
					<div class="kt-widget14__content">
						<div class="kt-widget14__chart">
							<div class="kt-widget14__stat">
								<?= $year ?>
							</div>
							<canvas id="totalesPorSucursal" style="height: 280px; width: 280px;"></canvas>
						</div>
					</div>
				</div>
			</div>
</div>
<script>
    function loadChartotalesPorSucursal(){        
        var chartContainer = KTUtil.getByID('totalesPorSucursal');
        
        var config = {
            type: 'doughnut',
            data: {
            	labels: <?php 
	                $data = array();
	                foreach($qry->result() as $n=>$v){
	                    $data[] = $v->categoria;
	                }
	                echo json_encode($data);
	              ?>,
	            datasets: [{
	                //label: 'Dataset 1',
	                backgroundColor: [
                        KTApp.getStateColor('success'),
                        KTApp.getStateColor('info'),
                        KTApp.getStateColor('primary'),
                        KTApp.getStateColor('warning'),
                        KTApp.getStateColor('danger'),
                        KTApp.getStateColor('brand')
                    ],
	                data: <?php 
	                    $data = array();
	                    foreach($qry->result() as $n=>$v){
	                        $data[] = $v->total;
	                    }
	                    echo json_encode($data);
	                  ?>
	            }]
            },
            options: {
                cutoutPercentage: 75,
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                    position: 'top',
                },
                title: {
                    display: false,
                    text: 'Technology'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
                tooltips: {
                    enabled: true,
                    intersect: false,
                    mode: 'nearest',
                    bodySpacing: 5,
                    yPadding: 10,
                    xPadding: 10, 
                    caretPadding: 0,
                    displayColors: false,
                    backgroundColor: KTApp.getStateColor('brand'),
                    titleFontColor: '#ffffff', 
                    cornerRadius: 4,
                    footerSpacing: 0,
                    titleSpacing: 0
                }
            }
        };
        var ctx = KTUtil.getByID('totalesPorSucursal').getContext('2d');
        var myDoughnut = new Chart(ctx, config);  
    }
    

    <?php if(!empty($_POST['year'])): ?>
        loadChartotalesPorSucursal();
    <?php else: ?>
        window.afterLoad.push(loadChartotalesPorSucursal);        
    <?php endif ?>

    function changeYeartotalesPorSucursal(y){
        $.post('dashboards/refresh/totales_por_categoria',{year:y},function(data){
            $("#totalesPorSucursalContent").html(data);
        });
    }
</script>
</div>