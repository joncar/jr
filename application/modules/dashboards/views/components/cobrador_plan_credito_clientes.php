
<?php 
	$where = '';
	if(!empty($_GET['cliente'])){
		$where = "AND (clientes.nombres LIKE '%".$_GET['cliente']."%' OR clientes.apellidos LIKE '%".$_GET['cliente']."%' OR clientes.nro_documento LIKE '%".$_GET['cliente']."%')";
		$creditos = $this->db->query("
			SELECT 
			clientes.id,
			creditos.id as credito,
			CONCAT(clientes.nombres,' ',clientes.apellidos) as cliente,
			(SELECT MIN(plan_credito.nro_cuota) FROM plan_credito WHERE creditos_id = creditos.id AND pagado = 0) as cuota,
			FORMAT(plan_credito.monto_a_pagar,0,'de_DE') as importe,
			(SELECT DATE_FORMAT(MIN(fecha_a_pagar),'%d/%m/%Y') as fecha_a_pagar FROM plan_credito WHERE creditos_id = creditos.id AND pagado = 0) as fecha_a_pagar,
			FORMAT(get_saldo(clientes.id),0,'de_DE') as saldo,
			FORMAT(get_saldo_por_credito(creditos.id),0,'de_DE') as saldoC,
			getResumenCuotas(creditos.id) AS cuotas
			FROM plan_credito
			INNER JOIN creditos ON creditos.id = plan_credito.creditos_id
			INNER JOIN ventas ON ventas.id = creditos.ventas_id
			INNER JOIN clientes ON clientes.id = ventas.cliente
			WHERE user_id = ".$this->user->id." ".$where." AND (creditos.anulado IS NULL OR creditos.anulado = 0) AND get_saldo_por_credito(creditos.id) > 0
			GROUP BY creditos.id
			ORDER BY clientes.nombres
		");
	}
?>

	
<?php if(empty($_GET['creditos_id'])): ?>
<div class="clientesPrestamos">
	<div class="row">
		<div class="col-12 col-md-4">
			<div class="kt-portlet transparent ui-sortable-handle" data-id="4">    		
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							<i class="ace-icon fa fa-search"></i>
							Buscar cliente
						</h3>
						<div class="widget-toolbar no-border">
							<div class="inline dropdown-hover">						
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="widget-main" style="padding-bottom: 0;">					
						<form action="" onsubmit="return search(this)">
							<input type="text" class="form-control" name="cliente" placeholder="Cédula, Nombre o apellidos" value="<?= @$_GET['cliente'] ?>">
							<div style="margin: 29px 0;text-align: center;">
	  							<button type="submit" class="btn btn-success">
	  								<i class="fa fa-search"></i> Buscar
	  							</button>
	  						</div>
						</form>
					</div>
				</div><!-- /.widget-body -->
			</div>
		</div>
	<?php if(!empty($_GET['cliente'])): ?>
	<?php if($creditos->num_rows()==0): ?>
		<div class="col-12 col-md-4">
			<div class="kt-portlet transparent ui-sortable-handle" data-id="4">    		
				<div class="kt-portlet__body">
					<div class="widget-main" style="/*! padding-bottom: 0; */">					
						Sin resultados a mostrar
					</div>
				</div><!-- /.widget-body -->
			</div>
		</div>
	<?php endif ?>
	<?php foreach($creditos->result() as $c): ?>
		<div class="col-12 col-md-3">
			<div class="kt-portlet transparent ui-sortable-handle" data-id="4">    		
			    <div class="kt-portlet__head">
			        <div class="kt-portlet__head-label">
				        <h3 class="kt-portlet__head-title"><?= cortar_palabras($c->cliente,3) ?></h3>				        
				    </div>
			    </div>

			    <div class="kt-portlet__body"  style="cursor:pointer;" onclick="verCliente(<?= $c->credito ?>)">
			        
			    	<div class="widget-main no-padding">
			            <div class="widget-main"> 							
			            	<div class="itemdiv memberdiv">
								<div class="user text-center" style="margin-bottom:10px">
									<img alt="<?= $c->cliente ?>" src="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/cropper/vacio.png" style="width:40px">
								</div>
								<div class="body">
									<div class="name">
										<a href="#" style="max-width:100%">
											Credito #<?= $c->credito ?> |
											Cuota #<?= $c->cuota ?> - <?= $c->importe ?>
										</a>
									</div>
									<div class="time">
										Próxima cuota:
										<span class="green"><?= $c->fecha_a_pagar ?></span><br/>
										Saldo:
										<span class="green" title="Saldo"><?= $c->saldoC ?></span><br/>
										Cuotas Pagadas:
										<span class="green" title="Cuotas pagadas"><?= $c->cuotas ?></span><br/>
										Saldo General:
										<span class="green" title="Saldo general"><?= $c->saldo ?></span><br/>
									</div>
								</div>
							</div>
						</div>
			        </div>
			    </div>
			</div>
		</div>
	<?php endforeach ?>
	<?php endif ?>


	<div class="modal fade" tabindex="-1" role="dialog" id="creditoCobro">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	      	<h4 class="modal-title" id="clienteModal">Consultando</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        	<span aria-hidden="true">&times;</span>
	        </button>	        
	      </div>
	      <div class="modal-body">
			  
	      </div>
	      <div class="modal-footer">
	      	<span style="float:left">Saldo actual: <span id="CreditoSaldo" style="font-weight: bold"></span></span>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<script>
		function verCliente(cliente){
			$("#creditoCobro .modal-body").html('Cargando');
			$("#clienteModal").html('Consultando');
			$("#creditoCobro").modal('show');
			$.post('dashboards/refresh/cobrador_plan_credito_clientes?creditos_id='+cliente,{},function(data){
	            $("#creditoCobro .modal-body").html(data);
	        });
			
		}

		function search(f){
			var cliente = $(".clientesPrestamos input[name='cliente']").val();
			$.post('dashboards/refresh/cobrador_plan_credito_clientes?cliente='+cliente,{},function(data){
	            $(".clientesPrestamos").html(data);
	        });
			return false;
		}
	</script>

	</div>
</div>



<!------------------------- Viene por POST ----------------------------->
<?php else: ?>
	<style>
		.tab-pane#home table tr{
			cursor: pointer;
		}
	</style>
	<?php 
		$this->db->select("plan_credito.id,nro_cuota as '#Cuota',fecha_a_pagar,DATE_FORMAT(fecha_a_pagar,'%d/%m') AS vencimiento,saldo as Importe,pagado");
		$this->db->order_by('plan_credito.nro_cuota','ASC');
		$qr = $this->db->get_where('plan_credito',array('creditos_id'=>$_GET['creditos_id']));
		$this->db->select("creditos.*,clientes.id as cliente_id, CONCAT(clientes.nombres,' ',clientes.apellidos) as cliente, FORMAT(get_saldo_por_credito(creditos.id),0,'de_DE') as saldo",FALSE);
		$this->db->join('ventas','ventas.id = creditos.ventas_id');
		$this->db->join('clientes','clientes.id = ventas.cliente');
		$credito = $this->db->get_where('creditos',array('creditos.id'=>$_GET['creditos_id']))->row();
	?>
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" id="myTab" role="tablist">
	  	  <li role="presentation" class="nav-item">
	  	  	<a class="nav-link active" href="#home" aria-controls="home" role="tab" data-toggle="tab">Selecciona una cuota</a>
	  	  </li>
	      <li role="presentation" class="nav-item" id="cargarPagoTabBtn" style="display: none;">
	      	<a class="nav-link" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Cargar pago</a>
	      </li>
	  </ul>

	  <!-- Tab panes -->
	  <div class="tab-content">
	    <div role="tabpanel" class="tab-pane active" id="home" style="max-height: 66vh;overflow-y: auto;">
	    	<div style="text-align: left">
	    		Cuota: 
				<span class="badge badge-success">Pagada</span>
				<span class="badge badge-danger">Vencida</span>
				<span class="badge badge-warning">Por vencer</span>
	    	</div>
	    	
	    	<?= sqlToHtml($qr,array('id','#Cuota','vencimiento','Importe'),array(),array(
				'#Cuota'=>function($val,$row){
					$label = 'warning';
					if($row->pagado==1){
						$label = 'success';
					}
					elseif(strtotime($row->fecha_a_pagar)<time()){
						$label = 'danger';
					}
					return '<span class="badge badge-'.$label.'">'.$val.'</span>';
				},
				'vencimiento'=>function($val,$row){
					return dia_short(date("w",strtotime($row->fecha_a_pagar))).' '.$val;
				}
	    	)) ?>
	    </div>
	    <div role="tabpanel" class="tab-pane" id="profile">
	    	<div style="text-align: right">
	    		Cuota <span id="spanCuotaNumero">#2</span>
	    	</div>
	    	<form  method="post" id="crudForm" onsubmit="return pagarCredito(this)">
	    			<div class="form-group" id="total_credito_field_box">
	                	<label for="field-formapago_id" id="formapago_id_display_as_box" style="width:100%">
	                		Forma de pago:
	                	</label>
	                	<?= form_dropdown_from_query('formapago_id','formapago','id','denominacion') ?>
	                </div>
	                <div class="form-group" id="total_credito_field_box">
	                	<label for="field-total_credito" id="total_credito_display_as_box" style="width:100%">
	                		Total a cobrar:
	                	</label>
	                	<input type="text" name="total_credito" value="61111" id="field-total_credito" class="form-control" readonly="">
	                </div>		                		                
	                <div class="form-group" id="total_pagado_field_box">
	                	<label for="field-total_pagado" id="total_pagado_display_as_box" style="width:100%">
	                		Total cobrado<span class="required">*</span>  :
	                	</label>
	                	<input type="text" name="total_pagado" value="61111" id="field-total_pagado" class="form-control">
	                </div>
	                <input id="field-recibo" type="hidden" name="recibo" value="0">	                
	                <input id="field-concepto" type="hidden" name="concepto" value="Abono de cuota">
	                <input id="field-total_mora" type="hidden" name="total_mora" value="0">
	                <input id="field-total_mora" type="hidden" name="total_pago_mora" value="0">
	                <input id="field-clientes_id" type="hidden" name="clientes_id" value="<?= $credito->cliente_id ?>">
	                <input id="field-cliente" type="hidden" name="cliente" value="<?= $credito->cliente ?>">
	                <input id="field-fecha" type="hidden" name="fecha" value="<?= date("Y-m-d H:i:s") ?>">
	                <input id="field-creditos_id" type="hidden" name="creditos_id" value="<?= $_GET['creditos_id'] ?>">
	                <input id="field-plan_credito_id" type="hidden" name="plan_credito_id" value="0">
	                <input id="field-sucursal" type="hidden" name="sucursal" value="<?= $this->user->sucursal ?>">
	                <input id="field-caja" type="hidden" name="caja" value="<?= $this->user->caja ?>">
	                <input id="field-user" type="hidden" name="user" value="<?= $this->user->id ?>">
	                <input id="field-cajadiaria" type="hidden" name="cajadiaria" value="<?= $this->user->cajadiaria ?>">
	                <input id="field-anulado" type="hidden" name="anulado" value="0">
	                <input id="field-efectivo" type="hidden" name="efectivo" value="0">
	                <input id="field-vuelto" type="hidden" name="vuelto" value="0">
	                <input id="field-fecha_anulado" type="hidden" name="fecha_anulado" value="0000-00-00">
	                <input id="field-quien_anulo" type="hidden" name="quien_anulo" value="0">
	                <!-- End of hidden inputs -->		                
	                <div id="report-success"></div>
					<div class="btn-group">			
	                    <button id="form-button-save" type="submit" class="btn btn-success">PAGAR</button>		                    		                    
					</div>                
			</form>
	    </div>
	  </div>

	</div>
	<script>
		var plan = 0;
		$("#creditoCobro #clienteModal").html('<?= $credito->cliente ?>');
		$("#CreditoSaldo").html('<?= $credito->saldo ?>')
		$(".tab-pane#home table tr").on('click',function(){
			var id = $($(this).find('td')[0]).html();
			var cuota = $($(this).find('td')[1]).text();			
			var importe = $($(this).find('td')[3]).html();
			importe = importe.replace(/\./g,'');
			importe = importe.replace(/,/g,'.');
			if(!isNaN(parseInt(id))){
				$("#cargarPagoTabBtn").show();
				$('#cargarPagoTabBtn a[href="#profile"]').tab('show');
				$("#spanCuotaNumero").html('#'+cuota);
				$("#field-plan_credito_id").val(id);
				plan = id;
				$("#field-total_credito").val(importe);
				$("#field-total_pagado").val(importe);
			}
		});

		function pagarCredito(f){	
			var maximo = '<?= $credito->saldo ?>';
			maximo = maximo.replace(/\./g,'');
			maximo = parseFloat(maximo);
			var pagado = parseFloat($(f).find('#field-total_pagado').val());
			$('#report-success').removeClass('alert alert-danger')
			if(!isNaN(pagado) && pagado<=maximo){	
				insertar('movimientos/creditos/pagar_credito/'+plan+'/insert',f,'#report-success',function(data){
					$('#report-success').html(data.success_message);
					if(data.success){
						verCliente('<?= $_GET['creditos_id'] ?>');
					}
				});
			}
			if(pagado>=maximo){
				$('#report-success').addClass('alert alert-danger').html('El importe pagado no puede superar al saldo del crèdito');
			}
			return false;
		}
	</script>
<?php endif ?>