<div id="pagoPorFechaContent">
<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $resumen = $this->db->query("
        SELECT
        sum(t.Enero) as Enero,
        sum(t.Febrero) as Febrero,
        sum(t.Marzo) as Marzo,
        sum(t.Abril) as Abril,
        sum(t.Mayo) as Mayo,
        sum(t.Junio) as Junio,
        sum(t.Julio) as Julio,
        sum(t.Agosto) as Agosto,
        sum(t.Setiembre) as Setiembre,
        sum(t.Octubre) as Octubre,
        sum(t.Noviembre) as Noviembre,
        sum(t.Diciembre) as Diciembre
        FROM(
        SELECT
        if(month(pagocliente.fecha)=1,pagocliente.total_pagado,0) as Enero,
        if(month(pagocliente.fecha)=2,pagocliente.total_pagado,0) as Febrero,
        if(month(pagocliente.fecha)=3,pagocliente.total_pagado,0) as Marzo,
        if(month(pagocliente.fecha)=4,pagocliente.total_pagado,0) as Abril,
        if(month(pagocliente.fecha)=5,pagocliente.total_pagado,0) as Mayo,
        if(month(pagocliente.fecha)=6,pagocliente.total_pagado,0) as Junio,
        if(month(pagocliente.fecha)=7,pagocliente.total_pagado,0) as Julio,
        if(month(pagocliente.fecha)=8,pagocliente.total_pagado,0) as Agosto,
        if(month(pagocliente.fecha)=9,pagocliente.total_pagado,0) as Setiembre,
        if(month(pagocliente.fecha)=10,pagocliente.total_pagado,0) as Octubre,
        if(month(pagocliente.fecha)=11,pagocliente.total_pagado,0) as Noviembre,
        if(month(pagocliente.fecha)=12,pagocliente.total_pagado,0) as Diciembre
        FROM pagocliente
        WHERE pagocliente.anulado = 0 AND year(pagocliente.fecha) = '".$year."') AS t
    ")->row();
?>

<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="kt-portlet__head-title">Resumen de pagos por mes (<?= $year ?>)</h3>
                </div>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" data-toggle="tab">Año</a>

                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <a class="dropdown-item" href="javascript:changeYearpagoPorFecha(<?= $i ?>)"><?= $i ?></a>
                            <?php endfor ?>     
                        </div>
                    </div>
                </div>
            </div>

           <div class="kt-portlet__body">
                <div class="kt-section">
                    <div class="widget-main">                        						
                        <canvas id="pagoPorFecha" width="auto" height="300"></canvas>
                    </div>
                </div>
            </div>
</div>

<script>
	function loadCharpagoPorFecha(){        
        var chartContainer = KTUtil.getByID('pagoPorFecha');
        var chartData = {
            labels: <?php 
                $data = array();
                foreach($resumen as $n=>$v){
                    $data[] = $n;
                }
                echo json_encode($data);
              ?>,
            datasets: [{
                //label: 'Dataset 1',
                backgroundColor: KTApp.getStateColor('success'),
                data: <?php 
                    $data = array();
                    foreach($resumen as $n=>$v){
                        $data[] = $v;
                    }
                    echo json_encode($data);
                  ?>
            }]
        };
        var chart = new Chart(chartContainer, {
            type: 'bar',
            data: chartData,
            options: {
                title: {
                    display: true,
                },
                tooltips: {
                    intersect: true,
                    mode: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10
                },
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
                barRadius: 4,
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: false,
                        stacked: true
                    }],
                    yAxes: [{
                        display: true,
                        stacked: true,
                        gridLines: false
                    }]
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }
                }
            }
        });        
    }
    

    <?php if(!empty($_POST['year'])): ?>
        loadCharpagoPorFecha();
    <?php else: ?>
        window.afterLoad.push(loadCharpagoPorFecha);        
    <?php endif ?>

    function changeYearpagoPorFecha(y){
        $.post('dashboards/refresh/pago_por_fecha',{year:y},function(data){
            $("#pagoPorFechaContent").html(data);
        });
    }
</script>
</div>