
<div class="row">
    <div class="col-12" align="right">
        <span class="badge badge-primary">Ocupadas</span>
        <span class="badge badge-default">Disponibles</span>
        <span class="badge badge-warning">Sucio</span>
        <span class="badge badge-secondary">Reservado</span>
    </div>
     <?php foreach ($this->db->get_where("habitaciones")->result() as $m): ?>
	      <div class="col-6 col-lg-2" style="margin-bottom:15px">
	          <a href="<?= $m->estado ? base_url("pedidos/admin/pedidos_detalles/" . $m->pedidos_id) : "javascript:reservarHabitacion(" . $m->id . ",'habitacion')" ?>">
	              <div class="card <?= $m->estado > 0 ? 'text-white' : '' ?>">
	                  <div class="card-header bg-<?= $estados[$m->estado] ?>" style="padding:5px 0">
	                      <div class="row" style="margin-left:0; margin-right:0">
                          <div class="col-12 col-sm-3">
                              <i class="fa fa-<?= $m->estado ? 'users' : 'cutlery' ?> fa-2x"></i>
                          </div>
                          <div class="col-12  col-sm-9 text-right">
                              <?php
                                $monto = 0;
                                $cliente = '';
                                if($m->estado && !empty($m->pedidos_id)){
                                $monto = $this->db->query("SELECT SUM(pedidos_detalles.total) as total from pedidos_detalles where pedidos_id = ".$m->pedidos_id)->row()->total;
                                $this->db->select('clientes.*, pedidos.cliente_nombre');
                                $this->db->join('clientes','clientes.id = pedidos.clientes_id');
                                $cliente = @$this->db->get_where('pedidos',array('pedidos.id'=>$m->pedidos_id))->row();
                              }
                              ?>
                              <h4 class="huge">
                                <?= $m->habitacion_nombre ?>
                              </h4>
                              <div id="clientes">
                                <?= number_format($monto,0,',','.') ?> Gs <br/>
                                <?= @$cliente->id==1?@$cliente->cliente_nombre:@$cliente->nombres.' '.@$cliente->apellidos ?>
                              </div>                                     
                          </div>
                        </div>
	                  </div>
	                  <div class="card-footer bg-<?= $estados[$m->estado] ?>">
	                      <span class="pull-left"><?= $m->estado ? 'Ver Detalles' : 'Reservar' ?></span>
	                      <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
	                      <div class="clearfix"></div>
	                  </div>                        
	              </div>
	          </a>
	      </div>
  <?php endforeach ?>
</div>