<?php
    $qry = $this->db->query("
        SELECT 
		productosucursal.producto,
		productos.nombre_comercial,
		productosucursal.stock,
		productos.stock_minimo
		FROM productosucursal
		INNER JOIN productos ON productos.codigo = productosucursal.producto AND productos.inventariable = 1 AND productos.stock_minimo > 0
		WHERE sucursal = {$this->user->sucursal} AND productosucursal.stock<productos.stock_minimo
    ");
?>
<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="kt-portlet__head-title">Productos con pocas existencias</h3>
                </div>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <!--<a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año Lectivo</b></a>
                            </li>                            
                        </ul>-->
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="kt-section">
                    <div class="widget-main no-padding">
                        <div style="width:100%; height:150px; overflow-y:auto">
                        	<?php sqlToHtml($qry); ?>
                        </div>
                    </div>
                </div>
            </div>
</div>
<script>

</script>
