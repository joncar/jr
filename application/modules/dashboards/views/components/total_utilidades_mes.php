<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="kt-portlet__head-title">Total de utilidades por mes</h3>
                </div>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <!--<ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año</b></a>
                            </li>              
                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <li>
                                <a href="javascript:clickPorcentajeAlumnosAprobadosAplazados(<?= $i ?>)"><?= $i ?></a>
                            </li>
                            <?php endfor ?>              
                        </ul>-->
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

           <div class="kt-portlet__body">
                <div class="kt-section">
                    <div class="widget-main">                        
						<div id="totalUtilidadesMes"></div>
                    </div>
                </div>
            </div>
</div>
<script>
	Morris.Bar({
	  element: 'totalUtilidadesMes',
	  data: [
	    { y: '2006', a: 100, b: 90 },
	    { y: '2007', a: 75,  b: 65 },
	    { y: '2008', a: 50,  b: 40 },
	    { y: '2009', a: 75,  b: 65 },
	    { y: '2010', a: 50,  b: 40 },
	    { y: '2011', a: 75,  b: 65 },
	    { y: '2012', a: 100, b: 90 }
	  ],
	  xkey: 'y',
	  ykeys: ['a'],
	  labels: ['Series A']
	});
</script>
