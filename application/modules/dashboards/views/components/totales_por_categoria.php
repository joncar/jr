<div id="totalesPorCategoriaContent">
<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $qry = $this->db->query("
        SELECT 
        categoriaproducto.denominacion as categoria, 
        TRUNCATE(SUM(ventadetalle.totalcondesc),0) as total 
        FROM ventas 
        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
        INNER JOIN productos on productos.codigo = ventadetalle.producto 
        INNER JOIN clientes on clientes.id = ventas.cliente 
        INNER JOIN categoriaproducto on categoriaproducto.id = productos.categoria_id 
        WHERE ventas.status = 0 and YEAR(ventas.fecha) = '".$year."' group by productos.categoria_id order by sum(ventadetalle.totalcondesc) desc
    ");
?>
<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="kt-portlet__head-title">Totales por categoría</h3>
                </div>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" data-toggle="tab">Año</a>

                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <a class="dropdown-item" href="javascript:changeYeartotalesPorCategoria(<?= $i ?>)"><?= $i ?></a>
                            <?php endfor ?>     
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="kt-section">
                    <div class="widget-main">                        						
                        <canvas id="totalesPorCategoria" width="auto" height="300"></canvas>
                    </div>
                </div>
            </div>
</div>
<script>
    function loadChartotalesPorCategoria(){        
        var chartContainer = KTUtil.getByID('totalesPorCategoria');
        var chartData = {
            labels: <?php 
                $data = array();
                foreach($qry->result() as $n=>$v){
                    $data[] = $v->categoria;
                }
                echo json_encode($data);
              ?>,
            datasets: [{
                //label: 'Dataset 1',
                backgroundColor: KTApp.getStateColor('success'),
                data: <?php 
                    $data = array();
                    foreach($qry->result() as $n=>$v){
                        $data[] = $v->total;
                    }
                    echo json_encode($data);
                  ?>
            }]
        };
        var chart = new Chart(chartContainer, {
            type: 'bar',
            data: chartData,
            options: {
                title: {
                    display: true,
                },
                tooltips: {
                    intersect: true,
                    mode: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10
                },
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
                barRadius: 4,
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: false,
                        stacked: true
                    }],
                    yAxes: [{
                        display: true,
                        stacked: true,
                        gridLines: false
                    }]
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }
                }
            }
        });        
    }
    

    <?php if(!empty($_POST['year'])): ?>
        loadChartotalesPorCategoria();
    <?php else: ?>
        window.afterLoad.push(loadChartotalesPorCategoria);        
    <?php endif ?>

    function changeYeartotalesPorCategoria(y){
        $.post('dashboards/refresh/totales_por_categoria',{year:y},function(data){
            $("#totalesPorCategoriaContent").html(data);
        });
    }
</script>
</div>