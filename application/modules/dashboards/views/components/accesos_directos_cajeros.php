<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
              <span class="kt-portlet__head-icon kt-hidden">
                <i class="la la-gear"></i>
              </span>
              <h3 class="kt-portlet__head-title">
              	<i class="ace-icon fa fa-star orange"></i> Accesos directos
              </h3>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <!--<a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año Lectivo</b></a>
                            </li>                            
                        </ul>-->
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <div class="kt-section">
            <div class="widget-main no-padding">
                <div class="widget-main" style="text-align: center;">
                      <a href="<?= base_url('movimientos/creditos/creditos') ?>" class="btn btn-danger btn-md btn-app no-radius">
                        <i class="ace-icon fa fa-percent"></i>
                        Créditos
                        <!--<span class="badge badge-pink">+3</span>-->
                      </a>  
                      <a href="<?= base_url('movimientos/ventas/pagocliente') ?>" class="btn btn-info btn-md btn-app no-radius">
                        <i class="ace-icon fa fa-percent"></i>
                        Pagos
                        <!--<span class="badge badge-pink">+3</span>-->
                      </a>                         
                </div>
            </div>
            <div class="widget-main no-padding">
                <div class="widget-main" style="text-align: center;">                                                  
                    Total cobrado hoy: <b style="font-size: 24px;"><?= $this->db->query("SELECT FORMAT(IFNULL(SUM(total_pagado),0),0,'de_DE') as total FROM pagocliente INNER JOIN creditos ON creditos.id = pagocliente.creditos_id WHERE (pagocliente.anulado IS NULL OR pagocliente.anulado = 0) AND (creditos.anulado IS NULL OR creditos.anulado = 0) AND DATE(fecha) = DATE(NOW()) AND user_id = ".$this->user->id)->row()->total; ?> Gs</b>
                    <!--<span class="badge badge-pink">+3</span>-->                          creditos
                </div>
            </div>
        </div>
	</div>
</div>
<script>

</script>
