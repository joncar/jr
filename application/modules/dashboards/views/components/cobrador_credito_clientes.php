
<?php 
	$where = '';
	if(!empty($_GET['cliente'])){
		$where = "AND (clientes.nombres LIKE '%".$_GET['cliente']."%' OR clientes.apellidos LIKE '%".$_GET['cliente']."%' OR clientes.nro_documento LIKE '%".$_GET['cliente']."%')";
		$creditos = $this->db->query("
			SELECT 
			ventas.*, 
			CONCAT('', COALESCE(clientes.nombres, ''), ' ', COALESCE(clientes.apellidos, ''), '') as cliente_nombre, 
			tipo_facturacion.denominacion, 
			timbrados.nro_timbrado, 
			empleados.user_id, 
			servicios.fecha,
			clientes.nro_documento,
			get_saldo(clientes.id) as saldo,
			ifnull(max(date(ventas.fecha)),0) as ultima_compra,
			ifnull(SUM(ventadetalle.totalcondesc),0) as total_compra,
			ifnull(pagos.ultimo_pago,0) as ultimo_pago,
			get_total_pagado(clientes.id) as total_pagado
			FROM ventas
			INNER JOIN ventadetalle on ventadetalle.venta = ventas.id
			LEFT JOIN clientes ON clientes.id = ventas.cliente 
			LEFT JOIN tipo_facturacion ON tipo_facturacion.id = ventas.tipo_facturacion_id 
			LEFT JOIN timbrados ON timbrados.id = ventas.timbrados_id 
			LEFT JOIN empleados ON empleados.id = ventas.empleados_id 
			LEFT JOIN servicios ON servicios.id = ventas.servicios_id 
			LEFT JOIN(
                SELECT 
                clientes.id as clienteid,
                max(date(pagocliente.fecha)) as ultimo_pago,
                sum(pagocliente.total_pagado) as total_pagos
                FROM pagocliente 
                INNER JOIN clientes on clientes.id = pagocliente.clientes_id
                WHERE pagocliente.anulado = 0 or pagocliente.anulado is null
                GROUP BY pagocliente.clientes_id
            ) as pagos on pagos.clienteid =ventas.cliente
			WHERE ventas.transaccion = 2 AND ventas.status != -1 $where
			GROUP BY ventas.cliente
			LIMIT 10
		");
	}
?>

	
<?php if(empty($_GET['creditos_id'])): ?>
<div class="clientesPrestamos">
	<div class="row">
		<div class="col-12 col-md-4">
			<div class="kt-portlet transparent ui-sortable-handle" data-id="4">    		
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							<i class="ace-icon fa fa-search"></i>
							Buscar cliente
						</h3>
						<div class="widget-toolbar no-border">
							<div class="inline dropdown-hover">						
							</div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="widget-main" style="padding-bottom: 0;">					
						<form action="" onsubmit="return search(this)">
							<input type="text" class="form-control" name="cliente" placeholder="Cédula, Nombre o apellidos" value="<?= @$_GET['cliente'] ?>">
							<div style="margin: 29px 0;text-align: center;">
	  							<button type="submit" class="btn btn-success">
	  								<i class="fa fa-search"></i> Buscar
	  							</button>
	  						</div>
						</form>
					</div>
				</div><!-- /.widget-body -->
			</div>
		</div>
	<?php if(!empty($_GET['cliente'])): ?>
	<?php if($creditos->num_rows()==0): ?>
		<div class="col-12 col-md-4">
			<div class="kt-portlet transparent ui-sortable-handle" data-id="4">    		
				<div class="kt-portlet__body">
					<div class="widget-main" style="/*! padding-bottom: 0; */">					
						Sin resultados a mostrar
					</div>
				</div><!-- /.widget-body -->
			</div>
		</div>
	<?php endif ?>
	<?php foreach($creditos->result() as $c): ?>
		<div class="col-12 col-md-3">
			<div class="kt-portlet transparent ui-sortable-handle" data-id="4">    		
			    <div class="kt-portlet__head">
			        <div class="kt-portlet__head-label">
				        <h3 class="kt-portlet__head-title"><?= cortar_palabras($c->cliente_nombre,3) ?></h3>				        
				    </div>
			    </div>

			    <div class="kt-portlet__body"  style="cursor:pointer;" onclick="verCliente(<?= $c->id ?>)">
			        
			    	<div class="widget-main no-padding">
			            <div class="widget-main"> 							
			            	<div class="itemdiv memberdiv">
								<div class="user text-center" style="margin-bottom:10px">
									<img alt="<?= $c->cliente ?>" src="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/cropper/vacio.png" style="width:40px">
								</div>
								<div class="body">
									<div class="name">
										<a href="#" style="max-width:100%">
											Cliente #<?= $c->id ?>	Doc#: <?= $c->nro_documento ?>										
										</a>
									</div>
									<div class="time">										
										Saldo:
										<span class="green" title="Saldo"><?= number_format($c->saldo,0,',','.') ?></span><br/>
										Total Pagos:
										<span class="green" title="Cuotas Pagadas"><?= number_format($c->total_pagado,0,',','.') ?></span><br/>
										Último Pago:
										<span class="green" title="Saldo General"><?= $c->ultimo_pago ?></span><br/>
										Total Compras:
										<span class="green" title="Total Compra"><?= number_format($c->total_compra,0,',','.') ?></span><br/>
									</div>
								</div>
							</div>
						</div>
			        </div>
			    </div>
			</div>
		</div>
	<?php endforeach ?>
	<?php endif ?>


	<div class="modal fade" tabindex="-1" role="dialog" id="creditoCobro">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	      	<h4 class="modal-title" id="clienteModal">Consultando</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        	<span aria-hidden="true">&times;</span>
	        </button>	        
	      </div>
	      <div class="modal-body">
			  
	      </div>
	      <div class="modal-footer" style="flex-wrap: nowrap;">
	      	<span style="width:100%">Saldo actual: <span id="CreditoSaldo" style="font-weight: bold; font-size:22px"></span></span>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>        
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<script>
		function verCliente(cliente){
			$("#creditoCobro .modal-body").html('Cargando');
			$("#clienteModal").html('Consultando');
			$("#creditoCobro").modal('show');
			$.post('dashboards/refresh/cobrador_credito_clientes?creditos_id='+cliente,{},function(data){
	            $("#creditoCobro .modal-body").html(data);
	        });
			
		}

		function search(f){
			var cliente = $(".clientesPrestamos input[name='cliente']").val();
			$(".clientesPrestamos").html('Consultando...');
			$.post('dashboards/refresh/cobrador_credito_clientes?cliente='+cliente,{},function(data){
	            $(".clientesPrestamos").html(data);
	        });
			return false;
		}
	</script>

	</div>
</div>



<!------------------------- Viene por POST ----------------------------->
<?php else: ?>
	<style>
		.tab-pane#home table tr{
			cursor: pointer;
		}
	</style>
	<?php 
		$this->db->select('
			ventas.*, 
			clientes.id as clienteid, 
			clientes.nombres as clientename, 
			clientes.apellidos as apellidos,
			get_saldo(clientes.id) as saldo
		');
	    $this->db->join('clientes', 'clientes.id = ventas.cliente');
	    $cliente = $this->db->get_where('ventas', array('ventas.id' => $_GET['creditos_id']));	 
	    if($cliente->num_rows()>0):   
	?>
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" id="myTab" role="tablist">
	  	  <li role="presentation" class="nav-item">
	  	  	<a class="nav-link active" href="#home" aria-controls="home" role="tab" data-toggle="tab">Abonar deuda</a>
	  	  </li>	      
	  </ul>

	  <!-- Tab panes -->
	  <div class="tab-content">
	  	<form  method="post" action="/cajas/admin/abonarDeuda/<?= $_GET['creditos_id'] ?>/1" id="crudForm" onsubmit="return sendForm(this,'.response')">
		    <div role="tabpanel" class="tab-pane active" id="home" style="max-height: 66vh;overflow-y: auto;">	    	
				<?php if($cliente->row()->saldo>0 || $this->ajustes->permitir_pago_superior_a_saldo==1): ?>
					<div class="row ml-0 mr-0">					    
				        <div class="col-12 col-md-6">
				        	<div class="form-group" id="total_credito_field_box">
			                	<label for="field-formapago_id" id="formapago_id_display_as_box">
			                		Forma de pago:
			                	</label>
			                	<?= form_dropdown_from_query('formapago_id','formapago','id','denominacion') ?>
			                </div>
			            </div>
			            <div class="col-12 col-md-6">
			                <div class="form-group" id="total_credito_field_box">
			                	<label for="field-monto" id="monto_display_as_box">
			                		Monto:
			                	</label>
			                	<input type="text" name="monto" id="field-monto" class="form-control" placeholder="Monto a abonar" autocomplete="off">
			                </div>		    
		                </div>
		                <div class="col-12">
		                	<div class="response"></div>
		                </div>
		                <div class="col-12">
			                <button type="submit" class="btn btn-success">Abonar monto</button>	    
		                </div>
				    </div>
			    <?php else: ?>
		            Este cliente no posee deudas, o ya ha cancelado la totalidad de las mismas
	            <?php endif ?>
		    </div>
		</form>
	  </div>

	</div>
	<script>
		var plan = 0;
		$("#creditoCobro #clienteModal").html('<?= $cliente->row()->clientename ?>');
		$("#CreditoSaldo").html('<?= number_format($cliente->row()->saldo,0,',','.') ?>')
		$(".tab-pane#home table tr").on('click',function(){
			var id = $($(this).find('td')[0]).html();
			var cuota = $($(this).find('td')[1]).text();			
			var importe = $($(this).find('td')[3]).html();
			importe = importe.replace(/\./g,'');
			importe = importe.replace(/,/g,'.');
			if(!isNaN(parseInt(id))){
				$("#cargarPagoTabBtn").show();
				$('#cargarPagoTabBtn a[href="#profile"]').tab('show');
				$("#spanCuotaNumero").html('#'+cuota);
				$("#field-plan_credito_id").val(id);
				plan = id;
				$("#field-total_credito").val(importe);
				$("#field-total_pagado").val(importe);
			}
		});

		function pagarCredito(f){	
			sendForm('cajas/admin/abonarDeuda/<?= $_GET['creditos_id'] ?>',f,'#report-success',function(data){
				$('#report-success').html(data.success_message);
				if(data.success){
					verCliente('<?= $_GET['creditos_id'] ?>');
				}
			});
			return false;
		}
	</script>
	<?php else: ?>
		Cliente no encontrado
	<?php endif ?>
<?php endif ?>