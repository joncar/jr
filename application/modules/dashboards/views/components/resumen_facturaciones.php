<?php
    $dia = $this->db->query("SELECT 
        format(COALESCE(sum(ventadetalle.totalcondesc),0),0,'de_DE') as totalventa 
        FROM ventas 
        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
        WHERE ventas.status = 0 and date(ventas.fecha) ='".date("Y-m-d")."'")->row()->totalventa;

    $mes = $this->db->query("SELECT 
        format(COALESCE(sum(ventadetalle.totalcondesc),0),0,'de_DE') as totalventa 
        FROM ventas 
        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
        WHERE ventas.status = 0 and MONTH(ventas.fecha) ='".date("m")."'")->row()->totalventa;

    $year = $this->db->query("SELECT 
        format(COALESCE(sum(ventadetalle.totalcondesc),0),0,'de_DE') as totalventa 
        FROM ventas 
        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
        WHERE ventas.status = 0 and YEAR(ventas.fecha) ='".date("Y")."'")->row()->totalventa;
?>
<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="kt-portlet__head-title">Resumen de facturaciones</h3>
                </div>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                    </div>

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="kt-section">
                    <div class="widget-main">
                        <div class="row" style="margin-left: 0; margin-right: 0">
                            <div class="col-sm-6 col-md-4" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Por día</span>
    							      <div class="caption">
    							        <h4><?= $dia ?> Gs.</h4>               
    							      </div>
    							    </div>
    							</a>
    						</div>

    						<div class="col-sm-6 col-md-4" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Por mes</span>
    							      <div class="caption">
    							        <h4 title="<?= $mes ?>"><?= cortarNumero((int)str_replace('.','',$mes)) ?> Gs.</h4>       
    							      </div>
    							    </div>
    							</a>
    						</div>
    						<div class="col-sm-6 col-md-4" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Por año</span>
    							      <div class="caption">
    							        <h4 title="<?= $year ?>"><?= cortarNumero(str_replace('.','',$year)) ?> Gs.</h4>       
    							      </div>
    							    </div>
    							</a>
    						</div>
                        </div>

                    </div>
                </div>
            </div>
</div>
<script>

</script>
