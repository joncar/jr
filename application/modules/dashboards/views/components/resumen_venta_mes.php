<div id="resumenVentaMesContent">
<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $resumen = $this->db->query("
        SELECT 
        round(sum(ventames.Enero),0) as ENE, 
        round(sum(ventames.Febrero),0) as FEB, 
        round(sum(ventames.Marzo),0) as MAR, 
        round(sum(ventames.Abril),0) as ABR, 
        round(sum(ventames.Mayo),0) as MAY, 
        round(sum(ventames.Junio),0) as JUN, 
        round(sum(ventames.Julio),0) as JUL, 
        round(sum(ventames.Agosto),0) as AGO, 
        round(sum(ventames.Setiembre),0) as SEP, 
        round(sum(ventames.Octubre),0) as OCT, 
        round(sum(ventames.Noviembre),0) as NOV, 
        round(sum(ventames.Diciembre),0) as DIC 
        FROM (
        SELECT 
        date_format(ventas.fecha,'%m') as Mes, 
        date_format(ventas.fecha,'%Y') as Anho, 
        If(date_format(ventas.fecha,'%m') = 01,ventad.total_detalle,0) as Enero, 
        If(date_format(ventas.fecha,'%m') = 02,ventad.total_detalle,0) as Febrero, 
        If(date_format(ventas.fecha,'%m') = 03,ventad.total_detalle,0) as Marzo, 
        If(date_format(ventas.fecha,'%m') = 04,ventad.total_detalle,0) as Abril, 
        If(date_format(ventas.fecha,'%m') = 05,ventad.total_detalle,0) as Mayo, 
        If(date_format(ventas.fecha,'%m') = 06,ventad.total_detalle,0) as Junio, 
        If(date_format(ventas.fecha,'%m') = 07,ventad.total_detalle,0) as Julio, 
        If(date_format(ventas.fecha,'%m') = 08,ventad.total_detalle,0) as Agosto, 
        If(date_format(ventas.fecha,'%m') = 09,ventad.total_detalle,0) as Setiembre, 
        If(date_format(ventas.fecha,'%m') = 10,ventad.total_detalle,0) as Octubre, 
        If(date_format(ventas.fecha,'%m') = 11,ventad.total_detalle,0) as Noviembre, 
        If(date_format(ventas.fecha,'%m') = 12,ventad.total_detalle,0) as Diciembre,

        ventad.total_detalle as total 
        FROM ventas 
        INNER JOIN clientes on clientes.id = ventas.cliente 
        INNER JOIN (
        SELECT ventadetalle.venta as venta, sum(ventadetalle.totalcondesc) as total_detalle 
        FROM ventadetalle

        inner join productos on productos.codigo = ventadetalle.producto 
        GROUP BY ventadetalle.venta 
        ) as ventad on ventad.venta = ventas.id 
        WHERE ventas.status = 0 and year(ventas.fecha) = '".$year."') as ventames 
        GROUP by ventames.Anho
    ")->row();
?>

<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="kt-portlet__head-title">Resumen de ventas por mes (<?= $year ?>)</h3>
                </div>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" data-toggle="tab">Año</a>

                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <a class="dropdown-item" href="javascript:changeYearResumenVentaMes(<?= $i ?>)"><?= $i ?></a>
                            <?php endfor ?>     
                        </div>
                    </div>
                </div>
            </div>

           <div class="kt-portlet__body">
                <div class="kt-section">
                    <div class="widget-main">
                        <canvas id="resumenVentaMes" width="auto" height="300"></canvas>
                    </div>
                </div>
            </div>
</div>

<script>
    function loadCharResumenVentaMes(){        
        var chartContainer = KTUtil.getByID('resumenVentaMes');
        var chartData = {
            labels: <?php 
                $data = array();
                foreach($resumen as $n=>$v){
                    $data[] = $n;
                }
                echo json_encode($data);
              ?>,
            datasets: [{
                //label: 'Dataset 1',
                backgroundColor: KTApp.getStateColor('success'),
                data: <?php 
                    $data = array();
                    foreach($resumen as $n=>$v){
                        $data[] = $v;
                    }
                    echo json_encode($data);
                  ?>
            }]
        };
        var chart = new Chart(chartContainer, {
            type: 'bar',
            data: chartData,
            options: {
                title: {
                    display: true,
                },
                tooltips: {
                    intersect: false,
                    mode: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10
                },
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
                barRadius: 4,
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: false,
                        stacked: true
                    }],
                    yAxes: [{
                        display: true,
                        stacked: true,
                        gridLines: false
                    }]
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }
                }
            }
        });        
    }
    

    <?php if(!empty($_POST['year'])): ?>
        loadCharResumenVentaMes();
    <?php else: ?>
        window.afterLoad.push(loadCharResumenVentaMes);        
    <?php endif ?>

    function changeYearResumenVentaMes(y){
        $.post('dashboards/refresh/resumen_venta_mes',{year:y},function(data){
            $("#resumenVentaMesContent").html(data);
        });
    }
</script>
</div>