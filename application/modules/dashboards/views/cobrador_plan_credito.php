<style>
	h4{
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
	}
</style>
<?php get_instance()->js[] = '
	<script src="'.base_url() .'js/raphael-min.js"></script>
	<script src="'.base_url() .'js/prettify.min.js"></script>
	<script src="'.base_url() .'js/morris.js"></script>'; 
?>
<?php get_instance()->hcss[] = '
	<link rel="stylesheet" href="'.base_url().'css/prettify.min.css">
	<link rel="stylesheet" href="'.base_url().'css/morris.css">
'; ?>
<div class="row">
	<div class="col-12">
  		<?= $this->load->view('components/accesos_directos_cajeros',array(),TRUE,'dashboards'); ?>
  	</div>
</div>

<div class="row">
	<div class="col-12">
  		<?= $this->load->view('components/cobrador_plan_credito_clientes',array(),TRUE,'dashboards'); ?>
  	</div>
</div>