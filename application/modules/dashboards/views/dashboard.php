<div class="row">
	<div class="col-12">
  		<?= $this->load->view('components/accesos_directos',array(),TRUE,'dashboards'); ?>
  	</div>

</div>

<div class="row">
	<div class="col-12 col-md-8">		
		<?= $this->load->view('components/productos_bajo_stock',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-12 col-md-4">
		<?= $this->load->view('components/resumen_facturaciones',array(),TRUE,'dashboards') ?>
	</div>	
</div>

<div class="row">
	<div class="col-12 col-md-4">
		<?= $this->load->view('components/informaciones_generales',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-12 col-md-4">
		<?= $this->load->view('components/resumen_venta_mes',array(),TRUE,'dashboards') ?>
	</div>	
	<div class="col-12 col-md-4">
		<?= $this->load->view('components/compras_por_mes',array(),TRUE,'dashboards') ?>
	</div>		
</div>

<div class="row">	
	<div class="col-12 col-md-4">
		<?= $this->load->view('components/pago_por_fecha',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-12 col-md-4">
		<?= $this->load->view('components/totales_por_categoria',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-12 col-md-4">
		<?= $this->load->view('components/top_10_productos_mas_vendidos_admin',array(),TRUE,'dashboards') ?>
	</div>	
</div>
<div class="row">	
	<div class="col-12 col-md-4">		
		<?= $this->load->view('components/grafico_ventas_mes',array(),TRUE,'dashboards') ?>
	</div>	
	<div class="col-12 col-md-4">		
		<?= $this->load->view('components/totales_por_sucursal',array(),TRUE,'dashboards') ?>
	</div>
</div>
