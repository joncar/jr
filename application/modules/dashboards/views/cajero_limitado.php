<style>
	h4{
		white-space: nowrap;
		text-overflow: ellipsis;
		overflow: hidden;
	}
</style>
<script src="<?= base_url() ?>js/raphael-min.js"></script>
<script src="<?= base_url() ?>js/prettify.min.js"></script>
<script src="<?= base_url().'js/morris.js' ?>"></script>                                
<link rel="stylesheet" href="<?= base_url() ?>css/prettify.min.css">
<link rel="stylesheet" href="<?= base_url().'css/morris.css' ?>">
<div class="row">
	<div class="col-xs-12">
  		<?= $this->load->view('components/accesos_directos',array(),TRUE,'dashboards'); ?>
  	</div>
  	<div class="col-12 col-md-12">		
		<?= $this->load->view('components/productos_bajo_stock',array(),TRUE,'dashboards') ?>
	</div>

</div>