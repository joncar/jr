<div class="row">
	<div class="col-12">
  		<?= $this->load->view('components/accesos_directos',array(),TRUE,'dashboards'); ?>
  	</div>

</div>

<div class="row">
	<div class="col-12 col-md-8">		
		<?= $this->load->view('components/productos_bajo_stock',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-12 col-md-4">
		<?= $this->load->view('components/resumen_facturaciones',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-12 col-md-4">
		<?= $this->load->view('components/informaciones_generales',array(),TRUE,'dashboards') ?>
	</div>
	<div class="col-12 col-md-4">
		<?= $this->load->view('components/resumen_venta_mes',array(),TRUE,'dashboards') ?>
	</div>	
</div>
