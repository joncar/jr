<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Seguridad extends Panel{
        function __construct() {
            parent::__construct();
        }

        function menus(){
            $crud = $this->crud_function('','');  
            $crud->set_relation('menus_id','menus','nombre',array('menus_id'=>NULL));       
            $crud->columns('nombre','menus.url','tag','icono','menus_id','orden');          
            $crud->display_as('menus.url','URL')
                 ->callback_column('menus.url',function($val,$row){return $row->url;});
            $output = $crud->render();
            $this->loadView($output);
        }

        function ajustes(){
            $crud = $this->crud_function('','');            
            $crud->set_field_upload('logo','img/logos');
            $crud->set_field_upload('fondo','img/logos');
            $crud->set_field_upload('logo_login','img/logos');
            $crud->set_field_upload('favicon','img/logos');
            $crud->field_type('cod_balanza','tags');
            $crud->set_relation('sucursal_web','sucursales','denominacion');
            $crud->columns('id','tipo_codigo_barra','titulo_sistema','tasa_reales','tasa_dolares','tasa_pesos');
            $codigosBarras = array('C39','C39+','C39E','C93','S25','S25+','I25','I25+','C128','C128A','C128B','C128C','EAN2','EAN5','EAN8','EAN13','UPCA','UPCE','MSI','MSI+','POSTNET','PLANET','RMS4CC','KIX','IMB','CODABAR','CODE11','PHARMA','PHARMA2T');
            $crud->field_type('tipo_codigo_barra','enum',$codigosBarras);
            $crud->field_type('capturar_saldo_cliente','enum',['pagosclientes','facturas_clientes']);
            $crud->callback_after_update(function(){unset($_SESSION['ajustes']);});
            $crud->unset_add()                 
                 ->unset_delete();
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function grupos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('funciones','funcion_grupo','funciones','grupo','funcion','{nombre}');            
            $crud->set_relation_n_n('miembros','user_group','user','grupo','user','{nombre} {apellido}');
            $crud->columns('nombre');
            $output = $crud->render();
            $this->loadView($output);
        }               
        
        function funciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user($x = '',$y = ''){
            $this->norequireds = array('apellido_materno','foto');
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('estado_civil','enum',array('soltero'=>'Soltero','casado'=>'Casado','divorciado'=>'Divorciado','viudo'=>'Viudo'));
            $crud->field_type('sexo','enum',array('M'=>'M','F'=>'F'));
            $crud->field_type('password','password');
            $crud->field_type('repetir_password','password'); 
            $crud->field_type('fecha_registro','hidden'); 
            $crud->field_type('fecha_actualizacion','hidden'); 
            

            $crud->set_relation('cajas_id','cajas','denominacion');
            $crud->set_relation_dependency('cajas_id','sucursales_id','sucursal');           
            if($crud->getParameters()=='add'){
                $crud->set_rules('repetir_password','Repetir Password','required');            
                $crud->set_rules('cedula','Cedula','required|is_unique[user.cedula]');
            }
            $crud->unset_columns('password','foto');
            
            $crud->set_field_upload('foto','img/fotos');            
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_field('token',function($val){                
                return form_input('token',$val,'id="field-token" class="form-control"').'<a href="javascript:generarToken()">Generar</a>';
            }); 
            $dashs = array();
            foreach(scandir(APPPATH.'modules/dashboards/views') as $d){
                if($d!='.' && $d!='..' && strpos($d,'.php')){
                    $dashs[] = str_replace('.php','',$d);
                }
            }
            if($this->user->admin==1){
                $crud->add_action('<i class="fa fa-"></i> usar usuario','',base_url('seguridad/loginasuser').'/');
            }
            $crud->columns('foto','usuario','nombre','apellido','email','tipo_usuario','status','grupos');
            $crud->set_relation_n_n('grupos','user_group','grupos','user','grupo','nombre');
            $crud->field_type('pagina_principal','enum',$dashs);
            $output = $crud->render();
            $output->output = $this->load->view('user',['output'=>$output->output],TRUE);
            $this->loadView($output);
        }

        function loginasuser($id){
            if($this->user->admin==1 && is_numeric($id)){
                $userid = $this->user->id;                
                $this->user->unlog();
                $this->user->login_short($id);
                $_SESSION['admin'] = 1;
                if(!isset($_SESSION['useranterior'])){
                    $_SESSION['useranterior'] = $userid;
                }
                redirect('panel');
            }
        }
        
        function acciones($x = '',$y = ''){            
            $crud = $this->crud_function($x,$y);   
            $crud->set_clone();         
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function perfil($x = '',$y = ''){
            $this->as['perfil'] = 'user';
            $crud = $this->crud_function($x,$y);    
            $crud->where('id',$this->user->id);            
            $crud->field_type('password','password')
                 ->field_type('admin','hidden')
                 ->field_type('status','hidden')
                 ->field_type('pagina_principal','hidden')
                 ->field_type('sucursales_id','hidden')
                 ->field_type('cajas_id','hidden')
                 ->field_type('token','hidden')
                 ->field_type('api_token','hidden');
            $crud->set_field_upload('foto','img/fotos');
            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                    $post['password'] = md5($post['password']);                
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$id){
                $this->user->login_short($id);
            });
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user_insertion($post,$id = ''){
            if(!empty($id)){
                $post['pass'] = $this->db->get_where('user',array('id'=>$id))->row()->password!=$post['password']?md5($post['password']):$post['password'];
            }
            else $post['pass'] = md5($post['pass']);
            return $post;
        }

        function validarToken(){
            $response = ['result'=>'fail','msj'=>'Token incorrecto...'];
            if(!empty($_POST['token'])){
                $user = $this->db->get_where('user',['token'=>$_POST['token']]);
                if($user->num_rows()>0){
                    $response = ['result'=>'success','msj'=>''];
                }
            }
            echo json_encode($response);
        }
    }
?>
