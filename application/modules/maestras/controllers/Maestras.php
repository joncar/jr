<?php

require_once APPPATH.'/controllers/Panel.php';    

class Maestras extends Panel {

    function __construct() {
        parent::__construct();             
    }

    public function bancos($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);        
        $output = $crud->render();            
        $this->loadView($output);
    }

     public function tipo_contador($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->add_action('Detalles','',base_url('maestras/contadores/').'/');
        $output = $crud->render();            
        $this->loadView($output);
    }

    public function empresas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);        
        $output = $crud->render();            
        $this->loadView($output);
    }

    public function tipo_pago($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);        
        $output = $crud->render();            
        $this->loadView($output);
    }
    public function cantidades_mayoristas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->field_type('campo','dropdown',array('precio_venta_mayorista1'=>'Precio mayorista 1','precio_venta_mayorista2'=>'Precio mayorista 2','precio_venta_mayorista3'=>'Precio mayorista 3'));
        $output = $crud->render();            
        $this->loadView($output);
    }

    public function contadores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->where('tipo_contador_id',$x)
             ->field_type('tipo_contador_id',$x)
             ->unset_columns('tipo_contador_id');
        $output = $crud->render();        
        $this->loadView($output);
    }
    
    public function paises($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }

    public function ciudades($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('pais', 'paises', 'denominacion');
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }

    public function barrios($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('pais', 'paises', 'denominacion');
        $crud->set_relation('ciudad', 'ciudades', 'denominacion');
        $output = $crud->render();        
        $this->loadView($output);
    }

    public function zonas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $output = $crud->render();        
        $this->loadView($output);
    }

    public function forma_pago($x = '', $y = '') {
        $this->as['forma_pago'] = 'formapago';
        $crud = parent::crud_function($x, $y);
        $crud->unset_add()
             ->unset_delete();
        $crud->display_as('por_venta','Porcentaje de venta');
        $output = $crud->render();        
        $this->loadView($output);
    }
    
    public function proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('ciudad', 'ciudades', 'denominacion');
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('tipo_proveedor', 'tipo_proveedores', 'denominacion');
        $crud->field_type('created','hidden',date("Y-m-d H:i:s"))
             ->field_type('modified','hidden',date("Y-m-d H:i:s"));
        //$crud->required_fields('denominacion', 'ruc', 'direccion', 'ciudad', 'pais_id', 'telefonofijo', 'telefax', 'celular', 'email');
        //$crud->unset_fields('created', 'modified');
        $crud->field_type('usuario_id', 'hidden', $_SESSION['user']);
        $crud->columns('denominacion', 'tipo_proveedor', 'ruc', 'telefonofijo', 'celular', 'Observacion');
        $crud->unset_delete();
        $crud->where('(anulado = 0 OR anulado IS NULL)','ESCAPE',TRUE);
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function sucursales($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('ciudad_id', 'ciudades', 'denominacion');
        $crud->set_relation_dependency('ciudad_id', 'pais_id', 'pais');
        $crud->field_type('principal', 'true_false', array('0' => 'No', '1' => 'Si'));

        $crud->callback_before_insert(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $crud->callback_before_update(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function getUltimoCliente(){
        if(!empty($_SESSION['refreshClientes'])){ //variable que viene desde añadir clientes
            $this->db->order_by('id','DESC');
            $clientes = $this->db->get_where('clientes',array('inactivo'=>0));
            $cliente = array();
            if($clientes->num_rows()>0){
                $cliente = $clientes->row();
            }
            $cliente->refresh = true;
            echo json_encode($cliente);
        }
    }
    
    public function clientes($x = '', $y = '', $return = false) {
        $crud = parent::crud_function($x, $y);        
        $crud->field_type('tipo_doc', 'hidden','')            
            ->set_relation('pais','paises','denominacion')
            ->set_relation('ciudad','ciudades','denominacion')
            ->set_relation('barrio','barrios','denominacion')                      
            ->field_type('celular2', 'hidden','')
            ->field_type('celular3', 'hidden','')        
            ->field_type('foto','image',array('path'=>'img/clientes','width'=>'50px','height'=>'50px'));  
        $crud->columns('id','nombres','apellidos','nro_documento','celular','barrio','ciudad','mayorista','promo');
        $crud->callback_column('promo',function($val,$row){
            return !$val?0:1;
        });
        $crud->required_fields('nombres','apellidos');
        $crud->set_rules('email', 'Email', 'valid_email');
        $crud->unset_delete();
        
        if(in_array(3,$this->user->getGroupsArray())){ //Desabilitar para cajeros
            $crud->unset_edit()
                 ->unset_delete();
        } 
        
        if ($x == 'add' || $x == 'insert' || $x == 'insert_validation') {
            $crud->set_rules('nro_documento', 'Cedula', 'required|is_unique[clientes.nro_documento]');
        }        
        if (!empty($x) && !empty($y) && $y == 'json') {
            $crud->unset_back_to_list();            
        }
        if($x=='json_list'){
            $crud->limit(100);
        }        
        if(!empty($_POST['search_text']) && $x=='json_list'){
            $crud->where("CONCAT(nombres,' ',apellidos) LIKE '%".$_POST['search_text']."%'",'ESCAPE',FALSE);
        }
        if(!empty($_POST['clientes_id']) && $x=='json_list'){
            $crud->where("clientes.id",$_POST['clientes_id']);
        }
        $crud->unset_delete();
        $crud->where('(clientes.anulado = 0 OR clientes.anulado IS NULL)','ESCAPE',TRUE);
        $crud->replace_form_add('cruds/clientes/add','maestras');
        $crud->replace_form_edit('cruds/clientes/edit','maestras');
        $output = $crud->render();
        $output->crud = 'clientes';
        $this->loadView($output);
    }
    
    public function monedas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->field_type('primario', 'true_false', array('0' => 'No', '1' => 'Si'));
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'user';
        $this->loadView($output);
    }
    
    public function motivo_salida($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function motivo_entrada($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function tipo_proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }
    
    public function cuentas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);        
        $output = $crud->render();
        $this->loadView($output);
    }  
    
    function condiciones(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Condiciones de pago');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Condiciones de pago';
            $this->loadView($crud);
        }
        function estados(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Estados de ocupacion');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Estados de ocupacion';
            $this->loadView($crud);
        }     

    public function unidad_medida($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);    
        $crud->set_subject('Unidades de medida');
        $crud->columns('id','denominacion');
        if($crud->getParameters()=='add'){
            $crud->set_rules('denominacion','Denominación','required|is_unique[unidad_medida.denominacion]');
        }
        $output = $crud->render();            
        $output->title = 'Unidades de medida';
        $this->loadView($output);
    }   

    function mesas(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Mesa');
            /*$crud->unset_fields('estado');*/
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Mesas del restaurant';
            $this->loadView($crud);
        }
        
        function tipo_pedidos(){
            $crud = $this->crud_function('','');
            $crud->set_subject('tipo de pedidos');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Administrar de tipos de pedidos';
            $this->loadView($crud);
        }
        
        function mozos(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Mozo');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Administrar mozos';
            $this->loadView($crud);
        }
        
        function deliverys(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Delivery');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Administrar deliverys';
            $this->loadView($crud);
        }
        
        function barras(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Barra');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Administrar barras';
            $this->loadView($crud);
        }

        function tipo_facturacion(){
            $crud = $this->crud_function('','');            
            $crud->unset_delete();
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function sub_categoria_producto(){
            $crud = $this->crud_function('',''); 
            $crud->columns('id','nombre_sub_categoria_producto');           
            $crud->unset_delete();
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function sub_sub_categoria(){
            $crud = $this->crud_function('','');  
            $crud->columns('id','nombre_sub_sub_categoria');                     
            $crud->unset_delete();
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function repartidores(){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete();             
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        function Ruc_py(){
            $crud = $this->crud_function('',''); 
            $crud->set_primary_key('id');
            $crud->set_subject('Ruc Paraguay');
            $crud->unset_delete();             
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        function sucursales_transferencias(){
            $crud = $this->crud_function('',''); 
            $crud->set_relation('sucursal_id','sucursales','denominacion')
                 ->set_relation('sucursal_destino','sucursales','denominacion');
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function clientes_bidones(){
            $crud = $this->crud_function('',''); 
            $crud->set_relation('clientes_id','clientes','{nro_documento} {nombres} {apellidos}');
            $crud->field_type('fecha_mod','invisible',date("Y-m-d H:i:s"));
            $crud->field_type('usuario','invisible',$this->user->id);
            $crud->callback_before_insert(function($post){
                $post['fecha_mod'] = date("Y-m-d H:i:s");
                $post['usuario'] = $this->user->id;
                return $post;
            });
            $crud->callback_before_update(function($post){
                $post['fecha_mod'] = date("Y-m-d H:i:s");
                $post['usuario'] = $this->user->id;
                return $post;
            });
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function bodegas(){
            $crud = $this->crud_function('',''); 
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
