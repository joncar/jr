<div class="row">
	<div class="col-12 col-md-6">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						<b>Datos del cliente</b>
					</h1>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div class="kt-section">
					<div class="row">
						<div class="col-12 col-md-6">
							<div class="form-group" id="foto_field_box">
								<label for='field-foto' id="foto_display_as_box">
									<?php echo $input_fields['foto']->display_as; ?>
									<?php echo ($input_fields['foto']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['foto']->input ?></div>
					        </div>
					        <div class="form-group" id="empresas_id_field_box">
								<label for='field-empresas_id' id="empresas_id_display_as_box">
									<?php echo $input_fields['empresas_id']->display_as; ?>
									<?php echo ($input_fields['empresas_id']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['empresas_id']->input ?></div>
					        </div>

                			<div class="form-group" id="user_id_field_box">
								<label for='field-user_id' id="user_id_display_as_box">
									<?php echo $input_fields['user_id']->display_as; ?>
									<?php echo ($input_fields['user_id']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['user_id']->input ?></div>
					        </div>
					        <div class="form-group" id="limite_credito_field_box">
								<label for='field-limite_credito' id="limite_credito_display_as_box">
									<?php echo $input_fields['limite_credito']->display_as; ?>
									<?php echo ($input_fields['limite_credito']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['limite_credito']->input ?></div>
					        </div>					        
                			<div class="form-group" id="mayorista_field_box">
								<label for='field-mayorista' id="mayorista_display_as_box">
									<?php echo $input_fields['mayorista']->display_as; ?>
									<?php echo ($input_fields['mayorista']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['mayorista']->input ?></div>
					        </div>
							<div class="form-group" id="promo_field_box">
								<label for='field-promo' id="promo_display_as_box">
									<?php echo $input_fields['promo']->display_as; ?>
									<?php echo ($input_fields['promo']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['promo']->input ?></div>
					        </div>
                			
				        </div>
				        <div class="col-12 col-md-6">
				        	<div class="form-group" id="nombres_field_box">
								<label for='field-nombres' id="nombres_display_as_box">
									<?php echo $input_fields['nombres']->display_as; ?>
									<?php echo ($input_fields['nombres']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['nombres']->input ?></div>
					        </div>
			                <div class="form-group" id="apellidos_field_box">
								<label for='field-apellidos' id="apellidos_display_as_box">
									<?php echo $input_fields['apellidos']->display_as; ?>
									<?php echo ($input_fields['apellidos']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['apellidos']->input ?></div>
					        </div>
			                <div class="form-group" id="nro_documento_field_box">
								<label for='field-nro_documento' id="nro_documento_display_as_box">
									<?php echo $input_fields['nro_documento']->display_as; ?>
									<?php echo ($input_fields['nro_documento']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['nro_documento']->input ?></div>
					        </div>
			                <div class="form-group" id="fecha_nac_field_box">
								<label for='field-fecha_nac' id="fecha_nac_display_as_box">
									<?php echo $input_fields['fecha_nac']->display_as; ?>
									<?php echo ($input_fields['fecha_nac']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['fecha_nac']->input ?></div>
					        </div>
                			<div class="form-group" id="sexo_field_box">
								<label for='field-sexo' id="sexo_display_as_box">
									<?php echo $input_fields['sexo']->display_as; ?>
									<?php echo ($input_fields['sexo']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['sexo']->input ?></div>
					        </div>
					        <div class="form-group" id="tipo_pago_id_field_box">
								<label for='field-tipo_pago_id' id="tipo_pago_id_display_as_box">
									<?php echo $input_fields['tipo_pago_id']->display_as; ?>
									<?php echo ($input_fields['tipo_pago_id']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['tipo_pago_id']->input ?></div>
					        </div>
					        <div class="form-group" id="empleado_field_box">
								<label for='field-empleado' id="empleado_display_as_box">
									<?php echo $input_fields['empleado']->display_as; ?>
									<?php echo ($input_fields['empleado']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['empleado']->input ?></div>
					        </div>
					        <div class="form-group" id="anulado_field_box">
								<label for='field-anulado' id="anulado_display_as_box">
									<?php echo $input_fields['anulado']->display_as; ?>
									<?php echo ($input_fields['anulado']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['anulado']->input ?></div>
					        </div>
							<div class="form-group" id="plan_credito_field_box">
								<label for='field-plan_credito' id="plan_credito_display_as_box">
									<?php echo $input_fields['plan_credito']->display_as; ?>
									<?php echo ($input_fields['plan_credito']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['plan_credito']->input ?></div>
					        </div>
				        </div>
			        </div>
		    	</div>

			</div>
		</div>
	</div>
	<div class="col-12 col-md-6">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						<b>Dirección</b>
					</h1>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div class="kt-section">
					<div class="row">
						<div class="col-12 col-md-6">
							<div class="form-group" id="pais_field_box">
								<label for='field-pais' id="pais_display_as_box">
									<?php echo $input_fields['pais']->display_as; ?>
									<?php echo ($input_fields['pais']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['pais']->input ?></div>
					        </div>
			                <div class="form-group" id="barrio_field_box">
								<label for='field-barrio' id="barrio_display_as_box">
									<?php echo $input_fields['barrio']->display_as; ?>
									<?php echo ($input_fields['barrio']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['barrio']->input ?></div>
					        </div>
	                	</div>
						<div class="col-12 col-md-6">
							<div class="form-group" id="ciudad_field_box">
								<label for='field-ciudad' id="ciudad_display_as_box">
									<?php echo $input_fields['ciudad']->display_as; ?>
									<?php echo ($input_fields['ciudad']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['ciudad']->input ?></div>
					        </div>
			                <div class="form-group" id="zonas_id_field_box">
								<label for='field-zonas_id' id="zonas_id_display_as_box">
									<?php echo $input_fields['zonas_id']->display_as; ?>
									<?php echo ($input_fields['zonas_id']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['zonas_id']->input ?></div>
					        </div>
						</div>

						<div class="col-12 col-md-6">
							<div class="form-group" id="celular_field_box">
								<label for='field-celular' id="celular_display_as_box">
									<?php echo $input_fields['celular']->display_as; ?>
									<?php echo ($input_fields['celular']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['celular']->input ?></div>
					        </div>
					    </div>
					    <div class="col-12 col-md-6">
							<div class="form-group" id="lugar_trabajo_field_box">
								<label for='field-lugar_trabajo' id="lugar_trabajo_display_as_box">
									<?php echo $input_fields['lugar_trabajo']->display_as; ?>
									<?php echo ($input_fields['lugar_trabajo']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['lugar_trabajo']->input ?></div>
					        </div>
					    </div>
					    <div class="col-12 col-md-6">
							<div class="form-group" id="direccion_field_box">
								<label for='field-direccion' id="direccion_display_as_box">
									<?php echo $input_fields['direccion']->display_as; ?>
									<?php echo ($input_fields['direccion']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['direccion']->input ?></div>
					        </div>
				    	</div>
						<div class="col-12 col-md-12">
							<div class="form-group" id="observaciones_field_box">
								<label for='field-observaciones' id="observaciones_display_as_box">
									<?php echo $input_fields['observaciones']->display_as; ?>
									<?php echo ($input_fields['observaciones']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['observaciones']->input ?></div>
					        </div>
						</div>
					</div>
				</div>
			</div>
		</div> <!--- End ubicacion -->

		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						<b>Contacto</b>
					</h1>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div class="kt-section">
					<div class="row">
						<div class="col-12 col-md-3">
							<div class="form-group" id="nombre_contacto_field_box">
								<label for='field-nombre_contacto' id="nombre_contacto_display_as_box">
									<?php echo $input_fields['nombre_contacto']->display_as; ?>
									<?php echo ($input_fields['nombre_contacto']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['nombre_contacto']->input ?></div>
					        </div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group" id="email_field_box">
								<label for='field-email' id="email_display_as_box">
									<?php echo $input_fields['email']->display_as; ?>
									<?php echo ($input_fields['email']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['email']->input ?></div>
					        </div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group" id="celular_contacto_field_box">
								<label for='field-celular_contacto' id="celular_contacto_display_as_box">
									<?php echo $input_fields['celular_contacto']->display_as; ?>
									<?php echo ($input_fields['celular_contacto']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['celular_contacto']->input ?></div>
					        </div>
						</div>
						<div class="col-12 col-md-3">
							<div class="form-group" id="cedula_contacto_field_box">
								<label for='field-cedula_contacto' id="cedula_contacto_display_as_box">
									<?php echo $input_fields['cedula_contacto']->display_as; ?>
									<?php echo ($input_fields['cedula_contacto']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['cedula_contacto']->input ?></div>
					        </div>
						</div>
						<div class="col-12 col-md-12">
							<div class="form-group" id="direccion_field_box">
								<label for='field-direccion' id="direccion_display_as_box">
									<?php echo $input_fields['direccion']->display_as; ?>
									<?php echo ($input_fields['direccion']->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					            <div><?php echo $input_fields['direccion']->input ?></div>
					        </div>
						</div>
					</div>
				</div>
			</div>
		</div><!--- EnD contacto --->


	</div>
</div>