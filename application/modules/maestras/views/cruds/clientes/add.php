<?php   
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-add.js?v=1.1"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';        
    get_instance()->hcss[] = '<style>
    	a.chzn-single span{
    		white-space: normal !important;
    	}
	</style>';
?>

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
<?php echo form_open( $insert_url, 'method="post" onsubmit="save(this); return false;" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>

	<?php $this->load->view('campos',[]); ?>

<div class="row">
	<div class="col-12">
		<div class="report"></div>
	</div>	
	<div class="col-12">
		<button type='submit' class="btn btn-success">Guardar</button>
        <button type="button" id="cancel-button" class="btn btn-danger">Cancelar</button>
	</div>
</div>

<?php echo form_close(); ?>
<script>
	window.afterLoad.push(function(){
		$("input[name='plan_credito'][value='<?= $this->ajustes->plan_credito ?>']").prop('checked',true);
	});
</script>