<?php

require_once APPPATH.'/controllers/Panel.php';    

class Sport extends Panel {

    function __construct() {
        parent::__construct();             
    }
        
    public function sport_canchas($x = '', $y = ''){
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('productos_id','productos','{codigo} {nombre_comercial}');
        $crud->set_relation_n_n('Tarifas','sport_canchas_tarifas','sport_tarifas','sport_canchas_id','sport_tarifas_id' , 'denominacion');
        $output = $crud->render();
        $this->loadView($output);
    }
    public function sport_tarifas($x = '', $y = ''){
        $crud = parent::crud_function($x, $y);
        $crud->set_relation_n_n('Canchas','sport_canchas_tarifas','sport_canchas','sport_tarifas_id','sport_canchas_id' , 'denominacion');
        $output = $crud->render();
        $this->loadView($output);
    }

    public function sport_reservas_canchas($x = '', $y = ''){
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap2_horizontal');  
        $crud->field_type('user_id','hidden',$this->user->id);
        $crud->field_type('fecha','fecha');
        $crud->field_type('facturado','true_false',['0'=>'NO','1'=>'SI']);
        $crud->field_type('desde_hora','time');
        $crud->field_type('hasta_hora','time');
        $crud->add_action('Facturar','',base_url('movimientos/ventas/ventas/add/Sport').'/');
        $crud->set_relation('clientes_id','clientes','{nro_documento} {nombres} {apellidos}');
        if($crud->getParameters()=='add'){
            $crud->field_type('anulado','hidden',0);
        }
        if($crud->getParameters()!='edit'){
            $crud->set_rules('hasta_hora','Hasta','required|callback_validarReservas');    
        }

        $crud->callback_column('facturado',function($val,$row){

            return '<div class="facturado '.($val==1?'danger':'').'">'.['0'=>'NO','1'=>'SI'][$val].'</div>';
        });
        
        $crud->search_types = [
            'fecha'=>'<input type="hidden" name="search_text[]"><input type="date" name="fecha" class="form-control">',
            'desde_hora'=>'<input type="hidden" name="search_text[]"><input name="desde" type="time" class="form-control">',
            'hasta_hora'=>'<input type="hidden" name="search_text[]"><input name="hasta" type="time" class="form-control">'
        ];
        if(!empty($_POST['fecha']) && !empty($_POST['desde']) && !empty($_POST['hasta'])){
            $crud->where("
            (
                '{$_POST['fecha']} {$_POST['desde']}' BETWEEN CAST(CONCAT(fecha,' ',desde_hora) as datetime) AND CAST(CONCAT(fecha,' ',hasta_hora) as datetime) OR 
                '{$_POST['fecha']} {$_POST['hasta']}' BETWEEN CAST(CONCAT(fecha,' ',desde_hora) as datetime) AND CAST(CONCAT(fecha,' ',hasta_hora) as datetime)
            )
            ","ESCAPE",TRUE);
        }
        $crud->order_by('sport_reservas_canchas.id','DESC');
        $crud->search_types = [
            'facturado'=>'<input type="hidden" name="search_text[]">'.form_dropdown('facturado',[''=>'---','0'=>'NO','1'=>'SI'],'','class="form-control"')
        ];
        if(isset($_POST['facturado']) && is_numeric($_POST['facturado'])){
            $crud->where("(sport_reservas_canchas.facturado = '".$_POST['facturado']."')","ESCAPE",TRUE);
        }
        $output = $crud->render();
        if($crud->getParameters()=='list'){
            $output->output = $this->load->view('reservas',['output'=>$output->output],TRUE);
        }else{
            $output->output.= $this->load->view('reservas_form',[],TRUE);
        }
        $this->loadView($output);
    }

    function validarReservas(){
        $interes = strtotime($_POST['fecha'].' '.$_POST['desde_hora'].' +1 minute');
        $interes = date("Y-m-d H:i:s",$interes);
        $reservas = $this->db->query("SELECT * FROM (SELECT id,sport_canchas_id,CAST(CONCAT(fecha,' ',desde_hora) as datetime) as desde,CAST(CONCAT(fecha,' ',hasta_hora) as datetime) as hasta FROM `sport_reservas_canchas`) as res
            WHERE sport_canchas_id = '{$_POST['sport_canchas_id']}' AND ('{$interes}' BETWEEN res.desde AND res.hasta OR '{$_POST['fecha']} {$_POST['hasta_hora']}'  BETWEEN res.desde AND res.hasta)
        ");
        if($reservas->num_rows()>0){
            $this->form_validation->set_message('validarReservas','La fecha indicada y a ha sido reservada, por favor intente con otra');
            return false;
        }
    }
    
}