<script>
    window.tarifas = <?php 
        $this->db->select('sport_canchas_tarifas.*,sport_tarifas.monto,sport_tarifas.minutos');
        $this->db->join('sport_tarifas','sport_tarifas.id = sport_canchas_tarifas.sport_tarifas_id');
        echo json_encode($this->db->get_where('sport_canchas_tarifas',['sport_canchas_tarifas.vigente'=>1])->result()) 
    ?>;
    window.afterLoad.push(function(){
        $('#field-fecha,#field-desde_hora,#field-hasta_hora').on('change',cant_horas);
    });

    function cant_horas(){
       let fecha = $("#field-fecha").val();
       let desde = $("#field-desde_hora").val();
       let hasta = $("#field-hasta_hora").val();
       if(fecha!='' && desde!='' && hasta!=''){
            let _desde = new Date(`${fecha}T${desde}:00`)
            let _hasta = new Date(`${fecha}T${hasta}:00`)
            let minutos = Math.ceil((_hasta-_desde)/1000/60);
            $('#field-cant_minutos').val(minutos);
       }
       total();
    }

    function total(){
        let cancha = $("#field-sport_canchas_id").val();
        let minutos = $('#field-cant_minutos').val();
        let monto = 0;
        $('#field-monto_a_cobrar').val(monto);
        if(cancha!='' && minutos!=''){
            minutos = parseInt(minutos);
            if(isNaN(minutos)){
                return;
            }
            let tarifa = tarifas.filter(x=>x.sport_canchas_id==cancha);
            if(!tarifa){
                return;
            }
            
            tarifa.map(x=>{
                let min = parseInt(x.minutos);
                if(!isNaN(min) && minutos>=min){
                    let mnt = Math.ceil(minutos/min)*x.monto;
                    $('#field-monto_a_cobrar').val(mnt);
                }
            })
        }
    }
</script>