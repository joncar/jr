<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <button class="nav-link active" id="home-tab" data-toggle="tab" data-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Calendario</button>
  </li>
  <li class="nav-item" role="presentation">
    <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Listado</button>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
    <div id="root"></div>
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <?= $output ?>
  </div>
</div>

<?php 
    get_instance()->js[] = '<script type="module" crossorigin src="'.base_url('sportCalendar/public/js/app-index.js').'?v='.time().'"></script>';
?>

<script>
  window.afterLoad.push(function(){
    //#fd397a
    $('.flexigrid').on('list_loaded',function(){
      $('.flexigrid table .facturado.danger').parents('tr').css({
        'background':'#fd397a',
        'color':'#fff'
      });
    })
  });
</script>