<?php

require_once APPPATH.'/controllers/Panel.php';    

class Admin extends Panel {

    function __construct() {
        parent::__construct();
        if(($this->router->fetch_method()=='cajadiaria' || $this->router->fetch_method()=='cajas') && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()=='cajadiaria' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
    }
    
    public function timbrados($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);        
        $crud->set_relation('sucursales_id','sucursales','denominacion');
        $crud->set_relation('cajas_id','cajas','denominacion');
        $crud->set_relation_dependency('cajas_id','sucursales_id','sucursal');
        if($crud->getParameters()=='add'){
            $crud->set_rules('nro_timbrado','Numero de timbrado','required');
        }
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function cajas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->fields('sucursal', 'denominacion', 'serie','cerrarAlImprimirVenta','tipo_facturacion_id','tipotransaccion_id','solicitar_token','factura_nota_credito_cliente','ventastemp','permitir_descuento','tipo_venta','max_presupuesto','bloquear_ingreso');
        $crud->field_type('abierto', 'true_false', array('0' => 'No', '1' => 'Si'));
        $crud->add_action('Cajas diarias','',base_url('cajas/admin/vercajadiaria').'/');
        $crud->callback_before_update(function($post){
            $post['user_modified'] = get_instance()->user->id;
            return $post;
        });
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }

    public function timbrados_nota_remision($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);   
        $crud->set_subject('Timbrado Nota de Remisión');     
        $crud->set_relation('sucursales_id','sucursales','denominacion');
        $crud->set_relation('cajas_id','cajas','denominacion');
        $crud->set_relation_dependency('cajas_id','sucursales_id','sucursal');
        if($crud->getParameters()=='add'){
            $crud->set_rules('nro_timbrado','Numero de timbrado','required');
        }
        $crud->unset_delete();
        $crud->display_as('sucursales_id','Sucursal')
             ->display_as('cajas_id','Caja');        
        $output = $crud->render();        
        $output->title = 'Timbrado Nota de Remisión';
        $this->loadView($output);
    }

    public function vercajadiaria($x = '', $y = '') {
        $this->as['vercajadiaria'] = 'cajadiaria';
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->where('caja',$x);
        $crud->order_by('cajadiaria.id','DESC');
        $crud->unset_delete()
             ->unset_add()
             ->unset_edit()             
             ->unset_print()
             ->unset_export()
             ->field_type('abierto','true_false',array('0'=>'NO','1'=>'SI'))
             ->columns('id','timbrados_id','fecha_apertura','fecha_cierre','usuario','monto_inicial','total_ventas','total_contado','total_pago_clientes','total_credito','total_egreso','efectivoarendir','abierto');
        $crud->callback_column('monto_inicial',array($this,'formatear'))
             ->callback_column('total_ventas',array($this,'formatear'))
             ->callback_column('total_contado',array($this,'formatear'))
             ->callback_column('total_pago_clientes',array($this,'formatear'))
             ->callback_column('total_credito',array($this,'formatear'))
             ->callback_column('total_egreso',array($this,'formatear'))
             ->callback_column('efectivoarendir',array($this,'formatear'));
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->set_relation('caja', 'cajas', 'denominacion', array('abierto' => 0));
        $crud->set_relation('usuario', 'user', '{nombre} {apellido}');
        $output = $crud->render();
        $this->loadView($output);
    }

    function formatear($val){
        return number_format($val,0,',','.').' GS';
    }

    public function cajadiaria($x = '', $y = '', $z = '') { 
        if($x=='abrir'){
            if($this->user->admin==1){
                $this->db->update('cajadiaria',['abierto'=>1],['id'=>$y]);
            }
            redirect('cajas/admin/cajadiaria');
            return;
        }               
        $crud = parent::crud_function($x, $y);        
        $crud->unset_delete();        
        if (!empty($_SESSION['caja']) && $this->db->get_where('cajadiaria', array('caja' => $_SESSION['caja'], 'sucursal' => $_SESSION['sucursal'], 'abierto' => 1))->num_rows() > 0) {
            $crud->unset_add();
            $crud->where('cajadiaria.abierto', 1);
        }else{
            if($this->ajustes->permitir_reapertura_caja==1){
                $crud->add_action('Reaperturar caja','',base_url('cajas/admin/cajadiaria/abrir').'/');
            }
        }
        $crud->field_type('fecha_apertura', 'hidden',date('Y-m-d H:i:s'))             
                ->field_type('efectivoarendir', 'invisible')
                ->field_type('total_descuentos', 'invisible')
                ->field_type('total_debito', 'invisible')
                ->field_type('total_cheque', 'invisible')
                ->field_type('fecha_cierre', 'invisible')
                ->field_type('total_contado', 'invisible')
                ->field_type('total_pago_clientes', 'invisible')
                ->field_type('total_credito', 'invisible')
                ->field_type('total_egreso', 'invisible')                
                ->field_type('total_ventas', 'invisible')
                ->field_type('total_entrega_credito', 'invisible')
                ->field_type('total_servicios', 'invisible')                
                ->field_type('correlativo', 'hidden')
                ->field_type('nota_cred_correlativo', 'hidden')
                ->field_type('sucursal', 'hidden', $_SESSION['sucursal'])
                ->field_type('caja', 'hidden', @$_SESSION['caja'])
                ->field_type('usuario', 'hidden', $_SESSION['user'])
                ->display_as('timbrados_id','Numero de timbrado');

        if ($crud->getParameters() == 'edit') {
            $crud->field_type('correlativo', 'input');
        }
        $crud->display_as('abierto', 'Tipo de apertura');
        if ($crud->getParameters() == 'add') {
            $crud->field_type('abierto', 'hidden',1);
        } else {
            $crud->field_type('abierto', 'true_false', array('0' => 'Cerrado', '1' => 'Abierto'));
        }

        if($this->user->admin==0){
            $crud->field_type('monto_inicial','hidden');
        }

        if(is_numeric($y) && $this->db->get_where('cajadiaria',array('id'=>$y,'abierto'=>1))->num_rows()==0){
            redirect('cajas/admin/cajadiaria');
        }

        $crud->callback_add_field('correlativo', function($val) {
            get_instance()->db->order_by('correlativo', 'DESC');
            $aperturas = get_instance()->db->get_where('cajadiaria', array('sucursal' => $_SESSION['sucursal'], 'caja' => $_SESSION['caja']));
            $correlativo = $aperturas->num_rows() == 0 ? '1' : $aperturas->row()->correlativo;
            return '<input type="hidden" name="correlativo" value="'.$correlativo.'" id="field-correlativo" class="form-control">';
        });

        $crud->callback_add_field('nota_cred_correlativo', function($val) {
            get_instance()->db->order_by('nota_cred_correlativo', 'DESC');
            $aperturas = get_instance()->db->get_where('cajadiaria', array('sucursal' => $_SESSION['sucursal'], 'caja' => $_SESSION['caja']));
            $correlativo = $aperturas->num_rows() == 0 ? '1' : $aperturas->row()->nota_cred_correlativo;
            return '<input type="hidden" name="nota_cred_correlativo" value="'.$correlativo.'" id="field-nota_cred_correlativo" class="form-control">';
        });

        $crud->callback_before_insert(function($post) {
            $post['abierto'] = '1';
            $post['total_efectivo'] = $post['monto_inicial'];
            $post['fecha_apertura'] = date("Y-m-d H:i:s");            
            get_instance()->db->update('cajas', array('abierto' => 1), array('id' => $post['caja']));
            return $post;
        });
        $crud->callback_after_insert(function($post,$primary){
            $_SESSION['cajadiaria'] = $primary;
        });        
        $crud->callback_after_update(function($post,$primary){
            if(get_instance()->x == 'cerrar'){
                $this->db->query('CALL cerrarCaja('.$primary.')');
                unset($_SESSION['cajadiaria']);            
            }
        });

        $crud->set_relation('timbrados_id','timbrados','nro_timbrado',['vigencia_hasta >='=>date("Y-m-d"),'sucursales_id'=>$this->user->sucursal,'cajas_id'=>$this->user->caja,'vigente'=>1]);
        $crud->set_rules('timbrados_id','Numero de timbrado','required|callback_validarTimbrado');
        if ($x != 'cerrar' && ($crud->getParameters(FALSE) == 'list' || $crud->getParameters(FALSE) == 'ajax_list_info' || $crud->getParameters(FALSE) == 'ajax_list' || $crud->getParameters(FALSE) == 'success')) {
            $crud->set_relation('sucursal', 'sucursales', 'denominacion');
            $crud->set_relation('caja', 'cajas', 'denominacion', array('abierto' => 0));
            $crud->set_relation('usuario', 'user', '{nombre} {apellido}');            
            $crud->set_relation('timbrados_remision','timbrados_nota_remision','nro_timbrado',['vigencia_hasta >='=>date("Y-m-d"),'sucursales_id'=>$this->user->sucursal,'cajas_id'=>$this->user->caja]);
            $crud->set_relation_dependency('caja', 'sucursal', 'sucursal');
            $crud->columns('id','sucursal','caja','fecha_apertura','monto_inicial','correlativo','efectivoarendir','abierto');
        }        
        $crud->callback_column('efectivoarendir',array($this,'formatear_read_cierre'));
        $crud->callback_column('monto_inicial',array($this,'formatear_read_cierre'));
        if($crud->getParameters()=='read'){
            $crud->callback_field('efectivoarendir',array($this,'formatear_read_cierre'));
            $crud->callback_field('total_credito',array($this,'formatear_read_cierre'));
            $crud->callback_field('total_contado',array($this,'formatear_read_cierre'));
            $crud->callback_field('total_egreso',array($this,'formatear_read_cierre'));
            $crud->callback_field('total_pago_clientes',array($this,'formatear_read_cierre'));
            $crud->callback_field('total_ventas',array($this,'formatear_read_cierre'));
            $crud->callback_field('monto_inicial',array($this,'formatear_read_cierre'));
        }
        $crud->order_by('id','desc');
        $crud->where('j936400f1.sucursal',$this->user->sucursal);
        $crud->where('cajadiaria.caja',$this->user->caja);
        $crud->add_action('Hacer cierre','',base_url('cajas/admin/cajadiaria/cerrar').'/');

        //$crud->add_action('Imprimir ventas','',base_url('reportes/detalle_ventas').'/');
        if($crud->getParameters()=='add'){
            $crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url('movimientos/ventas/ventas/add').'";</script>');
        }
        $crud->unset_back_to_list();
        if($x=='add'){            
            $crud->field_type('monto_final_gs', 'invisible')
                ->field_type('monto_final_reales', 'invisible')
                ->field_type('monto_final_dolares', 'invisible')
                ->field_type('monto_final_pesos', 'invisible')
                ->field_type('monto_final_cheques', 'invisible')
                ->field_type('monto_final_tcredito', 'invisible')
                ->field_type('monto_final_tdebito', 'invisible')
                ->field_type('monedas_1000','invisible')
                ->field_type('monedas_500','invisible')
                ->field_type('monedas_100','invisible')
                ->field_type('monedas_50','invisible')
                ->field_type('billetes_100000','invisible')
                ->field_type('billetes_50000','invisible')
                ->field_type('billetes_20000','invisible')
                ->field_type('billetes_10000','invisible')
                ->field_type('billetes_5000','invisible')
                ->field_type('billetes_2000','invisible')
                ->field_type('ingreso_efectivo','invisible')
                ->field_type('pago_proveedor','invisible')
                ->field_type('salida_efectivo','invisible')
                ->field_type('total_efectivo','invisible')
                ->field_type('monto_final_gs','invisible')
                ->field_type('monto_final_cheques','invisible')
                ->field_type('monto_final_tcredito','invisible')
                ->field_type('monto_final_tdebito','invisible')
                ->field_type('monto_final_reales','invisible')
                ->field_type('monto_final_dolares','invisible')
                ->field_type('monto_final_pesos;','invisible');
            $crud->required_fields('timbrados_id','timbrados_remision','monto_inicial', 'caja', 'sucursal');
            $crud->set_relation('timbrados_id','timbrados','nro_timbrado',['vigencia_hasta >='=>date("Y-m-d"),'sucursales_id'=>$this->user->sucursal,'cajas_id'=>$this->user->caja,'vigente'=>1]);
            $crud->set_relation('timbrados_remision','timbrados_nota_remision','nro_timbrado',['vigencia_hasta >='=>date("Y-m-d"),'sucursales_id'=>$this->user->sucursal,'cajas_id'=>$this->user->caja]);
            $crud->set_relation_dependency('caja', 'sucursal', 'sucursal');            
        }else{
            $crud->set_relation('timbrados_remision','timbrados_nota_remision','nro_timbrado');
        }
        if($x=='cerrar' && is_numeric($y)){
            get_instance()->x = $x;
            $crud->field_type('timbrados_id', 'hidden')
                 ->field_type('monto_inicial', 'hidden')
                 ->field_type('timbrados_remision', 'hidden');     
            $crud->required_fields('monto_final_gs','monto_final_reales','monto_final_dolares','monto_final_pesos','monto_final_cheques','monto_final_tcredito','monto_final_tdebito');
            $crud->edit_fields('monedas_1000','monedas_500','monedas_100','monedas_50','billetes_100000','billetes_50000','billetes_20000','billetes_10000','billetes_5000','billetes_2000','ingreso_efectivo','pago_proveedor','salida_efectivo','nota_credito','total_efectivo','monto_final_cheques','monto_final_pos','monto_final_reales','monto_final_dolares','monto_final_pesos','monto_final_gs','monto_final_dolares2');
            $crud->set_lang_string('update_success_message','<script>document.location.href="'.base_url('cajas/admin/cajadiaria/success').'";</script>');
            $crud->replace_form_edit('cruds/cerrarCaja/edit','cajas');
        }
        if($x=='cerrar' && is_numeric($y) && empty($_POST)){
            $crud->set_primary_key_value($y);
            $output = $crud->render(3);
        }else{
            $output = $crud->render();
        }
        $output->output.= $this->load->view('cajadiaria_list',[],TRUE);
        $this->loadView($output);
    }

    public function formatear_read_cierre($val){
        return number_format($val,0,',','.').' Gs';
    }

    function validarTimbrado(){
        if(!empty($_POST['correlativo']) && !empty($_POST['timbrados_id'])){
            $this->db->where(['vigencia_hasta >='=>date("Y-m-d"),'sucursales_id'=>$this->user->sucursal,'cajas_id'=>$this->user->caja]);
            $this->db->where("'".$_POST['correlativo']."' BETWEEN nro_factura_desde AND nro_factura_hasta");
            $max = $this->db->get_where('timbrados',['id'=>$_POST['timbrados_id']]);
            if($max->num_rows()==0){
                $this->form_validation->set_message('validarTimbrado','El correlativo de la cajadiaria o el rango del timbrado no son válidos');
                return false;
            }
        }    
        return true;        
    }
    
    public function gastos($x = '', $y = '') {
        $crud = parent::crud_function($x, $y); 
        $crud->columns('gastos.id','cajadiaria','beneficiario','concepto','cuentas_id','fecha_gasto','monto','nro_documento');
        $crud->callback_column('gastos.id',function($val,$row){return $row->id;})
             ->callback_column('monto',function($val,$row){return number_format($val,0,',','.');})             
             ->display_as('gastos.id','ID');
        if($crud->getParameters()=='list' || $crud->getParameters()=='ajax_list'){
            $crud->set_relation('cajadiaria','cajadiaria','caja');
        }
        $crud->field_type('cajadiaria','hidden',$_SESSION['cajadiaria']);
        $crud->field_type('user_created','hidden',$_SESSION['user']);
        $crud->field_type('user_modified','hidden',$_SESSION['user']);
        $crud->field_type('fecha_created','hidden',date("Y-m-d"));
        $crud->field_type('fecha_modified','hidden',date("Y-m-d"));
        $crud->order_by('gastos.id','DESC');
        $crud->where('j5c50dc0c.caja',$this->user->caja);
        $output = $crud->render();
        $this->loadView($output);
    }

    public function cuentas_bancos($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);   
        $crud->callback_column('saldo',function($val){return number_format($val,0,',','.');});
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function pagocliente($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_read()
             ->unset_delete();
        $crud->set_relation('clientes_id', 'clientes', 'nombres');
        $crud->field_type('anulado', 'true_false', array(0 => 'No', 1 => 'Si'));
        $crud->where('sucursal', $_SESSION['sucursal'])
             ->where('caja', $_SESSION['caja']);
        //$crud->columns('cliente', 'recibo', 'fecha', 'totalefectivo', 'totaldebito', 'totalnotacredito', 'totalcheque', 'totalcredigo', 'totalpagado', 'saldo', 'anulado');
        $crud->callback_column('saldo', function($val, $row) {
            get_instance()->db->select('SUM(total_venta) as total');
            return number_format(get_instance()->db->get_where('ventas', array('cliente' => $row->cliente, 'transaccion' => 2, 'status' => 0))->row()->total, 2, ',', '.');
        });
        $output = $crud->render();
        $output->crud = 'ventas';
        if ($x == 'add')
            $output->output = $this->load->view('pagosclientes', array(), TRUE);
        if ($x == 'edit' && !empty($y) && is_numeric($y)) {
            $venta = $this->db->get_where('pagocliente', array('id' => $y));
            $detalles = $this->db->get_where('pagoclientesfactura', array('pagocliente' => $venta->row()->id));
            $output->output = $this->load->view('pagosclientes', array('pagocliente' => $venta->row(), 'detalles' => $detalles), TRUE);
        }
        if ($x == 'imprimir' || $x == 'imprimirticket') {
            if (!empty($y) && is_numeric($y)) {
                $this->db->select('pagocliente.*,clientes.id as cliente, sucursales.denominacion, sucursales.telefono, sucursales.direccion, cajas.denominacion as caja, clientes.nombres as clientename, clientes.apellidos as clienteadress');
                $this->db->join('clientes', 'clientes.id = pagocliente.clientes_id','left');
                $this->db->join('cajas', 'cajas.id = pagocliente.caja','left');
                $this->db->join('sucursales', 'sucursales.id = pagocliente.sucursal','left');
                $venta = $this->db->get_where('pagocliente', array('pagocliente.id' => $y));
                if ($venta->num_rows() > 0) {
                    $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
                    $this->db->select('SUM(total_venta) as total');
                    $venta->row()->saldo = $this->db->get_where('ventas', array('cliente' => $venta->row()->cliente, 'transaccion' => 2, 'status' => 0))->row()->total;
                    if ($venta->row()->saldo > 0) {
                        $total = 0;
                        $this->db->select('pagoclientesfactura.*');
                        $this->db->join('pagoclientesfactura', 'pagoclientesfactura.pagocliente = pagocliente.id');
                        foreach ($this->db->get_where('pagocliente', array('cliente' => $venta->row()->cliente))->result() as $p) {
                            $total+=$p->total + $p->nota_credito;
                        }
                        $venta->row()->saldo-=$total;
                    } else {
                        $venta->row()->saldo = 0;
                    }
                    $output = $this->load->view('reportes/imprimirTicketPago', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
                    if ($this->router->fetch_class() == 'admin') {
                        echo $output;
                    } else {
                        return $output;
                    }
                } else {
                    echo "Factura no encontrada";
                }
            } else {
                echo 'Factura no encontrada';
            }
        } else {
             $this->loadView($output);
        }
    }
    
    public function pagoproveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);            
        $crud->unset_read()
             ->unset_delete();
        $crud->set_relation('proveedor','proveedores','denominacion');
        if(!empty($y) && is_numeric($y)){
            $compra = $this->db->get_where('compras',['id'=>$y]);
            if($compra->num_rows()>0){
                $crud->set_relation('proveedor','proveedores','denominacion',['proveedores.id'=>$compra->row()->proveedor]);
                $_POST['compraDatos'] = $compra->row();
            }
        }       
        $crud->display_as('bancos_id','Banco')
             ->display_as('cuentas_bancos_id','Cuenta')
             ->field_type('sucursal','hidden',$this->user->sucursal)
             ->field_type('caja','hidden',$this->user->caja)
             ->field_type('cajadiaria','hidden',$this->user->cajadiaria)
             ->field_type('user','hidden',$this->user->id)
             ->field_type('forma_pago','enum',['Efectivo','Cheque','Transferencia','Deposito']) 
             ->field_type('anulado','invisible')
             ->field_type('totalnotacredito','invisible')            
             ->field_type('totalefectivo','invisible')
             ->field_type('tipo_cheque','dropdown',['1'=>'A la vista','-1'=>'Diferido'])
             ->set_relation('bancos_id','bancos','denominacion')
             ->set_relation('cuentas_bancos_id','cuentas_bancos','nro_cuenta')
             ->set_relation_dependency('cuentas_bancos_id','bancos_id','bancos_id')
             ->callback_field('fecha_emision',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_emision" id="field-fecha_emision" class="form-control" value="'.$value.'">';})
             ->callback_field('fecha_pago',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_pago" id="field-fecha_pago" class="form-control" value="'.$value.'">';})

             ->callback_after_insert(function($post,$primary){
                foreach($_POST['compra'] as $n=>$v){
                    $notacredito = 0;
                    $this->db->select('notas_credito_proveedor.*,SUM(total_monto) as total_notas');
                    $this->db->group_by('notas_credito_proveedor.compras_id');
                    $notas = $this->db->get_where('notas_credito_proveedor',['compras_id'=>$v]);
                    if($notas->num_rows()>0){
                        $notacredito = $notas->row()->total_notas;
                    }
                    if(!empty($v)){
                        $compra = $this->db->get_where('compras',['id'=>$v]);
                        $data = array('pagoproveedor'=>$primary);
                        $data['compra'] = $v;
                        $data['fecha_factura'] = date("Y-m-d",strtotime(str_replace("/","-",$_POST['fecha_factura'][$n])));                                                
                        $data['total_factura'] = $_POST['total_factura'][$n];
                        $data['monto'] = $_POST['saldo'][$n];
                        $data['monto_abonado'] = $_POST['monto_abonado'][$n];
                        $data['nota_credito'] = $_POST['nota_credito'][$n];
                        //$data['nota_credito'] = $notacredito;
                        $data['total'] = $_POST['total'][$n];
                        $data['nro_factura'] = $compra->row()->nro_factura;
                        $this->db->insert('pagoproveedor_detalles',$data);
                        
                        $this->db->select('(SUM(total)+SUM(nota_credito)) as total_notas');
                        $this->db->group_by('pagoproveedor_detalles.compra');
                        $pagos = $this->db->get_where('pagoproveedor_detalles',['compra'=>$v]);
                        $pagos = $pagos->num_rows()>0?$pagos->row()->total_notas:0;
                                                
                        if($compra->num_rows()>0 && $pagos>=($compra->row()->total_compra - $compra->row()->total_retencion)){
                            $this->db->update('compras',array('status'=>1),array('id'=>$v));
                        }
                    }
                }

                //Insertar en libreo de banco
                if($post['forma_pago'] == 'Cheque' || $post['forma_pago']=='Transferencia' || $post['forma_pago']=='Deposito'){                    
                    $saldo_actual = $this->db->get_where('cuentas_bancos',['id'=>$post['cuentas_bancos_id']])->row()->saldo;
                    $this->db->insert("libro_banco",[
                        'sucursales_id'=>$this->user->sucursal,
                        'cajas_id'=>$this->user->caja,
                        'cajadiaria_id'=>$this->user->cajadiaria,
                        'fecha'=>date("Y-m-d H:i:s"),
                        'bancos_id'=>$post['bancos_id'],
                        'cuentas_bancos_id'=>$post['cuentas_bancos_id'],
                        'tipo_movimiento'=>-1,
                        'saldo_actual'=>$saldo_actual,
                        'concepto'=>$post['forma_pago'],
                        'beneficiario'=>$this->db->get_where('proveedores',['id'=>$post['proveedor']])->row()->denominacion,
                        'monto'=>$post['total_abonado'],
                        'comprobante'=>$post['comprobante'],
                        'forma_pago'=>$post['forma_pago'],
                        'tipo_cheque'=>$post['tipo_cheque'],
                        'nro_cheque'=>$post['nro_cheque'],
                        'fecha_emision'=>date("Y-m-d H:i:s",strtotime(str_replace('/','-',$post['fecha_emision']))),
                        'fecha_pago'=>date("Y-m-d H:i:s",strtotime(str_replace('/','-',$post['fecha_pago']))),
                        'cobrado'=>0,
                        'anulado'=>0,
                        'user_id'=>$this->user->id,
                    ]);                    
                }
             })
             ->replace_form_add('cruds/pagoproveedor/add.php','cajas')
             ->set_rules('forma_pago','Forma de Pago','required|callback_validarFormaPago');

        $output = $crud->render();        
        $this->loadView($output);
    }

    function validarFormaPago(){
        $forma = $_POST['forma_pago'];
        if($forma=='Cheque' || $forma=='Transferencia'){
            $err = false;

            if(empty($_POST['bancos_id']) || empty($_POST['cuentas_bancos_id']))
                $err = true;
            if($forma=='Cheque' && (empty($_POST['nro_cheque']) || empty($_POST['tipo_cheque']) || empty($_POST['fecha_emision']) || empty($_POST['fecha_pago'])))
                $err = true;
            if($err){
                $this->form_validation->set_message('validarFormaPago','Debe completar los campos requeridos por la forma del pago seleccionada');
                return false;
            }
        }
        return true;
    }

    public function saldos($x = '', $y = '') {
        $this->as = array('saldos' => 'ventas');
        $crud = parent::crud_function($x, $y);
        $crud->unset_read()
                ->unset_delete()
                ->unset_add()
                ->unset_edit()
                ->unset_read()
                ->unset_export()
                ->unset_print();
        $crud->set_relation('cliente', 'clientes', '{nro_documento}|{nombres}|{apellidos}|{celular}|{id}');
        $crud->where('ventas.transaccion', 2);
        $crud->where('ventas.status != ', -1);
        $crud->group_by('ventas.cliente');
        $crud->columns('id_cliente','nro_documento', 'nombres', 'apellidos', 'celular','ultima_compra', 'total_compra', 'ultimo_pago', 'total_pagos','saldo');
        $crud->callback_column('id_cliente',function($val,$row){
            return explode('|', $row->s4983a0ab)[4];
        });
        $crud->callback_column('nro_documento', function($val, $row) {
            return explode('|', $row->s4983a0ab)[0];
        });
        $crud->callback_column('nombres', function($val, $row) {
            return explode('|', $row->s4983a0ab)[1];
        });
        $crud->callback_column('apellidos', function($val, $row) {
            return explode('|', $row->s4983a0ab)[2];
        });
        $crud->callback_column('celular', function($val, $row) {
            return explode('|', $row->s4983a0ab)[3];
        });

        $crud->callback_column('ultima_compra', function($val, $row) {
            $this->total = $this->db->query("SELECT
                clientes.nro_documento,
                clientes.nombres,
                clientes.apellidos,
                ifnull(clientes.celular,'Falta_cargar') as celular,
                ifnull(max(date(ventas.fecha)),0) as ultima_compra,
                ifnull(SUM(ventadetalle.totalcondesc),0) as total_compra,
                ifnull(pagos.ultimo_pago,0) as ultimo_pago,
                ifnull(pagos.total_pagos,0) as total_pagos,
                ifnull(SUM(ventadetalle.totalcondesc),0)- ifnull(pagos.total_pagos,0) as saldo
                FROM ventas
                INNER JOIN clientes ON clientes.id = ventas.cliente
                INNER JOIN ventadetalle on ventadetalle.venta = ventas.id
                LEFT JOIN(
                SELECT 
                clientes.id as clienteid,
                max(date(pagocliente.fecha)) as ultimo_pago,
                sum(pagocliente.total_pagado) as total_pagos
                FROM pagocliente 
                INNER JOIN clientes on clientes.id = pagocliente.clientes_id
                WHERE pagocliente.anulado = 0 or pagocliente.anulado is null
                GROUP BY pagocliente.clientes_id
                ) as pagos on pagos.clienteid =ventas.cliente
                WHERE ventas.transaccion = 2 and ventas.status = 0 AND clientes.id = ".$row->cliente."
                GROUP BY ventas.cliente");            
            return $this->total->num_rows()>0?$this->total->row()->ultima_compra:0;
        })->callback_column('total_compra',function($val,$row){
             return $this->total->num_rows()>0?$this->total->row()->total_compra:0;
          })->callback_column('ultimo_pago',function($val,$row){
             return $this->total->num_rows()>0?$this->total->row()->ultimo_pago:0;
          })->callback_column('total_pagos',function($val,$row){
             return $this->db->query('select get_total_pagado('.$row->cliente.') as saldo')->row()->saldo;
          })->callback_column('saldo',function($val,$row){
             return $this->db->query('select get_saldo_old('.$row->cliente.') as saldo')->row()->saldo;
          });       
        $crud->add_action('Abonar deuda', '', base_url('cajas/admin/abonarDeuda/{id_cliente}') . '/');        
        $crud->add_action('Pagos cliente', '', base_url('movimientos/ventas/pagocliente/{id_cliente}/') . '/');        
        $output = $crud->render();
        $output->title = 'Saldos';
        $this->loadView($output);
    }
    
    public function actualizardescuentos(){
        if(!empty($_POST)){
            $this->form_validation->set_rules('porcentaje','Porcentaje','required|numeric');
            $this->form_validation->set_rules('proveedores','Proveedor','required|numeric');
            $this->form_validation->set_rules('categoriaproducto','Categoria del producto','required|numeric');
            if($this->form_validation->run()){
                $where = array('categoria_id'=>$this->input->post('categoriaproducto'),'proveedor_id'=>$this->input->post('proveedores'));
                if(!empty($_POST['paises'])){
                    $where['nacionalidad_id'] = $_POST['paises'];
                }
                $this->db->update('productos',array('descmax'=>$this->input->post('porcentaje')),$where);
                $msj = $this->success('Los datos se han actualizado con éxito');
            }else{
                $msj = $this->error($this->form_validation->error_string());
            }
        }else{
            $msj = '';
        }
        $this->loadView(array('view'=>'actualizardescuentos','msj'=>$msj));
    }
    
    public function saldos_proveedores($x = '', $y = '') {
            $this->as = array('saldos_proveedores' => 'compras');
            $crud = parent::crud_function($x, $y);
            $crud->unset_read()
                    ->unset_delete()
                    ->unset_add()
                    ->unset_edit()
                    ->unset_read()
                    ->unset_export()
                    ->unset_print();           
            $crud->set_subject('Pago a Proveedores');
            $crud->where('compras.status != ', -1);
            $crud->group_by('compras.proveedor');
            $crud->columns('sucursal', 'proveedor', 'total_pagos', 'total_compra', 'total_retencion','total_notas','ultima_compra', 'ultimo_pago', 'saldo');
            $crud->set_relation('sucursal','sucursales','denominacion');
            $crud->set_relation('proveedor','proveedores','denominacion');
            
            $crud->callback_column('total_compra', function($val, $row) {
                get_instance()->db->select('SUM(compras.total_compra) as total_compra');
                $p = get_instance()->db->get_where('compras', array('compras.proveedor' => $row->proveedor,'compras.status >='=>0/*,'transaccion'=>2*/));
                return $p->row()->total_compra == null ? (string) 0 : $p->row()->total_compra;
            });

            $crud->callback_column('total_retencion', function($val, $row) {
                get_instance()->db->select('SUM(compras.total_retencion) as total_retencion');
                $p = get_instance()->db->get_where('compras', array('compras.proveedor' => $row->proveedor,'compras.status >='=>0/*,'transaccion'=>2*/));
                return $p->row()->total_retencion == null ? (string) 0 : $p->row()->total_retencion;
            });

            $crud->callback_column('total_notas', function($val, $row) {
                get_instance()->db->select('SUM(notas_credito_proveedor.total_monto) as total_notas');
                $p = get_instance()->db->get_where('notas_credito_proveedor', array('notas_credito_proveedor.proveedor' => $row->proveedor,'anulado'=>0,'descontar_saldo'=>1));
                return $p->row()->total_notas == null ? (string) 0 : $p->row()->total_notas;
            });

            

            $crud->callback_column('total_pagos', function($val, $row) {
                get_instance()->db->select('SUM(pagoproveedores.total_abonado) as total_pagos');
                $p = get_instance()->db->get_where('pagoproveedores', array('pagoproveedores.proveedor' => $row->proveedor));
                ($p->num_rows()>0 && $p->row()->total_pagos==null)?$p->row()->total_pagos = 0:$p->row()->total_pagos;
                return $p->num_rows() == 0 ? (string) '0' : (string) $p->row()->total_pagos;
            });

            $crud->callback_column('ultima_compra', function($val, $row) {
                get_instance()->db->select('compras.fecha');                
                get_instance()->db->order_by('id', 'DESC');
                $p = get_instance()->db->get_where('compras', array('compras.proveedor' => $row->proveedor,'compras.status >='=>0,'transaccion'=>2));
                return $p->num_rows()>0?date("d-m-Y H:i:s", strtotime($p->row()->fecha)):'N/A';
            });

            $crud->callback_column('ultimo_pago', function($val, $row) {
                get_instance()->db->select('pagoproveedores.fecha');
                $p = get_instance()->db->get_where('pagoproveedores', array('pagoproveedores.proveedor' => $row->proveedor,'pagoproveedores.anulado !='=>1));
                return $p->num_rows() == 0 ? (string) 'N/A' : date("d-m-Y H:i:s", strtotime($p->row()->fecha));
            });
            $crud->add_action('Abonar deuda', '', base_url('cajas/admin/pagoproveedores/add') . '/');        
            $crud->callback_column('saldo', function($val, $row) {
                return number_format($row->total_compra - $row->total_pagos - $row->total_retencion - $row->total_notas, 0, ',', '.');
            });
            $output = $crud->render();
            $output->title = 'Saldos';
            $this->loadView($output);
        }
        
        
        public function abonarDeuda($x = '',$json = 0) {
            $this->validate_caja();
            $cliente = $this->db->get_where('clientes',['id'=>$x]);
            if($cliente->num_rows()>0){
                if (!empty($x) && is_numeric($x)) {
                    //Factuas Clientes ?
                    if($cliente->row()->plan_credito==0){
                        $facturas_clientes = $this->db->get_where('facturas_clientes',['clientes_id'=>$x]);
                        if($facturas_clientes->num_rows()>0){
                            redirect('movimientos/facturas_clientes/facturas_clientes_pagos/'.$facturas_clientes->row()->id.'/add');
                        }else{
                            $this->db->insert('facturas_clientes',['clientes_id'=>$x,'saldo_cliente'=>0]);
                            $id = $this->db->insert_id();
                            $this->db->query("
                                INSERT INTO facturas_clientes_detalle
                                (
                                    id,
                                    facturas_clientes,
                                    venta,
                                    total_venta,
                                    total_pagado,
                                    toial_credito,
                                    pagado
                                )
                                SELECT 
                                NULL,
                                $id,
                                ventas.id,
                                ventas.total_venta,
                                0,
                                NULL,
                                0
                                FROM ventas 
                                WHERE cliente = $x AND transaccion = 2 AND (anulado = 0 OR anulado IS NULL)
                            ");
                            $this->db->query("CALL ajustSaldoFromVentaCredito($id)");

                            redirect('movimientos/facturas_clientes/facturas_clientes_pagos/'.$id.'/add');
                            
                        }
                        
                    }else{
                        redirect('movimientos/creditos/creditos/credito/0/'.$x);
                        return;
                    }
                    
                    $this->db->select('ventas.*, clientes.id as clienteid, clientes.nombres as clientename, clientes.apellidos as apellidos');
                    $this->db->join('clientes', 'clientes.id = ventas.cliente');
                    $cliente = $this->db->get_where('ventas', array('ventas.id' => $x));
                    if ($cliente->num_rows() > 0) {
                        $nombre = $cliente->row()->clientename . ' ' . $cliente->row()->apellidos;                    
                        $clienteid = $cliente->row()->clienteid;
                        $cliente = $cliente->row()->cliente;
                        //Total venta
                        $this->db->select('SUM(ventas.total_venta) as total_venta');
                        $this->db->where('transaccion', '2');
                        $this->db->where('ventas.status !=',-1);
                        $total_venta = $this->db->get_where('ventas', array('ventas.cliente' => $cliente));
                        $totalVentas = $total_venta->row()->total_venta == null ? 0 : $total_venta->row()->total_venta;
                        //Total pagos
                        /*$this->db->select('SUM(pagocliente.total_pagado) as total_pagos');
                        $total_pago = $this->db->get_where('pagocliente', array('pagocliente.clientes_id' => $cliente));
                        $totalPagos = $total_pago->num_rows() == 0 ? 0 : $total_pago->row()->total_pagos;
                        $saldo = $totalVentas - $totalPagos;*/
                        $totalPagos = 0;
                        $saldo = $this->db->query('select get_saldo_old('.$cliente.') as saldo')->row()->saldo;
    
                        //saldo notas de credito
                        /*$this->db->select('SUM(notas_credito_cliente.total_monto) as notas');
                        $this->db->join('ventas', 'ventas.id = notas_credito_cliente.venta');
                        $total_nota = $this->db->get_where('notas_credito_cliente', array('ventas.cliente' => $cliente));
                        $notas = $total_nota->num_rows() == 0 ? 0 : $total_nota->row()->notas;*/
                        $notas = 0;
                        //Se resta a las notas de credito las facturas a las cuales se les ha pagado con notas de credito
                        /*$this->db->select('SUM(totalnotacredito) as notasusadas');
                        $notasusadas = $this->db->get_where('pagocliente', array('cliente' => $cliente));
                        $notasusadas = $notasusadas->num_rows() == 0 ? 0 : $notasusadas->row()->notasusadas;*/
                        $notasusadas = 0;
                        $notas = $notas - $notasusadas;
                        $msj = '';
                        if (!empty($_POST)) {
                            $this->form_validation->set_rules('monto', 'Monto', 'required|numeric');
                            if ($this->form_validation->run()) {
                                //Traemos saldos
                                if ($this->ajustes->permitir_pago_superior_a_saldo==0 && $saldo < $notas + $_POST['monto']) {
                                    $msj = $this->error('El monto ingresado excede de la cantidad total de la deuda');
                                } else {
                                    $detalles = array();
                                    $notasdisponible = $notas;
                                    $saldodisponible = $_POST['monto'];
                                    //Traemos las facturas que se deben
                                    $facturas = $this->db->get_where('ventas', array('ventas.cliente' => $cliente, 'transaccion' => 2));
                                    foreach ($facturas->result() as $factura) {
                                        //Cuanto se debe de esta factura
                                        $this->db->order_by('id', 'DESC');
                                        $deuda = $this->db->get_where('pagoclientesfactura', array('venta' => $factura->id));
                                        if ($deuda->num_rows() > 0) {
                                            $deuda = $deuda->row()->monto - ($deuda->row()->total + $deuda->row()->nota_credito);
                                        } else {
                                            $deuda = $factura->total_venta;
                                        }
    
                                        if ($deuda > 0 && ($saldodisponible > 0 || $notasdisponible > 0)) {
                                            $pago = array('venta' => $factura->id, 'nro_factura' => $factura->nro_factura, 'monto' => $deuda);
                                            if ($notasdisponible > 0) {
                                                $pago['nota_credito'] = $notasdisponible > $deuda ? $deuda : $notasdisponible;
                                                $notasdisponible-=$deuda;
                                                $deuda -= $pago['nota_credito'];
                                            } else {
                                                $pago['nota_credito'] = 0;
                                            }
                                            if ($saldodisponible > 0) {
                                                $pago['total'] = $saldodisponible > $deuda ? $deuda : $saldodisponible;
                                                $saldodisponible-= $pago['total'];
                                            }
                                            array_push($detalles, $pago);
                                        }
                                    }
                                    //Se carga la cabecera del pago
                                    $this->db->insert('pagocliente', array(
                                        'clientes_id' => $cliente,
                                        'fecha' => date("Y-m-d H:i:s"),
                                        /*'totalefectivo' => $_POST['monto'],
                                        'totalnotacredito' => $notas,*/                                    
                                        'formapago_id' =>$_POST['formapago_id'],
                                        'total_pagado' => $notas + $_POST['monto'],
                                        'sucursal' => $_SESSION['sucursal'],
                                        'caja' => $_SESSION['caja'],
                                        'cajadiaria'=>$_SESSION['cajadiaria'],
                                        'user' => $_POST['user']
                                    ));
                                    $idpago = $this->db->insert_id();
                                    foreach ($detalles as $d) {
                                        $d['pagocliente'] = $idpago;
                                        $this->db->insert('pagoclientesfactura', $d);
                                    }
                                    $msj = $this->success('El pago se ha realizado satisfactoriamente <a href="javascript:imprimir(' . $idpago . ')">Imprimir</a> | <a href="javascript:imprimirTicket(' . $idpago . ')">imprimir Ticket</a>');
                                    if($json==0){
                                        redirect('cajas/admin/abonarDeuda/'.$x.'?pago='.$idpago);
                                    }else{
                                        echo $msj;
                                    }
                                }
                            }
                        }
                        //if($json==0){
                            $output = $this->load->view('abonarDeuda',array( 'msj' => $msj, 'totalPagos' => $totalPagos, 'cliente' => $nombre, 'totalVentas' => $totalVentas, 'saldo' => $saldo, 'notas' => $notas,'clienteid'=>$clienteid),TRUE);
                            $this->loadView(array('view' => 'panel','crud'=>'user','output'=>$output));
                        //}
                    } else {
                        header("Location:" . base_url('cajero/saldos'));
                    }
                } else {
                    header("Location:" . base_url('cajero/saldos'));
                }

            }
        }

        public function abonarDeudaProveedor($x = '') {
            if (!empty($x) && is_numeric($x)) {
                $this->db->select('compras.*, proveedores.denominacion as nombre');
                $this->db->join('proveedores', 'proveedores.id = compras.proveedor');
                $proveedor = $this->db->get_where('compras', array('compras.id' => $x));
                if ($proveedor->num_rows() > 0) {
                    $nombre = $proveedor->row()->nombre;
                    $proveedor = $proveedor->row()->proveedor;
                    //Total venta
                    $this->db->select('SUM(compras.total_compra) as total_compra');
                    //$this->db->where('transaccion', '2');
                    $total_venta = $this->db->get_where('compras', array('compras.proveedor' => $proveedor));
                    $totalVentas = $total_venta->row()->total_compra == null ? 0 : $total_venta->row()->total_compra;
                    //Total pagos
                    $this->db->select('SUM(pagoproveedores.total_abonado) as total_pagos');
                    $total_pago = $this->db->get_where('pagoproveedores', array('pagoproveedores.proveedor' => $proveedor));
                    $totalPagos = $total_pago->num_rows() == 0 ? 0 : $total_pago->row()->total_pagos;
                    $saldo = $totalVentas - $totalPagos;
                    //saldo notas de credito
                    $this->db->select('SUM(notas_credito.total_monto) as notas');
                    $this->db->join('compras', 'compras.id = notas_credito.compra');
                    $total_nota = $this->db->get_where('notas_credito', array('compras.proveedor' => $proveedor));
                    $notas = $total_nota->num_rows() == 0 ? 0 : $total_nota->row()->notas;
                    //Se resta a las notas de credito las facturas a las cuales se les ha pagado con notas de credito
                    $this->db->select('SUM(totalnotacredito) as notasusadas');
                    $notasusadas = $this->db->get_where('pagoproveedores', array('proveedor' => $proveedor));
                    $notasusadas = $notasusadas->num_rows() == 0 ? 0 : $notasusadas->row()->notasusadas;
                    $notas = $notas - $notasusadas;
                    $msj = '';
                    if (!empty($_POST)) {
                        $this->form_validation->set_rules('monto', 'Monto', 'required|numeric');
                        if ($this->form_validation->run()) {
                            //Traemos saldos
                            if ($saldo < $notas + $_POST['monto']) {
                                $msj = $this->error('El monto ingresado excede de la cantidad total de la deuda');
                            } else {
                                $detalles = array();
                                $notasdisponible = $notas;
                                $saldodisponible = $_POST['monto'];
                                //Traemos las facturas que se deben
                                $facturas = $this->db->get_where('compras', array('compras.proveedor' => $proveedor/*, 'transaccion' => 2*/,'status >='=>0));
                                foreach ($facturas->result() as $factura) {
                                    //Cuanto se debe de esta factura
                                    $this->db->order_by('id', 'DESC');
                                    $deuda = $this->db->get_where('pagoproveedor_detalles', array('compra' => $factura->id));
                                    if ($deuda->num_rows() > 0) {
                                        $deuda = $deuda->row()->monto - ($deuda->row()->total + $deuda->row()->nota_credito);
                                    } else {
                                        $deuda = $factura->total_compra;
                                    }

                                    if ($deuda > 0 && ($saldodisponible > 0 || $notasdisponible > 0)) {
                                        $pago = array('compra' => $factura->id, 'nro_factura' => $factura->nro_factura, 'monto' => $deuda);

                                        /*if ($notasdisponible > 0) {
                                            $pago['nota_credito'] = $notasdisponible > $deuda ? $deuda : $notasdisponible;
                                            $notasdisponible-=$deuda;
                                            $deuda -= $pago['nota_credito'];
                                        } else {
                                            $pago['nota_credito'] = 0;
                                        }*/
                                        if ($saldodisponible > 0) {
                                            $pago['total'] = $saldodisponible > $deuda ? $deuda : $saldodisponible;
                                            $saldodisponible-= $pago['total'];
                                        }
                                        array_push($detalles, $pago);
                                    }
                                }
                                //Se carga la cabecera del pago
                                $this->db->insert('pagoproveedores', array(
                                    'proveedor' => $proveedor,
                                    'fecha' => date("Y-m-d H:i:s"),
                                    'totalefectivo' => $_POST['monto'],
                                    'totalnotacredito' => $notas,
                                    'total_abonado' => $notas + $_POST['monto'],
                                    'forma_pago'=>$_POST['forma_pago'],
                                    'comprobante'=>$_POST['comprobante'],
                                    'nro_recibo'=>$_POST['nro_recibo'],
                                    'sucursal' => $_SESSION['sucursal'],
                                    'caja' => $_SESSION['caja'],
                                    'user' => $_SESSION['user']
                                ));
                                $idpago = $this->db->insert_id();
                                foreach ($detalles as $d) {
                                    $d['pagoproveedor'] = $idpago;
                                    $this->db->insert('pagoproveedor_detalles', $d);
                                }
                                $msj = $this->success('El pago se ha realizado satisfactoriamente <a href="javascript:imprimir(' . $idpago . ')">Imprimir</a> | <a href="javascript:imprimirTicket(' . $idpago . ')">imprimir Ticket</a>');
                            }
                        }
                    }


                    $this->loadView(array('view' => 'abonarDeudaProveedor', 'msj' => $msj, 'totalPagos' => $totalPagos, 'cliente' => $nombre, 'totalVentas' => $totalVentas, 'saldo' => $saldo, 'notas' => $notas));
                } else {
                    header("Location:" . base_url('cajero/saldos'));
                }
            } else {
                header("Location:" . base_url('cajero/saldos'));
            }
        /* Cruds */
    }

    public function ajuste_cajas($x = '', $y = '') {
        $this->as['ajuste_cajas'] = 'ajuste_efectivo';
        $crud = parent::crud_function($x, $y);        
        $crud->field_type('user_id','hidden',$this->user->id);
        $crud->field_type('sucursales_id','hidden',$this->user->sucursal);
        $crud->field_type('cajas_id','hidden',$this->user->caja);
        $crud->field_type('cajadiaria_id','hidden',$this->user->cajadiaria);
        $crud->field_type('tipo_ajuste','dropdown',['-1'=>'Egreso','1'=>'Ingreso','-2'=>'Gasto']);
        $crud->field_type('fecha_registro','hidden',date("Y-m-d H:i:s"));
        if($crud->getParameters()!='edit'){
            $crud->field_type('anulado','hidden',0);
        }
        $crud->edit_fields('anulado');
        $crud->unset_columns('fecha_registro');
        $crud->unset_delete();
        $output = $crud->render();
        $output->output = $this->load->view('ajuste_cajas',['output'=>$output->output],TRUE);
        $this->loadView($output);
    }

     public function ajuste_cajadiaria($x = '', $y = '') {    
        //$this->db->order_by('ajuste_cajadiaria.id','DESC');    
        $crud = parent::crud_function($x, $y);    
        $crud->set_theme('bootstrap2_horizontal');    
        $crud->set_subject('Ajustes de Caja Diaria');
        //$crud->field_type('user_id','hidden',$this->user->id);
        $crud->set_relation('user_id','user','{nombre} {apellido}');
        $crud->field_type('sucursales_id','hidden',$this->user->sucursal);
        $crud->field_type('cajas_id','hidden',$this->user->caja);
        $crud->field_type('cajadiaria_id','hidden',$this->user->cajadiaria);
        $crud->field_type('tipo_ajuste','dropdown',['-1'=>'Egreso','1'=>'Ingreso','-2'=>'Gasto']);        
        $crud->field_type('fecha','hidden',date("Y-m-d H:i:s"));
        $crud->field_type('tipo_moneda','dropdown',['1'=>'Guaraníes','2'=>'Pesos','3'=>'Dolares','4'=>'Reales','4'=>'Cheque']); 
        $crud->columns('id', 'cajadiaria_id','fecha', 'tipo_ajuste', 'observacion', 'monto', 'tipo_moneda','user_id','anulado');
        $crud->edit_fields('anulado');
        if($crud->getParameters()=='add'){
            $crud->field_type('anulado','hidden',0);
        }
        if(in_array(3,$this->user->getGroupsArray())){
            $crud->unset_edit();
        }
        $crud->unset_delete();
        $crud->callback_field('tipo_moneda',function($val){return form_dropdown('tipo_moneda',['1'=>'Guaranies','2'=>'Peso','3'=>'Dolar','4'=>'Real','5'=>'Cheque'],$val,'id="field-tipo_moneda" class="form-control"');});        
        $crud->set_lang_string('insert_success_message','Ajuste almacenado con éxito <script>window.open("'.base_url('reportes/rep/verReportes/138/html/valor/{id}').'"); </script>');
        $crud->where('ajuste_cajadiaria.cajas_id',$this->user->caja);
        $crud->order_by('ajuste_cajadiaria.id','DESC');
        $output = $crud->render();
        $output->title = 'Ajustes de Caja Diaria';
        if($x=='add'){
            $output->output.= '<script>window.afterLoad.push(function(){$("#field-user_id").val('.$this->user->id.').chosen().trigger("liszt:updated")});</script>';
        }
        $this->loadView($output);
    }

     public function otros_ingresos($x = '', $y = '') {
        $crud = parent::crud_function($x, $y); 
        $crud->set_relation('forma_pago_id','formapago','denominacion')
             ->display_as('forma_pago_id','Forma de Pago')
             ->set_subject('Otros Ingresos')
             ->set_lang_string('form_add','Añadir')
             ->edit_fields('concepto','forma_pago_id','anulado');
        $output = $crud->render();
        $output->title = 'Otros Ingresos';
        $this->loadView($output);
    }

}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
