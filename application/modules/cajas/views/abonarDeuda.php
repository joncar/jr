<?php if(!empty($msj)): ?>
<?= $msj ?>
<?php endif ?>
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h1 class="kt-portlet__head-title">
				<b>Abonar deuda</b>
			</h1>
		</div>
	</div>
	<div class="kt-portlet__body">
		<div class="kt-section">
			<form class="form-horizontal" id='formulario' method="post" action="" role="form" onsubmit="$('button').attr('disabled',true)">
			    <div class="row well">
			        <div class="col-12 col-md-6">
			            Cliente: <b><?= $cliente ?></b>
			        </div>
			        <div class="col-12 col-md-6" align="right">
			            Fecha: <b><?= date("d-m-Y H:i:s") ?></b>
			        </div>
			    </div>
			    <div class="row">
			        <div class="col-12 col-md-4">
			            <div align="right">
			                Saldo de notas de crédito: <span style="font-size:29px"><b><?= number_format($notas,2,',','.') ?> Gs</b></span>
			            </div>
			            <div align="right">
			               Saldo del cliente <span style="font-size:29px"><b><?= number_format($saldo,2,',','.') ?> Gs</b></span>
			            </div>
			            <?php if($saldo>0 || $this->ajustes->permitir_pago_superior_a_saldo==1): ?>
		            	<div class="col-sm-8">
		            		<label for="formapago_id" class="col-sm-8 control-label">Forma de Pago</label>
			                <?php 
								$formas = $this->db->get_where('formapago');								
							?>
							<select name="formapago_id" id="formapago_id" class="form-control chosen-select">
								<option value="">Seleccione forma de pago</option>	
									<?php foreach($formas->result() as $f): ?>
								<option value="<?= $f->id ?>"><?= $f->denominacion ?> </option>
								<?php endforeach ?>
							</select>
			            </div>
		            	<div class="col-sm-8">
		            		<label for="user" class="col-sm-8 control-label">Cobrador/a</label>
			                <?php 
								$user = $this->db->get_where('user');								
							?>
							<select name="user" id="user" class="form-control chosen-select">
								<option value="">Seleccione Cobrador/a</option>	
									<?php foreach($user->result() as $u): ?>
								<option value="<?= $u->id ?>"><?= $u->nombre ?> </option>
								<?php endforeach ?>
							</select>
		
			            </div>			            
			            <div class="form-group">
			                <label for="monto" class="col-sm-4 control-label">Monto</label>
			                <div class="col-sm-8">
			                    <input type="text" name="monto" id="monto" class="form-control" placeholder="Monto a abonar" required="" min="1">
			                </div>
			            </div>
			            <div align="center">
			                <button type="submit" class="btn btn-success">Abonar monto</button>
			                <a href="<?= base_url() ?>cajas/admin/saldos" class="btn btn-danger">Volver a saldos</a>
			                <?php if(!empty($_GET['pago'])): ?>
			                	<a href="javascript:imprimirTicket(<?= $_GET['pago'] ?>)" class="btn btn-info">Imprimir recibo</a>
			                <?php endif ?>
			            </div>
			            <?php else: ?>
			            Este cliente no posee deudas, o ya ha cancelado la totalidad de las mismas
			            <?php endif ?>
			        </div>
			        <div class="col-12 col-md-8">
			            <b>Cheques diferidos para este cliente</b>
			            <?php 
			                $query = "
			                    SELECT 
			                    ventas.nro_factura,
			                    cheques_a_cobrar.nro_cheque,                    
			                    bancos.denominacion as banco,
			                    cheques_a_cobrar.monto,
			                    DATE_FORMAT(cheques_a_cobrar.vencimiento,'%d/%m/%Y') as vencimiento,
			                    cheques_a_cobrar.cruzado
			                    FROM cheques_a_cobrar
			                    INNER JOIN ventas ON ventas.id = cheques_a_cobrar.ventas_id
			                    INNER JOIN bancos ON bancos.id = cheques_a_cobrar.bancos_id
			                    WHERE ventas.cliente = '".$clienteid."' AND cheques_a_cobrar.vencimiento >= DATE(NOW())
			                ";
			                $query = $this->db->query($query);
			                if($query->num_rows()>0){
			                    echo sqlToHtml($query);
			                }else{
			                    echo 'Sin cheques por cobrar para este cliente';
			                }
			            ?>
			        </div>
			    </div>
			</form>
		</div>
</div>
<script>    
    function imprimir(codigo){
    	var rep = <?php echo get_instance()->ajustes->id_reporte_ticket_pagocliente ?>;
		window.open('<?= base_url(); ?>reportes/rep/verReportes/'+rep+'/html/valor/'+codigo);
     }
      function imprimirTicket(codigo){
      	var rep = <?php echo get_instance()->ajustes->id_reporte_pagocliente ?>;
        window.open('<?= base_url(); ?>reportes/rep/verReportes/'+rep+'/html/valor/'+codigo);
     }

    <?php if(!empty($_GET['pago'])): ?>
        imprimirTicket(<?= $_GET['pago'] ?>);
    <?php endif ?>
</script>
