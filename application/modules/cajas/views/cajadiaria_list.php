<?php
	$ajustes = $this->ajustes;
	$ajustes->solicitar_token_ventas = $this->db->get_where('cajas',['id'=>$this->user->caja])->row()->solicitar_token;
?>
<?php $this->load->view('_token_modal',array(),FALSE,'movimientos'); ?>
<script>
	var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;
	window.afterLoad.push(function(){
		$(document).on('click','.crud-action',function(e){
			if($(this).attr('href').indexOf('abrir')>-1){
				e.preventDefault();
				validateToken().then(()=>{
					document.location.href=$(this).attr('href');
				});
			}
		});
	});

	var validateToken = function(){
	    return new Promise((resolve,reject)=>{
	        if(ajustes.solicitar_token_ventas=='0'){
	            resolve();
	        }else{
	            validarToken().then((result)=>{
	                if(result){
	                    resolve();
	                }else{
	                    reject();
	                }
	            });
	        }
	    });
	}
</script>