
<?php   
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-edit.js?v=1.1"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
    get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';    
?>

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <?php echo form_open( $update_url, 'method="post" id="crudForm" onsubmit="insertar(\''.$update_url.'\',this,\'.report\'); return false;" autocomplete="off" enctype="multipart/form-data"'); ?>
    
    <div class="row">
    	<div class="col-12 col-md-6">
		    <div class="kt-portlet">
		        <div class="kt-portlet__head">
		            <div class="kt-portlet__head-label">
		                <h3 class="kt-portlet__head-title">
		                    Efectivo
		                </h3>
		            </div>
		        </div>

		        <!--begin::Form-->        
		        <div class="kt-portlet__body">
		            <div class="kt-section kt-section--first">
		                <div class="row">
		                	
				                <?php 
				                	foreach($fields as $field): 
				                		if($field->field_name=='ingreso_efectivo')break;
				                ?>
				                	<div class="col-12 col-md-3">
					                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
					                        <div>
					                            <label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label>
					                        </div>
					                        <?php echo $input_fields[$field->field_name]->input ?>                            
					                    </div>
				                    </div>
				                <?php endforeach ?>	                
		                </div>
		                
		            </div>
		        </div>
		        <!--end::Form-->
		    </div>   
	    </div>

	    <div class="col-12 col-md-6">

		    <div class="kt-portlet">
		        <div class="kt-portlet__head">
		            <div class="kt-portlet__head-label">
		                <h3 class="kt-portlet__head-title">
		                    Resumen
		                </h3>
		            </div>
		        </div>

		        <!--begin::Form-->        
		        <div class="kt-portlet__body">
		            <div class="kt-section kt-section--first">
		                <div class="row">
		                	<?php 
		                			$start = false;
				                	foreach($fields as $field): 
			                		if($field->field_name=='ingreso_efectivo')$start = true;
				                	if($start):	                		
			                ?>
			                	<div class="col-12 col-md-4">
				                    <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
				                        <div>
				                            <label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label>
				                        </div>
				                        <?php echo $input_fields[$field->field_name]->input ?>                            
				                    </div>
			                    </div>
			                    <?php endif ?>
			                <?php endforeach ?>	                

		                </div>
		                
		            </div>
		        </div>
		        <div class="kt-portlet__foot text-right">
			        <div class="kt-form__actions">
			            <button type='submit' class="btn btn-success">Cerrar</button>                    
			        </div>
			    </div>  

		        <!--end::Form-->
		    </div>   
	    </div>
    </div>

    <!-- Start of hidden inputs -->
    <?php
        foreach($hidden_fields as $hidden_field){
            echo $hidden_field->input;
        }
    ?>
    <!-- End of hidden inputs -->
    <?php if ($is_ajax) { ?>
        <input type="hidden" name="is_ajax" value="true" />
    <?php }?>

    <div class="report"></div>
    
<?php echo form_close(); ?>
</div>

<script>
	window.afterLoad.push(function(){		
		$('#field-monedas_1000,#field-monedas_500,#field-monedas_100,#field-monedas_50,#field-billetes_100000,#field-billetes_50000,#field-billetes_20000,#field-billetes_10000,#field-billetes_5000,#field-billetes_2000').on('change',calcularEfectivo);
		$('#field-nota_credito,#field-monto_final_dolares,#field-monto_final_reales,#field-monto_final_pesos,#field-ingreso_efectivo,#field-salida_efectivo,#field-pago_proveedor,#field-monto_final_cheques,#field-monto_final_pos').on('change',calcularFinal);
		$("#field-monto_final_gs,#field-total_efectivo").attr('readonly',true);
		$(document).on('keydown',function(e){
			if(e.which==13){
				e.preventDefault();
			}			
		});
		calcularEfectivo();
	});

	function calcularEfectivo(){
		var total = 0;
		moneda_1000 = parseInt($("#field-monedas_1000").val());
		moneda_500 = parseInt($("#field-monedas_500").val());
		moneda_100 = parseInt($("#field-monedas_100").val());
		moneda_50 = parseInt($("#field-monedas_50").val());

		billete_100000 = parseInt($("#field-billetes_100000").val());
		billete_50000 = parseInt($("#field-billetes_50000").val());
		billete_20000 = parseInt($("#field-billetes_20000").val());
		billete_10000 = parseInt($("#field-billetes_10000").val());
		billete_5000 = parseInt($("#field-billetes_5000").val());
		billete_2000 = parseInt($("#field-billetes_2000").val());

		total+= isNaN(moneda_1000)?0:moneda_1000*1000;
		total+= isNaN(moneda_500)?0:moneda_500*500;
		total+= isNaN(moneda_100)?0:moneda_100*100;
		total+= isNaN(moneda_50)?0:moneda_50*50;
		total+= isNaN(billete_100000)?0:billete_100000*100000;
		total+= isNaN(billete_50000)?0:billete_50000*50000;
		total+= isNaN(billete_20000)?0:billete_20000*20000;
		total+= isNaN(billete_10000)?0:billete_10000*10000;
		total+= isNaN(billete_5000)?0:billete_5000*5000;
		total+= isNaN(billete_2000)?0:billete_2000*2000;
		$("#field-total_efectivo").val(total);
		calcularFinal();
	}

	function calcularFinal(){
		var total = 0;
		var efectivo = parseFloat($("#field-total_efectivo").val());
		var cheques = parseFloat($("#field-monto_final_cheques").val());
		var debito = parseFloat($("#field-monto_final_pos").val());

		var dolares = parseFloat($("#field-monto_final_dolares").val());
		var reales = parseFloat($("#field-monto_final_reales").val());
		var pesos = parseFloat($("#field-monto_final_pesos").val());

		var nota_credito = parseFloat($("#field-nota_credito").val());
		
		
		var salida_efectivo = parseFloat($("#field-salida_efectivo").val());
		var pago_proveedor = parseFloat($("#field-pago_proveedor").val());

		total+= !isNaN(efectivo)?efectivo:0;
		total+= !isNaN(cheques)?cheques:0;
		total+= !isNaN(debito)?debito:0;
		

		
		total+= !isNaN(salida_efectivo)?salida_efectivo:0;
		total+= !isNaN(pago_proveedor)?pago_proveedor:0;

		//total+= !isNaN(dolares)?dolares:0;
		//total+= !isNaN(reales)?reales:0;
		//total+= !isNaN(pesos)?pesos:0;

		total+= !isNaN(nota_credito)?nota_credito:0;

		

		$("#field-monto_final_gs").val(total);
	}
</script>	