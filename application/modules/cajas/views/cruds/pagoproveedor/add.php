<?php	
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/jquery.form.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/themes/bootstrap/js/flexigrid-add.js?v=1.2"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/jquery.noty.js"></script>';
	get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js"></script>';
    get_instance()->hcss[] = '<style>.alert{flex-wrap:wrap;} .alert p{width:100%;}</style>';

?>
<!--
<div class="alert <?= !empty($_SESSION['msj'])?'alert-success':'' ?>" style="<?= !empty($_SESSION['msj'])?'':'display:none' ?>"><?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?></div>
<?php $_SESSION['msj'] = !empty($_SESSION['msj'])?'':'' ?>
-->

<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
	<?php echo form_open( $insert_url, 'method="post" onsubmit="enviarProductos(this); return false;" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
	    <div class="kt-portlet">
	        <div class="kt-portlet__body" style="padding:10px 25px;">
	            <div class="kt-section">
	                <div class="row">
	                    <?php foreach($fields as $field): ?>
	                    	<div class="col-12 col-md-3">
		                        <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
		                            <div><label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"><?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>:</label></div>
		                            <?php echo $input_fields[$field->field_name]->input ?>                            
		                        </div>
	                        </div>
	                    <?php endforeach ?>   
                        <div class="col-12 col-md-3">
                            <div class="form-group" id="retencion_field_box">
                                <div><label for='field-retencion' id="saldo_display_as_box">Retención:</label></div>
                                <input type="text" readonly="" class="form-control" id="retencion">
                            </div>
                        </div> 
                        <div class="col-12 col-md-3">
                            <div class="form-group" id="saldo_field_box">
                                <div><label for='field-saldo' id="saldo_display_as_box">Saldo:</label></div>
                                <input type="text" readonly="" class="form-control" id="saldo">
                            </div>
                        </div>            
	                    <div class="col-12">
	                        <table class="table table-striped" style="font-size:12px;" cellspacing="2">
	                            <thead>
	                                <tr>
	                                    <th>Nro. Factura</th>
	                                    <th>Fecha Factura</th>                                        
	                                    <th>Total Factura</th>
                                        <th>Retención</th>
                                        <th>Notas</th>
                                        <th>Saldo</th>
                                        <th>Monto Abonar</th>
	                                    <th>Total Pagar</th> 
                                        <th></th>                       
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <?php if(!empty($detalles)): foreach($detalles->result() as $d): ?>
	                                <tr>
	                                    <td>
	                                       <?= form_dropdown_from_query('compra[]','compras','id','nro_factura',$d->compra,'readonly',FALSE,'compra'); ?> 
	                                    </td>
	                                    <td><input name="fecha_factura[]" type="text" value='<?= date("d/m/Y",strtotime($d->fecha_factura)) ?>'  class="form-control fecha_factura" readonly placeholder="Fecha factura"></td>
                                        <td><input name="total_factura[]" type="text" value='<?= $d->total_factura ?>'  class="form-control total_factura" readonly placeholder="Total Factura"></td>
                                        <td><input name="retencion[]" type="text" value='<?= $d->retencion ?>'  class="form-control retencion" readonly placeholder="Total Retención"></td>
                                        <td><input name="nota_credito[]" type="text" value='<?= $d->nota_credito ?>'  class="form-control notas" readonly placeholder="Notas" readonly></td>
                                        <td><input name="saldo[]" type="text" value='<?= $d->saldo ?>'  class="form-control saldo" readonly placeholder="Saldo" readonly></td>
                                        <td><input name="monto_abonado[]" type="text" value='<?= $d->monto_abonado ?>'  class="form-control monto_abonado" readonly placeholder="Monto Abonado"></td>
	                                    <td><input name='total[]' type="text" value='<?= $d->total ?>'  class="form-control total" placeholder="Total"></td>
	                                    <td><p align="center">
	                                            <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
	                                            <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
	                                        </p></td>
	                                </tr>
	                                <?php  endforeach; endif ?>
	                                <tr>
	                                    <td>
	                                       <?= form_dropdown_from_query('compra[]',$this->db->get_where('compras',array('status'=>0)),'id','nro_factura','','',TRUE,'compra'); ?> 
	                                    </td>
	                                    <td><input name="fecha_factura[]" type="text" class="form-control fecha_factura" readonly placeholder="Fecha factura"></td>
                                        <td><input name="total_factura[]" type="text" class="form-control total_factura" readonly placeholder="Monto Factura"></td>	                                    
                                        <td><input name="retencion[]" type="text" class="form-control retencion" readonly placeholder="Total Retención"></td>                                     
                                        <td><input name="nota_credito[]" type="text" class="form-control notas" readonly="" placeholder="Notas"></td>
                                        <td><input name="saldo[]" type="text" class="form-control saldo" readonly="" placeholder="Saldo"></td>
                                        <td><input name="monto_abonado[]" type="text" class="form-control monto_abonado" placeholder="Monto Abonado"></td>
	                                    <td><input name='total[]' type="text" value=''  class="form-control total" placeholder="Total"></td>
	                                    <td><p align="center">
	                                            <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
	                                            <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
	                                        </p></td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="report"></div>
	        <div class="kt-portlet__foot">
	        	<!-- Start of hidden inputs -->
                <?php
                    foreach($hidden_fields as $hidden_field){
                        echo $hidden_field->input;
                    }
                ?>
                <!-- End of hidden inputs -->
                <?php if ($is_ajax) { ?>
                    <input type="hidden" name="is_ajax" value="true" />
                <?php }?>
	            <button id="guardar" type="submit" class="btn btn-success">Guardar</button>
                <a href="#" class="btn btn-default">Imprimir Reporte</a>
	        </div>
	    </div>
	<?php echo form_close(); ?>
</div>

<script>

	var validation_url = '<?php echo $validation_url?>';

	var list_url = '<?php echo $list_url?>';



	var message_alert_add_form = "Datos ingresados correctamente";

	var message_insert_error = "Error al ingresar los datos";	
</script>
<script>
    var bancos = '';
    var cheques = '';
    window.afterLoad.push(function(){
        $("#field-fecha").val('<?= date("d/m/Y H:i:s") ?>');
        $("#field-total_abonado").attr('readonly',true);
        //Eventos
        $(document).on('keydown','input',function(event){        
                
            if (event.which == 13){
                if($(this).hasClass('total')){
                    addrow($(this));
                }
                var inputs = $(this).parents("form").eq(0).find(":input");
                var idx = inputs.index(this);
                if (idx == inputs.length - 1) {
                    inputs[0].select()
                } 
                else if($(this).val()!='' && $(this).hasClass('codigo')){
                    $(this).trigger('change');                
                }
                else if($(this).val()=='' && $(this).hasClass('codigo')){
                    return false;
                }
                else
                {
                    inputs[idx + 1].focus(); //  handles submit buttons
                    inputs[idx + 1].select();
                }
                return false;
            }
        });
        
        $("body").on('click','.addrow',function(e){
            e.preventDefault();        
            addrow($(this).parent('p'));
        })

        $("body").on('click','.remrow',function(e){
            e.preventDefault();
            removerow($(this).parent('p'));
        }); 

        $("body").on('change','.monto_abonado',function(){
            $(document).trigger('total');
        });
        
       $("body").on('change','.compra',function(){
           if($(this).val()!=''){
            focused = $(this);
            elements = $(".compra").length;
            focusedcode = $(this).val();        
            $.post('<?= base_url('json/getCompra') ?>',{codigo:$(this).val()},function(data){
                data = JSON.parse(data);
                data = data['producto'];
                if(data!=''){
                focused = focused.parent().parent();
                focused.find('.fecha_factura').val(data['fecha'])
                focused.find('.total_factura').val(data['total_compra']);                    
                focused.find('.retencion').val(data['total_retencion']);                    
                focused.find('.monto_nota_credito').val(0);  
                focused.find('.notas').val(data['notas']); 
                focused.find('.saldo').val(data['total_compra'] - data['total_retencion'] - data['notas']);
                focused.find('.monto_abonado').val(data['total_compra'] - data['total_retencion'] - data['notas']);
                //focused.find('.monto_abonado').val(data['total_compra'] - data['total_retencion']);              
                focused.find('.total').val(data['total_compra']);              
                $(document).trigger('total');
                addrow(focused.find('a'));
                $("tbody tr").last().find('.compra').focus();
                }
                else{
                    emergente('El producto ingresado no se encuentra registrado. <a target="_new" href="<?= base_url($this->router->fetch_class().'/productos/add/json') ?>/'+focused.val()+'">¿Desea registrar uno nuevo?</a>');                        
                }                                        
            });        
            }
        });
        
        $("body").on('change','#field-proveedor',function(){
            $.post('<?= base_url('json/getCompra') ?>',{codigo:'N/A',proveedor:$(this).val()},function(data){
                producto = JSON.parse(data);
                str = '<option value="">Todos</option>';            
                for(i in producto.productos){
                    str += '<option value="'+producto.productos[i].id+'">'+producto.productos[i].nro_factura+'</option>';
                }            
                $(".compra").html(str);            
                x = $("tbody .chosen-select").parents('td').find('.chzn-container');
                w = x.css('width')
                x.remove();
                $("tbody .chosen-select").removeAttr('id').removeClass('chzn-done');
                $("tbody .chosen-select").chosen({allow_single_deselect:true});
                $("tbody .chzn-container, tbody .chzn-drop").css('width',w);
                $("tbody .chzn-search input").css('width','238px');
                $("#saldo").val(producto.saldo);
            });
        })
        
        $(document).on('total',function(){      
            $("#field-total_abonado").val(0); 
            $("#retencion").val(0); 
            var total_retencion = 0;
            $("#saldo").removeClass('is-invalid');
            $("tbody tr").each(function(){
                self = $(this);            
                total = parseInt(self.find('.monto_abonado').val());
                if(!isNaN(total)){
                    self.find('.total').val(total);
                    /*Total Venta*/
                    total_venta = parseInt($("#field-total_abonado").val());
                    total_venta += total;

                    retencion = parseInt(self.find('.retencion').val());
                    total_retencion+= retencion;

                    if(total_venta>parseInt($("#saldo").val())){
                        $("#saldo").addClass('is-invalid');
                    }
                    $("#field-total_abonado").val(total_venta);
                    $("#retencion").val(total_retencion);                
                }
            });
        });
        <?php if(!empty($_POST['compraDatos'])): ?>
            $("#field-proveedor").val('<?= $_POST['compraDatos']->proveedor ?>').chosen().trigger('liszt:updated').trigger('change');
        <?php endif ?>
        $("#saldo").after('<div id="validationServer03Feedback" class="invalid-feedback">Intenta ingresar un monto a pagar superior al saldo</div>');

        bancos = $("#bancos_id_field_box,#cuentas_bancos_id_field_box");
        cheques = $("#nro_cheque_field_box,#tipo_cheque_field_box,#fecha_emision_field_box,#fecha_pago_field_box");
        bancos.parent().hide();
        cheques.parent().hide();
        $("#field-forma_pago").on('change',function(){
            var forma = $(this).val();
            bancos.parent().hide();
            cheques.parent().hide();
            switch(forma){
                case 'Transferencia':
                    bancos.parent().show();
                break;
                case 'Deposito':
                    bancos.parent().show();
                break;
                case 'Cheque':
                    bancos.parent().show();
                    cheques.parent().show();
                break;
            }
        });
    });

    function enviarProductos(form){
        if(confirm('¿Seguro desea registrar estos pagos?')){
            var url = $(form).attr('action');
            insertar(url,form,'.report',function(data){
                for(var i in data){
                    data.success_message = data.success_message.replace('{id}',data.insert_primary_key);
                }
                $(document).find('.report').removeClass('alert').removeClass('alert-info');
                $(document).find('.report').addClass('alert alert-success').html(data.success_message);
                $(form).find('input[type="text"]').val('');
                $(form).find('input[type="number"]').val('');
                $(form).find('input[type="password"]').val('');
                $(form).find('select').val('');
                $(form).find('textarea').val('');
                $(form).find("select").chosen().trigger('liszt:updated');
                clearForm();                
                imprimir(data.insert_primary_key);   
                $("#field-fecha").val('<?= date("d/m/Y") ?>');         
            });
        }
    }

    function imprimir(codigo){
        var url = '<?= base_url() ?>reportes/rep/verReportes/197/html/valor/'+codigo;
        var w = window.open(url,'Impresion','width=800,height=600');
        <?php if(isset($this->ajustes->cerrarAlImprimirVenta) && $this->ajustes->cerrarAlImprimirVenta == 1): ?>
            w.onload = function(){
                setTimeout(function(){w.close()},3000);
            }        
        <?php endif ?>
        $("#imprimirTicket").attr('href',url).removeClass('disabled');
    }
    
    function addrow(obj){
        var row = $(obj).parents('tr');
            row.after('<tr>'+row.html()+'</tr>');
            $(".total").attr('readonly',true);
            $(".hasDatepicker").removeAttr('id').removeClass('hasDatepicker');
            x = $("tbody .chosen-select").parents('td').find('.chzn-container');
            w = x.css('width')
            x.remove();
            $("tbody .chosen-select").removeAttr('id').removeClass('chzn-done');
            $("tbody .chosen-select").chosen({allow_single_deselect:true});
            $("tbody .chzn-container, tbody .chzn-drop").css('width',w);
            $("tbody .chzn-search input").css('width','238px');          
            date_init_calendar();
    }
    function removerow(obj){
        $(obj).parent('td').parent('tr').remove();
        $(document).trigger('total');
    }
   
</script>
<?php $this->load->view('predesign/datepicker') ?>
<?= $this->load->view('predesign/chosen.php') ?>
<div class="mask" style="background:rgba(0,0,0,.8); width:100%; height:100%; position:fixed; top:0px; left:0px; display:none;"></div>
