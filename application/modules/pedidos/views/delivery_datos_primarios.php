<?php get_instance()->hcss[] = '<style>
    .chzn-drop, .chzn-search input{
        width:100% !important;
    }
</style>'; ?>
<div class="row" style="margin-left:0; margin-right:0">
    <div class="col-12">
<div id="accordion" role="tablist" aria-multiselectable="true">
  <div class="card">
    <div class="card-header" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <i class="fa fa-chevron-down"></i> Datos del cliente
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="card-body">
          <div class="alertpedido"></div>
          <form id="actualizarDeliveryForm" action="" onsubmit="return actualizardelivery(this)">
                <div>
                    <div class="row" style="margin-bottom:30px;">
                        <div class="col-12 col-sm-4">
                            <?php $this->db->order_by("nombres"); ?>
                            <b>Cliente: </b> <a href="#addCliente" data-toggle="modal" style="color:green"><i class="fa fa-plus"></i></a>
                            <?php 
                                $cliente = empty($pedidos->clientes_id)?1:$pedidos->clientes_id;
                                echo form_dropdown_from_query('clientes_id','clientes','id','nro_documento nombres apellidos',$cliente,'id="clientes_id"',FALSE) 
                            ?>
                        </div>
                        <div class="col-12 col-sm-4" id="cliente_id_field_group" style="display:none">
                            <b>Cliente: </b>                            
                            <input type="text" name="cliente_nombre" class="form-control" value="<?= $pedidos->cliente_nombre ?>">
                        </div>
                        <div class="col-12 col-sm-4">                            
                            <b>Dirección de entrega: </b>
                            <?= form_input('direccion_entrega_pedido',$pedidos->direccion_entrega_pedido,'class="form-control"'); ?>
                        </div>
                        <div class="col-12 col-sm-4">                            
                            <b>Total pedido: </b>
                            <?php 
                                $total = $this->db->query('SELECT SUM(total) as total from pedidos_detalles where pedidos_id = '.$pedidos->id)->row()->total;
                            ?>
                            <?= form_input('total_pedido',$total,'class="form-control" readonly id="totaldelivery"'); ?>
                        </div>
                        <div class="col-12 col-sm-4">                            
                            <b>Efectivo: </b>
                            <?= form_input('efectivo',$pedidos->efectivo,'class="form-control" id="efectivoPedido"'); ?>
                        </div>
                        <div class="col-12 col-sm-4">                            
                            <b>Vuelto: </b>
                            <?= form_input('vuelto',$pedidos->vuelto,'class="form-control" readonly id="vueltoPedido"'); ?>
                        </div>
                        <?php if($pedidos->tipo_pedidos_id==5): ?>
                            <div class="col-12 col-sm-4">                            
                                <b>Deliverista: </b>
                                <?php
                                    $this->db->select('user.id,user.nombre,user.apellido'); 
                                    $this->db->join('user','user.id = user_group.user');
                                    $this->db->where('user_group.grupo',$this->ajustes->id_grupo_deliverista);
                                    echo form_dropdown_from_query('deliverista','user_group','id','nombre apellido',$pedidos->deliverista,'id="deliverista"'); 
                                ?>
                            </div>
                        <?php endif ?>
                        <div class="col-12" style="padding-top:20px">
                            <b>Observaciones </b>
                            <?php foreach($this->db->get('textos_rapidos')->result() as $o): ?>
                                <a href="javascript:;" onclick="$('#observaciones').val($('#observaciones').val()+'<?= $o->texto.' ' ?>')" class="badge badge-info"><?= $o->texto ?></a>
                            <?php endforeach ?>
                            <?= form_input('observaciones',$pedidos->observaciones,'class="form-control" id="observaciones"'); ?>
                        </div>
                    </div>                    
                </div>
                
                <div style="text-align: right">
                    <button class="btn btn-success" id="btnActualizarPedido" type="submit" id='cargarFactura'>Cargar Datos</button>
                </div>
            </form>
      </div>
    </div>
  </div>
  
</div>

<script>    
    window.afterLoad.push(function(){    
        $("#clientes_id").chosen();
        if($("#clientes_id").val()==1){
            $("#cliente_id_field_group").show();
        }
        $("#efectivoPedido").on('change',function(){
            var total = parseFloat($("#totaldelivery").val().replace(/\./g,''));
            var efectivo = parseFloat($(this).val());
            var vuelto = efectivo-total;
            vuelto = vuelto<0?0:vuelto;
            $("#vueltoPedido").val(vuelto);
        });
        $("#accordion").on('shown.bs.collapse',function(){
            $("#clientes_id_chosen").css('width','100%');
        });
        $("#clientes_id").on('change',function(){
            if($(this).val()=='1'){
                $("#cliente_id_field_group").show();
            }else{
                $("#cliente_id_field_group").hide();
            }
        });
        sumartodoPedido();
    });
    function sumartodoPedido(){
        $("#totaldelivery").val('0');
        var x = 0;
        $(".flexigrid .total").each(function(){
            var total = parseFloat($("#totaldelivery").val());
            $("#totaldelivery").val(total+parseFloat($(this).html()));
            x++;            
        });
    }    
    function actualizardelivery(form){
        $(".alertpedido").removeClass('alert').removeClass('alert-success').html('');
        $("#btnActualizarPedido").attr('disabled',true);
        var f = new FormData(form);
        f.append('tipo_pedidos_id','<?= $pedidos->tipo_pedidos_id ?>');
        $.ajax({
            url:'<?= base_url("pedidos/admin/pedidos/update/".$pedidos->id) ?>',
            type:'POST',
            contentType:false,
            data:f,
            processData:false,
            cache:false,
            success:function(data){                
                $(".alertpedido").html('Los datos del cliente se han actualizado con éxito').addClass('alert alert-success');
                $("#btnActualizarPedido").attr('disabled',false);
            }
        });
        return false;
    }
</script>
</div>
</div>