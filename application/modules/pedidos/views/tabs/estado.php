<div class="row" style="margin-left:0; margin-right:0px; margin-top:10px">    
    <div class="col-12" id="estadoTable">        
        <div class="card">
            <div class="card-header">
                Estado del lugar
            </div>
            <div class="card-body">                                
                <form id="actualizarDeliveryForm" action="" onsubmit="return actualizarestado(this)">
	                <div>
	                    <div class="row" style="margin-bottom:30px;">                        
	                        <div class="col-12 col-sm-4">                            
	                            <b>Estado: </b>
	                            <?= form_dropdown('estado',[0=>'Disponible',1=>'Ocupado',2=>'Sucio',3=>'Reservado'],$pedidos->estado,'class="form-control" readonly id="EstadoPedidoInp"'); ?>
	                        </div>
	                    </div>                    
	                </div>	
	                <div class="alertEstado"></div>  
	                <div style="text-align: right">
	                    <button class="btn btn-success" id="btnActualizarEstado" type="submit" id='cargarFactura'>Cambiar Estado</button>
	                </div>
            	</form>
            </div>       
        </div>
    </div>
</div>
<script>
    function actualizarestado(form){
        $(".alertEstado").removeClass('alert alert-success alert-info alert-danger').html('');
        $("#btnActualizarEstado").attr('disabled',true);
        var f = new FormData(form);
        f.append('tipo_pedidos_id','<?= $pedidos->tipo_pedidos_id ?>');
        $.ajax({
            url:'<?= base_url("pedidos/admin/actualizarEstado/".$pedidos->id) ?>',
            type:'POST',
            contentType:false,
            data:f,
            processData:false,
            cache:false,
            success:function(data){                
                $(".alertEstado").html(data);
                $("#btnActualizarEstado").attr('disabled',false);
            }
        });
        return false;
    }

</script>