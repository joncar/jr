<ul class="list-group">
    <li class="list-group-item active">Facturación</li>
    <li class="list-group-item">Para poder facturar debes ir a la pestaña 1 (Habitación, Parrillas, barra) y seleccionar los productos que deseas facturar</li>
    <li class="list-group-item">Ir a la pestaña facturación, cargar los pagos y procesar el mismo</li>
    <li class="list-group-item active">Teclas rapidas</li>
    <li class="list-group-item">F3 - Para realizar busqueda avanzada de productos</li>
    <li class="list-group-item">F4 - Para añadir producto al pedido</li>
    <li class="list-group-item">F9 - Imprimir cuenta</li>
</ul>