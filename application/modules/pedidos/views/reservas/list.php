<style>
  .chzn-drop,.chzn-drop .chzn-search, .chzn-drop .chzn-search input{
    width:100% !important;
  }
</style>
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Listado</a>
    <a class="nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Calendario</a>    
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
  	<?= $output ?>
  </div>
  <div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  	<div id="dp"></div>
  </div>
</div>

<?php $this->load->view('crearReservaModal'); ?>
<?php $this->load->view('reservaDetailModal'); ?>
<?php $this->load->view('calendario'); ?>
<?php $this->load->view('_add_cliente_modal',array(),FALSE,'movimientos'); ?>
<?php $this->load->view('predesign/chosen'); ?>
<div id="saldo" class="modal" tabindex="-1" role="dialog">  
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">          
          <h4 class="modal-title">Saldo del cliente</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="kt-portlet__head">
          <h1 id="saldoTag" style="text-align: center">0GS</h1> 
          <p style="padding: 0 20px; color:red">Límite de crédito: <span id="limiteSaldoTag"> 0GS </span></p>   
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>          
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php get_instance()->js[] = '<script src="'.base_url().'assets/grocery_crud/js/jquery_plugins/ajax-chosen-jsonlist.js?v=1.3"></script>'; ?>
<script>
  window.afterLoad.push(function(){
    $("#cliente").ajaxChosenJsonList({
         dataType: 'json',
          type: 'POST',
          url:URI+'maestras/clientes/json_list',
          success:function(data,val){l.selectClient(data,val);}
      },{
          loadingImg: URI,
          processItems:function(data){
            var d = [];
            for(var i in data){
              d.push({id:data[i].id,text:data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos});
            }
            return d;
          }
      },{
          "search_contains": true, allow_single_deselect:true
      }); 
  });
  function setCliente(data){
    $("#addCliente").modal('hide');
    if(data.success){
      success(".resultClienteAdd",'Cliente añadido con éxito');
      $.post(URI+'maestras/clientes/json_list',{              
              'clientes_id':data.insert_primary_key
          },function(data){       
              data = JSON.parse(data);   
              selectClient(data);
          });
    }
  }
  function selectClient(data,val){
    var opt = '';   
    console.log(data);     
    //data = JSON.parse(data);
    for(var i in data){
      data[i].mayorista = data[i].mayorista=='activo' || data[i].mayorista=='1'?1:0;          
      opt+= '<option value="'+data[i].id+'" data-mayorista="'+data[i].mayorista+'">'+data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos+'</option>';
    }
    if(data.length==0){
      opt+= '<option value="">Cliente no existe</option>';
    }
    $("#cliente").html(opt);
    $("#cliente").chosen().trigger('liszt:updated');        
    $("#cliente").trigger('change');            
  }
  function saldo(){
    $.post(base_url+'movimientos/ventas/saldo',{cliente:$("#cliente").val()},function(data){
      data = JSON.parse(data);
      $("#saldoTag").html(data[0]);
      $("#limiteSaldoTag").html(data[1]);
      $("#saldo").modal('toggle');
    });
  }
</script>