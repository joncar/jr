<style type="text/css">
    .scheduler_default_corner > div:last-child{
        display:none !important;
    }
</style>
<?php get_instance()->js[] = '<script src="'.base_url().'js/daypilot-all.min.js?v=3091" type="text/javascript"></script>'; ?>
<script type="text/javascript">
    window.afterLoad.push(function(){      
      window.dp = new DayPilot.Scheduler("dp");
      dp.startDate = new DayPilot.Date("<?= date("Y-m-d") ?>");  // or just dp.startDate = "2013-03-25";
      dp.days = 365;
      dp.scale = "Day"; // one day
      dp.timeHeaders = [
          { groupBy: "Month"},
          { groupBy: "Day", format: "d"}
      ]
      dp.bubble = new DayPilot.Bubble({
          onLoad: function(args) {
              var ev = args.source;
              args.html = "Reserva " + ev.text();
          }
      });
      dp.treeEnabled = true;
      dp.rowHeaderWidth = 150;
      dp.cellWidth = 70;
      <?php
        $hab = array();
        foreach($this->db->get_where('habitaciones')->result() as $h){
          $hab[] = array('name'=>$h->habitacion_nombre,'id'=>$h->id);
        }   
      ?>
      dp.resources = <?= json_encode($hab) ?>;
      window.reservas = <?php 
        $reservas = array();
        $this->db->select('reservas.*,clientes.nombres, clientes.apellidos');        
        $this->db->join('clientes','clientes.id = reservas.clientes_id'); 
        $this->db->where('anulado IS NULL OR anulado = 0',NULL,TRUE);
        foreach($this->db->get_where('reservas')->result() as $r){
          $reservas[] = array(            
            'monto'=>$r->monto,
            'primary'=>$r->id,
            'text'=>$r->nombres.' '.$r->apellidos,            
            'id'=>$r->id,
            'relation'=>$r->habitaciones_id,
            'desde'=>$r->fecha_desde.'T12:00:00',
            'hasta'=>$r->fecha_hasta.'T00:00:00'
          );
        }
        echo json_encode($reservas);
      ?>;
      for(var i in reservas){
        var e = new DayPilot.Event({
          start:new DayPilot.Date(reservas[i].desde),
          end:new DayPilot.Date(reservas[i].hasta),
          id:reservas[i].id,
          resource:reservas[i].relation,
          text:reservas[i].text,
          primary:reservas[i].primary,
          monto:reservas[i].monto          
        });
        dp.events.add(e);
      }
      dp.eventHoverHandling = "Bubble";

      dp.onEventResized = function (args) {
        actualizarFechas(args.e.id(),args.newStart.value,args.newEnd.value,args.e.resource());
      };
      dp.onTimeRangeSelected = function (args) {
      	  var reserva = $("#reservaModal");                  
          reserva.find('input[name="fecha_desde"]').val(args.start);
          reserva.find('input[name="fecha_hasta"]').val(args.end);
          reserva.find('input[name="habitaciones_id"]').val(args.resource);
          reserva.modal('show');
      };
      dp.onEventClicked = function(args) {        
        clickEvent(args.e.id());       
      };
      dp.onEventMoved = function (args) {           
      	  actualizarFechas(args.e.id(),args.newStart.value,args.newEnd.value,args.newResource);          
      };
      dp.init();
    });
</script>