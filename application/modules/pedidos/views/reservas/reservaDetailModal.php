<div class="modal" tabindex="-1" role="dialog" id="reservaDetail">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Administrar Reserva</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row ml-0 mr-0">
            <div class="col-12 col-md-4">
              <div class="form-group">
                <label for="">#Habitación</label>
                <input type="text" name="habitacion" class="form-control">
              </div>
            </div>
            <div class="col-12 col-md-4">
              <div class="form-group">
                <label for="">Cliente</label>
                <input type="text" name="cliente" class="form-control">
              </div>
            </div>
            <div class="col-12 col-md-4">
              <div class="form-group">
                <label for="">Monto</label>
                <input type="text" name="monto" id="monto" class="form-control">
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
              <div class="form-group">
                <label for="">Guía</label>
                <input type="text" name="guia" id="guia" class="form-control">
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
              <div class="form-group">
                <div style="margin-top: 20px;"></div>
                <label for="embarcacion">Embarcación</label>
                <input type="checkbox" name="embarcacion" id="embarcacion">
                <div></div>
                <label for="carnadas">Carnadas</label>
                <input type="checkbox" name="carnadas" id="carnadas">
              </div>
            </div>
            <div class="col-12 col-md-12">
              <div class="">
                <label for="">Observaciones</label>
              </div>
              <div class="form-group">                
                <textarea name="observaciones" id="observaciones" class="form-control"></textarea>
              </div>
            </div>
            <div class="col-12 response2"></div>
          </div>          
        </div>
        <div class="modal-footer text-right">                    
          <a href="" class="btn btn-info mod" target="_blank">Modificar</a>
          <button type="button" class="btn btn-primary conectar" onclick="conectar()">Aperturar</button>
          <button type="button" class="btn btn-danger anular" onclick="anular()">Anular</button>          
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        </div>
      </div>
    </div>
</div>

<script>
	function clickEvent(id){
    $(".response2").attr('class','col-12 response2').html('');
    var detail = $("#reservaDetail");
		window.clickEventId = id;
    info('.response2','Consultando reserva por favor espere');
    $.post('<?= base_url() ?>pedidos/reservas/reservas/json_list',{
      id:id
    },function(data){
      data = JSON.parse(data);
      if(data.length>0){
        data = data[0];
        detail.find('input[name="cliente"]').val(data.s3eb7f57f);
        detail.find('input[name="monto"]').val(data.monto);
        detail.find('input[name="habitacion"]').val(data.habitaciones_id);
        detail.find('input[name="guia"]').val(data.guia);
        detail.find('textarea[name="observaciones"]').val(data.observaciones);
        detail.find('input[name="carnadas"]').prop('checked',data.carnadas=='activo');
        detail.find('input[name="embarcacion"]').prop('checked',data.embarcacion=='activo');
        detail.find('.mod').attr('href','<?= base_url() ?>pedidos/reservas/reservas/edit/'+data.id);
      }
      $(".response2").attr('class','col-12 response2').html('');
    });
		$("#reservaDetail").modal('show');
	}

  function conectar() {
      var detail = $("#reservaDetail");
      var datos = {
          sucursales_id:<?= $this->user->sucursal ?>,
          cajadiaria:'<?= $_SESSION['cajadiaria'] ?>',
          facturado: 0,
          fecha_pedido: '<?= date("Y-m-d H:i:s") ?>',
          mozos_id: '<?= !empty($this->user->mozo) ? $this->user->mozo : 0 ?>',
          user_id:<?= $this->user->id ?>,
          habitaciones_id:detail.find('input[name="habitacion"]').val(),
          tipo_pedidos_id:1,
          reservas_id:window.clickEventId
      };            
      $.post("<?= base_url("pedidos/admin/pedidos/insert_validation") ?>", datos, function (data) {
          data = data.replace('<textarea>', '', data);
          data = data.replace('</textarea>', '', data);
          data = JSON.parse(data);
          if (data.success) {
              $.post("<?= base_url("pedidos/admin/pedidos/insert") ?>", datos, function (data) {
                  data = data.replace('<textarea>', '', data);
                  data = data.replace('</textarea>', '', data);
                  data = JSON.parse(data);
                  if (data.success) {
                    document.location.href = "<?= base_url('pedidos/admin/pedidos_detalles') ?>/" + data.insert_primary_key + '/';
                  }else{
                    error('.response2','Ocurrio un error');                    
                  }
              });
          } else {
              error('.response2',data.error_message);              
              //document.location.reload();
          }
      });
  }

	function anular(){
		var id = window.clickEventId;
    info('.response2','Anulando por favor espere...');		    
		$.post('<?= base_url('pedidos/reservas/reservas/anular') ?>',{id:id},function(data){
			$("#reservaDetail").modal('hide');
			var e = dp.events.find(id);
      dp.events.remove(e).queue();
		});
	}

  function actualizarFechas(id,desde,hasta,habitacion){
    $.post('<?= base_url('pedidos/reservas/reservas/actualizarFechas') ?>',{
      id:id,
      desde:desde,
      hasta:hasta,
      habitacion:habitacion
    },function(data){      
      if(data=='true'){
        dp.message("La reserva se ha actualizado");
      }else{
        dp.message("Ya existe una reserva en esta fecha");
      }
    });
  }
</script>