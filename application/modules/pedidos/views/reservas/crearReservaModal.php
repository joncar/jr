<div class="modal" tabindex="-1" role="dialog" id="reservaModal">
  <form action="" method="post" onsubmit="insertar('pedidos/reservas/reservas/insert',this,'.response',function(data){reservaCreada(data);}); return false;">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Crear Reserva</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row ml-0 mr-0">
            <div class="col-12 col-md-6 col-lg-3">
              <div class="form-group">
                Cliente<a href="#addCliente" data-toggle="modal" style="color:green"><i class="fa fa-plus"></i></a>: 
        				<?php 
        					$this->db->limit(1);
        					echo form_dropdown_from_query('clientes_id','clientes','id','nro_documento nombres apellidos',1,'id="cliente"') 
        				?>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
              <div class="form-group">
                <label for="">Monto</label>
                <input type="text" name="monto" id="monto" class="form-control">
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
              <div class="form-group">
                <label for="">Guía</label>
                <input type="text" name="guia" id="guia" class="form-control">
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
              <div class="form-group">
                <div style="margin-top: 20px;"></div>
                <label for="embarcacion">Embarcación</label>
                <input type="checkbox" name="embarcacion" id="embarcacion">
                <div></div>
                <label for="carnadas">Carnadas</label>
                <input type="checkbox" name="carnadas" id="carnadas">
              </div>
            </div>
            <div class="col-12 col-md-12">
              <div class="">
                <label for="">Observaciones</label>
              </div>
              <div class="form-group">                
                <textarea name="observaciones" id="observaciones" class="form-control"></textarea>
              </div>
            </div>
            <div class="col-12 response"></div>
          </div>
        </div>
        <div class="modal-footer text-right">          
          <input type="hidden" name="fecha_desde">
          <input type="hidden" name="fecha_hasta">
          <input type="hidden" name="habitaciones_id">
          <input type="hidden" name="user_id" value="<?= $this->user->id ?>">
          <input type="hidden" name="sucursal" value="<?= $this->user->sucursal ?>">
          <input type="hidden" name="cajadiaria" value="<?= $this->user->cajadiaria ?>">
          <input type="hidden" name="caja" value="<?= $this->user->caja ?>">
          <input type="hidden" name="anulado" value="0">
          
          <button type="submit" class="btn btn-primary">Crear Reserva</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        </div>
      </div>
    </div>
  </form>
</div>

<script>
	function reservaCreada(data){	  
	  if(data.success){
	  	  var reserva = $("#reservaModal");  	      
	      dp.clearSelection();	      
	      var e = new DayPilot.Event({
	          start: reserva.find('input[name="fecha_desde"]').val(),
	          end: reserva.find('input[name="fecha_hasta"]').val(),
	          id: /*DayPilot.guid(),*/ data.insert_primary_key,
	          resource: reserva.find('input[name="habitaciones_id"]').val(),
	          text: reserva.find("#cliente").val()+'-'+reserva.find("#cliente option:selected").html()
	      });	      
	      dp.events.add(e);
	      dp.message("Created"); 
	      reserva.modal('toggle');
	      reserva.find('input[type="monto"]').val('');
	      setCliente({success:true,insert_primary_key:1});
      }	 
	}
</script>