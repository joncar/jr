<?php get_instance()->hcss[] = '
  <style>
    .kt-grid.kt-grid--hor-desktop.kt-grid--desktop{
      display:none;
    }

    .kt-aside--enabled .kt-header.kt-header--fixed{
      left:0;
    }
  </style>
'; ?>
<?php              
    switch($pedidos->tipo_pedidos_id){
         case 1://Habitación
             $label =  '<i class="fa fa-bed"></i> '.$this->db->get_where('habitaciones',array('id'=>$pedidos->habitaciones_id))->row()->habitacion_nombre.'';
         break;
         case 2://parrillas
            $label = '<i class="fa fa-drumstick-bite"></i> '.$this->db->get_where('parrillas',array('id'=>$pedidos->parrillas_id))->row()->nombre_parrilla.'';
         break;
         case 3://camping
            $label = '<i class="fa fa-campground"></i> '.$this->db->get_where('campings',array('id'=>$pedidos->campings_id))->row()->nombre_camping.'';
         break;
         case 4://mesa
            $label = '<i class="fa fa-utensils"></i> '.$this->db->get_where('mesas',array('id'=>$pedidos->mesas_id))->row()->nombre_mesa.'';
         break;
         case 5://delivery
            $label = '<i class="fa fa-motorcycle"></i> '.$this->db->get_where('deliverys',array('id'=>$pedidos->deliverys_id))->row()->nombre_delivery.'';
         break;
         case 6://barra
            $label = '<i class="fa fa-chair"></i> '.$this->db->get_where('barras',array('id'=>$pedidos->barras_id))->row()->nombre_barra.'';
         break;
    }
?>
<!--<div class="alert alert-info">
    <i class="fa fa-question-circle"></i> Marca los productos de la pestaña <b>Pedido</b> antes de facturarlos en la pestaña de <b>Facturar</b>
</div>-->
<?php if($pedidos->facturado==1): ?>
    <div class="alert alert-warning">
        <i class="fa fa-exclamation-triangle"></i> Pedido ya facturado por lo que no se pueden realizar cambios en el mismo.
    </div>
<?php endif ?>
<div>  
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item" role="presentation"><a class="nav-link active" href="#home" aria-controls="home" role="tab" data-toggle="tab"><?= $label ?></a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Facturar</a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" href="#profile2" aria-controls="profile" role="tab" data-toggle="tab">Facturas</a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" href="#profile4" aria-controls="profile" role="tab" data-toggle="tab">Estado</a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" href="#profile3" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-question-circle"></i></a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">        
        <?php $this->load->view('tabs/pedido'); ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
        <?php $this->load->view('tabs/facturar'); ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile2">        
        <?= $facturas->output ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile3">        
        <?php $this->load->view('tabs/help'); ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile4">        
        <?php $this->load->view('tabs/estado'); ?>
    </div>
     <?php $this->load->view('_add_cliente_modal',array(),FALSE,'movimientos'); ?>
     <?= $this->load->view('includes/modals/productosframe') ?>
  </div>

</div>
<?php $this->load->view('predesign/chosen'); ?>
<?php get_instance()->js[] = '<script src="'.base_url('assets/grocery_crud/js/jquery_plugins/jquery.mask.js').'"></script>'; ?>
<script>

    window.afterLoad.push(function(){ 
        window.reportes = [];
        <?php foreach($this->db->get('tipo_facturacion')->result() as $t): ?>
            window.reportes.push(<?= json_encode($t) ?>);
        <?php endforeach ?>
          
        $(document).on('ready',function(){        
            $(".pago, input[name='total[]'], #totalparrilla").mask("000.000.000", {reverse: true});
        });

        window.total = 0;
        window.productos = 0;
        window.totalProductos = 0;
        window.totalizado = 0;
        window.itenes = [];
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {        
            $("#profile select").chosen();
            $("#profile2 form").submit();
            fillNroFactura();
        });
        
        $(document).on("completeRefreshList",function(){
            fillProducts();
        });
        
        $(document).on('click',"tr input, tr select, tr textarea",function(e){       
            e.stopPropagation();
        });
        
        $('#pedidoTable .flexigrid').on('success',function(){
            sumartodoPedido();
        });
        
        $(document).on("change",".producto",function(){        
            $(this).parents('table').find('input').css({'color':'white','background':'gray'});
                $(this).parents('table').find('input').readonly;
                var form = $(this).parents('tr');
                var data = {};
                var producto = $(this).val();
                data['detalle'] = "";
                data['cantidad'] = 1;
                data['pedidos_id'] = <?= $pedido ?>;
                data['productos_id'] = producto;
                data['precio_venta'] = form.find('.producto').data('precio_venta');
                data['precio_costo'] = form.find('.producto').data('precio_costo');
                data['total'] = form.find('.producto').data('precio_venta');
                $.post('<?= base_url('pedidos/admin/pedidos_detalles/'.$pedido.'/insert/') ?>',data,function(data){
                    $('.flexigrid').on('success',function(){
                        setTimeout(function(){
                            $("#c"+producto).focus();              
                            sumartodoPedido();
                        },1000);                    
                        $('.flexigrid').unbind('success');
                    });
                    $("#pedidoTable .ajax_refresh_and_loading").trigger('click');
                    
                });
        });
        
        $(document).on('click',"tr",function(e){       
            var chk = $(this).find(".chk");
            if(typeof(chk)!=='undefined' && chk.length>0){
                var checked = chk.prop('checked');
                chk.prop('checked',checked?false:true);
                chk.trigger('clickchk');
            }
        });
        
        $(document).on("click clickchk",".chk",function(){
            if(!$(this).prop('checked')){
                $("#todos").prop('checked',false);
            }
            var cantidad = parseFloat($(this).data('cantidad'));
            if(cantidad>0 && parseFloat($(this).data('precio'))!==0){
                fillProducts();
                $(".pago,.vuelto").val(0);
            }else{
                alert("El producto elegido ya fue liquidado");
                $(this).prop('checked',false);
            }
        });
        
        $(document).on("click","#todos",function(){
            $(".chk").prop('checked',$(this).prop('checked'));
            fillProducts();
        });
        
        $(document).on("change","input[name='cantidad[]']",function(){
            var max = parseFloat($(this).data('cantidad'));
            var val = parseFloat($(this).val());
            if(max<val || val<=0){
                alert("El numéro máximo permitido para este campo es "+max);
                $(this).val(max);
            }else{
                totalizar();
            }
        });
        
        $(document).on("change",".pago",function(){
            var val = parseFloat($(this).val());
            if(isNaN(val)){
                $(this).val(0);
            }
            relacionDeudaPago();
        });    
        
        $(document).on("change","input[name='total_efectivo']",function(){        
            var val = parseFloat($(this).val().replace(/\./g,''));
            var total = window.total-<?= $pedidos->abonado ?>;
            var vuelto = val-total;
            vuelto = vuelto<0?0:vuelto;
            $("input[name='vuelto']").val(vuelto.formatMoney(0,',','.'));
            relacionDeudaPago();
        });

        Number.prototype.formatMoney = function(c, d, t){
        var n = this, 
            c = isNaN(c = Math.abs(c)) ? 2 : c, 
            d = d == undefined ? "." : d, 
            t = t == undefined ? "," : t, 
            s = n < 0 ? "-" : "", 
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
            j = (j = i.length) > 3 ? j % 3 : 0;
           return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
         };
    });

    function setCliente(data){
        $("#addCliente").modal('hide');
        if(data.success){
            success(".resultClienteAdd",'Cliente añadido con éxito');
            $.post(URI+'maestras/clientes/json_list',{              
                'clientes_id':data.insert_primary_key
            },function(data){                       
                data = JSON.parse(data);
                clientes = $("#cliente,#clientes_id");
                clientes.html('');
                var selected = 0;
                var selectedName = "Seleccione una opcion";
                for(var i in data){                    
                    clientes.append('<option value="'+data[i].id+'">'+data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos+'</option>');
                    if(parseInt(data[i].id)>selected){
                        selected = data[i].id;      
                        selectedName = data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos;
                    }
                }
                clientes.find('option[value="'+selected+'"]').attr('selected',true);
                clientes.chosen().trigger('liszt:updated');
                clientes.parent().find('.chosen-single span').html(selectedName);
            });
        }
    }

    function relacionDeudaPago(){
        var deuda = window.total;
        $(".pago").each(function(){           
            var val = $(this).val();
            val = val.replace(/\./g,''); 
            val = parseFloat(val);            
            if($(this).attr('name')=='total_efectivo'){ 
                var vuelto = $("#profile input[name='vuelto']").val();
                vuelto = vuelto.replace(/\./g,'');
                val-= parseFloat(vuelto);
            }
            deuda-=val;            
            $("#PendientePorCancelar").html(deuda.formatMoney(0,',','.'));
        });
    }
    
    function mostrarPrecios(){
        $("#productos").html(window.totalProductos);
        $("#productosElegidos").html(window.productos);
        $("#importeTotal").html(window.totalizado);
        $("#importeAFacturar").html(window.total.formatMoney(0,',','.'));
        var EfectivoACobrar = window.total-<?= $pedidos->abonado ?>;
        $("#EfectivoACobrar").html(EfectivoACobrar.formatMoney(0,',','.'));
        $("input[name='total_venta']").val(window.total);
        $("#importePendiente").html(window.totalizado-window.total);
        relacionDeudaPago();
    }
    function totalizar(){
        window.productos = 0;
        window.total = 0;
        var cantidades = $("input[name='cantidad[]']");
        if(cantidades.length===0){
            mostrarPrecios();
        }
        cantidades.each(function(){
            if(!$(this).hasClass("retrato")){
                var o = $(this);            
                window.productos+= parseFloat(o.val());
                var importe = parseFloat(o.val())*parseFloat(o.data("precio"));                
                window.total+= parseInt(importe.toFixed(0));
                o.parents("tr").find("input[name='total[]']").val(importe.toFixed(0));
            }
            mostrarPrecios();
        });
    }
    
    function fillProducts(){
        $("#emptylist").show();
        $(".productlist").remove();
        var inputs = $(".chk").length;
        var carrete = 0;                
        window.totalProductos = 0;
        window.totalizado = 0;
        window.itenes = [];
        $(".chk").each(function(){
            carrete++;
            window.totalProductos+= parseFloat($(this).data("cantidad"));
            window.totalizado+=parseFloat($(this).data("total"));
            if($(this).prop('checked')){
                window.itenes.push($(this).val());
                $("#emptylist").hide();
                var fila = $("#productlist").clone();
                fila.removeAttr('id');
                fila.addClass("productlist");
                fila.removeClass("d-none");
                var n = fila.html();
                n = n.replace("{nombre}",$(this).data("nombre"));                                
                fila.html(n);                
                var inputcantidad = fila.find("#cantidadInput");
                inputcantidad.removeAttr('id');
                inputcantidad.attr('name',"cantidad[]");
                inputcantidad.val($(this).data("cantidad"));
                inputcantidad.attr("data-precio",$(this).data("precio"));
                inputcantidad.attr("data-cantidad",$(this).data("cantidad"));
                var inputtotal = fila.find("#totalInput");
                inputtotal.removeAttr('id');
                inputtotal.attr('name','total[]');
                inputtotal.val($(this).data("total"));
                inputtotal.attr("data-total",$(this).data("total"));
                fila.find(".retrato").removeClass("retrato");
                $("#listado").append(fila);
            }
            if(carrete===parseFloat(inputs)){
                totalizar();
            }
        });
    }
    
    function fillNroFactura(){
        $.post("<?= base_url("movimientos/ventas/next_nro_factura") ?>",{},function(data){
            $("input[name='nro_factura']").val(data);
            $("#nrofactura").html(data);
        });
    }    

    function limpiar_campo(valor){
        valor = valor.replace(/\./g,'');
        valor = valor.replace(/\,/g,'.');
        return valor;
    }

    function validar_pago(){
        var pendiente = window.total-<?= $pedidos->abonado ?>;
        var pagado = 0;
        pagado+= parseFloat(limpiar_campo($("input[name='total_efectivo']").val()));
        pagado+= parseFloat(limpiar_campo($("input[name='total_debito']").val()));
        pagado+= parseFloat(limpiar_campo($("input[name='total_credito']").val()));
        pagado+= parseFloat(limpiar_campo($("input[name='total_cheque']").val()));
        console.log(pendiente+'!='+pagado);
        if(pendiente>pagado){
            return false;
        }
        return true;
    }
    function send(form){       
        if(!validar_pago()){
            alert("El pago no ha sido cargado correctamente, verifique que la sumatoria de los importes es igual a la totalidad del saldo pendiente.");
            return false;
        }
        $("#cargarFactura").attr('disabled',true);
        var f = new FormData(form);
        for(var i in window.itenes){
            f.append('codigo[]',window.itenes[i]); 
        }
        $.ajax({
            url:'<?= base_url("pedidos/admin/ventas/insert_validation") ?>',
            type:'POST',
            contentType:false,
            data:f,
            processData:false,
            cache:false,
            success:function(data){
                data = data.replace("<textarea>","",data);
                data = data.replace("</textarea>","",data);
                data = JSON.parse(data);
                if(data.success){
                    $.ajax({
                            url:'<?= base_url("pedidos/admin/ventas/insert") ?>',
                            type:'POST',
                            contentType:false,
                            data:f,
                            processData:false,
                            cache:false,
                            success:function(data){
                                data = data.replace("<textarea>","",data);
                                data = data.replace("</textarea>","",data);
                                data = JSON.parse(data);
                                if(data.success){
                                    $("#cargarFactura").attr('disabled',false);
                                    $("#messagebox").removeClass('alert').removeClass('alert-danger');
                                    var msjBox = 'Su factura ha sido creada con éxito';
                                    
                                    if($("select[name='tipo_facturacion_id']").val()==1){
                                        imprimir(window.reportes[0].reporte+data.insert_primary_key);
                                    }else{
                                        imprimir(window.reportes[1].reporte+data.insert_primary_key);
                                    }
                                    
                                    for(var i in window.reportes){                                        
                                        msjBox+= "<a href='javascript:imprimir(\""+window.reportes[i].reporte+data.insert_primary_key+"\")'> Imprimir "+window.reportes[i].denominacion+"</a> |";
                                    }
                                    $("#messagebox").addClass('alert').addClass('alert-success').html(msjBox);
                                    fillNroFactura();
                                    $(".pago").val(0);
                                    $("input[name='vuelto']").val(0);
                                    $.post('<?= base_url('pedidos/admin/pedidos/json_list') ?>',{'search_field[]':'id','search_text[]':<?= $pedidos->id ?>},function(data){
                                        data = JSON.parse(data);
                                        if(data[0].facturado==1){
                                            //document.location.reload();
                                            $(".kt-container").prepend('<div class="alert alert-warning">Pedido ya facturado, refresque para poder reaperturar la habitación</div>');
                                            $("button[type='submit']:not(#btnActualizarEstado)").before('<div class="alert alert-warning">Pedido ya facturado, refresque para poder reaperturar la habitación</div>');
                                            $("button[type='submit']:not(#btnActualizarEstado)").remove();
                                        }
                                    });
                                    $("#pedidoTable .ajax_refresh_and_loading").trigger('click');
                                    $("#profile2 .ajax_refresh_and_loading").trigger('click');
                                }else{
                                    $("#cargarFactura").attr('disabled',false);
                                    $("#messagebox").addClass('alert').addClass('alert-danger').html(data.error_message);
                                }
                            }
                        });
                }else{
                    $("#cargarFactura").attr('disabled',false);
                    $("#messagebox").addClass('alert').addClass('alert-danger').html(data.error_message);
                }
            }
        });
        return false;
    }
    
    function imprimir(url){
            window.open('<?= base_url() ?>'+url);
     }
     function imprimir2(codigo){
            window.open('<?= base_url('movimientos/ventas/ventas/imprimir2/') ?>/'+codigo);
     }
     function imprimirCuenta(){
         window.open('<?= base_url() ?>reportes/rep/verReportes/172/html/valor/<?= $pedido ?>');
     }

     function imprimirCocina(){
         window.open('<?= base_url() ?>reportes/rep/verReportes/170/html/valor/<?= $pedido ?>');
     }
     
     function searchProduct(){
         $('#productosModal').modal('show');
     }
     
     window.afterLoad.push(function(){     
         $(document).on('keyup','#filtering_form tbody input',function(e){                         
             if(e.which==13){ //Enter
                 //e.preventDefault();
                 //return false;
             }
         });
         
         $(window).keyup(function(e){  
             //alert(e.which);
             if(e.which==27){ //ESC
                 $(".modal-footer button").trigger('click');
             }
             if(e.which==115){ //F4             
                 $("#productosTable .searchRow").removeClass('hide');

                 var input = $("#productosTable .searchRow input[type='text']")[1];             
                 input.focus();
                 $(input).val('');
             }
             if(e.which==119){ //F8
                 imprimirCuenta(); 
             }
             if(e.which==113){ //F2
                 searchProduct();
             }
         });
         var query = undefined;
         var keyboard = undefined
         $(document).on('keyup','#pedidoTable .ajax_list .chzn-search input',function(e){
             if(e.which!==40 && e.which!==38){                
                    if(typeof(query)!=='undefined'){
                       query.abort();
                    }
                    if(typeof(keyboard)!=='undefined'){
                       clearTimeout(keyboard);
                    }
                    var val = $(this).val();
                    keyboard = setTimeout(function(){
                        query = $.post('<?= base_url('movimientos/productos/buscador_productos/json_list') ?>',{'search_field[]':'nombre_comercial','search_text[]':val},function(data){
                            var data = JSON.parse(data);        
                            str = '<option>Seleccione una opcion</option>';
                            for(var i in data){
                                str+= '<option value="'+data[i].id+'" data-precio_venta="'+data[i].precio_venta+'" data-precio_costo="'+data[i].precio_costo+'">'+data[i].nombre_comercial+'</option>';
                            }
                            $(".pro").html(str);
                            $(".pro").chosen().trigger('liszt:updated');         
                            $("#pedidoTable .ajax_list .chosen-search input").val(val);
                        });
                    },800);
            }
         });
         
         $(document).on('change','.pro',function(){
             $(".inp.producto").val($(this).val());
             $(".inp.producto").attr('data-precio_venta',$(this).find('option:selected').data('precio_venta'));
             $(".inp.producto").attr('data-precio_costo',$(this).find('option:selected').data('precio_costo'));
             $(".inp.producto").trigger("change");
         });
    });
     
     function enviarBusqueda(id,valor,precio_venta,precio_costo){
         $(".inp.producto").val(id);
         $(".inp.producto").attr('data-precio_venta',precio_venta);
         $(".inp.producto").attr('data-precio_costo',precio_costo);
     }
     
     function seleccionarProducto(id,valor,precio_venta,precio_costo){        
         enviarBusqueda(id,valor,precio_venta,precio_costo);
         $(".inp.producto").trigger("change");
         $(".pro").val(valor);
         $("#productosModal .modal-footer button").trigger('click');
     }
     
     <?php if($pedidos->tipo_pedidos_id!=2): ?>
        function sumartodoPedido(){
            $("#totalparrilla").val('0');
            var x = 0;
            $("#pedidoTable .flexigrid .total").each(function(){            
                var total = parseFloat($("#totalparrilla").val().replace(/\./g,''));
                total = total+parseFloat($(this).html());
                $("#totalparrilla").val(total.formatMoney(0,',','.'));
                x++;
            });
        } 
        window.afterLoad.push(function(){
            sumartodoPedido();
        });   
     <?php endif ?>
</script>
