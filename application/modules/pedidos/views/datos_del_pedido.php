<style>
    #productosTable tbody > tr > td:nth-child(5){
        max-width:10px !important;
    }
    .table th,.table td {
        padding: 0;
    }
    .table th:last-child input {
        display:none
    }
    #productosTable .table th:first-child input {
        display:none
    }
    #productosTable .table th:first-child,#productosTable .table td:first-child {
        display:none
    }
  @media screen and (max-width:1024px){
    #productosTable thead th{
      min-width:120px;
    }

    #pedidoTable{
        position: fixed;
        left: 100%;
        width: 100%;
        height: 100vh;
        top: 100px;
        transition: .2s ease-in-out;
    }

     #pedidoTable.show{
        left: 5%;
        width: calc(95%);
    }

    #pedidoTable.show::before {
      content: ' ';
      positioN: fixed;
      left: 0;
      top: 0;
      width: 100%;
      height: 100vh;
      background: #00000059;
      z-index: 1;
    }

    #pedidoTable .card{
        z-index:2;
    }

    #pedidoTable .cartToggle{
        position: fixed;
        left: 92%;
        z-index: 3;
        top: 5%;
        width: 20px;
        height: 20px;
    }

    #pedidoTable.show .cartToggle{
        position: absolute;
        left: -51px;
        z-index: 3;
        top: -19%;
        width: 20px;
        height: 20px;
    }
    .cartToggle span{
        font-weight: bold;
        font-size: 20px;
    }
    
  }
</style>
<div class="row" style="margin-left:0; margin-right:0px; margin-top:10px">
    <?php if($pedidos->facturado==0): ?>
    <div class="col-12 col-xl-6" id="productosTable">
        <div class="card">     
            <div class="card-header">
                Productos
            </div>       
            <div>                
                <?= $productos->output ?>
            </div>
        </div>
    </div>
    <?php endif ?>
    <div class="col-12 col-xl-<?= $pedidos->facturado==0?'6':'12' ?>" id="pedidoTable">        
        <div class="card">
            <div class="d-xl-none cartToggle" style="position: absolute;left: -65px;z-index: 3;">
              <button class="btn btn-info dialogCart d-flex" type="button">
                <span>+2</span> 
                <i class="fa fa-shopping-cart"></i>
              </button>        
            </div>
            <div class="card-header" style="padding:0">
                <div style="position: relative;">
                    <i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 7px;cursor:pointer;"></i>
                    <input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" style="padding-left: 25px;padding-right: 73px;">
                    <button style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar">Insertar</button>            
                </div>  
            </div>
            <div>                                
                <?= $output ?>                
            </div>       
        </div>
    </div>
</div>
<script>
    window.afterLoad.push(function(){    
        $('#productosTable .flexigrid').on('success',function(){          
            $("#pedidoTable .ajax_refresh_and_loading").trigger('click');
        });
        /*$(document).on('keydown','.cantidadProducto',function(e){              
            if(e.which===13){
                e.preventDefault();
                $(this).parents('tr').find(".refreshlist").trigger('click');
            }
        });*/
        $(document).on('click',".refreshlist",function(){
            var x = $(this).parents('tr').find('.cantidadProducto');
            if(x.val()===''){
                x.val('*1');
            }
            $("#productosTable .filtering_form").submit();
        });        
        $(document).on('change','#codigoAdd',function(){
            var url = $("#productosTable form").attr('action');
            var cantidad = 1;
            var codigo = $(this).val();
            if(codigo!=''){
                codigo = codigo.split('*');
                if(codigo.length==2){
                    cantidad = codigo[0];
                    codigo = codigo[1];
                }else{
                    codigo = codigo[0];
                    codigo = codigo.split('+');
                    if(codigo.length==2){
                        cantidad = codigo[0];
                        codigo = codigo[1];                   
                    }
                    else{
                        cantidad = 1;
                        codigo = codigo[0];
                    }
                }
                $('#codigoAdd').removeClass('is-valid is-invalid');
                $.post(url,{cantidad:cantidad,codigo:codigo},function(data){
                    data = JSON.parse(data);
                    if(data.success){
                        $('#codigoAdd').addClass('is-valid').attr('placeholder','Producto añadido').val('');
                    }else{
                        $('#codigoAdd').addClass('is-invalid').attr('placeholder',data.msj).val('');
                    }
                    $("#pedidoTable .ajax_refresh_and_loading").trigger('click');
                });
            }
        });
        $("#insertar").on('click',function(){
            $('#codigoAdd').trigger('change');
        });

        $("#productosTable .flexigrid").on('success',function(){
            $('.chosen-multiple-select').removeAttr('id').removeClass('chzn-done').chosen({"search_contains": true, allow_single_deselect:true});
        });

        $("#pedidoTable .flexigrid").on('success',function(){
            $(".cartToggle span").html('+'+$("#pedidoTable .ajax_list tr").length);
        });

        

        $(document).on('keyup','li.search-field input',function(e){
            if(e.which==13 && $(this).val()!=''){
                var newVal = $(this).val();
                var enc = false;
                var opts = $(this).parents('td').find("select.observ option").toArray();
                for(var i in opts){
                    if($(opts[i]).val()==newVal){
                        enc = true;
                    }
                }
                if(!enc){
                    //Añadir nuevo elemento                    
                    $(this).parents('td').find("select.observ").append('<option value="'+newVal+'" selected="true">'+newVal+'</option>');
                    $(this).parents('td').find("select.observ").chosen().trigger('liszt:updated');
                }
            }
        });

        $(document).on('click','.cartToggle',function(){
            if($("#pedidoTable").hasClass('show')){
                $("#pedidoTable").removeClass('show');
            }else{
                $("#pedidoTable").addClass('show');
            }
        });

        
    });

</script>