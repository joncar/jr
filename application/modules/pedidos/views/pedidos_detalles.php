
<?php 
    switch($pedido->tipo_pedidos_id){
         case 1://Habitación
             echo '<h1>Habitación: '.$this->db->get_where('habitaciones',array('id'=>$pedido->habitaciones_id))->row()->habitacion_nombre.'</h1>';
         break;
         case 2://parrillas
            echo '<h1>Parrilla: '.$this->db->get_where('parrillas',array('id'=>$pedido->parrillas_id))->row()->nombre_parrilla.'</h1>';
         break;
         case 3://camping
            echo '<h1>Campings: '.$this->db->get_where('campings',array('id'=>$pedido->campings_id))->row()->nombre_camping.'</h1>';
         break;
         case 4://camping
            echo '<h1>Mesas: '.$this->db->get_where('mesas',array('id'=>$pedido->mesas_id))->row()->nombre_mesa.'</h1>';
         break;
         case 5://deliverys
            echo '<h1>Deliverys: '.$this->db->get_where('deliverys',array('id'=>$pedido->deliverys_id))->row()->nombre_delivery.'</h1>';
         break;
         case 6://Barras
            echo '<h1>Barra: '.$this->db->get_where('barras',array('id'=>$pedido->barras_id))->row()->nombre_barra.'</h1>';
         break;
    }
?>
<?= $output ?>
<script>
  window.afterLoad.push(function(){
      $("#field-total, #field-precio_venta,#field-precio_costo").attr('readonly',true);
     $("#field-productos_id").on('change',function(){
         var id = parseInt($(this).val());
         if(!isNaN(id)){
              $.post('<?= base_url('movimientos/productos/productos/json_list') ?>',{search_field:'id',search_text:id},function(data){
                  data = JSON.parse(data);
                  data = data[0];
                  $("#field-cantidad").val(1);
                  $("#field-precio_venta").val(data.precio_venta);
                  $("#field-precio_costo").val(data.precio_costo);
                  totalizar();
              });
          }
     });
     $("#field-cantidad").on('change',function(){
      totalizar();
     });
   });
   
   function totalizar(){
       var cantidad = parseFloat($("#field-cantidad").val());
       var precio = parseFloat($("#field-precio_venta").val());
       if(!isNaN(cantidad) && !isNaN(precio)){
           $("#field-total").val(cantidad*precio);
       }
   }
</script>