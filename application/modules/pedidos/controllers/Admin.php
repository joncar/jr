<?php

require_once APPPATH . '/controllers/Panel.php';

class Admin extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }            
    }

    function pedidos() {
        $crud = $this->crud_function('', '');
        $crud->field_type('user_id','hidden',$this->user->id)
             ->field_type('facturado','hidden',0)
             ->field_type('cajadiaria','hidden',$_SESSION['cajadiaria'])
             ->field_type('sucursales_id','hidden',$this->user->sucursal)
             ->field_type('clientes_id','Cliente')
             ->field_type('fecha_pedido','hidden',date("Y-m-d H:i:s"));
        if($crud->getParameters()=='list'){
            $crud->field_type('facturado','true_false',array('0'=>'<span style="color:red">NO</span>','1'=>'<span style="color:green">SI</span>'));
        }
        $crud->unset_searchs('total');
        if($crud->getParameters()=='list'){
            $crud->set_relation('clientes_id','clientes','{nombres} {apellidos}');
        }
        $crud->columns('id','clientes_id','fecha_pedido','habitaciones_id','parrillas_id','campings_id','mesas_id','deliverys_id','barras_id','facturado','total');
        $crud->callback_field('mozos_id',function($val,$row){
            get_instance()->db->select('mozos.id, user.nombre');
            get_instance()->db->join('mozos','mozos.user_id = user.id');
            return form_dropdown_from_query('mozos_id','user','id','nombre',$val,'id="field-mozos_id"');
        });
        $crud->callback_column('total',function($val,$row){
            $total = $this->db->query('select FORMAT(SUM(total),2,"de_DE") as total from pedidos_detalles where pedidos_id = '.$row->id)->row()->total;
            return $total;
        }); 

        $crud->callback_before_delete(function($primary){
            $pedido = get_instance()->db->get_where('pedidos',array('id'=>$primary))->row();
            if($pedido->facturado==1){
                return false;
            }else{
                switch($pedido->tipo_pedidos_id){
                    case '1':
                        get_instance()->db->update('habitaciones',array('estado'=>0),array('id'=>$pedido->habitaciones_id));
                    break;
                    case '2':
                        get_instance()->db->update('parrillas',array('estado'=>0),array('id'=>$pedido->parrillas_id));
                    break;
                    case '3':
                        get_instance()->db->update('campings',array('estado'=>0),array('id'=>$pedido->campings_id));
                    break;
                    case '4':
                        get_instance()->db->update('mesas',array('estado'=>0),array('id'=>$pedido->mesas_id));
                    break;
                    case '5':
                        get_instance()->db->update('deliverys',array('estado'=>0),array('id'=>$pedido->deliverys_id));
                    break;
                    case '6':
                        get_instance()->db->update('barras',array('estado'=>0),array('id'=>$pedido->barras_id));
                    break;
                }
                foreach(get_instance()->db->get_where('pedidos_detalles',array('pedidos_id'=>$primary))->result() as $p){
                    get_instance()->reversar_stock((array)$p);
                }
                get_instance()->db->delete('pedidos_detalles',array('pedidos_id'=>$primary));
            }
        });
        
        $crud->callback_after_insert(function($post,$primary){
            if(!empty($post['habitaciones_id'])){
                get_instance()->db->update('habitaciones',array('estado'=>1,'pedidos_id'=>$primary),array('id'=>$post["habitaciones_id"]));
            }
            if(!empty($post['parrillas_id'])){
                get_instance()->db->update('parrillas',array('estado'=>1,'pedidos_id'=>$primary),array('id'=>$post["parrillas_id"]));
            }
            if(!empty($post['campings_id'])){
                get_instance()->db->update('campings',array('estado'=>1,'pedidos_id'=>$primary),array('id'=>$post["campings_id"]));
            }
            if(!empty($post['mesas_id'])){
                get_instance()->db->update('mesas',array('estado'=>1,'pedidos_id'=>$primary),array('id'=>$post["mesas_id"]));
            }
            if(!empty($post['deliverys_id'])){
                get_instance()->db->update('deliverys',array('estado'=>1,'pedidos_id'=>$primary),array('id'=>$post["deliverys_id"]));
            }
            if(!empty($post['barras_id'])){
                get_instance()->db->update('barras',array('estado'=>1,'pedidos_id'=>$primary),array('id'=>$post["barras_id"]));
            }
        });
        if($crud->getParameters()=='edit'){
            $crud->required_fields('tipos_pedidos_id');
        }else{
            $crud->set_rules("tipo_pedidos_id","tipo de pedido",'required|callback_validate_tipo_pedido');        
        }
        $crud->set_relation('habitaciones_id','habitaciones','habitacion_nombre',array('sucursales_id'=>$this->user->sucursal));
        $crud->set_lang_string('insert_success_message','Pedido almacenado con éxito <script>document.location.href="'.base_url('pedidos/admin/pedidos_detalles').'/{id}/add"</script>');
        $crud->add_action('<i class="fa fa-book"></I> Detalles','',base_url('pedidos/admin/pedidos_detalles').'/');
        $crud->where('pedidos.sucursales_id',$this->user->sucursal);
        $crud->unset_columns('user_id');
        
        $crud = $crud->render();
        $crud->output = $this->load->view('pedidos',array('output'=>$crud->output),TRUE);
        $crud->title = 'Pedidos';
        $this->loadView($crud);
    }       
    
    function validate_tipo_pedido(){
        switch($_POST['tipo_pedidos_id']){
            case '1':
                if($this->db->get_where('habitaciones',array('id'=>$_POST['habitaciones_id'],'estado'=>0))->num_rows()==0){
                    $this->form_validation->set_message("validate_tipo_pedido","Habitación ya ocupada");
                    return false;
                }
            break;
            case '2':
                if($this->db->get_where('parrillas',array('id'=>$_POST['parrillas_id'],'estado'=>0))->num_rows()==0){
                    $this->form_validation->set_message("validate_tipo_pedido","Parrilla ya ocupada");
                    return false;
                }
            break;
            case '3':
                if($this->db->get_where('campings',array('id'=>$_POST['campings_id'],'estado'=>0))->num_rows()==0){
                    $this->form_validation->set_message("validate_tipo_pedido","Campings ya ocupada");
                    return false;
                }
            break;
            case '4':
                if($this->db->get_where('mesas',array('id'=>$_POST['mesas_id'],'estado'=>0))->num_rows()==0){
                    $this->form_validation->set_message("validate_tipo_pedido","Mesa ya ocupada");
                    return false;
                }
            break;
            case '5':
                if($this->db->get_where('deliverys',array('id'=>$_POST['deliverys_id'],'estado'=>0))->num_rows()==0){
                    $this->form_validation->set_message("validate_tipo_pedido","Delivery ya ocupad0");
                    return false;
                }
            break;
            case '6':
                if($this->db->get_where('barras',array('id'=>$_POST['barras_id'],'estado'=>0))->num_rows()==0){
                    $this->form_validation->set_message("validate_tipo_pedido","Barra ya ocupad0");
                    return false;
                }
            break;
        }
        
        return true;
    }
    //Una vez realizado la vista de carga de facturas con sus operaciones logicas, usar esta funcion para almacenar las ventas
    function ventas($x, $unset_delete = FALSE,$return = ""){        
        $crud = new ajax_grocery_CRUD($this);
        $crud->set_table("ventas");
        $crud->set_subject("Facturas");
        $crud->set_theme("bootstrap");
        $crud->where("pedidos_id",$x);
        $crud->required_fields_array();
        $crud->set_rules("cliente","Cliente","required|integer");
        $crud->set_rules("transaccion","Transaccion","required|integer");
        $crud->set_rules("pedidos_id","Pedido","required|callback_validate_pedido");
        //$crud->set_rules("nro_factura","#Factura","required|is_unique[ventas.nro_factura]");
        if($this->router->fetch_method()!='ventas'){
            $crud->unset_add();
        }
        if($unset_delete && $crud->getParameters()=='list'){
           $crud->unset_delete(); 
        }
        $crud->unset_edit()
                 /*->unset_delete()*/
                 ->unset_print()
                 ->unset_export()
                 ->unset_read();
        $crud->columns("fecha","nro_factura","total_venta");
        $crud->add_action('<i class="fa fa-search"></i> Ver Venta','',base_url("movimientos/ventas/ventas/edit")."/");
        $crud->add_action('<i class="fa fa-print"></i> Factura Legal','',base_url("reportes/rep/verReportes/36/pdf/valor/")."/");
        $crud->add_action('<i class="fa fa-print"></i> Factura Lineas','',base_url("movimientos/ventas/ventas/imprimir2")."/");
        $crud->add_action('<i class="fa fa-print"></i> Ticket','',base_url("movimientos/ventas/ventas/imprimirticket")."/");
        $crud->callback_before_insert(function($post){
            $post['total_efectivo'] = str_replace('.','',$post['total_efectivo']);
            $post['vuelto'] = str_replace('.','',$post['vuelto']);
            $post['total_debito'] = str_replace('.','',$post['total_debito']);
            $post['total_cheque'] = str_replace('.','',$post['total_cheque']);
            $post['total_credito'] = str_replace('.','',$post['total_credito']);
            $pedido = get_instance()->db->get_where('pedidos',['id'=>$post['pedidos_id'],'tipo_pedidos_id !='=>2]);
            if($pedido->num_rows()>0){
                $post['entregado'] = 1;
            }
            $tipoFacturacion = $this->db->get_where('tipo_facturacion',array('id'=>$post['tipo_facturacion_id']));
            if($tipoFacturacion->num_rows()==0 || $tipoFacturacion->row()->emite_factura==0){
                $post['nro_factura'] = 0;
            }else{
                $post['nro_factura'] = $this->querys->get_nro_factura();
            }
            return $post;
        });
        $crud->callback_after_insert(function($post,$primary){            
            $codigos = $_POST['codigo'];
            $cantidades = $_POST["cantidad"];
            $totales = $_POST['total'];
            foreach($totales as $n=>$t){
                $totales[$n] = str_replace('.','',$t);
            }
            foreach($codigos as $n=>$v){
                $data = array('venta'=>$primary);                
                $pre = $this->db->get_where('pedidos_detalles',array('id'=>$v))->row();
                $v = $this->db->get_where('productos',array('id'=>$pre->productos_id))->row();                
                $v = $v->codigo;
                $data['producto'] = $v;
                $data['lote'] = '';
                $data['cantidad'] = $cantidades[$n];
                $data['pordesc'] = 0;
                $data['precioventa'] = $pre->precio_venta;
                $data['precioventadesc'] = $totales[$n];
                $data['totalsindesc'] = 0;
                $data['totalcondesc'] = $totales[$n];
                $data['observaciones'] = $pre->detalle;
                $data['iva'] = 0;
                $this->db->insert('ventadetalle',$data);     
                //Liquidar
                $producto = $this->db->get_where('productos',array('codigo'=>$v))->row()->id;
                $cantidadpagada = 0;
                $totalpagada = 0;
                get_instance()->db->select('SUM(ventadetalle.cantidad) as cantidad, SUM(ventadetalle.precioventa) as precioventa');
                get_instance()->db->join('ventas','ventas.id = ventadetalle.venta');
                $yapagados = get_instance()->db->get_where('ventadetalle',array('pedidos_id'=>$post["pedidos_id"],'producto'=>$v));
                if($yapagados->num_rows()>0){
                    $cantidadpagada = $yapagados->row()->cantidad;
                    $totalpagada = $yapagados->row()->precioventa*$cantidadpagada;
                }
            
                get_instance()->db->select("pedidos_detalles.*");
                $pedido = get_instance()->db->get_where("pedidos_detalles",array('id'=>$pre->id));
                if($pedido->num_rows()>0){                                        
                    if($cantidadpagada >= $pedido->row()->cantidad && $totalpagada >= $pedido->row()->total){
                        get_instance()->db->update("pedidos_detalles",array("liquidar"=>1),array('id'=>$pre->id));
                    }
                }
            }

            $tipoFacturacion = $this->db->get_where('tipo_facturacion',array('id'=>$post['tipo_facturacion_id']));
            if($tipoFacturacion->num_rows()>0 && $tipoFacturacion->row()->emite_factura==1){
                $caja = $this->db->get_where('cajadiaria',array('id'=>$_SESSION['cajadiaria']));
                $correlativo = $caja->row()->correlativo+1;
                $this->db->update('cajadiaria',array('correlativo'=>$correlativo),array('caja'=>$_SESSION['caja'],'abierto'=>1));
            }
            //Actualizar habitación
            $pedido = get_instance()->db->get_where('pedidos',array('id'=>$post['pedidos_id']));
            get_instance()->db->where("pedidos_id",$pedido->row()->id);
            get_instance()->db->where("(liquidar IS NULL OR liquidar = 0)",'',FALSE);
            if(
                $pedido->num_rows()>0 &&                     
                get_instance()->db->get_where("pedidos_detalles")->num_rows()==0
            ){                
                get_instance()->db->update("pedidos",array("facturado"=>1),array("id"=>$pedido->row()->id));
                if(get_instance()->ajustes->disponible_automatico==1){
                    $_POST['estado'] = 0;
                    get_instance()->actualizarEstado($pedido->row()->id,0);
                }
            }
        });
        $crud->callback_after_delete(function($primary){
            get_instance()->db->delete("ventadetalle",array("venta"=>$primary));
        });
        $crud->set_url("pedidos/admin/ventas/".$x."/");
        if($return=="" && $x!='insert_validation' && $x!='insert'){
            return $crud;
        }else{
            $this->loadView($crud->render());
        }
    }
    
    function validate_pedido(){
        if(!empty($_POST['codigo']) && !empty($_POST["cantidad"]) && !empty($_POST["total"])){
            $codigos = count($_POST['codigo']);
            $cantidades = count($_POST["cantidad"]);
            $totales = count($_POST['total']);
            if($codigos == $cantidades && $codigos==$totales){
                $total_venta = str_replace($_POST["total_venta"],'.','');
                $efectivo = str_replace($_POST['total_efectivo']-$_POST['vuelto'],'.','');
                $credito = str_replace($_POST['total_credito'],'.','');
                $debito = str_replace($_POST['total_debito'],'.','');
                $cheque = str_replace($_POST['total_cheque'],'.','');
                $total_pago = $efectivo+$credito+$cheque+$debito;
                if($total_pago!=$total_venta){
                    $this->form_validation->set_message("validate_pedido","Ha ocurrido un error al cargar el pago, verifique que el monto expedido en Pendiente por Cancelar sea igual a 0");
                    return false;
                }
            }else{
                $this->form_validation->set_message("validate_pedido","Los datos han llegado incompletos");
                return false;
            }
        }else{
            $this->form_validation->set_message("validate_pedido","Los datos ingresados estan incompletos");
            return false;
        }
    }

    function set_estado($pedido){
        switch($pedido->tipo_pedidos_id){
            case '1':
                $pedido->estado = @$this->db->get_where('habitaciones',['id'=>$pedido->habitaciones_id])->row()->estado;
            break;
            case '2':
                $pedido->estado = @$this->db->get_where('parrillas',['id'=>$pedido->parrillas_id])->row()->estado;
            break;
            case '3':
                $pedido->estado = @$this->db->get_where('campings',['id'=>$pedido->campings_id])->row()->estado;
            break;
            case '4':
                $pedido->estado = @$this->db->get_where('mesas',['id'=>$pedido->mesas_id])->row()->estado;
            break;
            case '5':
                $pedido->estado = @$this->db->get_where('deliverys',['id'=>$pedido->deliverys_id])->row()->estado;
            break;
            case '6':
                $pedido->estado = @$this->db->get_where('barras',['id'=>$pedido->barras_id])->row()->estado;
            break;
        }
        return $pedido;
    }
    
    function pedidos_detalles($x = '') {   
        
        if(is_numeric($x)){ 
            if(!empty($_POST) && !empty($_POST['order_by']) && $_POST['order_by'][0]=='sel'){
                unset($_POST['order_by']);
            }
            if(!empty($_POST['page'])){
                $_POST['page'] = 1;
            }
            $pedido = $this->db->get_where('pedidos',array('id'=>$x));            
            $crud = $this->crud_function('', '');    
            $crud->set_subject("Detalle");
            $crud->where('pedidos_id',$x);
            $crud->field_type('pedidos_id','hidden',$x);            
            $crud->field_type('liquidar','hidden',0);
            $crud->field_type('precio_costo','hidden',0);
            $crud->unset_columns('pedidos_id');
            $crud->callback_after_insert(function($post){get_instance()->actualizar_stock($post);});
            $crud->unset_jquery();
            $crud->unsetSelectAll = true;
            $this->db->select("productos.id, productos.nombre_comercial, productos.precio_costo, productos.precio_venta");
            $this->db->join("productosucursal","productos.id = productosucursal.productos_id","left");
            $this->db->order_by("productos.nombre_comercial");
            $this->db->where("productosucursal.stock >",0);
            $this->db->limit(50);
            $this->db->or_where("productos.no_inventariable",1);
            
            $productos = $this->db->get('productos');
            $str = '<select name="pro" class="pro chosen-select">';
            $str.= '<option value="">Escriba el nombre del producto para buscarlo</option>';
            foreach($productos->result() as $p){
                $str.= "<option value='".$p->id."' data-precio_costo='".$p->precio_costo."' data-precio_venta='".$p->precio_venta."'>".$p->nombre_comercial."</option>";
            }
            $str.= '</select>'.$this->load->view('predesign/chosen',array(),TRUE);
            $this->fieldProducto = $str;
                
            /*$crud->callback_before_update(function($post,$primary){
                $data = get_instance()->db->get_where("pedidos_detalles",array("id"=>$primary))->row();
                get_instance()->reversar_stock($data);
                get_instance()->actualizar_stock($post);
            });
            $crud->callback_before_delete(function($primary){
                $data = get_instance()->db->get_where("pedidos_detalles",array("id"=>$primary))->row();
                get_instance()->reversar_stock($data);                
            });*/
            $crud->set_relation("productos_id","productos","{id}|{codigo}|{nombre_comercial}");            
            $crud->display_as("j4b04c546.nombre_comercial","Producto")
                     ->display_as("liquidar","Liquidado");
            if($pedido->num_rows()>0 && $pedido->row()->facturado==0){
                $crud->columns("sel","j4b04c546.nombre_comercial","detalle","precio_venta","cantidad","pendientes","total",'liquidar');    
            }
            $crud->unset_searchs("sel");
            $crud->display_as('sel','<input type="checkbox" id="todos">');
            $crud->callback_column('sel',function($val,$row){
                $producto = explode('|',$row->s4b04c546)[1];
                get_instance()->db->select('SUM(ventadetalle.cantidad) as cantidad, SUM(ventadetalle.precioventa) as precioventa');
                get_instance()->db->join('ventas','ventas.id = ventadetalle.venta');
                $yapagados = get_instance()->db->get_where('ventadetalle',array('pedidos_id'=>$row->pedidos_id,'producto'=>$producto));
                $cantidadpagada = 0;
                if($yapagados->num_rows()>0){
                    $cantidadpagada = $yapagados->row()->cantidad;
                    $totalpagado = $yapagados->row()->precioventa;
                }
                $cantidad = $row->liquidar?0:$row->cantidad;
                $cantidad-= $cantidad>0?$cantidadpagada:0;
                //$cantidad = $row->cantidad;
                $precio = $row->precio_venta;
                $total = $cantidad*$precio;
                $input =  "<input title='Marcar para facturar' type='checkbox' value='".$row->id."' name='codigo[]' data-nombre='".$row->s4b04c546."' data-precio='".$precio."' data-cantidad='".$cantidad."' data-total='".$total."' id='a".$row->id."' class='chk'>";
                return $row->liquidar?'':$input;
            });
            $crud->callback_column('pendientes',function($val,$row){
                $producto = explode('|',$row->s4b04c546)[1];
                get_instance()->db->select('SUM(ventadetalle.cantidad) as cantidad, SUM(ventadetalle.precioventa) as precioventa');
                get_instance()->db->join('ventas','ventas.id = ventadetalle.venta');
                $yapagados = get_instance()->db->get_where('ventadetalle',array('pedidos_id'=>$row->pedidos_id,'producto'=>$producto));
                $cantidadpagada = 0;
                if($yapagados->num_rows()>0){
                    $cantidadpagada = $yapagados->row()->cantidad;
                    $totalpagado = $yapagados->row()->precioventa;
                }
               
                return (string)$row->cantidad-$cantidadpagada;
            });
            $crud->callback_column('j4b04c546.nombre_comercial',function($val,$row){
                return explode('|',$row->s4b04c546)[2];
            });
            $crud->callback_column('total',function($val,$row){
                return '<span class="total">'.$val.'</span>';
            });
            $crud->unset_edit()->unset_add();
            
            $crud->callback_column('liquidar',function($val,$row){
                return $val?'SI':'<span class="label label-danger">NO</span>';
            });          
            if($crud->getParameters()=='add'){
                $crud->set_rules("pedidos_id","Pedido","required|callback_validate_pedido_detalle");            
            }
            $crud->unset_read()
                     ->unset_print()
                     ->unset_export();
            $accion = $crud->getParameters();
            
            $crud = $crud->render('','application/modules/pedidos/views/');
            if($accion!='list'){
                $pedido = $this->set_estado($pedido->row());
                $crud->output = $this->load->view('pedidos_detalles',array('pedido'=>$pedido,'output'=>$crud->output),TRUE);
            }else{                
                $unset_delete_factura = $pedido->row()->facturado?TRUE:FALSE;                
                $ventas = $this->ventas($x,$unset_delete_factura);
                $unset_delete_factura = $pedido->row()->facturado?1:0;
                $ventas->set_url('pedidos/admin/ventas/'.$x.'/1/1/');
                $ventas = $ventas->render();
                $productos = $this->inventario($x);
                $pedido = $this->set_estado($pedido->row());
                $pedido->abonado = 0;
                if(!empty($pedido->reservas_id)){
                    $reserva = $this->db->get_where('reservas',['id'=>$pedido->reservas_id]);
                    if($reserva->num_rows()>0){
                        $pedido->abonado = $reserva->row()->monto;
                    }
                }
                $crud->js_files = array_merge($crud->js_files,$productos->js_files);
                $crud->output = $this->load->view('facturar',array('productos'=>$productos,'pedidos'=>$pedido,"pedido"=>$x,"facturas"=>$ventas,'output'=>$crud->output),TRUE);
            }
            $crud->title = 'Detalle del Pedido';            
            $this->loadView($crud);
        }else{
            redirect('panel');
        }
    }   
    
    function inventario($pedido,$return = '',$accion = ''){
        if(!empty($_POST['cantidad']) && $accion=='ajax_list'){
            if(!is_array($_POST['cantidad'])){
                $_POST['cantidad'] = [$_POST['cantidad']];
            }
            foreach($_POST['cantidad'] as $n=>$v){
                if(!empty($v)){
                    if(empty($_POST['codigo'])){
                        $producto = $this->db->get_where('productos',array('id'=>$n))->row();
                    }else{
                        $producto = $this->db->get_where('productos',array('codigo'=>$_POST['codigo']));
                        if($producto->num_rows()==0){
                            echo json_encode(['success'=>false,'msj'=>'Código de producto no encontrado']);
                            die();
                        }
                        $producto = $producto->row();
                        $_POST['detalle'][$n] = '';
                        $_POST['precio_venta'][$n] = $producto->precio_venta;
                    }
                    $_POST['detalle'][$n] = isset($_POST['detalle'][$n]) && is_array($_POST['detalle'][$n])?implode(', ',$_POST['detalle'][$n]):'';
                    $pedidod = $this->db->get_where('pedidos_detalles',array('detalle'=>$_POST['detalle'][$n],'pedidos_id'=>$pedido,'productos_id'=>$producto->id));
                    if($v == '*1'){
                        if($pedidod->num_rows()>0){
                            $v = $pedidod->row()->cantidad+1;
                        }else{
                            $v = 1;
                        }
                    }elseif($pedidod->num_rows()>0){
                        $v = $pedidod->row()->cantidad+$v;
                    }                    
                    $data = array(
                        'pedidos_id'=>$pedido,
                        'productos_id'=>$producto->id,
                        'detalle'=>$_POST['detalle'][$n],
                        'cantidad'=>$v,
                        'precio_venta'=>$_POST['precio_venta'][$n],
                        'total'=>$_POST['precio_venta'][$n]*$v,
                        'liquidar'=>0,
                        'precio_costo'=>$producto->precio_costo
                    );                    
                    if($pedidod->num_rows()==0){
                        $this->db->insert('pedidos_detalles',$data);                        
                    }else{
                        $this->db->update('pedidos_detalles',$data,array('id'=>$pedidod->row()->id));                        
                    }   

                    if(!empty($_POST['codigo'])){
                        echo json_encode(['success'=>true,'msj'=>'Código de producto no encontrado']);
                        die();
                    }                 
                }
            }
        }
        $productos = new ajax_grocery_CRUD();
        $productos->set_table('productos');
        $productos->set_theme('productos');
        $productos->set_url('pedidos/admin/inventario/'.$pedido.'/1/');
        $productos->unset_add()->unset_edit()->unset_delete()->unset_export()->unset_print()->unset_read();                
        $productos->display_as('nombre_comercial','Producto')
                          ->display_as('precio_venta','Precio');
        $productos->columns('Acciones','codigo','nombre_comercial','cantidad','detalle','precio_venta','Acciones');  
        $productos->search_types = [
            'codigo'=>'
            <div class="input-group">                                  
                <input type="text" name="search_text[]" class="form-control">
                <div class="input-group-prepend" style="cursor:pointer" onclick="jQuery(this).parents(\'form\').submit();">
                  <div class="input-group-text"><i class="fa fa-chevron-right"></i></div>
                </div>
            </div>                
            ',
            'nombre_comercial'=>'
            <div class="input-group">                                  
                <input type="text" name="search_text[]" class="form-control">
                <div class="input-group-prepend d-xl-none" style="cursor:pointer" onclick="jQuery(this).parents(\'form\').submit();">
                  <div class="input-group-text"><i class="fa fa-chevron-right"></i></div>
                </div>
            </div>                
            '
          ];  
        $productos->callback_column('detalle',function($val,$row){
            $id = $row->id;
            return form_dropdown('detalle['.$id.'][]',[],'','class="observ form-control chosen-multiple-select" multiple="multiple" data-placeholder="Selecciona una opción"');
            //return '<input type="text" name="detalle['.$id.']" class="detalleProducto form-control" placeholder="'.$row->nombre_comercial.'" size="5" value="">';
        });
        $productos->callback_column('precio_venta',function($val,$row){
            $id = $row->id;
            return get_instance()->db->get_where('ajustes')->row()->permitir_modificar_precio_en_ventas==1?'<input type="text" name="precio_venta['.$id.']" class="precioVentaProducto form-control" placeholder="'.$val.'" size="5" value="'.$val.'">':$val.'<input type="hidden" name="precio_venta['.$id.']" value="'.$val.'"/>';
        });
        $productos->callback_column('cantidad',function($val,$row){
            $id = $row->id;
            return '<input type="text" name="cantidad['.$id.']" class="cantidadProducto form-control" placeholder="0.00" size="5">';
        });
        $productos->callback_column('stock',function($val,$row){
            $this->db->select('productosucursal.*');
            $this->db->join('productos','productos.codigo = productosucursal.producto');
            return (string)@$this->db->get_where('productosucursal',array('producto'=>$row->codigo,'sucursal'=>$this->user->sucursal))->row()->stock;
        });
        $productos->callback_column('Acciones',function($val,$row){
            return '<button class="btn btn-success refreshlist" type="button"><i class="fa fa-arrow-right"></i></button>';
        });        
        $productos->order_by('nombre_comercial','ASC');
        $productos->where("productos.codigo IN (SELECT producto from productosucursal WHERE sucursal='".$this->user->sucursal."')",'ESCAPE',TRUE);
        
        $list = empty($accion)?1:'';
        $productos->unsetSelectAll = true;
        $productos = $productos->render($list,'application/modules/pedidos/views/');
        if($return!=''){
            $this->loadView($productos);
        }
        return $productos;
    }
        
    function validate_pedido_detalle(){
        $post = $_POST;
        
        $pedido = $this->db->get_where('pedidos_detalles',array('pedidos_id'=>$post['pedidos_id'],'productos_id'=>$post['productos_id']));
        if($pedido->num_rows()>0){
            $data = array();
            $data['cantidad'] = $pedido->row()->cantidad+$post['cantidad'];
            $data['total'] = $data['cantidad']*$pedido->row()->precio_venta;
            $this->db->update('pedidos_detalles',$data,array('id'=>$pedido->row()->id));
            $this->actualizar_stock($post);
            $this->form_validation->set_message("validate_pedido_detalle","Se ha añadido la cantidad asignada a otro item ya existente <script>$(\"input,textarea,select\").val('');</script>");
            return false;
        }
        return true;
    }
    
    function actualizar_stock($post){
        //Actualizar stock
        $this->db->select("productosucursal.*");
        $this->db->join("productos","productos.codigo = productosucursal.producto");
        $where = array('productos.id'=>$post['productos_id'],'productosucursal.sucursal'=>$_SESSION['sucursal'],'productosucursal.stock >'=>0,'productos.no_inventariable'=>0);        
        $stock = $this->db->get_where('productosucursal',$where);
        $st = $post['cantidad'];        
        foreach($stock->result() as $s){  
            if($st<0)$st = $st*-1;
            $st = $s->stock-$st;
            $st2 = $st<0?0:$st;
            $this->db->update('productosucursal',array('stock'=>$st2),array('id'=>$s->id));                            
        }
    }

    function actualizarEstado($post,$echo = 1){
        $pedido = $this->db->get_where('pedidos',['id'=>$post]);
        if($pedido->num_rows()>0){
            $pedido = $pedido->row();
            if($pedido->facturado==0 && $this->db->get_where('pedidos_detalles',['pedidos_id'=>$pedido->id,'liquidar'=>0])->num_rows()>0){
                echo $this->error('El pedido aún no ha sido facturado, para poder realizar su cierre la totalidad del servicio debe estar facturado');
            }else{
                switch($pedido->tipo_pedidos_id){
                    case '1':
                        $this->db->update('habitaciones',['estado'=>$_POST['estado']],['id'=>$pedido->habitaciones_id]);
                    break;
                    case '2':
                        $this->db->update('parrillas',['estado'=>$_POST['estado']],['id'=>$pedido->parrillas_id]);
                    break;
                    case '3':
                        $this->db->update('campings',['estado'=>$_POST['estado']],['id'=>$pedido->campings_id]);
                    break;
                    case '4':
                        $this->db->update('mesas',['estado'=>$_POST['estado']],['id'=>$pedido->mesas_id]);
                    break;
                    case '5':
                        $this->db->update('deliverys',['estado'=>$_POST['estado']],['id'=>$pedido->deliverys_id]);
                    break;
                    case '6':
                        $this->db->update('barras',['estado'=>$_POST['estado']],['id'=>$pedido->barras_id]);
                    break;
                }
                if($echo){
                    echo $this->success('Lugar actualizado con éxito');
                }
            }
        }else{
            if($echo){
                echo $this->error('Pedido no encontrado');
            }
        }
    }
    
    function reversar_stock($post){
        $post = (array)$post;
        //Actualizar stock
        $this->db->select("productosucursal.*");
        $this->db->join("productos","productos.codigo = productosucursal.producto");
        $where = array('productos.id'=>$post['productos_id'],'productosucursal.sucursal'=>$_SESSION['sucursal']);
        $stock = $this->db->get_where('productosucursal',$where);
        $st = $post['cantidad'];
        if($stock->num_rows()>0){
            $st2 = $stock->row()->stock+$st;
            $this->db->update('productosucursal',array('stock'=>$st2),array('id'=>$stock->row()->id));
        }
    }
    
    function imprimirCuenta($id){
        $this->db->select('pedidos.*,pedidos.fecha_pedido as fecha, sucursales.denominacion, sucursales.telefono, sucursales.direccion');        
        $this->db->join('sucursales', 'sucursales.id = pedidos.sucursales_id');
        $venta = $this->db->get_where('pedidos', array('pedidos.id' => $id));
        if($venta->num_rows()>0){
            $this->db->select('productos.codigo as producto, pedidos_detalles.cantidad, FORMAT(pedidos_detalles.precio_venta,0,"de_DE") as precioventa, FORMAT(pedidos_detalles.total,0,"de_DE") as totalcondesc, pedidos_detalles.total');            
            $this->db->join('productos','productos.id = pedidos_detalles.productos_id');
            $detalles = $this->db->get_where('pedidos_detalles',array('pedidos_id'=>$id));
            $this->load->view('reportes/imprimirCuenta', array('venta' => $venta->row(), 'detalles' => $detalles));
        }
    }

}

?>
