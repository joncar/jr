<?php

require_once APPPATH . '/controllers/Panel.php';

class Reservas extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }            
    }

    function reservas($x = '') {
        if($x == 'anular' && is_numeric($_POST['id'])){
            $this->db->update('reservas',['anulado'=>1],['id'=>$_POST['id']]);
            return;
        }      
        if($x == 'actualizarFechas' && !empty($_POST['id']) && !empty($_POST['desde']) && !empty($_POST['hasta'])){
            $this->db->where("reservas.id!='".$_POST['id']."' AND habitaciones_id = '".$_POST['habitacion']."' AND (DATE(fecha_desde) BETWEEN DATE('".$_POST['desde']."') AND DATE('".$_POST['hasta']."') OR DATE(fecha_hasta) BETWEEN DATE('".$_POST['desde']."') AND DATE('".$_POST['hasta']."'))",NULL,TRUE);
            if($this->db->get_where('reservas')->num_rows()==0){
                $this->db->update('reservas',['fecha_desde'=>$_POST['desde'],'fecha_hasta'=>$_POST['hasta'],'habitaciones_id'=>$_POST['habitacion']],['id'=>$_POST['id']]);
                echo 'true';
            }else{
                echo 'false';
            }            
            return;
        }
        $crud = $this->crud_function('', '');   
        if(!empty($_POST['id'])){
            $crud->where('id',$_POST['id']);
        }      
        $crud->callback_before_insert(function($post){
            $post['embarcacion'] = empty($post['embarcacion'])?0:1;
            $post['carnadas'] = empty($post['carnadas'])?0:1;
            return $post;
        });   
        $crud->callback_before_update(function($post){
            $post['embarcacion'] = empty($post['embarcacion'])?0:1;
            $post['carnadas'] = empty($post['carnadas'])?0:1;
            return $post;
        });   
        $output = $crud->render();
        if($crud->getParameters()=='list'){
        	$output->output = $this->load->view('reservas/list',array('output'=>$output->output),TRUE);        
    	}
        $this->loadView($output);
    }

}

?>
