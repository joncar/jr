<?php

require_once APPPATH.'/controllers/Panel.php';    

class Maestras extends Panel {

    function __construct() {
        parent::__construct();             
    }

    function mesas(){
        $crud = $this->crud_function('','');
        $crud->set_subject('mesa');
        /*$crud->unset_fields('estado');*/
        $crud->unset_delete();
        $crud = $crud->render();
        $crud->title = 'mesas';
        $this->loadView($crud);
    }

    function habitaciones(){
        $crud = $this->crud_function('','');
        $crud->set_subject('Habitación');
        /*$crud->unset_fields('estado');*/
        $crud->unset_delete();
        $crud = $crud->render();
        $crud->title = 'Habitaciones';
        $this->loadView($crud);
    }

    function orden_lugares(){
        $crud = $this->crud_function('','');          
        $crud->field_type('servicio','enum',['mesas','habitaciones','parrillas','campings','deliverys']);              
        $crud->unset_delete()
             ->unset_add();
        $crud = $crud->render();        
        $this->loadView($crud);
    }

    
    
    function tipo_pedidos(){
        $crud = $this->crud_function('','');
        $crud->set_subject('tipo de pedidos');
        $crud->unset_delete();
        $crud = $crud->render();
        $crud->title = 'Administrar de tipos de pedidos';
        $this->loadView($crud);
    }
    
    function mozos(){
        $crud = $this->crud_function('','');
        $crud->set_subject('Mozo');
        $crud->unset_delete();
        $crud = $crud->render();
        $crud->title = 'Administrar mozos';
        $this->loadView($crud);
    }
    
    function parrillas(){
        $crud = $this->crud_function('','');
        $crud->set_subject('Parrilla');
        $crud->unset_delete();
        $crud = $crud->render();
        $crud->title = 'Administrar Parrillas';
        $this->loadView($crud);
    }
    
    function campings(){
        $crud = $this->crud_function('','');
        $crud->set_subject('Campings');
        $crud->unset_delete();
        $crud = $crud->render();
        $crud->title = 'Administrar Campings';
        $this->loadView($crud);
    }
    function deliverys(){
        $crud = $this->crud_function('','');
        $crud->set_subject('Deliverys');
        $crud->unset_delete();
        $crud = $crud->render();
        $crud->title = 'Administrar Deliverys';
        $this->loadView($crud);
    }
    function textos_rapidos(){
        $crud = $this->crud_function('','');
        $crud->set_subject('Textos Rápidos');
        $crud->set_order('orden');
        $crud = $crud->render();
        $crud->title = 'Textos Rápidos';
        $this->loadView($crud);
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
