<?php
foreach ($var as $n => $v): $vardata = explode(':', $v); ?>
    <div class="form-group">
        <label for="text"><?= ucwords($vardata[0]) ?></label>
        <?php
        $vardata[0] = trim($vardata[0]);
        $default = !empty($_POST[$vardata[0]]) ? $_POST[$vardata[0]] : '';
        if (count($vardata) > 1) {
            switch ($vardata[1]) {
                case 'text':
                    echo form_input(trim($vardata[0]), $default, 'class="form-control"');
                    break;
                case 'date':
                    $default = empty($default)?date("Y-m-d"):$default;
                    echo form_input(trim($vardata[0]), $default, 'class="date-input form-control"  autocomplete="off" id="'.$n.'text"');
                    break;
                case 'query':
                    if (!empty($vardata[2])) {
                        //Reemplazar variables llegadas desde post
                        if (!empty($_POST)) {
                            foreach ($_POST as $n =>$p) {
                                if(!empty($p)){
                                    $vardata[2] = str_replace('$_' . $n, $p, $vardata[2]);
                                }
                            }
                        }
                        if (!strpos($vardata[2], '$_') || !empty($_POST) && !strpos($vardata[2], '$_')) {
                            
                            $ajax = false;
                            if(strpos($vardata[2],'__search')){
                                if(!empty($_POST) && !empty($_POST['searchParam']) && $_POST['searchField'] ==$vardata[0] && !empty($vardata[3])){
                                    $likes = explode(',',$vardata[3]);
                                    $cond = '';
                                    foreach($likes as $l){
                                        $cond.= ' OR '.$l.' like \'%'.$_POST['searchParam'].'%\'';
                                    }
                                    $cond = substr($cond,3);
                                    $cond = '('.$cond.')';
                                    $vardata[2] = str_replace('__search',$cond,$vardata[2]);
                                }else{
                                    $vardata[2] = str_replace('__search','1=1',$vardata[2]);
                                }
                                $ajax = true;
                            }
                            
                            $query = $this->db->query($vardata[2]);                            
                            $ajax_query = $query->num_rows()>10 || $ajax?'ajax_query':'';
                            $str = '<select name="' . $vardata[0] . '" class="query form-control chosen-select '.$ajax_query.'"><option value="">Seleccione una opción</option>';                            
                            foreach ($query->result() as $n=>$que) {                                
                                $data = implode('[!]', (array) $que);
                                list($id, $data) = explode('[!]', $data, 2);
                                $data = str_replace('[!]', ' ', $data);
                                if($n<100 || $default == $id){
                                    $selected = $default == $id ? 'selected' : '';
                                    $str.= '<option value="' . $id . '" ' . $selected . '>' . $data . '</option>';
                                }
                            }
                            $str.= '</select>';
                        } else {
                            $str = 'Seleccione un campo';
                        }
                        echo $str;
                    } else {
                        echo form_input(trim($vardata[0]), $default, 'class="form-control" id="text"');
                    }
                    break;
                default:
                    echo form_input(trim($vardata[0]), $default, 'class="form-control"');
                    break;
            }
        } else {
            echo form_input(trim($vardata[0]), $default, 'class="form-control"');
        }
        ?>
    </div>                  
<?php endforeach; ?>
<div class="form-group">
    <input type='radio' name='docType' value='pdf' <?= !empty($_POST['docType']) && $_POST['docType']=='pdf'?'checked=""':'' ?>> PDF
    <input type='radio' name='docType' value='excel' <?= !empty($_POST['docType']) && $_POST['docType']=='excel'?'checked=""':'' ?>> EXCEL
    <input type='radio' name='docType' value='html' <?= empty($_POST['docType']) || $_POST['docType']=='html'?'checked=""':'' ?>> HTML
    <input type='radio' name='docType' value='txt' <?= !empty($_POST['docType']) && $_POST['docType']=='txt'?'checked=""':'' ?>> TXT
</div>
<button type="submit" class="btn btn-default">Consultar</button>