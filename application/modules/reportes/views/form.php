<?php get_instance()->hcss[] = '<link rel="stylesheet" href="'.base_url('assets/grocery_crud/css/').'/jquery_plugins/bootstrap.datepicker/bootstrap.datepicker.css">'; ?>
<?php get_instance()->js[] = '<script src="'.base_url('assets/grocery_crud/js/').'/jquery_plugins/bootstrap.datepicker.js"></script>'; ?>
<?php get_instance()->js[] = '<script src="'.base_url('assets/grocery_crud/js/').'/jquery_plugins/config/jquery.datepicker.config.js"></script>'; ?>
<?php $this->load->view('predesign/chosen'); ?>
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h1 class="kt-portlet__head-title">
                <b><?= $reporte->titulo ?></b>
            </h1>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-section">
            <form method="post" id="formulario">
                <?php $this->load->view('_form'); ?>
            </form>
        </div>
    </div>
</div>
<script>
    window.afterLoad.push(function(){
        $(".date-input").datepicker({                    
            format: "yyyy-mm-dd",
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true
        });
        $(document).on("keyup",".chzn-search input",setAjaxSearch);
        window.consult = true;
        if(localStorage.repForm){
            let s = JSON.parse(localStorage.repForm);
            Object.keys(s).map(ss=>{
                let vv = s[ss];
                let inp = document.querySelector(`[name="${ss}"]`);
                if(inp && inp.value==''){
                    inp.value = vv;
                    if(inp.type=='select' && inp.classList.contains('chosen-select')){
                        $(inp).chosen().trigger('liszt:updated')
                    }
                }
            })
        }
        refresh();
    })
    var timeOut = '';
    var setAjaxSearch = function(){
        //Buscar parametro        
        var select = $(this).parents('.form-group').find('select');
        if(select.hasClass('ajax_query'))
        {            
            var valor = $(this).val();
            clearTimeout(timeOut);
            timeOut = setTimeout(function(){
                var data = new FormData(document.getElementById('formulario'));                
                data.append('searchParam',valor);
                data.append('searchField',select.attr('name'));
                $.ajax({
                url: '<?= base_url('reportes/rep/mostrarForm/'.$reporte->id.'/1') ?>',
                data: data,
                context: document.body,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST'
             }).always(function(data){
                 $(document).off('focus','input,select');
                 $(document).off("change","#formulario > div > input,#formulario > div > select");
                 $("#formulario").html(data);
                 //setTimeout(function(){refresh();},500);                 
                 $(".chosen-select").chosen({"search_contains": true, allow_single_deselect:true});
                 $(".chzn-search input").on("keyup",setAjaxSearch);
                 $(".chzn-search input").val(valor);
                 $(".chosen-select").chosen().trigger('liszt:open');
                 $(".date-input").datepicker({                    
                    format: "yyyy-mm-dd",
                    showButtonPanel: true,
                    changeMonth: true,
                    changeYear: true
                });
                //$(".ajax_query").trigger('change');
             });
            },500);                
        }
    }
    
    function refresh(){
        $(document).on('focus','input,select',function(){consult = true;});
        $(document).on("change","#formulario > div > input,#formulario > div > select",function(){
            if(consult){
                consult = false;
                var data = new FormData(document.getElementById('formulario'));
                $.ajax({
                    url: '<?= base_url('reportes/rep/mostrarForm/'.$reporte->id.'/1') ?>',
                    data: data,
                    context: document.body,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST'
                 }).always(function(data){
                     $("#formulario").html(data);
                     $(".chosen-select").chosen({"search_contains": true, allow_single_deselect:true});
                     $(".chzn-search input").on("keyup",".chzn-search input",setAjaxSearch);
                     $(".date-input").datepicker({                
                        format: "yyyy-mm-dd",
                        showButtonPanel: true,
                        changeMonth: true,
                        changeYear: true
                    });
                    let obj = {};
                    $('#formulario input[type="text"],#formulario input[type="date"],#formulario input[type="radio"]:checked,#formulario select').toArray().map(x=>{
                        obj[x.name] = x.value;
                    });
                    localStorage.setItem('repForm',JSON.stringify(obj));
                 });
            }
        });
    }
</script>