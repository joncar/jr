<?php echo $output ?>
<?php get_instance()->js[] = '
	<script src="'.base_url().'printer/zip/zip.js"></script>
	<script src="'.base_url().'printer/zip/zip-ext.js"></script>
	<script src="'.base_url().'printer/zip/deflate.js"></script>
	<script src="'.base_url().'printer/scripts/JSPrintManager.js"></script>
'; ?>
<script>
 	window.afterLoad.push(function(){
	    //WebSocket settings
	    JSPM.JSPrintManager.auto_reconnect = true;
	    JSPM.JSPrintManager.start();
	    JSPM.JSPrintManager.WS.onStatusChanged = function () {
	        if (jspmWSStatus()) {
	            //get client installed printers            
	            JSPM.JSPrintManager.getPrinters().then(function (myPrinters) {
	            	var val = $('#field-impresora').val();
	                var options = '<select id="field-impresora" name="impresora" class="form-control">';
	                if(val!=''){
	                	options += '<option value="'+val+'">' + val + '</option>';
	                }
	                for (var i = 0; i < myPrinters.length; i++) {
	                	var sel = val==myPrinters[i]?'selected="true"':'';
	                    options += '<option value="'+myPrinters[i]+'" '+sel+'>' + myPrinters[i] + '</option>';
	                }
	                options+= '</select>';
	                $('#field-impresora').replaceWith(options);
	                console.log(myPrinters);
	            });
	        }
	    };
	});

	//Check JSPM WebSocket status
    function jspmWSStatus() {
        if (JSPM.JSPrintManager.websocket_status == JSPM.WSStatus.Open){
            return true;
        }
        return false;
    }
</script>