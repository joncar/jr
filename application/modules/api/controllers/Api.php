<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Api extends Main{

    	protected $cookie = '';
    	protected $timeout = 30;
        function __construct() {
            parent::__construct();            
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');            
            if (isset($_SERVER['HTTP_ORIGIN'])) {
		        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		        header('Access-Control-Allow-Credentials: true');
		        header('Access-Control-Max-Age: 86400');    // cache for 1 day
		    }
		    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		 
		        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])){
		            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
		        }
		 
		        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])){
		            header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		        }
		 
		        exit(0);
		    }
            date_default_timezone_set('America/Asuncion');
            setlocale(LC_ALL,'Spanish');   
        }

        function printjson($object){
            header('Content-Type: application/json');
            echo json_encode($object);            
        }  

        function login(){
            $response = ['success'=>false,'msj'=>'Campos no recibidos'];
            $this->form_validation->set_rules('email','Usuario','required');
            $this->form_validation->set_rules('password','Password','required');
            if($this->form_validation->run()){
                $password = md5($_POST['password']);
                $user = $this->db->get_where('user',['email'=>$_POST['email'],'password'=>$password,'status'=>1]);
                if($user->num_rows()>0){
                    $response['msj'] = 'Token Creado correctamente';
                    $response['token'] = md5(uniqid());
                    $response['success'] = true;
                    $response['user_id'] = $user->row()->id;
                    $this->db->update('user',['api_token'=>$response['token']],['id'=>$user->row()->id]);
                }else{
                    $response['msj'] = 'Usuario o contraseña incorrecta';
                }

            }else{
                $response['msj'] = $this->form_validation->error_string();
            }

            $this->printjson($response);
        }

        function validateLogin(){
        	$this->form_validation->set_rules('token','Token','required');
        	if($this->form_validation->run()){        		
        		$user = $this->db->get_where('user',['api_token'=>$_POST['token']]);
        		if($user->num_rows()>0){
        			return true;
        		}else{
        			return false;
        		}

        	}else{
        		return false;
        	}
        }


        function ventas(){
            $response = ['success'=>false,'msj'=>'Token no recibido o ha sido revocado'];
            if($this->validateLogin()){
                $response['success'] = true;
                $response['msj'] = '';
                $response['data'] = [];
                $response['page'] = 1;
                $response['per_page'] = 10;
                $query = "
                    SELECT
                    ventas.id,
                    ventas.fecha,
                    day(ventas.fecha) as dia,
                    monthname(ventas.fecha) as mes,
                    year(ventas.fecha) as anho,
                    concat_ws(' ',clientes.nombres,clientes.apellidos) as cliente,
                    tipotransaccion.denominacion as transaccion,
                    categoriaproducto.denominacion as categoria_producto,
                    productos.codigo,
                    productos.nombre_comercial,
                    ventadetalle.totalcondesc as total_detalle
                    FROM ventas
                    INNER JOIN ventadetalle on ventadetalle.venta = ventas.id
                    INNER JOIN productos on productos.codigo = ventadetalle.producto
                    INNER JOIN categoriaproducto on categoriaproducto.id = productos.categoria_id
                    INNER JOIN tipotransaccion on tipotransaccion.id = ventas.transaccion
                    INNER JOIN clientes on clientes.id = ventas.id
                    WHERE 1=1 AND ventas.status = 0
                ";

                /*if(isset($_POST['estudiantes_id']) && is_numeric($_POST['estudiantes_id'])){
                    $query.= " AND estudiantes.id= '".$_POST['estudiantes_id']."'";
                }
                if(isset($_POST['programacion_carreras_id']) && is_numeric($_POST['programacion_carreras_id'])){
                    $query.= " AND programacion_carreras.id='".$_POST['programacion_carreras_id']."'";
                }
                if(isset($_POST['facultades_id']) && is_numeric($_POST['facultades_id'])){
                    $query.= " AND facultades.id='".$_POST['facultades_id']."'";
                }
                if(isset($_POST['carreras_id']) && is_numeric($_POST['carreras_id'])){
                    $query.= " AND carreras.id='".$_POST['carreras_id']."'";
                }  
                if(isset($_POST['user_id']) && is_numeric($_POST['user_id'])){
                    $query.= " AND user.id='".$_POST['user_id']."'";
                } */
                $response['total'] = $this->db->query($query)->num_rows();
                if(isset($_POST['per_page']) && is_numeric($_POST['per_page'])){
                    $response['per_page'] = $_POST['per_page'];
                }
                if(isset($_POST['page']) && is_numeric($_POST['page'])){
                    $query.= " LIMIT ".(($_POST['page']-1)*10).",".$response['per_page'];
                    $response['page'] = $_POST['page'];
                }else{
                    $query.= " LIMIT 0,".$response['per_page'];                    
                }
                $datos = $this->db->query($query);
                foreach($datos->result() as $d){
                    $response['data'][] = $d;
                }
            }
            $this->printjson($response);
        }
        
    }
?>
