<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class SportApi extends Main{

    	protected $cookie = '';
    	protected $timeout = 30;
        function __construct() {
            parent::__construct();            
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');            
            if (isset($_SERVER['HTTP_ORIGIN'])) {
		        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		        header('Access-Control-Allow-Credentials: true');
		        header('Access-Control-Max-Age: 86400');    // cache for 1 day
		    }
		    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		 
		        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])){
		            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
		        }
		 
		        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])){
		            header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		        }
		 
		        exit(0);
		    }
            date_default_timezone_set('America/Asuncion');
            setlocale(LC_ALL,'Spanish');   
        }

        function getCanchas(){
            $canchas = $this->db->get_where('sport_canchas')->result();
            echo json_encode($canchas);
            return;
        }

        function getReservas(){
            $reservas = [];
            
            if(!empty($_POST['cancha']) && !empty($_POST['fecha'])){
                
                $desde = date("Y-m-01",strtotime($_POST['fecha']));
                $hasta = date("Y-m-t",strtotime($_POST['fecha']));
                $this->db->where("(fecha BETWEEN '$desde' AND '$hasta')",NULL,TRUE);
                $this->db->where("sport_canchas_id",$_POST['cancha']);
                $this->db->select('clientes.*,fecha,desde_hora,hasta_hora,cant_minutos,sport_reservas_canchas.id as reservaid,sport_reservas_canchas.facturado');
                $this->db->join('clientes','clientes.id = sport_reservas_canchas.clientes_id','left');
                $reservas = [];
                $res = $this->db->get_where('sport_reservas_canchas')->result();
                foreach($res as $r){
                    $reservas[] = [
                        'id'=>$r->reservaid,
                        'startDate'=>$r->fecha.'T'.$r->desde_hora,
                        'endDate'=>$r->fecha.'T'.$r->hasta_hora,
                        'minutos'=>$r->cant_minutos,
                        'title'=>$r->nombres.' '.$r->apellidos.' '.$r->desde_hora.' '.$r->hasta_hora,
                        'facturado'=>$r->facturado
                    ];
                }
            }
            echo json_encode($reservas);
            return;
        }
        
    }
?>
