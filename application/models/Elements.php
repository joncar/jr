<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Elements extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
        $this->ajustes = $this->db->get('ajustes')->row();
    }

    function clientes($where = array(),$limit = null){        
        $data = $this->db->get_where('clientes',$where);        
        return $data;
    }

    function proveedores($where = array(),$limit = null){        
        $data = $this->db->get_where('proveedores',$where);        
        return $data;
    }

    function ventas($where = array(),$limit = null,$order = null){
        if($limit){
            $this->db->limit($limit);
        }
        if($order){
            $this->db->order_by($order[0],$order[1]);
        }
    	$data = $this->db->get_where('ventas',$where);
    	foreach($data->result() as $n=>$v){
    		$data->row($n)->detalle = $this->ventadetalle(array('venta'=>$v->id));
    		$data->row($n)->_fecha = $v->fecha;
    		$data->row($n)->fecha = date("d/m/Y",strtotime($v->fecha));

    	}
    	return $data;
    }
    
    function ventadetalle($where = array(),$limit = null,$order = NULL){
    	if($limit){
    		$this->db->limit($limit);
    	}
    	if($order){
    		$this->db->order_by($order[0],$order[1]);
    	}
    	$data = $this->db->get_where('ventadetalle',$where);
    	foreach($data->result() as $n=>$v){

    	}		
    	return $data;
    }

    function compras($where = array(),$limit = null,$returnObject = true){
        if($limit){
            $this->db->limit($limit);
        }
        /*if($order){
            $this->db->order_by($order[0],$order[1]);
        }*/
        $dato = array();
    	$data = $this->db->get_where('compras',$where);
    	foreach($data->result() as $n=>$v){
    		$data->row($n)->detalle = $this->compradetalles(array('compra'=>$v->id));
    		$data->row($n)->_fecha = $v->fecha;
    		$data->row($n)->fecha = date("d/m/Y",strtotime($v->fecha));

            $data->row($n)->productos = array();
            $this->db->select('compradetalles.*,productos.nombre_comercial');
            $this->db->join('productos','productos.codigo = compradetalles.producto');
            foreach($this->db->get_where('compradetalles',array('compra'=>$v->id))->result() as $c){
                $pro = $this->db->get_where('productos',array('codigo'=>$c->producto))->row();
                $pro->cantidad = $c->cantidad;
                $pro->por_desc = $c->por_desc;
                $pro->por_venta = $c->por_venta;
                $pro->precio_venta = $c->precio_venta;
                $pro->precio_costo = $c->precio_costo;
                $pro->total = $c->total;
                $data->row($n)->productos[] = $pro;
            }
            $dato[] = $data->row($n);
    	}
    	return $returnObject?$data:$dato;
    }

    function compradetalles($where = array(),$limit = null,$order = NULL){
    	if($limit){
    		$this->db->limit($limit);
    	}
    	if($order){
    		$this->db->order_by($order[0],$order[1]);
    	}
    	$data = $this->db->get_where('compradetalles',$where);
		foreach($data->result() as $n=>$v){

		}
    	return $data;
    }

    function pagocliente($where = array(),$limit = null,$order = null){
        if($limit){
            $this->db->limit($limit);
        }
        if($order){
            $this->db->order_by($order[0],$order[1]);
        }
        $data = $this->db->get_where('pagocliente',$where);
        foreach($data->result() as $n=>$v){            
            $data->row($n)->_fecha = $v->fecha;
            $data->row($n)->fecha = date("d/m/Y",strtotime($v->fecha));
        }
        return $data;
    }

    function productos($where = array()){        
        $data = $this->db->get_where('productos',$where);
        $nota = array();
        foreach($data->result() as $n=>$v){                        
            $nota[] = $v;
        }
        return $nota;
    }

    function notas_credito_cliente_detalle($where = array()){        
        $data = $this->db->get_where('notas_credito_cliente_detalle',$where);
        $nota = array();
        foreach($data->result() as $n=>$v){            
            $v->producto = $this->productos(array('productos.id'=>$v->producto))[0];
            $nota[] = $v;
        }
        return $nota;
    }

    function notas_credito_cliente($where = array(),$limit = null,$order = null){
        if($limit){
            $this->db->limit($limit);
        }
        if($order){
            $this->db->order_by($order[0],$order[1]);
        }
        $data = $this->db->get_where('notas_credito_cliente',$where);
        $nota = array();
        foreach($data->result() as $n=>$v){            
            $venta = $this->ventas(array('ventas.id'=>$v->venta))->row();
            $cliente = $this->clientes(array('clientes.id'=>$v->cliente))->row();
            $v->productos = $this->notas_credito_cliente_detalle(array('nota_credito'=>$v->id));                        
            foreach($v->productos as $nn=>$pp){
                $v->productos[$nn]->venta = $venta->id;
                $v->productos[$nn]->precioventadesc = $pp->total;
            }
            $v->nro_factura = (INT)$venta->nro_factura;
            $v->clienteNombre = $cliente->nombres.' '.$cliente->apellidos;            
            $nota[] = $v;
        }
        return $nota;
    }

    function notas_credito_proveedor($where = array(),$limit = null,$order = null){
        if($limit){
            $this->db->limit($limit);
        }
        if($order){
            $this->db->order_by($order[0],$order[1]);
        }
        $data = $this->db->get_where('notas_credito_proveedor',$where);
        $nota = array();
        foreach($data->result() as $n=>$v){            
            $compra = $this->compras(array('compras.id'=>$v->compra))->row();
            $proveedor = $this->proveedores(array('proveedores.id'=>$v->proveedor))->row();
            $v->productos = $this->notas_credito_proveedor_detalle(array('nota_credito'=>$v->id));                        
            foreach($v->productos as $nn=>$pp){
                $v->productos[$nn]->compra = $compra->id;
                $v->productos[$nn]->precio_costo = $pp->precio_costo;
            }
            $v->nro_factura = (INT)$compra->nro_factura;
            $v->proveedorNombre = $proveedor->denominacion;            
            $nota[] = $v;
        }
        return $nota;
    }

    function notas_credito_proveedor_detalle($where = array()){        
        $data = $this->db->get_where('notas_credito_proveedor_detalle',$where);
        $nota = array();
        foreach($data->result() as $n=>$v){            
            $v->producto = $this->productos(array('productos.id'=>$v->producto))[0];
            $nota[] = $v;
        }
        return $nota;
    }

    function pedidos_proveedores_detalles($where = array(),$limit = null,$order = NULL){
        if($limit){
            $this->db->limit($limit);
        }
        if($order){
            $this->db->order_by($order[0],$order[1]);
        }
        $data = $this->db->get_where('pedidos_proveedores_detalles',$where);
        foreach($data->result() as $n=>$v){

        }
        return $data;
    }

    function pedidos_proveedores($where = array(),$limit = null,$returnObject = true){
        if($limit){
            $this->db->limit($limit);
        }
        /*if($order){
            $this->db->order_by($order[0],$order[1]);
        }*/
        $dato = array();
        $data = $this->db->get_where('pedidos_proveedores',$where);
        foreach($data->result() as $n=>$v){
            $data->row($n)->detalle = $this->pedidos_proveedores_detalles(array('pedidos_proveedores'=>$v->id));
            $data->row($n)->_fecha = $v->fecha;
            $data->row($n)->fecha = date("d/m/Y",strtotime($v->fecha));

            $data->row($n)->productos = array();
            $this->db->select('pedidos_proveedores_detalles.*,productos.nombre_comercial');
            $this->db->join('productos','productos.codigo = pedidos_proveedores_detalles.producto');
            foreach($this->db->get_where('pedidos_proveedores_detalles',array('pedidos_proveedores'=>$v->id))->result() as $c){
                $pro = $this->db->get_where('productos',array('codigo'=>$c->producto))->row();
                $pro->cantidad = $c->cantidad;
                $pro->por_desc = $c->por_desc;
                $pro->por_venta = $c->por_venta;
                $pro->precio_venta = $c->precio_venta;
                $pro->precio_costo = $c->precio_costo;
                $pro->total = $c->total;
                $data->row($n)->productos[] = $pro;
            }
            $dato[] = $data->row($n);
        }
        return $returnObject?$data:$dato;
    }

    function get_saldo($cliente){
        return $this->db->query('SELECT get_saldo('.$cliente.') as saldo')->row()->saldo + $this->db->query('SELECT get_saldo_old('.$cliente.') as saldo')->row()->saldo;
        switch(get_instance()->ajustes->capturar_saldo_cliente){
            case 'pagosclientes':
                return $this->db->query('SELECT get_saldo('.$cliente.') as saldo')->row()->saldo;
            break;
            case 'facturas_clientes':
                $saldo = $this->db->get_where('facturas_clientes',['clientes_id'=>$cliente]);
                return $saldo->num_rows()>0?$saldo->row()->saldo_cliente:0;

            break;
        }
    }
}
?>
