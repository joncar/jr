<?php
class Productos_ventas_model  extends grocery_CRUD_Generic_Model  {

    public function __construct(){
        parent::__construct();
    }

    function get_list()
    {
        $productos = parent::get_list();

        if(count($productos)>0 && !empty($_POST['venta']) && !empty($_POST['codigo'])){
        	$temp = $this->db->get_where('ventatemp',['codigo'=>$productos[0]->codigo,'cajas_id'=>get_instance()->user->caja]);
        	if($temp->num_rows()==0){
	        	$this->db->insert('ventatemp',[
	        		'codigo'=>$productos[0]->codigo,
					'cantidad'=>1,
					'precio_venta'=>$productos[0]->precio_venta,
					'vencimiento'=>'',
					'cajas_id'=>get_instance()->user->caja,
					'user_id'=>get_instance()->user->id
	        	]);
        	}else{
        		$this->db->update('ventatemp',['cantidad'=>$temp->row()->cantidad+1],['codigo'=>$productos[0]->codigo,'cajas_id'=>get_instance()->user->caja]);
        	}
        }
        return $productos;
    }
}
