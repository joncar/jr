<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
        $this->ajustes = $this->db->get('ajustes')->row();
    }
    
    function fecha($val)
    {
        return date($this->ajustes->formato_fecha,strtotime($val));
    }
    
    function moneda($val)
    {
        return $val.' '.$ajustes->moneda;
    }
    
    //Valida si tiene acceso a algun controaldor
    function has_access($controller,$function = '')
    {
        if(!is_numeric($controller))
            $con = $this->db->get_where('controladores',array('nombre'=>$controller));
        
        if(!empty($function) && !is_numeric($function) && (!empty($con) && $con->num_rows()>0 || is_numeric($controller)))
            $fun = $this->db->get_where('funciones',array('nombre'=>$function,'controlador'=>$con->row()->id));
        
        $controller = !empty($con) && $con->num_rows()>0?$con->row()->id:$controller;
        $function = !empty($fun) && $fun->num_rows()>0?$fun->row()->id:$function;        
        
        if($_SESSION['cuenta']!=1)
        {            
            if(!empty($function))
            return $this->db->get_where('roles',array('controlador'=>$controller,'funcion'=>$function,'user'=>$_SESSION['user']))->num_rows()>0?TRUE:FALSE;
            else
            return $this->db->get_where('roles',array('controlador'=>$controller,'user'=>$_SESSION['user']))->num_rows()>0?TRUE:FALSE;
        }
        else
            return TRUE;
    }
    
    function get_menu($controlador,$returnforeach = FALSE)
    {
        $str = '';
        $this->db->select('controladores.id as controlador,funciones.etiqueta as label, funciones.id as funcion_id, controladores.nombre, funciones.nombre as funcion');
        $this->db->join('funciones','funciones.controlador = controladores.id');
        $menu = $this->db->get_where('controladores',array('controladores.nombre'=>$controlador,'funciones.visible'=>1));
        if($menu->num_rows()>0 && $this->has_access($menu->row()->controlador)){
        $str .= '';
        foreach($menu->result() as $m){
        if($this->has_access($m->controlador,$m->funcion_id)){
        $label = empty($m->label)?ucwords($m->funcion):$m->label;
        $str.= '<li><a href="'.base_url($controlador.'/'.$m->funcion).'">'.$label.'</a></li>';
        }
        }
        $str.= '';
        }
        if(!$returnforeach)
        return $str;
        else return $menu->result();
    }
    //Funcion para saber cuantos productos hay disponibles para distribuir entre las sucursales de la compra realizada
    function get_disponible($compra){
        
            $this->db->select('SUM(distribucion.cantidad) as p');
            $productosucursal = $this->db->get_where('distribucion',array('compra'=>$compra));
            
            $this->db->select('SUM(compradetalles.cantidad) as p');
            $compradetalles = $this->db->get_where('compradetalles',array('compra'=>$compra));
            
            if($compradetalles->num_rows()>0 && $productosucursal->num_rows()>0)
            return $compradetalles->row()->p-$productosucursal->row()->p;
            
    }
    
    //Traer el nuevo nro de factura para realizar una venta en la caja seleccionada.
    function get_nro_factura(){
        $sucursal = $_SESSION['sucursal'];
        if(empty($_SESSION['caja'])){
            $caja = $this->db->get_where('cajas',array('sucursal'=>$sucursal));            
        }
        else
            $caja = $this->db->get_where('cajas',array('id'=>$_SESSION['caja']));
        if($caja->num_rows()>0){
            $caja = $caja->row();
            if(empty($_SESSION['caja']))$_SESSION['caja'] = $caja->id;
            $serie = $caja->serie;
            
            $ultima_venta = $this->db->get_where('cajadiaria',array('caja'=>$caja->id,'abierto'=>1));
            if($ultima_venta->num_rows()==0){
                echo '<script>document.location.href="'.base_url('panel/selcajadiaria').'";</script>';
                die();
            }
            $ultima_venta = $serie.($ultima_venta->row()->correlativo+1);
            return $ultima_venta;
        }
        else
            header("Location:".base_url('panel/selsucursal').'?redirect='.$this->router->fetch_class().'/ventas');
    }

    function getNroNota(){
        $sucursal = $_SESSION['sucursal'];
        if(empty($_SESSION['caja'])){
            $caja = $this->db->get_where('cajas',array('sucursal'=>$sucursal));            
        }
        else
            $caja = $this->db->get_where('cajas',array('id'=>$_SESSION['caja']));
        if($caja->num_rows()>0){
            $caja = $caja->row();
            if(empty($_SESSION['caja']))$_SESSION['caja'] = $caja->id;
            $serie = $caja->serie;
            
            $ultima_venta = $this->db->get_where('cajadiaria',array('caja'=>$caja->id,'abierto'=>1));
            if($ultima_venta->num_rows()==0){
                echo '<script>document.location.href="'.base_url('panel/selcajadiaria').'";</script>';
                die();
            }
            $ultima_venta = $serie.($ultima_venta->row()->nota_cred_correlativo+1);
            return $ultima_venta;
        }
        else
            header("Location:".base_url('panel/selsucursal').'?redirect='.$this->router->fetch_class().'/ventas');
    }

    

    function getVenta($y){
        $this->db->select('ventas.*, observacion as observaciones');
        $ventas = $this->db->get_where('ventas',array('id'=>$y));
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->productos = array();
            $this->db->select('ventadetalle.*,productos.nombre_comercial,productos.id as producto_id');
            $this->db->join('productos','productos.codigo = ventadetalle.producto');
            foreach($this->db->get_where('ventadetalle',array('venta'=>$ventas->id))->result() as $v){
                $ventas->productos[] = array(
                    'id'=>$v->producto_id,
                    'codigo'=>$v->producto,
                    'nombre'=>$v->nombre_comercial,
                    'cantidad'=>$v->cantidad,
                    'precio_venta'=>$v->precioventa,
                    'por_desc'=>$v->pordesc,
                    'precio_descuento'=>0,
                    'total'=>$v->totalcondesc
                );
            }
            return $ventas;
        }else{
            return null;
        }

    }

    function getServicioDetalle($y){

        $ventas = $this->db->get_where('servicios',array('id'=>$y));
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->productos = array();
            $ventas->fecha_a_entregar = date("Y-m-d",strtotime($ventas->fecha_a_entregar));
            $this->db->select('servicios_detalles.*,productos.codigo,productos.nombre_comercial');
            $this->db->join('productos','productos.id = servicios_detalles.productos_id');
            foreach($this->db->get_where('servicios_detalles',array('servicios_id'=>$ventas->id))->result() as $v){
                $d = array(
                    'id'=>$v->productos_id,
                    'codigo'=>$v->codigo,
                    'nombre'=>$v->nombre_comercial,
                    'categoria_precios_id'=>$v->categoria_precios_id,
                    'cantidad'=>$v->cantidad,
                    'precio_venta'=>$v->precio,                                
                    'total'=>$v->total,
                );

                $rangos = $this->db->get_where('categoria_precios',array('productos_id'=>$v->productos_id));
                $rang = array();
                foreach($rangos->result() as $r){
                    $rang[] = $r;
                }                
                $d['rangos'] = $rang;
                $ventas->productos[] = $d;
            }
            return $ventas;
        }else{
            return null;
        }

    }

    function getOrdenTrabajo($y,$validarFacturacion = TRUE){
        $where['id'] = $y;
        $ventas = $this->db->get_where('orden_trabajo',$where);
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->tipo = 'Orden Trabajo';
            $ventas->productos = array();            
            $ventas->pago_pesos = 0;
            $ventas->pago_dolares = 0;
            $ventas->pago_reales = 0;
            $ventas->pago_guaranies = 0;            
            $ventas->total_efectivo = 0;
            $ventas->total_debito = 0;
            $ventas->total_cheque = 0;
            $ventas->total_credito = 0;
            $ventas->orden_trabajo_id = $ventas->id;
            $ventas->transaccion = 1;
            $ventas->tipo_facturacion_id = 1;
            $ventas->total_pagado = 0;
            $ventas->forma_pago = 1;
            /*$ventas->sucursal = $this->user->sucursal;
            $ventas->caja = $this->user->caja;
            $ventas->cajadiaria = $this->user->cajadiaria;*/
            $ventas->success = array($this,'successAddOrdenTrabajo');
            $ventas->cheques = array();            
            $this->db->select('
                orden_trabajo_detalle.*,
                productos.nombre_comercial,
                productos.codigo as producto,
                productos.precio_credito,
                productos.precio_venta,
                productos.precio_venta_mayorista1,
                productos.precio_venta_mayorista2,
                productos.precio_venta_mayorista3,
                productos.porc_venta AS porc,
                productos.cant_1,
                productos.cant_2,
                productos.cant_3
            ');
            $this->db->join('productos','productos.id = orden_trabajo_detalle.productos_id');
            foreach($this->db->get_where('orden_trabajo_detalle',array('orden_trabajo_id'=>$ventas->id,'orden_trabajo_detalle.anulado'=>0))->result() as $v){
                if($this->ajustes->controlar_vencimiento_stock==0){
                    $stock = @$this->db->get_where('productosucursal',array('producto'=>$v->producto,'sucursal'=>$this->user->sucursal))->row()->stock;
                    $venc = '00/0000';
                    $vencimiento = '';
                }else{
                    $this->db->order_by('vencimiento','ASC');
                    $venc = $this->db->get_where('productosucursal',array('producto'=>$v->producto,'sucursal'=>$this->user->sucursal,'stock >'=>0));
                    $v1 = [];
                    $v2 = [];
                    foreach($venc->result() as $vv){
                        $v1[] = $vv->stock;
                        $v2[] = $vv->vencimiento;
                    }
                    $stock = count($v1)>0?$v1[0]:'';                    
                    $vencimiento = count($v2)>0?$v2[0]:'';
                    $stockOpts = $v1;
                    $vencimientoOpts = $v2;
                }
                $stock = empty($stock)?0:$stock;
                /*if(get_instance()->router->fetch_class()=='ventas' && $v->total_adic>0){//Sacar nuevo precio de ventas con adicional
                    $v->total_adic = ($v->precio_venta*$v->cantidad)+((($v->precio_venta*$v->cantidad)*$v->porc)/100);
                    $v->precio_venta = $v->total_adic/$v->cantidad;
                }*/
                $pr = array(
                    'codigo'=>$v->producto,
                    'nombre'=>$v->nombre_comercial,
                    'cantidad'=>$v->cantidad,
                    'precio_venta'=>$v->precio_venta,
                    'precio_original'=>$v->precio_venta,
                    'precio_credito'=>empty($v->precio_credito)?0:$v->precio_credito,
                    'precio_venta_contado'=>$v->precio_venta,
                    'precio_venta_mayorista1'=>$v->precio_venta_mayorista1,
                    'precio_venta_mayorista2'=>$v->precio_venta_mayorista2,
                    'precio_venta_mayorista3'=>$v->precio_venta_mayorista3,
                    'cant_1'=>$v->cant_1,
                    'cant_2'=>$v->cant_2,
                    'cant_3'=>$v->cant_3,
                    'por_desc'=>$v->precio_venta,
                    'por_venta'=>(empty($v->porc)?0:$v->porc),
                    'precio_descuento'=>0,
                    'total'=>$v->total,
                    'stock'=>$stock,
                    'vencimiento'=>$vencimiento,
                );
                if($this->ajustes->controlar_vencimiento_stock==1){
                    $pr['stockOpts'] = $stockOpts;
                    $pr['vencimientoOpts'] = $vencimientoOpts;
                }
                $ventas->plan = 0;
                $ventas->productos[] = $pr;
                
            }
            return $ventas;
        }
    }

    function getPresupuesto($y,$validarFacturacion = TRUE){
        $where['id'] = $y;
        $ventas = $this->db->get_where('presupuesto',$where);
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->tipo = 'Presupuesto';
            $ventas->productos = array();            
            $ventas->pago_pesos = 0;
            $ventas->pago_dolares = 0;
            $ventas->pago_reales = 0;
            $ventas->pago_guaranies = 0;            
            $ventas->total_efectivo = 0;
            $ventas->total_debito = 0;
            $ventas->total_cheque = 0;
            $ventas->total_credito = 0;
            $ventas->presupuestos_id = $ventas->id;
            $ventas->transaccion = 1;
            $ventas->tipo_facturacion_id = 1;
            $ventas->total_pagado = 0;
            /*$ventas->sucursal = $this->user->sucursal;
            $ventas->caja = $this->user->caja;
            $ventas->cajadiaria = $this->user->cajadiaria;*/
            $ventas->success = array($this,'successAddPresupuesto');
            $ventas->cheques = array();            
            $this->db->select('
                presupuesto_detalle.*,
                productos.nombre_comercial,
                productos.codigo as producto,
                productos.precio_credito,
                productos.precio_venta_mayorista1,
                productos.precio_venta_mayorista2,
                productos.precio_venta_mayorista3
            ');
            $this->db->join('productos','productos.codigo = presupuesto_detalle.producto');
            foreach($this->db->get_where('presupuesto_detalle',array('presupuesto_id'=>$ventas->id,'presupuesto_detalle.anulado'=>0))->result() as $v){
                if($this->ajustes->controlar_vencimiento_stock==0){
                    $stock = @$this->db->get_where('productosucursal',array('producto'=>$v->producto,'sucursal'=>$this->user->sucursal))->row()->stock;
                    $venc = '00/0000';
                    $vencimiento = '';
                }else{
                    $this->db->order_by('vencimiento','ASC');
                    $venc = $this->db->get_where('productosucursal',array('producto'=>$v->producto,'sucursal'=>$this->user->sucursal,'stock >'=>0));
                    $v1 = [];
                    $v2 = [];
                    foreach($venc->result() as $vv){
                        $v1[] = $vv->stock;
                        $v2[] = $vv->vencimiento;
                    }
                    $stock = count($v1)>0?$v1[0]:'';
                    $vencimiento = count($v2)>0?$v2[0]:'';
                    $stockOpts = $v1;
                    $vencimientoOpts = $v2;
                }
                if(get_instance()->router->fetch_class()=='ventas' && $v->total_adic>0){//Sacar nuevo precio de ventas con adicional
                    $v->total_adic = ($v->precio_venta*$v->cantidad)+((($v->precio_venta*$v->cantidad)*$v->porc)/100);
                    $v->precio_venta = $v->total_adic/$v->cantidad;
                }
                $pr = array(
                    'codigo'=>$v->producto,
                    'nombre'=>$v->nombre_comercial,
                    'cantidad'=>$v->cantidad,
                    'precio_venta'=>$v->precio_venta,
                    'precio_original'=>$v->precio_venta,
                    'precio_credito'=>$v->precio_credito,
                    'precio_venta_contado'=>$v->precio_venta,
                    'precio_venta_mayorista1'=>$v->precio_venta_mayorista1,
                    'precio_venta_mayorista2'=>$v->precio_venta_mayorista2,
                    'precio_venta_mayorista3'=>$v->precio_venta_mayorista3,
                    'por_desc'=>$v->precio_venta,
                    'por_venta'=>$v->porc,
                    'precio_descuento'=>0,
                    'total'=>$v->total,
                    'stock'=>$stock,
                    'vencimiento'=>$vencimiento,
                );
                if($this->ajustes->controlar_vencimiento_stock==1){
                    $pr['stockOpts'] = $stockOpts;
                    $pr['vencimientoOpts'] = $vencimientoOpts;
                }
                $ventas->plan = 0;
                $ventas->productos[] = $pr;
            }
            return $ventas;
        }

    }

    function getSport($y,$validarFacturacion = TRUE){
        $where['sport_reservas_canchas.id'] = $y;
        $where['facturado'] = 0;
        $this->db->select('
            sport_reservas_canchas.*,
            sport_canchas.productos_id,
            productos.codigo,
            productos.nombre_comercial,
            productos.precio_venta,
            productos.precio_venta,
            productos.precio_venta,
            productos.precio_venta_mayorista1,
            productos.precio_venta_mayorista2,
            productos.precio_venta_mayorista3,
            productos.precio_venta
        ');
        $this->db->join('sport_canchas','sport_canchas.id = sport_reservas_canchas.sport_canchas_id');
        $this->db->join('productos','productos.id = sport_canchas.productos_id');
        $ventas = $this->db->get_where('sport_reservas_canchas',$where);
        if($ventas->num_rows()==0){
            redirect('/sport/sport_reservas_canchas');
            return;
        }
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->tipo = 'Reserva Cancha';
            $ventas->productos = array();            
            $ventas->pago_pesos = 0;
            $ventas->pago_dolares = 0;
            $ventas->pago_reales = 0;
            $ventas->pago_guaranies = 0;            
            $ventas->total_efectivo = 0;
            $ventas->forma_pago = 1;
            $ventas->total_debito = 0;
            $ventas->total_cheque = 0;
            $ventas->total_credito = 0;
            $ventas->presupuestos_id = $ventas->id;
            $ventas->transaccion = 1;
            $ventas->tipo_facturacion_id = 1;
            $ventas->total_pagado = 0;
            /*$ventas->sucursal = $this->user->sucursal;
            $ventas->caja = $this->user->caja;
            $ventas->cajadiaria = $this->user->cajadiaria;*/
            $ventas->success = array($this,'successAddCancha');
            $ventas->cheques = array();            
            $pr = array(
                'codigo'=>$ventas->codigo,
                'nombre'=>$ventas->nombre_comercial,
                'precio_venta'=>$ventas->precio_venta,
                'precio_original'=>$ventas->precio_venta,
                'precio_credito'=>0,
                'cantidad'=>round($ventas->cant_minutos/60,0),
                'precio_venta_contado'=>$ventas->precio_venta,
                'precio_venta_mayorista1'=>$ventas->precio_venta_mayorista1,
                'precio_venta_mayorista2'=>$ventas->precio_venta_mayorista2,
                'precio_venta_mayorista3'=>$ventas->precio_venta_mayorista3,
                'por_desc'=>$ventas->precio_venta,
                'por_venta'=>100,
                'precio_descuento'=>0,
            );
            $ventas->plan = 0;
            $ventas->productos[] = $pr;
            return $ventas;
        }

    }

    function successAddPresupuesto($post,$primary){
        if(!empty($post['servicios_id']) && is_numeric($post['servicios_id'])){
            //Actualizamos la venta en servicios id y facturado = 0
            $this->db->update('presupuesto',array('facturado'=>1,'nro_factura'=>$primary),array('id'=>$post['servicios_id']));
        }
    }

    function successAddOrdenTrabajo($post,$primary){
        if(!empty($post['orden_trabajo_id']) && is_numeric($post['orden_trabajo_id'])){
            //Actualizamos la venta en servicios id y facturado = 0
            $this->db->update('orden_trabajo',array('estado'=>1),array('id'=>$post['orden_trabajo_id']));
        }
    }

    function successAddCancha($post,$primary){
        if(!empty($post['id']) && is_numeric($post['id'])){
            //Actualizamos la venta en servicios id y facturado = 0
            $this->db->update('sport_reservas_canchas',array('facturado'=>1),array('id'=>$post['id']));
        }
    }

    

    function getSalida($y){

        $ventas = $this->db->get_where('salidas',array('id'=>$y));
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->productos = array();
            $this->db->select('salida_detalle.*,productos.nombre_comercial');
            $this->db->join('productos','productos.codigo = salida_detalle.producto');
            foreach($this->db->get_where('salida_detalle',array('salida'=>$ventas->id))->result() as $v){
                $ventas->productos[] = array(
                    'codigo'=>$v->producto,
                    'nombre'=>$v->nombre_comercial,
                    'cantidad'=>$v->cantidad,
                    'precio_costo'=>$v->precio_costo,
                    'sucursal'=>$v->sucursal,
                    'total'=>$v->total
                );
            }
            return $ventas;
        }else{
            return null;
        }

    }

    function getEntrada($y){

        $ventas = $this->db->get_where('entrada_productos',array('id'=>$y));
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->productos = array();
            $this->db->select('entrada_productos_detalles.*,productos.nombre_comercial');
            $this->db->join('productos','productos.codigo = entrada_productos_detalles.producto');
            foreach($this->db->get_where('entrada_productos_detalles',array('entrada_producto'=>$ventas->id))->result() as $v){
                $ventas->productos[] = array(
                    'codigo'=>$v->producto,
                    'nombre'=>$v->nombre_comercial,
                    'cantidad'=>$v->cantidad,
                    'precio_costo'=>$v->precio_costo,
                    'precio_venta'=>$v->precio_venta,
                    'sucursal'=>$v->sucursal,
                    'vencimiento'=>$v->vencimiento,
                    'total'=>$v->total
                );
            }
            return $ventas;
        }else{
            return null;
        }

    }

    function getTransferencia($y){

        $ventas = $this->db->get_where('transferencias',array('id'=>$y));
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->fecha_solicitud = date("Y-m-d",strtotime($ventas->fecha_solicitud));
            $ventas->productos = array();
            $this->db->select('transferencias_detalles.*,productos.nombre_comercial, productos.precio_costo, productos.precio_venta, productos.stock, (productos.precio_costo*transferencias_detalles.cantidad) as total');
            $this->db->join('productos','productos.codigo = transferencias_detalles.producto');
            foreach($this->db->get_where('transferencias_detalles',array('transferencia'=>$ventas->id))->result() as $v){
                $ventas->productos[] = array(
                    'codigo'=>$v->producto,
                    'nombre'=>$v->nombre_comercial,
                    'cantidad'=>$v->cantidad,
                    'precio_costo'=>$v->precio_costo,
                    'precio_venta'=>$v->precio_venta,
                    'total'=>$v->total
                );
            }
            return $ventas;
        }else{
            return null;
        }

    }

    function getPedidosProveedores($y,$validarFacturacion = TRUE){
        $where['id'] = $y;
        $ventas = $this->db->get_where('pedidos_proveedores',$where);
        if($ventas->num_rows()>0){
            $ventas = $ventas->row();
            $ventas->tipo = 'Pedido';
            $ventas->productos = array();                        
            $ventas->pedidos_proveedores = $ventas->id;
            $ventas->success = array($this,'successAddPedido');            
            $this->db->select('
                pedidos_proveedores_detalles.*,
                productos.nombre_comercial,
                productos.codigo as producto, 
                productos.stock,
                productos.precio_venta_mayorista1,
                productos.precio_venta_mayorista2,
                productos.precio_venta_mayorista3
            ');
            $this->db->join('productos','productos.codigo = pedidos_proveedores_detalles.producto');
            foreach($this->db->get_where('pedidos_proveedores_detalles',array('pedidos_proveedores'=>$ventas->id))->result() as $v){
                $ventas->productos[] = array(
                    'codigo'=>$v->producto,
                    'nombre_comercial'=>$v->nombre_comercial,
                    'cantidad'=>$v->cantidad,
                    'precio_costo'=>$v->precio_costo,
                    'precio_venta'=>$v->precio_venta,
                    'por_desc'=>$v->por_desc,
                    'por_venta'=>$v->por_venta,
                    'precio_descuento'=>0,
                    'total'=>$v->total,
                    'stock'=>$v->stock,
                    'precio_venta_mayorista1'=>$v->precio_venta_mayorista1,
                    'precio_venta_mayorista2'=>$v->precio_venta_mayorista2,
                    'precio_venta_mayorista3'=>$v->precio_venta_mayorista3,
                );
            }
            return $ventas;
        }

    }

    
}
?>
