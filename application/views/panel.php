<?php $this->load->view('includes/header'); ?>
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
        <?php $this->load->view('includes/topbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                <?php $this->load->view('includes/breadcum'); ?>
                        <!-- begin:: Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                            <!--Begin::Dashboard 1-->

                             <?php
                                if(empty($crud)):
                                    if(empty($_SESSION['dashboard']) || $this->ajustes->habilitar_cache==0){
                                        $dashboard = $this->user->pagina_principal;
                                        $dashboard = empty($dashboard)?'dashboard':$dashboard;
                                        $dashboard = $this->load->view($dashboard,array(),TRUE,'dashboards');
                                        $_SESSION['dashboard'] = $dashboard;
                                        $_SESSION['lastTime'] = time();
                                    }                                    
                                endif;
                            ?>
                            <?= empty($crud) ? $_SESSION['dashboard']: $this->load->view('cruds/' . $crud) ?>   
                            <?php 
                                if(!empty($_SESSION['lastTime']) && time()-$_SESSION['lastTime']>900){
                                    unset($_SESSION['dashboard']);
                                }
                            ?>
                            <!--End::Dashboard 1-->
                        </div>

                        <!-- end:: Content -->
                    </div>

                    <!-- begin:: Footer -->
                    <?php $this->load->view('includes/footer'); ?>
                    <!-- end:: Footer -->
                </div>
            </div>
        </div>

        <!-- end:: Page -->

        <!-- begin::Scrolltop -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>

        <!-- end::Scrolltop -->

<?php     
    if(empty($crud)):
    get_instance()->js[] = '
        <script src="'.base_url().'assets/plugins/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>                
        <script src="'.base_url().'js/pages/dashboard.js" type="text/javascript"></script>';
    endif;
?>

<script>
    window.afterLoad.push(function(){
        if(typeof(localStorage.ocultarBarra) !== 'undefined' && localStorage.ocultarBarra == '1'){
            $('body').attr('class','kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize');
        }
        
        $("button#kt_aside_toggler").click(function(){
            if($("body").hasClass('kt-aside--minimize')){
                localStorage.ocultarBarra = '1';
            }else{
                localStorage.ocultarBarra = '0';
            }
        }); 
    });
</script>