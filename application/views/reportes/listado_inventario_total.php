<?php if(empty($_POST)): ?>
<div class="container">
    <h1 align="center"> Resumen de ventas</h1>
<form action="<?= base_url('reportes/listado_inventario_total') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione un proveedor</label>
        <?= form_dropdown_from_query('proveedor','proveedores','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione una sucursal</label>
        <?= form_dropdown_from_query('sucursal','sucursales','id','denominacion',0) ?>
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
    <? if(!empty($_POST['proveedor']))$cliente = $this->db->get_where('proveedores',array('id'=>$_POST['proveedor']))->row()->denominacion; ?>
<? if(!empty($_POST['sucursal']))$sucursal = $this->db->get_where('sucursales',array('id'=>$_POST['sucursal']))->row()->denominacion; ?>
    <h1 align="center"> Listado de inventario</h1>
    <p><strong>Sucursal: </strong> <?= empty($_POST['sucursal'])?'Todos':$sucursal ?></p>
    <p><strong>Proveedor: </strong> <?= empty($_POST['proveedor'])?'Todos':$cliente ?></p>    
    
    <table border="0" cellspacing="18" class="table" width="100%">
        <thead>
                <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Cantidad</th>                        
                </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($_POST['proveedor']))$this->db->where('productos.proveedor_id',$_POST['proveedor']);
                if(!empty($_POST['sucursal']))$this->db->where('productosucursal.sucursal',$_POST['sucursal']);
                $total = 0;
                $total_debito = 0;
                $total_credito = 0;
                $this->db->select('productosucursal.producto as codigo, productos.nombre_comercial as producto, SUM( productosucursal.stock ) AS stockTotal',false);
                $this->db->join('productos','productos.codigo = productosucursal.producto');
                $this->db->group_by('productosucursal.producto');
                $this->db->having('stockTotal > ',0);
                $this->db->order_by('productos.nombre_comercial','ASC');
                $ventas = $this->db->get('productosucursal');
                
            ?>
            <?php foreach($ventas->result() as $c): ?>
                <tr>                        
                        <td><?= $c->codigo ?></td>
                        <td><?= $c->producto ?></td>
                        <td><?= $c->stockTotal ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
<?php endif; ?>