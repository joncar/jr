<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));
                $this->load->library('html2pdf/html2pdf');
                $this->as = array('usuarios'=>'user','categorias'=>'categoriaproducto');
	}
       
        public function index($url = 'main',$page = 0)
	{
		$this->loadView('panel');
	}
        
        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));
            else
            parent::loadView($crud);
        } 
        
        protected function crud_function($x,$y){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            $requireds = array();
            foreach($this->db->field_data($table) as $row){
                if(empty($this->norequireds) || !empty($this->norequireds) && !in_array($row->name,$this->norequireds))
                        array_push($requireds,$row->name);
            }
            $crud->required_fields_array($requireds);
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }
        
        
        /*Cruds*/                     
        protected function funciones($crud,$x,$y)
        {        
            $this->refresh_funciones();
            $crud->set_relation('controlador','controladores','nombre');            
            $crud->display_as('visible','Visible en el menu');
            $crud->field_type('visible','true_false',array('0'=>'No','1'=>'Si'));
            $crud->required_fields('nombre','controlador','visible');
            //Agregar reportes especificos
            $this->db->where('reportes.tipo','1');
            $crud = $this->get_reportes($crud);
            return $crud;
        }
        
        protected function usuarios($crud,$x,$y)
        {            
            //Fields
            $crud->field_type('status','invisible')
                 ->field_type('password','password')
                 ->field_type('status','true_false',array('0'=>'Bloqueado','1'=>'Activo'))
                 ->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));
            //unsets           
            //Displays
            $crud->display_as('password','Contraseña')
                 ->display_as('admin','Super administrador');            
            //Fields types            
            //Validations  
            $crud->required_fields(
                    'nombre','apellido','email',
                    'password','password','usuario');  
            
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            $crud->set_rules('usuario','Usuario','required|is_unique[user.usuario]');
            $crud->set_rules('password','Contraseña','required|min_length[8]');                    
            //Callbacks            
            $crud->callback_before_insert(array($this,'user_binsertion'));
            $crud->callback_after_insert(array($this,'user_ainsertion'));    
            //Agregar reportes especificos
            $this->db->where('reportes.tipo','1');
            $crud = $this->get_reportes($crud);
            return $crud;
        }
        
        protected function roles($crud,$x,$y)
	{            
            $crud->set_relation('user','user','{nombre} {apellido}');
            $crud->set_relation('controlador','controladores','nombre');
            $crud->set_relation('funcion','funciones','nombre');
            $crud->set_relation_dependency('funcion','controlador','controlador');
            $crud->display_as('user','Usuario')
                 ->display_as('update','Accesos');
            $crud->field_type('insert','invisible');
            $crud->field_type('select','invisible');
            $crud->field_type('delete','invisible');
            $crud->callback_field('update',array($this,'update_rolesField'));                        
            $crud->field_type('created','invisible');          
            $crud->field_type('modified','invisible');  
            $crud->required_fields('user','controlador');
            $crud->callback_before_insert(array($this,'roles_binsertion'));
            $crud->callback_after_insert(array($this,'roles_ainsertion'));
            //Agregar reportes especificos
            $this->db->where('reportes.tipo','1');
            $crud = $this->get_reportes($crud);
            return $crud;            
	}                
               
        function reportes($crud,$x,$y)
        {            
            $crud->set_relation('controlador','controladores','nombre')
                 ->set_relation('funcion','funciones','nombre');
            $crud->set_relation_dependency('funcion','controlador','controlador');
            $crud->field_type('tipo','true_false',array('0'=>'General','1'=>'Especifico'));
            $crud->field_type('orientacion','dropdown',array('P'=>'Vertical','L'=>'Horizontal'));
            $crud->field_type('papel','dropdown',array('Letter'=>'Carta','A4'=>'A4','otro'=>'Medida'));
            $crud->field_type('variables','products',array(
                'table'=>'reportes_variables',
                'dropdown'=>'tipo_dato',
                'relation_field'=>'reporte',
                'source'=>array('input'=>'input','date'=>'date'),                
            ));
            $crud->add_action('<i title="Imprimir reporte" class="fa fa-print"></i>','',base_url('panel/imprimir_reporte').'/');
            $crud->callback_add_field('margen_izquierdo',function($val){return form_input('margen_izquierdo',5,'id="field-margen_izquierdo"');});
            $crud->callback_add_field('margen_derecho',function($val){return form_input('margen_derecho',5,'id="field-margen_derecho"');});
            $crud->callback_add_field('margen_superior',function($val){return form_input('margen_superior',5,'id="field-margen_superior"');});
            $crud->callback_add_field('margen_inferior',function($val){return form_input('margen_inferior',8,'id="field-margen_inferior"');});
            $crud->required_fields('controlador','funcion','nombre','contenido','tipo','orientacion','papel','margen_izquierdo','margen_derecho',
                  'margen_superior','margen_inferior');
            $crud->columns('tipo','nombre','controlador','funcion');
            if(!empty($_POST) && !empty($_POST['papel']) && $_POST['papel']=='otro')
            {
                $crud->set_rules('ancho','Ancho','required');
                $crud->set_rules('alto','Alto','required');
            }
            
            $crud->callback_before_insert(function($post){$post['contenido'] = base64_encode($post['contenido']); return $post;});
            $crud->callback_before_update(function($post){$post['contenido'] = base64_encode($post['contenido']); return $post;});
            return $crud; 
        }

        function compras($crud,$x,$y){
            $crud->set_relation('proveedor','proveedores','denominacion');            
            $crud->field_type('descripcion','products',array(
                'table'=>'compradetalles',
                'dropdown'=>'producto_id',
                'relation_field'=>'compra_id',
                'source'=>'productos',
                'field_detalle'=>'nombre_comercial',
                'class'=>array('articulo'=>'chosen-select','codigo'=>'codigoproducto','cantidad'=>'cantidades','precio'=>'precios','total'=>'totales','preciomayorista'=>'p1','preciominorista'=>'p2')
            ));

            return $crud;
        }
        
        function refresh_funciones(){            
            $this->_refresh_funciones('application/controllers/admin.php',1);
            $this->_refresh_funciones('application/controllers/cajero.php',2);
        }
        
        function _refresh_funciones($file,$controller){
            $admin_file = fopen($file, "r");
            $funciones = array();
            while(!feof($admin_file))
            {
                $line = fgets($admin_file);
                if(strstr($line,'public function'))
                    $funciones = array_merge ($funciones,fragmentar($line,'public function ','('));
            }
            fclose($admin_file);
            foreach($funciones as $f){
                if($this->db->get_where('funciones',array('controlador'=>$controller,'nombre'=>$f))->num_rows==0)
                    $this->db->insert('funciones',array('controlador'=>$controller,'nombre'=>$f,'visible'=>1));
            }   
        }
        /* Callbacks */                
        function user_binsertion($post)
        {            
            $post['status'] = 1;            
            $post['password'] = md5($post['password']);
            return $post;
        }
        
        function user_ainsertion($post,$id)
        {
            $this->roles_binsertion(array('controlador'=>$post['admin']));
            $this->roles_ainsertion(array('user'=>$id,'controlador'=>$post['admin'],));
            return true;
        }
        
        function roles_binsertion($post)
        {
            if(empty($post['funcion'])){
                $this->db->delete('roles',array('user'=>$post['user']));
                $post['funcion'] = $this->db->get_where('funciones',array('controlador'=>$post['controlador']))->row()->id;
                $this->all = TRUE;
            }
            else
                $this->all = FALSE;
            return $post;
        }
        
        function roles_ainsertion($post)
        {            
            if($this->all)
            {
                
                $x = 0;
                foreach($this->db->get_where('funciones',array('controlador'=>$post['controlador']))->result() as $y)
                {
                    if($x>0)
                    {
                        if($this->db->get_where('roles',array('controlador'=>$post['controlador'],'funcion'=>$y->id,'user'=>$post['user']))->num_rows==0)
                        $this->db->insert('roles',array('controlador'=>$post['controlador'],'funcion'=>$y->id,'user'=>$post['user']));
                    }
                    $x++;
                }
            }
            return true;
        }
        
        function update_rolesField($val = '',$primary = '')
        {
            if(!empty($primary))
            {
                $roles = $this->db->get_where('roles',array('id'=>$primary))->row();
                $i = $roles->insert==1?TRUE:FALSE;
                $u = $roles->update==1?TRUE:FALSE;
                $s = $roles->select==1?TRUE:FALSE;
                $d = $roles->delete==1?TRUE:FALSE;
            }
            else
            {
                $i = FALSE;
                $u = FALSE;
                $s = FALSE;
                $d = FALSE;
            }
            return form_checkbox('select',1,$i,'id="field-select"').' Select '.
                   form_checkbox('insert',1,$i,'id="field-insert"').' Insertar '.
                   form_checkbox('update',1,$i,'id="field-update"').' Update '.
                   form_checkbox('delete',1,$i,'id="field-delete"').' Delete ';           
        }                        
        
        //Esto es para la parte de reportes variables
        protected function get_reportes($crud)
        {
            $this->db->where('funciones.nombre',$this->router->fetch_method());           
            $this->db->where('controladores.nombre',$this->router->fetch_class());           
            $this->db->join('controladores','controladores.id = reportes.controlador');            
            $this->db->join('funciones','funciones.id = reportes.funcion');            
            $this->db->select('reportes.*');
            foreach($this->db->get('reportes')->result() as $r){
                if($r->icono!='')
                $crud->add_action('<i title="'.$r->nombre.'" class="fa fa-'.$r->icono.'"></i>','',base_url('panel/imprimir_reporte/'.$r->id).'/');
                else
                $crud->add_action($r->nombre,'',base_url('panel/imprimir_reporte/'.$r->id).'/');
            }
            return $crud;
        }
        
        public function imprimir_reporte($id,$idtable = '')
        {
            $idtable = empty($idtable)?$id:$idtable;
            $reporte = $this->db->get_where('reportes',array('id'=>$id));
            if($reporte->num_rows>0)
            {
                //Es un reporte con variables? Pues se recogen antes de procesar                
                $variables = $this->db->get_where('reportes_variables',array('reporte'=>$id));
                if($variables->num_rows>0 && empty($_POST))
                {
                    $output = '<form action="'.base_url('panel/imprimir_reporte/'.$id.'/'.$idtable).'" method="post" class="form-group" role="form" onsubmit="return validar(this)">';
                    $output.= '<h3>Por favor complete los datos para generar el reporte</h3>';
                    //Cargar librerias
                    $date = 0;
                    foreach($variables->result() as $v)
                    {
                        switch($v->tipo_dato)
                        {
                            case 'date': $date = 1; break;                            
                        }
                    }
                    
                    if($date == 1)
                     $output.= $this->load->view('predesign/datepicker',null,TRUE);                    
                    //dibujar forms
                    foreach($variables->result() as $v){
                        switch($v->tipo_dato)
                        {
                            case 'date': $class = 'datetime-input'; break;
                        }
                        $output.= '<div class="form-group">'.form_input($v->variable,'','id="field-'.$v->variable.'" class="form-control '.$class.'" placeholder="'.ucfirst($v->variable).'"').'</div>';
                    }
                    $output.= '<div align="center"><button type="submit" class="btn btn-success">Procesar</button></div>';
                    $output.= '</form>';
                    $this->loadView(array('crud'=>'user','view'=>'panel','output'=>$output));
                }
                else{
                $reporte = $reporte->row();
                $texto = base64_decode($reporte->contenido);
                $texto = str_replace('$_USER',$_SESSION['user'],$texto);
                $texto = str_replace('$_ID',$idtable,$texto);
                //Reemplazar variables recogidas en el formulario
                if($variables->num_rows>0)
                {
                    foreach($variables->result() as $v)
                    {
                        if(!empty($_POST[$v->variable]))
                        {
                            if($v->tipo_dato=='date')$_POST[$v->variable] = date("Y-m-d",strtotime(str_replace("/","-",$_POST[$v->variable])));
                            $texto = str_replace('$_'.$v->variable,$_POST[$v->variable],$texto);
                        }
                    }
                }
                //Funciones 
                $funciones = fragmentar($texto,'{function=','}');
                foreach($funciones as $f){
                    $texto = call_user_func($f,$idtable,$texto);
                }
                $texto = str_replace("&gt;", '>', $texto);
                $texto = str_replace("&lt;", '<', $texto);
                $sql = fragmentar($texto,'{sql=','}');                
                foreach($sql as $s)
                {
                    list($sentencia,$type) = explode(";",$s);
                    $sentencia = str_replace('"','',$sentencia);                    
                    switch($type)
                    {
                        case 'data':$texto = str_replace('{sql='.$s.'}',sqltodata($this->db->query(strip_tags($sentencia))),$texto);
                        case 'table':$texto = str_replace('{sql='.$s.'}',sqltotable($this->db->query(strip_tags($sentencia))),$texto);
                    }
                }
                $papel = $reporte->papel=='otro'?array($reporte->ancho,$reporte->alto):$reporte->papel;
                $html2pdf = new HTML2PDF($reporte->orientacion,$papel,'fr', false, 'ISO-8859-15', array($reporte->margen_izquierdo, $reporte->margen_superior, $reporte->margen_derecho, $reporte->margen_inferior));
                $html2pdf->writeHTML(utf8_decode($texto));
                $html2pdf->Output($reporte->nombre.'.pdf');
                }
                
            }
            else $this->loadView('404');
        }
        
        function selsucursal($x = '')
        {
            if(!empty($x) && is_numeric($x))
            {
                $_SESSION['sucursal'] = $x;
                if(empty($_GET['redirect']))
                header("Location:".base_url('panel'));
                else
                header("Location:".base_url($_GET['redirect']));
            }
            else{
            $crud = new ajax_grocery_CRUD();
            $crud->set_subject('Seleccione una sucursal');
            $crud->set_table('sucursales');
            $crud->set_theme('bootstrap2');
            
            $crud->unset_delete()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_print()
                 ->unset_read()
                 ->unset_export()
                 ->columns('denominacion')
                 ->callback_column('denominacion',function($val,$row){$redirect = empty($_GET['redirect'])?'':'?redirect='.$_GET['redirect']; return '<a href="'.base_url('panel/selsucursal/'.$row->id.$redirect).'">'.$val.'</a>';});            
            
            $output = $crud->render();
            $output->crud = 'user';
            $output->view = 'panel';
            $output->title = 'Seleccione una sucursal';
            $output->output = '<h2>Seleccione una sucursal</h2>'.$output->output;
            $this->loadView($output);
            }
        }
        
        function selcaja($x = '')
        {
            if(!empty($x) && is_numeric($x))
            {
                $_SESSION['caja'] = $x;
                if(empty($_GET['redirect']))
                header("Location:".base_url('panel'));
                else
                header("Location:".base_url($_GET['redirect']));
            }
            else{
            $crud = new ajax_grocery_CRUD();
            $crud->set_subject('Seleccione una caja');
            $crud->set_table('cajas');
            $crud->set_theme('bootstrap2');
            if(!empty($_SESSION['sucursal']))
                $crud->where('cajas.sucursal',$_SESSION['sucursal']);
            $crud->unset_delete()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_print()
                 ->unset_read()
                 ->unset_export()
                 ->columns('denominacion')
                 ->callback_column('denominacion',function($val,$row){$redirect = empty($_GET['redirect'])?'':'?redirect='.$_GET['redirect']; return '<a href="'.base_url('panel/selcaja/'.$row->id.$redirect).'">'.$val.'</a>';});            
            
            $output = $crud->render();
            $output->crud = 'user';
            $output->view = 'panel';
            $output->title = 'Seleccione una caja';
            $output->output = '<h2>Seleccione una caja</h2>'.$output->output;
            $this->loadView($output);
            }
        }
        
        function change($val){
            $_SESSION[$val] = '';
            header("Location: ".base_url('panel/sel'.$val));
        }
        //JSONS                   
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
