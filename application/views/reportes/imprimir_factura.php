<html lang="es">
        <body style="cursor: auto;">       

        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 426px; top: 113px; font-size:11px;">Fecha:<?= date("d/m/Y",strtotime($venta->fecha)) ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 710px; top: 113px; font-size:11px;">Transacción:<?= $this->db->get_where('tipotransaccion',array('id'=>$venta->transaccion))->row()->denominacion ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 426px; top: 128px; font-size:11px;">CI/Ruc:<?php $cliente = $this->db->get_where('clientes',array('id'=>$venta->cliente))->row() ?>
                    <?= $cliente->nro_documento ?></div>
       
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 426px; top: 138px; font-size:11px;"> Cliente:<?= $cliente->nombres.' '.$cliente->apellidos ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 412px; top: 187px; font-size:11px;">
            <table cellspacing="5" style="font-size:11px;">
                    <tbody>
                        
						    <tr>
							  <td> Descripción</td>
							  <td align="right">  Cantidad</td>
							  <td align="right"> Precio</td>
							  <td align="right"> Total</td>
							   <td align="right"> IVA</td>
							</tr>
                            <?php foreach($detalles->result() as $d): ?>
                            <tr>
                                <td style="width:120px;"><?php
                                    $this->db->or_where('id',$d->producto);
                                    $this->db->or_where('codigo',$d->producto);
                                    echo cortar_palabras($this->db->get('productos')->row()->nombre_comercial,4); ?></td>
                                <td style="width:50px;" align="right"><?= $d->cantidad ?></td>
                                <td style="width:80px;" align="right"><?= number_format($d->precioventa,0,',','.') ?></td>
                                <td style="width:75px;" align="right"><?= number_format($d->totalcondesc,0,',','.') ?></td>
                                <td style="width:30px;" align="right"><?= number_format($d->iva,0,',','.') ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
        </div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 417px; top: 391px; font-size:11px;">Son:<?= @$this->enletras->ValorEnLetras($venta->total_venta,'Gs') ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 733px; top: 391px; font-size:11px;">Total:<?= number_format($venta->total_venta,0,',','.').' Gs' ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 20px; top: 411px; font-size:11px;">Ex.:<?= number_format($venta->exenta,0,',','.') ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 80px; top: 411px; font-size:11px;">IVA 5%: <?= number_format($venta->iva,0,',','.') ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 180px; top: 411px; font-size:11px;">IVA 10%: <?= number_format($venta->iva2,0,',','.') ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 280px; top: 411px; font-size:11px;">Total IVA:<?= number_format($venta->total_iva,0,',','.') ?></div>
        
        
        
        
        
        
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 20px; top: 113px; font-size:11px;">Fecha:<?= date("d/m/Y",strtotime($venta->fecha)) ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 172px; top: 113px; font-size:11px;">Transacción:<?= $this->db->get_where('tipotransaccion',array('id'=>$venta->transaccion))->row()->denominacion ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 20px; top: 128px; font-size:11px;">CI/Ruc: <?php $cliente = $this->db->get_where('clientes',array('id'=>$venta->cliente))->row() ?>
                    <?= $cliente->nro_documento ?></div>
      
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 20px; top: 138px; font-size:11px;">Cliente:<?= $cliente->nombres.' '.$cliente->apellidos ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 20px; top: 187px; font-size:11px;">
                <table cellspacing="5">
                    <tbody>
                        
						<tr>
							  <td> Descripción</td>
							  <td align="right">  Cantidad</td>
							  <td align="right"> Precio</td>
							  <td align="right"> Total</td>
							   <td align="right"> IVA</td>
							</tr>
                        <?php foreach($detalles->result() as $d): ?>
                        <tr>
                            <td style="width:120px;"><?php
                                    $this->db->or_where('id',$d->producto);
                                    $this->db->or_where('codigo',$d->producto);
                                    echo cortar_palabras($this->db->get('productos')->row()->nombre_comercial,4); ?></td>
                            <td style="width:50px;" align="right"><?= $d->cantidad ?></td>
                            <td style="width:80px;" align="right"><?= number_format($d->precioventa,0,',','.') ?></td>
                            <td style="width:75px;" align="right"><?= number_format($d->totalcondesc,0,',','.') ?></td>
                            <td style="width:30px;" align="right"><?= number_format($d->iva,0,',','.') ?></td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
        </div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 21px; top: 391px; font-size:11px;">Son: <?= @$this->enletras->ValorEnLetras($venta->total_venta,'Gs') ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 265px; top: 391px; font-size:11px;">Total:<?= number_format($venta->total_venta,0,',','.').' Gs' ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 420px; top: 411px; font-size:11px;">Ex.:<?= number_format($venta->exenta,0,',','.') ?></div>
		
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 510px; top: 411px; font-size:11px;">IVA 5%:<?= number_format($venta->iva,0,',','.') ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 620px; top: 411px; font-size:11px;">IVA 10%: <?= number_format($venta->iva2,0,',','.') ?></div>
        <div class="ui-draggable ui-draggable-handle" style="position: absolute; left: 720px; top: 411px; font-size:11px;">TOTAL IVA:<?= number_format($venta->total_iva,0,',','.') ?></div>
        </body>
</html>
