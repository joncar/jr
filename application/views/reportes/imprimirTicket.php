
<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <title>Factura</title>
                
                
        </head>
        <body style='font-size:9px; width:200px; margin:0px;'>
            <h3 align="center" style="font-size:20px; font-weight:bold; margin-bottom:5px"><?= $venta->denominacion ?></h3>
            <div align="center"><?= $venta->direccion ?></div>
            <div align="center" style='border-bottom:1px solid black'>Telef. <?= $venta->telefono ?></div>
            <table style='width:227px; font-size:9px;'>
                <tr><td><b>Nro. Venta: </b><?= $venta->id ?></td><td><b>Cajero/a</b> <?= $_SESSION['nombre'] ?></td></tr>
            </table>
        <div><b>Fecha: </b><?= date("d/m/Y H:i:s",strtotime($venta->fecha)) ?></div>
        <div><b>Caja: </b><?= $venta->caja ?></div>
        <div><b>Condición venta: </b><?= $this->db->get_where('tipotransaccion',array('id'=>$venta->transaccion))->row()->denominacion ?></div>
        <div><b>Cliente: </b><?= $venta->clientename.' '.$venta->clienteadress ?></div>
        
        <div>
            <table cellspacing="4" style="font-size:11px;">
                <thead>
                    <tr style="border-top:1px solid black; border-bottom:1px solid black">
                        <th style="width:30%;">Descripcion</th>
                        <th style="width:10%; text-align:center;">Cant.</th>
                        <th style="width:10%; text-align:center;">Precio Unit.</th>
                        <th style="width:10%; text-align:center;">Total</th>
                    </tr>
                </thead>
                    <tbody>
                        <?php foreach($detalles->result() as $d): ?>
                            <tr><td><?php $this->db->or_where('id',$d->producto); $this->db->or_where('codigo',$d->producto); echo $this->db->get_where('productos')->row()->nombre_comercial ?></td><td align="right"><?= $d->cantidad ?></td><td align="right"><?= $d->precioventa ?></td><td align="right"><?= $d->totalcondesc ?></td></tr>
                        <?php endforeach ?>
                            <!--<tr>
                                <td style="border-top:1px solid black">Total Venta: </td>
                                <td style="border-top:1px solid black" colspan='3' align="right">
                                    <?= number_format($venta->subtotal,0,',','.').' Gs' ?>                                        
                                </td>
                            </tr>-->
                            <tr><td>Descuento: </td><td colspan='3' align="right"><?= number_format($venta->descuento,0,',','.').' Gs' ?></td></tr>
                            <?php if($venta->transaccion==1): ?>
                            <tr><td>Total a pagar: </td><td colspan='3' align="right"><?= number_format($venta->total_venta,0,',','.').' Gs' ?></td></tr>
                            <tr><td>Efectivo: </td><td colspan='3' align="right"><?= number_format($venta->total_efectivo,0,',','.').' Gs' ?></td></tr>
                            <tr><td>Vuelto: </td><td colspan='3' align="right"><?= number_format($venta->vuelto,0,',','.').' Gs' ?></td></tr>
                            <?php else: ?>
                            <tr><td>Total compra: </td><td colspan='3' align="right"><?= number_format($venta->total_venta,0,',','.').' Gs' ?></td></tr>
                            <?php
                                $saldo = $this->db->query('select get_saldo('.$venta->cliente.') as saldo')->row()->saldo;
                            ?>
                            <tr><td>Saldo actual: </td><td colspan='3' align="right"><?= number_format($saldo,0,',','.').' Gs' ?></td></tr>
                            <?php endif ?>
                    </tbody>
                </table>
        </div>
        <p align='center' style="margin:10px; font-size:14px;"><i>Agradecemos su preferencia</i></p>
        </body>
        <script>
            window.print();
        </script>
</html>
