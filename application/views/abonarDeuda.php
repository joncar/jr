<?php if(!empty($msj)): ?>
<?= $msj ?>
<?php endif ?>
<form class="form-horizontal" id='formulario' method="post" action="" role="form" onsubmit="$('button').attr('disabled',true)">
    <div class="row well">
        <div class="col-xs-6">
            Cliente: <b><?= $cliente ?></b>
        </div>
        <div class="col-xs-6" align="right">
            Fecha: <b><?= date("d-m-Y H:i:s") ?></b>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <div align="right">
                Saldo de notas de crédito: <span style="font-size:29px"><b><?= number_format($notas,2,',','.') ?> Gs</b></span>
            </div>
            <div align="right">
                Saldo del cliente: <span style="font-size:29px"><b><?= number_format($saldo,2,',','.') ?> Gs</b></span>
            </div>
            <?php if($saldo>0): ?>
            <div class="form-group">
                <label for="monto" class="col-sm-4 control-label">Monto</label>
                <div class="col-sm-8">
                    <input type="text" name="monto" id="monto" class="form-control" placeholder="Monto a abonar" required="" min="1">
                </div>
            </div>
            <div align="center">
                <button type="submit" class="btn btn-success">Abonar monto</button>
                <a href="<?= base_url() ?>cajas/admin/saldos" class="btn btn-danger">Volver a saldos</a>
            </div>
            <?php else: ?>
            Este cliente no posee deudas, o ya ha cancelado la totalidad de las mismas
            <?php endif ?>
        </div>
        <div class="col-xs-12 col-md-8">
            <b>Cheques diferidos para este cliente</b>
            <?php 
                $query = "
                    SELECT 
                    ventas.nro_factura,
                    cheques_a_cobrar.nro_cheque,                    
                    bancos.denominacion as banco,
                    cheques_a_cobrar.monto,
                    DATE_FORMAT(cheques_a_cobrar.vencimiento,'%d/%m/%Y') as vencimiento,
                    cheques_a_cobrar.cruzado
                    FROM cheques_a_cobrar
                    INNER JOIN ventas ON ventas.id = cheques_a_cobrar.ventas_id
                    INNER JOIN bancos ON bancos.id = cheques_a_cobrar.bancos_id
                    WHERE ventas.cliente = '".$clienteid."' AND cheques_a_cobrar.vencimiento >= DATE(NOW())
                ";
                $query = $this->db->query($query);
                if($query->num_rows()>0){
                    echo sqlToHtml($query);
                }else{
                    echo 'Sin cheques por cobrar para este cliente';
                }
            ?>
        </div>
    </div>
</form>
<script>    
    function imprimir(codigo){
            window.open('<?= base_url('cajas/'.$this->router->fetch_class()) ?>/pagocliente/imprimir/'+codigo);
     }
      function imprimirTicket(codigo){
            window.open('<?= base_url('cajas/'.$this->router->fetch_class()) ?>/pagocliente/imprimirticket/'+codigo);
     }

    <?php if(!empty($_GET['pago'])): ?>
        imprimirTicket(<?= $_GET['pago'] ?>);
    <?php endif ?>
</script>
