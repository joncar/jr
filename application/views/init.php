<body class="login" style="background-image:url(<?= base_url().'img/logos/'.$this->ajustes->fondo ?>) !important; background-size: cover">
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="<?= site_url() ?>">
           <img src="<?= base_url().'img/logos/'.$this->ajustes->logo_login ?>" style=" width: 306px" alt="" />
         </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">        
        <div align="center">
        	<a href="javascript:open()" class="btn btn-success btn-large" style=" width: auto; padding-top: 20px">Entrar en el sistema</a>
        </div>
    </div>
    <div class="copyright"> <?= date("Y") ?> © EVA Software. </div>
    <script src="<?= base_url() ?>js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/additional-methods.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/app.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/login.min.js" type="text/javascript"></script>
</body>
<script>
	function open(){
		window.open('<?= base_url() ?>main/init','','toolbar=no,width='+window.innerWidth+',height='+window.innerHeight)
	}
</script>