<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Instalación de Binario Sysventas
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="kt-section kt-section--first">
                <div class="row">
                	<div class="col-12 col-md-6">
                		<form action="instalacion/instalar" method="post" name="instalacion" onsubmit="instalar(this); return false;">
                			<div class="form-group">
                				<label for="">
                					Dirección Host Servidor de base de datos
                				</label>
                				<input type="text" class="form-control" name="dbhost">
                			</div>
                			<div class="form-group">
                				<label for="">
                					Nombre de base de datos
                				</label>
                				<input type="text" class="form-control" name="dbname">
                			</div>
                			<div class="form-group">
                				<label for="">
                					Usuario de base de datos
                				</label>
                				<input type="text" class="form-control" name="dbuser">
                			</div>
                			<div class="form-group">
                				<label for="">
                					Contraseña de base de datos
                				</label>
                				<input type="text" class="form-control" name="dbpass">
                			</div>
                			<div class="form-group">
                				<button class="btn btn-success" type="submit">Instalar</button>
                			</div>
                		</form>
                	</div>
                	<div class="col-12 col-md-6">
                		<div class="row">
                			<div class="col-12">
                				<h3>LOG</h3>
                			</div>
                			<div class="col-12" id="log" style="height:50vh; overflow-y:auto;border:1px solid #000">
                				
                			</div>
                		</div>
                	</div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>

<script>
	function instalar(f){
		$("#log").html('');		
		sendForm(f,'',function(data){
			$("#log").html(data);
		});
	}
</script>