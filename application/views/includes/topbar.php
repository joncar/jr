<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    <!-- begin:: Header Menu -->

    <!-- Uncomment this to display the close button of the panel
<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
-->
    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
            <ul class="kt-menu__nav ">
                <li class="kt-menu__item  kt-menu__item--open kt-menu__item--here kt-menu__item--submenu kt-menu__item--rel kt-menu__item--open kt-menu__item--here kt-menu__item--active" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                	<a href="javascript:;" class="kt-menu__link kt-menu__toggle">                		
	                	Fecha: <span style="color:blue" id="fechaSistema"><?= date("d/m/Y H:i")  ?></span>&nbsp;
                        Sucursal: <span style="color:blue"><?= @$this->user->sucursalnombre ?></span>&nbsp;
                        Caja: <span style="color:blue"><?= @$this->user->cajanombre ?></span>&nbsp;
                        ID caja: <span style="color:blue"><?= @$this->user->caja ?></span>&nbsp;
                        ID C.Diaria: <span style="color:blue"><?= @$this->user->cajadiaria ?></span> 
                	</a>
                </li>
            </ul>
        </div>
    </div>

    <!-- end:: Header Menu -->

    <!-- begin:: Header Topbar -->
    <div class="kt-header__topbar">

        <!--begin: Search -->

        <!--begin: Search -->
        <div class="kt-header__topbar-item kt-header__topbar-item--search dropdown" id="kt_quick_search_toggle">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                <span class="kt-header__topbar-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24" />
                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
                        </g>
                    </svg>
                </span>
            </div>
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
                <div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" id="kt_quick_search_dropdown">
                    <form method="get" class="kt-quick-search__form">
                        <div class="input-group">
                            <div class="input-group-prepend">
                            	<span class="input-group-text">
                            		<i class="flaticon2-search-1"></i>
                            	</span>
                            </div>
                            <input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
                            <div class="input-group-append">
                            	<span class="input-group-text">
                            		<i class="la la-close kt-quick-search__close"></i>
                            	</span>
                            </div>
                        </div>
                    </form>
                    <div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="325" data-mobile-height="200"></div>
                </div>
            </div>
        </div>

        <!--end: Search -->

        <!--end: Search -->

        <div class="kt-header__topbar-item kt-header__topbar-item--search" id="kt_quick_search_toggle">
            <a href="<?= base_url('panel') ?>" class="kt-header__topbar-wrapper">
                <span class="kt-header__topbar-icon">
                    <i class="fa fa-home"></i>
                    <span class="kt-pulse__ring"></span>
                </span>
            </a>
        </div>

        <!--begin: Notifications -->
        <div class="kt-header__topbar-item dropdown">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px" aria-expanded="true">
                <span class="kt-header__topbar-icon <?php //Si hay notificaciones colocar la clase kt-pulse ?> kt-pulse--brand">
                    <i class="fa fa-bell"></i>
                    <span class="kt-pulse__ring"></span>
                </span>

                <!--
Use dot badge instead of animated pulse effect:
<span class="kt-badge kt-badge--dot kt-badge--notify kt-badge--sm kt-badge--brand"></span>
-->
            </div>
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg">
                <form>

                    <!--begin: Head -->
                    <div class="kt-head kt-head--skin-dark kt-head--fit-x kt-head--fit-b" style="background-image: url(<?php echo base_url() ?>img/notification-bar-bg.jpg)">
                        <h3 class="kt-head__title">
                            Notificaciones
                            &nbsp;
                            <span class="btn btn-success btn-sm btn-bold btn-font-md">0</span>
                        </h3>
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-success kt-notification-item-padding-x" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications" role="tab" aria-selected="true">Alerts</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_events" role="tab" aria-selected="false">Events</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_logs" role="tab" aria-selected="false">Logs</a>
                            </li>
                        </ul>
                    </div>

                    <!--end: Head -->
                    <div class="tab-content">
                        <div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
                            <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                                <!--<a href="#" class="kt-notification__item">
                                    <div class="kt-notification__item-icon">
                                        <i class="flaticon2-line-chart kt-font-success"></i>
                                    </div>
                                    <div class="kt-notification__item-details">
                                        <div class="kt-notification__item-title">
                                            New order has been received
                                        </div>
                                        <div class="kt-notification__item-time">
                                            2 hrs ago
                                        </div>
                                    </div>
                                </a>-->  
                                <div class="kt-grid kt-grid--ver" style="min-height: 200px;">
	                                <div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle">
	                                    <div class="kt-grid__item kt-grid__item--middle kt-align-center">
	                                        All caught up!
	                                        <br>No new notifications.
	                                    </div>
	                                </div>
	                            </div>                              
                            </div>
                        </div>
                        <div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
                            <div class="kt-grid kt-grid--ver" style="min-height: 200px;">
                                <div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle">
                                    <div class="kt-grid__item kt-grid__item--middle kt-align-center">
                                        All caught up!
                                        <br>No new notifications.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                            <div class="kt-grid kt-grid--ver" style="min-height: 200px;">
                                <div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle">
                                    <div class="kt-grid__item kt-grid__item--middle kt-align-center">
                                        All caught up!
                                        <br>No new notifications.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!--end: Notifications -->

        <!--begin: User Bar -->
        <div class="kt-header__topbar-item kt-header__topbar-item--user">
            <?php if($this->user->log): ?>
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                    <div class="kt-header__topbar-user">
                        <span class="kt-header__topbar-welcome kt-hidden-mobile">Hola,</span>
                        <span class="kt-header__topbar-username kt-hidden-mobile"><?php echo $this->user->nombre ?></span>
                        <img class="kt-hidden" alt="Foto de perfil" src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" />

                        <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                        <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?php echo substr($this->user->nombre,0,1).substr($this->user->apellido,0,1) ?></span>
                    </div>
                </div>
                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

                    <!--begin: Head -->
                    <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(<?php echo base_url() ?>img/notification-bar-bg.jpg)">
                        <div class="kt-user-card__avatar">
                            <img alt="Foto de perfil" src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" />

                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                            <span class="kt-hidden kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success"><?php echo substr($this->user->nombre,0,1).substr($this->user->apellido,0,1) ?></span>
                        </div>
                        <div class="kt-user-card__name">
                            <?php echo $this->user->nombre.' '.$this->user->apellido ?>
                        </div>
                        <!--<div class="kt-user-card__badge">
                            <span class="btn btn-success btn-sm btn-bold btn-font-md">23 messages</span>
                        </div>-->
                    </div>

                    <!--end: Head -->

                    <!--begin: Navigation -->
                    <div class="kt-notification">
                        <a href="<?= base_url('panel/selsucursal') ?>" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <i class="ace-icon fa fa-shopping-cart"></i>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Cambiar sucursal
                                </div>
                                <div class="kt-notification__item-time">
                                    Sucursal: <span style="color:blue"><?= @$this->user->sucursalnombre ?></span>
                                    
                                    
                                </div>
                            </div>
                        </a>
                        <a href="<?= base_url('panel/selcaja') ?>" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                               <i class="ace-icon fa fa-shopping-cart"></i>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Cambiar caja
                                </div>
                                <div class="kt-notification__item-time">
                                    ID caja: <span style="color:blue"><?= @$this->user->caja ?></span> 
                                </div>
                            </div>
                        </a>
                        <a href="<?= base_url('panel/selcajadiaria') ?>" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <i class="ace-icon fa fa-shopping-cart"></i>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Cambiar caja diaria
                                </div>
                                <div class="kt-notification__item-time">
                                    ID C.Diaria: <span style="color:blue"><?= @$this->user->cajadiaria ?></span> 
                                </div>
                            </div>
                        </a>
                        <a href="<?= base_url('seguridad/perfil/edit/'.$this->user->id) ?>" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-bold">
                                    Perfil
                                </div>
                                <div class="kt-notification__item-time">
                                    Configuración de la cuenta
                                </div>
                            </div>
                        </a>





                        
                        <div class="kt-notification__custom kt-space-between">
                            <a href="<?php echo base_url('main/unlog'); ?>" class="btn btn-label btn-label-brand btn-sm btn-bold">Salir</a>
                        </div>
                    </div>

                    <!--end: Navigation -->
                </div>
            <?php endif ?>
        </div>

        <!--end: User Bar -->
    </div>

    <!-- end:: Header Topbar -->
</div>

<!-- end:: Header -->