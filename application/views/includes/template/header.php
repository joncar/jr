<header class="top">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header visible-xs visible-sm">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand d-none d-md-block" href="<?= site_url() ?>"><?= img('img/logo.png','width:100px') ?></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav hidden-xs hidden-sm">
              <li><a href="<?= site_url() ?>"><?= img('img/logo.png','width:250px') ?></a></li>
            </ul>                    
            <ul class="nav navbar-nav navbar-right">
              <li><a href="#">PRECIOS</a></li>
              <li><a href="<?= site_url('p/empresas') ?>">PARA EMPRESA</a></li>
              <li><a href="<?= site_url('p/unete') ?>">UNETE A LA RED</a></li>              
              <?php if(empty($_SESSION['user'])): ?>
                <li><a href="<?= site_url('solicitar-servicio') ?>" class="boton-yellow hidden-xs hidden-sm">SOLICITAR SERVICIO</a></li>
                <li><a href="<?= site_url('panel') ?>" class="boton-outlined hidden-xs hidden-sm">INICIAR SESION</a></li>
                <li><a href="<?= site_url('solicitar-servicio') ?>" class="visible-xs visible-sm">SOLICITAR SERVICIO</a></li>
                <li><a href="<?= site_url('panel') ?>" class="visible-xs visible-sm">INICIAR SESION</a></li>
              <?php else: ?>
                <?php if($this->user->admin==1): ?>
                <li><a href="<?= site_url('panel') ?>">CUENTA <?= strtoupper($this->user->nombre) ?></a></li>
                <?php else: ?>
                <li><a href="<?= site_url('pedidos/admin/mispedidos') ?>">MIS SERVICIOS</a></li>
                <?php endif ?>
                <li><a href="<?= site_url('solicitar-servicio') ?>" class="boton-yellow hidden-xs hidden-sm">SOLICITAR SERVICIO</a></li>
                <li><a href="<?= site_url('solicitar-servicio') ?>" class="visible-xs visible-sm">SOLICITAR SERVICIO</a></li>
                <li><a href="<?= site_url('main/unlog') ?>">SALIR</a></li>
              <?php endif ?>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
</header>