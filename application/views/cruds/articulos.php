<?= $output ?>
<?php if(!empty($json)): ?>
<script>
    $(document).on('insert_success',function(){
        window.opener.tryagain();
        window.close();
    });
    
    
     $(document).on('keydown','input',function(event){        
        if (event.which == 13){            
            var inputs = $(this).parents("form").eq(0).find(":input");
            var idx = inputs.index(this);
            if (idx == inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
                inputs[idx + 1].select();
            }
            return false;
        }
    });
</script>
<?php endif ?>