<h2>Seleccione una caja diaria</h2>
<?= $output ?>
<script>
	window.afterLoad.push(function(){
		$(document).on('click','.flexigrid a',function(e){
			if(typeof($(this).data('user'))!='undefined'){
				if($(this).data('user')!=<?= $this->user->id ?> && $(this).data('bloquear')==1){
					e.preventDefault();
					alert('El acceso a esta caja esta restringido solo al que aperturo la caja');
					return;
				}
				if($(this).data('user')!=<?= $this->user->id ?> && !confirm('Esta cajadiaria ha sido aperturada por un usuario diferente, seguro deseas ingresar en esta caja?')){
					e.preventDefault();
				}
			}
		});
	});
</script>