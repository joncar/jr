<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_010 extends CI_Migration {

        public function up()
        {
        	$this->db->query("ALTER TABLE presupuesto_detalle ADD COLUMN IF NOT EXISTS total_adic INT NULL DEFAULT 0 AFTER total;");

        	$this->db->query("ALTER TABLE ventatemp ADD COLUMN IF NOT EXISTS tipo varchar(20) NULL DEFAULT 'ventas';");
        }

        public function down()
        {

        }
}