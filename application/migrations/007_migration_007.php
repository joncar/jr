<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_007 extends CI_Migration {

        public function up()
        {
        	$this->db->query("ALTER TABLE compradetalles ADD COLUMN IF NOT EXISTS precio_venta_ant FLOAT NULL AFTER por_venta; ");
        	$this->db->query("ALTER TABLE compradetalles ADD COLUMN IF NOT EXISTS precio_costo_ant FLOAT NULL AFTER vencimiento; ");
        }

        public function down()
        {

        }
}