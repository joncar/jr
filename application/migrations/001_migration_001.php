<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_001 extends CI_Migration {

        public function up()
        {   
            $this->db->query("ALTER TABLE ajustes ADD COLUMN IF NOT EXISTS cupon_fijo INT(11)");   
        	$this->db->query("ALTER TABLE ajustes ADD COLUMN IF NOT EXISTS imprimir_cupon TINYINT(1) NOT NULL DEFAULT '0' AFTER cupon_fijo");
        	$this->db->query("ALTER TABLE ajustes ADD COLUMN IF NOT EXISTS imprimir_cupon_id INT NOT NULL DEFAULT '0' AFTER imprimir_cupon");
        }

        public function down()
        {	
           $this->db->query("ALTER TABLE ajustes DROP COLUMN IF EXISTS imprimir_cupon");
	       $this->db->query("ALTER TABLE ajustes DROP COLUMN IF EXISTS imprimir_cupon_id");
        }
}