<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_006 extends CI_Migration {

        public function up()
        {
        	$this->db->query("ALTER TABLE `ajustes` CHANGE `cod_balanza` `cod_balanza` VARCHAR(50) NOT NULL; ");
        }

        public function down()
        {

        }
}