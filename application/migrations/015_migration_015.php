<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_015 extends CI_Migration {

        public function up()
        {        	
        	$this->db->query("DROP VIEW IF EXISTS view_empleados");
        	$this->db->query("CREATE VIEW view_empleados AS SELECT empleados.*,user.nombre,user.apellido,user.email,user.cedula FROM empleados INNER JOIN user ON user.id = empleados.user_id");
        	
        }

        public function down()
        {

        }
}