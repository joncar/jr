<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_011 extends CI_Migration {

        public function up()
        {
        	$this->db->query("ALTER TABLE `cajas` ADD COLUMN IF NOT EXISTS `cerrarAlImprimirVenta` BOOLEAN NULL DEFAULT TRUE AFTER `abierto`; ");
        	$this->db->query("ALTER TABLE `cajas` ADD COLUMN IF NOT EXISTS `tipo_facturacion_id` INT NULL DEFAULT 1 AFTER `abierto`; ");
        }

        public function down()
        {

        }
}