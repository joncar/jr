<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_003 extends CI_Migration {

        public function up()
        {              	
        	$this->db->query("
        		CREATE TABLE IF NOT EXISTS otros_ingresos (
				  id int(11) NOT NULL,
				  fecha date NOT NULL,
				  monto int(11) NOT NULL,
				  concepto varchar(255) NOT NULL,
				  forma_pago_id int(11) NOT NULL,
				  anulado tinyint(1) NOT NULL DEFAULT '0'
				) ENGINE=InnoDB;
        	");
        	$this->db->query("ALTER TABLE otros_ingresos ADD PRIMARY KEY (id)");
        	$this->db->query("ALTER TABLE otros_ingresos CHANGE id id INT(11) NOT NULL AUTO_INCREMENT");
        }

        public function down()
        {	           
        }
}