<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_008 extends CI_Migration {

        public function up()
        {
        	$this->db->query("ALTER TABLE productos ADD COLUMN IF NOT EXISTS precio_costo_ant INT NULL AFTER precio_costo;");
        	$this->db->query("ALTER TABLE productos ADD COLUMN IF NOT EXISTS fecha_mod_precio DATETIME NULL AFTER precio_costo; ");
        }

        public function down()
        {

        }
}