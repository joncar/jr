<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_014 extends CI_Migration {

        public function up()
        {
        	$this->db->query("
        		DROP PROCEDURE IF EXISTS ajustSaldoFromVentaCredito;			
        	");

        	$this->db->query("
        		CREATE PROCEDURE ajustSaldoFromVentaCredito(varClienteID INT)
				BEGIN

					DECLARE varTotalPagado INT; ##Cuanto ha pagado
					DECLARE varTotalCreditos INT; ##Cuanto es el total de la deuda
					DECLARE varID INT;
					DECLARE varI INT;
					DECLARE varT INT;	
					DECLARE varLastPay DATETIME;
					DECLARE varPresupuesto INT;
					DECLARE varVenta INT;

					SELECT IFNULL(SUM(monto),0) INTO varTotalPagado 
					FROM facturas_clientes_pagos
					WHERE facturas_clientes = varClienteID AND (anulado IS NULL OR anulado = 0);

					#SE RECORREN LOS CREDITOS Y SE VA AJUSTANDO EL PAGADO = 1
					SET varI:= 0;
					SELECT IFNULL(COUNT(id),0) INTO varT FROM (
						SELECT 
						ventadetalle.id
						FROM facturas_clientes_detalle
						INNER JOIN ventadetalle ON ventadetalle.venta = facturas_clientes_detalle.venta
						INNER JOIN ventas ON ventas.id = ventadetalle.venta AND (ventas.anulado IS NULL OR ventas.anulado = 0)
						WHERE ventadetalle.totalcondesc > 0 AND facturas_clientes_detalle.facturas_clientes = varClienteID AND (facturas_clientes_detalle.anulado IS NULL OR facturas_clientes_detalle.anulado = 0)
						GROUP BY facturas_clientes_detalle.id
						ORDER BY ventas.fecha ASC
					) as facturas;	

					WHILE varI<varT DO

						SELECT 
						facturas_clientes_detalle.id,SUM(ventadetalle.totalcondesc),ventas.presupuestos_id,ventas.id
						INTO varID,varTotalCreditos,varPresupuesto,varVenta
						FROM facturas_clientes_detalle
						INNER JOIN ventadetalle ON ventadetalle.venta = facturas_clientes_detalle.venta
						INNER JOIN ventas ON ventas.id = ventadetalle.venta AND (ventas.anulado IS NULL OR ventas.anulado = 0)
						WHERE ventadetalle.totalcondesc > 0 AND facturas_clientes_detalle.facturas_clientes = varClienteID AND (facturas_clientes_detalle.anulado IS NULL OR facturas_clientes_detalle.anulado = 0)
						GROUP BY facturas_clientes_detalle.id
						ORDER BY ventas.fecha ASC		
						LIMIT varI,1;

						##Se calcula el nuevo saldo
						SET varTotalPagado:= varTotalPagado - varTotalCreditos;
						UPDATE facturas_clientes_detalle 
						SET 
							saldo_venta = IF(varTotalPagado>0,0,varTotalPagado*-1),
							pagado = IF(varTotalPagado>=0,1,0),
							fecha_pagado = IF(fecha_pagado IS NULL AND varTotalPagado>=0,NOW(),fecha_pagado)
						WHERE id = varID;
						SET varTotalPagado:= IF(varTotalPagado<0,0,varTotalPagado);
						
						#Si la venta fue pagada y contiene un presupuesto se cierra el presupuesto			
						

						SET varI:= varI+1;
					END WHILE;

					#Se busca el saldo total del cliente
					SELECT 
					IFNULL(SUM(facturas_clientes_detalle.saldo_venta),0) INTO varTotalCreditos
					FROM facturas_clientes_detalle
					WHERE facturas_clientes_detalle.facturas_clientes = varClienteID;

					#Se busca el ultimo pago del cliente
					SELECT IFNULL(MAX(fecha),NULL) INTO varLastPay 
					FROM facturas_clientes_pagos
					WHERE facturas_clientes = varClienteID AND (anulado IS NULL OR anulado = 0);

					#Se actualiza el resumen general del cliente
					UPDATE facturas_clientes SET saldo_cliente = varTotalCreditos,ultimo_pago=varLastPay WHERE id = varClienteID;

				END
        	");
        	
        }

        public function down()
        {

        }
}