<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_017 extends CI_Migration {

        public function up()
        {   
        	$this->db->query("ALTER TABLE productosucursal ADD COLUMN productos_id INT NULL AFTER `producto`");
                $this->db->query(
                        "UPDATE productosucursal,productos SET productosucursal.productos_id = productos.id WHERE productosucursal.producto = productos.codigo"
                );
        }

        public function down()
        {

        }
}