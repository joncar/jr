<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_002 extends CI_Migration {

        public function up()
        {      
        	$this->db->query("DROP PROCEDURE IF EXISTS reversar_stock_from_ventas");
        	$this->db->query("
        		CREATE PROCEDURE reversar_stock_from_ventas(ventaID INT)
				BEGIN
				DECLARE control_stock INT;
				DECLARE varSucursal INT;
				DECLARE sucursalWeb INT;
				SELECT vender_sin_stock,sucursal_web INTO control_stock,sucursalWeb FROM ajustes limit 1;
				#IF(control_stock=0) THEN
					UPDATE productosucursal,(
					SELECT 
					productosucursal.id,
					ventadetalle.producto,
					ventas.sucursal,
					ventadetalle.cantidad,
					productosucursal.stock
					FROM ventadetalle
					INNER JOIN ventas ON ventas.id = ventadetalle.venta
					INNER JOIN productosucursal ON productosucursal.producto = ventadetalle.producto AND productosucursal.sucursal = ventas.sucursal
					WHERE venta = ventaID) AS venta SET productosucursal.stock = (productosucursal.stock+venta.cantidad) WHERE productosucursal.id = venta.id;
					#Enviamos stock a la web
					SELECT sucursal INTO varSucursal FROM ventas WHERE ventas.id = ventaID;	
					UPDATE creditos set anulado = 1 WHERE ventas_id = ventaID;
					#Enviamos stock a la web
					IF varSucursal = sucursalWeb THEN 
						CALL send_stock_to_wp();
					END IF;
				#END IF;
				END
        	");
        }

        public function down()
        {	           
        }
}