<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_009 extends CI_Migration {

        public function up()
        {
        	$this->db->query("ALTER TABLE presupuesto_detalle ADD COLUMN IF NOT EXISTS anulado INT NULL DEFAULT 0;");
        }

        public function down()
        {

        }
}