<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_013 extends CI_Migration {

        public function up()
        {
        	$this->db->query("
        		CREATE TABLE IF NOT EXISTS `nota_cred_proveedor` (
				  `id` int(11) NOT NULL,
				  `fecha` date NOT NULL,
				  `canjes_id` int(11) NOT NULL,
				  `proveedores_id` int(11) NOT NULL,
				  `compras_id` int(11) NOT NULL,
				  `nro_nota` int(11) DEFAULT NULL,
				  `total` int(11) NOT NULL,
				  `motivo_nota_id` int(11) NOT NULL,
				  `descontar_saldo` tinyint(1) NOT NULL,
				  `sucursal` int(11) NOT NULL,
				  `cajadiaria` int(11) NOT NULL,
				  `usuario` int(11) NOT NULL,
				  `anulado` tinyint(1) DEFAULT '0'
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;				
        	");
        	$this->db->query("
        		CREATE TABLE IF NOT EXISTS `nota_cred_proveedor_detalle` (
				  `id` int(11) NOT NULL,
				  `nota_credito` int(11) NOT NULL,
				  `producto` varchar(255) NOT NULL,
				  `cantidad` int(11) NOT NULL,
				  `precio_costo` int(11) NOT NULL,
				  `total` int(11) NOT NULL,
				  `vencimiento` date NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;				
        	");
        	$this->db->query("
        		ALTER TABLE `nota_cred_proveedor`
				  ADD PRIMARY KEY (`id`);
        	");
        	$this->db->query("        		
				ALTER TABLE `nota_cred_proveedor_detalle`
				  ADD PRIMARY KEY (`id`);				
        	");
        	$this->db->query("
        		ALTER TABLE `nota_cred_proveedor`
				  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;			    
        	");
        	$this->db->query("        		
			    ALTER TABLE `nota_cred_proveedor_detalle`
				  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
        	");

        	$this->db->query("        		
			    ALTER TABLE `pagoproveedor_detalles` ADD COLUMN IF NOT EXISTS total_factura INT NOT NULL AFTER fecha_factura
        	");

        	$this->db->query("        		
			    ALTER TABLE `pagoproveedor_detalles` ADD COLUMN IF NOT EXISTS monto_abonado INT NOT NULL AFTER nota_credito
        	");

        	$this->db->query("
        		ALTER TABLE `libro_banco` CHANGE `forma_pago` `forma_pago` VARCHAR(255) NOT NULL; 
        	");

        	$this->db->query("
        		ALTER TABLE `libro_banco` CHANGE `anulado` `anulado` INT(11) NULL DEFAULT '0', CHANGE `quien_anulo` `quien_anulo` INT(11) NULL, CHANGE `fecha_anulado` `fecha_anulado` DATE NULL; 
        	");

                $this->db->query("
                        ALTER TABLE `libro_banco` CHANGE `tipo_cheque` `tipo_cheque` INT(11) NULL COMMENT '1-vista -1-diferido'; 
                ");

                $this->db->query("
                        DROP FUNCTION IF EXISTS getSaldoCuenta;
                ");

                $this->db->query("
                        CREATE FUNCTION getSaldoCuenta(varCuenta INT) RETURNS INT
                        BEGIN
                            DECLARE varSaldo INT;
                            SELECT 
                            SUM((libro_banco.tipo_movimiento*libro_banco.monto))
                            INTO 
                            varSaldo
                            FROM cuentas_bancos
                            LEFT JOIN libro_banco ON libro_banco.cuentas_bancos_id = cuentas_bancos.id
                            WHERE cuentas_bancos.id = varCuenta AND
                            (
                                forma_pago = 'Transferencia' OR
                                forma_pago = 'Efectivo' OR
                                forma_pago = 'Deposito' OR
                                forma_pago = 'Cheque' AND libro_banco.tipo_cheque = '1' OR
                                forma_pago = 'Cheque' AND libro_banco.tipo_cheque = '-1' AND libro_banco.fecha_pago>=DATE(NOW())
                            ) AND libro_banco.anulado = 0;

                            RETURN IFNULL(varSaldo,0);
                        END
                ");

                $this->db->query("
                    CREATE TRIGGER `libro_banco_ainsert` AFTER INSERT ON `libro_banco` FOR EACH ROW BEGIN UPDATE `cuentas_bancos` SET saldo = getSaldoCuenta(cuentas_bancos.id) WHERE cuentas_bancos.id = NEW.cuentas_bancos_id; END 
                ");

                $this->db->query("
                    CREATE TRIGGER `libro_banco_aupdate` AFTER UPDATE ON `libro_banco` FOR EACH ROW BEGIN UPDATE `cuentas_bancos` SET saldo = getSaldoCuenta(cuentas_bancos.id) WHERE cuentas_bancos.id = NEW.cuentas_bancos_id; IF NEW.cuentas_bancos_id != OLD.cuentas_bancos_id THEN UPDATE `cuentas_bancos` SET saldo = getSaldoCuenta(cuentas_bancos.id) WHERE cuentas_bancos.id = OLD.cuentas_bancos_id; END IF; END 
                ");

                $this->db->query("
                    ALTER TABLE `libro_banco` CHANGE `anulado` `anulado` BOOLEAN NULL DEFAULT FALSE; 
                ");
        	
        }

        public function down()
        {

        }
}