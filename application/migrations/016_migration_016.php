<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_016 extends CI_Migration {

        public function up()
        {   
        	$this->db->query("ALTER TABLE productos ADD COLUMN IF NOT EXISTS codigo2 VARCHAR(50) NULL AFTER `codigo`; ");
        }

        public function down()
        {

        }
}