<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_005 extends CI_Migration {

        public function up()
        {
        	$this->db->query("
        		CREATE TABLE IF NOT EXISTS ventatemp (
                          productos longtext NOT NULL,
                          cajas_id int(11) NOT NULL,
                          user_id int(11) NOT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        	");        	

          $this->db->query("ALTER TABLE user ADD COLUMN IF NOT EXISTS token VARCHAR(255) NULL; ");
          $this->db->query("ALTER TABLE ajustes ADD COLUMN IF NOT EXISTS solicitar_token_ventas BOOLEAN NULL DEFAULT 0");
        }

        public function down()
        {

        }
}