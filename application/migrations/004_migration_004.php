<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Migration_004 extends CI_Migration {

        public function up()
        {
        	$this->db->query("ALTER TABLE ventas ADD COLUMN IF NOT EXISTS plan BOOLEAN NOT NULL DEFAULT FALSE AFTER transaccion");
        	$this->db->query("ALTER TABLE ajustes ADD COLUMN IF NOT EXISTS permitir_descuento BOOLEAN DEFAULT TRUE");
        	$this->db->query("ALTER TABLE productos ADD COLUMN IF NOT EXISTS porc_venta FLOAT DEFAULT 0 AFTER descuentos");
        	$this->db->query("ALTER TABLE productos ADD COLUMN IF NOT EXISTS porc_mayorista1 FLOAT DEFAULT 0 AFTER precio_venta_mayorista1");
        	$this->db->query("ALTER TABLE productos ADD COLUMN IF NOT EXISTS porc_mayorista2 FLOAT DEFAULT 0 AFTER precio_venta_mayorista2");
        	$this->db->query("ALTER TABLE productos ADD COLUMN IF NOT EXISTS porc_mayorista3 FLOAT DEFAULT 0 AFTER precio_venta_mayorista3");
        }

        public function down()
        {

        }
}