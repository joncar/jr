DROP TRIGGER IF EXISTS `presupuesto_detalle_after_insert`;
DELIMITER $$
CREATE TRIGGER `presupuesto_detalle_after_insert` AFTER INSERT ON `presupuesto_detalle` FOR EACH ROW 
BEGIN 
    DECLARE varDescontar INT; 
    SELECT descontar_prespuesto INTO varDescontar FROM ajustes;
    
    IF varDescontar = 1 THEN 
        CALL ajust_stock_from_presupuesto_detalle(NEW.id); 
    END IF; 
END$$