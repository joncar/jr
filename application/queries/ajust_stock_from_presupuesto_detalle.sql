#función para actualizar stock desde after_insert_presupuesto_detalle ID de presupuesto_detalle
#PARAMS presupuesto_detalle INT
DROP PROCEDURE IF EXISTS  ajust_stock_from_presupuesto_detalle;
DELIMITER $$
CREATE PROCEDURE  ajust_stock_from_presupuesto_detalle(presupuesto_detalle INT)
BEGIN
DECLARE varProduct VARCHAR(255);
DECLARE varSucursal INT;
DECLARE varProductoSucursalId INT;
DECLARE varCantidad DECIMAL(11,2);
DECLARE varVencimiento DATE;
DECLARE control_stock INT;
DECLARE controlar_venc INT;
DECLARE varAsociados INT;
DECLARE varX INT;
DECLARE varAsociadosP VARCHAR(255);#productos_asociados.producto
DECLARE varAsociadosC INT;#productos_asociados.cantidad
DECLARE varAsociadosS INT;#Productos-sucursal.id
SELECT vender_sin_stock,controlar_vencimiento_stock INTO control_stock,controlar_venc FROM ajustes limit 1;
#IF(control_stock=0) THEN	
	#Traemos el id de stock
	IF controlar_venc = 0 THEN
		#Traemos el producto
		SELECT producto,presupuesto.sucursal,presupuesto_detalle.cantidad INTO varProduct,varSucursal,varCantidad FROM presupuesto_detalle INNER JOIN presupuesto ON presupuesto.id = presupuesto_detalle.presupuesto_id WHERE presupuesto_detalle.id = presupuesto_detalle LIMIT 1;
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
	ELSE
		SELECT producto,presupuesto.sucursal,presupuesto_detalle.cantidad,presupuesto_detalle.vencimiento INTO varProduct,varSucursal,varCantidad,varVencimiento FROM presupuesto_detalle INNER JOIN presupuesto ON presupuesto.id = presupuesto_detalle.presupuesto_id WHERE presupuesto_detalle.id = presupuesto_detalle LIMIT 1;
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal AND vencimiento = varVencimiento LIMIT 1;
	END IF;
	IF varProductoSucursalId IS NULL THEN
		#Insertamos el producto para comenzar su stockage
		IF controlar_venc = 0 THEN
			INSERT INTO productosucursal (id,proveedor_id,fechaalta,producto,sucursal,lote,vencimiento,precio_venta,precio_costo,stock) SELECT NULL, proveedor_id, NOW(), codigo, varSucursal, NULL, NULL, precio_venta, precio_costo, 0 FROM productos WHERE BINARY codigo = BINARY varProduct;
			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		ELSE
			INSERT INTO productosucursal (id,proveedor_id,fechaalta,producto,sucursal,lote,vencimiento,precio_venta,precio_costo,stock) SELECT NULL, proveedor_id, NOW(), codigo, varSucursal, NULL, varVencimiento, precio_venta, precio_costo, 0 FROM productos WHERE BINARY codigo = BINARY varProduct;
			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal AND vencimiento = varVencimiento LIMIT 1;
		END IF;
	END IF;

	#Actualizamos el stock
	#Si no e suna orden se descuenta el stock
	
	UPDATE productosucursal SET stock = (stock-varCantidad) WHERE productosucursal.id = varProductoSucursalId;
	#Descontamos asociados
	SELECT IFNULL(COUNT(producto_asociado.id),0) INTO varAsociados FROM producto_asociado INNER JOIN productos AS ori ON ori.id = producto_asociado.productos_id INNER JOIN productos ON productos.id = producto_asociado.descontar WHERE BINARY ori.codigo = BINARY varProduct;
	IF varAsociados > 0 THEN
		SET varX:= 0;
		WHILE varX<varAsociados DO 
			SET varAsociadosS:= NULL;			
			IF controlar_venc = 0 THEN
				SELECT productos.codigo,producto_asociado.cant INTO varAsociadosP,varAsociadosC FROM producto_asociado INNER JOIN productos AS ori ON ori.id = producto_asociado.productos_id INNER JOIN productos ON productos.id = producto_asociado.descontar WHERE BINARY ori.codigo = BINARY varProduct LIMIT varX,1;	
				SELECT id INTO varAsociadosS FROM productosucursal WHERE BINARY producto = BINARY varAsociadosP AND sucursal = varSucursal LIMIT 1;
				IF varAsociadosS IS NULL THEN
					##SI no existe el id en productosucursal del asociado lo creamos
					INSERT INTO productosucursal (id,proveedor_id,fechaalta,producto,sucursal,lote,vencimiento,precio_venta,precio_costo,stock) SELECT NULL, proveedor_id, NOW(), codigo, varSucursal, NULL, NULL, precio_venta, precio_costo, 0 FROM productos WHERE BINARY codigo = BINARY varAsociadosP;
					SELECT id INTO varAsociadosS FROM productosucursal WHERE BINARY producto = BINARY varAsociadosP AND sucursal = varSucursal LIMIT 1;
				END IF;
			ELSE
				SELECT productos.codigo,producto_asociado.cant,IFNULL(producto_asociado.vencimiento,'0000-00-00') INTO varAsociadosP,varAsociadosC,varVencimiento FROM producto_asociado INNER JOIN productos AS ori ON ori.id = producto_asociado.productos_id INNER JOIN productos ON productos.id = producto_asociado.descontar WHERE BINARY ori.codigo = BINARY varProduct LIMIT varX,1;	
				SELECT id INTO varAsociadosS FROM productosucursal WHERE BINARY producto = BINARY varAsociadosP AND sucursal = varSucursal AND vencimiento = varVencimiento LIMIT 1;
				IF varAsociadosS IS NULL THEN
					##SI no existe el id en productosucursal del asociado lo creamos
					INSERT INTO productosucursal (id,proveedor_id,fechaalta,producto,sucursal,lote,vencimiento,precio_venta,precio_costo,stock) SELECT NULL, proveedor_id, NOW(), codigo, varSucursal, NULL, varVencimiento, precio_venta, precio_costo, 0 FROM productos WHERE BINARY codigo = BINARY varAsociadosP;
					SELECT id INTO varAsociadosS FROM productosucursal WHERE BINARY producto = BINARY varAsociadosP AND sucursal = varSucursal AND vencimiento = varVencimiento LIMIT 1;
				END IF;
			END IF;
			#Actualizamos el stock
			UPDATE productosucursal SET stock = (stock-varAsociadosC) WHERE productosucursal.id = varAsociadosS;
			SET varX:= varX+1;
		END WHILE;
	END IF;
END