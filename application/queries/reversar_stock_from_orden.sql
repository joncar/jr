#CALL reversar_stock_from_ventas after update and anulado = 1 ventaID
DROP PROCEDURE IF EXISTS reversar_stock_from_orden;
DELIMITER /
CREATE PROCEDURE reversar_stock_from_orden(ordenID INT)
BEGIN
DECLARE control_stock INT;
DECLARE varSucursal INT;
DECLARE sucursalWeb INT;
DECLARE varOrden INT;

SELECT vender_sin_stock,sucursal_web INTO control_stock,sucursalWeb FROM ajustes limit 1;
#IF(control_stock=0) THEN
	UPDATE productosucursal,(
	SELECT 
	productosucursal.id,
	orden_trabajo_detalle.producto,
	orden_trabajo.sucursal,
	orden_trabajo_detalle.cantidad,
	productosucursal.stock
	FROM orden_trabajo_detalle
	INNER JOIN orden_trabajo ON orden_trabajo.id = orden_trabajo_detalle.orden_trabajo_id
	INNER JOIN productosucursal ON BINARY productosucursal.producto = BINARY orden_trabajo_detalle.producto AND productosucursal.sucursal = orden_trabajo.sucursal
	WHERE orden_trabajo_id = ordenID) AS orden 
	SET productosucursal.stock = (productosucursal.stock+orden.cantidad) 
	WHERE productosucursal.id = orden.id;
#END IF;
END