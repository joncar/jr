#CALL reversar_stock_from_salidas after update and anulado = 1 salidaID
DROP PROCEDURE IF EXISTS reversar_stock_from_salidas;
DELIMITER /
CREATE PROCEDURE reversar_stock_from_salidas(salidaID INT)
BEGIN
DECLARE control_stock INT;
DECLARE varSucursal INT;
DECLARE sucursalWeb INT;
SELECT vender_sin_stock,sucursal_web INTO control_stock,sucursalWeb FROM ajustes limit 1;
#IF(control_stock=0) THEN
	UPDATE productosucursal,(SELECT 
	productosucursal.id,
	salida_detalle.producto,
	salida_detalle.sucursal,
	salida_detalle.cantidad,
	productosucursal.stock
	FROM salida_detalle	
	INNER JOIN productosucursal ON productosucursal.producto = salida_detalle.producto AND productosucursal.sucursal = salida_detalle.sucursal
	WHERE salida_detalle.id = salidaID) AS salida SET productosucursal.stock = (productosucursal.stock+salida.cantidad) WHERE productosucursal.id = salida.id;
	#Enviamos stock a la web
	SELECT sucursal INTO varSucursal FROM salida_detalle WHERE salida_detalle.id = salidaID;	
	#Enviamos stock a la web
	IF varSucursal = sucursalWeb THEN 
		CALL send_stock_to_wp();
	END IF;
#END IF;
END