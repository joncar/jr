#función para actualizar stock desde after_insert_transferencias_detalles ID de compradetalles
#ajust_stock_from_transferencias_detalles PARAMS TransferenciasDetalle INT
DROP PROCEDURE IF EXISTS ajust_stock_from_transferencias_detalles;
DELIMITER /
CREATE PROCEDURE ajust_stock_from_transferencias_detalles(TransferenciasDetalle INT)
BEGIN
DECLARE varProduct VARCHAR(255);
DECLARE varSucursal INT;
DECLARE varProductoSucursalId INT;
DECLARE varCantidad DECIMAL(11,2);
DECLARE controlar_venc INT;
DECLARE varVencimiento DATE;

DECLARE varProduct2 VARCHAR(255);
DECLARE varSucursal2 INT;
DECLARE varProductoSucursalId2 INT;
DECLARE varCantidad2 DECIMAL(11,2);

DECLARE control_stock INT;
SELECT vender_sin_stock,controlar_vencimiento_stock INTO control_stock,controlar_venc FROM ajustes limit 1;
#IF(control_stock=0) THEN
	##SUCURSAL ORIGEN
	IF controlar_venc = 0 THEN
		#Traemos el producto
		SELECT transferencias_detalles.producto,transferencias.sucursal_origen,transferencias_detalles.cantidad INTO varProduct,varSucursal,varCantidad FROM transferencias_detalles INNER JOIN transferencias ON transferencias.id = transferencias_detalles.transferencia WHERE transferencias_detalles.id = TransferenciasDetalle LIMIT 1;
		#Traemos el id de stock
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
	ELSE
		#Traemos el producto
		SELECT transferencias_detalles.producto,transferencias.sucursal_origen,transferencias_detalles.cantidad,transferencias_detalles.vencimiento INTO varProduct,varSucursal,varCantidad,varVencimiento FROM transferencias_detalles INNER JOIN transferencias ON transferencias.id = transferencias_detalles.transferencia WHERE transferencias_detalles.id = TransferenciasDetalle LIMIT 1;
		#Traemos el id de stock
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal AND vencimiento = varVencimiento LIMIT 1;
	END IF;
	IF varProductoSucursalId IS NULL THEN
		#Insertamos el producto para comenzar su stockage
		IF controlar_venc = 0 THEN
			INSERT INTO productosucursal 
			(
				id,
				proveedor_id,
				fechaalta,
				producto,
				productos_id ,
				sucursal,
				lote,
				vencimiento,
				precio_venta,
				precio_costo,
				stock,
				precio_venta_mayorista1,
				cant_1,
				porc_mayorista2,
				precio_venta_mayorista2,
				cant_2,
				porc_mayorista3,
				precio_venta_mayorista3,
				cant_3
			)
			SELECT 
			NULL, 
			proveedor_id, 
			NOW(), 
			codigo, 
			varSucursal, 
			NULL, 
			NULL, 
			precio_venta, 
			precio_costo,
			0,
			precio_venta_mayorista1,
			cant_1,
			porc_mayorista2,
			precio_venta_mayorista2,
			cant_2,
			porc_mayorista3,
			precio_venta_mayorista3,
			cant_3
			FROM productos 
			WHERE BINARY codigo = BINARY varProduct;
			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		ELSE
			INSERT INTO productosucursal 
			(
				id,
				proveedor_id,
				fechaalta,
				producto,
				productos_id ,
				sucursal,
				lote,
				vencimiento,
				precio_venta,
				precio_costo,
				stock,
				precio_venta_mayorista1,
				cant_1,
				porc_mayorista2,
				precio_venta_mayorista2,
				cant_2,
				porc_mayorista3,
				precio_venta_mayorista3,
				cant_3
			)
			SELECT 
			NULL, 
			proveedor_id, 
			NOW(), 
			codigo, 
			varSucursal2, 
			NULL, 
			varVencimiento, 
			precio_venta, 
			precio_costo,
			0,
			precio_venta_mayorista1,
			cant_1,
			porc_mayorista2,
			precio_venta_mayorista2,
			cant_2,
			porc_mayorista3,
			precio_venta_mayorista3,
			cant_3
			FROM productos 
			WHERE BINARY codigo = BINARY varProduct;
			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal AND vencimiento = varVencimiento LIMIT 1;
		END IF;
	END IF;

	#Actualizamos el stock
	UPDATE productosucursal SET stock = (stock-varCantidad) WHERE productosucursal.id = varProductoSucursalId;


	##SUCURSAL DESTINO
	IF controlar_venc = 0 THEN
		#Traemos el producto
		SELECT transferencias_detalles.producto,transferencias.sucursal_destino,transferencias_detalles.cantidad INTO varProduct2,varSucursal2,varCantidad2 FROM transferencias_detalles INNER JOIN transferencias ON transferencias.id = transferencias_detalles.transferencia WHERE transferencias_detalles.id = TransferenciasDetalle LIMIT 1;
		#Traemos el id de stock
		SELECT id INTO varProductoSucursalId2 FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal2 LIMIT 1;
	ELSE
		#Traemos el producto
		SELECT transferencias_detalles.producto,transferencias.sucursal_destino,transferencias_detalles.cantidad,transferencias_detalles.vencimiento INTO varProduct2,varSucursal2,varCantidad2,varVencimiento FROM transferencias_detalles INNER JOIN transferencias ON transferencias.id = transferencias_detalles.transferencia WHERE transferencias_detalles.id = TransferenciasDetalle LIMIT 1;
		#Traemos el id de stock
		SELECT id INTO varProductoSucursalId2 FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal2 AND vencimiento = varVencimiento LIMIT 1;
	END IF;
	IF varProductoSucursalId2 IS NULL THEN
		IF controlar_venc = 0 THEN
			#Insertamos el producto para comenzar su stockage
			INSERT INTO productosucursal 
			(
				id,
				proveedor_id,
				fechaalta,
				producto,
				sucursal,
				lote,
				vencimiento,
				precio_venta,
				precio_costo,
				stock,
				precio_venta_mayorista1,
				cant_1,
				porc_mayorista2,
				precio_venta_mayorista2,
				cant_2,
				porc_mayorista3,
				precio_venta_mayorista3,
				cant_3
			)
			SELECT 
			NULL, 
			proveedor_id, 
			NOW(), 
			codigo, 
			varSucursal2, 
			NULL, 
			NULL, 
			precio_venta, 
			precio_costo,
			0,
			precio_venta_mayorista1,
			cant_1,
			porc_mayorista2,
			precio_venta_mayorista2,
			cant_2,
			porc_mayorista3,
			precio_venta_mayorista3,
			cant_3
			FROM productos 
			WHERE BINARY codigo = BINARY varProduct;
			SELECT id INTO varProductoSucursalId2 FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal2 LIMIT 1;
		ELSE
			#Insertamos el producto para comenzar su stockage
			INSERT INTO productosucursal 
			(
				id,
				proveedor_id,
				fechaalta,
				producto,
				sucursal,
				lote,
				vencimiento,
				precio_venta,
				precio_costo,
				stock,
				precio_venta_mayorista1,
				cant_1,
				porc_mayorista2,
				precio_venta_mayorista2,
				cant_2,
				porc_mayorista3,
				precio_venta_mayorista3,
				cant_3
			)
			SELECT 
			NULL, 
			proveedor_id, 
			NOW(), 
			codigo, 
			varSucursal2, 
			NULL, 
			varVencimiento, 
			precio_venta, 
			precio_costo,
			0,
			precio_venta_mayorista1,
			cant_1,
			porc_mayorista2,
			precio_venta_mayorista2,
			cant_2,
			porc_mayorista3,
			precio_venta_mayorista3,
			cant_3
			FROM productos 
			WHERE BINARY codigo = BINARY varProduct;
			SELECT id INTO varProductoSucursalId2 FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal2 AND vencimiento = vencimiento LIMIT 1;
		END IF;
	END IF;

	#Actualizamos el stock
	UPDATE productosucursal SET stock = (stock+varCantidad2) WHERE productosucursal.id = varProductoSucursalId2;

	#Enviamos stock a la web
	CALL send_stock_to_wp();
#END IF;
END