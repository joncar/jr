#función para actualizar stock desde after_insert_ventadetalle ID de ventadetalle
#PARAMS VentaDetalle INT
DROP PROCEDURE IF EXISTS  reversar_fraccionamiento;
DELIMITER //
CREATE PROCEDURE reversar_fraccionamiento(varFracc INT,newAnulado INT,oldAnulado INT)
BEGIN
	IF newAnulado = 1 AND (oldAnulado = 0 OR oldAnulado IS NULL) THEN
		#Reversamos
		#Sumamos Padre		
		UPDATE productosucursal,(
			SELECT 		
				productosucursal.id,
				(productosucursal.stock + fraccionamientos.parent_cant) as newStock
			FROM fraccionamientos
			INNER JOIN productosucursal ON productosucursal.id = fraccionamientos.productosucursal
			WHERE fraccionamientos.id = varFracc
		) as cd
		SET productosucursal.stock = cd.newStock
		WHERE productosucursal.id = cd.id;
	#Restar hijos
		#Actualizamos stock en hijo
		UPDATE productosucursal, (
			SELECT 
			hijo.id,
	        (hijo.stock - fraccionamientos.cantidad) as newStock
			FROM fraccionamientos
			INNER JOIN productos ON productos.codigo = fraccionamientos.producto
			INNER JOIN productosucursal AS padre ON padre.id = fraccionamientos.productosucursal
			INNER JOIN productosucursal AS hijo ON BINARY hijo.producto = BINARY fraccionamientos.producto AND hijo.sucursal = padre.sucursal AND hijo.vencimiento = padre.vencimiento AND hijo.id != padre.id
			WHERE fraccionamientos.id = varFracc
		) as cd
        SET productosucursal.stock = cd.newStock
        WHERE productosucursal.id = cd.id;
	END IF;	
END 
//
DELIMITER ;
DROP TRIGGER IF EXISTS fraccionamientos_aupdate;
CREATE TRIGGER fraccionamientos_aupdate AFTER UPDATE ON `fraccionamientos` FOR EACH ROW CALL reversar_fraccionamiento(NEW.id,NEW.anulado,OLD.anulado);