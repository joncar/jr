DROP FUNCTION IF EXISTS get_saldo_compra;
DELIMITER $$
CREATE FUNCTION get_saldo_compra (varCompra INT) RETURNS INT
BEGIN
	DECLARE varSaldo INT;

	SELECT 		
	    (total_compra - (total_pagado+total_nota)) INTO varSaldo
	FROM(
	    SELECT 
	    compras.id,
	    compras.nro_factura,
	    SUM(compradetalles.total) as total_compra,
	    IFNULL(pagos.pagado,0) as total_pagado,
	    IFNULL(notas.pagado,0) as total_nota
	    FROM compras 
	    INNER JOIN compradetalles ON compradetalles.compra = compras.id
	    LEFT JOIN (
	        SELECT 
	            compra,
	            SUM(total) AS pagado
	        FROM pagoproveedor_detalles
	        GROUP BY compra
	    ) as pagos ON pagos.compra = compras.id
	    LEFT JOIN (
	        SELECT 
	            nota_cred_proveedor.compras_id as compra,
	            SUM(nota_cred_proveedor_detalle.total) AS pagado
	        FROM nota_cred_proveedor_detalle
	        INNER JOIN nota_cred_proveedor ON nota_cred_proveedor.id = nota_cred_proveedor_detalle.nota_credito
	        GROUP BY compra
	    ) as notas ON notas.compra = compras.id
	    WHERE compras.id = varCompra
	    GROUP BY compras.id
	) as saldo_proveedor;

	RETURN varSaldo;
END $$