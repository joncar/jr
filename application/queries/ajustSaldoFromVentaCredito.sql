#función para actualizar stock desde after_insert_compradetalles ID de compradetalles
#ajust_stock_from_compradetalles PARAMS CompraDetalle INT
DROP PROCEDURE IF EXISTS ajustSaldoFromVentaCredito;
DELIMITER $$
CREATE PROCEDURE ajustSaldoFromVentaCredito(varClienteID INT)
BEGIN

	DECLARE varTotalPagado INT; ##Cuanto ha pagado
	DECLARE varTotalCreditos INT; ##Cuanto es el total de la deuda
	DECLARE varID INT;
	DECLARE varI INT;
	DECLARE varT INT;	
	DECLARE varLastPay DATETIME;
	DECLARE varPresupuesto INT;
	DECLARE varVenta INT;
	DECLARE varNotas INT;
	DECLARE varUltimoPago DATE;

	SELECT IFNULL(SUM(monto),0),MAX(DATE(facturas_clientes_pagos.fecha)) INTO varTotalPagado,varUltimoPago
	FROM facturas_clientes_pagos
	WHERE facturas_clientes = varClienteID AND (anulado IS NULL OR anulado = 0) AND (pagare = 0 OR pagare IS NULL);

	#Se consultan las notas
	SELECT 
	IFNULL(SUM(notas_credito_cliente.total_monto),0)
	INTO varNotas 
	FROM facturas_clientes_detalle
	INNER JOIN facturas_clientes ON facturas_clientes.id = facturas_clientes_detalle.facturas_clientes
	INNER JOIN notas_credito_cliente ON notas_credito_cliente.cliente = facturas_clientes.clientes_id AND (notas_credito_cliente.anulado = 0 OR notas_credito_cliente.anulado IS NULL)
	INNER JOIN ventas ON ventas.id = notas_credito_cliente.venta
	WHERE facturas_clientes = varClienteID AND ventas.transaccion=2 AND ventas.pagare = 0;

	#SET varTotalPagado:= varTotalPagado+varNotas;
	#SE RECORREN LOS CREDITOS Y SE VA AJUSTANDO EL PAGADO = 1
	SET varI:= 0;
	SELECT IFNULL(COUNT(id),0) INTO varT FROM (
		SELECT 
		ventadetalle.id
		FROM facturas_clientes_detalle
		INNER JOIN ventadetalle ON ventadetalle.venta = facturas_clientes_detalle.venta
		INNER JOIN ventas ON ventas.id = ventadetalle.venta AND (ventas.anulado IS NULL OR ventas.anulado = 0)
		WHERE ventadetalle.totalcondesc > 0 AND 
		facturas_clientes_detalle.pagare = 0 AND
		facturas_clientes_detalle.facturas_clientes = varClienteID AND 
		(facturas_clientes_detalle.anulado IS NULL OR facturas_clientes_detalle.anulado = 0)
		GROUP BY facturas_clientes_detalle.id
		ORDER BY ventas.fecha ASC
	) as facturas;	

	WHILE varI<varT DO

		SELECT 
		facturas_clientes_detalle.id,SUM(ventadetalle.totalcondesc),ventas.presupuestos_id,ventas.id
		INTO varID,varTotalCreditos,varPresupuesto,varVenta
		FROM facturas_clientes_detalle
		INNER JOIN ventadetalle ON ventadetalle.venta = facturas_clientes_detalle.venta
		INNER JOIN ventas ON ventas.id = ventadetalle.venta AND (ventas.anulado IS NULL OR ventas.anulado = 0)
		WHERE ventadetalle.totalcondesc > 0 AND 
		facturas_clientes_detalle.facturas_clientes = varClienteID AND 
		facturas_clientes_detalle.pagare = 0 AND
		(facturas_clientes_detalle.anulado IS NULL OR facturas_clientes_detalle.anulado = 0)
		GROUP BY facturas_clientes_detalle.id
		ORDER BY ventas.fecha ASC		
		LIMIT varI,1;

		

		#Se consultan las notas
		SET varNotas:= 0;
		SELECT total_monto INTO varNotas FROM notas_credito_cliente 
		WHERE venta = varVenta AND notas_credito_cliente.anulado = 0;
		
		##Se calcula el nuevo saldo 
		SELECT varTotalPagado + varNotas - varTotalCreditos INTO varTotalPagado;		
		UPDATE facturas_clientes_detalle 
		SET 
			saldo_venta = IF(varTotalPagado>0,0,varTotalPagado*-1),
			total_pagado = varTotalPagado,
			total_venta = varTotalCreditos,
			total_notas = varNotas,
			pagado = IF(varTotalPagado>=0,1,0),
			fecha_pagado = IF(fecha_pagado IS NULL AND varTotalPagado>=0,NOW(),fecha_pagado)
		WHERE id = varID;
		#SET varTotalPagado:= varTotalPagado - varTotalCreditos;
		SET varTotalPagado:= IF(varTotalPagado<0,0,varTotalPagado);
		
		#Si la venta fue pagada y contiene un presupuesto se cierra el presupuesto					

		SET varI:= varI+1;
	END WHILE;

	#Se busca el saldo total del cliente
	#SELECT 
	#IFNULL(SUM(facturas_clientes_detalle.saldo_venta),0) INTO varTotalCreditos
	#FROM facturas_clientes_detalle
	#WHERE facturas_clientes_detalle.facturas_clientes = varClienteID;

	#Se busca el ultimo pago del cliente
	SELECT IFNULL(MAX(fecha),NULL) INTO varLastPay 
	FROM facturas_clientes_pagos
	WHERE facturas_clientes = varClienteID AND (anulado IS NULL OR anulado = 0) AND (pagare = 0 OR pagare IS NULL);

	#Se actualiza el resumen general del cliente
	UPDATE facturas_clientes SET saldo_cliente = get_saldo(clientes_id),ultimo_pago=varLastPay WHERE id = varClienteID;

END $$

DELIMITER ;
CALL ajustSaldoFromVentaCredito(23);
CALL ajustSaldoFromVentaCredito(22);
SELECT * FROM `facturas_clientes_detalle` WHERE 1