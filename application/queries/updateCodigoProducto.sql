DROP PROCEDURE IF EXISTS updateCodigoProducto;
DELIMITER __
CREATE PROCEDURE updateCodigoProducto (varOld VARCHAR(25),varNew VARCHAR(25)) 
BEGIN

	UPDATE compradetalles SET producto = varNew WHERE BINARY producto = BINARY varOld;
	UPDATE entrada_productos_detalles SET producto = varNew WHERE BINARY producto = BINARY varOld;
	UPDATE fraccionamientos SET parent = varNew WHERE BINARY parent = BINARY varOld;
	UPDATE fraccionamientos SET producto = varNew WHERE BINARY producto = BINARY varOld;
	UPDATE notas_credito_cliente_detalle SET producto = varNew WHERE BINARY producto = BINARY varOld;
	UPDATE notas_credito_detalles SET producto = varNew WHERE BINARY producto = BINARY varOld;
	UPDATE notas_credito_proveedor_detalle SET producto = varNew WHERE BINARY producto = BINARY varOld;
	UPDATE presupuesto_detalle SET producto = varNew WHERE BINARY producto = BINARY varOld;
	#UPDATE productos SET parent_codigo = varNew WHERE BINARY parent_codigo = BINARY varOld;
	UPDATE productosucursal SET producto = varNew WHERE BINARY producto = BINARY varOld;
	UPDATE salida_detalle SET producto = varNew WHERE BINARY producto = BINARY varOld;
	UPDATE transferencias_detalles SET producto = varNew WHERE BINARY producto = BINARY varOld;
	UPDATE ventadetalle SET producto = varNew WHERE BINARY producto = BINARY varOld;	

END __
DELIMITER ;
DROP TRIGGER IF EXISTS updateCodigoAfterUpdateProducto;
DELIMITER __
CREATE TRIGGER updateCodigoAfterUpdateProducto AFTER UPDATE ON productos FOR EACH ROW BEGIN IF NEW.codigo!=OLD.codigo THEN CALL updateCodigoProducto(OLD.codigo,NEW.codigo); END IF; END