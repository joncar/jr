DROP TRIGGER IF EXISTS addProductoSucursal;


CREATE TRIGGER `addProductoSucursal` 
BEFORE INSERT ON `productosucursal` FOR EACH ROW 
BEGIN 
    DECLARE varId INT; 
    SELECT productos.id INTO varId 
    FROM productos WHERE productos.codigo = NEW.producto; 
    SET NEW.productos_id:= varId; 
END