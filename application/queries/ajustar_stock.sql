# CALL ajustar_stock(_producto INT, _sucursal INT,_operacion varchar(1),_cantidad INT)
BEGIN
DECLARE _stock INT;
DECLARE control_stock INT;
SELECT vender_sin_stock INTO control_stock FROM ajustes limit 1;
IF(control_stock=0) THEN
SELECT stock INTO _stock FROM productosucursal WHERE producto = _producto AND sucursal = _sucursal;
IF (_stock IS NOT NULL) THEN

IF _operacion = '+' THEN
SET _stock = _stock + _cantidad;
ELSE 
SET _stock = _stock - _cantidad;
END IF;

END IF;

UPDATE productosucursal SET stock = _stock WHERE producto = _producto AND sucursal = _sucursal;
END IF;
END