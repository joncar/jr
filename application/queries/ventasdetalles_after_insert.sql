DROP TRIGGER IF EXISTS `ventasdetalles_after_insert`;
DELIMITER $$
CREATE TRIGGER `ventasdetalles_after_insert` AFTER INSERT ON `ventadetalle` FOR EACH ROW 
BEGIN 
    DECLARE varDescontar INT; 
    SELECT descontar_prespuesto INTO varDescontar FROM ajustes; 
    
    IF varDescontar = 1 THEN 
        #Verificamos que la venta no teng apresupuesto asignado
        SELECT IF(presupuestos_id IS NULL,1,0) INTO varDescontar FROM ventas WHERE id = NEW.venta;
    ELSE
        #Si no tiene presupuesto asignado se debe descontar el stock
        SET varDescontar = 1;
    END IF;
    
    IF varDescontar = 1 THEN 
        CALL ajust_stock_from_ventadetalle(NEW.id); 
    END IF; 
END$$