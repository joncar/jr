DROP FUNCTION IF EXISTS getResumenCuotas;
DELIMITER $$ 
CREATE FUNCTION getResumenCuotas(varCredito INT) RETURNS VARCHAR(255)
BEGIN
DECLARE varCuotas VARCHAR(255);

	SELECT GROUP_CONCAT(cuotas SEPARATOR '/') INTO varCuotas FROM (
	SELECT 
	COUNT(id) as cuotas
	FROM plan_credito WHERE pagado = 1 AND creditos_id = varCredito
	UNION ALL
	SELECT 
	COUNT(id)
	FROM plan_credito WHERE creditos_id = varCredito
	) as saldo;

RETURN varCuotas;
END $$