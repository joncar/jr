#CALL reversar_stock_from_ventas after update and anulado = 1 ventaID
DROP PROCEDURE IF EXISTS reversar_stock_from_ventas;
DELIMITER /
CREATE PROCEDURE reversar_stock_from_ventas(ventaID INT)
BEGIN
DECLARE control_stock INT;
DECLARE varSucursal INT;
DECLARE sucursalWeb INT;
DECLARE varOrden INT;

SELECT orden_detalle_id INTO varOrden FROM ventas WHERE ventas.id = ventaID;
#Si no es una orden se reversa el stock
IF varOrden IS NULL OR varOrden = 0 THEN
	SELECT vender_sin_stock,sucursal_web INTO control_stock,sucursalWeb FROM ajustes limit 1;
	#IF(control_stock=0) THEN
		UPDATE productosucursal,(
		SELECT 
		productosucursal.id,
		ventadetalle.producto,
		ventas.sucursal,
		ventadetalle.cantidad,
		productosucursal.stock
		FROM ventadetalle
		INNER JOIN ventas ON ventas.id = ventadetalle.venta
		INNER JOIN productosucursal ON productosucursal.producto = ventadetalle.producto AND productosucursal.sucursal = ventas.sucursal
		WHERE venta = ventaID) AS venta SET productosucursal.stock = (productosucursal.stock+venta.cantidad) WHERE productosucursal.id = venta.id;
		#Enviamos stock a la web
		SELECT sucursal INTO varSucursal FROM ventas WHERE ventas.id = ventaID;	
		UPDATE creditos set anulado = 1 WHERE ventas_id = ventaID;
		UPDATE facturas_clientes_detalle SET anulado = 1 WHERE venta = ventaID;
	#END IF;
END IF;
END