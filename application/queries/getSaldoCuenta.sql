DROP FUNCTION IF EXISTS getSaldoCuenta;
DELIMITER $$
CREATE FUNCTION getSaldoCuenta(varCuenta INT) RETURNS INT
BEGIN
	DECLARE varSaldo INT;
	SELECT 
	SUM((libro_banco.tipo_movimiento*libro_banco.monto))
	INTO 
	varSaldo
	FROM cuentas_bancos
	LEFT JOIN libro_banco ON libro_banco.cuentas_bancos_id = cuentas_bancos.id
	WHERE cuentas_bancos.id = varCuenta AND
	(
	    forma_pago = 'Transferencia' OR
	    forma_pago = 'Efectivo' OR
	    forma_pago = 'Deposito' OR
	    forma_pago = 'Cheque' AND libro_banco.tipo_cheque = '1' OR
	    forma_pago = 'Cheque' AND libro_banco.tipo_cheque = '-1' AND libro_banco.fecha_pago>=DATE(NOW())
	) AND libro_banco.anulado = 0;

	RETURN IFNULL(varSaldo,0);
END $$