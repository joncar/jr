DROP PROCEDURE IF EXISTS updatePlan;
DELIMITER --
CREATE PROCEDURE updatePlan(varF INT)
BEGIN
	DECLARE varArancel INT;
	DECLARE varCant INT;
	DECLARE varInscripcion INT;
	DECLARE varPagados INT;
	DECLARE varTotalCuotas INT;
	DECLARE varMontoCuota FLOAT;
	DECLARE varTotalPagado INT;
	DECLARE varSaldo INT;

	DECLARE varTotalPagoArancel INT;
	DECLARE varTotalCuotasWhile INT;
	DECLARE varI INT;
	DECLARE varK INT;	
	SELECT 	
		aranceles_detalles_id,
		cantidad,
		inscripciones_id,
		total_pago
	INTO
		varArancel,
		varCant,
		varInscripcion,
		varTotalPagado
	FROM 
		facturacion_detalle
	INNER JOIN facturacion ON facturacion.id = facturacion_detalle.facturacion_id		
	WHERE facturacion_detalle.id = varF;

	SELECT 			
	aranceles_detalles.cant_cuota,
	aranceles_detalles.monto_cuota
	INTO
	varTotalCuotas,
	varMontoCuota
	FROM aranceles_detalles			
	WHERE aranceles_detalles.id = varArancel;

	IF varTotalCuotas > 1 THEN 
		SELECT 
		SUM(facturacion_detalle.total_pago) INTO varTotalPagoArancel
		FROM facturacion_detalle
		INNER JOIN facturacion ON facturacion.id = facturacion_detalle.facturacion_id
		WHERE facturacion.inscripciones_id = varInscripcion AND facturacion_detalle.aranceles_detalles_id = varArancel AND facturacion_detalle.anulado = 0 AND facturacion.anulado = 0;

		## HAcemos el Foreach
		SELECT 
		COUNT(plan_cuotas.id) INTO varTotalCuotasWhile
		FROM plan_cuotas
		INNER JOIN inscripcion_aranceles ON inscripcion_aranceles.id = plan_cuotas.inscripcion_aranceles_id		
		WHERE plan_cuotas.inscripciones_id = varInscripcion AND aranceles_detalles_id = varArancel
		ORDER BY cuota_nro ASC;
		SET varI:= 0;

		WHILE varI<varTotalCuotasWhile DO

			SELECT 
			plan_cuotas.id			
			INTO 
			varK
			FROM plan_cuotas
			INNER JOIN inscripcion_aranceles ON inscripcion_aranceles.id = plan_cuotas.inscripcion_aranceles_id			
			WHERE plan_cuotas.inscripciones_id = varInscripcion AND aranceles_detalles_id = varArancel
			ORDER BY cuota_nro ASC
			LIMIT varI,1;

			#Restamos			
			IF varTotalPagoArancel >= varMontoCuota THEN
				UPDATE plan_cuotas SET total_pagado = varMontoCuota, saldo = 0, pagado = 1 WHERE plan_cuotas.id = varK;
			ELSE
				SET varSaldo:= IF(varTotalPagoArancel<0,0,varTotalPagoArancel);				
				UPDATE plan_cuotas SET total_pagado = varSaldo, saldo = IF(varSaldo>0,(varMontoCuota - varTotalPagoArancel),varMontoCuota), pagado = IF(varTotalPagoArancel = varMontoCuota,1,0) WHERE plan_cuotas.id = varK;
			END IF;
			SET varTotalPagoArancel:= varTotalPagoArancel - varMontoCuota;
			SET varI:= varI+1;
		END WHILE;
		##Se cuentan las cuotas pagadas
		SELECT 
			count(plan_cuotas.id)			
			INTO
			varPagados
			FROM plan_cuotas
			INNER JOIN inscripcion_aranceles ON inscripcion_aranceles.id = plan_cuotas.inscripcion_aranceles_id
			INNER JOIN aranceles_detalles ON inscripcion_aranceles.aranceles_detalles_id = aranceles_detalles.id
			WHERE plan_cuotas.inscripciones_id = varInscripcion AND inscripcion_aranceles.aranceles_detalles_id = varArancel AND plan_cuotas.pagado = 1;
	ELSE
		##Si no es un plan se pone nomas pagado en inscripcion_aranceles
		SET varPagados:= varCant;
	END IF;

	

	IF varPagados = varTotalCuotas THEN
		UPDATE inscripcion_aranceles SET pagado = 1 WHERE inscripcion_aranceles.inscripciones_id = varInscripcion AND inscripcion_aranceles.aranceles_detalles_id = varArancel AND inscripcion_aranceles.pagado = 0;
	ELSE
		UPDATE inscripcion_aranceles SET pagado = 0 WHERE inscripcion_aranceles.inscripciones_id = varInscripcion AND inscripcion_aranceles.aranceles_detalles_id = varArancel;
	END IF;
END--
DELIMITER ;