DROP FUNCTION IF EXISTS get_saldo;
DELIMITER $$
CREATE FUNCTION get_saldo (cliente_id INT) RETURNS INT
BEGIN
declare saldo int;
	SELECT 
	SUM(saldo_venta) INTO saldo
	FROM facturas_clientes_detalle 
	INNER JOIN facturas_clientes ON facturas_clientes.id = facturas_clientes_detalle.facturas_clientes
	WHERE facturas_clientes.clientes_id = cliente_id;
    RETURN ifnull(saldo,0);
END


##################### OBSOLETE #$$$$$$$$$$$$$$$$$##########
DROP FUNCTION IF EXISTS get_saldo;
DELIMITER $$
CREATE FUNCTION get_saldo (cliente_id INT) RETURNS INT
BEGIN
declare saldo int;
SELECT 
ifnull(compra.Total_compra,0) - get_total_pagado(cliente_id) as saldo into saldo 
FROM clientes 
INNER JOIN(
    SELECT 
    ventas.cliente as cliente_id, 
    sum(ventadetalle.totalcondesc) as Total_compra, 
    MAX(date(ventas.fecha)) as fecha_ult_compra
    FROM ventas 
    INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
    WHERE ventas.status = 0 and ventas.transaccion = 2 and ventas.cliente= cliente_id AND ventas.cliente != 1
    GROUP BY ventas.cliente) compra on compra.cliente_id=clientes.id 
WHERE clientes.id= cliente_id ;
    RETURN ifnull(saldo,0);
END