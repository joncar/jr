BEGIN
DECLARE _saldo INT;
SET @saldo = 0;
SET @deuda = 0;
SELECT get_total_pagado(_cliente) INTO _saldo;
SET @saldo = _saldo;

SELECT * FROM(
SELECT
ventas_id,
producto as Codigo,
fecha as Fecha_compra,
descripcion as Descripcion,
totalcondesc as Total_compra ,
cubre as Monto_cubierto,
saldo as Saldo,
residuo as Residuo,
cantidad,
@deuda:= @deuda + saldo as Deuda
FROM (
SELECT
ventas_id,
producto,
fecha,
descripcion,
totalcondesc,
cubre,
saldo,
cantidad,
@saldo:= TRUNCATE(IF(@saldo>0,@saldo-cubre,0),0) as residuo
FROM 
(
SELECT 
ventas.id as ventas_id,
ventadetalle.producto,
ventas.fecha as fecha,
productos.nombre_comercial as Descripcion,
ventadetalle.totalcondesc,
ventadetalle.cantidad,
IF(@saldo>ventadetalle.totalcondesc,ventadetalle.totalcondesc,TRUNCATE(@saldo,0)) as cubre,
( CASE 
    WHEN @saldo > 0 THEN
		IF(@saldo - ventadetalle.totalcondesc>0,0,ventadetalle.totalcondesc - @saldo)
    ELSE ventadetalle.totalcondesc
    END
) as saldo
FROM ventadetalle 
INNER JOIN ventas ON ventas.id = ventadetalle.venta
LEFT JOIN productos on productos.codigo = ventadetalle.producto
WHERE 
ventas.cliente = _cliente AND
ventas.transaccion = 2 AND 
(ventas.status = 0 OR ventas.status IS NULL)
) as extracto) as estrac
UNION 
SELECT
'TOTALES',
(SELECT SUM(ventadetalle.totalcondesc) FROM ventadetalle INNER JOIN ventas ON ventas.id = ventadetalle.venta WHERE ventas.cliente = _cliente AND ventas.transaccion = 2 AND (ventas.status = 0 OR ventas.status IS NULL)),
0,
0,
0,
0,
@deuda,
0,
0,
0) AS extracto WHERE deuda > 0;

END