#función para actualizar stock desde after_insert_compradetalles ID de compradetalles
#ajust_stock_from_compradetalles PARAMS CompraDetalle INT
DROP PROCEDURE IF EXISTS ajust_stock_from_compradetalles;
DELIMITER /
CREATE PROCEDURE ajust_stock_from_compradetalles(CompraDetalle INT)
BEGIN
DECLARE varProduct VARCHAR(255);
DECLARE varSucursal INT;
DECLARE varProductoSucursalId INT;
DECLARE varCantidad DECIMAL(11,2);
DECLARE varVencimiento DATE;
DECLARE control_stock INT;
DECLARE varPrecioCompra DECIMAL(11,2);
DECLARE varPrecioVenta DECIMAL(11,2);
DECLARE sucursalWeb INT;
DECLARE controlar_venc INT;

SELECT vender_sin_stock,sucursal_web,controlar_vencimiento_stock INTO control_stock,sucursalWeb,controlar_venc FROM ajustes limit 1;
#IF(control_stock=0) THEN
	#Traemos el producto
	SELECT producto,compradetalles.sucursal,compradetalles.cantidad,compradetalles.vencimiento,compradetalles.precio_costo,compradetalles.precio_venta INTO varProduct,varSucursal,varCantidad,varVencimiento,varPrecioCompra,varPrecioVenta FROM compradetalles INNER JOIN compras ON compras.id = compradetalles.compra WHERE compradetalles.id = CompraDetalle LIMIT 1;
	#Traemos el id de stock
	IF controlar_venc = 0 THEN
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
	ELSE
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal AND vencimiento = varVencimiento LIMIT 1;
	END IF;

	IF varProductoSucursalId IS NULL THEN
		#Insertamos el producto para comenzar su stockage
		INSERT INTO productosucursal 
		(
			id,
			proveedor_id,
			fechaalta,
			producto,
			sucursal,
			lote,
			vencimiento,
			precio_venta,
			precio_costo,
			stock,
			precio_venta_mayorista1,
			cant_1,
			porc_mayorista2,
			precio_venta_mayorista2,
			cant_2,
			porc_mayorista3,
			precio_venta_mayorista3,
			cant_3
		)
		SELECT 
		NULL, 
		proveedor_id, 
		NOW(), 
		codigo, 
		varSucursal, 
		NULL, 
		varVencimiento, 
		precio_venta, 
		precio_costo,
		0,
		precio_venta_mayorista1,
		cant_1,
		porc_mayorista2,
		precio_venta_mayorista2,
		cant_2,
		porc_mayorista3,
		precio_venta_mayorista3,
		cant_3
		FROM productos 
		WHERE BINARY codigo = BINARY varProduct;
		IF controlar_venc = 0 THEN
			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		ELSE
			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal AND vencimiento = varVencimiento LIMIT 1;
		END IF;
	END IF;

	#Actualizamos el stock
	UPDATE productosucursal SET stock = (stock+varCantidad) WHERE productosucursal.id = varProductoSucursalId;
	#Actualizamos precio venta y precio compra
	UPDATE productos SET precio_venta = varPrecioVenta, precio_costo = varPrecioCompra WHERE BINARY codigo = BINARY varProduct;
	
	#Enviamos stock a la web
	IF varSucursal = sucursalWeb THEN 
		CALL send_stock_to_wp();
	END IF;
#END IF;
END