#función para actualizar stock desde after_insert_ventadetalle ID de ventadetalle
#PARAMS VentaDetalle INT
DROP PROCEDURE IF EXISTS  ajust_stock_from_fraccionamiento;
DELIMITER //
CREATE PROCEDURE ajust_stock_from_fraccionamiento(varFracc INT)
BEGIN
	
	#Descontar Padre		
		UPDATE productosucursal,(
			SELECT 		
				productosucursal.id,
				(productosucursal.stock - fraccionamientos.parent_cant) as newStock
			FROM fraccionamientos
			INNER JOIN productosucursal ON productosucursal.id = fraccionamientos.productosucursal
			WHERE fraccionamientos.id = varFracc
		) as cd
		SET productosucursal.stock = cd.newStock
		WHERE productosucursal.id = cd.id;
	#Sumar hijos
		#Creamos elemento inventario
		INSERT INTO productosucursal
		SELECT 
		NULL,
		padre.proveedor_id,
		NOW(),
		productos.codigo,
		padre.sucursal,
		padre.lote,
		padre.vencimiento,
		productos.precio_venta,
		productos.precio_costo,
		0,
		0
		FROM fraccionamientos
		INNER JOIN productos ON productos.codigo = fraccionamientos.producto
		INNER JOIN productosucursal AS padre ON padre.id = fraccionamientos.productosucursal
		LEFT JOIN productosucursal AS hijo ON BINARY hijo.producto = BINARY fraccionamientos.producto AND hijo.sucursal = padre.sucursal AND hijo.vencimiento = padre.vencimiento AND hijo.id != padre.id
		WHERE fraccionamientos.id = varFracc AND hijo.id IS NULL;

		#Actualizamos stock en hijo
		UPDATE productosucursal, (
			SELECT 
			hijo.id,
	        (hijo.stock + fraccionamientos.cantidad) as newStock
			FROM fraccionamientos
			INNER JOIN productos ON productos.codigo = fraccionamientos.producto
			INNER JOIN productosucursal AS padre ON padre.id = fraccionamientos.productosucursal
			INNER JOIN productosucursal AS hijo ON BINARY hijo.producto = BINARY fraccionamientos.producto AND hijo.sucursal = padre.sucursal AND hijo.vencimiento = padre.vencimiento AND hijo.id != padre.id
			WHERE fraccionamientos.id = varFracc
		) as cd
        SET productosucursal.stock = cd.newStock
        WHERE productosucursal.id = cd.id;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS fraccionamientos_ainsert;
CREATE TRIGGER fraccionamientos_ainsert AFTER INSERT ON `fraccionamientos` FOR EACH ROW CALL ajust_stock_from_fraccionamiento(NEW.id);