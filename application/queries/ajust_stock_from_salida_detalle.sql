#función para actualizar stock desde after_insert_salida_detalle ID de salida_detalle
#ajust_stock_from_salida_detalle PARAMS SalidaDetalle INT
DROP PROCEDURE IF EXISTS ajust_stock_from_salida_detalle;
DELIMITER /
CREATE PROCEDURE ajust_stock_from_salida_detalle(SalidaDetalle INT)
BEGIN
DECLARE varProduct VARCHAR(255);
DECLARE varSucursal INT;
DECLARE varProductoSucursalId INT;
DECLARE varCantidad DECIMAL(11,2);
DECLARE control_stock INT;
DECLARE sucursalWeb INT;
DECLARE varVencimiento DATE;
DECLARE controlar_venc INT;
SELECT vender_sin_stock,controlar_vencimiento_stock INTO control_stock,controlar_venc FROM ajustes limit 1;
#IF(control_stock=0) THEN
	IF controlar_venc = 0 THEN
		#Traemos el producto
		SELECT salida_detalle.producto,salida_detalle.sucursal,salida_detalle.cantidad INTO varProduct,varSucursal,varCantidad FROM salida_detalle WHERE salida_detalle.id = SalidaDetalle;
		#Traemos el id de stock
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
	ELSE 
		#Traemos el producto
		SELECT salida_detalle.producto,salida_detalle.sucursal,salida_detalle.cantidad,salida_detalle.vencimiento INTO varProduct,varSucursal,varCantidad,varVencimiento FROM salida_detalle WHERE salida_detalle.id = SalidaDetalle;
		#Traemos el id de stock
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal AND vencimiento = varVencimiento LIMIT 1;
	END IF;
	IF varProductoSucursalId IS NULL THEN
		IF controlar_venc = 0 THEN
			#Insertamos el producto para comenzar su stockage
			

			INSERT INTO productosucursal 
			(
				id,
				proveedor_id,
				fechaalta,
				producto,
				sucursal,
				lote,
				vencimiento,
				precio_venta,
				precio_costo,
				stock,
				precio_venta_mayorista1,
				cant_1,
				porc_mayorista2,
				precio_venta_mayorista2,
				cant_2,
				porc_mayorista3,
				precio_venta_mayorista3,
				cant_3
			)
			SELECT 
			NULL, 
			proveedor_id, 
			NOW(), 
			codigo, 
			varSucursal, 
			NULL, 
			NULL, 
			precio_venta, 
			precio_costo,
			0,
			precio_venta_mayorista1,
			cant_1,
			porc_mayorista2,
			precio_venta_mayorista2,
			cant_2,
			porc_mayorista3,
			precio_venta_mayorista3,
			cant_3
			FROM productos 
			WHERE BINARY codigo = BINARY varProduct;



			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		ELSE 
			#Insertamos el producto para comenzar su stockage
		

			INSERT INTO productosucursal 
			(
				id,
				proveedor_id,
				fechaalta,
				producto,
				sucursal,
				lote,
				vencimiento,
				precio_venta,
				precio_costo,
				stock,
				precio_venta_mayorista1,
				cant_1,
				porc_mayorista2,
				precio_venta_mayorista2,
				cant_2,
				porc_mayorista3,
				precio_venta_mayorista3,
				cant_3
			)
			SELECT 
			NULL, 
			proveedor_id, 
			NOW(), 
			codigo, 
			varSucursal, 
			NULL, 
			varVencimiento, 
			precio_venta, 
			precio_costo,
			0,
			precio_venta_mayorista1,
			cant_1,
			porc_mayorista2,
			precio_venta_mayorista2,
			cant_2,
			porc_mayorista3,
			precio_venta_mayorista3,
			cant_3
			FROM productos 
			WHERE BINARY codigo = BINARY varProduct;


			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal AND vencimiento = varVencimiento LIMIT 1;
		END IF;
	END IF;

	#Actualizamos el stock
	UPDATE productosucursal SET stock = (stock-varCantidad) WHERE productosucursal.id = varProductoSucursalId;
	
	#Enviamos stock a la web
	IF varSucursal = sucursalWeb THEN 
		CALL send_stock_to_wp();
	END IF;
#END IF;
END