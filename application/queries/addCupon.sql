DROP PROCEDURE IF EXISTS addCupon;
DELIMITER //
CREATE PROCEDURE addCupon (varVenta INT)
BEGIN
	DECLARE varCliente INT;
	DECLARE varFecha DATETIME;
	DECLARE varMin INT;
	DECLARE varTotal INT;
	DECLARE varCant INT;
	DECLARE varGenerar INT;
	DECLARE varCarrier INT;
	DECLARE varCupones INT;
	DECLARE varFijo INT;

	SELECT generar_cupones, minimo_venta_cupones, cupon_fijo
	INTO varGenerar,varMin,varFijo
	FROM ajustes;

	SELECT ventas.cliente,ventas.fecha,ventas.total_venta
	INTO varCliente,varFecha,varTotal
	FROM ventas 
	WHERE ventas.id = varVenta;
	
	SELECT COUNT(id) INTO varCupones FROM cupones WHERE ventas_id = varVenta;
	
	IF varGenerar = 1 AND varCliente != 1 AND varTotal >= varMin AND varCupones = 0 THEN
		    SET varCant:= IF(varFijo = 0,FLOOR(varTotal/varMin),varFijo);
		    SET varCarrier:= 0;
		    WHILE varCarrier < varCant DO
		       	INSERT INTO cupones (id,nro,total_cupon,clientes_id,fecha,ventas_id,importe_minimo,total_venta) VALUES(NULL,(varCarrier+1),varCant,varCliente,varFecha,varVenta,varMin,varTotal);
		    	SET varCarrier:= varCarrier+1;
		    END WHILE;
	END IF;

END