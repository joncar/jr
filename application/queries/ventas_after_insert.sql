DROP TRIGGER IF EXISTS ventas_after_insert;
DELIMITER &&
CREATE TRIGGER ventas_after_insert AFTER INSERT ON ventas FOR EACH ROW 
BEGIN
	CALL addCupon(NEW.id); 
	IF NEW.presupuestos_id != 0 OR NEW.presupuestos_id IS NOT NULL THEN 
		UPDATE presupuesto SET facturado = 1 WHERE presupuesto.id = NEW.presupuestos_id; 
	END IF; 
END