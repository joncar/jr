#CALL reversar_stock_from_compras after update and anulado = 1 compraID
DROP PROCEDURE IF EXISTS reversar_stock_from_compras;
DELIMITER /
CREATE PROCEDURE reversar_stock_from_compras(compraID INT)
BEGIN
DECLARE control_stock INT;
DECLARE varSucursal INT;
DECLARE sucursalWeb INT;
SELECT vender_sin_stock,sucursal_web INTO control_stock,sucursalWeb FROM ajustes limit 1;
#IF(control_stock=0) THEN
	UPDATE productosucursal,(SELECT 
	productosucursal.id,
	compradetalles.producto,
	compras.sucursal,
	compradetalles.cantidad,
	productosucursal.stock
	FROM compradetalles
	INNER JOIN compras ON compras.id = compradetalles.compra
	INNER JOIN productosucursal ON productosucursal.producto = compradetalles.producto AND productosucursal.sucursal = compras.sucursal
	WHERE compra = compraID) AS compra SET productosucursal.stock = (productosucursal.stock-compra.cantidad) WHERE productosucursal.id = compra.id;
	#Enviamos stock a la web
	SELECT sucursal INTO varSucursal FROM compras WHERE compras.id = compraID;	
	#Enviamos stock a la web
	IF varSucursal = sucursalWeb THEN 
		CALL send_stock_to_wp();
	END IF;
#END IF;
END