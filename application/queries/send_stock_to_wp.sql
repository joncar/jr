DROP PROCEDURE IF EXISTS send_stock_to_wp;
DELIMITER /
CREATE PROCEDURE send_stock_to_wp()
BEGIN
	DECLARE canSyncronize INT;
	SELECT IF(db_web IS NULL OR db_web = '',0,1),db_web INTO canSyncronize,@dbName FROM ajustes;
	IF canSyncronize = 1 THEN
		#SET @qry:= CONCAT('CALL ',@dbName,'.updateStock()');		
		#PREPARE str FROM @qry;
		#EXECUTE str;
		#DEALLOCATE PREPARE str;
		UPDATE ajustes SET sincronizar_stock = 1;
	END IF;
END