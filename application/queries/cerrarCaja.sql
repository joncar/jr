DROP PROCEDURE IF EXISTS cerrarCaja;
DELIMITER &&
CREATE PROCEDURE cerrarCaja(xcajadiaria INT)
BEGIN
	DECLARE xmonto_inicial INT;
	DECLARE xcontado INT;
	DECLARE xpagocliente INT;
	DECLARE xcreditos INT;
	DECLARE xventas INT;
	DECLARE xtotal_entrega INT;
	DECLARE xcaja INT;
	DECLARE xefectivoARendir INT;
	DECLARE xfecha_cierre DATETIME;
	DECLARE xegresos INT;
	DECLARE xtotal_efectivo INT;
	DECLARE xtotal_retiros INT;
	DECLARE xsucursal INT;
	DECLARE xusuario INT;
	DECLARE xentregasiniciales INT;
	DECLARE xpagospresupuestos INT;
	DECLARE xEgresosAjuste INT;
	DECLARE xIngresosAjuste INT;

	SELECT IFNULL(SUM(monto),0) INTO xegresos FROM gastos WHERE cajadiaria = xcajadiaria;
	SELECT IFNULL(SUM(monto),0) INTO xIngresosAjuste FROM ajuste_cajadiaria WHERE cajadiaria_id = xcajadiaria AND tipo_ajuste > 0;
	SELECT IFNULL(SUM(monto),0) INTO xEgresosAjuste FROM ajuste_cajadiaria WHERE cajadiaria_id = xcajadiaria AND tipo_ajuste < 0;

	SELECT caja,sucursal,usuario INTO xcaja,xsucursal,xusuario FROM cajadiaria WHERE cajadiaria.id = xcajadiaria;

	SELECT monto_inicial INTO xmonto_inicial FROM cajadiaria WHERE cajadiaria.id = xcajadiaria;

	SELECT IFNULL(SUM(totalcondesc),0) INTO xcontado 
	FROM ventadetalle 
	INNER JOIN ventas ON ventas.id = ventadetalle.venta
	INNER JOIN productos ON productos.codigo = ventadetalle.producto
	WHERE cajadiaria=xcajadiaria AND status=0 AND transaccion=1 AND (ventas.presupuestos_id IS NULL OR ventas.presupuestos_id = 0);

	SELECT IFNULL(SUM(total_pagado),0) INTO xpagocliente 
	FROM pagocliente
	WHERE (anulado = 0 OR anulado IS NULL) AND pagocliente.cajadiaria = xcajadiaria;

	SELECT IFNULL(SUM(totalcondesc),0) INTO xcreditos
	FROM ventadetalle
	INNER JOIN ventas ON ventas.id = ventadetalle.venta
	INNER JOIN productos ON productos.codigo = ventadetalle.producto
	WHERE cajadiaria = xcajadiaria AND status=0 AND transaccion=2 AND (ventas.presupuestos_id IS NULL OR ventas.presupuestos_id = 0);

	SELECT IFNULL(SUM(totalcondesc),0) INTO xventas 
	FROM ventadetalle
	INNER JOIN ventas ON ventas.id = ventadetalle.venta
	INNER JOIN productos ON productos.codigo = ventadetalle.producto
	WHERE cajadiaria=xcajadiaria AND status=0 AND (ventas.presupuestos_id IS NULL OR ventas.presupuestos_id = 0);

	SELECT IFNULL(sum(entrega_inicial),0) INTO xtotal_entrega
	FROM creditos
	inner JOIN ventas on ventas.id = creditos.ventas_id
	WHERE ventas.status != -1 and creditos.cajadiaria =xcajadiaria AND (ventas.presupuestos_id IS NULL OR ventas.presupuestos_id = 0);

	SELECT (NOW()) INTO xfecha_cierre;

	#Entregas Iniciales
	SELECT IFNULL(sum(entrega_inicial),0) INTO xentregasiniciales
	FROM presupuesto	
	WHERE (presupuesto.anulado IS NULL OR presupuesto.anulado = 0) AND presupuesto.cajadiaria = xcajadiaria;

	#Total Pagos presupuestos
	SELECT IFNULL(sum(monto),0) INTO xpagospresupuestos
	FROM presupuesto_pagos	
	WHERE (presupuesto_pagos.anulado IS NULL OR presupuesto_pagos.anulado = 0) AND presupuesto_pagos.cajadiaria = xcajadiaria;

	

	SELECT (xmonto_inicial+xcontado+xpagocliente+xtotal_entrega+xentregasiniciales+xpagospresupuestos+xIngresosAjuste) - (xegresos+xEgresosAjuste) INTO xefectivoARendir;

	UPDATE cajadiaria 
	SET 
	abierto = 0,
	total_contado = xcontado,
	total_pago_clientes = xpagocliente,
	total_credito = xcreditos,
	total_ventas = xventas,
	total_descuentos = 0,
	total_entrega_credito = xtotal_entrega,
	total_egreso = xegresos,
	efectivoarendir = xefectivoARendir,
	fecha_cierre = xfecha_cierre
	WHERE cajadiaria.id = xcajadiaria;

	#SELECT IFNULL(SUM(efectivoarendir),0) INTO xtotal_efectivo FROM cajadiaria WHERE caja = xcaja AND cajadiaria.abierto = 1;

	#SELECT IFNULL(SUM(monto),0) INTO xtotal_retiros FROM ajuste_cajas WHERE cajas_id = xcaja;

	#UPDATE cajas SET abierto = 0,efectivo_disponible = (xtotal_efectivo - xtotal_retiros) WHERE cajas.id = xcaja;
	INSERT INTO ajuste_efectivo(
		id,
		sucursales_id,
		cajas_id,
		cajadiaria_id,
		tipo_ajuste,
		fecha,
		observacion,
		monto,
		comprobante,
		anulado,
		user_id
	) VALUES(
		NULL,
		xsucursal,
		xcaja,
		xcajadiaria,
		1,
		NOW(),
		'Total efectivoarendir en cierre de caja',
		(xefectivoARendir - xmonto_inicial),
		'',
		0,
		xusuario	
	);

END