DROP FUNCTION IF EXISTS get_total_pagado;
DELIMITER $$
CREATE FUNCTION get_total_pagado (varCliente INT) RETURNS INT
BEGIN 
DECLARE saldo FLOAT;

SELECT 
(ifnull(pagos.total_pago,0)+ifnull(notas.total_credito,0)+ifnull(entrega_inicial.total_entrega,0)) as saldo INTO saldo
FROM clientes 
INNER JOIN(
    SELECT 
    ventas.cliente as cliente_id, 
    sum(ventadetalle.totalcondesc) as Total_compra, 
    MAX(date(ventas.fecha)) as fecha_ult_compra
    FROM ventas 
    INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
    WHERE ventas.status = 0 and ventas.transaccion = 2 and ventas.cliente= varCliente AND ventas.cliente != 1
    GROUP BY ventas.cliente) compra on compra.cliente_id=clientes.id 

LEFT JOIN(
    SELECT sum(creditos.entrega_inicial) as total_entrega,
    ventas.cliente as cliente_id
    FROM creditos
    INNER JOIN ventas on ventas.id = creditos.ventas_id
    WHERE ventas.cliente = varCliente and ventas.anulado =0
) as entrega_inicial on entrega_inicial.cliente_id = clientes.id 

LEFT JOIN (
    SELECT 
    pagocliente.clientes_id as cliente_id,
    sum(pagocliente.total_pagado) as total_pago, 
    max(date(pagocliente.fecha)) as fecha_ult_pago  
    FROM pagocliente 
    WHERE pagocliente.anulado = 0 AND pagocliente.clientes_id=varCliente 
    GROUP by pagocliente.clientes_id) pagos on pagos.cliente_id=clientes.id 
LEFT JOIN (
    SELECT 
    notas_credito_cliente.cliente as cliente_id,
    SUM(notas_credito_cliente.total_monto) as total_credito,
    MAX(date(notas_credito_cliente.fecha)) as fecha_ult_nota 
    FROM notas_credito_cliente
    INNER JOIN ventas on ventas.id = notas_credito_cliente.venta
    WHERE ventas.transaccion = 2 and notas_credito_cliente.cliente=varCliente and notas_credito_cliente.anulado = 0
    GROUP BY notas_credito_cliente.cliente) notas on notas.cliente_id=clientes.id 
WHERE clientes.id= varCliente ;

RETURN saldo;
END