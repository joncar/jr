<?php 
class Kuatia {
    //protected $url = 'http://181.124.226.11:8080/facturacion-api/data.php';
    protected $url = 'http://35.182.54.52/facturacion-api/data.php';
    protected $debug = false;
    function __construct(){
        $this->db = get_instance()->db;
        $this->url = $this->db->get_where('ajustes')->row()->fe_api_link;
    }

    function sendToServer($data = []){
        $headers = array(          
          'Content-Type:application/x-www-form-urlencoded'
        );  
        if($this->debug){  
            echo '<hr>';
            echo 'ENDPOINT: '.print_r($url,TRUE).'<br/><br/>'; 
            echo '<hr>';
            echo 'Headers: '.print_r($headers,TRUE).'<br/><br/>';           
            echo '<hr>';
            echo 'data: '.print_r($data,TRUE).'<br/><br/>'; 
        }
        
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL,$this->url);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );  
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );      
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt( $ch, CURLOPT_POST,true);              	
        curl_setopt( $ch, CURLOPT_POSTFIELDS,'recordID=123&datajson='.$data);
        //echo 'recordID=123&datajson='.$data;
        $response = curl_exec($ch);
        if(!$this->debug){
            return $response;
        }
        curl_close($ch);
        return $response;
    }

    function generarFactura($venta){
        $venta = $this->db->get_where('ventas',['id'=>$venta]);
        if($venta->num_rows()==0){
            return false;
        }
        $ajustes = $this->db->get('ajustes')->row();
        $v = $venta->row();
        $v->cliente = $this->db->get_where('clientes',['id'=>$v->cliente])->row();
        $v->sucursal = $this->db->get_where('sucursales',['id'=>$v->sucursal])->row();
        $v->caja = $this->db->get_where('cajas',['id'=>$v->caja])->row();
        $this->db->select('ventadetalle.*,productos.codigo, productos.codigo_interno,productos.nombre_comercial,productos.iva_id');
        $this->db->join('productos','productos.codigo = ventadetalle.producto');
        $this->db->join('unidad_medida','unidad_medida.id = productos.unidad_medida_id','LEFT');
        $v->detalles = $this->db->get_where('ventadetalle',['venta'=>$v->id])->result();
        $v->forma_pago = $this->db->get_where('formapago',['id'=>$v->forma_pago])->row();
        $v->credito = @$this->db->get_where('creditos',['ventas_id'=>$v->id])->row();
        if($v->sucursal->generar_factura_electronica!=1){
            return true;
        }
        if($v->credito){
            $v->credito->plan = @$this->db->get_where('plan_credito',['creditos_id'=>$v->credito->id])->result();
        }
        if($v->cliente->id==1){
            //return false;
        }
        $cliente = [
            "ruc"=>'',
            "nombre"=>'',
            "diplomatico"=>false
        ];
        if($v->cliente->id!=1){
            $cliente = [
                "ruc"=>$v->cliente->nro_documento,
                "nombre"=>$v->cliente->nombres.' '.$v->cliente->apellidos,
                "diplomatico"=>false
            ];
        }
        $v->total_venta = round($v->total_venta,4);
        
        $correlativo = explode('-',$v->nro_factura);
        $correlativo = $correlativo[count($correlativo)-1];
        /*$correlativo = str_pad($v->caja->correlativo_code100+1,7,'0',STR_PAD_LEFT);
        if(!empty($v->factura_electronica)){
            $correlativo = str_pad($v->factura_electronica,7,'0',STR_PAD_LEFT);
        }*/


        $codigo_seguridad = rand(100000000,999999999);
        if(!empty($v->codigo_seguridad_fe)){
            $codigo_seguridad = $v->codigo_seguridad_fe;
        }
        

        $data = [
            "fecha"=>$v->fecha ,
            "cdcAsociado"=>"",
            "documentoAsociado"=>false,
            "punto"=>$v->caja->cod_code100_facturas,
            "establecimiento"=>$v->sucursal->cod_est_code100,
            "numero"=> $correlativo,
            "descripcion"=>"2",
            "tipoDocumento"=>1,
            "tipoEmision"=>1,
            "tipoTransaccion"=>1,
            "receiptid"=>$v->id,
            "condicionPago"=>$v->transaccion,
            "moneda"=>"PYG",
            "cambio"=>0,
            "cliente"=>$cliente,
            "codigoSeguridadAleatorio"=>$codigo_seguridad,
            "totalPago"=>$v->total_venta,
            "totalRedondeo"=>0,
            "codigo"=>1222,
            "pais"=>"PYG"
        ];
        
        $data['items'] = [];
        foreach($v->detalles as $det){
            $det->precioventadesc = $det->totalcondesc/$det->cantidad;
            $medida = 77.0;
            if(!empty($det->codigo_fe)){
                $medida = $det->codigo_fe;
            }
            $total_iva = $det->iva_id==0?0:$det->totalcondesc/[5=>21,10=>11][$det->iva_id];
            $data['items'][] = [
                "descripcion"=>$det->nombre_comercial,
                "codigo"=>$det->id,
                "tipoIva"=>"I.V.A. ".$det->iva_id."%",
                "unidadMedida"=>$medida,
                "ivaTasa"=>$det->iva_id,
                "ivaAfecta"=>$det->iva_id==0?3:1,
                "cantidad"=>$det->cantidad,
                "precioUnitario"=>$det->precioventadesc,
                "precioTotal"=>$det->totalcondesc,
                "baseGravItem"=>$det->iva_id==0?0:$det->totalcondesc-$total_iva,
                "liqIvaItem"=>$total_iva
            ];
        }
        $data['credito'] = [];
        if($v->transaccion==2){//Credito
            if($v->plan==1){
                //Plan
                $credito = [
                    "condicionCredito"=> 2,
                    "descripcion"=> "Cuota",
                    "cantidadCuota"=> $v->credito->cant_cuota,
                ];
                $cuotas = [];
                foreach($v->credito->plan as $plan){
                    $cuotas[] = [
                        "numero"=>$plan->nro_cuota,
                        "monto"=>$plan->monto_a_pagar,
                        "fechaVencimiento"=>$plan->fecha_a_pagar
                    ];
                }
                $credito['cuotas'] = $cuotas;
            }else{//Sin plan
                $credito = [
                    "condicionCredito"=> 1,
                    "descripcion"=> "Plazo 30 dias"
                ];
            }
            $data['credito'] = $credito;
            /*$data["iCondCred"] = 2;
            $data['dCuotas'] = $v->credito->cant_cuota;
            $data['Cuotas'] = [];
            for($i=0;$i<$v->credito->cant_cuota;$i++){
                $data['Cuotas'][] = [
                    "cMoneCuo"=>"PYG",
                    "dMonCuota"=>$v->credito->monto_cuota
                ];
            }*/
        }
        if($v->transaccion==2){
            $data['pagos'] = [new stdclass()];
        }else{
            $data['pagos'] = [
                [
                    'name'=>"Cash",
                    "tipoPago"=>$v->forma_pago->codigo_fe,
                    'monto'=>$v->total_venta
                ]
            ];
        }
        $data = json_encode($data);
        //print_r($data);
        //die;
        //$data = ['recordID'=>123,'datajson'=>$data];
        $rr = $this->sendToServer($data);
        
        $this->db->update('ventas',['codigo_seguridad_fe'=>$codigo_seguridad,'factura_electronica_log'=>$rr,'factura_electronica_data'=>$data],['id'=>$v->id]);
        $response = json_decode($rr);
        //if($response->status){
        if(empty($v->factura_electronica)){
            $this->db->update('ventas',['factura_electronica'=>$v->caja->correlativo_code100+1],['id'=>$v->id]);
            $this->db->update('cajas',['correlativo_code100'=>$v->caja->correlativo_code100+1],['id'=>$v->caja->id]);
            
            return true;
        }
        echo 'Documento generado con exito';
        return false;
        
        return $response->status;
    }

    function generarLink($venta){
        if($this->db->get_where('sucursales',['id'=>get_instance()->user->sucursal])->row()->generar_factura_electronica!=1){
            return '';
        }
        $venta = $this->db->get_where('ventas',['id'=>$venta]);
        if($venta->num_rows()>0){
            $venta = $venta->row();
            if(!empty($venta->factura_electronica_log)){
                $log = json_decode($venta->factura_electronica_log);
                if($log && $log->status){
                    $xml = file_get_contents($log->xmlLink);
                    $xml = simplexml_load_string($xml);
                    $json = json_encode($xml);
                    $factura = json_decode($json,TRUE);
                    if(!isset($factura['gCamFuFD']) || !isset($factura['gCamFuFD']['dCarQR'])){
                        return;
                    }
                    return $factura['gCamFuFD']['dCarQR'];
                }
            }
        }
        return '';
    }

    function getCDC($venta){
        if($this->db->get_where('sucursales',['id'=>get_instance()->user->sucursal])->row()->generar_factura_electronica!=1){
            return '';
        }
        $venta = $this->db->get_where('ventas',['id'=>$venta]);
        if($venta->num_rows()>0){
            $venta = $venta->row();
            if(!empty($venta->factura_electronica_log)){
                $log = json_decode($venta->factura_electronica_log);
                if($log && $log->status){
                    $xml = file_get_contents($log->xmlLink);
                    $xml = simplexml_load_string($xml);
                    $json = json_encode($xml);
                    $factura = json_decode($json,TRUE);
                    if(!isset($factura['gCamFuFD']) || !isset($factura['gCamFuFD']['dCarQR'])){
                        return;
                    }
                    return $log;
                }
            }
        }
        return '';
    }

    /********************Notas d ecredito */
    function generarFacturaNCR($venta){
        $url = $this->url.'operation/';
        $venta = $this->db->get_where('notas_credito_cliente',['id'=>$venta]);
        if($venta->num_rows()==0){
            return false;
        }
        $ajustes = $this->db->get('ajustes')->row();
        $v = $venta->row();
        $v->cliente = $this->db->get_where('clientes',['id'=>$v->cliente])->row();
        $v->sucursal = $this->db->get_where('sucursales',['id'=>$v->sucursal])->row();
        $v->caja = $this->db->get_where('cajas',['id'=>$v->caja])->row();
        $this->db->select('notas_credito_cliente_detalle.*,productos.codigo_interno,productos.nombre_comercial,productos.iva_id as iva');
        $this->db->join('productos','productos.id = notas_credito_cliente_detalle.producto');
        $this->db->join('unidad_medida','unidad_medida.id = productos.unidad_medida_id','LEFT');
        $v->detalles = $this->db->get_where('notas_credito_cliente_detalle',['nota_credito'=>$v->id])->result();
        $v->motivo = @$this->db->get_where('motivo_nota',['id'=>$v->motivo_nota_id])->row();
        $v->credito = @$this->db->get_where('creditos',['ventas_id'=>$v->id])->row();
        $v->venta = $this->db->get_where('ventas',['id'=>$v->venta])->row();
        $v->venta->timbrado = $this->db->get_where("timbrados",['id'=>$v->venta->timbrados_id])->row();
        if($v->sucursal->generar_factura_electronica!=1){
            return true;
        }
        if(empty($v->venta->factura_electronica_log) || !json_decode($v->venta->factura_electronica_log)->status){
            $this->db->update('notas_credito_cliente',['factura_electronica_log'=>json_encode(['status'=>false,'message'=>'Nota de credito huerfana'])],['id'=>$v->id]);
            return;
        }
        $v->venta->factura_electronica_log = json_decode($v->venta->factura_electronica_log);
        $correlativo = explode('-',$v->nro_nota_credito);
        $correlativo = $correlativo[count($correlativo)-1];
        $cliente = [
            "ruc"=>'',
            "nombre"=>'',
            "diplomatico"=>false
        ];
        if($v->cliente->id!=1){
            $cliente = [
                "ruc"=>$v->cliente->nro_documento,
                "nombre"=>$v->cliente->nombres.' '.$v->cliente->apellidos,
                "diplomatico"=>false
            ];
        }
        $data = [
            "fecha"=>$v->fecha,
            "cdcAsociado"=>"",
            "documentoAsociado"=>[
                "remision"=>false,
                "tipoDocumentoAsoc"=>"1",
                "cdcAsociado"=>$v->venta->factura_electronica_log->cdc,
                "establecimientoAsoc"=>$v->sucursal->cod_est_code100,
                /*"puntoAsoc"=>$v->caja->cod_code100_facturas,
                "numeroAsoc"=>str_pad($v->venta->factura_electronica,7,'0',STR_PAD_LEFT),
                "tipoDocuemntoIm"=>"1",
                "fechaDocIm"=>$v->venta->fecha,
                "timbradoAsoc"=>$v->venta->timbrado->nro_timbrado*/
            ],
            "punto"=>$v->caja->cod_code100_facturas,
            "establecimiento"=>$v->sucursal->cod_est_code100,
            "numero"=> str_pad($correlativo,7,'0',STR_PAD_LEFT),
            "descripcion"=>"2",
            "tipoDocumento"=>5,
            "tipoEmision"=>1,
            "tipoTransaccion"=>1,
            "receiptid"=>$v->id,
            "condicionPago"=>1,
            "moneda"=>"PYG",
            "cambio"=>0,
            "cliente"=>$cliente,
            "pagos"=>[
                [
                   'name'=>"cash",
                   'tipoPago'=>99,
                   'monto'=>$v->total_monto
                ]
            ],
            "credito"=>[],
            "codigoSeguridadAleatorio"=>rand(100000000,999999999),
            "totalPago"=>$v->total_monto,
            "totalRedondeo"=>0,
            "codigo"=>1222,
            "pais"=>"PYG"
        ];
        $data['items'] = [];
        foreach($v->detalles as $det){
            $medida = 77.0;
            if(!empty($det->codigo_fe)){
                $medida = $det->codigo_fe;
            }
            //$total_iva = $det->iva==0?0:($det->total*100)/[5=>105,10=>110][$det->iva];
            $total_iva = $det->iva==0?0:$det->total/[5=>21,10=>11][$det->iva];
            $data['items'][] = [
                "descripcion"=>$det->nombre_comercial,
                "codigo"=>$det->codigo_interno,
                "tipoIva"=>"I.V.A. ".$det->iva."%",
                "unidadMedida"=>$medida,
                "ivaTasa"=>$det->iva,
                "ivaAfecta"=>$det->iva==0?3:1,
                "cantidad"=>$det->cantidad,
                "precioUnitario"=>$det->precio_venta,
                "precioTotal"=>$det->total,
                //"baseGravItem"=>$det->total-$total_iva,
                "baseGravItem"=>$det->iva==0?0:$det->total-$total_iva,
                "liqIvaItem"=>$total_iva
            ];
        }
        $data = json_encode($data);
        //print_r($data);
        //die;
        //$data = ['recordID'=>123,'datajson'=>$data];
        $rr = $this->sendToServer($data);
        print_r($rr);
        $this->db->update('notas_credito_cliente',['factura_electronica_log'=>$rr],['id'=>$v->id]);
        $response = json_decode($rr);
        if($response->status){
            $this->db->update('notas_credito_cliente',['factura_electronica'=>$v->caja->correlativo_nota_code100],['id'=>$v->id]);
            $this->db->update('cajas',['correlativo_code100'=>$v->caja->correlativo_nota_code100+1],['id'=>$v->caja->id]);
            echo 'Documento generado con exito';
            return true;
        }
        return false;
        
        return $response->status;
    }

    function generarLinkNCR($venta){
        if($this->db->get_where('sucursales',['id'=>get_instance()->user->sucursal])->row()->generar_factura_electronica!=1){
            return '';
        }
        $venta = $this->db->get_where('notas_credito_cliente',['id'=>$venta]);
        if($venta->num_rows()>0){
            $venta = $venta->row();
            if(!empty($venta->factura_electronica_log)){
                $log = json_decode($venta->factura_electronica_log);
                if($log && $log->status){
                    $xml = file_get_contents($log->xmlLink);
                    $xml = simplexml_load_string($xml);
                    $json = json_encode($xml);
                    $factura = json_decode($json,TRUE);
                    if(!isset($factura['gCamFuFD']) || !isset($factura['gCamFuFD']['dCarQR'])){
                        return;
                    }
                    $factura['cdc'] = $log->cdc;
                    return $factura;
                }
            }
        }
        return '';
    }
}