const Venta = function(){}

Venta.prototype.initEvents = function(){
	var l = this;
	this.tr = $("#productoVacio").clone();
	this.tbody = $("#productoVacio").parents('tbody');
	$("#productoVacio").remove();
	$(document).on('keydown','#codigoAdd',function(e){
		
    	if(e.which==13){
    		e.preventDefault();
    		l.addProduct($(this).val());
    	}
    });

    $(document).on('keydown','.producto input',function(e){
        
        if(e.which==13){
            e.preventDefault();            
        }
    });

    $(document).on('click','.insertar',function(){
    	l.addProduct($('#codigoAdd').val());
    });

    $(document).on('click','.rem',function(){
    	$(this).parents('tr').remove();
    	l.updateEmpty();
        l.updateTemp(true);
    });

    /*$(document).on('change','.producto input,.producto select',function(){    	
    	l.updateEmpty();
        l.updateTemp();
    });*/

    $(document).on('change','input[name="pago_guaranies"],input[name="pago_reales"],input[name="pago_dolares"],input[name="pago_pesos"]',function(){
        l.actualizarPago();
    });

    $(document).on('change','.producto input[name="precio_venta"]',function(){        
        $(this).parents('tr').find('[name="precio_venta"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_min_cant"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_min_caja"]').val($(this).val());

        $(this).parents('tr').find('[name="precio_may_unidad"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_may_cant"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_may_caja"]').val($(this).val());

        $(this).parents('tr').find('[name="precio_campo_unidad"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_campo_cant"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_campo_caja"]').val($(this).val());
        l.updateEmpty();
    });

    $(document).on('keydown',function(e){

        if(e.which==18 && !l.alt){
            e.preventDefault();
            l.alt = true; 
            $(".alt").show();               
        }

        else if(e.which==18 && l.alt){
            e.preventDefault();
            l.alt = false;                
            $(".alt").hide();
        }

        if(l.alt){
            e.preventDefault();
        }
    });
    $(document).on('keyup',function(e){                        
    	if(l.alt){   

            switch(e.which){
                case 67: //[c] Focus en codigos
                    $("#codigoAdd").focus();
                    l.alt = false;
                    $(".alt").hide();
                break;
                case 73: //[i] busqueda avanzada
                    $("#inventarioModal").modal('toggle');
                    l.alt = false;  
                    $(".alt").hide();                          
                break;
                case 80: //[p] Procesar venta
                    /*$("#procesar").modal('toggle');        */
                    l.procesar();
                    l.alt = false;  
                    $(".alt").hide();                  
                break;
                case 78: //[n] Nueva venta
                    l.nuevaVenta();
                    l.alt = false;
                    $(".alt").hide();
                break;
                case 66: //[B] Nueva venta
                    saldo();
                    l.alt = false;
                    $(".alt").hide();
                break;
                case 72: //[H] Enfocar en la cabecera
                    //$('a[href="#addCliente"]').focus();
                    $('#field-cliente').chosen().trigger('liszt:activate'); 
                    l.alt = false;  
                    $(".alt").hide();                        
                break;
                case 87: //[W] New Window
                    window.open($("#newWindow").attr('href'),'_new');
                    l.alt = false;
                    $(".alt").hide();
                break;
                case 38: //UP
                    //Cantidad focus
                    var index = 0;
                    var focused = $('.cantidad:focus');
                    if(focused.length>0){
                        index = $('.cantidad:focus').parents('tr').index();
                        index-=1;                                
                        index = index<0?0:index;
                    }
                    $($('.cantidad')[index]).focus(); 
                    l.alt = false; 
                    $(".alt").hide();                            
                break;
                case 40: //DOWN
                    //Cantidad focus
                    var index = 0;
                    var focused = $('.cantidad:focus');                            
                    if(focused.length>0){
                        index = $('.cantidad:focus').parents('tr').index();
                        index+=1;                                
                        index = index>$('.cantidad').length?$('.cantidad').length-1:index;
                    }
                    $($('.cantidad')[index]).focus(); 
                    l.alt = false;      
                    $(".alt").hide();                      
                break;

                case 88: //X
                    //Cantidad focus
                    //$('#addCliente').modal('show');
                    l.AddCliente();
                    l.alt = false;      
                    $(".alt").hide();                      
                break;
            }
        }
    });

    $(".precios input,.porcentajes input").on('change',function(){
       var name = $(this).attr('name');
       var valor = $(this).val();       
       $('input[data-name="'+name+'"]').val(valor);
    });

    $(".porcentajes input").on('change',function(){
        var valor = parseFloat($(this).val());
        var costo = parseFloat($("#field-precio_costo").val());
        var precio = 0;
        if(!isNaN(valor) && !isNaN(costo)){
            precio = costo + (valor*costo/100);
        }
        $(this).parents('.precios_wrap').find('.precios input').val(precio.toFixed(0)).trigger('change');
    });

    $(document).on('change','[data-name="por_desc"]',function(){
        var precio = parseFloat($(this).parents('tr').find('[data-name="precio_venta"]').val());
        var por = parseFloat($(this).val());
        var desc = 0;        
        if(!isNaN(precio) && !isNaN(por)){
            desc = l.redondeo((por*precio)/100);
        }
        $(this).parents('tr').find('[data-name="precio_desc"]').val(Math.ceil(desc));
        l.updateEmpty();
        l.updateTemp();
    });

    $(document).on('change','[data-name="precio_desc"]',function(){
        var precio = parseFloat($(this).parents('tr').find('[data-name="precio_venta"]').val());
        var desc = parseFloat($(this).val());
        var por = 0;        
        if(!isNaN(precio) && !isNaN(desc)){
            por = ((desc*100)/precio);
        }
        $(this).parents('tr').find('[data-name="por_desc"]').val(por.toFixed(2));
        l.updateEmpty();
        l.updateTemp();
    });
}

Venta.prototype.redondeo = function (valor){
    if(valor<100){
        return valor;
    }               
    var centena = valor.toString().slice(-2);
    if(centena<50){
        valor-= centena;
    }else if(centena>50){
        valor+= 100-centena;
    }
    return valor<0?Math.abs(valor):valor;
}

Venta.prototype.getBalanza = (codigo,medida) =>{
	if(codigo.toString().length!=13){
        return codigo;
    }
    medida = typeof medida == 'undefined'?'peso':medida;
    for(var i in ajustes.codigo_balanza){
        var cc = ajustes.codigo_balanza[i];
        var numerosBalanza = cc.toString().length;
        if(codigo.toString().substring(0,numerosBalanza)==cc){
            var peso = parseInt(codigo.toString().substring(7,12));
            peso = medida=='peso'?parseInt(peso)/1000:parseFloat(peso);
            codigo = parseInt(codigo.toString().substring(2,7));
            codigo = {
                codigo: codigo,
                cantidad: peso
            }
            return codigo;  
        }
    }
    return codigo;
}

Venta.prototype.formatearCodigo = function (codigo){	
	codigo = codigo.split('*');
    if(codigo.length==2){
        cantidad = codigo[0];
        codigo = codigo[1];
    }else{
        codigo = codigo[0];
        codigo = codigo.split('+');
        if(codigo.length==2){
            cantidad = codigo[0];
            codigo = codigo[1];                    
            /*if(ajustes.activar_mayorista_por_venta == '1'){
                forceMayorista = true;
            }*/
        }
        else{
            cantidad = 1;
            codigo = codigo[0];
        }
    }
    if(typeof cantidad.substring == 'function' && cantidad.substring(0,1)=='m'){
        cantidad = parseFloat(cantidad.substring(1));
        cantidad = isNaN(cantidad)?1:cantidad;
        forceMayorista = true;
    }else{
        cantidad = parseFloat(cantidad);
    }
    
    //Es balanza?   
    var codigoOriginal = codigo;        
    codigo = this.getBalanza(codigo);
    var balanza = typeof codigo == 'object'?1:0;
    cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
    codigo = typeof codigo == 'object'?codigo.codigo:codigo;      
    return {
    	codigo: codigo,
    	cantidad: cantidad,
    	balanza: balanza,
    	venta:1
    };
}

Venta.prototype.updateEmpty = function(){
	if(this.tbody.find('tr').length>1){
		this.tbody.find('.empty').hide();
	}else{
		this.tbody.find('.empty').show();
	}
	this.updateTotales();
}

Venta.prototype.formatCurrency = function(input,decimales){   
    var val = input;     
    input = parseInt(input);
    input = isNaN(input)?0:input;        
    let num = (input === 0) ? "" : input.toLocaleString("en-US");
    input = num.split(",").join("."); 
    input = input==''?0:input;
    if(decimales){
        val = parseFloat(val);
        //val = val.toLocaleString("en-US");
        val = val.toFixed(2);
        val = val.split('.');
        if(val.length==2){
            input+= ','+val[1];
        }
    }       
    return input;
}

Venta.prototype.updateTotales = function(){
	var productos = $(".producto");	
	$("#cant").html(productos.length);
    this.fixScroll();
}

Venta.prototype.addProducto = function(data){
	var newTr = this.tr.clone();
	newTr.removeAttr('id');	
	var cols = newTr.find('th');
	for(var i in data){
		newTr.find('[data-val="'+i+'"]').html(data[i]);
		newTr.find('[data-name="'+i+'"]').val(data[i]);
	}	
	this.tbody.append(newTr);
	//Añadir campos extras
	var extras = newTr.find('.extraFields');
	for(var i in data){
		if(newTr.find('[data-name="'+i+'"]').length==0){
			val = data[i]?data[i]:0;
			extras.append('<input type="hidden" data-name="'+i+'" value="'+val+'">');
		}
	}
	this.updateEmpty();	
}

Venta.prototype.checkIfPushed = function(codigo,cant){	
	var codigos = $("input[data-name='codigo']");
	if(codigos.length>0){
		for(var i=0;i<codigos.length;i++){
			if($(codigos[i]).val()==codigo){
				return true;
			}
		}
	}
	return false;
}

Venta.prototype.addProduct = async function(codigo){
	if(codigo!=''){
        window.shouldSave = true;
		$('#codigoAdd').attr('placeholder','Buscando Producto').val('');
		try{
			codigo = this.formatearCodigo(codigo);
			var form = new FormData();
		    form.append('codigo',codigo.codigo);
		    form.append('cantidad',codigo.cantidad);
		    form.append('balanza',codigo.balanza);
		    form.append('venta',1); 
		    
		    if(!this.checkIfPushed(codigo.codigo,codigo.cantidad)){
			    let data = {
					method: 'POST',
					body: form		
				};
				const response = await fetch(URI+'movimientos/descuentos/productos/json_list',data).then((response)=>{
					$('#codigoAdd').attr('placeholder','Código de producto (ALT + C)');					
					return response;
				});
				const datos = await response.json();
				if(datos.length>0){
					let producto = datos[0];
					producto.por_desc = 0;
					producto.tipo_cantidad = 1;
                    producto.precio_desc = 0;
                    producto.stocks = datos[0].stocks.length==0?0:datos[0].stocks[0].stock;
                    
					this.addProducto(producto);
				}	
			}
            this.updateTemp();
		}catch(e){
			alert('Ocurrio un error al insertar');
			console.log(e);
		}
	}else{
		$('#codigoAdd').val('').attr('placeholder','Ingresa un código de producto').attr('disabled',false);
	}
}

/***************************************** Enviar Venta *******************************************/
Venta.prototype.transformProductsInJson = function(){
    var prods = [];
    var productos = $(".producto");
    for(var i=0;i<productos.length;i++){
        var producto = $(productos[i]);
        var valores = producto.find('input');
        var prod = [];
        for(var k=0;k<valores.length;k++){
        	var valor = $(valores[k]);
            prod[valor.data('name')] = valor.val();
      	}

      	valores = producto.find('select');
      	for(var k=0;k<valores.length;k++){
        	var valor = $(valores[k]);
            prod[valor.data('name')] = valor.val();
      	}
        prods.push(Object.assign({},prod));
    }
    return prods;
}

Venta.prototype.save = function(form){  
    if(!this.onsend){
        this.onsend = true;  
        $('.report').html('').removeClass('alert alert-danger alert-success alert-info');
        var url = $(form).attr('action');
        form = new FormData(form);
        form.append('productos',JSON.stringify(this.transformProductsInJson()));
        


        insertar(url,form,'.report',function(data){
            for(var i in data){
                data.success_message = data.success_message.replace('{id}',data.insert_primary_key);
            }
            venta.imprimir(data.insert_primary_key);
            $(document).find('.report').removeClass('alert').removeClass('alert-info');
            $(document).find('.report').addClass('alert alert-success').html(data.success_message);
            $(form).find('input[type="text"]').val('');
            $(form).find('input[type="number"]').val('');
            $(form).find('input[type="password"]').val('');
            $(form).find('select').val('');
            $(form).find('textarea').val('');            
            window.shouldSave = false;
            venta.nuevaVenta();
        },function(data){
          $(document).find('.report').removeClass('alert').removeClass('alert-info');
          $(document).find('.report').addClass('alert alert-danger').html(data.error_message);
          venta.onsend = false;
        });
    }else{
        error('.report','Ya se ha enviado una venta, por favor inicie una nueva venta');
    }
}

Venta.prototype.edit = function(form){      
    $('.report').html('').removeClass('alert alert-danger alert-success alert-info');
    var url = $(form).attr('action');
    form = new FormData(form);
    form.append('productos',JSON.stringify(this.transformProductsInJson()));
    


    insertar(url,form,'.report',function(data){
        for(var i in data){
            data.success_message = data.success_message.replace('{id}',data.insert_primary_key);
        }        
        $(document).find('.report').removeClass('alert').removeClass('alert-info');
        $(document).find('.report').addClass('alert alert-success').html(data.success_message);
        $(form).find('input[type="text"]').val('');
        $(form).find('input[type="number"]').val('');
        $(form).find('input[type="password"]').val('');
        $(form).find('select').val('');
        $(form).find('textarea').val('');                    
    },function(data){
      $(document).find('.report').removeClass('alert').removeClass('alert-info');
      $(document).find('.report').addClass('alert alert-danger').html(data.error_message);      
    });
}

Venta.prototype.imprimir = function(codigo){
    var url = 'reportes/rep/verReportes/261/html/valor/';
    var w = window.open(URI+url+codigo,'Impresion','width=800,height=600');
    if(ajustes.cerrarAlImprimirVenta=='1'){
        w.onload = function(){            
            setTimeout(function(){w.close()},3000);
        }        
    }
}

Venta.prototype.nuevaVenta = function(){
    if(!window.shouldSave || confirm('¿Seguro desea crear una nueva venta y perder los datos ya almacenados?')){
        $(".producto").remove();
        $('.cabecera input:not([type="radio"])').val('');
        $('.report').html('').removeClass('alert alert-danger alert-success alert-info');
        this.updateEmpty();
        this.updateTemp(true);
        venta.onsend = false;
    }
}

Venta.prototype.fixScroll = function(){
    setTimeout(function(){
        var box = document.getElementById('seguimiento');
        box.scrollTo(0,box.scrollHeight);
    },50);
}


/************************************************ Cruds **************************/


Venta.prototype.updateTemp = function(force){
    if($(".producto").length>0 || force){
        $.post(URI+'movimientos/descuentos/descuentos/updateTemp',{
            productos:JSON.stringify(this.transformProductsInJson())
        },function(data){
            if(data!='success'){
                alert(data);
                console.log(data);
            }
        });
    }
}

var venta = new Venta();


window.afterLoad.push(function(){
	venta.initEvents();
    venta.onsend = false;
    venta.updateEmpty(); 
});