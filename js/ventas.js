var PuntoDeVenta = function(){
	
	this.productoHTML = $("#productoVacio").clone();
	this.productoHTML.removeAttr('id');
	this.productoHTML.find('a,input').show();
	this.productoshtml = $("#ventaDescr tbody");	
	this.alt = false;
	this.modPrecio = 1;
	this.chequesContent = $("#addCheque");
	this.chequesForm = $($("#addCheque table tbody tr")[0]);
	this.refrshEvents = false;
    this.doUpdate = true;
	this.initEvents = function(){
        var l = this;
        //Seleccionar cliente
        $(document).on('change','#cliente',function(){
            l.datos.cliente = $(this).val();
            l.datos.empleado = $(this).find('option:selected').data('empleado');
            let plan = $(this).find('option:selected').data('plan');
            if(plan=='1'){
                l.datos.plan = 1;
                //$("#field-transaccion").val(2).chosen().trigger('liszt:updated').trigger('change');
                $("input[name='plan'][value='1']").prop('checked',true);
                $("input[name='plan']").attr('disabled',true);
            }else if(plan=='0'){
                l.datos.plan = 0;
                //$("#field-transaccion").val(1).chosen().trigger('liszt:updated').trigger('change');
                $("input[name='plan'][value='0']").prop('checked',true);
                $("input[name='plan']").attr('disabled',true);
            }
            if($(this).find('option:selected').data('mayorista')!='undefined'){
                l.datos.mayorista = $(this).find('option:selected').data('mayorista');
                $("#direccion_cliente").val($(this).find('option:selected').data('direccion'));
                $("#telefono_cliente").val($(this).find('option:selected').data('celular'));
            }
            l.updateFields();
            if(typeof(ajustes.recalcular_mayoristas) == 'undefined' || ajustes.recalcular_mayorista==1){
                l.updatePrecios();
            }
        });
        
        $("#cliente").ajaxChosenJsonList({
           dataType: 'json',
            type: 'POST',
            url:URI+'maestras/clientes/json_list',
            success:function(data,val){l.selectClient(data,val);}
        },{
            loadingImg: URI,
            processItems:function(data){
            	var d = [];
            	for(var i in data){
            		d.push({id:data[i].id,text:data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos});
            	}
            	return d;
            }
        },{
            "search_contains": true, allow_single_deselect:true
        });   

	    //Campo añadir codigo
	    $(document).on('change','#codigoAdd',function(){
	    	//l.addProduct($(this).val());
	    });
	    $(document).on('change','#usuario',function(){
	    	l.datos.usuario = $(this).val();
	    });
	    $(document).on('change','#field-pedido',function(){
	    	l.updateFields();
	    });
	    $(document).on('change','#field-pagado',function(){
	    	l.updateFields();
	    });
        $(document).on('change','#pagare',function(){
            l.datos.pagare = $(this).val();
            l.updateFields();
        });
	    
	    $(document).on('keyup','#codigoAdd',function(e){
	    	if(e.which==13){
	    		//$(this).trigger('change');
	    		l.addProduct($(this).val());
	    	}
	    });

	    $(document).on('click','.insertar',function(){
	    	l.addProduct($('#codigoAdd').val());
	    });

	    $(document).on('keyup','input[name="pago_guaranies"],input[name="pago_pesos"],input[name="pago_dolares"],input[name="pago_reales"],input[name="total_debito"]',function(e){	    	
	    	if(e.which==13){
	    		//$(this).trigger('change');
	    		sendVenta();
	    	}
	    });


	    
	    /* Event precio, cantidad en productos */
	    $(document).on('change','.precio',function(){
	    	var precio_venta = parseFloat($(this).val());
	    	var codigo = $(this).parents('tr').data('codigo');
            var enc = -1;
	    	for(var i in l.datos.productos){
	    		if(
                    (controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                    l.datos.productos[i].codigovenc==codigo
                ){
	    			enc = i; break;
	    		}
	    	}

            if(enc>=0){
                i = enc;
                if(isNaN(parseFloat(precio_venta))){
                    $(this).val(l.datos.productos[i].precio_venta);
                    return;
                }
                if(parseFloat(l.datos.productos[i].precio_venta)>parseFloat(precio_venta)){
                    l.validateToken().then(()=>{                                                
                        l.datos.productos[i].precio_venta = precio_venta;
                        l.updateFields();
                    }).catch(()=>{
                        l.updateFields();
                    });
                }else{
                    l.datos.productos[i].precio_venta = precio_venta;
                    l.updateFields();
                }
            }
	    });

	    //Event calcular vuelto
	    $(document).on('change','input[name="efectivo"]',function(){
	    	l.datos.total_efectivo = parseFloat($(this).val());
	    	l.updateFields();
	    });

	    //Event Remove
	    $(document).on('click','.rem',function(){
	    	l.removeProduct($(this).parents('tr').data('codigo'));
	    });

	    $(document).on('change','.cantidad',function(){
            var cantidad = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var enc = -1;
            var forceMayorista = false;
            for(var i in l.datos.productos){
                if((controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) || l.datos.productos[i].codigovenc==codigo){enc = i; break;}
            }            
            if(enc>=0){
                i = enc;
                if(ajustes.activar_mayorista_por_venta == '1'){
                    forceMayorista = true;
                }
                if(isNaN(cantidad)){
                    $(this).val(l.datos.productos[i].cantidad);
                    return;
                }
                if(parseFloat(l.datos.productos[i].cantidad)>parseFloat(cantidad)){
                    l.validateToken().then(()=>{
                        console.log(i,l.datos.productos[i].cantidad,cantidad);
                        l.datos.productos[i].cantidad = cantidad;
                        l.datos.productos[i].precio_venta = l.getPrecio(l.datos.productos[i],cantidad,forceMayorista);
                        l.updateFields();
                    }).catch(()=>{
                        l.updateFields();
                    });
                }else{
                    l.datos.productos[i].cantidad = cantidad;
                    l.datos.productos[i].precio_venta = l.getPrecio(l.datos.productos[i],cantidad,forceMayorista);
                    l.updateFields();
                }
            }
	    });
	    $(document).on('change','#formapago',function(){            
	    	l.datos.inflar_por_forma_pago = parseFloat($("#formapago option:selected").data('porventa'));
            for(var i in l.datos.productos){                
                l.datos.productos[i].precio_venta = l.getPrecio(l.datos.productos[i],l.datos.productos[i].cantidad);
            }
            l.updateFields();
	    });

	    //Event descuentos
	    $(document).on('change','.por_desc',function(){	    	
	    	var tr = $(this).parents('tr').data('codigo');
	    	var descuento = parseFloat($(this).val());
	    	for(var i in l.datos.productos){
	    		if(l.datos.productos[i].codigo==tr || l.datos.productos[i].codigovenc == tr){
                    var descmax = parseFloat(l.datos.productos[i].descmax);
		    		if(!isNaN(descmax) && descmax>0 && descuento > descmax){
                        descuento = descmax;
                    }
                    l.datos.productos[i].por_desc = descuento;
		    		l.datos.productos[i].precio_descuento = (descuento*l.datos.productos[i].precio_venta)/100;
	    		}
	    	}
	    	l.updateFields();
	    });
	    //Event descuentos
	    $(document).on('change','.precio_descuento',function(){
	    	var tr = $(this).parents('tr').data('codigo');
            var descuento = $(this).val();
            descuento = descuento.replace(/\./g,'');
	    	descuento = parseFloat(descuento);
	    	for(var i in l.datos.productos){
	    		if(l.datos.productos[i].codigo==tr || l.datos.productos[i].codigovenc == tr){
		    		l.datos.productos[i].precio_descuento = descuento;
		    		l.datos.productos[i].por_desc = (descuento*100)/l.datos.productos[i].precio_venta;

                    var descmax = parseFloat(l.datos.productos[i].descmax);
                    if(!isNaN(descmax) && descmax>0 && l.datos.productos[i].por_desc > descmax){
                        $(this).parents('tr').find('.por_desc').val(descmax).trigger('change');
                    }
                    
	    		}
	    	}
	    	l.updateFields();
	    });

	    $(document).on('change','#field-transaccion',function(){
            
            if($(this).val()!='2' && l.datos.empleado=='1'){
                l.validateToken().then(()=>{
                    l.updatePrecios();                    
                    l.updateFields();                    
                }).catch(() => {
                    $(this).val(2).chosen().trigger('liszt:updated');
                    l.datos.transaccion = 2;
                    l.updatePrecios();                    
                    l.updateFields();                    
                });                
            }
	    	
            if($(this).val()=='2' && ajustes.descontar_a_credito == '0'){	    		
		    	for(var i in l.datos.productos){
		    		l.datos.productos[i].precio_descuento = 0;
		    		l.datos.productos[i].por_desc = 0;
		    	}       
                l.updatePrecios();                      
                l.updateFields();                
	    	}
	    });

	    //Event calcular vuelto
	    $(document).on('change','input[name="pago_dolares"]',function(){
            var val = $(this).val();
            val = val.replace(/[\D\s\._\-]+/g, "");
	    	l.datos.pago_dolares = !isNaN(parseFloat(val))?parseFloat(val):l.datos.pago_dolares;
	    	l.updateFields();
	    });

	    //Event calcular vuelto
	    $(document).on('change','input[name="pago_reales"]',function(){
            var val = $(this).val();
            val = val.replace(/[\D\s\._\-]+/g, "");
	    	l.datos.pago_reales = !isNaN(parseFloat(val))?parseFloat(val):l.datos.pago_reales;
	    	l.updateFields();
	    });

	    //Event calcular vuelto
	    $(document).on('change','input[name="pago_pesos"]',function(){
            var val = $(this).val();
            val = val.replace(/[\D\s\._\-]+/g, "");
	    	l.datos.pago_pesos = !isNaN(parseFloat(val))?parseFloat(val):l.datos.pago_pesos;
	    	l.updateFields();
	    });

	    //Event calcular vuelto
	    $(document).on('keyup','input[name="pago_guaranies"]',function(){
            var val = $(this).val();
            val = val.replace(/[\D\s\._\-]+/g, "");
	    	l.datos.pago_guaranies = !isNaN(parseFloat(val))?parseFloat(val):l.datos.pago_guaranies;
	    	l.updateFields();
	    	$(this).val(val);
	    });

        $(document).on('change','input[name="total_debito"]',function(){
            var val = $(this).val();
            val = val.replace(/[\D\s\._\-]+/g, "");
            l.datos.total_debito = !isNaN(parseFloat(val))?parseFloat(val):l.datos.total_debito;
            l.updateFields();
        });

	    /*$(document).on('blur','input[name="pago_guaranies"]',function(){	    	
	    	l.updateFields();
	    });*/

        $(document).on('click','input[name="plan"]',function(){
            l.datos.plan = $(this).val();
        });
        $(document).on('click','input[name="pagare"]',function(){
            l.datos.pagare = $(this).val();
        });

	    

	    //Otros campos
	    $(document).on('change','#field-transaccion,#observacion,#repartidor',function(){
            l.updateFields();l.updatePrecios();});

	    $(document).on('change','.tipoFacturacion',function(){
	    	l.datos.tipo_facturacion_id = $(this).val();
	    	if($(this).find('option:selected').data('emitefactura')==1){
	    		$(".facturaLegalDiv").show();
	    	}else{
	    		$(".facturaLegalDiv").hide();
	    	}
	    	l.updateFields();
	    });

        //Formatos al escribir
        $(document).on('keydown','input[name="precio_descuento"],input[name="por_desc"],input[name="cantidad"],input[name="pago_guaranies"],input[name="pago_pesos"],input[name="pago_dolares"],input[name="pago_reales"],input[name="total_debito"]',function(e){
            //Last digit            
            var c = e.which;           
            if(c!=110 && c!=9 && c!=13 && c!=37 && c!=39 && c!=46 && c!=8 && c!=188 && c!=190 && (c<48 || c>57) && (c<96 || c>105)){
                e.preventDefault();
            }            
        });
        
        $(document).on('keyup','input[name="precio_descuento"],input[name="por_desc"],input[name="pago_guaranies"],input[name="pago_pesos"],input[name="pago_dolares"],input[name="pago_reales"],input[name="total_debito"]',function(e){
            var c = e.which; 
            if((c>=48 && c<=57) || (c>=96 && c<=105)){
                let input = $(this).val();
                input = input.replace(/[\D\s\._\-]+/g, "");
                input = input ? parseInt(input, 10) : 0;
                $(this).val(l.formatCurrency(input));    
            }      
        });
        $(document).on('keydown',function(e){

            if(e.which==18 && !l.alt){
                e.preventDefault();
                l.alt = true; 
                $(".alt").show();               
            }

            else if(e.which==18 && l.alt){
                e.preventDefault();
                l.alt = false;                
                $(".alt").hide();
            }

            if(l.alt){
                e.preventDefault();
            }
        });
	    $(document).on('keyup',function(e){                        
	    	if(l.alt){   

                switch(e.which){
                    case 67: //[c] Focus en codigos
                        $("#codigoAdd").focus();
                        l.alt = false;
                        $(".alt").hide();
                    break;
                    case 73: //[i] busqueda avanzada
                        $("#inventarioModal").modal('toggle');
                        l.alt = false;  
                        $(".alt").hide();                          
                    break;
                    case 80: //[p] Procesar venta
                        $("#procesar").modal('toggle');        
                        l.alt = false;  
                        $(".alt").hide();                  
                    break;
                    case 78: //[n] Nueva venta
                        nuevaVenta();
                        l.alt = false;
                        $(".alt").hide();
                    break;
                    case 66: //[B] Nueva venta
                        saldo();
                        l.alt = false;
                        $(".alt").hide();
                    break;
                    case 72: //[H] Enfocar en la cabecera
                        //$('a[href="#addCliente"]').focus();
                        $('#cliente').chosen().trigger('liszt:activate'); 
                        l.alt = false;  
                        $(".alt").hide();                        
                    break;
                    case 87: //[W] New Window
                        window.open($("#newWindow").attr('href'),'_new');
                        l.alt = false;
                        $(".alt").hide();
                    break;
                    case 38: //UP
                        //Cantidad focus
                        var index = 0;
                        var focused = $('.cantidad:focus');
                        if(focused.length>0){
                            index = $('.cantidad:focus').parents('tr').index();
                            index-=1;                                
                            index = index<0?0:index;
                        }
                        $($('.cantidad')[index]).focus(); 
                        l.alt = false; 
                        $(".alt").hide();                            
                    break;
                    case 40: //DOWN
                        //Cantidad focus
                        var index = 0;
                        var focused = $('.cantidad:focus');                            
                        if(focused.length>0){
                            index = $('.cantidad:focus').parents('tr').index();
                            index+=1;                                
                            index = index>$('.cantidad').length?$('.cantidad').length-1:index;
                        }
                        $($('.cantidad')[index]).focus(); 
                        l.alt = false;      
                        $(".alt").hide();                      
                    break;

                    case 88: //X
                        //Cantidad focus
                        $('#addCliente').modal('show');
                        l.alt = false;      
                        $(".alt").hide();                      
                    break;
                }
            }
	    });

        $(document).on('change','.vencimiento',function(){
            var vencimiento = $(this).val();
            var codigo = $(this).parents('tr').data('codigo');  
            for(var i in l.datos.productos){
                if(
                    (controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                    l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].vencimiento = vencimiento;                    
                    if(controlar_vencimiento==1){
                        l.datos.productos[i].stock = $($(this).find('option:selected')).data('stock');
                        l.datos.productos[i].codigovenc = l.datos.productos[i].codigo+vencimiento;                      
                        $(this).parents('tr').attr('data-codigo',codigo+vencimiento)
                    }
                }
            }
            l.updateFields();
        });



	}

    this.formatCurrency = function(input,decimales){   
        var val = input;     
        input = parseInt(input);
        input = isNaN(input)?0:input;        
        let num = (input === 0) ? "" : input.toLocaleString("en-US");
        input = num.split(",").join("."); 
        input = input==''?0:input;
        if(decimales){
            val = parseFloat(val);
            //val = val.toLocaleString("en-US");
            val = val.toFixed(2);
            val = val.split('.');
            if(val.length==2){
                input+= ','+val[1];
            }
        }       
        return input;
    }

	this.selectClient = function(data,val){
		var opt = '';
        for(var i in data){
          data[i].mayorista = data[i].mayorista=='activo' || data[i].mayorista=='1'?1:0;
          var emp = data[i].empleado==1 && data[i].user_id && data[i].user_id > 0?1:0;
          opt+= '<option value="'+data[i].id+'" data-promo="'+data[i].promo+'" data-plan="'+data[i].plan_credito+'" data-direccion="'+data[i].direccion+'" data-celular="'+data[i].celular+'" data-mayorista="'+data[i].mayorista+'" data-empleado="'+emp+'">'+data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos+'</option>';
        }
        if(data.length==0){
          opt+= '<option value="">Cliente no existe</option>';
        }
        $("#cliente").html(opt);
        $("#cliente").chosen().trigger('liszt:updated');        
    	$("#cliente").trigger('change');
    	//$("#cliente_chzn input[type='text']").val($("#cliente option:selected").html());
    	this.updateFields();
	}

	this.selectUsuario = function(data,val){
		var opt = '';
        for(var i in data){          
          opt+= '<option value="'+data[i].id+'">'+data[i].nombre+' '+data[i].apellido+'</option>';
        }
        if(data.length==0){
          opt+= '<option value="">Cliente no existe</option>';
        }
        $("#usuario").html(opt);
        $("#usuario").chosen().trigger('liszt:updated');
        $("#usuario_chzn input[type='text']").val(val);
    	$("#usuario").trigger('change');
    	this.updateFields();
	}

    this.updatePrecios = function(){
        for(var i in this.datos.productos){            
            let newprecio = this.getPrecio(this.datos.productos[i],this.datos.productos[i].cantidad,false);
            newprecio = isNaN(newprecio)?this.datos.productos[i].precio_venta:newprecio;
            this.datos.productos[i].precio_venta = newprecio;
            //this.datos.productos[i].precio_original = this.datos.productos[i].precio_venta;
        }
        this.updateFields();
    }
        
    this.getPrecio = function(producto,cantidad,forceMayorista){         
        var precio = this.datos.transaccion==1 || isNaN(parseFloat(producto.precio_credito)) || producto.precio_credito<=0?parseFloat(producto.precio_venta_contado):parseFloat(producto.precio_credito);                        
        var cant_1 = parseFloat(isNaN(producto.cant_1))?0:producto.cant_1;
        var cant_2 = parseFloat(isNaN(producto.cant_2))?0:producto.cant_2;
        var cant_3 = parseFloat(isNaN(producto.cant_3))?0:producto.cant_3;
        if($('#cliente option:selected').data('promo')=='1'){
            cant_3 = cant_2;
            cant_2 = cant_1;
            cant_1 = 1;
        }
        /*if(ajustes.activar_mayorista_por_venta == '1'){
            if(!forceMayorista){
                this.datos.mayorista = 0;
            }else{
                this.datos.mayorista = 1;
            }
        }*/           
        if(this.datos.empleado=='0'){
            producto.precio_original = producto.precio_venta_contado;
        }

        if(
            (this.datos.transaccion==1 || isNaN(parseFloat(producto.precio_credito)) || parseFloat(producto.precio_credito)==0) && 
            ajustes.activar_mayorista_por_venta == '1' && 
            ((this.datos.mayorista == 1 || this.datos.empleado=='1' || $('#cliente option:selected').data('promo')=='1') && parseInt(ajustes.rango_precio_cantidades)==1) &&
            (
                parseFloat(producto.precio_venta_mayorista1) > 0 &&
                parseFloat(producto.precio_venta_mayorista2) > 0 &&
                parseFloat(producto.precio_venta_mayorista3) > 0 
            )
        ){
            var enc = false;
            if(parseFloat(cant_1)!=0 || parseFloat(cant_2)!=0 || parseFloat(cant_3)!=0){
                if(this.datos.empleado=='1' || this.datos.mayorista=='1'){
                    precio = producto.precio_original?parseFloat(producto.precio_original):parseFloat(producto.precio_venta);
                }
            	if(parseFloat(cant_1)>0 && parseFloat(cantidad)>=parseFloat(cant_1)){
                    precio = parseFloat(producto.precio_venta_mayorista1);
                }
                if((this.datos.transaccion=='2' && this.datos.empleado=='1') || parseFloat(cant_2)>0 && parseFloat(cantidad)>=parseFloat(cant_2)){
                    precio = parseFloat(producto.precio_venta_mayorista2);
                }
                if((this.datos.transaccion=='1' && this.datos.empleado=='1') || parseFloat(cant_3)>0 && parseFloat(cantidad)>=parseFloat(cant_3)){
                    precio = parseFloat(producto.precio_venta_mayorista3);
                }
            }else{
	            for(var i in cantidades_mayoristas){
	            	if(!enc && parseFloat(cantidad)>=parseFloat(cantidades_mayoristas[i].desde)){
	                    var campo = cantidades_mayoristas[i].campo;
	                    precio = parseFloat(producto[campo]);
	                }
	            }
        	}
        }
        if(ajustes.inflar_por_forma_pago=='1' && this.datos.inflar_por_forma_pago != 0){
			precio += (precio*this.datos.inflar_por_forma_pago)/100;				
		}
        return precio;
    }

	this._addProduct = async function(producto,cantidad,forceMayorista){
        cantidad = cantidad!=undefined?cantidad:1;
        var pr = this.datos.productos;
        var enc = false;
        for(var i in pr){
            if(pr[i].codigo==producto.codigo && (controlar_vencimiento == 0 || pr[i].vencimiento==producto.vencimiento.split(',')[0])){
                enc = true;
                this.datos.productos[i].cantidad+=cantidad;
                this.datos.productos[i].precio_venta = this.getPrecio(this.datos.productos[i],this.datos.productos[i].cantidad,forceMayorista);
            }
        }
        if(!enc){            
            if(parseInt($('.tipoFacturacion option:selected').data('max_items'))>-1 && (pr.length+1)>parseInt($('.tipoFacturacion option:selected').data('max_items'))){
                alert('La cantidad de items excede del máximo permitido para este tipo de facturación, por favor elimine los items excedentes y regístrelos en una nueva facturación');
                return;
            }
            var precio_descuento = 0;
            if(producto.descuentos.length>0){
                producto.por_desc = parseFloat(producto.descuentos[0].por_desc); 
                producto.por_desc_producto = parseFloat(producto.descuentos[0].por_desc);
                producto.descmax = parseFloat(producto.descuentos[0].por_desc);
                producto.precio_descuento = parseFloat(producto.descuentos[0].precio_desc);
                precio_descuento = parseFloat(producto.descuentos[0].precio_desc);
            }else{
                producto.precio_descuento = 0;
                producto.por_desc = 0;
                precio_descuento = 0;
            }
            var precio = 0;
            if(producto.servicios=='0' || producto.servicios=='inactivo'){
                precio = this.getPrecio(producto,cantidad,forceMayorista);            
            }else{
                precio = await this.getPrecioServicio(producto);
            }
            
            if(parseFloat(producto.descmax)>0 && producto.descuentos.length==0){        			
		    	var descuento = parseFloat(producto.descmax);			    			    		
		    	precio_descuento = (descuento*precio)/100;		    		
			}
            
              
            
            this.datos.productos.push({
                    codigo:producto.codigo,
                    nombre:producto.nombre_comercial,
                    cantidad:cantidad,
                    stock:producto.stock,
                    descmax:producto.descmax,
                    por_desc:parseFloat(producto.descmax),
                    precio_venta:precio,
                    precio_original:precio,
                    precio_credito:producto.precio_credito,
                    cant_1:producto.cant_1,
                    cant_2:producto.cant_2,
                    cant_3:producto.cant_3,
                    precio_venta_contado:parseFloat(producto.precio_venta),
                    precio_venta_mayorista1: parseFloat(producto.precio_venta_mayorista1),
                    precio_venta_mayorista2: parseFloat(producto.precio_venta_mayorista2),
                    precio_venta_mayorista3: parseFloat(producto.precio_venta_mayorista3),
                    precio_descuento:precio_descuento,
                    mod_precio: producto.mod_precio,
                    total: 0                        
            });            
            if(controlar_vencimiento==1){
                this.datos.productos[this.datos.productos.length-1].vencimientoOpts = !producto.vencimiento || typeof(producto.vencimiento)=='undefined'?[]:producto.vencimiento.split(',');
                this.datos.productos[this.datos.productos.length-1].stockOpts = producto.stock.split(',');
                this.datos.productos[this.datos.productos.length-1].vencimiento = this.datos.productos[this.datos.productos.length-1].vencimientoOpts[0];
                this.datos.productos[this.datos.productos.length-1].stock = this.datos.productos[this.datos.productos.length-1].stock[0];
                this.datos.productos[this.datos.productos.length-1].codigovenc = producto.codigo+this.datos.productos[this.datos.productos.length-1].vencimiento;                
            }
            this.refrshEvents = true;
        } 
           
        this.updateFields();
        
	}
        
    this.getBalanza = function(codigo,medida){
        if(codigo.toString().length!=13){
            return codigo;
        }
        medida = typeof medida == 'undefined'?'peso':medida;
        for(var i in codigo_balanza){
            var cc = codigo_balanza[i];
            var numerosBalanza = cc.toString().length;
            if(codigo.toString().substring(0,numerosBalanza)==cc){
                var peso = parseInt(codigo.toString().substring(7,12));
                peso = medida=='peso'?parseInt(peso)/1000:parseFloat(peso);
                codigo = parseInt(codigo.toString().substring(2,7));
                codigo = {
                    codigo: codigo,
                    cantidad: peso
                }
                return codigo;  
            }
        }
        return codigo;
    }

	this.addProduct = function(codigo,obj){                                
		//buscamos el codigo
		var l = this;				
        var forceMayorista = false;
		if(codigo!=''){
            if(codigo=='0'){
                $("#codigoAdd").val('');
                $("#codigoAdd").attr('placeholder','Producto no encontrado');
                $("#codigoAdd").addClass('error');
                return;
            }
            codigo = codigo.split('*');
            if(codigo.length==2){
                cantidad = codigo[0];
                codigo = codigo[1];
            }else{
                codigo = codigo[0];
                codigo = codigo.split('+');
                if(codigo.length==2){
                    cantidad = codigo[0];
                    codigo = codigo[1];                    
                    if(ajustes.activar_mayorista_por_venta == '1'){
                        forceMayorista = true;
                    }
                }
                else{
                    cantidad = 1;
                    codigo = codigo[0];
                }
            }
            if(typeof cantidad.substring == 'function' && cantidad.substring(0,1)=='m'){
                cantidad = parseFloat(cantidad.substring(1));
                cantidad = isNaN(cantidad)?1:cantidad;
                forceMayorista = true;
            }else{
                cantidad = parseFloat(cantidad);
            }
            
            //Es balanza?   
            var codigoOriginal = codigo;                                     
            codigo = this.getBalanza(codigo);
            var balanza = typeof codigo == 'object'?1:0;
            cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
            codigo = typeof codigo == 'object'?codigo.codigo:codigo;            
            $.post(URI+'movimientos/productos/productos/json_list',{                    
                    'codigo':codigo,
                    'cliente':this.datos.cliente,
                    'balanza':balanza,
                    'venta':1
            },function(data){
                    data = JSON.parse(data);
                    if(data.length>0){
                    	if(vender_sin_stock==1 || (cantidad>0 && parseFloat(data[0].stock)>=cantidad) || (data[0].inventariable=='inactivo')){
                            $("#codigoAdd").val('');
                            if(data[0].unidad_medida_id==1){
                                //Verificar si es por balanza y si es por cantidad
                                codigo = l.getBalanza(codigoOriginal,'unidad');                                                                
                                cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
                                codigo = typeof codigo == 'object'?codigo.codigo:codigo;
                            }

                            if(controlar_vencimiento==1 && typeof(obj)=='object'){
                                data[0].vencimiento = obj.vencimiento;
                                data[0].stock = obj.stock;
                            }

                            data[0].precio_venta_contado = data[0].precio_venta;

                            $("#codigoAdd").attr('placeholder','Código de producto');
                            $("#codigoAdd").removeClass('error');
                            $("#foto").html(data[0].foto_principal.replace('50px"','50px" style="width: 50%;height: auto;margin: 0 auto"'));                            
                            l._addProduct(data[0],cantidad,forceMayorista);
                        }else{
                        	$("#codigoAdd").val('');
                            $("#codigoAdd").attr('placeholder','Sin stock suficiente');
                            $("#codigoAdd").addClass('error');
                        }
                    }else{
                            alert('Producto no encontrado');
                            $("#codigoAdd").val('');
                            $("#codigoAdd").attr('placeholder','Producto no encontrado');
                            $("#codigoAdd").addClass('error');
                    }
            });
		}
	}

	this.removeProduct = function(codigo){
        if(this.validateToken().then(()=>{
    		if(codigo!=''){
    			var pr = this.datos.productos;
    			var pr2 = [];
    			for(var i in pr){
    				if(pr[i].codigo!=codigo && pr[i].codigovenc!=codigo){
    					pr2.push(pr[i]);
    				}
    			}
    		}
    		this.datos.productos = pr2;
    		this.refrshEvents = true;
    		this.updateFields();
        }));
	}

	this.updateFields = function(){
		this.datos.cliente = $("#cliente").val();
		this.datos.transaccion = $("#field-transaccion").val();
		this.datos.forma_pago = $("#formapago").val();
		this.datos.inflar_por_forma_pago = parseFloat($("#formapago option:selected").data('porventa'));
		this.datos.observacion = $("#observacion").val();						
		this.datos.pedido = $("#field-pedido").val();
		this.datos.pagado = $("#field-pagado").val();
        this.datos.observaciones = $("input[name='observaciones']").val();
        this.datos.repartidor_id = $("#repartidor").val();
		//Calcular total
		var pr = this.datos.productos;
		var total = 0;
		for(var i in pr){			
			//Inflar si esta activo inflar_por_pago
			var precio_venta = pr[i].precio_venta;			
			this.datos.productos[i].total = pr[i].cantidad*(precio_venta-pr[i].precio_descuento);
			total+= this.datos.productos[i].total;
		}
		this.datos.total_venta = total;
		//Calcular divisas
		this.datos.total_dolares = total/tasa_dolar;
		this.datos.total_reales = total/tasa_real;
		this.datos.total_pesos = total/tasa_peso;
		//Calcular acumulado
		this.datos.total_efectivo = 0;
		this.datos.total_efectivo+= this.datos.pago_guaranies;
		this.datos.total_efectivo+= tasa_dolar>0?this.datos.pago_dolares/tasa_dolar:0;
		this.datos.total_efectivo+= tasa_real>0?this.datos.pago_reales/tasa_real:0;
		this.datos.total_efectivo+= tasa_peso>0?this.datos.pago_pesos/tasa_peso:0;		
		//this.datos.vuelto = this.datos.vuelto<0?0:this.datos.vuelto;
		//Calcular acumulado
		this.datos.total_pagado = 0;		
		this.datos.total_pagado+= this.datos.pago_guaranies;
		this.datos.total_pagado+= tasa_dolar>0?this.datos.pago_dolares*tasa_dolar:0;
		this.datos.total_pagado+= tasa_real>0?this.datos.pago_reales*tasa_real:0;
		this.datos.total_pagado+= tasa_peso>0?this.datos.pago_pesos*tasa_peso:0;
		this.datos.total_pagado+= this.datos.total_debito>0?this.datos.total_debito:0;		
		this.datos.total_pagado+= this.datos.total_cheque;		
		//Calcular vuelto		
		this.datos.vuelto = this.datos.total_pagado>0?this.datos.total_pagado - this.datos.total_venta:0;
		this.datos.vuelto_dolares = this.datos.pago_dolares>0?this.datos.pago_dolares - this.datos.total_dolares:0;
		this.datos.vuelto_reales =  this.datos.pago_reales>0?this.datos.pago_reales - this.datos.total_reales:0;
		this.datos.vuelto_pesos =   this.datos.pago_pesos>0?this.datos.pago_pesos - this.datos.total_pesos:0;		
		this.datos.vuelto_dolares = this.datos.vuelto_dolares<0?0:this.datos.vuelto_dolares;
		this.datos.vuelto_reales =  this.datos.vuelto_reales<0?0:this.datos.vuelto_reales;
		this.datos.vuelto_pesos =   this.datos.vuelto_pesos<0?0:this.datos.vuelto_pesos;		
        window.shouldSave = true;
		this.refresh();
	}

	this.refresh = function(){        
		$("input[name='total_venta']").val(this.formatCurrency(this.datos.total_venta));
		$("input[name='total_pesos']").val(this.formatCurrency(this.datos.total_pesos));
		$("input[name='total_reales']").val(this.formatCurrency(this.datos.total_reales));
		$("input[name='total_dolares']").val(this.formatCurrency(this.datos.total_dolares,true));
		$("input[name='total_cheque']").val(this.formatCurrency(this.datos.total_cheque));		
        $("input[name='total_debito']").val(this.formatCurrency(this.datos.total_debito));
		$("input[name='pago_guaranies']").val(this.formatCurrency(this.datos.pago_guaranies));
		$("input[name='pago_pesos']").val(this.formatCurrency(this.datos.pago_pesos));
		$("input[name='pago_reales']").val(this.formatCurrency(this.datos.pago_reales));
		$("input[name='pago_dolares']").val(this.formatCurrency(this.datos.pago_dolares));
        $("input[name='pago_debito']").val(this.formatCurrency(this.datos.pago_debito));		
		$("input[name='efectivo']").val(this.datos.total_efectivo);
		$("input[name='vuelto']").val(this.formatCurrency(this.datos.vuelto));
		$("input[name='vuelto_pesos']").val(this.formatCurrency(this.datos.vuelto_pesos));
		$("input[name='vuelto_reales']").val(this.formatCurrency(this.datos.vuelto_reales));
		$("input[name='vuelto_dolares']").val(this.formatCurrency(this.datos.vuelto_dolares));		
        $("input[name='observaciones']").val(this.datos.observaciones);
		$("#field-pedido").val(this.datos.pedido);
		$("#field-pedido").chosen().trigger('liszt:updated');
		$("#field-pagado").val(this.datos.pagado);		
		$("#field-pagado").chosen().trigger('liszt:updated');
		$("#field-transaccion").val(this.datos.transaccion);		
		$("#field-transaccion").chosen().trigger('liszt:updated');
        $("#repartidor").val(this.datos.repartidor_id).chosen().trigger('liszt:updated');
        $("select[name='forma_pago']").val(this.datos.forma_pago);
        $("select[name='forma_pago']").chosen().trigger('liszt:updated');

		$("#total_total span").html(this.formatCurrency(this.datos.total_venta));
		$("#total_vuelto span").html(this.formatCurrency(this.datos.vuelto));
		
		if(this.datos.transaccion=='1'){
			$(".patternCredito").css('display','none');
			$("#pago_total span").html(this.formatCurrency(this.datos.total_pagado));
            $(".plan").hide();
		}else{
			$(".patternCredito").css('display','block');
			$("#pago_total span").html(this.formatCurrency(this.datos.total_venta));
            $(".plan").show();
		}


		$("#cantidadProductos").html(this.datos.productos.length);		
		$("#procesar_total_efectivo").html(this.formatCurrency(this.datos.total_efectivo));
		$("#nroFactura").html(this.datos.nro_factura);
		$(".tipoFacturacion").val(this.datos.tipo_facturacion_id);
        $(".tipoFacturacion").chosen().trigger('liszt:updated');
		if($(".tipoFacturacion option:selected").data('emitefactura')=='1'){
			$(".facturaLegalDiv").show();
		}else{
			$(".facturaLegalDiv").hide();
		}

		//Draw products
		var pr = this.datos.productos;
		this.productoshtml.html('');		
		for(var i=pr.length-1;i>=0;i--){
			var newRow = this.productoHTML.clone();
			var td = newRow.find('td');
			var c = pr[i].codigo;
            if(controlar_vencimiento==1){
                c+= pr[i].vencimiento;
            }
            newRow.attr('data-codigo',c);
			$(td[0]).find('span').html(pr[i].codigo);
			$(td[1]).find('span').html(pr[i].nombre);
			$(td[2]).find('input').val(pr[i].cantidad);
			$(td[3]).find('input').val(pr[i].precio_venta.toFixed(0));
            
			if(this.modPrecio==0 && pr[i].mod_precio==0){
				$(td[3]).find('input').attr('disabled',true);
			}
			var pordesc = !isNaN(parseFloat(pr[i].por_desc))?parseFloat(pr[i].por_desc).toFixed(2):0;
			var preciodesc = this.formatCurrency(pr[i].precio_descuento.toFixed(0));
			if(ajustes.descontar_a_credito == '0' && $("#field-transaccion").val()=='2'){
				pordesc = 0;
				preciodesc = 0;
			}
			$(td[4]).find('input').val(pordesc);
			$(td[5]).find('input').val(preciodesc);
            if(controlar_vencimiento==1){
                var opts = '';
                for(var k in pr[i].vencimientoOpts){
                    var sel = pr[i].vencimiento == pr[i].vencimientoOpts[k]?'selected="true"':'';
                    var lb = pr[i].vencimientoOpts[k];
                    lb = lb.split('-');
                    lb = lb[1]+'/'+lb[0];
                    opts+= '<option '+sel+' data-stock="'+pr[i].stockOpts[k]+'" value="'+pr[i].vencimientoOpts[k]+'">'+lb+'</option>';
                }                
                $(td[6]).find('select').html(opts);
                $(td[7]).html(this.formatCurrency(pr[i].total));
                $(td[8]).html(pr[i].stock);
            }else{
    			$(td[6]).html(this.formatCurrency(pr[i].total));
    			$(td[7]).html(pr[i].stock);
            }
			this.productoshtml.append(newRow);			
		}
		if(ajustes.permitir_descuento == '0' || (this.datos.transaccion==2 && ajustes.descontar_a_credito=='0')){
			$('.descuento').hide();
		}else{
			$('.descuento').show();
		}

		if(this.datos.forma_pago == '4'){
			$("#chequeInp").show();
		}else{
			$("#chequeInp").hide();
		}
        this.updateTemp();
        window.shouldSave = true;
	}

	this.setDatos = function(datos){		
		this.datos = datos;
		this.datos.total_venta = parseFloat(this.datos.total_venta);
		this.datos.total_pesos = parseFloat(this.datos.total_pesos);
		this.datos.total_reales = parseFloat(this.datos.total_reales);
		this.datos.total_dolares = parseFloat(this.datos.total_dolares);
		this.datos.vuelto = parseFloat(this.datos.vuelto);
		this.datos.total_efectivo = parseFloat(this.datos.total_efectivo);			
		for(var i in this.datos.productos){
			this.datos.productos[i].cantidad = parseFloat(this.datos.productos[i].cantidad);
			this.datos.productos[i].precio_venta = parseFloat(this.datos.productos[i].precio_venta);
			this.datos.productos[i].por_desc = ajustes.descontar_a_credito == '1' || this.datos.transaccion!=2?parseFloat(this.datos.productos[i].por_desc):0;
			this.datos.productos[i].precio_descuento = ajustes.descontar_a_credito == '1' || this.datos.transaccion!=2?parseFloat(this.datos.productos[i].precio_descuento):0;
			this.datos.productos[i].total = parseFloat(this.datos.productos[i].total);
		}	
        
		var l = this;
		setCliente({success:true,insert_primary_key:this.datos.cliente});
		/*$.post(URI+'seguridad/user/json_list',{   
            search_field:'id',
            search_text:this.datos.usuario,
            operator:'where'
        },function(data){       
            data = JSON.parse(data);   
            l.selectUsuario(data,l.datos.usuario);
        });        */
        
        $('#usuario').val(this.datos.usuario).chosen().trigger('liszt:updated');
		this.refresh();	
	}

	this.initVenta = function(){
		this.datos = {			
			sucursal:'',
			caja:'',
			cajadiaria:'',
			fecha:'',
			nro_factura:'',
			usuario:'',
			cliente:0,
			transaccion:1,
			forma_pago:0,
			observaciones:'',							
			total_pesos:0,
			total_descuentos:0,
			total_venta:0,
			total_dolares:0,
			total_reales:0,
			total_efectivo:0,
			total_cheque:0,
			pago_dolares:0,
			pago_reales:0,
			pago_pesos:0,
			pago_guaranies:0,
			total_debito:0,
			vuelto_dolares:0,
			vuelto_reales:0,
			vuelto_pesos:0,		
			vuelto:0,
			productos:[],
            mayorista:0,
            pedido:0,
            pagado:0,
            plan:0,
            repartidor_id:0,
            pagare:0,
            cheques:[]
                                
		};
		this.reloadNroFactura();
        $("#codigoAdd").focus();
		$(".facturaLegalDiv").hide();
		this.getTotalCheque();
		this.clearChequeModal();
	}

    this.reloadNroFactura = function(){
        var l = this;
        $.get(URI+'movimientos/ventas/ventas/nroFactura',{},function(data){
            l.datos.nro_factura = data;
            l.refresh();        
            l.updateTemp();    
        });
    }

	


	//Cheques
	this.clearChequeModal = function(){
		this.datos.cheques = [];
		for(var i=0;i<$('#addCheque table tbody tr').length-1;i++){
			$($('#addCheque table tbody tr')[i]).remove();
		}
	}
	this.getTotalCheque = function(){
		var totalCheque = 0;
		for(var i in this.datos.cheques){
			totalCheque+= parseFloat(this.datos.cheques[i].monto);
		}
		this.datos.total_cheque = totalCheque		
		this.updateFields();
	}
	this.remCheque = function(el){
		var cheque = $($(el).parents('tr').find('td')[1]).html();
		if(cheque!=''){
			if(this.datos.cheques.find(x => x.cheque==cheque)){
				$(el).parents('tr').remove();
			}
		}
	}
	this.addCheque = function(){		
		var dato = {};
		var labels = {};		
		clear('#addCheque .mensajes');
		for(var i=0;i<this.chequesForm.find('select,input').length;i++){
			dato[$(this.chequesForm.find('select,input')[i]).attr('name')] = $(this.chequesForm.find('select,input')[i]).val();
			if($(this.chequesForm.find('select,input')[i]).is('select')){
				labels[$(this.chequesForm.find('select,input')[i]).attr('name')] = $(this.chequesForm.find('select,input')[i]).find('option:selected').html();
			}else{
				labels[$(this.chequesForm.find('select,input')[i]).attr('name')] = $(this.chequesForm.find('select,input')[i]).val();
			}
		}
		if(dato.vencimiento!='' && dato.bancos_id!='' && dato.cheque!='' && !isNaN(parseFloat(dato.monto)) && parseFloat(dato.monto) > 0 && !this.datos.cheques.find(x => x.cheque==dato.cheque)){			
			this.datos.cheques.push(dato);
			var tr = this.chequesForm.clone();
			tr.find('td').html('');	
			var x = 0;
			for(var i in dato){
				$(tr.find('td')[x]).html(labels[i]);
				x++;
			}
			tr.find('td:last-child').html('<span onclick="venta.remCheque(this)" style="cursor:pointer"><i class="fa fa-times"></i></span>');
			this.chequesForm.before(tr);
			this.chequesForm.find('select,input').val('');
			this.chequesForm.find('select').val(0);

			this.getTotalCheque();
		}else{
			error('#addCheque .mensajes','Faltan datos para poder añadir este cheque');
		}			
	}

	this.showChequeModal = function(){
		$("#procesar").modal('hide');
		$("#addCheque").modal('show');
	}

	this.closeChequeModal = function(){
		$("#procesar").modal('show');
		$("#addCheque").modal('hide');
	}

	this.initVenta();
}

PuntoDeVenta.prototype.updateTemp = function(){
    if(ajustes.ventastemp=='0'){
        return;
    }
    if(this.datos.productos.length>0 && this.doUpdate && !editar){
        $.post(URI+'movimientos/ventas/ventas/updateTemp',{
            productos:JSON.stringify(this.datos.productos)
        },function(data){
            if(data!='success'){
                alert(data);
                console.log(data);
            }
        });
    }
}

PuntoDeVenta.prototype.loadTemp = function(){
    if(ajustes.ventastemp=='0'){
        return;
    }
    let $this = this;
    $.get(URI+'movimientos/ventas/ventas/getTemp',{},function(data){
        data = JSON.parse(data);
        if(data.length>0){
            $this.datos.productos = data;
            $this.updatePrecios();            
        }
    });
}

PuntoDeVenta.prototype.validateToken = function(){
    return new Promise((resolve,reject)=>{
        if(ajustes.solicitar_token_ventas=='0'){
            resolve();
        }else{
            validarToken().then((result)=>{
                if(result){
                    resolve();
                }else{
                    reject();
                }
            });
        }
    });
}

PuntoDeVenta.prototype.getPrecioServicio = (producto)=>{
    return new Promise((resolve,reject)=>{
        getPrecioServicio(resolve);
    });
}