var PuntoDeVenta = function(){
	
	this.productoHTML = $("#productoVacio").clone();
	this.productoHTML.removeAttr('id');
	this.productoHTML.find('a,input').show();
	this.productoshtml = $("#ventaDescr tbody");
	this.alt = false;
	this.modPrecio = 1;
	this.initEvents = function(){
        var l = this;                
	    //Campo añadir codigo
	    $(document).on('change','#codigoAdd',function(){
	    	//l.addProduct($(this).val());
	    });
	    $(document).on('keyup','#codigoAdd',function(e){
	    	if(e.which==13){
	    		//$(this).trigger('change');
	    		l.addProduct($(this).val());
	    	}
	    });

	    $(document).on('click','.insertar',function(){
	    	l.addProduct($('#codigoAdd').val());
	    });
	    
	    /* Event precio, cantidad en productos */
	    $(document).on('change','.precio',function(){
	    	var precio_venta = parseFloat($(this).val());
	    	var codigo = $(this).parents('tr').data('codigo');
	    	for(var i in l.datos.productos){
	    		if(
                    (controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                    l.datos.productos[i].codigovenc==codigo
                ){
	    			l.datos.productos[i].precio_venta = precio_venta;	    			
	    		}
	    	}
	    	l.updateFields();
	    });

	    //Event Remove
	    $(document).on('click','.rem',function(){
	    	l.removeProduct($(this).parents('tr').data('codigo'));
	    });

	    $(document).on('change','.cantidad',function(){
            var cantidad = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(
                    (controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                    l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].cantidad = cantidad;
                    l.datos.productos[i].precio_venta = l.getPrecio(l.datos.productos[i],cantidad);
                }
            }
            l.updateFields();
	    });

        $(document).on('change','#motivo_transferencia_id',function(){
            l.datos.motivo_transferencia_id = $(this).val();
            l.updateFields();
	    });

        

	    $(document).on('change','select[name="sucursal_destino"], select[name="sucursal_origen"]',function(){            
            l.updateFields();
	    });
	    //Atajos
	    $(document).on('keydown',function(e){
	    	if(e.which==18){
	    		l.alt = true;
	    	}
	    });

	    //Atajos
	    $(document).on('keyup',function(e){
	    	if(e.which==18){
                l.alt = false;
	    	}else{
                if(l.alt){                	
                    switch(e.which){
                        case 67: //[c] Focus en codigos
                                $("#codigoAdd").focus();
                        break;
                        case 73: //[i] busqueda avanzada
                                $("#inventarioModal").modal('toggle');		    				
                        break;
                        case 80: //[p] Procesar venta
                                $("#procesar").modal('toggle');		    				
                        break;
                        case 78: //[n] Nueva venta
                                nuevaVenta();
                        break;
                        case 66: //[B] Nueva venta
                                saldo();
                        break;
                    }
                }
	    	}

	    });

	    $(document).on('change','.vencimiento',function(){
            var vencimiento = $(this).val();
            var codigo = $(this).parents('tr').data('codigo');  
            for(var i in l.datos.productos){
                if(
                    (controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                    l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].vencimiento = vencimiento;                    
                    if(controlar_vencimiento==1){
                        l.datos.productos[i].stock = $($(this).find('option:selected')).data('stock');
                        l.datos.productos[i].codigovenc = l.datos.productos[i].codigo+vencimiento;                      
                        $(this).parents('tr').attr('data-codigo',codigo+vencimiento)
                    }
                }
            }
            l.updateFields();
        });
	}
        
    this.getPrecio = function(producto,cantidad){         
        var precio = parseFloat(producto.precio_venta);  
        var cant_1 = parseFloat(isNaN(producto.cant_1))?0:producto.cant_1;
        var cant_2 = parseFloat(isNaN(producto.cant_2))?0:producto.cant_2;
        var cant_3 = parseFloat(isNaN(producto.cant_3))?0:producto.cant_3;
        if(this.datos.mayorista==1 && parseInt(ajustes.rango_precio_cantidades)==1){
            var enc = false;            
            if(parseFloat(cant_1)!=0 || parseFloat(cant_2)!=0 || parseFloat(cant_3)!=0){            	
            	if(parseInt(cantidad)>=parseInt(cant_1)){                    
                    precio = parseFloat(producto.precio_venta_mayorista1);
                }                
                if(parseInt(cantidad)>=parseInt(cant_2)){                    
                    precio = parseFloat(producto.precio_venta_mayorista2);
                }                
                if(parseInt(cantidad)>=parseInt(cant_3)){                    
                    precio = parseFloat(producto.precio_venta_mayorista3);
                }
            }else{
	            for(var i in cantidades_mayoristas){
	            	if(!enc && parseInt(cantidad)>=parseInt(cantidades_mayoristas[i].desde)){
	                    var campo = cantidades_mayoristas[i].campo;                                                    
	                    precio = parseFloat(producto[campo]);
	                }
	            }
        	}
        }
        return precio;
    }

	this._addProduct = function(producto,cantidad){
        cantidad = cantidad!=undefined?cantidad:1;
        var pr = this.datos.productos;
        var enc = false;
        for(var i in pr){
            if(pr[i].codigo==producto.codigo && (controlar_vencimiento == 0 || pr[i].vencimiento==producto.vencimiento.split(',')[0])){
                enc = true;
                this.datos.productos[i].cantidad+=cantidad;				
            }
        }
        if(!enc){
                var precio = this.getPrecio(producto,cantidad);
                var precio_descuento = 0;
                if(parseFloat(producto.descmax)>0){        			
			    	var descuento = parseFloat(producto.descmax);			    			    		
			    	precio_descuento = (descuento*precio)/100;		    		
    			}                     
                this.datos.productos.push({
                        codigo:producto.codigo,
                        nombre:producto.nombre_comercial,
                        cantidad:cantidad,
                        stock:producto.stock,
                        por_desc:parseFloat(producto.descmax),
                        precio_venta:precio,
                        precio_costo:parseFloat(producto.precio_costo),
                        cant_1:producto.cant_1,
                        cant_2:producto.cant_2,
                        cant_3:producto.cant_3,
                        precio_venta_mayorista1: parseFloat(producto.precio_venta_mayorista1),
                        precio_venta_mayorista2: parseFloat(producto.precio_venta_mayorista2),
                        precio_venta_mayorista3: parseFloat(producto.precio_venta_mayorista3),
                        precio_descuento:precio_descuento,
                        total: 0,
                        sucursal:this.datos.sucursal
                });

                if(controlar_vencimiento==1){
                    this.datos.productos[this.datos.productos.length-1].vencimientoOpts = producto.vencimiento.split(',');
                    this.datos.productos[this.datos.productos.length-1].stockOpts = producto.stock.split(',');
                    this.datos.productos[this.datos.productos.length-1].vencimiento = this.datos.productos[this.datos.productos.length-1].vencimientoOpts[0];
                    this.datos.productos[this.datos.productos.length-1].stock = this.datos.productos[this.datos.productos.length-1].stock[0];
                    this.datos.productos[this.datos.productos.length-1].codigovenc = producto.codigo+this.datos.productos[this.datos.productos.length-1].vencimiento;
                }
        } 
           
        this.updateFields();
        
	}
        
    this.getBalanza = function(codigo){
        var numerosBalanza = codigo_balanza.toString().length;
        if(codigo.toString().substring(0,numerosBalanza)==codigo_balanza){
            var peso = parseInt(codigo.toString().substring(7,12));
            peso = parseInt(peso)/1000;
            codigo = parseInt(codigo.toString().substring(2,7));
            codigo = {
                codigo: codigo,
                cantidad: peso
            }
            return codigo;  
        }else{
            return codigo;  
        }
    }

	this.addProduct = function(codigo,obj){                                
		//buscamos el codigo
		var l = this;		
		let cantidad = 0;		
		if(codigo!=''){
            codigo = codigo.split('*');
            if(codigo.length==2){
                    cantidad = parseFloat(codigo[0]);
                    codigo = codigo[1];
            }else{
                    codigo = codigo[0];
                    codigo = codigo.split('+');
                    if(codigo.length==2){
                            cantidad = parseFloat(codigo[0]);
                            codigo = codigo[1];	
                    }
                    else{
                            cantidad = 1;
                            codigo = codigo[0];	
                    }
            }
            
            //Es balanza?                                        
            codigo = this.getBalanza(codigo);                                        
            cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
            codigo = typeof codigo == 'object'?codigo.codigo:codigo;
            console.log(codigo); 
            $.post(URI+'movimientos/productos/productos/json_list',{
                    'codigo':codigo,
                    'operator':'where'
            },function(data){
                    data = JSON.parse(data);
                    if(data.length>0){
                    	if(vender_sin_stock==1 || (cantidad>0 && parseFloat(data[0].stock)>=cantidad) || (data[0].inventariable=='inactivo')){
                            $("#codigoAdd").val('');
                            if(controlar_vencimiento==1 && typeof(obj)=='object'){
                                data[0].vencimiento = obj.vencimiento;
                                data[0].stock = obj.stock;
                            }                            
                            $("#codigoAdd").attr('placeholder','Código de producto');
                            $("#codigoAdd").removeClass('error');
                            l._addProduct(data[0],cantidad);
                        }else{
                        	$("#codigoAdd").val('');
                            $("#codigoAdd").attr('placeholder','Sin stock suficiente');
                            $("#codigoAdd").addClass('error');
                        }
                    }else{
                            $("#codigoAdd").val('');
                            $("#codigoAdd").attr('placeholder','Producto no encontrado');
                            $("#codigoAdd").addClass('error');
                    }
            });
		}
	}

	this.removeProduct = function(codigo){
		if(codigo!=''){
			var pr = this.datos.productos;
			var pr2 = [];
			for(var i in pr){
				if(pr[i].codigo!=codigo && pr[i].codigovenc!=codigo){
					pr2.push(pr[i]);
				}
			}
		}
		this.datos.productos = pr2;
		this.updateFields();
	}

	this.updateFields = function(){
		this.datos.sucursal_origen = $("#sucursal_origen").val();
		this.datos.sucursal_destino = $("#sucursal_destino").val();
		this.datos.fecha_solicitud = $("#fecha_solicitud").val();
        this.datos.motivo_transferencia_id = $("#motivo_transferencia_id").val();		
		//Calcular total
		var pr = this.datos.productos;
		var total = 0;
		for(var i in pr){
			this.datos.productos[i].total = pr[i].cantidad*pr[i].precio_costo;
			total+= this.datos.productos[i].total;
		}
		this.datos.total_venta = total;
		this.datos.total_costo = total;
		//Calcular divisas
		this.datos.total_dolares = total/tasa_dolar;
		this.datos.total_reales = total/tasa_real;
		this.datos.total_pesos = total/tasa_peso;
		this.refresh();
	}

	this.refresh = function(){
		const gs = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'PYG',minimumFractionDigits: 0});
		const usd = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'USD',minimumFractionDigits: 2});
		const arg = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'ARP',minimumFractionDigits: 2});
		const br = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'BRL',minimumFractionDigits: 2});
		
		$("input[name='total_venta']").val(gs.format(this.datos.total_venta));
		$("input[name='total_pesos']").val(arg.format(this.datos.total_pesos));
		$("input[name='total_reales']").val(br.format(this.datos.total_reales));
		$("input[name='total_dolares']").val(usd.format(this.datos.total_dolares));		
		$("input[name='total_debito']").val(gs.format(this.datos.total_debito));
		$("#total_total span").html(gs.format(this.datos.total_venta));				
		$("#cantidadProductos").html(this.datos.productos.length);	
		$('select[name="sucursal_destino"]').val(this.datos.sucursal_destino);
		$('select[name="sucursal_origen"]').val(this.datos.sucursal_origen);
        $('select[name="motivo_transferencia_id"]').val(this.datos.motivo_transferencia_id);

		$('select[name="sucursal_destino"]').chosen().trigger('liszt:updated');							
		$('select[name="sucursal_origen"]').chosen().trigger('liszt:updated');
        $('select[name="motivo_transferencia_id"]').chosen().trigger('liszt:updated');

		//Draw products
		var pr = this.datos.productos;
		this.productoshtml.html('');		
		for(var i=pr.length-1;i>=0;i--){
			var newRow = this.productoHTML.clone();
			var td = newRow.find('td');
			var c = pr[i].codigo;
            if(controlar_vencimiento==1){
                c+= pr[i].vencimiento;
            }
            newRow.attr('data-codigo',c);
			$(td[0]).find('span').html(pr[i].codigo);
			$(td[1]).html(pr[i].nombre);
			$(td[2]).find('input').val(pr[i].cantidad);
			$(td[3]).find('input').val(pr[i].precio_costo.toFixed(0));
			if(this.modPrecio==0){
				$(td[3]).find('input').attr('disabled',true);
			}		
			if(controlar_vencimiento==1){
                var opts = '';
                for(var k in pr[i].vencimientoOpts){
                    var sel = pr[i].vencimiento == pr[i].vencimientoOpts[k]?'selected="true"':'';
                    var lb = pr[i].vencimientoOpts[k];
                    lb = lb.split('-');
                    lb = lb[1]+'/'+lb[0];
                    opts+= '<option '+sel+' data-stock="'+pr[i].stockOpts[k]+'" value="'+pr[i].vencimientoOpts[k]+'">'+lb+'</option>';
                }                
                $(td[4]).find('select').html(opts);
                $(td[5]).html(gs.format(pr[i].total));
                $(td[6]).html(pr[i].stock);
            }else{	
				$(td[4]).html(gs.format(pr[i].total));			
				$(td[5]).html(pr[i].stock);
			}
			this.productoshtml.append(newRow);			
		}		
	}

	this.setDatos = function(datos){
		this.datos = {...datos};
		this.datos.total_venta = parseFloat(this.datos.total_venta);
		this.datos.total_pesos = parseFloat(this.datos.total_pesos);
		this.datos.total_reales = parseFloat(this.datos.total_reales);
		this.datos.total_dolares = parseFloat(this.datos.total_dolares);
		for(var i in this.datos.productos){
			this.datos.productos[i].cantidad = parseFloat(this.datos.productos[i].cantidad);
			this.datos.productos[i].precio_costo = parseFloat(this.datos.productos[i].precio_costo);					
			this.datos.productos[i].total = parseFloat(this.datos.productos[i].total);			
		}	
		this.refresh();	
	}

	this.initVenta = function(){
		this.datos = {			
			sucursal_origen:'',
			sucursal_destino:'',
			caja:'',
			cajadiaria:'',
			fecha_solicitud:'',			
			user_id:'',
            motivo_transferencia_id:'',
			procesado:2,			
			total_venta:0,
			total_dolares:0,
			total_reales:0,
			total_efectivo:0,			
			total_debito:0,						
			total_costo:0,
			productos:[],
		};
	}

	this.initVenta();
}

