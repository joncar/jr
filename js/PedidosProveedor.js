var Compras  = function(){
	this.productoHTML = $("#productoVacio").clone();
	this.productoHTML.removeAttr('id');
	this.productoHTML.find('a,input').show();
	this.productoshtml = $("#ventaDescr tbody");
	this.inputs = {
		sucursal:$("select[name='sucursal']"),
		proveedor:$("select[name='proveedor']"),
		transaccion:$("select[name='transaccion']"),
		fecha:$("input[name='fecha']"),
		forma_pago:$("select[name='forma_pago']"),		
		vencimiento_pago:$("input[name='vencimiento_pago']"),
		total_compra:$("input[name='total_compra']"),
		tipo_facturacion_id:$("select[name='tipo_facturacion_id']"),
		pago_caja_diaria:$("select[name='pago_caja_diaria']"),
		total_pesos:$("input[name='total_pesos']"),
		total_reales:$("input[name='total_reales']"),
		total_dolares:$("input[name='total_dolares']"),		
	};
	this.initDatos = function(){
		this.datos = {
			 sucursal:window.sucursalConectada,
			 caja:0,
			 cajadiaria:0,
			 proveedor:1,
			 transaccion:1,
			 fecha:'00/00/0000',
			 forma_pago:1,
			 establecimiento:'000',
			 nro_factura:'',
			 timbrado:'',
			 vencimiento_pago:'00/00/0000',
			 total_compra:0,
			 total_descuento:0,
			 status:0,
			 tipo_facturacion_id:1,
			 pago_caja_diaria:0,
			 total_pesos:0,
			 total_reales:0,
			 total_dolares:0,
			 productos:[]
		};
	}
	this.initDatos();
	this.focused = undefined;
	this.focused2 = undefined;
	this.initEvents = function(){
		var l = this;
		$(document).on('change',"input,select",function(){			
			setTimeout(function(){l.updateData();},600);
		});

		

		$(document).on('change','.cantidad',function(){
            var cantidad = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(l.datos.productos[i].codigo==codigo){
                    l.datos.productos[i].cantidad = cantidad;
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.por_desc',function(){
            var por_desc = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(l.datos.productos[i].codigo==codigo){
                    l.datos.productos[i].por_desc = por_desc;
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.precio_costo',function(){
            var precio_costo = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(l.datos.productos[i].codigo==codigo){
                    l.datos.productos[i].precio_costo = precio_costo;
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.por_venta',function(){
            var por_venta = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(l.datos.productos[i].codigo==codigo){
                    l.datos.productos[i].por_venta = por_venta;
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.precio_venta',function(){
            var precio_venta = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(l.datos.productos[i].codigo==codigo){
                    l.datos.productos[i].precio_venta = precio_venta;
                }
            }
            l.updateData();
	    });

		//Atajos
	    $(document).on('keydown',function(e){
	    	if(e.which==18){
	    		l.alt = true;
	    	}
	    });
		//Atajos
	    $(document).on('keyup',function(e){
	    	if(e.which==18){
                l.alt = false;
	    	}else{
	            if(l.alt){	            
	                switch(e.which){
                        case 67: //[c] Focus en codigos
                                $("#codigoAdd").focus();
                        break;
                        case 73: //[i] busqueda avanzada
                                $("#inventarioModal").modal('toggle');		    				
                        break;
                        case 80: //[p] Procesar venta
                                save();		    				
                        break;
                        case 66:
                        	$("#addProduct").modal('toggle');
                        	setTimeout(function(){$("input[name='codigo_interno']").focus();},600);
                        break;
                        case 78: //[n] Nueva venta
                                nuevaCompra();                                
                        break;
	                }
	            }
	    	}

	    });

	    //Event Remove
	    $(document).on('click','.rem',function(){
	    	l.removeProduct($(this).parents('tr').data('codigo'));
	    });

	    $(document).on('change','#codigoAdd',function(){			
	    	l.addProduct($(this).val());
		});

	    $(document).on('click','.insertar',function(){
	    	l.addProduct($('#codigoAdd').val());
	    });

		$(document).on('focus','td input,td select,td textarea',function(){			
	    	l.focused = $(this).parents('tr').data('codigo');
	    	l.focused2 = $(this).attr('name');
		});
	}

	this.updateData = function(datos){		
		for(var i in this.inputs){
			this.datos[i] = this.inputs[i].val();
		}		
		var total = 0;
		for(var i in this.datos.productos){
			this.datos.productos[i].cantidad = parseFloat(this.datos.productos[i].cantidad);
			var mt = parseFloat(this.datos.productos[i].precio_costo) * parseFloat(this.datos.productos[i].cantidad);
			if(parseFloat(this.datos.productos[i].por_desc)>0){
				mt = mt-(mt*(parseFloat(this.datos.productos[i].por_desc)/100));
			}
			this.datos.productos[i].total = mt;
			total+= mt;
		}
		this.datos.total_compra = total;	
		//Calcular divisas
		this.datos.total_dolares = (total/tasa_dolar).toFixed(2);
		this.datos.total_reales = (total/tasa_real).toFixed(2);
		this.datos.total_pesos = (total/tasa_peso).toFixed(2);
		this.draw();	
	}

	this.draw = function(){		
		const gs = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'PYG',minimumFractionDigits: 0});
		$("#cantidadProductos").html(this.datos.productos.length);
		for(var i in this.inputs){
			this.inputs[i].val(this.datos[i]);
		}
		this.inputs.total_compra.val(gs.format(parseFloat(this.datos.total_compra)));
		//Draw products
		var pr = this.datos.productos;
		this.productoshtml.html('');		
		for(var i=pr.length-1;i>=0;i--){			
			var newRow = this.productoHTML.clone();
			var td = newRow.find('td');
			newRow.attr('data-codigo',pr[i].codigo);
			var total = parseFloat(pr[i].cantidad) * parseFloat(pr[i].precio_costo);
			if(parseFloat(pr[i].por_desc)>0){
				total = total-(total*(parseFloat(pr[i].por_desc)/100));
			}
			var precio_venta = parseFloat(pr[i].precio_venta);
			if(parseFloat(pr[i].por_venta)>0){
				por_venta = parseFloat(pr[i].precio_costo)*(parseFloat(pr[i].por_venta)/100);
				precio_venta = (por_venta+parseFloat(pr[i].precio_costo));
			}
			$(td[0]).find('span').html('<a href="'+URI+'movimientos/productos/productos/edit/'+pr[i].id+'" target="_new">'+pr[i].codigo+'</a>');
			$(td[1]).html(pr[i].nombre_comercial);
			$(td[2]).find('input').val(pr[i].cantidad);
			$(td[3]).find('input').val(parseFloat(pr[i].precio_costo).toFixed(0));						
			$(td[4]).find('input').val(parseFloat(pr[i].por_desc).toFixed(0));
			$(td[5]).find('input').val(parseFloat(pr[i].por_venta).toFixed(0));						
			$(td[6]).find('input').val(precio_venta.toFixed(0));						
			$(td[7]).html(gs.format(total));						
			$(td[8]).html(pr[i].stock);
			this.productoshtml.append(newRow);			
		}

		if(this.focused!=undefined && this.focused2!=undefined){
			//var foc = $('tr[data-codigo="'+this.focused+'"]').find('*[name="'+this.focused2+'"]').closest('td').find('input').focus();			
		}
		

		$("select.chosen-select").chosen().trigger('liszt:updated');
	}

	this._addProduct = function(producto,cantidad){
        cantidad = cantidad!=undefined?cantidad:1;
        var pr = this.datos.productos;
        var enc = false;
        for(var i in pr){
            if(pr[i].codigo==producto.codigo){
                enc = true;
                this.datos.productos[i].cantidad+=cantidad;				
            }
        }
        if(!enc){
            //var precio = this.getPrecio(producto,cantidad);                                
            producto.cantidad = cantidad;
            producto.por_desc = 0;
            producto.por_venta = 0;
            producto.precio_costo = isNaN(parseFloat(producto.precio_costo))?0:parseFloat(producto.precio_costo);
            this.datos.productos.push(producto);
        }

        this.updateData();
	}
        
    this.getBalanza = function(codigo){
        var numerosBalanza = codigo_balanza.toString().length;
        if(codigo.toString().substring(0,numerosBalanza)==codigo_balanza){
            var peso = parseInt(codigo.toString().substring(7,12));
            peso = parseInt(peso)/1000;
            codigo = parseInt(codigo.toString().substring(2,7));
            codigo = {
                codigo: codigo,
                cantidad: peso
            }
            return codigo;  
        }else{
            return codigo;  
        }
    }

	this.addProduct = function(codigo){                                
		//buscamos el codigo
		var l = this;				
		if(codigo!=''){
	        codigo = codigo.split('*');
	        if(codigo.length==2){
                cantidad = parseFloat(codigo[0]);
                codigo = codigo[1];
	        }else{
                codigo = codigo[0];
                codigo = codigo.split('+');
                if(codigo.length==2){
                    cantidad = parseFloat(codigo[0]);
                    codigo = codigo[1];	
                }
                else{
                    cantidad = 1;
                    codigo = codigo[0];	
                }
	        }
	        
	        //Es balanza?                                        
	        codigo = this.getBalanza(codigo);                                        
	        cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
	        codigo = typeof codigo == 'object'?codigo.codigo:codigo;
	        console.log(codigo); 
	        $.post(URI+'movimientos/productos/productos/json_list',{
	                'search_field[]':'codigo',
	                'search_text[]':codigo,
	                'anulado':0,
	                'operator':'where',
	                'cliente':this.datos.cliente
	        },function(data){
                data = JSON.parse(data);
                if(data.length>0){
                	$("#codigoAdd").val('');
                    l._addProduct(data[0],cantidad);
                    $("#codigoAdd").attr('placeholder','Código de producto');
                    $("#codigoAdd").removeClass('error');
                }else{
                        $("#codigoAdd").val('');
                        $("#codigoAdd").attr('placeholder','Producto no encontrado');
                        $("#codigoAdd").addClass('error');
                }
	        });
		}
	}

	this.removeProduct = function(codigo){
		if(codigo!=''){
			var pr = this.datos.productos;
			var pr2 = [];
			for(var i in pr){
				if(pr[i].codigo!=codigo){
					pr2.push(pr[i]);
				}
			}
		}
		this.datos.productos = pr2;
		this.updateData();
	}
}