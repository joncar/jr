function emergente(data,xs,ys,boton,header){


  var x = (xs==undefined)?(window.innerWidth/2)-325:xs;
  var y = (ys==undefined)?(window.innerHeight/2):ys;
  var b = (boton==undefined || boton)?true:false;
  var h = (header==undefined)?'Mensaje':header;
  if($(".modalEmergente").html()==undefined){
  $('body,html').animate({scrollTop: 0}, 800);
  var str = '';
            var str = '<!-- Modal -->'+
            '<div class="modal fade modalEmergente" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
            '<div class="modal-dialog">'+
            '<div class="modal-content">'+
            '<div class="modal-body">'+
            data+
            '</div>'+
            '<div class="modal-footer">'+
            '<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>'+
            '</div>'+
            '</div><!-- /.modal-content -->'+
            '</div><!-- /.modal-dialog -->'+
            '</div><!-- /.modal -->';
            $("body").append(str);
            $("#myModal").modal("toggle");
  }
  else
  {
    $(".modalEmergente .modal-body").html(data);
                    if($("#myModal").css('display')=='none')
                        $("#myModal").modal("toggle");
  }
}

function autocomplete(inp,url) {  
  this.inp = inp;
  this.url = url;
  this.ajax = undefined;
  this.val = '';  
  this.list = inp.parent().find('ul');
  this.events = function(){
    var l = this;
    this.inp.on('keyup',function(e){
      if($(this).val().length>3){
        l.list.show();
        l.val = $(this).val();
        l.search();
      }else{
        l.list.hide();
        l.val = '';
      }
    });

    this.inp.on('focus',function(e){
      if($(this).val().length>3){
        l.list.show();
        l.val = $(this).val();
      }
    });

    this.inp.on('blur',function(){
      l.list.hide();
      l.val = '';
    });
  }

  this.search = function(){
    var l = this;
    this.list.html('<li><a href="#">Buscando por favor espere...</a></li>');
    if(this.val!=''){
      if(typeof(this.ajax)!='undefined'){
        this.ajax.abort();
        this.ajax = undefined;
      }
      this.ajax = $.post(URL+url,{
        'q':this.val
      },function(data){
        data = JSON.parse(data);
        if(data.length>0){
          var str = '';
          for(var i in data){
            str+= '<li><a href="'+data[i].link+'">'+data[i].text+'</a></li>';
          }
          l.list.html(str);
        }else{
          l.list.html('<li><a href="#">No se encontraron resultados para mostrar</a></li>');
        }
      });
    }
  }

  this.events();
}

function remoteConnection(url,data,callback,callbackError){        
    var r;
    var ur = url.search('http')>-1?url:URI+url;    
    if(typeof(grecaptcha)!=='undefined'){
        grecaptcha.ready(function() {
            var r = grecaptcha.execute('6LdAkPQUAAAAACObrSDKM_UrgzjPikcmnAVVga7u', {action: 'action_name'})
            .then(function(token) {
                // Verifica el token en el servidor.
                    data.append('token',token);
                    return $.ajax({
                    url: ur,
                    data: data,
                    context: document.body,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:callback,
                    error:callbackError
                });
            });
        });

    }else{
        r = $.ajax({
            url: ur,
            data: data,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:callback,
            error:callbackError
        });
    }
    return r;
};

function insertar(url,datos,resultId,callback,callbackError){
    $(document).find(resultId).removeClass('alert alert-danger').html('');
    $(document).find(resultId).addClass('alert alert-info').html('Cargando, por favor espere');
    $("button[type=submit]").attr('disabled',true);
    var uri = url.replace('insert','insert_validation');
    uri = uri.replace('update','update_validation');        
    if(!(datos instanceof FormData) && (datos instanceof HTMLFormElement)){
        datos = new FormData(datos);
    }else if(!(datos instanceof FormData)){
        var d = new FormData();
        for(var i in datos){
            d.append(i,datos[i]);
        }
        datos = d;        
    }


    remoteConnection(uri,datos,function(data){
        $("button[type=submit]").attr('disabled',false);
        data = $(data).text();
        data = JSON.parse(data);
        
        if(data.success){
          remoteConnection(url,datos,function(data){
            data = $(data).text();            
            data = JSON.parse(data);
            if(typeof(callback)=='function'){
                callback(data);
            }else{
                data.success_message = data.success_message.replace(/{id}/g,data.insert_primary_key);
                $(document).find(resultId).removeClass('alert').removeClass('alert-info');
                $(document).find(resultId).addClass('alert alert-success').html(data.success_message);
            }
        },function(request) {
            console.log(request);
           $(document).find(resultId).html(request.responseText);
        });
        }else{
          $(document).find(resultId).removeClass('alert').removeClass('alert-info');
          $(document).find(resultId).addClass('alert alert-danger').html(data.error_message);
          if(typeof(callbackError)!=='undefined'){
            callbackError(data);
          }else{
            $(document).find(resultId).removeClass('alert').removeClass('alert-info');
            $(document).find(resultId).addClass('alert alert-danger').html(data.error_message);
          }
        }
    },function(request) {
        console.log(request);
       $(document).find(resultId).html(request.responseText);
    });
}

function sendForm(form,divResponseId,callback){
    divResponseId = typeof(divResponseId)==='undefined'?'#response':divResponseId;
    var url = $(form).attr('action');
    var response = $(form).find(divResponseId);
    var data = new FormData(form);
    remoteConnection(url,data,function(data){
        if(response!==undefined){
            response.html(data);
        }else{
            console.log(response);
        }
        console.log(callback);
        if(typeof callback!='undefined'){
          callback(data);
        }
    },function(request) {
        console.log(request);
       response.html(request.responseText);
    });
    return false;
}

function relation_dependency(){
    this.url = '';
    this.field = '';
    this.targetField = '';
    this.relation_dependency = '';
    this.valueFieldOnDB = '';
    this.labelFieldOnDB = '';
    this.selectorCSS = '#';
    this._select_dependency = function(callback){
      var l = this;
      $(document).on('change',this.selectorCSS+this.field.attr('id'),function(e){
            e.stopPropagation();
            $.post(URI+l.url,{
                'search_text[]':l.field.val(),
                'search_field[]':l.relationFieldOnDB,
                'operator':'where'
            },function(data){
                var data = JSON.parse(data);
                var opt = '<option>Seleccione una opción</option>';
                for(var i in data){
                    opt+= '<option value="'+data[i][l.valueFieldOnDB]+'">'+data[i][l.labelFieldOnDB]+'</option>';
                }
                l.targetField.html(opt); 
                if(typeof callback != 'undefined'){
                  callback(data,opt);
                }
            });
        });
    }

    this.select_dependency = function(url,field,targetField,relationFieldOnDB,valueFieldOnDB,labelFieldOnDB){
      this.url = url;
      this.field = field;
      this.targetField = targetField;
      this.relationFieldOnDB = relationFieldOnDB;
      this.valueFieldOnDB = valueFieldOnDB;
      this.labelFieldOnDB = labelFieldOnDB;
      this._select_dependency(function(data,opt){});
    }

    this.select_dependency_chosen = function(url,field,targetField,relationFieldOnDB,valueFieldOnDB,labelFieldOnDB){
      var l = this;
      this.url = url;
      this.field = field;
      this.targetField = targetField;
      this.relationFieldOnDB = relationFieldOnDB;
      this.valueFieldOnDB = valueFieldOnDB;
      this.labelFieldOnDB = labelFieldOnDB;
      this._select_dependency(function(data,opt){
        l.targetField.trigger('liszt:updated');
      });
    }
}

function autocomplete(inp,url,minLength,form) {  
  this.inp = inp;
  this.inp.attr('autocomplete','off');
  this.url = url;
  this.ajax = undefined;
  this.val = '';  
  this.list = inp.parent().find('ul');
  this.form = typeof form ==='undefined'?null:form;
  this.minLength = typeof minLength ==='undefined'?0:minLength;
  this.results = [];
  this.selected = -1;
  this.events = function(){
    var l = this;
    this.inp.on('keypress',function(e){           
      switch(e.which){
        case 13: //Enter
          l.onEnter(e);
        break;
      }
    });
    this.inp.on('keyup',function(e){         
      switch(e.which){        
        case 38: //UP
          e.preventDefault();
          if(l.results.length==0){
            l.search(e);
          }
          else{          
            l.selected = l.selected>0?l.selected-1:0;
            l.selected = typeof(l.results[l.selected])=='undefined'?0:l.selected;            
            var elegido = l.results[l.selected];
            l.inp.val(l.results[l.selected].text);
            l.list.find('li').removeClass('active');
            $(l.list.find('li')[l.selected]).addClass('active');  
          }        
        break;
        case 40: //Down
          e.preventDefault();
          if(l.results.length==0){
            l.search();
          }
          else{          
            l.selected = l.selected<l.results.length?l.selected+1:0;
            l.selected = typeof(l.results[l.selected])=='undefined'?0:l.selected;            
            var elegido = l.results[l.selected];
            l.inp.val(l.results[l.selected].text);
            l.list.find('li').removeClass('active');
            $(l.list.find('li')[l.selected]).addClass('active');  
          }        
        break;
        case 13: //ENTER          
          l.inp.blur();
        break;
        default:
          if($(this).val()!='' && $(this).val().length>=l.minLength){
            l.list.show();
            l.val = $(this).val();
            l.search();            
          }else{
            l.list.hide();
            l.val = '';
          }
        break;
      }         
    });

    this.inp.on('focus',function(e){      
      if($(this).val()!='' && $(this).val().length>=l.minLength){
        l.list.show();
        l.val = $(this).val();
        $(this).select();
      }
    });

    $(document).on('click',function(e){        
      if(e.target!=l.inp[0]){
          l.list.hide();
          l.val = '';
      }
    });
    this.inp.on('blur',function(e){        
      setTimeout(function(){l.list.hide();},600);
    });
  }

  this.onEnter = function(e){    
    if(this.selected!=-1){
      e.preventDefault();      
      this.inp.blur();
      this.list.hide();
      var lin = this.results[this.selected].link;      
      if(lin.search('javascript')==-1){        
        document.location.href=lin;
      }else{        
        $(document).find('a[href="'+lin+'"]')[0].click();
      }
      return;
    }                    
    if(this.list.length==1 && typeof this.results[0].link != 'undefined'){
      e.preventDefault();
      this.inp.blur();
      this.list.hide();
      document.location.href=this.results[0].link;
      return;
    }
  }


  this.validateSelected = function(){
    if(this.results.length>0){
      var escrito = this.inp.val();
      escrito = escrito.toLowerCase();
      for(var i in this.results){
        var r = this.results[i].text;
        r = r.toLowerCase();          
        if(r==escrito){
          this.selected = parseInt(i);
        }
      }
    }
  }
  
  this._search = function(){
      var l = this;
      l.results = [];
      l.selected = -1;
       if(typeof(this.ajax)!='undefined'){
              this.ajax.abort();
              this.ajax = undefined;
            }

            if(!this.form){
                    this.ajax = $.post(URL+this.url,{
                      'q':this.val
                    },function(data){
                      data = JSON.parse(data);
                      l.results = data;
                      l.validateSelected();
                      if(data.length>0){
                        var str = '';
                        for(var i in data){
                          str+= '<li><a href="'+data[i].link+'">'+data[i].text+'</a></li>';
                        }
                        l.list.html(str);
                      }else{
                        l.list.html('<li><a href="#">No se encontraron resultados para mostrar</a></li>');
                      }
                    });
            }else{
                var form = new FormData(this.form);
                form.append('q',this.val);
                this.ajax = remoteConnection(this.url,form,function(data){
                      data = JSON.parse(data);
                      l.results = data;
                      l.validateSelected();
                      if(data.length>0){
                        var str = '';
                        for(var i in data){
                          str+= '<li><a href="'+data[i].link+'">'+data[i].text+'</a></li>';
                        }
                        l.list.html(str);
                      }else{
                        l.list.html('<li><a href="#">No se encontraron resultados para mostrar</a></li>');
                      }
                });
            }
  }

  this.search = function(){
    var l = this;
    this.list.html('<li><a href="#">Buscando...</a></li>');
    if(this.val!=''){
       this._search();
    }else{
      this.list.hide();
    }
  }

  this.events();
}


function success(el,msj){
  jQuery(el).removeClass('alert alert-danger alert-info alert-success');
  jQuery(el).addClass('alert alert-success').html(msj);
}

function error(el,msj){
  jQuery(el).removeClass('alert alert-danger alert-info alert-success');
  jQuery(el).addClass('alert alert-danger').html(msj);
}

function info(el,msj){
  jQuery(el).removeClass('alert alert-danger alert-info alert-success');
  jQuery(el).addClass('alert alert-info').html(msj);
}

function clear(el,msj){
  jQuery(el).removeClass('alert alert-danger alert-info alert-success');
  jQuery(el).html('');
}