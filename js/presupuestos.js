var PuntoDeVenta = function(){
	
	this.productoHTML = $("#productoVacio").clone();
	this.productoHTML.removeAttr('id');
	this.productoHTML.find('a,input').show();
	this.productoshtml = $("#ventaDescr tbody");
	this.alt = false;
	this.initEvents = function(){
        var l = this;
        //Seleccionar cliente
        $(document).on('change','#cliente',function(){
            if($(this).val()!=''){
                l.datos.cliente = $(this).val();
                if($(this).find('option:selected').data('mayorista')!='undefined'){
                    l.datos.mayorista = $(this).find('option:selected').data('mayorista');
                    $("#direccion_cliente").val($(this).find('option:selected').data('direccion'));
                    $("#telefono_cliente").val($(this).find('option:selected').data('celular'));
                }
                l.updateFields();
            }
        });
        $("#cliente").ajaxChosenJsonList({
           dataType: 'json',
            type: 'POST',
            url:URI+'maestras/clientes/json_list',
            success:function(data,val){l.selectClient(data,val);}
        },{
            loadingImg: URI,
            processItems:function(data){
            	var d = [];
            	for(var i in data){
            		d.push({id:data[i].id,text:data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos});
            	}
            	return d;
            }
        },{
            "search_contains": true, allow_single_deselect:true
        }); 

	    //Campo añadir codigo
	    $(document).on('change','#codigoAdd',function(){
	    	//l.addProduct($(this).val());
	    });
	    $(document).on('change','#usuario',function(){
	    	l.datos.usuario = $(this).val();
	    });
	    $(document).on('keyup','#codigoAdd',function(e){
	    	if(e.which==13){
	    		//$(this).trigger('change');
	    		//l.addProduct($(this).val());
                $("#por_desc").focus();
	    	}
	    });

        $(document).on('keyup','#por_desc',function(e){
            if(e.which==13){
                //$(this).trigger('change');
                l.addProduct($('#codigoAdd').val());
                $("#codigoAdd").focus();
            }
        });

        

	    $(document).on('click','.insertar',function(){
	    	l.addProduct($('#codigoAdd').val());
	    });


	    
	    /* Event precio, cantidad en productos */
	    $(document).on('change','.precio',function(){
            var precio_venta = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var enc = -1;
            for(var i in l.datos.productos){
                if(
                    (controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                    l.datos.productos[i].codigovenc==codigo
                ){
                    enc = i; break;
                }
            }

            if(enc>=0){
                i = enc;
                l.datos.productos[i].precio_venta = precio_venta;
                l.updateFields();
            }
        });

	    //Event Remove
	    $(document).on('click','.rem',function(){
	    	l.removeProduct($(this).parents('tr').data('codigo'));
	    });

	    $(document).on('change','.cantidad',function(){
            var cantidad = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var enc = -1;
            var forceMayorista = false;
            for(var i in l.datos.productos){
                if((controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) || l.datos.productos[i].codigovenc==codigo){enc = i; break;}
            }            
            if(enc>=0){
                i = enc;
                if(ajustes.activar_mayorista_por_venta == '1'){
                    forceMayorista = true;
                }
                l.datos.productos[i].cantidad = cantidad;
                l.datos.productos[i].precio_venta = l.getPrecio(l.datos.productos[i],cantidad,forceMayorista);
                l.updateFields();
            }
        });

        $(document).on('change','.por_venta',function(){
            var por_venta = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var enc = -1;            
            for(var i in l.datos.productos){
                if((controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) || l.datos.productos[i].codigovenc==codigo){enc = i; break;}
            }            
            if(enc>=0){
                i = enc;
                l.datos.productos[i].por_venta = por_venta;                
                l.datos.productos[i].precio_descuento = por_venta*l.datos.productos[i].precio_venta/100;
            }
            l.updateFields();
        });

	    //Otros campos
	    $(document).on('change','#sucursal,#transaccion,#observacion,#formapago,#repartidor',function(){l.updateFields();});

	    //Atajos
	    $(document).on('keydown',function(e){
	    	if(e.which==18){
	    		l.alt = true;
	    	}
	    });

	    //Atajos
	    $(document).on('keyup',function(e){
	    	if(e.which==18){
                l.alt = false;
	    	}else{
                if(l.alt){                	
                    switch(e.which){
                        case 67: //[c] Focus en codigos
                                $("#codigoAdd").focus();
                        break;
                        case 73: //[i] busqueda avanzada
                                $("#inventarioModal").modal('toggle');		    				
                        break;
                        case 80: //[p] Procesar venta
                                $("#procesar").modal('toggle');		    				
                        break;
                        case 78: //[n] Nueva venta
                                nuevaVenta();
                        break;
                        case 66: //[B] Nueva venta
                                saldo();
                        break;
                    }
                }
	    	}

	    });
	}

	this.selectClient = function(data,val){
		var opt = '';
        for(var i in data){
          data[i].mayorista = data[i].mayorista=='activo' || data[i].mayorista=='1'?1:0;
          opt+= '<option value="'+data[i].id+'" data-direccion="'+data[i].direccion+'" data-celular="'+data[i].celular+'" data-mayorista="'+data[i].mayorista+'">'+data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos+'</option>';
        }
        if(data.length==0){
          opt+= '<option value="">Cliente no existe</option>';
        }
        $("#cliente").html(opt);
        $("#cliente").chosen().trigger('liszt:updated');
        $("#cliente_chzn input[type='text']").val(val);
    	$("#cliente").trigger('change');
    	this.updateFields();
	}

	this.selectUsuario = function(data,val){
		var opt = '';
        for(var i in data){          
          opt+= '<option value="'+data[i].id+'">'+data[i].nombre+' '+data[i].apellido+'</option>';
        }
        if(data.length==0){
          opt+= '<option value="">Cliente no existe</option>';
        }
        $("#usuario").html(opt);
        $("#usuario").chosen().trigger('liszt:updated');
        $("#usuario_chzn input[type='text']").val(val);
    	$("#usuario").trigger('change');
    	this.updateFields();
	}
        
    this.getPrecio = function(producto,cantidad,forceMayorista){         
        var precio = this.datos.transaccion==1 || isNaN(parseFloat(producto.precio_credito)) || producto.precio_credito<=0?parseFloat(producto.precio_venta):parseFloat(producto.precio_credito);                

        var cant_1 = parseFloat(isNaN(producto.cant_1))?0:producto.cant_1;
        var cant_2 = parseFloat(isNaN(producto.cant_2))?0:producto.cant_2;
        var cant_3 = parseFloat(isNaN(producto.cant_3))?0:producto.cant_3;
        if(ajustes.activar_mayorista_por_venta == '1'){
            if(!forceMayorista){
                this.datos.mayorista = 0;
            }else{
                this.datos.mayorista = 1;
            }
        }        
        if(parseInt(ajustes.rango_precio_cantidades)==1  &&
        (
            parseFloat(producto.precio_venta_mayorista1) > 0 &&
            parseFloat(producto.precio_venta_mayorista2) > 0 &&
            parseFloat(producto.precio_venta_mayorista3) > 0 
        )){
            var enc = false;            
            if(parseFloat(cant_1)!=0 || parseFloat(cant_2)!=0 || parseFloat(cant_3)!=0){
                
                precio = producto.precio_original?parseFloat(producto.precio_original):parseFloat(producto.precio_venta);
                
                if(parseFloat(cant_1)>0 && parseFloat(cantidad)>=parseFloat(cant_1)){
                    precio = parseFloat(producto.precio_venta_mayorista1);                    
                }
                if(parseFloat(cant_2)>0 && parseFloat(cantidad)>=parseFloat(cant_2)){
                    precio = parseFloat(producto.precio_venta_mayorista2);                    
                }
                if(parseFloat(cant_3)>0 && parseFloat(cantidad)>=parseFloat(cant_3)){
                    precio = parseFloat(producto.precio_venta_mayorista3);                    
                }

                /*if(producto.por_venta && !isNaN(producto.por_venta) && producto.por_venta>0){
                    precio = precio+(precio*producto.por_venta/100);
                }*/

            }else{
                for(var i in cantidades_mayoristas){
                    if(!enc && parseFloat(cantidad)>=parseFloat(cantidades_mayoristas[i].desde)){
                        var campo = cantidades_mayoristas[i].campo;                                                    
                        precio = parseFloat(producto[campo]);
                    }
                }
            }
        }
        return precio;
    }

	this._addProduct = function(producto,cantidad,forceMayorista){
        cantidad = cantidad!=undefined?cantidad:1;
        var pr = this.datos.productos;
        var enc = false;
        for(var i in pr){
            if(pr[i].codigo==producto.codigo && (controlar_vencimiento == 0 || pr[i].vencimiento==producto.vencimiento.split(',')[0])){
                enc = true;
                this.datos.productos[i].cantidad+=cantidad;                
            }
        }
        if(!enc){            
            if((max_lines && parseInt(max_lines)>-1 && (pr.length+1)>parseInt(max_lines)) || parseInt($('.tipoFacturacion option:selected').data('max_items'))>-1 && (pr.length+1)>parseInt($('.tipoFacturacion option:selected').data('max_items'))){
                alert('La cantidad de items excede del máximo permitido para este tipo de facturación, por favor elimine los items excedentes y regístrelos en una nueva facturación');
                return;
            }
            var precio = this.getPrecio(producto,cantidad,forceMayorista);            
            var precio_descuento = 0;
            if(parseFloat(producto.descmax)>0){                    
                var descuento = parseFloat(producto.descmax);                                        
                precio_descuento = (descuento*precio)/100;                    
            }
            if(!isNaN(parseFloat($("#por_desc").val())) && parseFloat($("#por_desc").val())>0){
                descuento = parseFloat($("#por_desc").val());
                precio_descuento = descuento*precio/100;                
            }else{
                descuento = 0;
            }
            this.datos.productos.push({
                    codigo:producto.codigo,
                    nombre:producto.nombre_comercial,
                    cantidad:cantidad,
                    stock:producto.stock,
                    por_desc:parseFloat(producto.descmax),
                    precio_venta:precio,
                    precio_original:precio,
                    precio_credito:producto.precio_credito,
                    cant_1:producto.cant_1,
                    cant_2:producto.cant_2,
                    cant_3:producto.cant_3,
                    por_venta:descuento,
                    precio_venta_mayorista1: parseFloat(producto.precio_venta_mayorista1),
                    precio_venta_mayorista2: parseFloat(producto.precio_venta_mayorista2),
                    precio_venta_mayorista3: parseFloat(producto.precio_venta_mayorista3),
                    precio_descuento:precio_descuento,
                    mod_precio: producto.mod_precio,
                    total: 0                        
            });            
            if(controlar_vencimiento==1){
                this.datos.productos[this.datos.productos.length-1].vencimientoOpts = !producto.vencimiento || typeof(producto.vencimiento)=='undefined'?[]:producto.vencimiento.split(',');
                this.datos.productos[this.datos.productos.length-1].stockOpts = producto.stock.split(',');
                this.datos.productos[this.datos.productos.length-1].vencimiento = this.datos.productos[this.datos.productos.length-1].vencimientoOpts[0];
                this.datos.productos[this.datos.productos.length-1].stock = this.datos.productos[this.datos.productos.length-1].stock[0];
                this.datos.productos[this.datos.productos.length-1].codigovenc = producto.codigo+this.datos.productos[this.datos.productos.length-1].vencimiento;                
            }
            this.refrshEvents = true;
        } 
           
        this.updateFields();
        
    }
        
    this.getBalanza = function(codigo,medida){
        if(codigo.toString().length!=13){
            return codigo;
        }
        medida = typeof medida == 'undefined'?'peso':medida;
        for(var i in codigo_balanza){
            var cc = codigo_balanza[i];
            var numerosBalanza = cc.toString().length;
            if(codigo.toString().substring(0,numerosBalanza)==cc){
                var peso = parseInt(codigo.toString().substring(7,12));
                peso = medida=='peso'?parseInt(peso)/1000:parseFloat(peso);
                codigo = parseInt(codigo.toString().substring(2,7));
                codigo = {
                    codigo: codigo,
                    cantidad: peso
                }
                return codigo;  
            }
        }
        return codigo;
    }

	this.addProduct = function(codigo,obj){                                
        //buscamos el codigo
        var l = this;                
        var forceMayorista = false;
        if(codigo!=''){
            codigo = codigo.split('*');
            if(codigo.length==2){
                cantidad = codigo[0];
                codigo = codigo[1];
            }else{
                codigo = codigo[0];
                codigo = codigo.split('+');
                if(codigo.length==2){
                    cantidad = codigo[0];
                    codigo = codigo[1];                    
                    /*if(ajustes.activar_mayorista_por_venta == '1'){
                        forceMayorista = true;
                    }*/
                }
                else{
                    cantidad = 1;
                    codigo = codigo[0];
                }
            }
            if(typeof cantidad.substring == 'function' && cantidad.substring(0,1)=='m'){
                cantidad = parseFloat(cantidad.substring(1));
                cantidad = isNaN(cantidad)?1:cantidad;
                forceMayorista = true;
            }else{
                cantidad = parseFloat(cantidad);
            }
            
            //Es balanza?   
            var codigoOriginal = codigo;                                     
            codigo = this.getBalanza(codigo);
            var balanza = typeof codigo == 'object'?1:0;
            cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
            codigo = typeof codigo == 'object'?codigo.codigo:codigo;            
            $.post(URI+'movimientos/productos/productos/json_list',{                    
                    'codigo':codigo,
                    'cliente':this.datos.cliente,
                    'balanza':balanza,
                    'venta':1
            },function(data){
                    data = JSON.parse(data);
                    if(data.length>0){
                        if(vender_sin_stock==1 || (cantidad>0 && parseFloat(data[0].stock)>=cantidad) || (data[0].inventariable=='inactivo')){                            
                            if(data[0].unidad_medida_id==1){
                                //Verificar si es por balanza y si es por cantidad
                                codigo = l.getBalanza(codigoOriginal,'unidad');                                                                
                                cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
                                codigo = typeof codigo == 'object'?codigo.codigo:codigo;
                            }

                            if(controlar_vencimiento==1 && typeof(obj)=='object'){
                                data[0].vencimiento = obj.vencimiento;
                                data[0].stock = obj.stock;
                            }

                            $("#codigoAdd").attr('placeholder','Código de producto');
                            $("#codigoAdd").removeClass('error');
                            $("#foto").html(data[0].foto_principal.replace('50px"','50px" style="width: 50%;height: auto;margin: 0 auto"'));                            
                            l._addProduct(data[0],cantidad,forceMayorista);
                            $("#por_desc").val('');
                            $("#codigoAdd").val('');
                        }else{
                            $("#codigoAdd").val('');
                            $("#codigoAdd").attr('placeholder','Sin stock suficiente');
                            $("#codigoAdd").addClass('error');
                        }
                    }else{
                            $("#codigoAdd").val('');
                            $("#codigoAdd").attr('placeholder','Producto no encontrado');
                            $("#codigoAdd").addClass('error');
                    }
            });
        }
    }

	this.removeProduct = function(codigo){
		if(codigo!=''){
			var pr = this.datos.productos;
			var pr2 = [];
			for(var i in pr){
				if(pr[i].codigo!=codigo){
					pr2.push(pr[i]);
				}
			}
		}
		this.datos.productos = pr2;
		this.updateFields();
	}

	this.updateFields = function(){
		this.datos.cliente = $("#cliente").val();
		this.datos.transaccion = $("#transaccion").val();		
		this.datos.observacion = $("#observacion").val();
		this.datos.forma_pago = $("#formapago").val();
		this.datos.sucursal = $("#sucursal").val();
		this.datos.repartidor_id = $("#repartidor").val();
		//Calcular total
		var pr = this.datos.productos;
		var total = 0;
		for(var i in pr){
            this.datos.productos[i].total = pr[i].cantidad*pr[i].precio_venta;

			this.datos.productos[i].total_desc = (pr[i].precio_venta*pr[i].cantidad)+(((pr[i].precio_venta*pr[i].cantidad)*pr[i].por_venta)/100);
			total+= this.datos.productos[i].total_desc;
		}
		this.datos.total_presupuesto = total;
		//Calcular divisas
		this.datos.total_dolares = total/tasa_dolar;
		this.datos.total_reales = total/tasa_real;
		this.datos.total_pesos = total/tasa_peso;
		//this.datos.vuelto = this.datos.vuelto<0?0:this.datos.vuelto;	
        window.shouldSave = true;	
		this.refresh();
	}

	this.refresh = function(){
		const gs = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'PYG',minimumFractionDigits: 0});
		const usd = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'USD',minimumFractionDigits: 2});
		const arg = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'ARP',minimumFractionDigits: 2});
		const br = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'BRL',minimumFractionDigits: 2});
		
		$("input[name='total_presupuesto']").val(gs.format(this.datos.total_presupuesto));
		$("input[name='total_pesos']").val(arg.format(this.datos.total_pesos));
		$("input[name='total_reales']").val(br.format(this.datos.total_reales));
		$("input[name='total_dolares']").val(usd.format(this.datos.total_dolares));				
		$("#total_total span").html(gs.format(this.datos.total_presupuesto));
		$("#cantidadProductos").html(this.datos.productos.length);		
		//Draw products
		var pr = this.datos.productos;
		this.productoshtml.html('');		
		for(var i=pr.length-1;i>=0;i--){
			var newRow = this.productoHTML.clone();
			var td = newRow.find('td');
			newRow.attr('data-codigo',pr[i].codigo);
			$(td[0]).find('span').html(pr[i].codigo);
			$(td[1]).html(pr[i].nombre);
			$(td[2]).find('input').val(pr[i].cantidad);
            $(td[3]).find('input').val(pr[i].por_venta);
			$(td[4]).find('input').val(pr[i].precio_venta.toFixed(0));	
            $(td[5]).html(gs.format(pr[i].total_desc));		
			$(td[6]).html(gs.format(pr[i].total));
			$(td[7]).html(pr[i].stock);
			this.productoshtml.append(newRow);			
		}		
        window.shouldSave = true;
        this.updateTemp();
	}

	this.setDatos = function(datos){        
		this.datos = datos;
		this.datos.total_presupuesto = parseFloat(this.datos.total_presupuesto);
		this.datos.total_pesos = parseFloat(this.datos.total_pesos);
		this.datos.total_reales = parseFloat(this.datos.total_reales);
		this.datos.total_dolares = parseFloat(this.datos.total_dolares);		
		for(var i in this.datos.productos){
			this.datos.productos[i].cantidad = parseFloat(this.datos.productos[i].cantidad);
			this.datos.productos[i].precio_venta = parseFloat(this.datos.productos[i].precio_venta);
			this.datos.productos[i].total = parseFloat(this.datos.productos[i].total);
		}	

		var l = this;
		setCliente({success:true,insert_primary_key:this.datos.cliente});
		/*$.post(URI+'seguridad/user/json_list',{   
            search_field:'id',
            search_text:this.datos.usuario,
            operator:'where'
        },function(data){       
            data = JSON.parse(data);   
            l.selectUsuario(data,l.datos.usuario);
        });        

		var l = this;
		$.post(URI+'seguridad/user/json_list',{   
            search_field:'id',
            search_text:this.datos.usuario,
            operator:'where'
        },function(data){       
            data = JSON.parse(data);   
            l.selectUsuario(data,l.datos.usuario);
        });*/
        
        $("#formapago").val(this.datos.forma_pago).chosen().trigger('liszt:updated');
        $("#transaccion").val(this.datos.transaccion).chosen().trigger('liszt:updated');
        $("#repartidor").val(this.datos.repartidor_id).chosen().trigger('liszt:updated');
        $("#sucursal").val(this.datos.sucursal).chosen().trigger('liszt:updated');
        $("#usuario").val(this.datos.usuario).chosen().trigger('liszt:updated');
        $("#observacion").val(this.datos.observacion);

		
		this.refresh();	
	}

	this.initVenta = function(){
		this.datos = {			
			sucursal:'',
			caja:'',
			cajadiaria:'',
			fecha:'',			
			usuario:'',
			forma_pago:0,
			cliente:0,
			transaccion:0,			
			observacion:'',							
			total_pesos:0,			
			total_presupuesto:0,
			total_dolares:0,
			total_reales:0,
            repartidor_id:0,
			productos:[],
		};		
	}

	this.initVenta();
}

PuntoDeVenta.prototype.updateTemp = function(){
    if(this.datos.productos.length>0 && !editar){
        $.post(URI+'movimientos/presupuesto/presupuesto/updateTemp',{
            productos:JSON.stringify(this.datos.productos)
        },function(data){
            if(data!='success'){
                alert(data);
                console.log(data);
            }
        });
    }
}

PuntoDeVenta.prototype.loadTemp = function(){
    let $this = this;
    $.get(URI+'movimientos/presupuesto/presupuesto/getTemp',{},function(data){
        data = JSON.parse(data);
        if(data.length>0){
            $this.datos.productos = data;
            $this.updateFields();    
        }
    });
}