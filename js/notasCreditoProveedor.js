var NotaCredito = function(){
	this.productoHTML = $("#productoVacio").clone();
	this.productoHTML.removeAttr('id');
	this.productoHTML.find('a,input').show();
	this.productoshtml = $("#ventaDescr tbody");
	this.INP_nro_compra = $("input[name='compra']");
	this.INP_nro_nota = $("input[name='nro_nota_credito']");
	this.INP_cliente = $("input[name='proveedor']");
	this.INP_total_nota = $("input[name='total_monto']");
	this.INP_total_dolares = $("input[name='total_dolares']");
	this.INP_total_reales = $("input[name='total_reales']");
	this.INP_total_pesos = $("input[name='total_pesos']");
	this.INP_descontar_saldo = $("select[name='descontar_saldo']");	
	this.INP_motivo_nota_id = $("select[name='motivo_nota_id']");	
	this.INP_codigo = $("#codigoAdd");
	this.datos = {};
	this.alt = false;
	this.initDatos = function(){
		this.datos = {
			 	compra:0,
			 	nro_factura:'',
			 	nro_nota_credito:0,
			 	fecha:0,
			 	proveedor:0,
			 	proveedorNombre:'',
			 	total_monto:0,
			 	actualizar_stock:1,
			 	sucursal:0,
			 	usuario:0,
			 	caja:0,
			 	cajadiaria:0,
			 	productos:[],
			 	total_dolares:0,
			 	total_reales:0,
			 	total_pesos:0
		};
	}
	this.initDatos();
	this.initEvents = function(){
		var l = this;
		$(document).on('change',"input[name='compra']",function(){
			l.INP_nro_compra.removeClass('error');
			l.updateData();
			l.searchCompra();
		});

		

		$(document).on('change','.cantidad',function(){
            var cantidad = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(l.datos.productos[i].producto.codigo==codigo){
                    l.datos.productos[i].cantidad = cantidad;
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.precio',function(){
            var cantidad = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(l.datos.productos[i].producto.codigo==codigo){
                    l.datos.productos[i].precio_costo = cantidad;
                }
            }
            l.updateData();
	    });

		//Atajos
	    $(document).on('keydown',function(e){
	    	if(e.which==18){
	    		l.alt = true;
	    	}
	    });
		//Atajos
	    $(document).on('keyup',function(e){
	    	if(e.which==18){
                l.alt = false;
	    	}else{
	            if(l.alt){
	                switch(e.which){
                        case 67: //[c] Focus en codigos
                                $("#codigoAdd").focus();
                        break;
                        case 73: //[i] busqueda avanzada
                                $("#inventarioModal").modal('toggle');		    				
                        break;
                        case 80: //[p] Procesar venta
                                save();		    				
                        break;
                        case 78: //[n] Nueva venta
                                nuevaNota();
                                l.INP_nro_compra.focus();
                        break;
	                }
	            }
	    	}

	    });

	    //Event Remove
	    $(document).on('click','.rem',function(){
	    	l.removeProduct($(this).parents('tr').data('codigo'));
	    });

	    $(document).on('change','',function(){			
			l.updateData();			
		});
	}



	this.initNota = function(){
		this.initDatos();
		this.draw();
	}

	this.searchCompra =  function(){
		var l = this;
		$.post(base_url+'movimientos/compras/compras/json_list',{
			'compras_id':this.datos.nro_factura
		},function(data){	
			data = JSON.parse(data);		
			if(data.length>0){				
				data = data[0];
				$.post(base_url+'movimientos/compras/compradetalle/json_list',{
					compra:data.id,
					per_page:1000,
					page:1
				},function(dd){					
					data.productos = JSON.parse(dd);
					data.productos = data.productos.filter(x=>x.producto);			
					l.updateData(data);
					l.INP_codigo.attr('readonly',false);
				});
			}else{
				l.INP_nro_compra.addClass('error');
			}
		});
	};

	this.updateData = function(datos){		
		if(typeof(datos)=='undefined'){
			this.datos.nro_factura = this.INP_nro_compra.val();
			//this.datos.nro_nota_credito = this.INP_nro_nota.val();			
			this.datos.nro_nota_credito = '0';
		}else{			
			this.datos.compra = datos.id;			
			this.datos.proveedor = datos.proveedor;
			this.datos.productos = datos.productos;
			this.datos.proveedorNombre = datos.sed53a12c;			
		}
		this.datos.actualizar_stock = $("input[name='actualizar']:checked").val();
		this.datos.descontar_saldo = this.INP_descontar_saldo.val();
		this.datos.motivo_nota_id = this.INP_motivo_nota_id.val();
		var total = 0;
		for(var i in this.datos.productos){
			this.datos.productos[i].cantidad = parseFloat(this.datos.productos[i].cantidad);
			total+= parseFloat(this.datos.productos[i].precio_costo) * parseFloat(this.datos.productos[i].cantidad);
		}
		this.datos.total_monto = total;			
		//Calcular divisas
		this.datos.total_dolares = (total/tasa_dolar).toFixed(2);
		this.datos.total_reales = (total/tasa_real).toFixed(2);
		this.datos.total_pesos = (total/tasa_peso).toFixed(2);	
		this.draw();	
	}

	this.draw = function(){		
		const gs = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'PYG',minimumFractionDigits: 0});
		$("#cantidadProductos").html(this.datos.productos.length);
		this.INP_cliente.val(this.datos.proveedorNombre);
		this.INP_total_nota.val(this.datos.total_monto);
		this.INP_nro_nota.val(this.datos.nro_nota_credito);
		this.INP_nro_compra.val(this.datos.nro_factura);
		this.INP_total_dolares.val(this.datos.total_dolares);
		this.INP_total_reales.val(this.datos.total_reales);
		this.INP_total_pesos.val(this.datos.total_pesos);	
		this.INP_descontar_saldo.val(this.datos.descontar_saldo);	
		this.INP_motivo_nota_id.val(this.datos.motivo_nota_id);	
		//Draw products
		var pr = this.datos.productos;
		this.productoshtml.html('');		
		for(var i=pr.length-1;i>=0;i--){			
			var newRow = this.productoHTML.clone();
			var td = newRow.find('td');
			newRow.attr('data-codigo',pr[i].producto.codigo);
			$(td[0]).find('span').html(pr[i].producto.codigo);
			$(td[1]).html(pr[i].producto.nombre_comercial);
			$(td[2]).find('input').val(pr[i].cantidad);
			$(td[3]).find('input').val(parseFloat(pr[i].precio_costo).toFixed(0));						
			$(td[4]).html(gs.format(parseFloat(pr[i].precio_costo * pr[i].cantidad)));
			this.productoshtml.append(newRow);			
		}
	}

	this.getPrecio = function(producto,cantidad){         
        var precio = parseFloat(producto.precio_costo);
        /*if(this.datos.mayorista==1){
            var enc = false;
            console.log(cantidad);
            for(var i in cantidades_mayoristas){
                if(!enc && parseInt(cantidad)>=parseInt(cantidades_mayoristas[i].desde)){
                    var campo = cantidades_mayoristas[i].campo;                                                    
                    precio = parseFloat(producto[campo]);
                    //enc = true;                                     
                }
            }
        }*/
        return precio;
    }

	this._addProduct = function(producto,cantidad){
        cantidad = cantidad!=undefined?cantidad:1;
        var pr = this.datos.productos;
        var enc = false;
        for(var i in pr){
            if(pr[i].producto.codigo==producto.codigo){
                enc = true;
                this.datos.productos[i].cantidad+=cantidad;				
            }
        }
        if(!enc){
                var precio = this.getPrecio(producto,cantidad);                                
                this.datos.productos.push({
                		producto:producto,                        
                        cantidad:cantidad,                        
                        precio_costo:precio,
                        totalcondesc:precio,
                        total: 0
                });
        }

        this.updateData();
	}
        
    this.getBalanza = function(codigo,medida){
        if(codigo.toString().length!=13){
            return codigo;
        }
        medida = typeof medida == 'undefined'?'peso':medida;
        for(var i in codigo_balanza){
            var cc = codigo_balanza[i];
            var numerosBalanza = cc.toString().length;
            if(codigo.toString().substring(0,numerosBalanza)==cc){
                var peso = parseInt(codigo.toString().substring(7,12));
                peso = medida=='peso'?parseInt(peso)/1000:parseFloat(peso);
                codigo = parseInt(codigo.toString().substring(2,7));
                codigo = {
                    codigo: codigo,
                    cantidad: peso
                }
                return codigo;  
            }
        }
        return codigo;
    }

	this.addProduct = function(codigo){                                
		//buscamos el codigo
		var l = this;				
		if(codigo!=''){
	        codigo = codigo.split('*');
	        if(codigo.length==2){
	                cantidad = parseFloat(codigo[0]);
	                codigo = codigo[1];
	        }else{
	                codigo = codigo[0];
	                codigo = codigo.split('+');
	                if(codigo.length==2){
	                        cantidad = parseFloat(codigo[0]);
	                        codigo = codigo[1];	
	                }
	                else{
	                        cantidad = 1;
	                        codigo = codigo[0];	
	                }
	        }
	        
	        //Es balanza?                                        
	        codigo = this.getBalanza(codigo);                                        
	        cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
	        codigo = typeof codigo == 'object'?codigo.codigo:codigo;
	        $.post(URI+'movimientos/productos/productos/json_list',{
	                'codigo':codigo,
	                'operator':'where',
	                'proveedor':this.datos.proveedor
	        },function(data){
	                data = JSON.parse(data);
	                if(data.length>0){
	                	if(vender_sin_stock==1 || (cantidad>0 && parseFloat(data[0].stock)>=cantidad) || (data[0].inventariable=='inactivo')){
	                        $("#codigoAdd").val('');
	                        l._addProduct(data[0],cantidad);
	                        $("#codigoAdd").attr('placeholder','Código de producto');
	                        $("#codigoAdd").removeClass('error');
	                    }else{
	                    	$("#codigoAdd").val('');
	                        $("#codigoAdd").attr('placeholder','Sin stock suficiente');
	                        $("#codigoAdd").addClass('error');
	                    }
	                }else{
	                        $("#codigoAdd").val('');
	                        $("#codigoAdd").attr('placeholder','Producto no encontrado');
	                        $("#codigoAdd").addClass('error');
	                }
	        });
		}
	}

	this.removeProduct = function(codigo){
		if(codigo!=''){
			var pr = this.datos.productos;
			var pr2 = [];
			for(var i in pr){
				if(pr[i].producto.codigo!=codigo){
					pr2.push(pr[i]);
				}
			}
		}
		this.datos.productos = pr2;
		this.updateData();
	}
}