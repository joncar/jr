var PuntoDeVenta = function(){
	
	this.productoHTML = $("#productoVacio").clone();    
	this.productoHTML.removeAttr('id');
	this.productoHTML.find('a,input').show();
	this.productoshtml = $("#ventaDescr tbody");
    this.cantidadProductos = $("#cantidadProductos");
	this.alt = false;
    this.countProducts = function(){
        this.cantidadProductos.html(this.productoshtml.find('tr').length);
    }
	this.initEvents = function(){
        var l = this;
        $("#productoVacio").remove();
        this.countProducts();
        //Seleccionar cliente
	    //Campo añadir codigo
	    $(document).on('change','#codigoAdd',function(){
	    	//l.addProduct($(this).val());
	    });
	    
	    $(document).on('keyup','#codigoAdd',function(e){
	    	if(e.which==13 || e.which==27){
	    		e.preventDefault();
	    		//$(this).trigger('change');
	    		//l.addProduct($(this).val());
                l.addProduct($('#codigoAdd').val());
	    	}
	    });

	    $(document).on('click','.insertar',function(){
	    	l.addProduct($('#codigoAdd').val());
	    });

	    //Event Remove
	    $(document).on('click','.rem',function(){
	    	$(this).parents('tr').remove();
            l.countProducts();
            l.updateTemp();
	    });

	    
	    //Atajos
	    $(document).on('keydown',function(e){
	    	if(e.which==18){
	    		l.alt = true;
	    	}
	    });

	    //Atajos
	    $(document).on('keyup',function(e){
	    	if(e.which==18){
                l.alt = false;
	    	}else{
                if(l.alt){                	
                    switch(e.which){
                        case 67: //[c] Focus en codigos
                                $("#codigoAdd").focus();
                        break;
                        case 73: //[i] busqueda avanzada
                                $("#inventarioModal").modal('toggle');		    				
                        break;
                        case 80: //[p] Procesar venta
                                $("#procesar").modal('toggle');		    				
                        break;
                        case 78: //[n] Nueva venta
                                nuevaVenta();
                        break;
                    }
                }
	    	}

	    });

        $(document).on('change','input[name="precio_costo"],input[name="porc_venta"],input[name="porc_mayorista1"],input[name="porc_mayorista2"],input[name="porc_mayorista3"]',function(){
            l.calcularPrecios($(this).parents('tr'));
        });

        $(document).on('change','input[name="precio_venta"],input[name="precio_venta_mayorista1"],input[name="precio_venta_mayorista2"],input[name="precio_venta_mayorista3"]',function(){
            l.calcularPorcentajes($(this).parents('tr'));
        });
	}

    this.resetVenta = function(){
        this.productoshtml.html('');
        this.countProducts();
    }

	this.addProduct = function(codigo,obj){                                
        //buscamos el codigo
        var l = this;                        
        if(codigo!=''){            
            $.post(URI+'movimientos/productos/productos/json_list',{                    
                'codigo':codigo,                    
            },function(data){
                data = JSON.parse(data);
                if(data.length>0){
                    data = data[0];
                    l.renderRow(data);
                    $("#codigoAdd").val('');
                }else{
                    $("#codigoAdd").val('');
                    $("#codigoAdd").attr('placeholder','Producto no encontrado');
                    $("#codigoAdd").addClass('error');
                }
            });
        }
    }
}

PuntoDeVenta.prototype.calcularPrecios = function(tr){
    var precio_costo = parseFloat(tr.find('input[name="precio_costo"]').val());
    var porc_venta = parseFloat(tr.find('input[name="porc_venta"]').val());
    var porc_mayorista1 = parseFloat(tr.find('input[name="porc_mayorista1"]').val());
    var porc_mayorista2 = parseFloat(tr.find('input[name="porc_mayorista2"]').val());
    var porc_mayorista3 = parseFloat(tr.find('input[name="porc_mayorista3"]').val());
    var precio = 0;
    if(isNaN(precio_costo)){
        return;
    }
    if(!isNaN(porc_venta)){
        precio = precio_costo + ((precio_costo*porc_venta)/100);
        tr.find("input[name='precio_venta']").val(redondeo(parseInt(precio.toFixed(0))));
    }

    if(!isNaN(porc_mayorista1)){
        precio = precio_costo + ((precio_costo*porc_mayorista1)/100);
        tr.find("input[name='precio_venta_mayorista1']").val(redondeo(parseInt(precio.toFixed(0))));
    }

    if(!isNaN(porc_mayorista2)){
        precio = precio_costo + ((precio_costo*porc_mayorista2)/100);
        tr.find("input[name='precio_venta_mayorista2']").val(redondeo(parseInt(precio.toFixed(0))));
    }

    if(!isNaN(porc_mayorista3)){
        precio = precio_costo + ((precio_costo*porc_mayorista3)/100);
        tr.find("input[name='precio_venta_mayorista3']").val(redondeo(parseInt(precio.toFixed(0))));
    }
    this.updateTemp();
}

PuntoDeVenta.prototype.calcularPorcentajes = function(tr){
    var precio_costo = parseFloat(tr.find('input[name="precio_costo"]').val());
    var porc_venta = parseFloat(tr.find('input[name="precio_venta"]').val());
    var porc_mayorista1 = parseFloat(tr.find('input[name="precio_venta_mayorista1"]').val());
    var porc_mayorista2 = parseFloat(tr.find('input[name="precio_venta_mayorista2"]').val());
    var porc_mayorista3 = parseFloat(tr.find('input[name="precio_venta_mayorista3"]').val());
    var precio = 0;
    if(isNaN(precio_costo)){
        return;
    }
    if(!isNaN(porc_venta)){
        precio = ((porc_venta/precio_costo)-1)*100;
        tr.find("input[name='porc_venta']").val(redondeo(parseInt(precio.toFixed(0))));
    }

    if(!isNaN(porc_mayorista1)){
        precio = ((porc_mayorista1/precio_costo)-1)*100;
        tr.find("input[name='porc_mayorista1']").val(redondeo(parseInt(precio.toFixed(0))));
    }

    if(!isNaN(porc_mayorista2)){
        precio = ((porc_mayorista2/precio_costo)-1)*100;
        tr.find("input[name='porc_mayorista2']").val(redondeo(parseInt(precio.toFixed(0))));
    }

    if(!isNaN(porc_mayorista3)){
        precio = ((porc_mayorista3/precio_costo)-1)*100;
        tr.find("input[name='porc_mayorista3']").val(redondeo(parseInt(precio.toFixed(0))));
    }
    this.updateTemp();
}

PuntoDeVenta.prototype.renderRow = function(product){
    var newtr = this.productoHTML.clone();
    for(i in product){
        newtr.find('[name="'+i+'"]').val(product[i]);
        newtr.find('[data-name="'+i+'"]').html(product[i]);
    }    
    this.productoshtml.append(newtr);    
    newtr.find('[name="precio_costo"]').focus();
    console.log(newtr.find('[name="precio_costo"]'));
    this.countProducts();
    this.updateTemp();
    this.fixScroll();
}

PuntoDeVenta.prototype.fixScroll = function(){
    setTimeout(function(){
        var box = document.getElementById('seguimiento');
        box.scrollTo(0,box.scrollHeight);
    },50);
}

PuntoDeVenta.prototype.getProductsJson = function(){
    var productos = [];
    for(var i = 0;i < venta.productoshtml.find('tr').length;i++){
      var producto = $(venta.productoshtml.find('tr')[i]);
        var obj = [];
        var inps = producto.find('input,select,textarea');
        for(k=0;k<inps.length;k++){
            var inp = $(inps[k]);
            obj[inp.attr('name')] = inp.val();
        }
        productos.push(Object.assign({},obj));
    }
    return productos;
}

PuntoDeVenta.prototype.updateTemp = function(){
    if(this.productoshtml.find('tr').length>0){
        $.post(URI+'movimientos/actualizacionPrecios/actualizador_precio/updateTemp',{
            productos:JSON.stringify(this.getProductsJson())
        },function(data){
            if(data!='success'){
                alert(data);
                console.log(data);
            }
        });
    }
}

function redondeo(valor){
    if(valor<100){
        return valor;
    }               
    var centena = valor.toString().slice(-2);
    if(centena<50){
        valor-= centena;
    }else if(centena>50){
        valor+= 100-centena;
    }
    return valor<0?Math.abs(valor):valor;
}