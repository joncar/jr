var Compras  = function(){
	this.productoHTML = $("#productoVacio").clone();
	this.productoHTML.removeAttr('id');
	this.productoHTML.find('a,input').show();
	this.productoshtml = $("#ventaDescr tbody");
	this.refrshEvents = false;
	this.inputs = {
		sucursal:$("select[name='sucursal']"),
		proveedor:$("select[name='proveedor']"),
		transaccion:$("select[name='transaccion']"),
		fecha:$("input[name='fecha']"),
		forma_pago:$("select[name='forma_pago']"),
		nro_factura:$("input[name='nro_factura']"),
		vencimiento_pago:$("input[name='vencimiento_pago']"),
		total_compra:$("input[name='total_compra']"),
		tipo_facturacion_id:$("select[name='tipo_facturacion_id']"),
		pago_caja_diaria:$("select[name='pago_caja_diaria']"),
		total_pesos:$("input[name='total_pesos']"),
		total_reales:$("input[name='total_reales']"),
		total_dolares:$("input[name='total_dolares']"),
		timbrado:$("input[name='timbrado']"),
		descontar:$("input[name='descontar']"),
	};
	this.initDatos = function(){
		this.datos = {
			 sucursal:window.sucursalConectada,
			 caja:0,
			 cajadiaria:0,
			 proveedor:1,
			 transaccion:1,
			 fecha:'00/00/0000',
			 forma_pago:1,
			 descontar:0,
			 establecimiento:'000',
			 nro_factura:'',
			 timbrado:'',
			 vencimiento_pago:'00/00/0000',
			 total_compra:0,
			 total_descuento:0,
			 status:0,
			 tipo_facturacion_id:1,
			 pago_caja_diaria:0,
			 total_pesos:0,
			 total_reales:0,
			 total_dolares:0,
			 productos:[]
		};
	}
	this.initDatos();
	this.focused = undefined;
	this.focused2 = undefined;
	this.initEvents = function(){
		var l = this;
		$(document).on('change',"input,select",function(){			
			setTimeout(function(){l.updateData();},600);
		});

		$(document).on('change','[name="descontar"]',function(){
            var descontar = parseFloat($(this).val());
            if(!isNaN(descontar)){
            	l.descontar = descontar;
            	for(var i in l.datos.productos){
            		l.datos.productos[i].desc_compra = l.descontar;
            		l.datos.productos[i].precio_costo = l.datos.productos[i].precio_costo_ant - ((l.datos.productos[i].desc_compra * l.datos.productos[i].precio_costo_ant) / 100);
            	}
            }
            l.updateData();
	    });

		

		$(document).on('change','.cantidad',function(){
            var cantidad = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var vencimiento = $($(this).parents('tr').find('.vencimiento')).val();
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].cantidad = cantidad;
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.por_desc',function(){
            var por_desc = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var vencimiento = $($(this).parents('tr').find('.vencimiento')).val();
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].por_desc = por_desc;
                	$(this).parents('tr').find('.precio_costo').change();
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.precio_costo',function(){
            var precio_costo = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var vencimiento = $($(this).parents('tr').find('.vencimiento')).val();
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].precio_costo = precio_costo;
                	l.datos.productos[i].precio_costo_ant = precio_costo;
					if(aplicar_sugerencia==1){
						if(parseFloat(l.datos.productos[i].por_venta)>0){				
							l.datos.productos[i].precio_venta = parseFloat(l.datos.productos[i].precio_costo)+Math.ceil(parseFloat(l.datos.productos[i].precio_costo)*parseFloat(l.datos.productos[i].por_venta)/100);							
							l.datos.productos[i].precio_venta = parseFloat(l.redondeo(l.datos.productos[i].precio_venta));
						}
		                if(parseFloat(l.datos.productos[i].porc_mayorista1)>0){				
							l.datos.productos[i].precio_venta_mayorista1 = l.redondeo(parseFloat(l.datos.productos[i].precio_costo)+Math.ceil(parseFloat(l.datos.productos[i].precio_costo)*parseFloat(l.datos.productos[i].porc_mayorista1)/100));
						}

						if(parseFloat(l.datos.productos[i].porc_mayorista2)>0){				
							l.datos.productos[i].precio_venta_mayorista2 = l.redondeo(parseFloat(l.datos.productos[i].precio_costo)+Math.ceil(parseFloat(l.datos.productos[i].precio_costo)*parseFloat(l.datos.productos[i].porc_mayorista2)/100));
						}

						if(parseFloat(l.datos.productos[i].porc_mayorista3)>0){				
							l.datos.productos[i].precio_venta_mayorista3 = l.redondeo(parseFloat(l.datos.productos[i].precio_costo)+Math.ceil(parseFloat(l.datos.productos[i].precio_costo)*parseFloat(l.datos.productos[i].porc_mayorista3)/100));
						}
					}
                }
            }
            l.updateData();
	    });

		$(document).on('change','#sucursales',function(){
			let val = $(this).val();
			l.datos.productos = l.datos.productos.map(x=>{
				x.sucursal = val;
				return x;
			});
			l.updateData();
		});

		$(document).on('change','.sucursal',function(){
            var precio_costo = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var vencimiento = $($(this).parents('tr').find('.vencimiento')).val();
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].sucursal = precio_costo;
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.por_venta',function(){
            var por_venta = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var vencimiento = $($(this).parents('tr').find('.vencimiento')).val();
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].por_venta = por_venta;

					if(l.datos.productos[i].por_venta>0){
						por_venta = parseFloat(l.datos.productos[i].precio_costo)+(parseFloat(l.datos.productos[i].precio_costo)*(parseFloat(l.datos.productos[i].por_venta)/100));						
						l.datos.productos[i].precio_venta = l.redondeo(Math.ceil(por_venta));
					}					
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.precio_venta',function(){
            var precio_venta = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var vencimiento = $($(this).parents('tr').find('.vencimiento')).val();
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].precio_venta = precio_venta;
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.precio_venta1',function(){
            var precio_venta = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var vencimiento = $($(this).parents('tr').find('.vencimiento')).val();
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].precio_venta_mayorista1 = l.redondeo(precio_venta);
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.precio_venta2',function(){
            var precio_venta = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var vencimiento = $($(this).parents('tr').find('.vencimiento')).val();
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].precio_venta_mayorista2 = l.redondeo(precio_venta);
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.precio_venta3',function(){
            var precio_venta = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var vencimiento = $($(this).parents('tr').find('.vencimiento')).val();
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].precio_venta_mayorista3 = l.redondeo(precio_venta);
                }
            }
            l.updateData();
	    });

		$(document).on('change','.precio_credito',function(){
            var precio_venta = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            var vencimiento = $($(this).parents('tr').find('.vencimiento')).val();
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].precio_credito = l.redondeo(precio_venta);
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.vencimiento',function(){
            var vencimiento = $(this).val();
            var codigo = $(this).parents('tr').data('codigo');  
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].vencimiento = vencimiento;                    
                    if(controlar_vencimiento==1){
                    	l.datos.productos[i].codigovenc = l.datos.productos[i].codigo+vencimiento;
                    	$(this).parents('tr').attr('data-codigo',codigo+vencimiento)
                	}
                }
            }
            l.updateData();
	    });

		//Atajos
	    $(document).on('keydown',function(e){
	    	if(e.which==18){
	    		l.alt = true;
	    	}
	    });
		//Atajos
	    $(document).on('keyup',function(e){
	    	if(e.which==18){
                l.alt = false;
	    	}else{
	            if(l.alt){	            
	                switch(e.which){
                        case 67: //[c] Focus en codigos
                                $("#codigoAdd").focus();
                        break;
                        case 73: //[i] busqueda avanzada
                                $("#inventarioModal").modal('toggle');		    				
                        break;
                        case 80: //[p] Procesar venta
                                save();		    				
                        break;
                        case 66:
                        	$("#addProduct").modal('toggle');
                        	setTimeout(function(){$("input[name='codigo_interno']").focus();},600);
                        break;
                        case 78: //[n] Nueva venta
                                nuevaCompra();                                
                        break;
                        case 38: //UP
                            //Cantidad focus
                            var index = 0;
                            var focused = $('.cantidad:focus');
                            if(focused.length>0){
                                index = $('.cantidad:focus').parents('tr').index();
                                index-=1;
                                index = index<0?0:index;
                            }
                            $($('.cantidad')[index]).focus();  
                        break;
                        case 40: //DOWN
                            //Cantidad focus
                            var index = 0;
                            var focused = $('.cantidad:focus');
                            if(focused.length>0){
                                index = $('.cantidad:focus').parents('tr').index();
                                index+=1;
                                index = index>$('.cantidad').length?$('.cantidad').length-1:index;
                            }
                            $($('.cantidad')[index]).focus();     
                        break;
	                }
	            }
	    	}

	    });

	    //Event Remove
	    $(document).on('click','.rem',function(){
	    	var cod = $(this).parents('tr').data('codigo');
	    	l.removeProduct(cod);
	    });

	    /*$(document).on('change','#codigoAdd',function(){			
	    	l.addProduct($(this).val());
		});*/
		$(document).on('keyup','#codigoAdd',function(e){
	    	if(e.which==13){
	    		//$(this).trigger('change');
	    		l.addProduct($(this).val());
	    	}
	    });

	    $(document).on('click','.insertar',function(){
	    	l.addProduct($('#codigoAdd').val());
	    });

		$(document).on('focus','td input,td select,td textarea',function(){			
	    	l.focused = $(this).parents('tr').data('codigo');
	    	l.focused2 = $(this).attr('name');
		});
	}

	this.redondeo = function(valor){
		return valor;
		if(valor<100){
			return valor;
		}				
		var centena = valor.toString().slice(-2);
		if(centena<50){
			valor-= centena;
		}else if(centena>50){
			valor+= 100-centena;
		}
		return valor<0?Math.abs(valor):valor;
	}

	this.mayoristas = function(el){
		var mayoristas = $(el).parent().find('.mayoristas');
		var codigo = $(el).parents('tr').data('codigo');  
		var show = false;
		mayoristas.toggle('show');
		if(mayoristas.hasClass('active')){
			mayoristas.removeClass('active');
			show = false;
		}else{
			mayoristas.addClass('active');
			show = true;
		}
		for(var i in this.datos.productos){
            if(
            	(controlar_vencimiento==0 && this.datos.productos[i].codigo==codigo) ||
            	this.datos.productos[i].codigovenc==codigo
            ){
                this.datos.productos[i].show_mayoristas = show;                                    
            }
        }
	}

	this.updateData = function(datos){		
		for(var i in this.inputs){
			this.datos[i] = this.inputs[i].val();
		}		
		var total = 0;
		for(var i in this.datos.productos){
			this.datos.productos[i].cantidad = parseFloat(this.datos.productos[i].cantidad);
			var mt = parseFloat(this.datos.productos[i].precio_costo) * parseFloat(this.datos.productos[i].cantidad);
			if(parseFloat(this.datos.productos[i].por_desc)>0){
				mt = mt-(mt*(parseFloat(this.datos.productos[i].por_desc)/100));
			}
			this.datos.productos[i].total = mt;
			total+= mt;
		}
		this.datos.total_compra = total;	
		//Calcular divisas
		this.datos.total_dolares = (total/tasa_dolar).toFixed(2);
		this.datos.total_reales = (total/tasa_real).toFixed(2);
		this.datos.total_pesos = (total/tasa_peso).toFixed(2);
		this.draw();	
	}

	this.draw = function(){		
		const gs = new Intl.NumberFormat('de-DE');
		$("#cantidadProductos").html(this.datos.productos.length);
		for(var i in this.inputs){
			this.inputs[i].val(this.datos[i]);
		}
		this.inputs.total_compra.val(gs.format(parseFloat(this.datos.total_compra)));
		//Draw products
		var pr = this.datos.productos;
		this.productoshtml.html('');		
		for(var i=pr.length-1;i>=0;i--){			
			var newRow = this.productoHTML.clone();
			var td = newRow.find('td');
			var c = pr[i].codigo;
			if(controlar_vencimiento==1){
				c+= pr[i].vencimiento;
			}
			newRow.attr('data-codigo',c);
			var total = parseFloat(pr[i].cantidad) * parseFloat(pr[i].precio_costo);
			if(parseFloat(pr[i].por_desc)>0){
				total = total-(total*(parseFloat(pr[i].por_desc)/100));
			}
			
			//Mayoristas
			

			$(td[0]).find('span').html('<a href="'+URI+'movimientos/productos/productos/edit/'+pr[i].id+'" target="_new">'+pr[i].codigo+'</a>');
			$(td[1]).html(pr[i].nombre_comercial);
			$(td[2]).find('input').val(pr[i].cantidad);
			$(td[3]).find('input').val(parseFloat(pr[i].precio_costo).toFixed(0));						
			$(td[4]).find('input').val(parseFloat(pr[i].por_desc).toFixed(0));
			$(td[5]).find('input').val(parseFloat(pr[i].por_venta));
			$(td[6]).find('input[name="precio_venta"]').val(pr[i].precio_venta);									
			$(td[6]).find('input[name="precio_venta1"]').val(pr[i].precio_venta_mayorista1);									
			$(td[6]).find('input[name="precio_venta2"]').val(pr[i].precio_venta_mayorista2);									
			$(td[6]).find('input[name="precio_venta3"]').val(pr[i].precio_venta_mayorista3);									
			$(td[6]).find('input[name="precio_credito"]').val(pr[i].precio_credito);									
			if(pr[i].show_mayoristas){
				$(td[6]).find('.mayoristas').show().addClass('active');
			}
			$(td[7]).html(gs.format(total));						
			$(td[8]).find('input').val(pr[i].vencimiento);						
			$(td[9]).html(pr[i].stock);
			$(td[10]).html(pr[i].iva_id);
			$(td[11]).find('select').val(pr[i].sucursal);
			this.productoshtml.append(newRow);			
			$('tbody .vencimiento').mask('00/0000');
		}

		if(this.focused!=undefined && this.focused2!=undefined){
			//var foc = $('tr[data-codigo="'+this.focused+'"]').find('*[name="'+this.focused2+'"]').closest('td').find('input').focus();			
		}
		

		$("select.chosen-select").chosen().trigger('liszt:updated');
		if(this.refrshEvents){			
			this.refrshEvents = false;
		}

		this.updateTemp();
	}

	this._addProduct = function(producto,cantidad){
        cantidad = cantidad!=undefined?cantidad:1;
        var pr = this.datos.productos;
        var enc = false;
        producto.vencimiento = typeof(producto.vencimiento)=='undefined'?'00/0000':producto.vencimiento;
        for(var i in pr){
            if(pr[i].codigo==producto.codigo && (controlar_vencimiento == 0 || pr[i].vencimiento==producto.vencimiento)){
                enc = true;
                this.datos.productos[i].cantidad+=cantidad;				
            }
        }
        if(!enc){
            //var precio = this.getPrecio(producto,cantidad);
            if(controlar_vencimiento==1){
            	producto.codigovenc = producto.codigo+producto.vencimiento;
            }                                
            producto.cantidad = cantidad;
            producto.por_desc = 0;
            producto.por_venta = isNaN(parseFloat(producto.porc_venta))?0:parseFloat(producto.porc_venta);
            producto.precio_costo = isNaN(parseFloat(producto.precio_costo))?0:parseFloat(producto.precio_costo);
            producto.precio_costo_ant = isNaN(parseFloat(producto.precio_costo))?0:parseFloat(producto.precio_costo);
			producto.sucursal = $('#sucursales').val();
            if(!isNaN(this.descontar) && this.descontar>0){
            	producto.desc_compra = this.descontar;
            	producto.precio_costo = producto.precio_costo_ant - ((producto.desc_compra * producto.precio_costo_ant) / 100);
            }            
            
            producto.precio_costo_origin = producto.precio_costo;
            this.datos.productos.push(producto);
            this.refrshEvents = true;
        }

        this.updateData();
	}
        
    this.getBalanza = function(codigo,medida){
        if(codigo.toString().length!=13){
            return codigo;
        }
        medida = typeof medida == 'undefined'?'peso':medida;
        for(var i in codigo_balanza){
            var cc = codigo_balanza[i];
            var numerosBalanza = cc.toString().length;
            if(codigo.toString().substring(0,numerosBalanza)==cc){
                var peso = parseInt(codigo.toString().substring(7,12));
                peso = medida=='peso'?parseInt(peso)/1000:parseFloat(peso);
                codigo = parseInt(codigo.toString().substring(2,7));
                codigo = {
                    codigo: codigo,
                    cantidad: peso
                }
                return codigo;  
            }
        }
        return codigo;
    }

	this.addProduct = function(codigo,obj){                                
		//buscamos el codigo
		var l = this;				
		if(codigo!=''){
	        codigo = codigo.split('*');
	        if(codigo.length==2){
                cantidad = parseFloat(codigo[0]);
                codigo = codigo[1];
	        }else{
                codigo = codigo[0];
                codigo = codigo.split('+');
                if(codigo.length==2){
                    cantidad = parseFloat(codigo[0]);
                    codigo = codigo[1];	
                }
                else{
                    cantidad = 1;
                    codigo = codigo[0];	
                }
	        }
	        
	        //Es balanza?                                        
	        codigo = this.getBalanza(codigo);                                        
	        cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
	        codigo = typeof codigo == 'object'?codigo.codigo:codigo;
	        console.log(codigo); 
	        $.post(URI+'movimientos/productos/productos/json_list',{
	                'codigo':codigo,
	                'cliente':this.datos.cliente
	        },function(data){
                data = JSON.parse(data);
                if(data.length>0){
                	$("#codigoAdd").val('');                    
                    if(controlar_vencimiento==1 && typeof(obj)!='undefined'){
                    	data[0].vencimiento = obj.vencimiento;
                    }else if(controlar_vencimiento==1){
						data[0].vencimiento = '00/0000';
                    }
                    $("#codigoAdd").attr('placeholder','Código de producto');
                    $("#codigoAdd").removeClass('error');
                    l._addProduct(data[0],cantidad);
                }else{
                        $("#codigoAdd").val('');
                        $("#codigoAdd").attr('placeholder','Producto no encontrado');
                        $("#codigoAdd").addClass('error');
                }
	        });
		}
	}

	this.removeProduct = function(codigo){		
		if(codigo!=''){
			var pr = this.datos.productos;
			var pr2 = [];
			for(var i in pr){				
				if(pr[i].codigo!=codigo && pr[i].codigovenc!=codigo){
					pr2.push(pr[i]);
				}
			}
		}
		this.datos.productos = pr2;
		this.updateData();
		this.refrshEvents = true;
	}
}

Compras.prototype.updateTemp = function(){	
    if(this.datos.productos.length>0){
        $.post(URI+'movimientos/compras/compras/updateTemp',{
            productos:JSON.stringify(this.datos.productos)
        },function(data){
            if(data!='success'){
                alert(data);
                console.log(data);
            }
        });
    }
}

Compras.prototype.loadTemp = function(){
    let $this = this;
    $.get(URI+'movimientos/compras/compras/getTemp',{},function(data){
        data = JSON.parse(data);
        if(data.length>0){
            $this.datos.productos = data;
            if($this.datos.productos[0].desc_compra){
            	$this.inputs.descontar.val($this.datos.productos[0].desc_compra);
            }
            $this.updateData();    
        }
    });
}