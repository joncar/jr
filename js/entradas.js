var PuntoDeVenta = function(){
	
	this.productoHTML = $("#productoVacio").clone();
	this.productoHTML.removeAttr('id');
	this.productoHTML.find('a,input').show();
	this.productoshtml = $("#ventaDescr tbody");
	this.alt = false;
	this.modPrecio = 1;
	this.initEvents = function(){
		var l = this;
	    $(document).on('keyup','#codigoAdd',function(e){
	    	if(e.which==13){	    		
	    		l.addProduct($(this).val());
	    	}
	    });
	    $(document).on('click','.insertar',function(){
	    	l.addProduct($('#codigoAdd').val());
	    });
	    /* Event precio, cantidad en productos */
	    $(document).on('change','.precio',function(){
	    	var precio_venta = parseFloat($(this).val());
	    	var codigo = $(this).parents('tr').data('codigo');
	    	for(var i in l.datos.productos){
	    		if(
                    (controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                    l.datos.productos[i].codigovenc==codigo
                ){
	    			l.datos.productos[i].precio_venta = precio_venta;	    			
	    		}
	    	}
	    	l.updateFields();
	    });

	    //Event Remove
	    $(document).on('click','.rem',function(){
	    	l.removeProduct($(this).parents('tr').data('codigo'));
	    });

	    $(document).on('change','.cantidad',function(){
            var cantidad = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(
                    (controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                    l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].cantidad = cantidad;
                    l.datos.productos[i].precio_venta = l.getPrecio(l.datos.productos[i],cantidad);
                }
            }
            l.updateFields();
	    });

	    $(document).on('change','input[name="vencimiento"]',function(){
            var vencimiento = $(this).val();
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(
                    (controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                    l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].vencimiento = vencimiento;
                }
            }
            l.updateFields();
	    });

	    $(document).on('change','select[name="sucursal"]',function(){
            var sucursal = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(
                    (controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                    l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].sucursal = sucursal;                    
                }
            }
            l.updateFields();
	    });

		$(document).on('change','input[name="observacion"]',function(){
	    	l.datos.observacion = $(this).val();
	    	l.updateFields();
	    });


	    //Otros campos
	    $(document).on('change','#field-proveedor,#field-motivo,#fecha',function(){l.updateFields();});

	    //Atajos
	    $(document).on('keydown',function(e){
	    	if(e.which==18){
	    		l.alt = true;
	    	}
	    });

	    //Atajos
	    $(document).on('keyup',function(e){
	    	if(e.which==18){
                l.alt = false;
	    	}else{
                if(l.alt){                	
                    switch(e.which){
                        case 67: //[c] Focus en codigos
                                $("#codigoAdd").focus();
                        break;
                        case 73: //[i] busqueda avanzada
                                $("#inventarioModal").modal('toggle');		    				
                        break;
                        case 80: //[p] Procesar venta
                                $("#procesar").modal('toggle');		    				
                        break;
                        case 78: //[n] Nueva venta
                                nuevaVenta();
                        break;
                        case 66: //[B] Nueva venta
                                saldo();
                        break;
                    }
                }
	    	}

	    });

	    $(document).on('change','.vencimiento',function(){
            var vencimiento = $(this).val();
            var codigo = $(this).parents('tr').data('codigo');  
            for(var i in l.datos.productos){
                if(
                	(controlar_vencimiento==0 && l.datos.productos[i].codigo==codigo) ||
                	l.datos.productos[i].codigovenc==codigo
                ){
                    l.datos.productos[i].vencimiento = vencimiento;                    
                    if(controlar_vencimiento==1){
                    	l.datos.productos[i].codigovenc = l.datos.productos[i].codigo+vencimiento;
                    	$(this).parents('tr').attr('data-codigo',codigo+vencimiento)
                	}
                }
            }
            l.updateData();
	    });
	}

	this.selectClient = function(data,val){
		var opt = '';
        for(var i in data){
          data[i].mayorista = data[i].mayorista=='activo' || data[i].mayorista=='1'?1:0;
          opt+= '<option value="'+data[i].id+'" data-mayorista="'+data[i].mayorista+'">'+data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos+'</option>';
        }
        if(data.length==0){
          opt+= '<option value="">Cliente no existe</option>';
        }
        $("#cliente").html(opt);
        $("#cliente").chosen().trigger('liszt:updated');
        $("#cliente_chzn input[type='text']").val(val);
    	$("#cliente").trigger('change');
    	this.updateFields();
	}
        
    this.getPrecio = function(producto,cantidad){         
        var precio = parseFloat(producto.precio_venta);  
        var cant_1 = parseFloat(isNaN(producto.cant_1))?0:producto.cant_1;
        var cant_2 = parseFloat(isNaN(producto.cant_2))?0:producto.cant_2;
        var cant_3 = parseFloat(isNaN(producto.cant_3))?0:producto.cant_3;
        if(this.datos.mayorista==1 && parseInt(ajustes.rango_precio_cantidades)==1){
            var enc = false;            
            if(parseFloat(cant_1)!=0 || parseFloat(cant_2)!=0 || parseFloat(cant_3)!=0){            	
            	if(parseInt(cantidad)>=parseInt(cant_1)){                    
                    precio = parseFloat(producto.precio_venta_mayorista1);
                }                
                if(parseInt(cantidad)>=parseInt(cant_2)){                    
                    precio = parseFloat(producto.precio_venta_mayorista2);
                }                
                if(parseInt(cantidad)>=parseInt(cant_3)){                    
                    precio = parseFloat(producto.precio_venta_mayorista3);
                }
            }else{
	            for(var i in cantidades_mayoristas){
	            	if(!enc && parseInt(cantidad)>=parseInt(cantidades_mayoristas[i].desde)){
	                    var campo = cantidades_mayoristas[i].campo;                                                    
	                    precio = parseFloat(producto[campo]);
	                }
	            }
        	}
        }
        return precio;
    }

	this._addProduct = function(producto,cantidad){
        cantidad = cantidad!=undefined?cantidad:1;
        var pr = this.datos.productos;
        var enc = false;
        for(var i in pr){
            if(pr[i].codigo==producto.codigo && (controlar_vencimiento == 0 || pr[i].vencimiento==producto.vencimiento.split(',')[0])){
                enc = true;
                this.datos.productos[i].cantidad+=cantidad;				
            }
        }
        if(!enc){
            var precio = this.getPrecio(producto,cantidad);
            var precio_descuento = 0;
            if(parseFloat(producto.descmax)>0){        			
		    	var descuento = parseFloat(producto.descmax);			    			    		
		    	precio_descuento = (descuento*precio)/100;		    		
			}       			
            this.datos.productos.push({
                    codigo:producto.codigo,
                    nombre:producto.nombre_comercial,
                    cantidad:cantidad,
                    stock:producto.stock,
                    por_desc:parseFloat(producto.descmax),
                    precio_venta:precio,
                    precio_costo:parseFloat(producto.precio_costo),
                    cant_1:producto.cant_1,
                    cant_2:producto.cant_2,
                    cant_3:producto.cant_3,
                    precio_venta_mayorista1: parseFloat(producto.precio_venta_mayorista1),
                    precio_venta_mayorista2: parseFloat(producto.precio_venta_mayorista2),
                    precio_venta_mayorista3: parseFloat(producto.precio_venta_mayorista3),
                    precio_descuento:precio_descuento,
                    total: 0,
                    sucursal:this.datos.sucursal
            });
            //var precio = this.getPrecio(producto,cantidad);
            if(controlar_vencimiento==1){
            	this.datos.productos[this.datos.productos.length-1].codigovenc = producto.codigo+producto.vencimiento;
            	this.datos.productos[this.datos.productos.length-1].vencimiento = producto.vencimiento;
            }             
        } 
           
        this.updateFields();
        
	}
        
    this.getBalanza = function(codigo,medida){
        if(codigo.toString().length!=13){
            return codigo;
        }
        medida = typeof medida == 'undefined'?'peso':medida;
        for(var i in codigo_balanza){
            var cc = codigo_balanza[i];
            var numerosBalanza = cc.toString().length;
            if(codigo.toString().substring(0,numerosBalanza)==cc){
                var peso = parseInt(codigo.toString().substring(7,12));
                peso = medida=='peso'?parseInt(peso)/1000:parseFloat(peso);
                codigo = parseInt(codigo.toString().substring(2,7));
                codigo = {
                    codigo: codigo,
                    cantidad: peso
                }
                return codigo;  
            }
        }
        return codigo;
    }

	this.addProduct = function(codigo,obj){                                
		//buscamos el codigo
		var l = this;				
		if(codigo!=''){
            codigo = codigo.split('*');
            if(codigo.length==2){
                    cantidad = parseFloat(codigo[0]);
                    codigo = codigo[1];
            }else{
                    codigo = codigo[0];
                    codigo = codigo.split('+');
                    if(codigo.length==2){
                            cantidad = parseFloat(codigo[0]);
                            codigo = codigo[1];	
                    }
                    else{
                            cantidad = 1;
                            codigo = codigo[0];	
                    }
            }
            
            //Es balanza?                                        
            codigo = this.getBalanza(codigo);                                        
            cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
            codigo = typeof codigo == 'object'?codigo.codigo:codigo;
            console.log(codigo); 
            $.post(URI+'movimientos/productos/productos/json_list',{
                    'codigo':codigo,
                    'operator':'where',
                    'cliente':this.datos.cliente
            },function(data){
                    data = JSON.parse(data);
                    if(data.length>0){
                    	if(vender_sin_stock==1 || (cantidad>0 && parseFloat(data[0].stock)>=cantidad) || (data[0].inventariable=='inactivo')){
                            $("#codigoAdd").val('');
                            if(controlar_vencimiento==1 && typeof(obj)!='undefined'){
		                    	data[0].vencimiento = obj.vencimiento;
		                    }else if(controlar_vencimiento==1){
								data[0].vencimiento = '00/0000';
		                    }                           
                            $("#codigoAdd").attr('placeholder','Código de producto');
                            $("#codigoAdd").removeClass('error');
                            l._addProduct(data[0],cantidad);
                        }else{
                        	$("#codigoAdd").val('');
                            $("#codigoAdd").attr('placeholder','Sin stock suficiente');
                            $("#codigoAdd").addClass('error');
                        }
                    }else{
                            $("#codigoAdd").val('');
                            $("#codigoAdd").attr('placeholder','Producto no encontrado');
                            $("#codigoAdd").addClass('error');
                    }
            });
		}
	}

	this.removeProduct = function(codigo){
		if(codigo!=''){
			var pr = this.datos.productos;
			var pr2 = [];
			for(var i in pr){
				if(pr[i].codigo!=codigo && pr[i].codigovenc!=codigo){
					pr2.push(pr[i]);
				}
			}
		}
		this.datos.productos = pr2;
		this.updateFields();
	}

	this.updateFields = function(){
		this.datos.cliente = $("#cliente").val();
		this.datos.fecha = $("#fecha").val();
		this.datos.proveedor = $("#field-proveedor").val();
		this.datos.motivo = $("#field-motivo").val();
		this.datos.observacion = $("#field-observacion").val();										
		//Calcular total
		var pr = this.datos.productos;
		var total = 0;
		for(var i in pr){
			this.datos.productos[i].total = pr[i].cantidad*pr[i].precio_costo;
			total+= this.datos.productos[i].total;
		}
		this.datos.total_monto = total;
		this.datos.total_costo = total;
		//Calcular divisas
		this.datos.total_dolares = total/tasa_dolar;
		this.datos.total_reales = total/tasa_real;
		this.datos.total_pesos = total/tasa_peso;
		//Calcular acumulado
		this.datos.total_efectivo = 0;
		this.datos.total_efectivo+= this.datos.pago_guaranies;
		this.datos.total_efectivo+= tasa_dolar>0?this.datos.pago_dolares/tasa_dolar:0;
		this.datos.total_efectivo+= tasa_real>0?this.datos.pago_reales/tasa_real:0;
		this.datos.total_efectivo+= tasa_peso>0?this.datos.pago_pesos/tasa_peso:0;		
		//this.datos.vuelto = this.datos.vuelto<0?0:this.datos.vuelto;
		//Calcular acumulado
		this.datos.total_pagado = 0;		
		this.datos.total_pagado+= this.datos.pago_guaranies;
		this.datos.total_pagado+= tasa_dolar>0?this.datos.pago_dolares*tasa_dolar:0;
		this.datos.total_pagado+= tasa_real>0?this.datos.pago_reales*tasa_real:0;
		this.datos.total_pagado+= tasa_peso>0?this.datos.pago_pesos*tasa_peso:0;
		this.datos.total_pagado+= this.datos.total_debito>0?this.datos.total_debito:0;		
		//Calcular vuelto		
		this.datos.vuelto = this.datos.total_pagado>0?this.datos.total_pagado - this.datos.total_monto:0;
		this.datos.vuelto_dolares = this.datos.pago_dolares>0?this.datos.pago_dolares - this.datos.total_dolares:0;
		this.datos.vuelto_reales =  this.datos.pago_reales>0?this.datos.pago_reales - this.datos.total_reales:0;
		this.datos.vuelto_pesos =   this.datos.pago_pesos>0?this.datos.pago_pesos - this.datos.total_pesos:0;		
		this.datos.vuelto_dolares = this.datos.vuelto_dolares<0?0:this.datos.vuelto_dolares;
		this.datos.vuelto_reales =  this.datos.vuelto_reales<0?0:this.datos.vuelto_reales;
		this.datos.vuelto_pesos =   this.datos.vuelto_pesos<0?0:this.datos.vuelto_pesos;		
		this.refresh();
	}

	this.refresh = function(){
		const gs = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'PYG',minimumFractionDigits: 0});
		const usd = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'USD',minimumFractionDigits: 2});
		const arg = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'ARP',minimumFractionDigits: 2});
		const br = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'BRL',minimumFractionDigits: 2});
		$("input[name='observacion']").val(this.datos.observacion);
		$("input[name='total_venta']").val(gs.format(this.datos.total_monto));
		$("input[name='total_pesos']").val(arg.format(this.datos.total_pesos));
		$("input[name='total_reales']").val(br.format(this.datos.total_reales));
		$("input[name='total_dolares']").val(usd.format(this.datos.total_dolares));
		$("input[name='pago_guaranies']").val(gs.format(this.datos.pago_guaranies));
		$("input[name='pago_pesos']").val(arg.format(this.datos.pago_pesos));
		$("input[name='pago_reales']").val(br.format(this.datos.pago_reales));
		$("input[name='pago_dolares']").val(usd.format(this.datos.pago_dolares));
		$("input[name='total_debito']").val(gs.format(this.datos.total_debito));
		$("input[name='efectivo']").val(this.datos.total_efectivo);
		$("input[name='vuelto']").val(gs.format(this.datos.vuelto));
		$("input[name='vuelto_pesos']").val(arg.format(this.datos.vuelto_pesos));
		$("input[name='vuelto_reales']").val(br.format(this.datos.vuelto_reales));
		$("input[name='vuelto_dolares']").val(usd.format(this.datos.vuelto_dolares));
		$("input[name='fecha']").val(this.datos.fecha);
		$("#field-pedido").val(this.datos.pedido);
		$("#field-pedido").chosen().trigger('liszt:updated');
		$("#field-pagado").val(this.datos.pagado);		
		$("#field-pagado").chosen().trigger('liszt:updated');

		$("#total_total span").html(gs.format(this.datos.total_monto));
		$("#total_vuelto span").html(gs.format(this.datos.vuelto));
		
		if(this.datos.transaccion=='1'){
			$(".patternCredito").css('display','none');
			$("#pago_total span").html(gs.format(this.datos.total_pagado));
		}else{
			$(".patternCredito").css('display','block');
			$("#pago_total span").html(gs.format(this.datos.total_monto));
		}


		$("#cantidadProductos").html(this.datos.productos.length);		
		$("#procesar_total_efectivo").html(gs.format(this.datos.total_efectivo));
		$("#nroFactura").html(this.datos.nro_factura);
		$(".tipoFacturacion[value='"+this.datos.tipo_facturacion_id+"']").prop('checked',true);		
		if($(".tipoFacturacion[value='"+this.datos.tipo_facturacion_id+"']").data('emitefactura')=='1'){
			$("#facturaD").show();
		}else{
			$("#facturaD").hide();
		}

		//Draw products
		var pr = this.datos.productos;
		this.productoshtml.html('');		
		for(var i=pr.length-1;i>=0;i--){
			var newRow = this.productoHTML.clone();
			var td = newRow.find('td');
			var c = pr[i].codigo;
            if(controlar_vencimiento==1){
                c+= pr[i].vencimiento;
            }
            newRow.attr('data-codigo',c);
			$(td[0]).find('span').html(pr[i].codigo);
			$(td[1]).html(pr[i].nombre);
			
			$(td[2]).find('input').val(pr[i].vencimiento);

			$(td[3]).find('input').val(pr[i].cantidad);
			$(td[4]).find('input').val(pr[i].precio_costo.toFixed(0));
			$(td[5]).find('input').val(pr[i].precio_venta.toFixed(0));
			if(this.modPrecio==0){
				$(td[5]).find('input').attr('disabled',true);
			}			
			$(td[6]).html(gs.format(pr[i].total));
			$(td[7]).find('select').val(pr[i].sucursal);
			$(td[8]).html(pr[i].stock);
			this.productoshtml.append(newRow);	
			$('tbody .vencimiento').mask('00/0000');		
		}		
	}

	this.setDatos = function(datos){
		this.datos = datos;
		this.datos.total_monto = parseFloat(this.datos.total_monto);
		this.datos.total_pesos = parseFloat(this.datos.total_pesos);
		this.datos.total_reales = parseFloat(this.datos.total_reales);
		this.datos.total_dolares = parseFloat(this.datos.total_dolares);
		this.datos.vuelto = parseFloat(this.datos.vuelto);
		this.datos.total_efectivo = parseFloat(this.datos.total_efectivo);			
		for(var i in this.datos.productos){
			this.datos.productos[i].cantidad = parseFloat(this.datos.productos[i].cantidad);
			this.datos.productos[i].precio_costo = parseFloat(this.datos.productos[i].precio_costo);					
			this.datos.productos[i].precio_venta = parseFloat(this.datos.productos[i].precio_venta);			
			this.datos.productos[i].total = parseFloat(this.datos.productos[i].total);			
		}	
		this.refresh();	
	}

	this.initVenta = function(){
		this.datos = {			
			sucursal:'',
			caja:'',
			cajadiaria:'',
			fecha:'',
			nro_factura:'',
			usuario:'',
			cliente:0,
			transaccion:0,
			forma_pago:0,
			observacion:'',							
			total_pesos:0,
			total_descuentos:0,
			total_monto:0,
			total_dolares:0,
			total_reales:0,
			total_efectivo:0,
			pago_dolares:0,
			pago_reales:0,
			pago_pesos:0,
			pago_guaranies:0,
			total_debito:0,
			vuelto_dolares:0,
			vuelto_reales:0,
			vuelto_pesos:0,		
			vuelto:0,
			proveedor:0,
			motivo:0,
			total_costo:0,
			productos:[],
            mayorista:0,
            pedido:0,
            pagado:0
                                
		};
		var l = this;
		$.get(URI+'movimientos/ventas/ventas/nroFactura',{},function(data){
			l.datos.nro_factura = data;
			l.refresh();
			$("#codigoAdd").focus();
		});

		$("#facturaD").hide();
	}

	this.initVenta();
}

