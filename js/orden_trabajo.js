const Venta = function(){
    this.cliente = {
        mayorista:0,
        porc_descuento:0,
        limite_credito:0,
        porc_credito:0,
        despensa:0,
    };
    this.stocks = [];
}

Venta.prototype.initEvents = function(){
    var l = this;
    this.tr = $("#productoVacio").clone();
    this.tbody = $("#productoVacio").parents('tbody');
    $("#productoVacio").remove();
    $(document).on('keydown','#codigoAdd',function(e){
        
        if(e.which==13){
            e.preventDefault();
            l.addProduct($(this).val());
        }
    });

    $(document).on('change','#field-cliente',function(){
        l.validarCliente().then(()=>{
            l.updateEmpty();
            l.updateTemp();
        });        
    });

    $(document).on('change','#field-transaccion,#field-forma_pago',function(){
        l.updateEmpty();
        l.updateTemp();
    });

    $(document).on('keydown','.producto input',function(e){
        
        if(e.which==13){
            e.preventDefault();            
        }
    });    

    $(document).on('keyup','#procesar input',function(e){
        l.actualizarPago();
    });

    $(document).on('click','.insertar',function(){
        l.addProduct($('#codigoAdd').val());
    });

    $(document).on('click','.rem',function(){
        
        $(this).parents('tr').remove();        
        l.updateEmpty();
        l.updateTemp(true);

    });

    $(document).on('change','#field-tipo_venta',function(){
        
        //Si no es mayorista y se coloca como mayorista
        $(this).parents('tr').remove();        
        l.updateEmpty();
        l.updateTemp(true);

    });

    $(document).on('change','.producto input:not([name="precio_venta"]):not([data-name="por_desc"])',function(){        
        l.updateEmpty();
        l.updateTemp();
    });

    $(document).on('change','input[name="pago_guaranies"],input[name="pago_reales"],input[name="pago_dolares"],input[name="pago_pesos"]',function(){
        l.actualizarPago();
    });

    $(document).on('change','.producto input[name="precio_venta"]',function(){        
        $(this).parents('tr').find('[name="precio_min_unidad"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_min_cant"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_min_caja"]').val($(this).val());

        $(this).parents('tr').find('[name="precio_may_unidad"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_may_cant"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_may_caja"]').val($(this).val());

        $(this).parents('tr').find('[name="precio_campo_unidad"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_campo_cant"]').val($(this).val());
        $(this).parents('tr').find('[name="precio_campo_caja"]').val($(this).val());
        l.updateEmpty();
    });

    /*$(document).on('change','.por_desc',function(){
        $(this).parents('tr').find('[data-name="por_desc_producto"]').val($(this).val());        
        l.updateEmpty();
    });

    $(document).on('change','[data-name="precio_descuento"]',function(){
        var porcentaje = 0;//235*100/4700
        var producto = $(this).parents('tr');
        var por_desc = producto.find('[data-name="por_desc"]');
        var precio = parseFloat(producto.find('[data-name="precio_venta"]').val());        
        var newdesc = parseFloat($(this).val());
        if(!isNaN(newdesc) && !isNaN(precio)){
            porcentaje = newdesc*100/precio;
        }

        por_desc.val(porcentaje.toFixed(2));
        l.updateEmpty();
    })*/

    $(document).on('change','.producto .por_desc',function(){
        
        //Si no es mayorista y se coloca como mayorista
        var producto = $(this).parents('tr');
        var precio_descuento = producto.find('[data-name="precio_descuento"]');
        var cantidad = parseFloat(producto.find('[data-name="cantidad"]').val());
        var precio = parseFloat(producto.find('[data-name="precio_venta"]').val());
        var descuento = parseFloat($(this).val());
        if(!isNaN(precio) && !isNaN(cantidad) && !isNaN(descuento)){
            precio = precio*cantidad;
            precio_descuento.val((descuento*precio)/100);
        }
        l.updateEmpty();
        l.updateTemp(true);

    });

    $(document).on('keydown',function(e){

        if(e.which==18 && !l.alt){
            e.preventDefault();
            l.alt = true; 
            $(".alt").show();               
        }

        else if(e.which==18 && l.alt){
            e.preventDefault();
            l.alt = false;                
            $(".alt").hide();
        }

        if(l.alt){
            e.preventDefault();
        }
    });
    $(document).on('keyup',function(e){                        
        if(l.alt){   

            switch(e.which){
                case 67: //[c] Focus en codigos
                    $("#codigoAdd").focus();
                    l.alt = false;
                    $(".alt").hide();
                break;
                case 73: //[i] busqueda avanzada
                    $("#inventarioModal").modal('toggle');
                    l.alt = false;  
                    $(".alt").hide();                          
                break;
                case 80: //[p] Procesar venta
                    /*$("#procesar").modal('toggle');        */
                    l.procesar();
                    l.alt = false;  
                    $(".alt").hide();                  
                break;
                case 78: //[n] Nueva venta
                    l.nuevaVenta();
                    l.alt = false;
                    $(".alt").hide();
                break;
                case 66: //[B] Nueva venta
                    saldo();
                    l.alt = false;
                    $(".alt").hide();
                break;
                case 72: //[H] Enfocar en la cabecera
                    //$('a[href="#addCliente"]').focus();
                    $('#field-cliente').chosen().trigger('liszt:activate'); 
                    l.alt = false;  
                    $(".alt").hide();                        
                break;
                case 87: //[W] New Window
                    window.open($("#newWindow").attr('href'),'_new');
                    l.alt = false;
                    $(".alt").hide();
                break;
                case 38: //UP
                    //Cantidad focus
                    var index = 0;
                    var focused = $('.cantidad:focus');
                    if(focused.length>0){
                        index = $('.cantidad:focus').parents('tr').index();
                        index-=1;                                
                        index = index<0?0:index;
                    }
                    $($('.cantidad')[index]).focus(); 
                    l.alt = false; 
                    $(".alt").hide();                            
                break;
                case 40: //DOWN
                    //Cantidad focus
                    var index = 0;
                    var focused = $('.cantidad:focus');                            
                    if(focused.length>0){
                        index = $('.cantidad:focus').parents('tr').index();
                        index+=1;                                
                        index = index>$('.cantidad').length?$('.cantidad').length-1:index;
                    }
                    $($('.cantidad')[index]).focus(); 
                    l.alt = false;      
                    $(".alt").hide();                      
                break;

                case 88: //X
                    //Cantidad focus
                    //$('#addCliente').modal('show');
                    l.AddCliente();
                    l.alt = false;      
                    $(".alt").hide();                      
                break;
            }
        }
    });
}

Venta.prototype.addStocks = function(data){
    if(data.stocks.length>0){
        data.stock = parseFloat(data.stocks[0].stock);
    }else{
        data.stock = 0;
    }
    for(var i in data.stocks){
        this.stocks.push(data.stocks[i]);
    }    
    return data;    
}

Venta.prototype.getBalanza = (codigo,medida) =>{
    if(codigo.toString().length!=13){
        return codigo;
    }
    medida = typeof medida == 'undefined'?'peso':medida;
    for(var i in ajustes.codigo_balanza){
        var cc = ajustes.codigo_balanza[i];
        var numerosBalanza = cc.toString().length;
        if(codigo.toString().substring(0,numerosBalanza)==cc){
            var peso = parseInt(codigo.toString().substring(7,12));
            peso = medida=='peso'?parseInt(peso)/1000:parseFloat(peso);
            codigo = parseInt(codigo.toString().substring(2,7));
            codigo = {
                codigo: codigo,
                cantidad: peso
            }
            return codigo;  
        }
    }
    return codigo;
}

Venta.prototype.formatearCodigo = function (codigo){    
    codigo = codigo.split('*');
    if(codigo.length==2){
        cantidad = codigo[0];
        codigo = codigo[1];
    }else{
        codigo = codigo[0];
        codigo = codigo.split('+');
        if(codigo.length==2){
            cantidad = codigo[0];
            codigo = codigo[1];                    
            /*if(ajustes.activar_mayorista_por_venta == '1'){
                forceMayorista = true;
            }*/
        }
        else{
            cantidad = 1;
            codigo = codigo[0];
        }
    }
    if(typeof cantidad.substring == 'function' && cantidad.substring(0,1)=='m'){
        cantidad = parseFloat(cantidad.substring(1));
        cantidad = isNaN(cantidad)?1:cantidad;
        forceMayorista = true;
    }else{
        cantidad = parseFloat(cantidad);
    }
    
    //Es balanza?   
    var codigoOriginal = codigo;        
    codigo = this.getBalanza(codigo);
    var balanza = typeof codigo == 'object'?1:0;
    cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
    codigo = typeof codigo == 'object'?codigo.codigo:codigo;      
    return {
        codigo: codigo,
        cantidad: cantidad,
        balanza: balanza,
        venta:1
    };
}

Venta.prototype.updateEmpty = function(){
    if(this.tbody.find('tr').length>1){
        this.tbody.find('.empty').hide();
    }else{
        this.tbody.find('.empty').show();
    }
    this.updateTotales();
}

Venta.prototype.formatCurrency = function(input,decimales){   
    var val = input;     
    input = parseInt(input);
    input = isNaN(input)?0:input;        
    let num = (input === 0) ? "" : input.toLocaleString("en-US");
    input = num.split(",").join("."); 
    input = input==''?0:input;
    if(decimales){
        val = parseFloat(val);
        //val = val.toLocaleString("en-US");
        val = val.toFixed(2);
        val = val.split('.');
        if(val.length==2){
            input+= ','+val[1];
        }
    }       
    return input;
}

Venta.prototype.updateTotales = function(){
    var totalVenta = 0;
    var totalRow = 0;
    var cantidad = 0;
    var tipo = $("#field-tipo_venta").val();    
    var productos = $(".producto"); 
    var precios = [];
    var cantidades = [];    
    $("#cant").html(productos.length);
    if(productos.length>0){
        for(var i=0;i<productos.length;i++){
            var producto = $(productos[i]);
            var cantidad = parseFloat(producto.find('input[data-name="cantidad"]').val());
            var por_desc = parseFloat(producto.find('input[data-name="por_desc"]').val());
            var precio = parseFloat(producto.find('input[data-name="precio_venta"]').val());            
            var precio_descuento = parseFloat(producto.find('[data-name="precio_descuento"]').val());
            var total = precio*cantidad;
            //total-= precio_descuento;
            totalVenta+= total;

            producto.find('input[data-name="total"]').val(total);
            producto.find('span[data-val="total"]').html(this.formatCurrency(total));
        }
    }
    $("#span_total_venta").html(this.formatCurrency(totalVenta));
    $("input[name='total_orden']").val(totalVenta);

    //Divisas
    var dolares = parseFloat(ajustes.tasa_dolares);
    var reales = parseFloat(ajustes.tasa_reales);
    var pesos = parseFloat(ajustes.tasa_pesos);

    dolares = (totalVenta/dolares).toFixed(2);
    reales = (totalVenta/reales).toFixed(2);
    pesos = (totalVenta/pesos).toFixed(2);

    $("input[name='total_dolares']").val(dolares);
    $("input[name='total_reales']").val(reales);
    $("input[name='total_pesos']").val(pesos);

    $("#total_dolares").html(dolares);
    $("#total_reales").html(reales);
    $("#total_pesos").html(pesos);
    this.fixScroll();
}

Venta.prototype.addDescuentos = function(data){
    if(data.descuentos.length>0){
        data.por_desc = parseFloat(data.descuentos[0].por_desc); 
        data.por_desc_producto = parseFloat(data.descuentos[0].por_desc);        
    }    
    return data;    
}

Venta.prototype.addProducto = function(data){
    //data = this.addDescuentos(data);
    //data = this.addStocks(data);
    var newTr = this.tr.clone();
    newTr.removeAttr('id'); 
    var cols = newTr.find('th');
    for(var i in data){
        newTr.find('[data-val="'+i+'"]').html(data[i]);
        newTr.find('[data-name="'+i+'"]').val(data[i]);
    }   
    this.tbody.append(newTr);
    //Añadir campos extras
    var extras = newTr.find('.extraFields');
    for(var i in data){
        if(newTr.find('[data-name="'+i+'"]').length==0){
            val = data[i]?data[i]:0;
            extras.append('<input type="hidden" data-name="'+i+'" value="'+val+'">');
        }
    }
    this.updateEmpty(); 
}


Venta.prototype.checkIfPushed = function(codigo,cant){    
    var codigos = $("input[data-name='codigo']");
    var internos = $('input[data-name="codigo_interno"]');
    if(codigos.length>0){
        for(var i=0;i<codigos.length;i++){
            if($(codigos[i]).val()==codigo || $(internos[i]).val()==codigo){
                
                if(confirm('El producto ya ha sido introducido, ¿Quieres sumar la cantidad introducida?')){
                    var cantidad = parseFloat($(codigos[i]).parents('tr').find('input[data-name="cantidad"]').val());
                    var codigo_origen = $(codigos[i]).parents('tr').find('input[data-name="codigo_origen"]').val();
                    var unidad_medida_id = $(codigos[i]).parents('tr').find('input[data-name="unidad_medida_id"]').val();
                    var balanza = $(codigos[i]).parents('tr').find('input[data-name="balanza"]').val();
                    if(!codigo_origen || balanza=='0'){
                        cantidad+= cant;
                    }else{
                        cant = this.getBalanza(codigo_origen,unidad_medida_id=='1'?'cantidad':'peso');
                        cant = parseFloat(cant.cantidad);
                        console.log(cantidad,cant);
                        cantidad+= cant;
                    }
                    $(codigos[i]).parents('tr').find('input[data-name="cantidad"]').val(cantidad);
                }
                this.updateEmpty();
                return true;
            }
        }
    }
    return false;
}

Venta.prototype.addProduct = async function(codigo_origen){
    $("#codigoAdd").removeClass('is-invalid').attr('placeholder','Ingresa un código de producto');
    if(codigo_origen!=''){
        window.shouldSave = true;
        $('#codigoAdd').attr('placeholder','Buscando Producto').val('');
        try{
            codigo = this.formatearCodigo(codigo_origen);
            var form = new FormData();
            form.append('codigo',codigo.codigo);
            form.append('cantidad',codigo.cantidad);
            form.append('balanza',codigo.balanza);
            form.append('venta',1); 
            
            if(!this.checkIfPushed(codigo.codigo,codigo.cantidad)){
                let data = {
                    method: 'POST',
                    body: form        
                };
                const response = await fetch(URI+'movimientos/productos/productos/json_list',data).then((response)=>{
                    $('#codigoAdd').attr('placeholder','Código de producto (ALT + C)');                    
                    return response;
                });
                const datos = await response.json();
                if(datos.length>0){
                    let producto = datos[0];
                    producto.cantidad = codigo.cantidad;
                    //producto.total = 0;
                    //producto.precio_venta = 0;
                    producto.por_desc = 0;
                    producto.precio_descuento = 0;
                    producto.por_desc_producto = 0;
                    producto.balanza = codigo.balanza;  
                    producto.codigo_origen = codigo_origen;
                    if(codigo.balanza==1){
                        let bal = this.getBalanza(codigo_origen,(producto.unidad_medida_id=='1'?'unidad':'peso'));
                        producto.cantidad = bal.cantidad;
                        producto.codigo = producto.id;
                    }
                    this.addProducto(producto);
                }else{
                    $("#codigoAdd").addClass('is-invalid').attr('placeholder','Produto no encontrado');
                }
            }
            this.updateTemp();
        }catch(e){
            alert('Ocurrio un error al insertar');
            console.log(e);
        }
    }else{
        $('#codigoAdd').val('').attr('placeholder','Ingresa un código de producto').attr('disabled',false);
    }
}

Venta.prototype.fillNroFactura = async function(){
    try{
        if(tiposFacturacion.find(x=>x.id==$("#field-tipo_facturacion_id").val()).emite_factura == 0 ){
            $("input[name='nro_factura']").val(0);
            $("#nroFactura").html(0);
            $("#facturaD").hide();
        }else{
            const nro = await fetch(URI+'movimientos/ventas/ventas/nroFactura');
            const d = await nro.json();
            if(d.data){
                $("input[name='nro_factura']").val(d.data);
                $("#nroFactura").html(d.data);
                $("#facturaD").show();
            }
        }
    }catch(e){
        console.log(e);
        alert('Error capturando el nro de factura');
    }
}

Venta.prototype.reloadNroFactura = function(){
    this.fillNroFactura();
}

/***************************************** Enviar Venta *******************************************/
Venta.prototype.transformProductsInJson = function(){
    var prods = [];
    var productos = $(".producto");
    for(var i=0;i<productos.length;i++){
        var producto = $(productos[i]);
        var valores = producto.find('input');
        var prod = [];
        for(var k=0;k<valores.length;k++){
            var valor = $(valores[k]);
            prod[valor.data('name')] = valor.val();
          }

          valores = producto.find('select');
          for(var k=0;k<valores.length;k++){
            var valor = $(valores[k]);
            prod[valor.data('name')] = valor.val();
          }
        prods.push(Object.assign({},prod));
    }
    return prods;
}

Venta.prototype.actualizarPago = function(){
    var tag = $(".totalGs");
    var total_pagado = 0;
    var pago_guaranies = parseFloat($("input[name='pago_guaranies']").val());
    var pago_dolares = parseFloat($("input[name='pago_dolares']").val());
    var pago_pesos = parseFloat($("input[name='pago_pesos']").val());
    var pago_reales = parseFloat($("input[name='pago_reales']").val());
    var total_debito = parseFloat($("input[name='total_debito']").val());
    var total_cheque = parseFloat($("input[name='total_cheque']").val());
    var vuelto = $("input[name='vuelto']");
    var vuelto_dolares = $("input[name='vuelto_dolares']");
    var vuelto_reales = $("input[name='vuelto_reales']");
    var vuelto_pesos = $("input[name='vuelto_pesos']");
    var tasa_dolar = parseFloat(ajustes.tasa_dolares);
    var tasa_real = parseFloat(ajustes.tasa_reales);
    var tasa_peso = parseFloat(ajustes.tasa_pesos);
    var total_venta = parseFloat($("input[name='total_venta']").val());
    var total_dolares = parseFloat($("input[name='total_dolares']").val());
    var total_reales = parseFloat($("input[name='total_reales']").val());
    var total_pesos = parseFloat($("input[name='total_pesos']").val());
    var vuelto_dolaresf = 0;
    var vuelto_realesf = 0;
    var vuelto_pesosf = 0;
    //Validacion
    pago_guaranies = !isNaN(pago_guaranies)?pago_guaranies:0;
    pago_dolares = !isNaN(pago_dolares)?pago_dolares:0;
    pago_pesos = !isNaN(pago_pesos)?pago_pesos:0;
    pago_reales = !isNaN(pago_reales)?pago_reales:0;
    total_debito = !isNaN(total_debito)?total_debito:0;
    total_cheque = !isNaN(total_cheque)?total_cheque:0;
    tasa_dolar = !isNaN(tasa_dolar)?tasa_dolar:0;
    tasa_real = !isNaN(tasa_real)?tasa_real:0;
    tasa_peso = !isNaN(tasa_peso)?tasa_peso:0;
    //Sumatoria
    total_pagado+= pago_guaranies;
    total_pagado+= tasa_dolar>0?pago_dolares*tasa_dolar:0;
    total_pagado+= tasa_real>0?pago_reales*tasa_real:0;
    total_pagado+= tasa_peso>0?pago_pesos*tasa_peso:0;
    total_pagado+= total_debito>0?total_debito:0;        
    total_pagado+= total_cheque>0?total_cheque:0;
    //Vueltos
    vuelto_dolaresf = pago_dolares>0?pago_dolares - total_dolares:0;
    vuelto_realesf = pago_reales>0?pago_reales - total_reales:0;
    vuelto_pesosf = pago_pesos>0?pago_pesos - total_pesos:0;

    vuelto_dolaresf = vuelto_dolaresf<0?0:vuelto_dolaresf;
    vuelto_realesf = vuelto_realesf<0?0:vuelto_realesf;
    vuelto_pesosf = vuelto_pesosf<0?0:vuelto_pesosf;

    vuelto.val(total_pagado>0?total_pagado - total_venta:0);
    vuelto_dolares.val(vuelto_dolaresf.toFixed(2));
    vuelto_reales.val(vuelto_realesf.toFixed(2));
    vuelto_pesos.val(vuelto_pesosf.toFixed(2));

    tag.html(this.formatCurrency(total_pagado));
}

Venta.prototype.save = function(form){  
    if(!this.onsend){
        this.onsend = true;  
        $('.report').html('').removeClass('alert alert-danger alert-success alert-info');
        var url = $(form).attr('action');
        form = new FormData(form);
        form.append('productos',JSON.stringify(this.transformProductsInJson()));
        


        insertar(url,form,'.report',function(data){
            for(var i in data){
                data.success_message = data.success_message.replace('{id}',data.insert_primary_key);
            }
            venta.imprimir(data.insert_primary_key);
            $(document).find('.report').removeClass('alert').removeClass('alert-info');
            $(document).find('.report').addClass('alert alert-success').html(data.success_message);
            $(form).find('input[type="text"]').val('');
            $(form).find('input[type="number"]').val('');
            $(form).find('input[type="password"]').val('');
            $(form).find('select').val('');
            $(form).find('textarea').val('');
            window.shouldSave = false;
        },function(data){
          $(document).find('.report').removeClass('alert').removeClass('alert-info');
          $(document).find('.report').addClass('alert alert-danger').html(data.error_message);
          venta.onsend = false;
        });
    }else{
        error('.report','Ya se ha enviado una venta, por favor inicie una nueva venta');
    }
}

Venta.prototype.edit = function(form){      
    $('.report').html('').removeClass('alert alert-danger alert-success alert-info');
    var url = $(form).attr('action');
    form = new FormData(form);
    form.append('productos',JSON.stringify(this.transformProductsInJson()));
    


    insertar(url,form,'.report',function(data){
        for(var i in data){
            data.success_message = data.success_message.replace('{id}',data.insert_primary_key);
        }        
        $(document).find('.report').removeClass('alert').removeClass('alert-info');
        $(document).find('.report').addClass('alert alert-success').html(data.success_message);
        $(form).find('input[type="text"]').val('');
        $(form).find('input[type="number"]').val('');
        $(form).find('input[type="password"]').val('');
        $(form).find('select').val('');
        $(form).find('textarea').val('');                    
    },function(data){
      $(document).find('.report').removeClass('alert').removeClass('alert-info');
      $(document).find('.report').addClass('alert alert-danger').html(data.error_message);      
    });
}

Venta.prototype.imprimir = function(codigo){
    var url = tiposFacturacion.find(x=>x.id==$("#field-tipo_facturacion_id").val()).reporte;    
    var w = window.open(URI+url+codigo,'Impresion','width=800,height=600');
    if(ajustes.cerrarAlImprimirVenta=='1'){
        w.onload = function(){            
            setTimeout(function(){w.close()},3000);
        }        
    }
}

Venta.prototype.imprimir = function(codigo){
    var url = 'reportes/rep/verReportes/'+ajustes.id_reporte_presupuestos+'/html/valor/';
    window.open(URI+url+codigo);
}

Venta.prototype.nuevaVenta = function(){
    if(!window.shouldSave || confirm('¿Seguro desea crear una nueva venta y perder los datos ya almacenados?')){
        $(".producto").remove();
        $("#procesar input[type='text']").val('0');
        $("#procesar").modal('hide');
        $("#field-cliente").val(1).chosen().trigger('liszt:updated');
        $("#field-tipo_venta").val(1).chosen().trigger('liszt:updated');
        $("#field-transaccion").val(1).chosen().trigger('liszt:updated');
        $("#field-tipo_facturacion_id").val(1).chosen().trigger('liszt:updated');
        $("#field-forma_pago").val(1).chosen().trigger('liszt:updated');          
        var c = clienteDefault;
        $("#field-cliente").html('<option value="'+c.id+'">'+c.nro_documento+' '+c.nombres+' '+c.apellidos+'</option>').val(c.id).chosen().trigger('liszt:updated');
        $("#field-observacion").val('');
        $('.report').html('').removeClass('alert alert-danger alert-success alert-info');
        this.updateEmpty();
        this.updateTemp(true);
        venta.onsend = false;
    }
}

Venta.prototype.fixScroll = function(){
    setTimeout(function(){
        var box = document.getElementById('seguimiento');
        box.scrollTo(0,box.scrollHeight);
    },50);
}


/************************************************ Cliente **************************/
Venta.prototype.AddCliente = function(){
    $('#addCliente').modal('show');
}

Venta.prototype.validarCliente = async function(act){
    var cliente = $("#field-cliente").val();
    this.cliente = {
        mayorista:0,
        porc_desc:0,
        limite_credito:0,
        porc_credito:0,
        despensa:0
    };
    if(cliente!=1){
        var f = new FormData();
        f.append('clientes_id',$("#field-cliente").val());
        let response = await fetch(URI+'maestras/clientes/json_list',{method:'POST',body:f});
        let cliente = await response.json();
        if(cliente.length>0){
            cliente = cliente[0];
            //Mayorista?            
            this.cliente = {
                mayorista:(cliente.mayorista=='activo' || cliente.mayorista=='1')?1:0,
                porc_descuento:!isNaN(parseFloat(cliente.porc_descuento))?parseFloat(cliente.porc_descuento):0,
                limite_credito:!isNaN(parseFloat(cliente.limite_credito))?parseFloat(cliente.limite_credito):0,
                porc_credito:!isNaN(parseFloat(cliente.porc_credito))?parseFloat(cliente.porc_credito):0,
                despensa:cliente.despensa=='activo'?1:0,
            };
            $("#field-direccion").val(cliente.direccion);
            if(this.cliente.mayorista==0 && $("#field-tipo_venta").val()=='2'){
                $("#field-tipo_venta").val(1).chosen().trigger('liszt:updated');
            }else if(this.cliente.mayorista==1 && $("#field-tipo_venta").val()!='2'){
                $("#field-tipo_venta").val(2).chosen().trigger('liszt:updated');
            }else{
                this.updateEmpty();
            }
        }
    }
    return new Promise((resolve,reject)=>{
        resolve();
    });
}

Venta.prototype.validarStocks = function(){
    var productos = $(".producto");
    var ret = true;
    for(var i=0;i<productos.length;i++){
        var producto = $(productos[i]);
        var id = producto.find('[data-name="id"]').val();
        var cantidad = parseFloat(producto.find('input[data-name="cantidad"]').val());
        var stock = this.stocks.find(x=>x.productos_id==id);
        if(stock){
            if(parseFloat(stock.stock)<cantidad){
                ret = false;
            }
        }
    }
    return ret;
}

/************************************************ Temps **************************/

Venta.prototype.updateTemp = function(force){
    if($(".producto").length>0 || force){
        $.post(URI+'movimientos/OrdenTrabajo/orden_trabajo/updateTemp',{
            productos:JSON.stringify(this.transformProductsInJson())
        },function(data){
            if(data!='success'){
                alert(data);
                console.log(data);
            }
        });
    }
}

/*************************************** Token *********************/
Venta.prototype.validateToken = function(){
    return new Promise((resolve,reject)=>{
        if(ajustes.solicitar_token_ventas=='0'){
            resolve();
        }else{
            validarToken().then((result)=>{
                if(result){
                    resolve();
                }else{
                    reject();
                }
            });
        }
    });
}

var venta = new Venta();

window.afterLoad.push(function(){
    venta.initEvents();
    venta.onsend = false;
    venta.updateEmpty();     
});