<?php 
class Config{
	function __construct(){

	}

	function add_option($id,$default = '0'){
		$val = empty($_POST[$id])?$default:$_POST[$id];
		
		if(!get_option($id)){
			add_option($id,$val);			
		}
		update_option($id,$val);
	}

	function saveOptions(){		
		$this->add_option('bd_sysventas','sysventas');
		$this->add_option('bd_sysventas_stock');
		$this->add_option('bd_sysventas_productos');
		$this->add_option('bd_sysventas_categorias');		
		$this->add_option('bd_sysventas_precios');
		$this->add_option('bd_sysventas_descontar_on','');
		$this->add_option('bd_sysventas_procesado','');
	}

	function get_template(){
		ob_start();
		require_once JONCARPATH.'/admin/views/config.php';
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

	function render(){
		if(!empty($_POST)){
			$this->saveOptions();
		}
		echo $this->get_template();
	}
}