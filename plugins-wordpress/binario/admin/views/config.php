<div class="wrap">
	<?php if(!empty($_POST)): ?>
		<div class="notice notice-success">
			<p>Sus datos han sido guardados con éxito</p>
		</div>
	<?php endif ?>
	<form method="post" id="mainform" action="" enctype="multipart/form-data">
		<nav class="nav-tab-wrapper woo-nav-tab-wrapper">
			<a href="#" class="nav-tab nav-tab-active">General</a>
		</nav>
		<h1 class="screen-reader-text">General</h1>
		<h2>Conexión con el sistema</h2>
		<div id="store_address-description">
			<p>Aquí es donde está situada la configuración de sincronización con el sistema sysventas elaborado por Binario.com.py .</p>
		</div>
		<table class="form-table">
			<tbody>
				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="woocommerce_store_address">
							Nombre de base de datos del sistema
						</label>
					</th>
					<td class="forminp forminp-text">
						<input type="text" style="" value="<?php echo get_option('bd_sysventas','sysventas') ?>" name="bd_sysventas"> 						
					</td>
				</tr>
			</tbody>
		</table>
		<h2>Opciones generales</h2>
		<table class="form-table">
			<tbody>
				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="woocommerce_store_address">Sincronizar Stock<span class="woocommerce-help-tip"></span></label>
					</th>
					<td class="forminp forminp-text">
						<input type="checkbox" style="" class="" placeholder="" name="bd_sysventas_stock" value="1" <?php echo get_option('bd_sysventas_stock',0)==1?'checked="true"':'' ?>>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="woocommerce_store_address">Sincronizar Productos<span class="woocommerce-help-tip"></span></label>
					</th>
					<td class="forminp forminp-text">
						<input type="checkbox" style="" class="" placeholder="" value="1" name="bd_sysventas_productos" <?php echo get_option('bd_sysventas_productos',0)==1?'checked="true"':'' ?>>  
					</td>
				</tr>
				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="woocommerce_store_address">Sincronizar Categorías<span class="woocommerce-help-tip"></span></label>
					</th>
					<td class="forminp forminp-text">
						<input type="checkbox" style="" class="" placeholder="" value="1" name="bd_sysventas_categorias" <?php echo get_option('bd_sysventas_categorias',0)==1?'checked="true"':'' ?>>
					</td>
				</tr>	
				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="woocommerce_store_address">Sincronizar Precios<span class="woocommerce-help-tip"></span></label>
					</th>
					<td class="forminp forminp-text">
						<input type="checkbox" style="" class="" placeholder="" value="1" name="bd_sysventas_precios" <?php echo get_option('bd_sysventas_precios',0)==1?'checked="true"':'' ?>> 
					</td>
				</tr>
				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="woocommerce_store_address">Estado de venta para descontar stock de sysventas<span class="woocommerce-help-tip"></span></label>
					</th>
					<td class="forminp forminp-text">
						<?php 
							$ondescontar = get_option('bd_sysventas_descontar_on','');
						?>
						<select name="bd_sysventas_descontar_on">
							<option value="" <?php echo $ondescontar==''?'selected="true"':'' ?>>Sin seleccionar</option>
							<option value="wc-on-hold" <?php echo $ondescontar=='wc-on-hold'?'selected="true"':'' ?>>Pendiente</option>
							<option value="wc-processing" <?php echo $ondescontar=='wc-processing'?'selected="true"':'' ?>>Pago procesado</option>
							<option value="wc-completed" <?php echo $ondescontar=='wc-completed'?'selected="true"':'' ?>>Pedido entregado</option>
						</select>
					</td>
				</tr>

				<tr valign="top">
					<th scope="row" class="titledesc">
						<label for="woocommerce_store_address">Estado de venta cuando se procesa en sysventas<span class="woocommerce-help-tip"></span></label>
					</th>
					<td class="forminp forminp-text">
						<?php 
							$ondescontar = get_option('bd_sysventas_procesado','');
						?>
						<select name="bd_sysventas_procesado">
							<option value="" <?php echo $ondescontar==''?'selected="true"':'' ?>>Sin seleccionar</option>
							<option value="wc-on-hold" <?php echo $ondescontar=='wc-on-hold'?'selected="true"':'' ?>>Pendiente</option>
							<option value="wc-processing" <?php echo $ondescontar=='wc-processing'?'selected="true"':'' ?>>Pago procesado</option>
							<option value="wc-completed" <?php echo $ondescontar=='wc-completed'?'selected="true"':'' ?>>Pedido entregado</option>
						</select>
					</td>
				</tr>
			</tbody>
		</table>		
		<p class="submit">
			<button name="save" class="button-primary woocommerce-save-button" type="submit" value="Guardar los cambios">Guardar los cambios</button> 
			<button type="button" value="Guardar los cambios" class="button-secondary woocommerce-save-button">Sincronización Manual</button>
		</p>
	</form>
</div>