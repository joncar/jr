DROP PROCEDURE IF EXISTS updateProductos;
delimiter /
CREATE PROCEDURE updateProductos()
BEGIN
DECLARE varCat VARCHAR(255);
DECLARE dbName VARCHAR(255);
DECLARE canSyncronize INT;
SELECT IFNULL(option_value,'') INTO @dbName FROM wp_options WHERE option_name = 'bd_sysventas';
SELECT IFNULL(option_value,0) INTO canSyncronize FROM wp_options WHERE option_name = 'bd_sysventas_productos';

IF @dbName != '' AND canSyncronize = 1 THEN
	DROP TABLE IF EXISTS productos;
	CREATE TABLE productos(
		id INT,
		codigo VARCHAR(255),
	    nombre VARCHAR(255),
	    precio VARCHAR(255),
	    descripcion VARCHAR(255),
	    categoria VARCHAR(255),
	    subcategoria VARCHAR(255),
	    slug VARCHAR(255),
	    foto_principal VARCHAR(255),
	    base_url VARCHAR(255)
	);
	ALTER TABLE productos ADD INDEX(id);
	ALTER TABLE productos ADD INDEX(slug);
	ALTER TABLE productos ADD INDEX(codigo);
	SET @qry:= CONCAT('INSERT INTO productos SELECT 
	productos.id,
	productos.codigo,
	productos.nombre_comercial,
	productos.precio_venta,
	productos.descripcion,
	categoriaproducto.denominacion as categoria,
	sub_categoria_producto.nombre_sub_categoria_producto as subcategoria,
	toURL(productos.nombre_comercial),
	productos.foto_principal,
	ajustes.system_url
	FROM ',@dbName,'.productos
	LEFT JOIN ',@dbName,'.categoriaproducto ON categoriaproducto.id = productos.categoria_id
	LEFT JOIN ',@dbName,'.sub_categoria_producto ON sub_categoria_producto.id = productos.sub_categoria_producto_id
	JOIN ',@dbName,'.ajustes');
	PREPARE st FROM @qry;
	EXECUTE st;

	INSERT INTO wp_posts SELECT 
	NULL,
	1,
	NOW(),
	NOW(),
	descripcion,
	nombre,
	'',
	'publish',
	'closed',
	'closed',
	'',
	slug,
	'',
	'',
	NOW(),
	NOW(),
	'',
	0,
	'',
	0,
	'product',
	'',
	0
	FROM productos
	WHERE 
		toURL(nombre) NOT IN (SELECT post_name FROM wp_posts) AND
		productos.id NOT IN (SELECT meta_value FROM wp_postmeta WHERE meta_key = '_idLocal');

	##INsertamos categorias
	INSERT INTO wp_term_relationships SELECT 
	wp_posts.ID,
	wp_term_taxonomy.term_taxonomy_id,
	0
	FROM
	wp_posts
	INNER JOIN productos ON productos.slug = post_name
	INNER JOIN wp_terms ON wp_terms.slug = productos.categoria
	INNER JOIN wp_term_taxonomy ON wp_term_taxonomy.term_id = wp_terms.term_id AND wp_term_taxonomy.taxonomy = 'product_cat'
	WHERE post_type = 'product' AND post_status = 'publish' AND wp_posts.ID NOT IN (SELECT object_id FROM wp_term_relationships)
	GROUP BY wp_posts.ID;

	#Actualizamos idLocal
	INSERT INTO wp_postmeta SELECT NULL,
	wp_posts.ID,
	'_idLocal',
	productos.id
	FROM
	wp_posts
	INNER JOIN productos ON productos.slug = post_name
	WHERE post_type = 'product' AND post_status = 'publish' AND wp_posts.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key = '_idLocal');

	#Actualizamos precio
	INSERT INTO wp_postmeta SELECT NULL,
	wp_posts.ID,
	'_price',
	productos.precio
	FROM
	wp_posts
	INNER JOIN productos ON productos.slug = post_name
	WHERE post_type = 'product' AND post_status = 'publish' AND wp_posts.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key = '_price');

	INSERT INTO wp_postmeta SELECT NULL,
	wp_posts.ID,
	'_regular_price',
	productos.precio
	FROM
	wp_posts
	INNER JOIN productos ON productos.slug = post_name
	WHERE post_type = 'product' AND post_status = 'publish' AND wp_posts.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key = '_regular_price');

	#Actualizamos codigo
	INSERT INTO wp_postmeta SELECT NULL,
	wp_posts.ID,
	'_sku',
	productos.codigo
	FROM
	wp_posts
	INNER JOIN productos ON productos.slug = post_name
	WHERE post_type = 'product' AND post_status = 'publish' AND wp_posts.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key = '_sku');

	#Inicializamos stock
	INSERT INTO wp_postmeta SELECT NULL,
	wp_posts.ID,
	'_stock',
	0
	FROM
	wp_posts
	INNER JOIN productos ON productos.slug = post_name
	WHERE post_type = 'product' AND post_status = 'publish' AND wp_posts.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key = '_stock');

	#Inicializamos stock
	INSERT INTO wp_postmeta SELECT NULL,
	wp_posts.ID,
	'_stock_status',
	'outofstock'
	FROM
	wp_posts
	INNER JOIN productos ON productos.slug = post_name
	WHERE post_type = 'product' AND post_status = 'publish' AND wp_posts.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key = '_stock_status');

	#Inicializamos stock
	INSERT INTO wp_postmeta SELECT NULL,
	wp_posts.ID,
	'_tax_status',
	'taxable'
	FROM
	wp_posts
	INNER JOIN productos ON productos.slug = post_name
	WHERE post_type = 'product' AND post_status = 'publish' AND wp_posts.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key = '_tax_status');

	#Inicializamos stock
	INSERT INTO wp_postmeta SELECT NULL,
	wp_posts.ID,
	'_manage_stock',
	'yes'
	FROM
	wp_posts
	INNER JOIN productos ON productos.slug = post_name
	WHERE post_type = 'product' AND post_status = 'publish' AND wp_posts.ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key = '_manage_stock');

	#Actualizar ThumbNail _thumbnail_id
	##Eliminamos tabla temporal	

	ALTER TABLE productos ADD post_id INT;
	UPDATE 
		productos,(
			SELECT 
				wp_posts.ID,wp_posts.post_name,meta_value as codigo
			FROM
				wp_posts
			LEFT JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID AND wp_postmeta.meta_key = '_idLocal'
		) as wp_posts
	SET 
		productos.post_id = wp_posts.ID 
	WHERE 
		productos.slug = wp_posts.post_name OR productos.id = wp_posts.codigo;

	#Actualizamos información de productos
	UPDATE wp_posts,productos
	SET
		wp_posts.post_title = productos.nombre,
		wp_posts.post_content = productos.descripcion,
		wp_posts.post_name = productos.slug
	WHERE 
		wp_posts.ID = productos.post_id;
	
	#Actualizamos _sku
	UPDATE wp_postmeta, productos 
	SET 
		wp_postmeta.meta_value = productos.codigo 
	WHERE
		wp_postmeta.post_id = productos.post_id AND wp_postmeta.meta_key = '_sku';

	#Actualizamos Thumb

	INSERT INTO wp_postmeta
	SELECT 
	NULL,
	productos.post_id,
	'_thumbnail_val',
	CONCAT(productos.base_url,'img/productos/',productos.foto_principal)
	FROM productos 
	LEFT JOIN wp_postmeta ON wp_postmeta.post_id = productos.post_id AND wp_postmeta.meta_key = '_thumbnail_val'
	WHERE foto_principal IS NOT NULL AND wp_postmeta.meta_value IS NULL;

	#Actualizamos _thumbnail_id
	UPDATE wp_postmeta, productos 
	SET 
		wp_postmeta.meta_value = CONCAT(productos.base_url,'img/productos/',productos.foto_principal)
	WHERE
		wp_postmeta.meta_value != CONCAT(productos.base_url,'img/productos/',productos.foto_principal) AND wp_postmeta.post_id = productos.post_id AND wp_postmeta.meta_key = '_thumbnail_val';

	#Eliminamos productos que se eliminaron
	UPDATE wp_posts,(
		SELECT 
	    wp_posts.ID
	    FROM wp_posts
	    LEFT JOIN productos ON productos.post_id = wp_posts.ID
	    WHERE 
	    	wp_posts.post_type = 'product' AND 
	    	wp_posts.post_status != 'trash' AND 
	    	productos.codigo IS NULL
	) as productos SET wp_posts.post_status = 'trash' 
	WHERE wp_posts.ID = productos.ID;

	DROP TABLE IF EXISTS productos;
END IF;
END 