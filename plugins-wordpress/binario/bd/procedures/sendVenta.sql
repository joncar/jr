SELECT 
	NULL,
    1 as transaccion,
    2 as tipo_facturacion,
    1 as cliente,
    wp_posts.post_date as fecha,
    1 as caja,
    NULL as timbrados_id,
    0 as nro_factura,
    1 as sucursal,
    1 as usuario,
    1 as forma_pago,
    null as pedidos_id,
    0 as empleados_id,
    CONCAT('Pedido vendido desde WC #',wp_posts.ID) as observacion,
    0 as efectivo,
    0 as total_descuento,
    0 as subtotal,
    0 as descuento,
    total.total as total,
    0 as anulado,
    0 as quienanulo,
    NULL,
    NULL,
    NULL,
    NULL,
    0 as total_dolares,
    0 as total_reales,
    0 as total_credito,
    0 as total_debito,
    0 as total_cheque,
    0 as pago_dolares,
    0 as pago_reales,
    0 as pago_pesos,
    total.total as pago_guaranies,
    0 as total_efectivo,
    0 as vuelto1,
    0 as vuelto2,
    0 as vuelto3,
    0 as vuelto4,
    0 as vuelto5,
    '' as productos,
    0 as status,
    1 as cajadiaria,
    0 as servicios_id,
    0 as presupuestos_id,
    0 as pedidos_id,
    0 as pagado
FROM 
	`wp_posts` 
LEFT JOIN 
	wp_woocommerce_order_items ON wp_woocommerce_order_items.order_id = wp_posts.ID
LEFT JOIN 
	wp_woocommerce_order_itemmeta AS producto ON producto.order_item_id = wp_woocommerce_order_items.order_item_id AND producto.meta_key = '_product_id'
LEFT JOIN 
	wp_woocommerce_order_itemmeta AS cantidad ON cantidad.order_item_id = wp_woocommerce_order_items.order_item_id AND cantidad.meta_key = '_qty'
LEFT JOIN 
	(SELECT SUM(meta_value) AS total,order_item_id FROM wp_woocommerce_order_itemmeta WHERE meta_key = '_line_total' GROUP BY order_item_id) total ON total.order_item_id = wp_woocommerce_order_items.order_item_id
    
WHERE 
	post_type = 'shop_order' AND ID = '11632';