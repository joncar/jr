DROP PROCEDURE IF EXISTS updateStock;
delimiter /
CREATE PROCEDURE updateStock()
BEGIN	
	DECLARE varCat VARCHAR(255);
	DECLARE dbName VARCHAR(255);
	DECLARE canSyncronize INT;
	SELECT IFNULL(option_value,'') INTO @dbName FROM wp_options WHERE option_name = 'bd_sysventas';
	SELECT IFNULL(option_value,0) INTO canSyncronize FROM wp_options WHERE option_name = 'bd_sysventas_stock';

	IF @dbName != '' AND canSyncronize = 1 THEN
		SET @qry:= CONCAT('SELECT sucursal_web INTO @varSucursal FROM ',@dbName,'.ajustes');
		PREPARE st FROM @qry;
		EXECUTE st;
		DROP TABLE IF EXISTS stock;	
		CREATE TABLE stock(producto VARCHAR(255),precio_venta varchar(255),stock INT,post_id INT,sucursal INT);
		ALTER TABLE stock ADD INDEX(producto);
		ALTER TABLE stock ADD INDEX(post_id);
		SET @qry:= CONCAT("
		INSERT INTO stock SELECT 
		producto,
		precio_venta,
		stock,
		wp_postmeta.post_id,
		sucursal
		FROM ",@dbName,".productosucursal 
		INNER JOIN mappin.wp_postmeta ON wp_postmeta.meta_key = '_sku' AND wp_postmeta.meta_value = producto
		WHERE sucursal = @varSucursal");
		PREPARE st FROM @qry;
		EXECUTE st;

		##Actualizamos Stock
		UPDATE wp_postmeta, stock SET wp_postmeta.meta_value = stock.stock WHERE wp_postmeta.post_id = stock.post_id AND wp_postmeta.meta_key = '_stock';
		##Actualizamos precio
		UPDATE wp_postmeta, stock SET wp_postmeta.meta_value = stock.precio_venta WHERE wp_postmeta.post_id = stock.post_id AND (wp_postmeta.meta_key = '_price' OR wp_postmeta.meta_key = '_regular_price');
		##Actualizamos dispnibilidad
		UPDATE wp_postmeta,stock SET wp_postmeta.meta_value = 'instock' WHERE wp_postmeta.meta_key = '_stock_status' AND stock.stock > 0 AND wp_postmeta.post_id = stock.post_id;
		UPDATE wp_postmeta,stock SET wp_postmeta.meta_value = 'outofstock' WHERE wp_postmeta.meta_key = '_stock_status' AND stock.stock <= 0 AND wp_postmeta.post_id = stock.post_id;
		DROP TABLE IF EXISTS stock;
	END IF;
END 