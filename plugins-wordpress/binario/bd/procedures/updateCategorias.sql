DROP PROCEDURE IF EXISTS updateCategorias;
delimiter /
CREATE PROCEDURE updateCategorias()
BEGIN
DECLARE varCat VARCHAR(255);
DECLARE dbName VARCHAR(255);
DECLARE canSyncronize INT;

SELECT IFNULL(option_value,'') INTO @dbName FROM wp_options WHERE option_name = 'bd_sysventas';
SELECT IFNULL(option_value,0) INTO canSyncronize FROM wp_options WHERE option_name = 'bd_sysventas_categorias';

IF @dbName != '' AND canSyncronize = 1 THEN
	SET @varCat:= CONCAT('CREATE TABLE IF NOT EXISTS categorias LIKE ',@dbName,'.categoriaproducto');
	PREPARE st FROM @varCat;
	EXECUTE st;
	SET @varCat:= CONCAT('INSERT INTO categorias SELECT * FROM ',@dbName,'.categoriaproducto');
	PREPARE st FROM @varCat;
	EXECUTE st;
	##Insertar categorias
	INSERT INTO wp_terms SELECT 
	NULL,
	categorias.denominacion,
	toURL(categorias.denominacion),
	0
	FROM categorias
	WHERE toURL(categorias.denominacion) NOT IN (SELECT slug FROM wp_terms);
	##Insertar taxonomias
	INSERT INTO wp_term_taxonomy SELECT NULL,term_id,'product_cat','',0,0
	FROM wp_terms 
	WHERE term_id NOT IN (SELECT term_id FROM wp_term_taxonomy);
	##Eliminamos tabla temporal
	DROP TABLE IF EXISTS categorias;

	##Insertamos subcategorias
	CREATE TABLE IF NOT EXISTS categorias(id INT,nombre VARCHAR(255), parent VARCHAR(255));

	SET @varCat:= CONCAT('INSERT INTO categorias SELECT sub_categoria_producto.id,sub_categoria_producto.nombre_sub_categoria_producto,toURL(categoriaproducto.denominacion) FROM ',@dbName,'.sub_categoria_producto INNER JOIN ',@dbName,'.categoriaproducto ON categoriaproducto.id = sub_categoria_producto.categoriaproducto_id');
	PREPARE st FROM @varCat;
	EXECUTE st;

	INSERT INTO wp_terms SELECT 
	NULL,
	categorias.nombre,
	toURL(categorias.nombre),
	0
	FROM categorias
	WHERE categorias.nombre NOT IN (SELECT name FROM wp_terms);

	##insertamos taxonomia
	INSERT INTO wp_term_taxonomy SELECT 
	NULL,
	wp_terms.term_id,
	'product_cat',
	'',
	rel.term_id,
	0
	FROM wp_terms 
	INNER JOIN categorias ON categorias.nombre = wp_terms.name
	INNER JOIN wp_terms AS rel ON rel.slug = categorias.parent
	WHERE wp_terms.term_id NOT IN (SELECT term_id FROM wp_term_taxonomy)
	GROUP BY wp_terms.term_id;

	##Eliminamos tabla temporal
	DROP TABLE IF EXISTS categorias;
END IF;
END 