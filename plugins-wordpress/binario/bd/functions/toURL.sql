DROP FUNCTION IF EXISTS toURL;
delimiter /
CREATE FUNCTION toURL(descr VARCHAR(255)) RETURNS varchar(255)
BEGIN
	SELECT TRIM(descr) into descr;
    SELECT REPLACE(descr,'á','a') into descr;
    SELECT REPLACE(descr,'é','e') into descr;
    SELECT REPLACE(descr,'í','i') into descr;
    SELECT REPLACE(descr,'ó','o') into descr;
    SELECT REPLACE(descr,'ú','u') into descr;
    SELECT REPLACE(descr,'à','a') into descr;
    SELECT REPLACE(descr,'è','e') into descr;
    SELECT REPLACE(descr,'ì','i') into descr;
    SELECT REPLACE(descr,'ò','o') into descr;
    SELECT REPLACE(descr,'ù','u') into descr;    
    SELECT REPLACE(descr,'+','') into descr;
    SELECT REPLACE(descr,'-','') into descr;
    SELECT REPLACE(descr,' ','-') into descr;
    SELECT REPLACE(descr,'--','-') into descr;
    SELECT REPLACE(descr,'(','') into descr;
    SELECT REPLACE(descr,')','') into descr;
    SELECT REPLACE(descr,'.','') into descr;
    SELECT REPLACE(descr,'/','-') into descr;
    SELECT LCASE(descr) into descr;
    return descr;
END