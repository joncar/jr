<?php
/*
Plugin Name: Binario
Plugin URI: https://www.binario.com.py/
Description: Custom plugin to this webpage
Version: 1
Author: Binario
Author URI: https://www.binario.com.py/
Text Domain: binario
*/
@define('JONCARPATH',dirname(__FILE__));
require_once JONCARPATH.'/actions.php';
require_once JONCARPATH.'/functions.php';
require_once JONCARPATH.'/filters.php';
require_once JONCARPATH.'/shortcodes.php';
require_once JONCARPATH.'/helpers.php';
require_once JONCARPATH.'/widgets.php';

add_action("wp_enqueue_scripts",function(){	
	wp_add_inline_script('uri', 'var URI = \''.site_url().'\';' );
    wp_register_script('general', site_url().'/wp-content/plugins/bluepixel/views/js/general.js', array('jquery'), '2.3', true,999);
    wp_enqueue_script('general');

    wp_register_style( 'frontCSS', site_url().'/wp-content/plugins/bluepixel/views/css/style.css' );
    wp_enqueue_style( 'frontCSS' );  
});

add_action( 'admin_enqueue_scripts',function(){
	wp_enqueue_script( 'uri', 'https://use.typekit.net/.js', array(), '1.0' );
	wp_add_inline_script('uri', 'window.$ = window.jQuery;' );
});