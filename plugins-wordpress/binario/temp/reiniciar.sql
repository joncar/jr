DROP TABLE wp_posts;
DROP TABLE wp_postmeta;
DROP TABLE wp_terms;
DROP TABLE wp_term_relationships;
DROP TABLE wp_term_taxonomy;

CREATE TABLE wp_posts LIKE _wp_posts;
CREATE TABLE wp_postmeta LIKE _wp_postmeta;
CREATE TABLE wp_terms LIKE _wp_terms;
CREATE TABLE wp_term_relationships LIKE _wp_term_relationships;
CREATE TABLE wp_term_taxonomy LIKE _wp_term_taxonomy;

INSERT INTO wp_posts SELECT * FROM _wp_posts;
INSERT INTO wp_postmeta SELECT * FROM _wp_postmeta;
INSERT INTO wp_terms SELECT * FROM _wp_terms;
INSERT INTO wp_term_relationships SELECT * FROM _wp_term_relationships;
INSERT INTO wp_term_taxonomy SELECT * FROM _wp_term_taxonomy;