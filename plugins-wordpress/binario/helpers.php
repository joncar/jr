<?php 

/************* FUNCTIONS GENERALS ******************/

function sqlToHtml($qry,$columns = array(),$labels = array(),$callbacks = array()){
    if(count($qry)>0): ?>
        <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
            <thead class="thin-border-bottom">
                <tr>  
                    <?php foreach($qry[0] as $n=>$q): if(empty($columns) || in_array($n,$columns)): ?>
                        <th class="woocommerce-orders-table__header" data-name="<?php echo $n ?>" style="cursor:pointer">
                        	<span class="nobr">
                            	<?= empty($labels) || !array_key_exists($n,$labels)?ucfirst(str_replace('_',' ',$n)):$labels[$n] ?>
                                <?php 
                                    if(!empty($_GET['order_by']) && $_GET['order_by']==$n):
                                ?>
                                    <i class="fa fa-chevron-up"></i>
                                <?php endif ?>
                            </span>
                        </th>
                    <?php endif; endforeach;   ?>
                </tr>
            </thead>

            <tbody>
                <?php $x = 0; foreach($qry as $nn=>$qq): ?>
                    <tr class="woocommerce-orders-table__row order">
                        <?php foreach($qq as $n=>$q): if(empty($columns) || in_array($n,$columns)): ?>
                             <td class="woocommerce-orders-table__cell"><?php 
                                $out = $q;
                                if(!empty($callbacks && array_key_exists($n,$callbacks))){
                                    $out = call_user_func($callbacks[$n],$q,$qq,$x);
                                }
                                echo $out;

                            ?></td>
                        <?php endif;  endforeach; ?>
                    </tr>
                <?php $x++; endforeach;   ?>
            </tbody>
        </table>
    <?php else: ?>
        Sin datos para mostrar
    <?php endif;
}

function html_to_obj($html) {
    $dom = new DOMDocument();
    $dom->loadHTML($html);
    return element_to_obj($dom->documentElement);
}

function element_to_obj($element) {
    $obj = array( "tag" => $element->tagName );
    foreach ($element->attributes as $attribute) {
        $obj[$attribute->name] = $attribute->value;
    }
    foreach ($element->childNodes as $subElement) {
        if ($subElement->nodeType == XML_TEXT_NODE) {
            $obj["html"] = $subElement->wholeText;
        }
        else {
            $obj["children"][] = element_to_obj($subElement);
        }
    }
    return $obj;
}

function form_dropdown_from_query($name,$table,$valueField,$labelField,$selected = "",$extras = "",$filter = "",$order = ""){
    GLOBAL $wpdb;
    if(is_array($valueField)){
        $args = $valueField;
        $valueField = $args['valueField'];
        $labelField = $args['labelField'];
        $selected = @$args['selected'];
        $extras = @$args['extras'];
        $filter = @$args['filter'];
        $order = @$args['order'];
    }
    $str = '<select name="'.$name.'" '.$extras.'>';
    $str.= '<option value="">Selecciona una opción</option>';
    if(is_string($table)){
        $qry = "SELECT * FROM {$wpdb->prefix}$table";
        if(!empty($filter)){
            $qry.= " WHERE ".$filter;
        }
        if(!empty($order)){
            $qry.= " ORDER BY ".$order;
        }
        $data = $wpdb->get_results($qry);
        //echo $qry;
    }else{
        $data = $table;
    }    
    foreach($data as $d){        
        $label = $labelField;
        if(strpos($labelField,'{')>-1){
            foreach($d as $n=>$f){
                $label = str_replace('{'.$n.'}',$f,$label);
            }
        }else{
            $label = $d->{$label};
        }
        $sel = $d->{$valueField} == $selected ? 'selected="true"':'';
        $str.= '<option value="'.$d->{$valueField}.'" '.$sel.'>'.$label.'</option>';
    }
    $str.= '</select>';
    return $str;
}