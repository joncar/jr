<?php 

//Añadir menu lateral
add_action('admin_menu',function(){
	add_menu_page(
        'Configuración de sysventas', // Title of the page
        'Sysventas', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        JONCARPATH.'/admin/config.php' // The 'slug' - file to display when clicking the link,
	);
});

//Cargar procedures
add_action( 'activated_plugin',function(){
	GLOBAL $wpdb;
	ob_start();
	require_once JONCARPATH.'/bd/procedures/updateCategorias.sql';
	$sql = ob_get_contents();
	ob_clean();
	$wpdb->get_results($wpdb);
});

//INIT
add_action('init',function(){	
	if(!empty($_GET['binario_cron'])){
		do_action('binario_cron_'.$_GET['binario_cron']);
		echo 'success';
	}
});

add_action('binario_cron_addThumbs',function(){
	require_once(ABSPATH . 'wp-admin/includes/media.php');
	require_once(ABSPATH . 'wp-admin/includes/file.php');
	require_once(ABSPATH . 'wp-admin/includes/image.php');
	GLOBAL $wpdb;
	$query = "select * from wp_postmeta WHERE meta_key = '_thumbnail_val'";
	$result = $wpdb->get_results($query);
	foreach($result as $r){
		$producto = wc_get_product($r->post_id);
		$imagen = get_the_post_thumbnail_url($r->post_id);
		$remote = explode('/',$r->meta_value);
		$remote = array_pop($remote);
		if($remote != basename($imagen)){
			$file = $r->meta_value;
			$filename = basename($file);			
			$upload_file = wp_upload_bits($filename, null, file_get_contents($file));
			if (!$upload_file['error']) {
				$wp_filetype = wp_check_filetype($filename, null );
				$attachment = array(
					'post_mime_type' => $wp_filetype['type'],
					'post_parent' => $parent_post_id,
					'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
					'post_content' => '',
					'post_status' => 'inherit'
				);
				$attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $r->post_id );
				if (!is_wp_error($attachment_id)) {
					require_once(ABSPATH . "wp-admin" . '/includes/image.php');
					$attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
					wp_update_attachment_metadata( $attachment_id,  $attachment_data );
				}
				// And finally assign featured image to post
				set_post_thumbnail( $r->post_id, $attachment_id );
			}
		}
	}
	die();
});

add_action('binario_cron_updateCategorias',function(){	
	GLOBAL $wpdb;
	$query = "CALL updateCategorias()";
	$result = $wpdb->query($query);	
	echo "Categorias importadas desde sysventas";
	die();
});