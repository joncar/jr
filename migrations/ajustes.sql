ALTER TABLE `ajustes` ADD IF NOT EXISTS `id_reporte_ticket_pagocliente` INT NULL AFTER `id_reporte_presupuestos`, ADD IF NOT EXISTS `id_reporte_pagocliente` INT NULL AFTER `id_reporte_ticket_pagocliente`;
#Permitir pago superior a saldo
ALTER TABLE `ajustes` ADD IF NOT EXISTS `permitir_pago_superior_a_saldo` BOOLEAN NULL DEFAULT FALSE AFTER `id_reporte_PedidosProveedores`;

ALTER TABLE ajustes ADD IF NOT EXISTS habilitar_cache INT(1) DEFAULT 0

#Campos web
ALTER TABLE ajustes ADD IF NOT EXISTS db_web VARCHAR(255);
ALTER TABLE ajustes ADD IF NOT EXISTS sucursal_web INT;
ALTER TABLE ajustes ADD IF NOT EXISTS sincronizar_stock BOOLEAN NULL DEFAULT 0 AFTER sucursal_web; 
ALTER TABLE ajustes ADD IF NOT EXISTS system_url VARCHAR(255) NULL DEFAULT 'https://nube.binario.com.py/sysventas/';

ALTER TABLE ajustes ADD IF NOT EXISTS cantidad_maxima_cuotas_plan_credito INT NULL DEFAULT 48;

##Controlar fecha de vencimiento en stock
ALTER TABLE ajustes ADD IF NOT EXISTS controlar_vencimiento_stock BOOLEAN NULL DEFAULT FALSE AFTER cantidad_maxima_cuotas_plan_credito; 

ALTER TABLE ventadetalle ADD IF NOT EXISTS vencimiento VARCHAR(255) NULL AFTER `comision`;
ALTER TABLE transferencias_detalles ADD IF NOT EXISTS vencimiento VARCHAR(255) NULL AFTER `cantidad`;

DROP TRIGGER IF EXISTS `entradas_after_insert`; TRIGGER `entradas_after_insert` AFTER INSERT ON `entrada_productos_detalles` FOR EACH ROW call ajust_stock_from_entrada_detalle(NEW.id)
DROP TRIGGER IF EXISTS `transferencias_after_insert`; TRIGGER ` transferencias_after_insert` AFTER INSERT ON `transferencias_detalles` FOR EACH ROW call ajust_stock_from_transferencias_detalles(NEW.id)

ALTER TABLE `ajustes` ADD IF NOT EXISTS `id_reporte_nota_credito` INT NULL AFTER `id_reporte_pagocliente`;

ALTER TABLE compras ADD IF NOT EXISTS user_id INT(11) NOT NULL;
ALTER TABLE compras ADD IF NOT EXISTS fecha_registro DATETIME DEFAULT 0;

##Parent_id en productos
ALTER TABLE productos ADD IF NOT EXISTS parent_fracc VARCHAR(255) NULL AFTER `codigo`;
ALTER TABLE productos ADD IF NOT EXISTS parent_codigo VARCHAR(255) NULL AFTER `codigo`;

ALTER TABLE plan_credito ADD IF NOT EXISTS dias_retraso INT(11) NULL DEFAULT 0 AFTER monto_a_pagar;
ALTER TABLE plan_credito ADD IF NOT EXISTS fecha_pago DATETIME NULL AFTER monto_a_pagar;
#UPDATE PROCEDURE updatePagos.sql

#Campo modificar precio en productos
ALTER TABLE `productos` ADD IF NOT EXISTS `mod_precio` BOOLEAN NULL DEFAULT FALSE AFTER `balanza`;

#Campo max_items tipo_facturacion
ALTER TABLE `tipo_facturacion` ADD IF NOT EXISTS `max_items` INT NULL DEFAULT -1;