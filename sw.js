const CACHE_NAME = "sysventas_v1",
urlsToCache = [	
	'./css/style.css',
	'./assets/plugins/global/plugins.bundle.css',
    './css/style.bundle.css',
    './css/skins/header/base/light.css',
    './css/skins/header/menu/light.css',
    './css/skins/brand/dark.css',
    './css/skins/aside/dark.css',
    './assets/plugins/global/plugins.bundle.min.js',
    './js/scripts.bundle.js',
    './js/jquery-migrate.min.js',
    './js/pages/dashboard.js',
]

self.addEventListener('install',e=>{
	e.waitUntil(
		caches.open(CACHE_NAME)
		.then(cache=>{
			return cache.addAll(urlsToCache)
				   .then(()=>self.skipWaiting())
		})
		.catch(err=>log.warn(err))
	);
})

//Hacer que funcione sin conexión
self.addEventListener('activate',e=>{
	const cacheWhiteList = [CACHE_NAME]
	e.waitUntil(
		caches.keys()
		.then(cachesNames=>{
			cachesNames.map(cacheName =>{
				//Eliminar lo que ya no necesitamos en cache
				if(cacheWhiteList.indexOf(cacheName) == -1){
					return caches.delete(cacheName)
				}
			})
		})//Activar el cache actual
		.then(()=>{
			self.clients.claim()
		})
	)
})

//Hacer que funcione sin conexión
/*self.addEventListener('fetch',e=>{
	e.respondWith(
		caches.match(e.request)
		.then(res=>{
			if(res){
				//Recuperar desde el cache
				return res;
			}
			//Si no esta en cache la url la busca
			return fetch(e.request)
		})
	)
});*/