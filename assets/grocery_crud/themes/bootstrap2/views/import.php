<div id="<?php echo $unique_hash; ?>import" class="modal fade" tabindex="-1" role="dialog">
	<form action="<?= $import_url ?>" onsubmit="CRUDsendForm(this,'#<?php echo $unique_hash; ?>response'); return false;">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Importar datos</h4>
	      </div>
	      <div class="modal-body">
	      	  <div id="<?php echo $unique_hash; ?>response"></div>			  
			  <div class="form-group">
			    <label for="exampleInputFile">Fichero</label>
			    <input type="file" id="exampleInputFile" name="file" accept=".xls">
			    <p class="help-block"><b>Nota: </b> El fichero debe tener el mismo formato de el modulo de exportación de tabla</p>
			  </div>
			  <div class="checkbox">
			    <label>
			      <input type="checkbox" name="sobrescribe" value="1"> Sobrescribir tabla
			    </label>
			  </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary">Importar</button>
	      </div>
	  </div><!-- /.modal-content -->
		 </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->