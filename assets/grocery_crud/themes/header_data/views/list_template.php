<?php if(!empty($list)): ?>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">Datos de <?= $subject ?></h3>
                <a href="<?= str_replace('ajax_list','',$ajax_list_url) ?>" style="position:absolute; right: 28px; top: 10px;"><i class="fa fa-edit"></i> Cambiar</a>
            </div>
        </div>
        <div class="panel-body">
            <form class=""> 
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="row">
                        <?php foreach ($columns as $column): ?>
                            <div class="form-group col-xs-6 col-sm-2">
                              <label for="id"><?= $column->display_as ?>: </label>
                              <input type="text" class="form-control" id="<?= $column->field_name ?>" value="<?= strip_tags($list[0]->{$column->field_name}) ?>" readonly="">
                            </div>
                        <?php endforeach ?>
                    </div>   
                </div> 
            </form>
        </div>
    </div>
<?php else: ?>
Sin datos para mostrar
<?php endif ?>
