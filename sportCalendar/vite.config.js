import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  build: {
		// entry: 'src/main.js',
		// outDir: resolve(__dirname, 'dist'),
		rollupOptions: {
			output: {
				// Default
				// dir: 'dist',
				// With laravel: laravel-app/public/js
				dir: './public/js',
				entryFileNames: 'app-[name].js',
				assetFileNames: 'app-[name].css',
				chunkFileNames: "chunk-[name].js",
				manualChunks: undefined,
			}
		}
	}
})
