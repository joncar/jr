import {useContext, useEffect,useState} from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import {URI} from './Params'

//ICONS
import AddCardIcon from '@mui/icons-material/AddCard';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import CreditCardIcon from '@mui/icons-material/CreditCard';
import axios from 'axios';

export default function({reserva,setReserva,syncReservas}) {
  const borrar = async ()=>{
    if(confirm('Esta accion no se puede reversar, esta seguro que desea eliminar esta reserva?')){
        try{
            await axios.get(`${URI}/sport/sport_reservas_canchas/delete/${reserva}`);
        }catch(e){

        }
        syncReservas();
    }
  }
  return (
    <div>
      <Drawer
            anchor={'bottom'}
            open={reserva!=-1}
            onClose={()=>{setReserva(-1)}}
          >
            <Box
                sx={{ width: 'auto' }}
                role="presentation"
                //onClick={close}
                //onKeyDown={close}
                >
                <List>
                    <ListItem>
                        <ListItemButton onClick={()=>{window.location.href = `${URI}/movimientos/ventas/ventas/add/Sport/${reserva}`}}>
                            <ListItemIcon>
                                <CreditCardIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Facturar"} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem>
                        <ListItemButton onClick={()=>{window.location.href = `${URI}/sport/sport_reservas_canchas/edit/${reserva}`}}>
                            <ListItemIcon>
                                <EditIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Editar"} />
                        </ListItemButton>
                    </ListItem>
                    <ListItem>
                        <ListItemButton onClick={borrar}>
                            <ListItemIcon>
                                <DeleteIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Eliminar"} />
                        </ListItemButton>
                    </ListItem>
                </List>
            </Box>
        </Drawer>
    </div>
  );
}