import { useState,useEffect } from 'react'
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import { ViewState } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  DayView,
  WeekView,
  MonthView,
  Toolbar,
  DateNavigator,
  Appointments,
  TodayButton,
  ViewSwitcher
} from '@devexpress/dx-react-scheduler-material-ui';
import CanchasSwitch from './CanchasSwitch';
import {URI} from './Params'
import EditModal from './EditModal'
import axios from 'axios';
import { Appointment } from './Appointment.jsx';

const currentDate = new Date();
/*const schedulerData = [
  { startDate: '2018-11-01T09:45', endDate: '2018-11-01T11:00', title: 'Juan Cuesta' },
  { startDate: '2018-11-01T12:00', endDate: '2018-11-01T13:30', title: 'Ruben Ramirez' },
];*/
function Calendario() {
  const [data,setData] = useState([]);
  const [cancha,setCancha] = useState(0);
  const [fecha,setFecha] = useState(new Date());
  const [reserva,setReserva] = useState(-1);
  const syncData = async ()=>{
    /*let d = await axios.post(`${URI}/api/sportApi/getReservas`,{
        cancha,
        fecha
    },{headers: { "Content-Type": "multipart/form-data" }})*/
    let d = await axios({
        url:`${URI}/api/sportApi/getReservas`,
        method:'post',
        data:{
            cancha,
            fecha
        },
        headers: { "Content-Type": "multipart/form-data" }
    });
    d.data = d.data.map((x)=>({...x,setReserva:setReserva}));
    setData(d.data);
  }
  useEffect(()=>{
    syncData();
  },[cancha]);

  return (
    <div>
      <Paper>
        <Scheduler
          data={data}
          locale={'es-ES'}
          firstDayOfWeek={1}
          
        >
          <ViewState
            defaultCurrentDate={currentDate}
            onCurrentDateChange={(e)=>{setFecha(e)}}
          />
          <MonthView  displayName="Mensual"/>
          <DayView
            startDayHour={9}
            endDayHour={23}
            displayName="Diario" 
          />
          <WeekView
            startDayHour={10}
            endDayHour={23}
            displayName="Semanal"
          />
          <Toolbar />
          <ViewSwitcher />
          <DateNavigator />
          <TodayButton  messages={{today:'Hoy'}}/>
          <CanchasSwitch cancha={cancha} setCancha={setCancha}/>
          <Appointments appointmentComponent={Appointment}/>
        </Scheduler>
      </Paper>
      <EditModal reserva={reserva} setReserva={setReserva} syncReservas={syncData}/>
    </div>
  )
}

export default Calendario
