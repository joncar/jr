import {
    Appointments
  } from '@devexpress/dx-react-scheduler-material-ui';
const Appointment = ({ data, ...restProps }) => {
    return (
      <Appointments.Appointment style={{background:data.facturado==0?'#1976d2':'#fd397a','color':'#fff',minHeight:'30px'}}>
        <div className="dayView" onClick={()=>{data.setReserva(data.id)}}>
            <div style={{padding:'10px'}}>
            <span style={{fontSize:'10px'}}>#{data.id} - {data.title}</span><br/>
            </div>
        </div>
      </Appointments.Appointment>
    )
  };

  export {Appointment}