import { useState,useEffect } from 'react'
import { Plugin, Template,TemplatePlaceholder } from '@devexpress/dx-react-core';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import {URI} from './Params'
import axios from 'axios';

export default function CanchasSwitch({
    cancha,
    setCancha
}) {
    const [data,setData] = useState([]);
    const consultarCanchas = async()=>{
        let c = await axios.get(`${URI}/api/sportApi/getCanchas`);
        setData(c.data);
    }
    useEffect(()=>{
        consultarCanchas()
    },[])
    return (
        <Plugin name={'CanchasSwitch'} dependencies={[
            { name: 'Toolbar' },
            { name: 'ViewState' },
        ]}>
            <Template name={'toolbarContent'}>
                <TemplatePlaceholder/>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={cancha}
                    label="Age"
                    onChange={(e)=>{setCancha(e.target.value)}}
                >
                    <MenuItem value={0}>Selecciona una cancha</MenuItem>
                    {
                        data.map(cancha=><MenuItem value={cancha.id} key={cancha.id}>{cancha.denominacion}</MenuItem>)
                    }
                </Select>
                
            </Template>
        </Plugin>
    )
}