import { useState } from 'react'
import Paper from '@mui/material/Paper';
import { ViewState } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  DayView,
  WeekView,
  MonthView,
  Toolbar,
  DateNavigator,
  Appointments,
  TodayButton,
  ViewSwitcher
} from '@devexpress/dx-react-scheduler-material-ui';
const currentDate = '2018-11-01';
const schedulerData = [
  { startDate: '2018-11-01T09:45', endDate: '2018-11-01T11:00', title: 'Juan Cuesta' },
  { startDate: '2018-11-01T12:00', endDate: '2018-11-01T13:30', title: 'Ruben Ramirez' },
];
function App() {
  const [data,setData] = useState(schedulerData);
  return (
    <Paper>
        <Scheduler
          data={data}
        >
          <ViewState
            defaultCurrentDate={currentDate}
          />
          <MonthView displayName="Mensual"/>
          <DayView
            startDayHour={9}
            endDayHour={23}
            displayName="Diario" 
          />
          <WeekView
            startDayHour={10}
            endDayHour={19}
            displayName="Semanal"
          />
          <Toolbar />
          <ViewSwitcher />
          <DateNavigator />
          <TodayButton  messages={{today:'Hoy'}}/>
          <Appointments />
        </Scheduler>
      </Paper>
  )
}

export default App
